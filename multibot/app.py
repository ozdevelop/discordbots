import builtins
import os
import logging
import threading
import time
import asyncio
from dotenv import load_dotenv
from bot import MultiBot
from sys import argv
from prompt_toolkit import history, prompt, PromptSession

load_dotenv()

format = "%(asctime)s: %(message)s"

logging.basicConfig(format=format, level=logging.INFO,

                    datefmt="%H:%M:%S")

# AWAIT_BOT_ADD = False

def print(*args, spacer=", "):
    logging.info(spacer.join([str(o) for o in args]))


class App:

    def __init__(self, input_init_delay=4, wait_for_loop=5, prompt_history_file=".prompt_history"):
        self.stop_thread = False
        self.input_init_delay = input_init_delay
        self.wait_for_loop = wait_for_loop
        self.prompt_history_file = prompt_history_file

    def input_loop(self, stop=".quit"):
        time.sleep(self.input_init_delay)

        # helper commands:

        bot = self.bot

        prompt_history = history.FileHistory(self.prompt_history_file)

        prompt_session = PromptSession(history=prompt_history)

        while not self.stop_thread:
            inp = prompt_session.prompt(">>> ", mouse_support=True)
            if inp == stop:
                break
            if inp == '':
                continue
            if inp.startswith(".clear"):
                os.system("clear")
                continue

            try:
                ret = exec(inp)
                if ret is not None:
                    builtins.print(ret)
            except Exception as e:
                ret = str(e)
                builtins.print(ret)
        print("Closed input thread.")
        print("Stopping bot...")

        self.future.close()

    async def run(self):
        print("Starting App")
        self.token = os.getenv("TEST_TOKEN" if any(
            [i in argv for i in ["--test", "-t", "--dev", "-d"]]) else "TOKEN")
        # global AWAIT_BOT_ADD
    
        # AWAIT_BOT_ADD = any([i in argv for i in ["-w","--wait","-a","--await"]])
        # print(self.token)
        self.bot = MultiBot()
        await self.bot._load_extensions()
        print("Created Bot")
        self.start = self.bot.start
        self.close = self.bot.close

        thread = threading.Thread(target=self.input_loop)

        thread.start()
        print(
            f"Created and started input thread ({self.input_init_delay}s delay)")

        try:
            print("Starting bot")
            self.future = self.start(self.token)
            await self.future
        except KeyboardInterrupt:
            logging.exception("Received KeyboardInterrupt, Stopping bot...")
            await self.close()
            # cancel all tasks lingering
        except Exception as e:
            logging.exception(
                f"Received {e.__class__} Exception. Bot was probably stopped from input-thread.")
        finally:
            print("Stopping Bot...")
            await self.close()
        print("App Stopped.")
        self.stop_thread = True


if __name__ == "__main__":
    
    
    
    a = App()
    asyncio.run(a.run())
