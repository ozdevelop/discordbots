import platform
import d20

from discord.ext import commands
import discord
import re
import random
import cogs.utils.utils as utils

import generators.generators as generators
import generators.gens as gens
from bot import MultiBot
from os.path import isfile


def clamp(x, l, u): return max(l, min(u, x))


class init_tracker_item:
    name: str = ""
    initiative: int = 0
    ac: int = 0
    max_hp: int = 0
    hp: int = 0
    Str: int = 0
    Dex: int = 0
    Con: int = 0
    Int: int = 0
    Wis: int = 0
    Cha: int = 0
    notes: str = ""

    def __init__(self, name, initiative, ac, max_hp, hp, Str, Dex, Con, Int, Wis, Cha, notes):
        self.name = name
        self.initiative = initiative
        self.ac = ac
        self.max_hp = max_hp
        self.hp = hp
        self.Str = Str
        self.Dex = Dex
        self.Con = Con
        self.Int = Int
        self.Wis = Wis
        self.Cha = Cha
        self.notes = notes

    def __eq__(self, other):
        if type(other) == str:
            return other == self.name
        return self.initiative == other.initiative if type(other) == type(self) else self.initiative == other

    def __gt__(self, other):
        return self.initiative > other.initiative if type(other) == type(self) else self.initiative > other

    def __ge__(self, other):
        return self.initiative >= other.initiative if type(other) == type(self) else self.initiative >= other

    def __lt__(self, other):
        return self.initiative < other.initiative if type(other) == type(self) else self.initiative < other

    def __le__(self, other):
        return self.initiative <= other.initiative if type(other) == type(self) else self.initiative <= other

    def __hash__(self):
        return hash(self.initiative)

    def __str__(self):
        scores = "" if any([i is None for i in [self.Str, self.Dex, self.Con, self.Int, self.Wis, self.Cha]]
                           ) else f", Scores:{'+' if self.Str > 0 else ''}{self.Str},{'+' if self.Dex > 0 else ''}{self.Dex},{'+' if self.Con > 0 else ''}{self.Con},{'+' if self.Int > 0 else ''}{self.Int},{'+' if self.Wis > 0 else ''}{self.Wis},{'+' if self.Cha > 0 else ''}{self.Cha}"
        return f"{self.name} (Init:{self.initiative}):\nAC{self.ac}, HP:{self.hp}/{self.max_hp}{scores}. Notes: {self.notes}"


class dnd(commands.Cog):

    def __init__(self, bot: MultiBot):
        self.bot = self.client = bot
        # initialize drouu's generator
        self.gen = gens.Generator()

    @commands.command(
        name="roll",
        help="Rolls a dice, using the Avrae roll engine.",
        usage="<prefix>roll <Avrae dice roll format, e.g. '1d8[sword]+2d6[necrotic]'>",
        aliases=["r"]
    )
    @utils.validate_cog("dnd")
    async def roll(self, ctx: commands.Context, *args):
        # await ctx.send('*Large monstrosity, neutral good*\n**Armor Class** 12\n**Hit Points** 45 (6d10 + 12)\n**Speed** 50 ft.\n```+-----------+-----------+-----------+----------+-----------+-----------+\n| STR       | DEX       | CON       | INT      | WIS       | CHA       |\n+===========+===========+===========+==========+===========+===========+\n| 18 (+4)   | 14 (+2)   | 14 (+2)   | 9 (-1)   | 13 (+1)   | 11 (+0)   |\n+-----------+-----------+-----------+----------+-----------+-----------+```\n**Skills** Athletics +6, Perception +3, Survival +3\n**Senses** passive Perception 13\n**Languages** Elvish, Sylvan\n**Challenge** 2 (450 XP)\n**Charge**: If the centaur moves at least 30 feet straight toward a\ntarget and then hits it with a pike attack on the same turn, the target\ntakes an extra 10 (3d6) piercing damage.\n\nActions\n-------\n**Multiattack**: The centaur makes two attacks: one with its pike and\none with its hooves or two with its longbow.\n**Pike**: *Melee Weapon\nAttack*: +6 to hit, reach 10 ft., one target. *Hit*: 9 (1d10 + 4)\npiercing damage.\n**Hooves**: *Melee Weapon Attack*: +6 to hit, reach 5\nft., one target. *Hit*: 11 (2d6 + 4) bludgeoning damage.\n**Longbow**:\n*Ranged Weapon Attack*: +4 to hit, range 150/600 ft., one target. *Hit*:\n6 (1d8 + 2) piercing damage.')
        r = d20.roll(" ".join(args))
        await ctx.send(r.result)

    @commands.command(
        name="reroll",
        help="Rolls a dice, using the Avrae roll engine, the amount of times specified.",
        aliases=["rr"],
        usage="--reroll <amount:int> <roll:RollObject(same as in the roll command)>",
        brief="Rolls the roll object, the amount of times given, returns all the rolls and their sum."
    )
    @utils.validate_cog("dnd")
    async def reroll(self, ctx: commands.Context, amount, *args):
        if len(args) == 0:
            return self.roll(ctx, amount)
        if amount.isnumeric():
            res = ""
            t = 0
            for i in range(int(amount)):
                res += "__"+str(i+1) + ".__ " + \
                    (r := d20.roll(*args)).result + "\n"
                t += r.total
            res += "__Total:__ ``"+str(t) + "``"
            return await ctx.send(res)
        else:
            return await ctx.send("**Roll:** ***ERROR:*** Given amount isn't a number!")

    @commands.command(
        name="rolld6",
        help="Rolls a given amount of D6s, for Cogent RPG",
        aliases=["r6"],
        usage="<prefix>rolld6 <amount of D6s:Number> [<how much advantage:Number>]",
        brief="Rolls <amount>D6, if <advantage> is given, lowers the Win level by <advantage> (default Win Level is 4)"
    )
    async def rolld6(self, ctx: commands.Context, dice_count: str, adv: str = 0, *args):
        if dice_count.isnumeric():
            if isinstance(adv, str) and not adv.isnumeric():
                return await utils.error(ctx, "Roll D6", "Given Advantage Isn't a Number!", [("Advantage:", adv, True)])
            else:
                dice_count = int(dice_count)
                adv = int(adv)
                def roll(): return random.randint(1, 6)
                wins = sum(
                    [1 if roll() >= 4-adv else 0 for _ in range(dice_count)])
                return await ctx.send(embed=utils.create_embed(f"Roll {dice_count}D6{f' with {adv} advantage' if adv > 0 else ''}:", f"Rolled ***{wins}*** Wins."))
        else:
            return await utils.error(ctx, "Roll D6", "Given Dice Amount Isn't a Number!", [("Dice Count:", dice_count, True)])

    @commands.command(
        name="generate",
        help="Allows you to use any of the configured drouu generators.",
        usage="See <help generators> for specific generator usage.",
        aliases=["g"],
        brief=""
    )
    @utils.validate_cog("dnd")
    async def generate(self, ctx: commands.Context, gen_name: str, *gen_args):
        # return await ctx.send(utils.create_embed(f"***Under reconstruction***","",color=discord.Color.red()))
        if not gen_name in self.gen.gens.keys():  # if given generator isn't in the configured generators
            # error
            await utils.error(ctx, "**Input Error**", "Given generator name isn't in the configured generators!", [("given name:", gen_name)])
            return
        args = [i for i in gen_args if re.match(r"^n=\d+$", i) is None]
        ns = [i for i in gen_args if re.match(r"^n=\d+$", i) is not None]
        if len(ns) > 1:
            await utils.error(ctx, "**Input Error**", "Too many `n` arguments were given. Please only input one `n` argument.")
            return
        if len(ns) == 0:
            n = 1
        else:
            n = int(ns[0][2:])

        if n == 1:
            res = self.gen.generate(gen_name, args)
            # print(res)
            if type(res[0]) == int:
                await utils.error(ctx, "**Generator Error**", res[1])
                return

            if type(res[1]) == str:
                await ctx.send(embed=utils.create_embed("**Generated "+res[0]+"**", res[1]))
            else:
                await ctx.send(embed=utils.create_embed("**Generated "+res[0]+"**", fields=(res[1] if type(res[1][0]) in [list, tuple] else [res[1], ])))
        else:
            title = None
            results = []
            for _ in range(n):

                res = self.gen.generate(gen_name, args)
                # print(res)
                if type(res[0]) == int:
                    await utils.error(ctx, "**Generator Error**", res[1])
                    return

                if title is None:
                    title = "**Generated " + res[0] + "**"

                if type(res[1]) == str:
                    results.append((res[0], res[1], False))
                else:
                    # structure: list[list[str,str]]
                    r = (res[1] if type(res[1][0]) in [
                         list, tuple] else [res[1], ])
                    s = ""
                    for i in r:
                        s += " - " + i[0] + "\n\t" + i[1] + "\n"
                    results.append((res[0], s[:-1], False))
            print(results)
            es = utils.long_embed(title, "", results)
            for e in es:
                await ctx.send(embed=e)

    @commands.command(
        name="gen-help",
        help="Help command for the `generate` command.",
        usage="`<prefix>gen-help` for all available generators and quick info about each one, `<prefix>gen-help <generator-name>` for detail info about a specific generator.",
        aliases=["gh"],
        brief=""
    )
    @utils.validate_cog("dnd")
    async def gen_help(self, ctx: commands.Context, gen: str = None):

        # if gen is None:
        #     gens = []
        # else:
        #     if not gen in self.gen.gens.keys():
        #         return await utils.error(ctx,"**Invalid Generator Name**","Given generator isn't defined.", [("Generator:",gen)])

        if gen is None:  # if no generator specified
            gens = [(g, f"_Arguments:_ {', '.join(list(dt['keys'].keys()))}", False) if "keys" in dt and len(dt["keys"]) != 0 else (g, "No arguments.", False)
                    for g, dt in self.gen.gens.items()]  # get all generators
            em = utils.create_embed(
                "D&D Generators", "All of the configured generators and their required fields.", gens)  # add to embed
            await ctx.send(embed=em)  # embed
            return  # exit
        else:  # gen given
            if not gen in self.gen.gens.keys():  # if given generator isn't in the configured generators
                # error
                await utils.error(ctx, "**Input Error:**", "Given generator name isn't in the configured generators.", [("given name:", gen)])
                return
            # get gen's keys
            keys = self.gen.gens[gen]["keys"] if "keys" in self.gen.gens[gen] else None
            # try to get description
            desc = self.gen.gens[gen]["desc"] if "desc" in self.gen.gens[gen] else "_No description._"
            em = utils.create_embed(
                f"{gen} Generator", desc+("\nGenerator arguments:" if keys is not None else ""), [(k, f"""_Options:_ "{'", "'.join(map(str,v))}".""", False) for k, v in keys.items()] if keys is not None else [])  # embed
            await ctx.send(embed=em)  # send

    init_stack: list[init_tracker_item] = []
    init_round_index = 0

    async def add_initiative_item(self, ctx: commands.Context, *args):
        # print(args)
        if len(args) < 5:  # if not enough arguments
            # error
            await utils.error(ctx, "**Invalid Input**", "Initiative Add command rquires at least 5 arguments (name,initialize,AC,max hp,hp)")
            return False
        name, initiative, ac, max_hp, hp, *stats_notes = args  # expand arguments

        if name in self.init_stack:
            await utils.error(ctx, "**Duplicate Item**", "Entity name already in initiative stack", [("Name:", name)])
            return False

        __s = initiative
        if utils.isint(initiative):  # if initiative is a number
            initiative = int(initiative)  # parse
        else:
            await utils.error(ctx, "**Invalid Input**", "Argument Initiative must be either an integer or a modifier!")
            return False
        # if starts with a + or a - (a modifier)
        if __s.startswith("+") or __s.startswith("-"):
            initiative = utils.roll(20)+initiative  # add to a d20 roll

        initiative = max(initiative, 1)  # make sure above 1

        # any of the other args isnt a number
        if not all(map(utils.isint, [ac, max_hp, hp])):
            # error
            await utils.error(ctx, "**Invalid Input**", "Arguments AC, max HP and HP nust be integers!")
            return False

        ac = int(ac)
        max_hp = int(max_hp)
        hp = int(hp)

        stats = [None]*6
        for i, s in enumerate(stats_notes):
            # print(i, s, utils.isint(s))
            if i < 6 and utils.isint(s):
                stats[i] = int(s)
        notes = stats_notes[6:]
        if any([i is None for i in stats]):
            notes = stats_notes
        item = init_tracker_item(
            name, initiative, ac, max_hp, hp, stats[0], stats[1], stats[2], stats[3], stats[4], stats[5], " ".join([str(i) for i in notes]))  # create the init_tracker item
        self.init_stack.append(item)  # add it to the stack
        # sort and reverse (large to small instead of small to large)
        self.init_stack.sort(key=lambda x: x.initiative, reverse=True)
        # msg info
        await ctx.send(f"Added {name} to initiative stack.")

    async def remove_initiative_item(self, ctx: commands.Context, *args):
        if len(args) == 0:
            await utils.error(ctx, "**Invalid Input**", "Initiative Remove command requires an item's name!")
            return
        entity, *rest = args
        # if given entity name isn't in the init stack
        if not entity in [it.name for it in self.init_stack]:
            # error
            await utils.error(ctx, "Entity not in initiative order", f"Given entity `{entity}` is not in the initiative order.")
            return
        # get init stack without the specified entity
        self.init_stack = [it for it in self.init_stack if it.name != entity]
        # msg
        await ctx.send(f"Removed entity `{entity}` from initiative order.")

    async def get_round(self, ctx: commands.Context, *args):
        if len(self.init_stack) == 0:  # if no items
            # msg & exit
            return await ctx.send("No entities in initiative order,")
        if self.init_round_index >= len(self.init_stack):  # if index overflow
            self.init_round_index = 0  # reset to 0
        current = self.init_stack[self.init_round_index]  # get current
        next = None  # init next
        if len(self.init_stack) > 1:  # if there is more than 1
            next = self.init_stack[self.init_round_index +
                                   1 if self.init_round_index + 1 < len(self.init_stack) else 0]  # get next
        if next is None:  # if no next
            # one item
            await ctx.send(embed=utils.create_embed("Initiative Round", f"Only one entity in initiative:", [("_Current:_ "+(t := str(current).split("\n"))[0], t[1], False), ]))
        else:  # if there is next
            # current & next
            await ctx.send(embed=utils.create_embed("Initiative Round", "Current and next entities:", [("_Current:_ "+(t := str(current).split("\n"))[0], t[1], False), ("_Next:_ "+(t := str(next).split("\n"))[0], t[1], False)]))

    async def next_round(self, ctx: commands.Context, *args):
        self.init_round_index += 1
        await self.get_round(ctx, True)

    async def reset_round(self, ctx: commands.Context, *args):
        self.init_round_index = 0
        await ctx.send("Reset to 0.")

    async def heal_initiative_item(self, ctx: commands.Context, *args):
        if len(args) < 2:
            await utils.error(ctx, "**Invalid Input**", "Initiative Heal command rquires item name and HP amount!")
            return False
        entity, hp, *rest = args
        if not utils.isint(hp):
            await utils.error(ctx, "**Invalid Input**", "HP is not an integer!")
            return
        hp = int(hp)
        # if given entity name isn't in the init stack
        if not entity in [it.name for it in self.init_stack]:
            # error
            await utils.error(ctx, "Entity not in initiative order", f"Given entity `{entity}` is not in the initiative order.")
            return
        i = self.init_stack.index(entity)
        # entity = self.init_stack[i]
        h = self.init_stack[i].hp  # current hp
        sm = h+hp  # sum current hp + heal
        if sm > self.init_stack[i].max_hp:  # if above max
            d = sm - self.init_stack[i].max_hp  # find difference
            self.init_stack[i].hp = self.init_stack[i].max_hp  # heal to max
            hp -= d  # set heal amount to difference
        else:  # if not above max
            self.init_stack[i].hp = sm  # heal
        await ctx.send(f"Healed `{entity}` for {hp} HP. before: {h}, after: {self.init_stack[i].hp}{'(max)' if self.init_stack[i].hp == self.init_stack[i].max_hp else ''}.")

    async def damage_initiative_item(self, ctx: commands.Context, *args):
        if len(args) < 2:
            await utils.error(ctx, "**Invalid Input**", "Initiative Damage command rquires item name and HP amount!")
            return False
        entity, hp, *rest = args
        if not utils.isint(hp):
            await utils.error(ctx, "**Invalid Input**", "HP is not an integer!")
            return
        hp = int(hp)
        # if given entity name isn't in the init stack
        if not entity in [it.name for it in self.init_stack]:
            # error
            return await utils.error(ctx, "Entity not in initiative order", f"Given entity `{entity}` is not in the initiative order.")

        i = self.init_stack.index(entity)
        # entity = self.init_stack[i]
        h = self.init_stack[i].hp  # current hp

        sm = h-hp  # get sum

        if sm < 0:  # if unconscious
            if -sm >= self.init_stack[i].max_hp:  # if full dead
                return await ctx.send(f"Insta-Kill! reached negative max HP, entity is dead without death saves.")
            d = hp+sm  # get difference (how much hp wasn't used)
            hp -= d  # remove from hp
            self.init_stack[i].hp = 0  # set hp to 0
        else:  # if not unconscious
            self.init_stack[i].hp = sm  # set to sum
        await ctx.send(f"Damaged `{entity}` for {hp} HP. before: {h}, after: {self.init_stack[i].hp}{'(unconscious)' if self.init_stack[i].hp == 0 else ''}.")

    async def clear_initiative_stack(self, ctx, *args):
        self.init_stack = []
        await ctx.send("Cleared the initiative stack.")
        return False

    init_commands = {
        "add": {
            "call": add_initiative_item,
            "aliases": ["a"],
            "help": """`Adds an item to the initiative stack. Arguments: <item name:_str_> <initiative:either _int_ or modifer (e.g. +2,-3)> <AC:int> <max HP> <current HP> [<strength score:modifier> <dexterity score:modifier> <constitution score:modifier> <inteligence score:modifier> <wisdom score:modifier> <charisma score:modifier>] [<extre notes:str>]`"""
        },
        "remove": {
            "call": remove_initiative_item,
            "aliases": ["rem", "r"],
            "help": """`Removes an item from the initiative stack. Arguments: <item name:str>`"""
        },
        "list": {
            "call": lambda self, ctx, *args: ctx.send(embed=utils.create_embed("Initiative Stack", "The loaded entities in initiative order:" if len(self.init_stack) > 0 else "No entities in initiative stack.", [(f"#{i+1} "+(t := str(item).split("\n"))[0], t[1], False) for i, item in enumerate(self.init_stack)])),
            "aliases": ["ls", "l"],
            "help": """`Lists the entire initiative stack.`"""
        },
        "round": {
            "call": get_round,
            "aliases": ["rn"],
            "help": """`Shows the current and next rounds.`"""
        },
        "next-round": {
            "call": next_round,
            "aliases": ["nextr", "nround", "nrn"],
            "help": """`Moves to the next round and displays it.`"""
        },
        "reset-round": {
            "call": reset_round,
            "aliases": ["resetr"],
            "help": """`Resets the round to the first index.`"""
        },
        "heal": {
            "call": heal_initiative_item,
            "aliases": ["h"],
            "help": """`Heals an item in the initiative stack. Arguments: <item name:str> <HP amount:int | modifier>`"""
        },
        "damage": {
            "call": damage_initiative_item,
            "aliases": ["d", "dmg"],
            "help": """`Damages an item in the initiative stack. Arguments: <item name:str> <HP amount:int | modifier>`"""
        },
        "clear": {
            "call": clear_initiative_stack,
            "aliases": ["cls", "c"],
            "help": """`Clears the entire initiative stack.`"""
        }
    }

    @commands.command(
        name="initiative",
        help="An interactive initiative tracker.",
        aliases=["init", "i"],
        usage="Available commands:\n" +
        "\n".join([f"_{k}:_ {dt['help']}" for k, dt in init_commands.items()])
    )
    @utils.validate_cog("dnd")
    async def initiative(self, ctx: commands.Context, command: str, *args):
        # print(args)
        for k, dt in self.init_commands.items():  # loop through commands
            # if command or command alias
            if command == k or command in dt["aliases"]:
                r = await dt["call"](self, ctx, *args)  # call command
                # if not any of the list commands and didn't get a failure from the command
                if k not in ["list", "get-round", "next-round", "reset-round"] and r != False:
                    # call the list command too
                    await self.init_commands["list"]["call"](self, ctx, *args)
                return  # exit
        # error, not of any of the defined commands
        await utils.error(ctx, "**Invalid Input**", "Given Initiative command doesn't exist!", [("command:", command)])

    @commands.command(
        name="get-monster",
        help="Returns a monster's stat block (if available in the SRD).",
        aliases=["getm"],
        usage="<prefix>get-monster <monster's name>"
    )
    @utils.validate_cog("dnd")
    async def get_monster(self, ctx: commands.Context, *monster):
        m = monster = " ".join(monster)  # join monster input
        monster = "assets/beastiary/"+monster.lower().replace(" ", "-") + \
            ".md"  # get file path
        if isfile(monster):  # if file exists
            with open(monster, "r", encoding="utf-8") as file:  # read
                name, data = file.read().split(";;;")  # split by spacer
                name = name.replace('"', '')  # clear name
                if ctx is not None:  # if ctx is not None
                    # send
                    await ctx.send(embed=utils.create_embed(name, data, color=discord.Color.orange()))
                return name, data  # return data
        else:  # if file doesn't exist
            # error
            if ctx is not None:  # if ctx is not None
                await ctx.send(embed=utils.create_embed("**Invalid Input**", "Given monster name, %s, doesn't exist in the beastiary." % m, color=discord.Color.orange()))
            return None, None

    @commands.command(
        name="modes-of-travel",
        help="A quick info page of the different modes of travel.",
        aliases=["mot"]
    )
    @utils.validate_cog("dnd")
    async def modes_of_travel(self, ctx: commands.Context):

        # embed=utils.create_embed("Modes of Travel", "Cost and travel time for each mount.\n"+l))
        await ctx.send(file=discord.File("assets/travel-modes-info.jpg"))


cog = dnd
# match platform.system():
#     case "Linux" | "Darwin":
#         def setup(bot):
#             bot.add_cog(cog(bot))
#     case "Windows":


async def setup(bot):
    t = bot.add_cog(cog(bot))
    if t is not None:
        await t
