import asyncio
import discord
from discord.ext import commands

from random import shuffle, randrange
from discord.ext.commands.context import Context
from youtube_dl import YoutubeDL

from cogs.utils.utils import *


class music(commands.Cog):
    looping = False
    play_index = 0

    def advance_index(self):
        """ advances the index. returns True if looped playlist  """
        self.play_index += 1
        if self.play_index >= len(self.music_queue):
            self.play_index = 0
            return True
        return False

    @property
    def curr_song(self):
        return self.music_queue[self.play_index] if self.play_index < len(self.music_queue) else None

    def next_song(self):
        r = self.advance_index()
        if r and not self.looping:
            return None
        return self.curr_song

    last = None

    def printl(self, *args):
        if self.last is None:
            return False
        self.bot.loop.create_task(self.last.send(*args))

    def __init__(self, bot):
        self.bot = bot
        self.isTimed = False
        self.is_playing = False
        self.current_song = None
        self.music_queue = []
        self.skip_votes = set()

        self.YDL_OPTIONS = {"format": "bestaudio", "noplaylist": "True"}
        self.FFMPEG_OPTIONS = {
            "before_options": "-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5",
            "options": "-vn",
        }

        self.vc = ""

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):

        if not member.id == self.bot.user.id:
            return

        elif before.channel is None:
            voice = self.vc
            time = 0
            if type(voice) == str:
                return
            while True:
                await asyncio.sleep(1)
                time = time + 1
                if voice.is_playing:
                    time = 0
                if time == 20:
                    await voice.disconnect()
                if not voice.is_connected():
                    break

    def search_yt(self, item):
        with YoutubeDL(self.YDL_OPTIONS) as ydl:
            try:
                info = ydl.extract_info("ytsearch:%s" % item, download=False)[
                    "entries"
                ][0]
            except Exception:
                return False
        return {
            "source": info["formats"][0]["url"],
            "title": info["title"],
            "song_length": info["duration"],
        }

    def get_song_by_hash(self, hash):
        """ just like search_yt, but gets a hash instead of a query string """
        with YoutubeDL(self.YDL_OPTIONS) as ydl:
            try:
                info = ydl.extract_info(
                    'https://www.youtube.com/watch?v='+str(hash), download=False)
            except Exception:
                return False
        return {
            "source": info["formats"][0]["url"],
            "title": info["title"],
            "song_length": info["duration"],
        }

    def next(self, e):
        if e is None:
            r = self.play_next()
            if r != 0:
                self.printl(r)

    ret_stat = 0

    def play_next(self):
        if len(self.music_queue) > 0:
            self.is_playing = True

            s = self.next_song()

            if s is None:
                self.is_playing = False
                self.current_song = None
                return "**Play Next:** Looped Playlist! Stopping."

            m_url = s[0]["source"]

            self.current_song = s

            try:
                self.vc.play(
                    discord.FFmpegPCMAudio(m_url, **self.FFMPEG_OPTIONS),
                    after=lambda e: self.next(e),
                )
                self.printl(
                    f""":arrow_forward: Playing **{self.curr_song[0]['title']}** -- requested by {self.curr_song[2]}"""
                )

            except Exception as e:
                ...  # return "**Play Next:** ***ERROR:*** " + str(e)
            return 0
        else:
            self.is_playing = False
            self.current_song = None
            return "**Play Next:** No Song in playlist, stopping."

    @commands.command(
        name="play", help='starts playing the queue'
    )
    @validate_cog("music")
    async def play_music(self, ctx: commands.Context, play_index=None):
        self.last = ctx
        if play_index is not None:
            self.play_index = play_index
        if not self.is_playing and 0 < len(self.music_queue):
            self.is_playing = True

            m_url = self.curr_song[0]["source"]

            if self.vc == "" or not self.vc.is_connected() or self.vc == None:
                self.vc = await self.curr_song[1].connect()
            else:
                await self.vc.move_to(self.curr_song[1])

            await ctx.send(
                f""":arrow_forward: Playing **{self.curr_song[0]['title']}** -- requested by {self.curr_song[2]}"""
            )

            self.vc.play(
                discord.FFmpegPCMAudio(m_url, **self.FFMPEG_OPTIONS),
                after=lambda e: self.next(e),
            )

        else:
            self.is_playing = False
            self.current_song = None

    @commands.command(
        name="p",
        help="Plays a selected song from youtube \U0001F3B5"
    )
    @validate_cog("music")
    async def p(self, ctx: commands.Context, *args):
        self.last = ctx
        query = " ".join(args)

        voice_channel = ctx.author.voice.channel
        if voice_channel is None:
            await ctx.send("Connect to a voice channel!")
        else:
            song = self.search_yt(query)
            if type(song) == type(True):
                await ctx.send(
                    "Could not download the song. Incorrect format. Try another keyword."
                )
            else:
                await ctx.send(
                    f""":headphones: **{song["title"]}** has been added to the queue by {ctx.author.mention}"""
                )
                self.music_queue.append(
                    [song, voice_channel, ctx.author.mention])

                if self.is_playing == False:
                    await self.play_music(ctx)

    @commands.command(
        name="cp",
        help="Shows the currently playing song \U0001F440",
        aliases=["playing"],
    )
    @validate_cog("music")
    async def cp(self, ctx):
        self.last = ctx
        msg = (
            "No music playing"
            if self.current_song is None
            else f"""Currently Playing: **{self.current_song[0]['title']}** -- added by {self.current_song[2]}\n"""
        )
        await ctx.send(msg)

    @commands.command(
        name="q",
        help="Shows the music added in list/queue \U0001F440",
        aliases=["queue"],
    )
    @validate_cog("music")
    async def q(self, ctx):
        self.last = ctx
        # print(self.music_queue)
        retval = ""
        for (i, m) in enumerate(self.music_queue):
            # print(m)
            retval += f"""{i+1}. **{m[0]['title']}** -- added by {m[int(2)]}\n"""

        if retval != "":
            await ctx.send(retval)
        else:
            await ctx.send("No music in queue")

    @commands.command(name="cq", help="Clears the queue", aliases=["clear"])
    @validate_cog("music")
    async def cq(self, ctx):
        self.last = ctx
        self.music_queue = []
        await ctx.send("""***Queue cleared !***""")

    @commands.command(name="shuffle", help="Shuffles the queue")
    @validate_cog("music")
    async def shuffle(self, ctx):
        self.last = ctx
        shuffle(self.music_queue)
        await ctx.send("""***Queue shuffled !***""")

    @commands.command(
        name="s", help="Skips the current song being played", aliases=["skip"]
    )
    @validate_cog("music")
    async def skip(self, ctx):
        self.last = ctx
        if self.vc != "" and self.vc:
            await ctx.send("""***Skipped current song !***""")
            self.skip_votes = set()
            self.vc.stop()
            await self.play_music(ctx)

    @commands.command(
        name="voteskip",
        help="Vote to skip the current song being played",
        aliases=["vs"],
    )
    @validate_cog("music")
    async def voteskip(self, ctx):
        self.last = ctx
        if ctx.voice_client is None:
            return
        num_members = len(ctx.voice_client.channel.members) - 1
        self.skip_votes.add(ctx.author.id)
        votes = len(self.skip_votes)
        if votes >= num_members / 2:
            await ctx.send(f"Vote passed by majority ({votes}/{num_members}).")
            await self.skip(ctx)

    @commands.command(
        name="l",
        help="Commands the bot to leave the voice channel \U0001F634 [requires one of the DJ roles]",
        aliases=["leave"],
    )
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def leave(self, ctx: commands.Context, *args):
        self.last = ctx
        if self.vc.is_connected():
            await ctx.send("""**Bye Bye **:slight_smile:""")
            await self.vc.disconnect(force=True)

    @commands.command(
        name="pn", help="Moves the song to the top of the queue \U0001F4A5 [requires one of the DJ roles]"
    )
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def playnext(self, ctx: commands.Context, *args):
        self.last = ctx
        query = " ".join(args)

        voice_channel = ctx.author.voice.channel
        if voice_channel is None:
            await ctx.send("Connect to a voice channel")
        else:
            song = self.search_yt(query)
            if type(song) == type(True):
                await ctx.send(
                    "Could not download the song. Incorrect format try another keyword."
                )
            else:
                vote_message = await ctx.send(
                    f":headphones: **{song['title']}** will be added to the top of the queue by {ctx.author.mention}\n"
                    "You have 30 seconds to vote by reacting :+1: on this message.\n"
                    "If more than 50% of the people in your channel agree, the request will be up next!"
                )
                await vote_message.add_reaction("\U0001F44D")
                await asyncio.sleep(30)
                voters = len(voice_channel.members)
                voters = voters - 1 if self.vc else voters
                result_vote_msg = await ctx.fetch_message(vote_message.id)
                votes = (
                    next(
                        react
                        for react in result_vote_msg.reactions
                        if str(react.emoji) == "\U0001F44D"
                    ).count
                    - 1
                )
                if votes >= voters / 2:
                    self.music_queue.insert(
                        0, [song, voice_channel, ctx.author.mention]
                    )
                    await ctx.send(
                        f":headphones: **{song['title']}** will be added played next!"
                    )
                else:
                    self.music_queue.append(
                        [song, voice_channel, ctx.author.mention])
                    await ctx.send(
                        f":headphones: **{song['title']}** will be played add the end of the queue!"
                    )

                if self.is_playing == False or (
                    self.vc == "" or not self.vc.is_connected() or self.vc == None
                ):
                    await self.play_music(ctx)

    """Pause the currently playing song."""

    @commands.command(name="pause", help="Pause the currently playing song. [requires one of the DJ roles]")
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def pause(self, ctx):
        self.last = ctx
        vc = ctx.voice_client

        if not vc or not vc.is_playing():
            return await ctx.send("I am currently playing nothing!", delete_after=20)
        elif vc.is_paused():
            return

        vc.pause()
        await ctx.send(f":pause_button:  {ctx.author.mention} Paused the song!")

    """Resume the currently playing song."""

    @commands.command(name="resume", help="Resume the currently playing song. [requires one of the DJ roles]")
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def resume(self, ctx):
        self.last = ctx
        vc = ctx.voice_client

        if not vc or vc.is_playing():
            return await ctx.send("I am already playing a song!", delete_after=20)
        elif not vc.is_paused():
            return

        vc.resume()
        await ctx.send(f":play_pause:  {ctx.author.mention} Resumed the song!")

    @commands.command(
        name="remove",
        help="removes song from queue at index given. \U0001F4A9 [requires one of the DJ roles]",
        aliases=["rem"],
    )
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def remove(self, ctx: commands.Context, *args):
        self.last = ctx
        query = "".join(*args)
        index = 0
        negative = True if (query[0] == "-") else False
        if not negative:
            for i in range(len(query)):
                convert = (int)(query[i])
                index = index * 10 + convert
        index -= 1

        if negative:
            await ctx.send("Index cannot be less than one")
        elif index >= len(self.music_queue):
            await ctx.send("Wrong index. Indexed music not present in the queue")
        else:
            await ctx.send(
                f""":x: Music at index {query} removed by {ctx.author.mention}"""
            )
            self.music_queue.pop(index)

    @commands.command(
        name="rep",
        help="Restarts the current song. \U000027F2 [requires one of the DJ roles]",
        aliases=["restart"],
    )
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def restart(self, ctx):
        self.last = ctx
        song = []
        if self.current_song != None:
            song = self.current_song[0]
            voice_channel = ctx.author.voice.channel
            if not self.looping:
                self.music_queue.insert(
                    0, [song, voice_channel, ctx.author.mention])
            self.vc.stop()
            if len(self.music_queue) > 0:
                self.is_playing = True

                self.play_index = 0

                m_url = self.curr_song[0]["source"]

                if self.vc == "" or not self.vc.is_connected() or self.vc == None:
                    self.vc = await self.curr_song[1].connect()
                    await ctx.send("No music added")
                else:
                    await self.vc.move_to(self.curr_song[1])

                    await ctx.send(
                        f""":repeat: Replaying **{self.music_queue[0][0]['title']}** -- requested by {self.music_queue[0][2]}"""
                    )

                    self.vc.play(
                        discord.FFmpegPCMAudio(m_url, **self.FFMPEG_OPTIONS),
                        after=lambda e: self.play_next(),
                    )
                    self.current_song = self.curr_song

        else:
            self.is_playing = False
            self.current_song = None
            await ctx.send(f""":x: No music playing""")

    @commands.command(
        name="qt",
        help="Calculates and outputs the total length of the songs in the queue.",
        aliases=["queuetime"],
    )
    @validate_cog("music")
    async def qt(self, ctx):
        self.last = ctx
        remaining_time = 0
        for song in self.music_queue:
            remaining_time += song[0]["song_length"]

        remaining_time_minutes = str(remaining_time // 60)
        remaining_time = str(remaining_time % 60)
        remaining_time = f"{remaining_time_minutes}:{remaining_time}"

        await ctx.send(f"""The queue has a total of {remaining_time} remaining!""")

    @commands.command(
        name="sleep", help="Sets the bot to sleep. \U0001F4A4 [requires one of the DJ roles]", aliases=["timer"]
    )
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def sleep(self, ctx: commands.Context, *args):
        self.last = ctx
        second = int(0)
        query = list(args)
        if self.is_playing == False:
            return await ctx.send(f"No music playing")

        if len(query) == 0 and self.isTimed:
            self.isTimed = False
            return
        elif len(query) == 2 and not self.isTimed:
            try:
                if query[0] == "m":
                    second = int(query[1]) * 60
                elif query[0] == "h":
                    second = int(query[1]) * 3600
                elif query[0] == "s":
                    second = int(query[1])
                else:
                    await ctx.send("Invalid time format.")
                    return
            except:
                await ctx.send("Invalid time specified")
                return
        elif len(query) == 2 and self.isTimed:
            await ctx.send("Timer already set. Unset to reset.")
            return
        else:
            await ctx.send("Invalid time format.")
            return
        seconds = f"{second}"
        if second < 0:
            await ctx.send("Time cannot be negative")
        else:
            self.isTimed = True
            message = await ctx.send("Timer set for : " + seconds + " seconds.")
            while True and self.isTimed:
                second = second - 1
                if second == 0:
                    await message.edit(new_content=("Ended!"))
                    break
                await message.edit(new_content=("Timer: {0}".format(second)))
                await asyncio.sleep(1)

            if self.isTimed == False:
                await ctx.send("Timer disabled.")
            else:
                await ctx.send(
                    f""" **{ctx.message.author.mention} Sleep time exceeded! Bye-Bye!** :slight_smile: """
                )
                self.isTimed = False
                await self.vc.disconnect(force=True)

    @commands.command(
        name="loo", help="Returns if looping the queue or not, or toggles looping. [requires one of the DJ roles]", aliases=["loop", "looping"]
    )
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def loop(self, ctx: commands.Context, *args):
        self.last = ctx
        if len(args) == 0:
            return await ctx.send(f"{'Not ' if not self.looping else ''}Looping Playlist.")
        a = args[0]
        self.looping = bool(int(a)) if a.isnumeric(
        ) else True if a.lower() == 'true' else False
        return await ctx.send(f"Toggled, {'Not ' if not self.looping else ''}Looping Playlist.")

    @commands.command(
        name="pp", help="Adds a subject playlist to the queue [requires one of the DEV roles]", aliases=["playlist", 'addp']
    )
    @commands.has_any_role(*config.roles.dev)
    @validate_cog("music")
    async def add_playlist(self, ctx: Context, *args):
        self.last = ctx
        v = ctx.author.voice
        if v is None:
            await ctx.send("Connect to a voice channel!")
        else:
            voice_channel = v.channel
            if any([i in args for i in ['-p', '--playlist', '-s', '--subject']]):
                try:
                    i = args.index('-p')
                except ValueError:
                    try:
                        i = args.index('--playlist')
                    except ValueError:
                        try:
                            i = args.index('-s')
                        except ValueError:
                            try:
                                i = args.index('--subject')
                            except ValueError:
                                i = 0

                if i < len(args)-1:
                    subject = args[i+1]
                else:
                    subject = args[0]
                    await ctx.send("***WARNING:*** Given subject argument was placed at the end of the list! Defaulting to index 0.")
            else:
                subject = args[0]
            playlist, status = self.get_playlist_subject(subject)
            if not status:
                return await ctx.send("***ERROR:*** Given subject does not appear in the Subject Playlists List!")

            # print(playlist)

            if not ("--keep" in args or '-k' in args):
                self.music_queue = []

            await ctx.send("Retrieving playlist of subject " + subject + ", may take a while.")

            for i, hash in enumerate(playlist):
                self.music_queue.append(
                    [s := self.get_song_by_hash(hash), voice_channel, ctx.author.mention])
                await ctx.send(f"{i+1}/{len(playlist)}: {s['title']}")
                if i == 0:
                    vc = ctx.voice_client

                    if vc and vc.is_playing():
                        vc.pause()

                    await self.play_music(ctx)

            shuffle(self.music_queue)

            await ctx.send("Finished.")

    @commands.command(
        name="ra", help="Re-Adds the current song to the queue. [requires one of the DJ roles]", aliases=["readd"]
    )
    @commands.has_any_role(*config.roles.dj_roles)
    @validate_cog("music")
    async def readd(self, ctx):
        self.last = ctx
        if self.current_song is not None and self.current_song not in self.music_queue:
            self.music_queue.append(self.current_song)
            await ctx.send("Re-Added song to playlist.")
        else:
            await ctx.send("No song is playing!" if self.current_song is None else "Song already in playlist!")

    def get_playlist_subject(self, subject: str):
        with open("subject_playlists", "r") as f:
            d = [s.split("\n") for s in f.read().split("* ")]
            for i in range(len(d)):
                d[i][0] = d[i][0].split("|") if '|' in d[i][0] else d[i][0]
                while '' in d[i]:
                    d[i].remove('')

        if d[0][0].isnumeric():
            c = int(d[0][0])
            d = d[1:]
        else:
            c = 10
            self.printl(
                "**Get Playlist Sbuject:** ***ERROR:*** first item is not numeric! defaulting to 10")

        for s in d:
            if subject.lower() in s[0]:
                s = s[1:]
                if len(s) > c:
                    r = []
                    for i in range(c):
                        t = randrange(len(s))
                        while t in r:
                            t = randrange(len(s))
                        r.append(t)

                    for i in range(c):
                        r[i] = s[r[i]]
                    return (r, True)
                return (s, True)

        return ([], False)

    @commands.command(
        name="gs", help="Returns the available playlist subjects. [requires one of the DEV roles]", aliases=["gets", "getsubject", "getsub"]
    )
    @commands.has_any_role(*config.roles.dev)
    @validate_cog("music")
    async def gets(self, ctx):
        self.last = ctx
        l = self.get_playlist_subjects_list()
        r = ''
        for t in l:
            r += "\n"+" | ".join(t)+": " + \
                str(len((s := self.get_playlist_subject(t[0])[0])))
            # print(t,s)
        await ctx.send("Available subjects:"+r)

    def get_playlist_subjects_list(self):
        with open("subject_playlists", "r") as f:
            d = [s.split("\n")[0] for s in f.read().split("* ")[1:]]
            for i in range(len(d)):
                d[i] = d[i].split("|")
            return d
        return []


import platform
cog=music
# match platform.system():
#     case "Linux" | "Darwin":
#         def setup(bot):
#             bot.add_cog(cog(bot))
#     case "Windows":


async def setup(bot):
    t = bot.add_cog(cog(bot))
    if t is not None:
        await t