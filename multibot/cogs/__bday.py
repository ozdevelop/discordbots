import asyncio
import discord
import datetime
from discord import Member
from discord.ext import commands
from discord.ext.commands.context import Context
import db.db_adapter as database

from db.birthday import Birthday

from cogs.utils.utils import *
from cogs.utils.roles import *

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.cron import CronTrigger


class bday(commands.Cog):

    def start_loop(self, time="12"):

        # initializing scheduler
        scheduler = AsyncIOScheduler()

        # sends "Your Message" at 12PM and 18PM (Local Time)
        scheduler.add_job(self.birthday_loop, CronTrigger(
            hour=time, minute="0", second="0"))

        # starting the scheduler
        scheduler.start()

    bot: commands.Bot = None

    def __init__(self, bot: commands.Bot):
        self.bot = self.client = bot

        with open("config.json") as file:
            config = json.load(file)
        self.birthday_time = config['birthday_time']

        self.start_loop(self.birthday_time)

    def bday_channel(self, server: int, channel: int = None):
        if channel == None:
            return database.get_channel(server)
        database.set_channel(server, channel)

    @commands.command(
        name="bday",
        help="Sets your birthday. (date in numbers, ex: ``--bday 13 05 2002``)",
        usage="--bday <day> <month> [<year>]"
    )
    @validate_cog("bday")
    async def birthday_add(self, ctx: Context, day, month, year=-1):
        """<Day Mont [Year]> Adds your birthday."""
        member: discord.Member = ctx.message.author
        # Just in case don't add bots to the database
        if member.bot:
            return
        database.create_birthday(
            user_id=member.id, day=day, month=month, year=year)
        await self.bot.say('Birthday added')

    @commands.command(
        name="bday-update",
        help="Updates your birthday.",
        aliases=["ubday"],
        usage="--bday-update <day> <month> <year>"
    )
    @validate_cog("bday")
    async def birthday_update(self, ctx: Context, day, month, year=-1):
        """<Day Mont [Year]> Update your birthday."""
        member: discord.Member = ctx.message.author
        database.update_birthday(
            user_id=member.id, day=day, month=month, year=year)
        await self.bot.say('Birthday updated')

    @commands.command(
        name="bday-delete",
        help="Deletes your birthday.",
        usage="--bday-delete"
    )
    @validate_cog("bday")
    async def birthday_delete(self, ctx: Context):
        """Delete your birthday."""
        member: discord.Member = ctx.message.author
        birthday = Birthday(user_id=member.id)
        database.delete_birthday(birthday)
        await self.bot.say('Birthday deleted')

    @commands.command(
        name="bday-get",
        help="Shows your birthday.",
        aliases=["gbday"],
        usage="--bday-get [<member ping=You>]"
    )
    @validate_cog("bday")
    async def birthday(self, ctx: Context, member: discord.Member = None):
        """<[Member]> Show a member's birthday. Displays your birthday by default."""
        if member is None:
            member = ctx.message.author
        birthday: Birthday = database.get_birthday_one(user_id=member.id)
        if birthday is None:
            message = f'\n{member.display_name}: no registered.'
        else:
            message = f'\n{member.display_name}\'s birthday is on {birthday.printable_date()}.'
        await self.bot.say(message)

    @commands.command(
        name="bday-all",
        help="Get birthdays of all users in the server.",
        usage="--bday-all [<show unregistered users = False>]"
    )
    @commands.has_any_role(*dev_roles())
    @validate_cog("bday")
    async def birthday_all(self, ctx: Context, show=None):
        """[show not registered?] Show everyone's birthday"""
        message = 'Birthdays:'
        for member in ctx.message.guild.members:
            if member.bot:
                # Ignore bots
                continue
            birthday = database.get_birthday_one(user_id=member.id)
            if birthday is None:
                if show is not None:
                    message += f'\n{member.display_name}: no registered.'
            else:
                message += f'\n{member.display_name}\'s birthday is on {birthday.printable_date()}.'
        await self.bot.say(message)

    @commands.command(
        name="bday-today",
        help="Get all bdays today.",
        usage="--bday-today"
    )
    @commands.has_any_role(*dev_roles())
    @validate_cog("bday")
    async def birthday_today(self, ctx: Context):
        """Show today's birthday"""
        birthdays = birthdays_today_server(ctx.message.server)
        if len(birthdays) == 0:
            message = "No birthdays today."
        else:
            jump = ''
            message = '@here\n'
            for bd in birthdays:
                member = discord.utils.get(
                    ctx.message.server.members, id=bd.user_id)
                message = f'{jump} It\'s {member.mention}\' birthday!'
                jump = '\n'
        await self.bot.say(message)

    @commands.command(
        name="set-bday-channel",
        help="Set channel used for birthday messages.",
        usage="--set-bday-channel <channel>"
    )
    @commands.has_any_role(*dev_roles())
    @validate_cog("bday")
    async def set_bday_channel(self, ctx: Context, channel):
        self.bday_channel(ctx.guild.id, channel)
        print(channel, type(channel))

    @commands.command(
        name="get-bday-channel",
        help="Gets channel used for birthday messages.",
        usage="--get-bday-channel"
    )
    @commands.has_any_role(*dev_roles())
    @validate_cog("bday")
    async def get_bday_channel(self, ctx: Context):
        t = self.bday_channel(ctx.guild.id)
        await ctx.send(t if t is not None else "Server has no set birthday notice channel!")
        # print(channel)

    @commands.command(
        name="trigger-bday",
        help="Gets channel used for birthday messages.",
        usage="--trigger-bday"
    )
    @commands.has_any_role(*dev_roles())
    @validate_cog("bday")
    async def trigger_bday(self, ctx: Context):
        await self.birthday_loop(False)

    async def birthday_loop(self, set=True):

        emojis = {
            'face': '\U0001f973',  # face
            'pop': '\U0001f389',  # pop-stick-thingy
            'conf': '\U0001f38a',  # confetty
            'cake': '\U0001f382',  # cake
        }

        servers: list[Guild] = await self.bot.fetch_guilds(limit=None).flatten()
        for server in servers:

            ctx = self.bday_channel(server.id)
            if ctx is None:
                print("***ERROR:*** NO BDAY CHANNEL SET!")
                return

            bds_msg = "Today our dear friend/s "

            mentions = []
            birthdays = birthdays_today_server(server)
            for bd in birthdays:
                member: Member = discord.utils.get(
                    server.members, id=bd.user_id)
                mentions.append(member.mention)

            mentions = ", ".join(mentions)
            bds_msg += mentions + \
                f" celebrate their brithday{emojis['cake']}, so let us celebrate with them, and wish them Happy Birthday!{emojis['face']}{emojis['conf']}"

            bday_embed = create_embed(
                f"**{emojis['pop']}{emojis['face']}Birthdays Today{emojis['face']}{emojis['cake']}**", bds_msg, color=discord.Color.magenta())

            if len(birthdays) > 0 or True:
                await ctx.send(embed=bday_embed)

        if set:
            self.start_loop(self.birthday_time)


def birthdays_today_server(server) -> list[Birthday]:
    birthdays = []
    today = datetime.date.today()
    for member in server.members:
        if member.bot:
            # Ignore bots
            continue
        birthday: Birthday = database.get_birthday_one(user_id=member.id)
        if birthday is not None:
            if today.day == birthday.day and today.month == birthday.month:
                birthdays.append(birthday)
    return birthdays


def setup(bot: commands.Bot):
    bot.add_cog(bday(bot))
