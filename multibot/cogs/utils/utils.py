import d20
from os import system
import sys
from io import StringIO
import ast
from typing import Any
import discord
import json
from discord import Guild
from discord.ext import commands
import json
from types import SimpleNamespace
from enum import Enum

""" └┴┬├┤│─┼┐┘┌ ╣║║╗╝╚╩╦╠═╬"""

cogs_file = "cogs/utils/server-cogs.json"

# load config
with open("config.json", "r") as config_file:
    config: SimpleNamespace = json.loads(config_file.read(),
                                         object_hook=lambda d: SimpleNamespace(**d))
    # config = json.load(config_file) # load json
    prefix = config.PREFIX  # get prefix
__config = config


def capture_stdout(code: str = None, function: Any = None, global_dict: dict = None):
    """ executes a string of python code and captures it's stdout """
    if code is not None:  # if code given
        def function(): return eval(code)  # evaluate it
    elif code == function == None:  # no code and no function given
        # error
        raise ValueError(
            "capture_stdout requires at least 1 code/function variable! missing both!")
    elif not callable(function):  # if none of the above and given function is not callable
        # error
        raise ValueError(
            "capture_stdout requires a callable function or code!")

    if global_dict is not None:  # if the given global dictionary is not none
        # print(global_dict)
        for key, v in global_dict.items():  # add it to global
            globals()[key] = v

    old_stdout = sys.stdout  # save old stdout
    sys.stdout = mystdout = StringIO()  # set new stdout to pipe

    ret = function()  # get output
    if ret is not None:  # if output given
        ret = str(ret) + "\n"  # add newlinw
    else:  # no output
        ret = ''  # empty

    ret += mystdout.getvalue()  # add stdout output
    sys.stdout = old_stdout  # revert to old stdout

    return ret  # return


def change_server_cog_access(server: Guild, cog: str, bot: commands.Bot, access=True):
    """ modify a cog's status for a server """
    data, servers = get_servers_data()  # get data
    id = str(server.id)  # get id

    if access:  # if activate
        # if server is not defined or if defined and no cog is enabled
        if not id in servers or id in servers and servers[id] == 'None':
            # set enabled list to cog (no other cog is active)
            data['servers'][id] = [cog, ]
            write_server_data(data)  # write to config file
            return True  # return success

        # if cog is enabled or all mark is set
        if cog in servers[id] or servers[id] == 'All':
            return False  # return failure - already enabled

        data['servers'][id].append(cog)  # add cog to server's list

    else:  # if deactivate
        if servers[id] == 'None':  # if none mark is given
            return False  # return failure - already deactivated

        if not id in servers:  # if server not defined
            data['servers'][id] = 'None'  # set none mark

        # if cog enabled - and not all mark
        if type(servers[id]) == list and cog in servers[id]:
            servers[id].remove(cog)  # remove cog from server's list
            data['servers'][id] = servers[id]  # set data to server's list

        if servers[id] == 'All':  # if all mark is set
            # get all cogs but the disabled cog
            data['servers'][id] = [c for c in bot.cogs if c != cog]

    data['servers'][id] = [c for c in data['servers'][id]
                           if c in bot.cogs]  # remove any non-existant cog

    write_server_data(data)  # write server data

    return True  # return enabled


def class_encoder(cls):
    """ a generator for Json Encoding class for custom class encoding. """
    class _encoder(json.JSONEncoder):
        def default(self, object):
            if isinstance(object, cls):
                return object.__dict__
            else:
                return json.JSONEncoder.default(self, object)
    return _encoder


def clamp(x, l, u):
    """ shortcut for the clamp function """
    return max(l, min(u, x))


def cog_enabled(cog, server: Guild):
    """ checks if a cog is enabled in a server """
    if cog == "dev":  # dev is always enabled
        return True

    data, servers = get_servers_data()  # get data

    id = str(server.id)  # server id

    if id not in servers:  # if not defined
        data['servers'][id] = 'All'  # define as all
        write_server_data(data=data)  # write to serverdata file
        return True  # return enabled
    elif id in servers:  # server defined
        cogs = servers[id]  # get enabled cogs
        if type(cogs) is list:  # if its a list
            if cog in cogs:  # if given cog name in the cogs list
                return True  # return enabled
        elif type(cogs) is str:  # if its a string
            if cogs == 'All':  # if an 'all' mark
                return True  # enabled
            else:  # anything else - error
                return False  # disabled


def copy_dict(d):
    """ shortcut for copying a dictionary """
    return {k: v for k, v in d.items()}


def create_embed(name, description="", fields: list[tuple[str, str, bool]] = None, color: discord.Color = discord.Color.blue(), inline=True):
    """ a shortcut to creating a message embed
    @param name: title of the message
    @param description: description of the embed
    @param fields: possible fields to add to the embed, in form of list[tuple[field title, field content, inline]]
    @param color: color of the embed, default is Blue
    @param inline: default inline value if inline is not set in the fields"""
    embed = discord.Embed(title=name, description=description,
                          colour=color)  # create embed

    if fields is not None:  # if fields given
        for f in fields:  # run on fields
            # print(f,len(f))
            embed.add_field(name=f[0], value=f[1],
                            inline=f[2] if len(f) > 2 else inline)  # add fields

    return embed  # return embed


class embed_colors(Enum):
    default = discord.Color.orange()
    help = discord.Color.blue()
    error = discord.Color.red()

    weather = discord.Color.light_gray()


async def error(ctx, title: str, msg: str = "", fields: list[tuple[str, str, bool]] = []):
    """sends an embedded error message with given title, description, and optional fields
    @ fields: are of type list[tuple[label,message,inline]]"""
    return await ctx.send(embed=create_embed(title, msg, fields, color=embed_colors.error.value))


def get_active_cogs(bot: commands.Bot, server: Guild):
    """ get all the active cogs for a given server"""
    if server is None:  # if no server given
        return bot.cogs  # return all cogs
    id = str(server.id)  # get id

    data, servers = get_servers_data()  # get data
    # print(server.name)
    if id in servers:  # if id defined
        if servers[id] == 'All':  # if all mark
            return bot.cogs  # return all cogs
        elif servers[id] == 'None':  # none mark
            return ["dev"]  # return dev cog (always enabled)
        else:  # if any other value
            cogs = servers[id]  # get cogs
            if not "dev" in cogs:  # if dev not in list
                cogs.append("dev")  # add it
            return cogs  # return cogs list

    # if reached here, server wasn't defined. set as all mark
    data['servers'][id] = 'All'

    write_server_data(data=data)  # write to coonfig

    return bot.cogs  # return all cogs


def get_server_cogs(server: Guild) -> list[str] | str:
    """ returns a list enabled cogs for a given server """
    _, servers = get_servers_data()  # get data
    id = str(server.id)  # get id
    if id in servers:  # if defined
        return servers[id]  # return data
    else:  # not defined
        return "all"  # return all mark


def get_servers_data():
    """ fetches the data about the servers from the config file """
    with open(cogs_file, 'r') as file:
        data = json.load(file)
        servers = data['servers']
    return data, servers


def isfloat(s: str) -> bool:
    """ returns true if given string is a float """
    if type(s) not in [str, int, float]:  # if not allowed type
        return False  # not a float
    try:  # tryparse
        f = float(s)  # parse
    except ValueError:  # if failed
        return False  # not a float
    return True  # a float


def isint(s: str) -> bool:
    """ returns true if given string is an int """
    if type(s) not in [str, int, float]:  # if not allowed type
        return False  # not an int
    try:  # tryparse
        f = int(s)  # parse
    except ValueError:  # if failed
        return False  # not an int
    return True  # an int


def is_valid_python(code):
    """ returns true if given code string is valid python code """
    try:  # tryexec
        ast.parse(code)  # parses the code
    except SyntaxError as e:  # if syntax error
        return (False, e)  # return error
    return (True, None)  # return success


def json_dump_simplenamespace(sns: SimpleNamespace) -> str:
    """ shortcut to class_encoder of the SimpleNamespace class """
    return class_encoder(SimpleNamespace)().encode(sns)


class lengths:
    title = 256
    description = 4096
    field_title = 256
    field_value = 1024
    footer = 2048
    author = 256
    total = 6000


def long_embed(embed_title: str, description: str, fields: list[tuple[str]] = []):
    """ takes a long embed input, and splits it to smaller embeds and fields and descriptions and titles and field names """

    embeds = []  # embeds list
    index = 1  # title index

    def gentitle(i): return embed_title + \
        (f" #{i}" if overflow_descriptions != []
         else "")  # shortcut for generating an indexed title

    if len(embed_title) > lengths.title:  # if title is too large
        # split to length
        embed_title, *rest = split_to_length(embed_title, lengths.title-4)
        rest = " ".join(rest)  # join overflow
        description = rest + "\n" + description  # prepend to description

    overflow_descriptions = []  # overflow description - if any
    if len(description) > lengths.description:  # if description is too large
        description, * \
            overflow_descriptions = split_to_length(
                description, lengths.description)  # split description

    # add new embed with generated title and first description.
    if description != "":
        embeds.append(create_embed(gentitle(index), description, inline=False))
        index += 1  # advance index

    # shortcut for generating and indexed title
    def genfieldtitle(t, i): return t + f" #{i}"
    def fieldsize(f): return sum(map(len, f))  # shortcut for field size
    fs = []  # result fields
    for f in fields:  # run on fields
        ftitle = f[0]  # seperate values
        fvalue = f[1]

        if len(ftitle) > lengths.field_title:  # if title too long
            ftitle, *overlow_ftitle = split_to_length(ftitle,
                                                      lengths.field_title-4)  # split it
            # prepend overflow to field value
            fvalue = "_"+" ".join(overlow_ftitle)+"_, " + fvalue

        overflow_v = []  # overflow values if any
        if len(fvalue) > lengths.field_value:  # if value too long
            # split
            fvalue, * \
                overflow_fvalue = split_to_length(fvalue, lengths.field_value)
            field_index = 2  # index
            for value in overflow_fvalue:  # run on ovverflows
                # get field with indexed title
                f = [genfieldtitle(ftitle, field_index), value]
                # add to overflows with field size
                overflow_v.append((f, fieldsize(f)))
                field_index += 1  # advance index

            ftitle = genfieldtitle(ftitle, 1)  # update title

        f = [ftitle, fvalue]  # get first field
        fs.append((f, fieldsize(f)))  # add it
        for f in overflow_v:  # run on overflows
            fs.append(f)  # add overflow to fields list

    # for f in fs:
    #     print(f"Field name: {f[0][0]}, field size: {len(f[0][0])}")

    # enumerate on overflow descriptions - if any
    for i, desc in enumerate(overflow_descriptions):
        # if there are any fields, and it is the last iteration, and the total size of the title, the current description and the field, is smaller than the max message size (because each component is already clamped to it's max size)
        if fs != [] and i == len(overflow_descriptions)-1 and lengths.title + len(desc) + fs[0][1] <= lengths.total:
            s = lengths.title + len(desc)  # get current size sum
            i = 0  # sum index
            i_overflow = False  # i overflow (i == len(fs)) flag
            # while size is in check and index is in range of fields
            while s <= lengths.total and i < len(fs):
                s += fs[i][1]  # sum the next size
                i += 1  # advance index
                if i == len(fs):  # if i overflows
                    i_overflow = True  # set flag to true
                    break  # break
                # loop breaks when the sum is too large, and index is the index of the overflow sum+1 or the last index of the list
            # if ioverflow, then keep same, if value overflow, then i-1
            i = i if i_overflow else i-1
            # slice sub-fields from the fields list
            fields = [i[0] for i in fs[:i]]
            if type(fields) == tuple:  # if its a tuple (only 1 value)
                fields = [fields, ]  # place in a list
            # create and embed, with title, current description and the sub-fields list
            embeds.append(create_embed(
                gentitle(index), desc, fields=fields, inline=False))
            index += 1  # advance index
            del fs[:i]  # remove used fields
            break  # exit loop
        # add new embed with sub-description
        embeds.append(create_embed(gentitle(index), desc, inline=False))
        index += 1  # advance index

    while fs != []:
        s = lengths.title  # get current size sum
        i = 0  # sum index
        i_overflow = False  # i overflow (i == len(fs)) flag
        # while size is in check and index is in range of fields
        while s <= lengths.total and i < len(fs):
            s += fs[i][1]  # sum the next size
            i += 1  # advance index
            if i == len(fs):  # if i overflows
                i_overflow = True  # set flag to true
                break  # break
            # loop breaks when the sum is too large, and index is the index of the overflow sum+1 or the last index of the list
        # if ioverflow, then keep same, if value overflow, then i-1
        i = i if i_overflow else i-1
        fields = fs[:i]  # slice sub-fields from the fields list
        if type(fields) == tuple:  # if its a tuple (only 1 value)
            fields = [fields, ]  # place in a list
        # create and embed, with title, current description and the sub-fields list
        embeds.append(create_embed(
            gentitle(index), "", fields=[f[0] for f in fields], inline=False))
        index += 1  # advance index
        del fs[:i]  # remove used fields
    return embeds


def modify_config_dev_roles(roles: list[str]):
    global config  # get global config
    config.roles.dev = roles  # set new roles
    js = json_dump_simplenamespace(config)  # get json dump
    with open("config.json", "w+") as cfg:  # write to config
        cfg.write(js)


def rindex(c: int = 6):
    """ rolls an index for useage in generation tables """
    return roll(c)-1  # roll -1


def roll(n):
    """ shortcut to d20's roll command """
    return d20.roll(f"d{n}").total


def roll_dist(values, counts, roll=True):
    """ rolls a value table based on a distribution table. roll=True: wether to roll the table too or to just return the table """
    table = []  # res table
    for i, v in enumerate(values):  # enumerate the values
        # get the distribution values with default of 1
        c = counts[i] if len(counts) > i else 1
        table += [v, ]*c  # add to the res table
    return table[rindex(len(table))] if roll else table  # roll/return


def split_to_length(string: str, length: int, delimiter: str = ' ') -> list[str]:
    """ split a string by delimiter to given length substrings """
    if len(string) <= length:  # if string is already in size
        return [string, ]  # return string in list

    ss = string.split(delimiter)  # split the string
    if len(ss) == 1:  # if no split available
        raise ValueError(
            "Given delimiter doesn't appear in given string")  # raise error

    ret = []  # return array
    ts = ""  # temp string
    for i, s in enumerate(ss):  # enumerate substrings
        if len(s) > length:  # if subs is too large
            raise ValueError(
                "Delimiter split is not enough to get under the given length, substrings are still too large.")  # raise error
        if len(ts + s) > length:  # if length of temp string + subs is too large -> meaning temp string is the largest it can be with given length limit
            ret.append(ts)  # add temp string to return array
            ts = ""  # clear temps
        # add delimiter and subs to temps
        ts += delimiter + s if len(ts) != 0 else s

    if ts != "":  # if there's still something in ts
        ret.append(ts)  # add ts to return array

    return ret  # return ret array


def temp_conv(f):
    """ convert between fahrenheit and celsius"""
    return (f - 32) * 5.0/9.0


def validate_cog(class_name):
    """ predicate to validate a cog for a guild """
    if class_name == "dev":  # if develop cog
        async def permit(ctx):
            return True  # always enabled
        return commands.check(permit)  # check permit

    async def predicate(ctx):
        # check if cog is enabled under the given guild
        return cog_enabled(class_name, ctx.guild)
    return commands.check(predicate)  # check


def write_server_data(data: str = None, servers: str = None):
    """ writes a new dataset to the serverdata config file """
    if data == servers == None:  # if none
        return False
    _d = None
    if data is not None and servers is None:  # if data
        _d = data
    elif data is None and servers is not None:  # if servers
        _d, s = get_servers_data()
        _d['servers'] = copy_dict(servers)
    else:  # if both
        _d = data
        _d['servers'] = copy_dict(servers)
    with open(cogs_file, "w+") as f:  # write
        f.write(json.dumps(_d))
