import platform
import discord
from discord.ext import commands

import cogs.utils.utils as utils


def check_dev():
    def predicate(ctx):
        user = ctx.message.author
        if user.id == utils.config.author:
            return True
        rs = user.roles
        drs = utils.config.roles.dev
        return any([r in drs for r in rs])
    return commands.check(predicate)


def check_author():
    def predicate(ctx):
        return ctx.message.author.id == utils.config.author
    return commands.check(predicate)


class dev(commands.Cog):

    def __init__(self, bot):
        self.bot = self.client = bot

    @commands.command(
        name="run", help="Runs a section of python code."
    )
    # @commands.has_any_role(*dev_roles())
    @check_dev()
    async def run(self, ctx: commands.Context, *args):
        code = ' '.join(args)
        if not (r := utils.is_valid_python(code))[0]:
            return await utils.error(ctx,"***Dev Cog ERROR:***", f"Invalid code!", [("code:", str(r[1]))])

        try:
            d = globals()
            d['self'] = self
            d['ctx'] = ctx
            r = utils.capture_stdout(code=code, global_dict=d)
        except Exception as e:
            return await utils.error(ctx,"***Dev Cog ERROR:***", "Unknown Error.", [("Error:", f"{str(e)}")])
        finally:
            return await ctx.send(embed=utils.create_embed("Code Results:", f"``{r}``" if r != '' else "", color=discord.Color.green()))

    @commands.command(
        name="cogs",
        help="Returns all available cogs, split into a list of enabled cogs and a list of disabled cogs."
    )
    # @commands.has_any_role(*dev_roles())
    @check_dev()
    async def cogs(self, ctx):
        cogs = self.bot.cogs
        en_cogs = utils.get_active_cogs(self.bot, ctx.guild)
        if len(en_cogs) == 0:
            en_cogs = "None"
        else:
            en_cogs = ", ".join(en_cogs)
        dis_cogs = [cog for cog in cogs if cog not in en_cogs]
        if len(dis_cogs) == 0:
            dis_cogs = "None"
        else:
            dis_cogs = ", ".join(dis_cogs)

        await ctx.send(embed=utils.create_embed("MultiBot Cogs", "Cogs:", [
            ("Enabled Cogs:", f"``{en_cogs}``", False), ("Disabled Cogs:", f"``{dis_cogs}``", False)], discord.Color.green()
        ))

    @commands.command(
        name="enable-cog",
        help="Enables the given cog. (if it exists and it's not enabled already.)",
        aliases=["en-cog", "ecog"]
    )
    # @commands.has_any_role(*dev_roles())
    @check_dev()
    async def enable_cog(self, ctx: commands.Context, cog):
        if cog == "dev":
            return await ctx.send(f"**WARNING:** The Dev Cog is always Enabled!")

        cogs = utils.get_active_cogs(self.bot, ctx.guild)
        if not cog in self.bot.cogs:
            return await utils.error(ctx, f"**ERROR:** Cog {cog} doesn't exist.", f"Run ``{self.bot.command_prefix}cogs`` to see all the available cogs.")
        if cog in cogs:
            return await ctx.send(f"Cog {cog} Already Enabled!")

        utils.change_server_cog_access(ctx.guild, cog, self.bot, True)
        return await ctx.send(f"Cog {cog} now enabled.")

    @commands.command(
        name="disable-cog",
        help="Disables the given cog. (if it exists and it's not enabled already.)",
        aliases=["dis-cog", "dcog"]
    )
    # @commands.has_any_role(*dev_roles())
    @check_dev()
    async def disable_cog(self, ctx: commands.Context, cog):

        if cog == "dev":
            return await ctx.send(f"**WARNING:** The Dev Cog is always Enabled!")

        cogs = utils.get_active_cogs(self.bot, ctx.guild)
        t = utils.get_server_cogs(ctx.guild)
        ecogs = t if isinstance(
            t, list) else self.bot.cogs

        if not cog in ecogs:
            if not cog in self.bot.cogs:
                return await utils.error(ctx, f"**Cog {cog} doesn't exist.**", f"Run ``{self.bot.command_prefix}cogs`` to see all the available cogs.")
            if not cog in cogs:
                return await ctx.send(f"Cog {cog} already disabled!")

        utils.change_server_cog_access(ctx.guild, cog, self.bot, False)
        return await ctx.send(f"Cog {cog} now disabled.")

    @commands.command(
        name="dev-roles",
        help="Shows the configured 'Develop' roles - roles that allow access to the dev cog.",
        aliases=["devr"]
    )
    @check_dev()
    async def dev_roles(self, ctx):
        rs = utils.config.roles.dev
        await ctx.send(embed=utils.create_embed("Develop Roles", "Roles required for accessing any of the develop and test cogs.", [("Roles:", ", ".join(rs), False), ], discord.Color.green()))

    @commands.command(
        name="add-dev",
        help="Adds the given role name to the dev roles.",
        aliases=["adev"]
    )
    @check_dev()
    async def add_dev(self, ctx: commands.Context, role: str):
        if not role in [r.name for r in ctx.guild.roles]:
            await utils.error(ctx, "Role doesn't exist in this Guild!", f"Given role '{role}' isn't in the roles list of this server.")
            return
        rs = utils.config.roles.dev
        rs.append(role)
        utils.modify_config_dev_roles(rs)
        await ctx.send(f"Added `{role}` as a developer role.")

    @commands.command(
        name="rem-dev",
        help="Removes the given role name from the dev roles",
        aliases=["rmdev"]
    )
    @check_author()
    async def rem_dev(self, ctx: commands.Context, role: str):
        if not role in utils.config.roles.dev:
            await utils.error(ctx, "Not a developer role!", f"Given role {role} isn't a configured developer role.")
        rs = [r for r in utils.config.roles.dev if r != role]
        utils.modify_config_dev_roles(rs)
        await ctx.send(f"Removed role {role} from the developer roles.")

    @commands.command(
        name="fetch-msgs"
    )
    @check_author()
    async def fetch_msgs(self, ctx: commands.Context):
        messages: list[discord.Message] = [m async for m in ctx.channel.history(limit=100)]
        file = "out.txt"
        messages.reverse()
        with open(file, "w+") as f:
            f.write("".join(
                [f"[{msg.created_at.strftime('%d/%m/%Y %H:%M:%S')}; {msg.author.name}]: {msg.content}\n" for msg in messages]))


cog = dev
# match platform.system():
#     case "Linux" | "Darwin":
#         def setup(bot):
#             bot.add_cog(cog(bot))
#     case "Windows":


async def setup(bot):
    t = bot.add_cog(cog(bot))
    if t is not None:
        await t
