import json
import requests


def check_loaded(f):
    def wrapper(*args):
        l = args[0]._loaded
        return f(*args) if l else None
    return wrapper


class Generator:
    # format: {genA:{link:"link",keys:{keyA:[possible values],keyb..}},genb...}
    gens: dict = {}

    __instance = None
    _loaded = False

    def load_cfg(self, config: str = "gens.json"):
        with open(config, 'r') as cfg:  # load config file
            self.gens = json.load(cfg)  # parse
            self._loaded = True

    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = object.__new__(cls)
            cls.__instance.__init__()
        return cls.__instance

    @check_loaded
    def _get_generators(self):
        return list(self.gens.keys())  # return the generators

    @check_loaded
    def _parse_generator(self, gen: str, keys: list[str]) -> str | None:
        if not gen in self.gens:  # if given generator is not in the configured generators
            raise IOError(
                f"Generator \"{gen}\" not in configured generators")  # error

        l = self.gens[gen]['link']+'?'  # set base link

        # if there is no "type" in keys
        if not "type" in self.gens[gen]["keys"]:
            l += "type="+(gen.replace(" ", "+")
                          if not "type" in self.gens[gen].keys() else self.gens[gen]["type"].replace(" ", "+"))+"&"  # add gen name as "type" if type not defined
        # enumerate on keys
        for i, (kn, vs) in enumerate(self.gens[gen]['keys'].items()):
            # get key at i and default for value[0]
            k = keys[i] if len(keys) > i else vs[0]
            if type(k) is not str:
                k = str(k)
            if not k in vs:  # if key not in values
                raise IOError(
                    f"key \"{k}\" not in values for keyname \"{kn}\"")  # error
            l += kn+"="+k.replace(' ', "+")+"&"  # add key to link
        return l  # return link

    @check_loaded
    def generate(self, gen: str, keys: list[str], n: int):
        # print("Generator: ", gen, keys)
        base_link = self._parse_generator(gen, keys)  # parse inputs
        l = base_link + ("" if base_link.endswith("&")
                         else "&") + "n="+str(n)  # add gen count
        res = requests.get(l)  # get request
        if res.status_code == 200:  # if success
            # parse output (replac null with empty values)
            return eval(res.content.decode().replace("null", "''"))
        else:  # any other value
            raise ConnectionError(
                "Recieved a bad code from the server: "+str(res.status_code))  # connection error
