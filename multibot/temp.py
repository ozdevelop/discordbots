# import re
# from os import listdir,system


# def func(file):
#     if not "---" in file:
#         return file
#     # print(1)
#     file = file.split("---")[1:]
#     # print(2)
#     text = file[0]
#     # print(3)
#     file = file[1:][0]
#     # print(text,text.split("\n"))
#     # d = {(k := i.split(": "))[0]: k[1].replace('"', "")
#     #      for i in text.split("\n") if i != ""}
#     d = {}
#     p = ""
#     for i in text.split("\n"):
#         if i == "":
#             continue
#         if not ":" in i and p != "":
#             d[p] += " "+ i
#             continue
#         k,*v = i.split(": ")
#         v = ": ".join(v)
#         p=k
#         d[k]=v
#     # print(4)

#     def parse(text): return f"""{d['name']};;;_{' '.join(d['size'].split(' ')[:-1])} {f if (f:=(t:=d['alignment'].split())[0][0].upper()) != 'T' else ''}{t[1][0].upper()} {d['challenge']}_
# | {d['str']} | {d['dex']} | {d['con']} | {d['int']} | {d['wis']} | {d['cha']} |
# HP {d["hit_points"]}, AC {d['armor_class']}, {d["speed"]}
# _Languages:_ {d["languages"]}
# _Skills:_ {d["skills"]}"""
#     # print(5)
#     # text = parse(text)
#     # print(6)
#     text=d
#     del text["layout"]
#     del text["tags"]
#     d=text
#     if 'str' in d:
#         stats = f"""| {d['str']} | {d['dex']} | {d['con']} | {d['int']} | {d['wis']} | {d['cha']} |"""
#         del text["cha"]
#         del text["str"]
#         del text["dex"]
#         del text["con"]
#         del text["wis"]
#         del text["int"]
#         text["stats"]=stats
#     name = text["name"]
#     del text["name"]
#     file = name+";;;"+"\n".join([f"_{k}_: {v}" for k,v in text.items()]) + file.replace("<i>","_").replace("</i>","_")
#     # print(7)
#     file = re.sub(r"### (\S+)", r"**\1**", file).replace("***", "___")
#     return file

# path = "multibot/assets/beastiary/"
# files = listdir(path)
# for file in files:
#     with open(path + file, "r",encoding="utf-8") as f:
#         r = f.read()
#     r = r.replace("hit_points","hit points").replace("armor_class","armor class")
#     with open(path + file, "w+",encoding="utf-8") as f:
#         f.write(r)
#     print(file)
