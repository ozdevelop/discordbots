"Nycaloth";;;_size_: Large fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Abyssal, Infernal, telepathy 60 ft."
_senses_: "blindsight 60 ft., darkvision 60 ft."
_skills_: "Intimidation +6, Perception +4, Stealth +4"
_damage_immunities_: "acid, poison"
_speed_: "40 ft., fly 60 ft."
_hit points_: "123 (13d10+52)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 20 (+5) | 11 (0) | 19 (+4) | 12 (+1) | 10 (0) | 15 (+2) |

___Innate Spellcasting.___ The nycaloth's innate spellcasting ability is Charisma. The nycaloth can innately cast the following spells, requiring no material components:

* At will: _darkness, detect magic, dispel magic, invisibility _(self only)_, mirror image_

___Magic Resistance.___ The nycaloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The nycaloth's weapon attacks are magical.

**Actions**

___Multiattack.___ The nycaloth makes two melee attacks, or it makes one melee attack and teleports before or after the attack.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage. If the target is a creature, it must succeed on a DC 16 Constitution saving throw or take 5 (2d4) slashing damage at the start of each of its turns due to a fiendish wound. Each time the nycaloth hits the wounded target with this attack, the damage dealt by the wound increases by 5 (2d4). Any creature can take an action to stanch the wound with a successful DC 13 Wisdom (Medicine) check. The wound also closes if the target receives magical healing.

___Greataxe.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 18 (2d12 + 5) slashing damage.

___Teleport.___ The nycaloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.

___Variant: Summon Yugoloth (1/Day).___ The yugoloth chooses what to summon and attempts a magical summoning.

A nycaloth has a 50 percent chance of summoning 1d4 mezzoloths or one nycaloth.

A summoned yugoloth appears in an unoccupied space within 60 feet of its summoner, does as it pleases, and can't summon other yugoloths. The summoned yugoloth remains for 1 minute, until it or its summoner dies, or until its summoner takes a bonus action to dismiss it.
