"Aurora Dancer";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "the languages it knew in life"
_skills_: "Perception +6, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_damage_immunities_: "cold, necrotic, poison"
_damage_resistances_: "acid, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_speed_: "35 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "19 (mithral half plate, shield)"
_stats_: | 14 (+2) | 18 (+4) | 17 (+3) | 5 (-3) | 10 (+0) | 9 (-1) |

___Blizzard Stalker.___ The dancer's vision and hearing are
unimpeded by inclement weather such as wind, snow,
and fog, and it has advantage on all Dexterity (Stealth)
checks it makes in such conditions.

___Reactive.___ The dancer can take an take one reaction on
every turn in a combat.

**Actions**

___Multiattack.___ The dancer makes three attacks with its
scimitar.

___Frozen Scimitar.___ Melee Weapon Attack: +7 to hit, reach
5 ft., one target. Hit: 7 (1d6 + 4) slashing damage plus 4 (1d8) cold damage.

___Cutting Wind.___ The dancer swings its blade in a wide
horizontal arc, slicing out at each creature in a 15-foot
square within 5 feet of the dancer. Each creature in the
area must succeed on a DC 15 Dexterity saving throw
or take 7 (2d6) slashing damage plus 10 (3d6) cold
damage.

**Reactions**

___Blade Dance.___ When a creature the dancer can see moves
while within 10 feet of it, the dancer can move up to
15 feet in a straight line to an unoccupied space it can
see. Opportunity attacks triggered by this movement
due to the dancer leaving another creature's reach are
made with disadvantage.
