"Ape";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_skills_: "Athletics +5, Perception +3"
_speed_: "30 ft., climb 30 ft."
_hit points_: "19 (3d8+6)"
_armor class_: "12"
_stats_: | 16 (+3) | 14 (+2) | 14 (+2) | 6 (-2) | 12 (+1) | 7 (-2) |

**Actions**

___Multiattack.___ The ape makes two fist attacks.

___Fist.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +5 to hit, range 25/50 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage.