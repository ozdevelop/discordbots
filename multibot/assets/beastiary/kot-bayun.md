"Kot Bayun";;;_size_: Medium monstrosity
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Common, Sylvan"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +5"
_speed_: "40 ft., climb 20 ft."
_hit points_: "44 (8d8 + 8)"
_armor class_: "15"
_stats_: | 16 (+3) | 16 (+3) | 13 (+1) | 12 (+1) | 16 (+3) | 17 (+3) |

___Folk Cure.___ A kot bayun's tales have the effect of a _lesser restoration_ spell at will, and once per week it can have the effect of _greater restoration_. The kot bayun designates one listener to benefit from its ability, and that listener must spend one uninterrupted hour listening to the cat's tales. Kot bayuns are reluctant to share this benefit and must be bargained with or under the effect of a spell such as _domination_ to grant the boon.

___Innate Spellcasting.___ The kot bayun's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no material components:

* 3/day each: _fog cloud, invisibility _(self only)

* 1/day: _blink_

**Actions**

___Multiattack.___ The kot bayun makes one bite attack and one claws attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5ft., one target. Hit: 14 (2d10 + 3) slashing damage.

___Slumbering Song.___ The kot bayun puts creatures to sleep with its haunting melody. While a kot bayun sings, it can target one hearing creature within a 100-foot radius. This target must succeed on a DC 13 Charisma saving throw or fall asleep. The target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. Each round the kot bayun maintains its song, it can select a new target. A creature that successfully saves is immune to the effect of that kot bayun's song for 24 hours. The slumbering song even affects elves, but they have advantage on the Charisma saving throw.

