"Rictavio";;;_page_number_: 238
_size_: Medium humanoid (human)
_alignment_: lawful good
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal, Common, Elvish, Infernal"
_skills_: "Arcana +9, Insight +7, Medicine +7, Perception +7, Religion +6, Sleight of Hand +4"
_saving_throws_: "Con +4, Wis +7"
_speed_: "30 ft."
_hit points_: "77 (14d8+14)"
_armor class_: "12 (leather armor)"
_stats_: | 9 (-1) | 12 (+1) | 13 (+1) | 16 (+3) | 18 (+4) | 16 (+3) |

___Special Equipment.___ In addition to his sword cane, Rictavio wears a hat of disguise and a ring of mind shielding, and he carries a spell scroll of raise dead.

___Spellcasting.___ Rictavio is a 9th-level spellcaster. His spellcasting ability is Wisdom (spell save DC 15, +7 to hit with spell attacks). Rictavio has the following cleric spells prepared:

* Cantrips (at will): _guidance, light, mending, thaumaturgy_

* 1st level (4 slots): _cure wounds, detect evil and good, protection from evil and good, sanctuary_

* 2nd level (3 slots): _augury, lesser restoration, protection from poison_

* 3rd level (3 slots): _magic circle, remove curse, speak with dead_

* 4th level (3 slots): _death ward, freedom of movement_

* 5th level (1 slot): _dispel evil and good_

___Undead Slayer.___ When Rictavio hits an undead with a weapon attack, the undead takes an extra 10 (3d6) damage of the weapon's type.

**Actions**

___Multiattack.___ Rictavio makes two attacks with his sword cane.

___Sword Cane.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d6+1) bludgeoning damage (wooden cane) or piercing damage (silvered sword).
