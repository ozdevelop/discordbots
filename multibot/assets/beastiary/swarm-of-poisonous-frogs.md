"Swarm of Poisonous Frogs";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Perception +1, Stealth +3"
_senses_: "passive Perception 11"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "20 ft., swim 20 ft."
_hit points_: "76 (17d8 - 8)"
_armor class_: "13"
_stats_: | 1 (-5) | 13 (+1) | 8 (-1) | 1 (-5) | 8 (-1) | 3 (-4) |

___Amphibious.___ The swarm can breathe air and water.

___Keen Smell.___ The swarm has advantage on Wisdom (Perception) checks
that rely on smell.

___Standing Leap.___ The swarm’s long jump is up to 10 feet and its high
jump is up to 5 feet, with or without a running start.

___Swarm.___ The swarm can occupy another creature’s space and vice versa,
and the swarm can move through any opening large enough for a poisonous
frog. The swarm can’t regain hit points or gain temporary hit points.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 0 ft., one target. Hit: 11
(3d6 + 1) piercing damage, and the target must succeed on a DC 12
Constitution saving throw or be poisoned for 1 hour.
