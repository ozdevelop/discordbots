"Gargoyle";;;_size_: Medium elemental
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "30 ft., fly 60 ft."
_hit points_: "52 (7d8+21)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "exhaustion, petrified, poisoned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_stats_: | 15 (+2) | 11 (0) | 16 (+3) | 6 (-2) | 11 (0) | 7 (-2) |

___False Appearance.___ While the gargoyle remains motion less, it is indistinguishable from an inanimate statue.

**Actions**

___Multiattack.___ The gargoyle makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage.