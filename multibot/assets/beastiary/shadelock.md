"Shadelock";;;_size_: Medium aberration
_alignment_: chaotic neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Common"
_skills_: "Arcana +7, Insight +9, Perception +9, Stealth +10"
_senses_: "darkvision 60 ft., passive Perception 19"
_saving_throws_: "Dex +7, Cha +7"
_damage_vulnerabilities_: "radiant"
_damage_immunities_: "psychic; bludgeoning, piercing and slashing damage from nonmagical weapons"
_damage_resistances_: "cold, necrotic"
_speed_: "30 ft., fly 30 ft. (hover)"
_hit points_: "108 (24d8)"
_armor class_: "16 (natural armor)"
_stats_: | 8 (-1) | 18 (+4) | 10 (+0) | 12 (+1) | 16 (+3) | 18 (+4) |

___Aura of Gloom.___ When not subjected to full daylight or its magical
equivalent, shadelockes have advantage on all Dexterity (Stealth) checks.

___Innate Spellcasting.___ The shadelocke’s spellcasting ability is Charisma
(spell save DC 15, +7 to hit with spell attacks). The shadelocke can
innately cast the following spells, requiring no material components:

* At will: _chill touch, detect thoughts, mage hand, minor illusion_

* 3/day each: _blink, darkness, major image_

* 1/day each: _dominate person, fly, true seeing_

**Actions**

___Multiattack.___ The shadelocke makes two shadow touch attacks.

___Shadow Touch.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
creature. Hit: 22 (4d8+4) cold damage.
