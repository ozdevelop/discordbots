"Barbegazi (Ice Gnome)";;;_size_: Small humanoid (gnome)
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Gnome"
_skills_: "Stealth +3"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "cold"
_speed_: "20 ft., burrow 20 ft."
_hit points_: "13 (3d6 + 3)"
_armor class_: "12 (natural armor)"
_stats_: | 10 (+0) | 13 (+1) | 13 (+1) | 11 (+0) | 11 (+0) | 8 (-1) |

___Snow Walk.___ The barbegazi can move across and climb icy or snowy
surfaces without needing to make an ability check. Additionally, difficult
terrain composed of ice or snow doesn’t cost extra movement.

___Innate Spellcasting.___ The barbegazi’s innate spellcasting ability is
Wisdom (spell save DC 10, +2 to hit with spell attacks). The barbegazi
can innately cast the following spells, requiring no material components:

* 3/day: _ray of frost_

* 1/day: _hold person_

**Actions**

___Shortsword.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit:
4 (1d6 + 1) piercing damage plus 3 (1d6) cold damage.

___Dagger.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3
(1d4 + 1) piercing damage plus 3 (1d6) cold damage.
