"Carrion Beetle";;;_size_: Large beast
_alignment_: neutral
_challenge_: "4 (1100 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_condition_immunities_: "paralysis"
_speed_: "30 ft., burrow 20 ft., climb 10 ft."
_hit points_: "127 (15d10 + 45)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 17 (+3) | 1 (-5) | 13 (+1) | 10 (+0) |

**Actions**

___Multiattack.___ The beetle makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft, one target. Hit: 10 (1d12 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

___Acid Spit (Recharge 5-6).___ The carrion beetle spits a line of acid that is 30 ft. long and 5 ft. wide. Each creature in that line takes 32 (5d12) acid damage, or half damage with a successful DC 13 Dexterity saving throw.

