"White Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +2"
_damage_immunities_: "cold"
_saving_throws_: "Dex +2, Con +4, Wis +2, Cha +2"
_speed_: "30 ft., burrow 15 ft., fly 60 ft., swim 30 ft."
_hit points_: "32 (5d8+10)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 10 (0) | 14 (+2) | 5 (-3) | 10 (0) | 11 (0) |

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) piercing damage plus 2 (1d4) cold damage.

___Cold Breath (Recharge 5-6).___ The dragon exhales an icy blast of hail in a 15-foot cone. Each creature in that area must make a DC 12 Constitution saving throw, taking 22 (5d8) cold damage on a failed save, or half as much damage on a successful one.