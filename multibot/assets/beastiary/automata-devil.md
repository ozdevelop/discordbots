"Automata Devil";;;_size_: Large fiend
_alignment_: lawful evil
_challenge_: "10 (5900 XP)"
_languages_: "Common, Infernal; telepathy 100 ft."
_skills_: "Athletics +11, Intimidation +8"
_senses_: ", passive Perception 12"
_saving_throws_: "Str +11, Dex +7, Con +9, Wis +6, Cha +8"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "168 (16d10 + 80)"
_armor class_: "17 (natural armor)"
_stats_: | 24 (+7) | 17 (+3) | 20 (+5) | 11 (+0) | 14 (+2) | 19 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The automata devil has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The automata devils' spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

At will: charm person, suggestion, teleport

1/day each: banishing smite, cloudkill

**Actions**

___Multiattack.___ The automata devil makes two melee attacks, using any combination of bite, claw, and whip attacks. The bite attack can be used only once per turn.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 18 (2d10 + 7) slashing damage.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) slashing damage.

___Whip.___ Melee Weapon Attack: +11 to hit, reach 15 ft., one target. Hit: 11 (1d8 + 7) slashing damage and the target is grappled (escape DC 17) and restrained. Only two targets can be grappled by the automata devil at one time, and each grappled target prevents one whip from being used to attack. An individual target can be grappled by only one whip at a time. A grappled target takes 9 (2d8) piercing damage at the start of its turn.

___Punishing Maw.___ If a target is already grappled in a whip at the start of the automata devil's turn, both creatures make opposed Strength (Athletics) checks. If the grappled creature wins, it takes 9 (2d8) piercing damage and remains grappled. If the devil wins, the grappled creature is dragged into the devil's stomach maw, a mass of churning gears, razor teeth, and whirling blades. The creature takes 49 (4d20 + 7) slashing damage and is grappled, and the whip is free to attack again on the devil's next turn. The creature takes another 49 (4d20 +7) slashing damage automatically at the start of each of the automata devil's turns for as long as it remains grappled in the maw. Only one creature can be grappled in the punishing maw at a time. The automata devil can freely "spit out" a creature or corpse during its turn, to free up the maw for another victim.

___Fear Aura.___ Automata devils radiate fear in a 10-foot radius. A creature that starts its turn in the affected area must make a successful DC 16 Wisdom saving throw or become frightened. A creature that makes the save successfully cannot be affected by the same automata devil's fear aura again.

