"Swashbuckler";;;_size_: Medium humanoid (any race)
_alignment_: any non-lawful alignment
_challenge_: "3 (700 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Acrobatics +8, Athletics +5, Persuasion +6"
_speed_: "30 ft."
_hit points_: "66 (12d8+12)"
_armor class_: "17 (leather armor)"
_stats_: | 12 (+1) | 18 (+4) | 12 (+1) | 14 (+2) | 11 (0) | 15 (+2) |

___Lightfooted.___ The swashbuckler can take the Dash or Disengage action as a bonus action on each of its turns.

___Suave Defense.___ While the swashbuckler is wearing light or no armor and wielding no shield, its AC includes its Charisma modifier.

**Actions**

___Multiattack.___ The swashbuckler makes three attacks: one with a dagger and two with its rapier.

___Dagger.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d4+4) piercing damage.

___Rapier.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8+4) piercing damage.