"Skulk";;;_page_number_: 227
_size_: Medium humanoid
_alignment_: chaotic neutral
_challenge_: "1/2 (100 XP)"
_languages_: "understands Common but can't speak"
_senses_: "darkvision 120 ft., passive Perception 8"
_skills_: "Stealth +8"
_damage_immunities_: "radiant"
_saving_throws_: "Con +2"
_speed_: "30 ft."
_hit points_: "18  (4d8)"
_armor class_: "14"
_condition_immunities_: "blinded"
_stats_: | 6 (-2) | 19 (+4) | 10 (0) | 10 (0) | 7 (-1) | 1 (-4) |

___Fallible Invisibility.___ The skulk is invisible. This invisibility can be circumvented by three things:

* The skulk appears as a drab, smooth-skinned humanoid if its reflection can be seen in a mirror or on another surface.
* The skulk appears as a dim, translucent form in the light of a candle made of fat rendered from a corpse whose identity is unknown.
* Humanoid children, aged 10 and under, can see through this invisibility.

___Trackless.___ The skulk leaves no tracks to indicate where it has been or where it's headed.

**Actions**

___Claws___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d4 + 4) slashing damage. If the skulk has advantage on the attack roll, the target also takes 7 (2d6) necrotic damage.
