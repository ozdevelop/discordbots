"Gelid Beetle";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "cold"
_speed_: "30 ft."
_hit points_: "32 (5d8 + 10)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 10 (+0) | 14 (+2) | 1 (-5) | 10 (+0) | 9 (-1) |

___Frigid Body.___ A creature that touches the beetle or hits it with
a melee attack while within 5 feet of it takes 3 (1d6) cold
damage.

___Frigid Weapons.___ The beetle’s weapon attacks deal an extra
3 (1d6) cold damage (included in the attack).

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage plus 3 (1d6) cold damage.
