"Bagiennik";;;_size_: Medium aberration
_alignment_: chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "Common"
_skills_: "Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "30 ft., swim 40 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 16 (+3) | 18 (+4) | 16 (+3) | 9 (-1) | 16 (+3) | 11 (+0) |

**Actions**

___Multiattack.___ The bagiennik makes two claw attacks.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 18 (4d6 + 4) slashing damage.

___Acid Spray.___ Ranged Weapon Attack: +6 to hit, range 15 ft., one target. Hit: 14 (2d10 + 3) acid damage. The target must make a successful DC 13 Dexterity saving throw or fall prone in the slick oil, which covers an area 5 feet square. A creature that enters the oily area or ends its turn there must also make the Dexterity saving throw to avoid falling prone. A creature needs to make only one saving throw per 5-foot-square per turn, even if it enters and ends its turn in the area. The slippery effect lasts for 3 rounds.

___Healing Oil.___ A bagiennik can automatically stabilize a dying creature by smearing some of its oily secretion on the dying creature's flesh. A similar application on an already-stable creature or one with 1 or more hit points acts as a potion of healing, restoring 2d4 + 2 hit points. Alternatively, the bagiennik's secretion can have the effect of a _lesser restoration_ spell. However, any creature receiving a bagiennik's Healing Oil must make a successful DC 13 Constitution saving throw or be slowed for 1 minute.
