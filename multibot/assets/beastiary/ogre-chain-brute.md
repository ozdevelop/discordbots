"Ogre Chain Brute";;;_page_number_: 221
_size_: Large giant
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft., passive Perception 8"
_speed_: "40 ft."
_hit points_: "59  (7d10 + 21)"
_armor class_: "11 (hide armor)"
_stats_: | 19 (+4) | 8 (-1) | 16 (+3) | 5 (-2) | 7 (-1) | 7 (-1) |

**Actions**

___Fist___ Melee Weapon Attack: +6 to hit, reach ft., one target. Hit: 9 (2d4 + 4) bludgeoning damage.

___Chain Sweep___ The ogre swings its chain, and every creature within 10 feet of it must make a DC 14 Dexterity saving throw. On a failed saving throw, a creature takes 8 (1d8 + 4) bludgeoning damage and is knocked prone. On a successful save, the creature takes half as much damage and isn't knocked prone.

___Chain Smash (Recharge 6)___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage, and the target must make a DC 14 Constitution saving throw. On a failure the target is unconscious for 1 minute. The unconscious target repeats the saving throw if it takes damage and at the end of each of its turns, ending the effect on itself on a success.