"Frost Giant Zombie";;;_size_: Huge undead
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "understands Giant but can't speak"
_saving_throws_: "Wis +2"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_immunities_: "cold, poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "138 (12d12 + 60)"
_armor class_: "15 (patchwork armor)"
_stats_: | 23 (+6) | 6 (-2) | 21 (+5) | 3 (-4) | 6 (-2) | 5 (-3) |

___Numbing Aura.___ Any creature that starts its turn within 10 feet of the zombie must make a DC 17 Constitution saving throw. Unless the save succeeds, the creature can’t make more than one attack, or take a bonus action on that turn.

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is fire, radiant, or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Multiattack.___ The zombie makes two weapon attacks.

___Greataxe.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 25 (3d12 + 6) slashing damage.

___Hurl Rock.___ Ranged Weapon Attack: +10 to hit, range 60/240 ft., one target. Hit: 28 (4d10 + 6) bludgeoning damage.

___Freezing Stare.___ The zombie targets one creature it can see within 60 feet of it. The target must succeed on a DC 17 Constitution saving throw or take 35 (10d6) cold damage and be paralyzed until the end of its next turn.
