"Rasa Mastermind";;;_size_: Medium humanoid
_alignment_: any evil alignment
_challenge_: "4 (1,100 XP)"
_languages_: "Common"
_skills_: "Deception +5, Perception +5, Stealth +8"
_senses_: "passive Perception 15"
_speed_: "30 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "16 (studded leather)"
_stats_: | 12 (+1) | 18 (+4) | 15 (+2) | 16 (+3) | 13 (+1) | 14 (+2) |

___Innate Spellcasting.___ The rasa's innate spellcasting
ability is Intelligence (spell save DC 13). It can
innately cast the following spells, requiring no
components:

* At will: _mage hand, message_

* 1/day each: _arcane eye, greater invisibility, silent image_

* 2/day each: _disguise self, misty step_

___Inscrutable Intentions.___ The rasa is immune to any
magical effects to determine if it is lying.

___Sneak Attack (1/Turn).___ The mastermind deals an extra
10 (3d6) damage when it hits a target with a weapon
attack and has advantage on the attack roll, or when the
target is within 5 feet of an ally of the mastermind that
isn't incapacitated and the mastermind doesn't have
disadvantage on the attack roll.

___Mask of Deceit.___ While wearing this mask, the
mastermind has advantage on Charisma (Deception)
and Charisma (Performance) checks when trying to
pass itself off as a different person. It also allows the
wearer to mimic the speech of a creature it has heard
speak for at least 1 minute. A successful Wisdom
(Insight) check contested by the mastermind's
Charisma (Deception) check allows a listener to
determine that the voice is faked.

___Mask of Slaying.___ While wearing this mask, when the
mastermind takes the multiattack action, it makes two
additional melee attacks.

___Sight of the Rasa.___ The rasa share a special sight that
is invisible to all non-rasa creatures. As an action, a
rasa can touch a creature and place a secret mark
on that creature's chest. This mark lets other rasa
know whether or not this creature should be
regarded as a friend or a foe.

**Actions**

___Multiattack.___ The mastermind makes two melee attacks,
or makes a melee attack and uses its mask of
dominance.

___Poisoned Dagger.___ Melee or Ranged Weapon Attack: +6
to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6
(1d4 + 4) piercing damage plus 3 (1d6) poison
damage.

___Phantom Strikes (Recharge 5-6).___ The mastermind
summons three ethereal clones in unoccupied spaces
within 60 feet. These clones immediately make two
dagger attacks against a creature within 5 feet. These
attacks are made with a +6 bonus to hit and deal 10
(3d6) force damage. Once these attacks are
completed, the mastermind can teleport to the
location of any of these clones and the clones
disappear.

___Mask of Dominance (1/Day).___ The mastermind places a
mask of dominance on the face of a creature it is
currently grappling. That creature must succeed on a
DC 14 Wisdom saving throw or become charmed by
the mastermind. While charmed, the creature obeys all
commands of the mastermind that do not inflict self-harm.
This effect ends after 24 hours or when another
creature uses its action to pry the mask from the
charmed creature's face.
