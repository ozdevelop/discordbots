"Blue Bulette";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "12 (8,400 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +5, Con +11, Wis +4"
_damage_immunities_: "lightning"
_speed_: "40 ft."
_hit points_: "150 (12d10 + 84)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 12 (+1) | 24 (+7) | 3 (-4) | 10 (+0) | 4 (-3) |

___Electromagnetic Field.___ The electrified nature of the blue
bulette is such that compasses will spin wildly within
half of a mile of the bulette, and any technology-based
items will cease to function within 150 feet of it.

___Motherlode.___ If killed, the body of a blue bulette explodes in a loud
roar of thunder (deafening all within 150 feet for 1 minute). Afterward, all
that remains of the beast is a 100-pound pile of meteoric iron, which if
broken, will also contain its single white horn and 1–6 small lodestones.

___Planar Connection.___ Because their arcane natures tie blue bulettes to the
Demi-plane of Lightning, they also simultaneously exist there as nearly
invisible silhouettes intermittently illuminated by flashes of electric light.

**Actions**

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 33
(5d10 + 6) piercing damage plus 26 (4d12) lightning damage. If the target
is a creature, it must succeed on a DC 15 Constitution saving throw or be
knocked unconscious for 1 minute.

___Electric Stomp.___ The bulette slams its forefeet into the ground, imparting
a ground-conductive electric charge in an 80-foot radius. All creatures,
excluding constructs and undead, must succeed on a DC 17 Constitution
saving throw, or take 26 (4d12) lightning damage and be stunned for 1
minute. A stunned creature can repeat the saving throw at the end of each
of its turns, ending the effect on a success.

___Bolt and Run.___ The blue bulette transforms into a sphere of ball lightning
and magically teleports up to 120 feet to an unoccupied space it can see.
