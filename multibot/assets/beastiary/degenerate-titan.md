"Degenerate Titan";;;_size_: Huge giant
_alignment_: chaotic evil
_challenge_: "8 (3900 XP)"
_languages_: "Titan"
_skills_: "Intimidation +1, Perception +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "40 ft."
_hit points_: "161 (14d12 + 70)"
_armor class_: "12 (crude armored coat)"
_stats_: | 24 (+7) | 6 (-2) | 20 (+5) | 6 (-2) | 9 (-1) | 7 (-2) |

___Magic Resistance.___ The degenerate titan has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The degenerate titan makes two greatclub attacks.

___Greatclub.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 20 (3d8 + 7) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +10 to hit, range 60/240 ft., one target. Hit: 29 (4d10 + 7) bludgeoning damage.

___Earthstrike (Recharge 4-6).___ The degenerate titan slams his fists onto the ground, creating a shockwave in a line 60 feet long and 10 feet wide. Each creature in the line takes 35 (10d6) force damage and is flung up 20 feet away from the titan and knocked prone; a successful DC 18 Dexterity saving throw halves the damage and prevents the creature from being flung or knocked prone. A creature that's flung against an unyielding object such as a wall or floor takes 3 (1d6) bludgeoning damage for every 10 feet it was thrown. If it collides with another creature, that creature must succeed on a DC 18 Dexterity saving throw or take the same damage (1d6 bludgeoning per 10 feet) and be knocked prone.

___Shout of the Void (Recharge 4-6).___ The degenerate titan utters a scream that rends reality in a 30-foot cone. Any ongoing spell or magical effect of 3rd level or lower in the area ends. For every spell or effect of 4th level or higher in the area, the degenerate titan makes a Constitution check against DC (10 + the level of the spell or effect). On a success, the spell or effect ends.

