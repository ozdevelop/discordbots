"Pidlwick II";;;_page_number_: 235
_size_: Small construct
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "understands Common but doesn't speak and can't read or write"
_skills_: "Performance +3"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "10 (3d6)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "paralyzed, petrified, poisoned"
_stats_: | 10 (0) | 14 (+2) | 11 (0) | 8 (-1) | 13 (+1) | 10 (0) |

___Ambusher.___ During the first round of combat, Pidlwick II has advantage on attack rolls against any creature that hasn't had a turn yet.

**Actions**

___Club.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d4) bludgeoning damage.

___Dart.___ Ranged Weapon Attack: +4 to hit, range 20/60 ft., one target. Hit: 4 (1d4+2) piercing damage.