"Shadow Hunter Hatchling";;;_size_: Medium monstrosity
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +3"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 14"
_condition_immunities_: "prone"
_speed_: "30 ft., climb 40 ft., swim 20 ft."
_hit points_: "55 (10d8 + 10)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 13 (+1) | 12 (+1) | 5 (-3) | 10 (+0) | 3 (-4) |

___Keen Smell.___ The shadow hunter has advantage on Wisdom (Perception)
checks that rely on smell.

___Shadow Stealth.___ While in dim light or darkness, the shadow hunter can take
the Hide action as a bonus action.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6
(1d6 + 3) piercing damage and 10 (3d6) poison damage. The target must
succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour.
