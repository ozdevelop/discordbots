"Feyward Tree";;;_size_: Huge construct
_alignment_: unaligned
_challenge_: "8 (3900 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Con +7, Wis +3, Cha +1"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft."
_hit points_: "94 (9d12 + 36)"
_armor class_: "17 (natural armor)"
_stats_: | 26 (+8) | 10 (+0) | 18 (+4) | 2 (-4) | 11 (+0) | 6 (-2) |

___Magic Resistance.___ A feyward tree has advantage on saving throws against spells and other magical effects.

___Contractibility and Conductivity.___ Certain spells and effects function differently against feyward trees:

A magical attack that deals cold damage slows a feyward tree (as the slow spell) for 3 rounds.

A magical attack that deals lightning damage breaks any slow effect on the feyward tree and heals 1 hit point for each 3 damage the attack would otherwise deal. If the amount of healing would cause the tree to exceed its full normal hp, it gains any excess as temporary hp. The tree gets no saving throw against lightning effects.

___Immutable Form.___ The feyward tree is immune to any spell or effect that would alter its form.

___Magic Weapons.___ The feyward tree's weapon attacks are magical.

___Warden's Reach.___ Creatures within 15 feet of a feyward tree provoke opportunity attacks even if they take the Disengage action before leaving its reach.

**Actions**

___Multiattack.___ The tree makes two razor-leafed branch attacks, and may use a bonus action to make a razor-leafed branch attack against any creature standing next to it.

___Razor-Leafed Branch.___ Melee Weapon Attack: +11 to hit, reach 15 ft., one target. Hit: 21 (3d8 + 8) slashing damage.

___Flaying Leaves (Recharge 5-6).___ The tree can launch a barrage of razor-sharp cold iron leaves from its branches in a 20-footradius burst. All creatures caught within this area must make a successful DC 16 Dexterity saving throw or take 21 (6d6) slashing damage, or half as much damage on a successful one.

