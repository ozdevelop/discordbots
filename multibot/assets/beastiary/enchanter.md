"Enchanter";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "any four languages"
_skills_: "Arcana +6, History +6"
_saving_throws_: "Int +6, Wis +4"
_speed_: "30 ft."
_hit points_: "40 (9d8)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 11 (0) | 17 (+3) | 12 (+1) | 11 (0) |

___Spellcasting.___ The enchanter is a 9th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks). The enchanter has the following wizard spells prepared:

* Cantrips (at will): _friends, mage hand, mending, message_

* 1st level (4 slots): _charm person\*, mage armor, magic missile_

* 2nd level (3 slots): _hold person\*, invisibility, suggestion* _

* 3rd level (3 slots): _fireball, haste, tongues_

* 4th level (3 slots): _dominate beast\*, stoneskin_

* 5th level (2 slots): _hold monster* _

*Enchantment spell of 1st level or higher

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d6-1) bludgeoning damage, or 3 (1d8-1) bludgeoning damage if used with two hands.

**Reactions**

___Instinctive Charm (Recharges after the Enchanter Casts an Enchantment Spell of 1st level or Higher).___ The enchanter tries to magically divert an attack made against it, provided that the attacker is within 30 feet of it and visible to it. The enchanter must decide to do so before the attack hits or misses.

The attacker must make a DC 14 Wisdom saving throw. On a failed save, the attacker targets the creature closest to it, other than the enchanter or itself. If multiple creatures are closest, the attacker chooses which one to target.
