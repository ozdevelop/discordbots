"Ghost-Faced Battle Priest";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Orc"
_skills_: "Intimidation +2, Perception +8, Religion +2, Stealth +6 (+9 in dim light or darkness)"
_senses_: "darkvision 120 ft., passive Perception 18"
_saving_throws_: "Wis +8, Cha +2"
_speed_: "30 ft."
_hit points_: "88 (16d8 + 16)"
_armor class_: "15 (studded leather)"
_stats_: | 17 (+3) | 16 (+3) | 12 (+1) | 8 (-1) | 20 (+5) | 8 (-1) |

___Shadow Stealth.___ When in dim light or darkness, the ghost-faced orc can
Hide as a bonus action.

___Spellcasting.___ The battle priest is a 9th level spellcaster. Its spellcasting
ability is Wisdom (spell save DC 16, +8 to hit with spell attacks). It has
the following cleric spells prepared:

* Cantrips (at will): _chill touch, guidance, mending, resistance, thaumaturgy_

* 1st level (4 slots): _bane, command, cure wounds, inflict wounds, protection from evil and good_

* 2nd level (3 slots): _blindness/deafness, enhance ability, hold person, silence, spiritual weapon_

* 3rd level (3 slots): _bestow curse, dispel magic, protection from energy, spirit guardians_

* 4th level (3 slots): _banishment, death ward_

* 5th level (1 slot): _insect plague_

**Actions**

___Multiattack.___ The battle priest makes two melee attacks.

___Mace.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12
(2d8 + 3) bludgeoning damage.

**Reactions**

___Summon Demon (1/day).___ When the battle priest drops a hostile target
to 0 hit points, the battle priest can attempt to summon a chaaor demon. It
has a 45% chance of succeeding on this attempt. The summoned demon
appears in an unoccupied space within 60 feet of the battle priest and can’t
summon other demons. It remains for 1 minute, until it or the battle priest
is slain, or until the battle priest takes an action to dismiss it.
