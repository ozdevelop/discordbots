"Hurricane";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Auran, Common"
_skills_: "Acrobatics +5"
_speed_: "45 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "14"
_stats_: | 12 (+1) | 16 (+3) | 13 (+1) | 10 (0) | 12 (+1) | 10 (0) |

___Spellcasting.___ The hurricane is a 3rd-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 11, +3 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _blade ward, gust, light, prestidigitation_

* 1st level (4 slots): _feather fall, jump, thunderwave_

* 2nd level (2 slots): _gust of wind_

___Unarmored Defense.___ While the hurricane is wearing no armor and wielding no shield, its AC includes its Wisdom modifier.

___Unarmored Movement.___ While the hurricane is wearing no armor and wielding no shield, its walking speed increases by 15 feet (included in its speed).

**Actions**

___Multiattack.___ The hurricane makes two melee attacks.

___Unarmed Strike.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage.

**Reactions**

___Deflect Missiles.___ When the hurricane is hit by a ranged weapon attack, it reduces the damage from the attack by 1d10 + 9. If the damage is reduced to 0, the hurricane can catch the missile if it is small enough to hold in one hand and the hurricane has at least one hand free.
