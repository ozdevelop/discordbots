"Outlaw Commander";;;_size_: Medium humanoid
_alignment_: any non-lawful alignment
_challenge_: "4 (1,100 XP)"
_languages_: "Common and any one other language"
_skills_: "Athletics +6, Intimidation +6, Performance +4, Persuasion +6"
_senses_: "passive Perception 10"
_speed_: "30 ft."
_hit points_: "98 (14d8 + 42)"
_armor class_: "17 (splint mail)"
_stats_: | 19 (+4) | 12 (+1) | 16 (+3) | 14 (+2) | 10 (+0) | 15 (+2) |

___Commander's Presence.___ Whenever one of the
commander's allies is within 60 feet must make a
saving throw, that creature gains a +1 bonus to the
saving throw if the commander isn't incapacitated.

**Actions**

___Multiattack.___ The commander makes two melee
attacks or two ranged attacks.

___Greataxe.___ Melee Weapon Attack: +6 to hit, reach 5
ft., one target. Hit: 10 (1d12 + 4) slashing damage.

___Handaxe.___ Melee or Ranged Weapon Attack: +6 to
hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7
(1d6 + 4) slashing damage.

___Expose Weakness (Recharge 4-6).___ Melee Weapon
Attack: +6 to hit, reach 5 ft., one target. Hit: 17
(2d12 + 4) slashing damage and the target has its
AC reduced by 2 until the beginning of the
commander's next turn.
