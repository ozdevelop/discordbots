"Prehistoric Beaver";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Survival +2"
_senses_: "passive Perception 10"
_speed_: "30 ft., swim 40 ft."
_hit points_: "52 (7d8 + 21)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 17 (+3) | 3 (-4) | 11 (+0) | 5 (-3) |

___Hold Breath.___ The armor-plated beaver can hold its breath for up to 20
minutes.

___Siege Monster.___ The prehistoric beaver deals double damage to objects
and structures.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 27 (4d10
+ 5) piercing damage.
