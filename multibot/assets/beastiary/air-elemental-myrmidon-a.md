"Air Elemental Myrmidon (A)";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Auran, one language of its creator's choice"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "30 ft., fly 30 ft."
_hit points_: "117 (18d8+36)"
_armor class_: "18 (plate)"
_condition_immunities_: "paralyzed, petrified, poisoned, prone"
_damage_resistances_: "lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 14 (+2) | 14 (+2) | 9 (-1) | 10 (0) | 10 (0) |

___Magic Weapons.___ The myrmidon's weapon attacks are magical.

**Actions**

___Multiattack.___ The myrmidon makes three flail attacks.

___Flail.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage.

___Lightning Strike (Recharge 6).___ The myrmidon makes one flail attack. If the attack hits, it deals an extra 18 (4d8) lightning damage, and the target must succeed on a DC 14 Constitution saving throw or be stunned until the end of the myrmidon's next turn.