"Cloud Giant Smiling One";;;_size_: Huge giant (cloud giant)
_alignment_: chaotic neutral
_challenge_: "11 (7,200 XP)"
_languages_: "Common, Giant"
_skills_: "Deception +11, Insight +7, Perception +7, Sleight of Hand +9"
_saving_throws_: "Con +10, Int +6, Wis +7"
_speed_: "40 ft."
_hit points_: "262 (21d12+126)"
_armor class_: "15 (natural armor)"
_stats_: | 26 (+8) | 12 (+1) | 22 (+6) | 15 (+2) | 16 (+3) | 17 (+3) |

___Innate Spellcasting.___ The giant's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

* At will: _detect magic, fog cloud, light_

* 3/day each: _featherfall, fly, misty step, telekinesis_

* 1/day each: _control weather, gaseous form_

___Spellcasting.___ The giant is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). The giant has the following bard spells prepared:

* Cantrips (at will): _minor illusion, prestidigitation, vicious mockery_

* 1st level (4 slots): _cure wounds, disguise self, silent image, Tasha's hideous laughter_

* 2nd level (3 slots): _invisibility, suggestion_

* 3rd level (2 slots): _major image, tongues_

___Keen Smell.___ The giant has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Multiattack.___ The giant makes two attacks with its morningstar.

___Morningstar.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 21 (3d8+8) bludgeoning damage. The attack deals an extra 14 (4d6) damage if the giant has advantage on the attack roll.

___Rock.___ Ranged Weapon Attack: +12 to hit, range 60/240 ft., one target. Hit: 30 (4d10+8) bludgeoning damage. The attack deals an extra 14 (4d6) damage if the giant has advantage on the attack roll.

___Change Shape.___ The giant magically polymorphs into a beast or humanoid it has seen, or back into its true form. Any equipment the giant is wearing or carrying is absorbed by the new form. Its statistics, other than its size, are the same in each form. It reverts to its true form if it dies.
