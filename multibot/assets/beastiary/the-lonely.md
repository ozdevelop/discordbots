"The Lonely";;;_page_number_: 232
_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "112  (15d8 + 45)"
_armor class_: "16 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing while in dim light or darkness"
_stats_: | 16 (+3) | 12 (+1) | 17 (+3) | 6 (-2) | 11 (0) | 6 (-2) |

___Psychic Leech.___ At the start of each of the Lonely's turns, each creature within 5 feet of it must succeed on a DC 15 Wisdom saving throw or take 10 (3d6) psychic damage.

___Thrives on Company.___ The Lonely has advantage on attack rolls while it is within 30 feet of at least two other creatures. It otherwise has disadvantage on attack rolls.

**Actions**

___Multiattack___ The Lonely makes one harpoon arm attack and uses Sorrowful Embrace.

___Harpoon Arm___ Melee Weapon Attack: +7 to hit, reach 60 ft., one target. Hit: 21 (4d8 + 3) piercing damage, and the target is grappled (escape DC 15) if it is a Large or smaller creature.
The Lonely has two harpoon arms and can grapple up to two creatures at once.

___Sorrowful Embrace___ Each creature grappled by the Lonely must make a DC 15 Wisdom saving throw. A creature takes 18 (4d8) psychic damage on a failed save, or half as much damage on a successful one. In either case, the Lonely pulls each creature grappled by it up to 30 feet straight toward it.