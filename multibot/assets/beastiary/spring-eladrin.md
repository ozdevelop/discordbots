"Spring Eladrin";;;_page_number_: 196
_size_: Medium fey (elf)
_alignment_: chaotic neutral
_challenge_: "10 (5900 XP)"
_languages_: "Common, Elvish, Sylvan"
_senses_: "darkvision 60 ft., passive Perception 10"
_skills_: "Deception +8, Persuasion +8"
_speed_: "30 ft."
_hit points_: "127  (17d8 + 51)"
_armor class_: "19 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 14 (+2) | 16 (+3) | 16 (+3) | 18 (+4) | 11 (0) | 18 (+4) |

___Fey Step (Recharge 4-6).___ As a bonus action, the eladrin can teleport up to 30 feet to an unoccupied space it can see.

___Innate Spellcasting.___ The eladrin's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _charm person, Tasha's hideous laughter_

* 3/day each: _confusion, enthrall, suggestion_

* 1/day each: _hallucinatory terrain, Otto's irresistible dance_

___Joyful Presence.___ Any non-eladrin creature that starts its turn within 60 feet of the eladrin must make a DC 16 Wisdom saving throw. On a failed save, the creature is charmed for 1 minute. On a successful save, the creature becomes immune to any eladrin's Joyful Presence for 24 hours.
Whenever the eladrin deals damage to the charmed creature, it can repeat the saving throw, ending the effect on itself on a success.

___Magic Resistance.___ The eladrin has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The eladrin makes two weapon attacks. The eladrin can cast one spell in place of one of these attacks.

___Longsword___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage plus 4 (1d8) psychic damage, or 7 (1d10 + 2) slashing damage plus 4 (1d8) psychic damage if used with two hands.

___Longbow___ Ranged Weapon Attack: +7 to hit, range 150/600 ft., one target. Hit: 7 (1d8 + 3) piercing damage plus 4 (1d8) psychic damage.
