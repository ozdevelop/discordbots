"City Watch Captian";;;_size_: Medium Humanoid
_alignment_: lawful neutral
_challenge_: "4 (1100 XP)"
_languages_: "one language (usually Common)"
_skills_: "Perception +2"
_senses_: "passive Perception 12"
_speed_: "30 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "17 (scale mail)"
_stats_: | 13 (+1) | 16 (+3) | 14 (+2) | 10 (+0) | 11 (+0) | 13 (+1) |

___Tactical Insight.___ The city watch captain has advantage on initiative rolls. City watch soldiers under the captain's command take their turns on the same initiative count as the captain.

**Actions**

___Multiattack.___ The city watch captain makes two rapier attacks and one dagger attack. The captain can substitute a disarming attack for one rapier attack.

___Rapier.___ Melee Weapon Attack: +5 to hit, reach 5 ft, one target. Hit: 6 (1d6 + 3) piercing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Light Crossbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Disarming Attack.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: the target must make a successful DC 13 Strength saving throw or drop one item it's holding of the city watch captain's choice. The item lands up to 10 feet from the target, in a spot selected by the captain.

___Orders to Attack (1/Day).___ Each creature of the city watch captain's choice that is within 30 feet of it and can hear it makes one melee or ranged weapon attack as a reaction. This person could easily have been on the other side of the law, but he likes the way he looks in the city watch uniform.and the way city residents look at him when he walks down the street leading a patrol. With a long mustache and a jaunty cap, there's no denying that he cuts a rakishly handsome figure. While a trained investigator, the city watch captain is not afraid to draw his blade to end a threat to his city.

