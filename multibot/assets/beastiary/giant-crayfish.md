"Giant Crayfish";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "blindsight 30 ft."
_skills_: "Stealth +3"
_speed_: "30 ft., swim 30 ft."
_hit points_: "45 (7d10+7)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 13 (+1) | 13 (+1) | 1 (-5) | 9 (-1) | 3 (-4) |

___Source.___ tales from the yawning portal,  page 235

___Amphibious.___ The giant crayfish can breathe air and water.

**Actions**

___Multiattack.___ The giant crayfish makes two claw attacks.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) bludgeoning damage, and the target is grappled (escape DC 12). The crayfish has two claws, each of which can grapple only one target.