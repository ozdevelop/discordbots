"Aurochs";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_speed_: "50 ft."
_hit points_: "38 (4d10+16)"
_armor class_: "11 (natural armor)"
_stats_: | 20 (+5) | 10 (0) | 19 (+4) | 2 (-4) | 12 (+1) | 5 (-3) |

___Charge.___ If the aurochs moves at least 20 feet straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 9 (2d8) piercing damage. If the target is a creature, it must succeed on a DC 15 Strength saving throw or be knocked prone.

**Actions**

___Gore.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (2d8+5) piercing damage.