"Sword Wight";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "the languages it knew in life"
_skills_: "Perception +4, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "poison"
_damage_resistances_: "fire, necrotic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t silvered"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "66 (12d8 + 12)"
_armor class_: "16 (chainmail)"
_stats_: | 12 (+1) | 12 (+1) | 13 (+1) | 11 (+0) | 13 (+1) | 15 (+2) |

___Improved Critical.___ Greatsword attacks score a critical hit on a roll of
19 or 20.

___Magical Weapons.___ Attacks by the sword wight using its weapons are
considered to be magical.

___Sunlight Sensitivity.___ While in sunlight, the sword wight has
disadvantage on attack rolls, as well as on Wisdom (Perception) checks
that rely on sight.

___Weapon Master.___ When using its greatsword attack, a sword wight
may reroll any 1 on damage dice, keeping the second result.

**Actions**

___Multiattack.___ The sword wight makes two
greatsword attacks or two longbow attacks. It can use its life drain in
place of one greatsword attack.

___Greatsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target.
Hit: 8 (2d6 + 1) slashing damage.

___Life Drain.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature.
Hit: 4 (1d6 + 1) necrotic damage. The target must succeed on a DC 15
Constitution saving throw or its hit point maximum is reduced by an
amount equal to the damage taken. This reduction lasts until the target
finishes a long rest. The target dies if this effect reduces its hit point
maximum to 0.

A humanoid slain by this attack rises 24 hours later as a zombie under
the wight’s control, unless the humanoid is restored to life or its body is
destroyed. The wight can have no more than twelve zombies under its
control at one time.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one
target. Hit: 5 (1d8 + 1) piercing damage.

**Reactions**

___Parry.___ The sword wight adds 2 to its AC against one melee attack
that would hit it. To do so, the sword wight must see the attacker and be
wielding a melee weapon.
