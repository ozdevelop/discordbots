"Venomous Mummy";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Wis +2"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "20 ft."
_hit points_: "58 (9d8 + 18)"
_armor class_: "11 (natural armor)"
_stats_: | 16 (+3) | 8 (-1) | 15 (+2) | 7 (-2) | 10 (+0) | 14 (+2) |

___Selket's Venom.___ The venomous mummy's body and wrappings are magically imbued with substances that are highly toxic. Any creature that comes in physical contact with the venomous mummy (e.g., touching the mummy barehanded, grappling, using a bite attack) must succeed on a DC 12 Constitution saving throw or be poisoned with Selket's venom. The poisoned target takes 3 (1d6) poison damage every 10 minutes. Selket's venom is a curse, so it lasts until ended by the _remove curse_ spell or comparable magic.

___Toxic Smoke.___ The venomous mummy's poison-imbued wrappings and flesh create toxic fumes when burned. If a venomous mummy takes fire damage, it is surrounded by a cloud of toxic smoke in a 10-foot radius. This cloud persists for one full round. A creature that starts its turn inside the cloud or enters it for the first time on its turn must make a DC 12 Constitution saving throw, taking 14 (4d6) poison damage on a failure, or half damage on a success.

**Actions**

___Venomous Fist.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage plus 10 (3d6) necrotic damage. If the target is a creature, it must succeed on a DC 12 Constitution saving throw or be affected by the Selket's venom curse (see above).

