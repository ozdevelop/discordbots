"Deer";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_speed_: "50 ft."
_hit points_: "4 (1d8)"
_armor class_: "13"
_stats_: | 11 (0) | 16 (+3) | 11 (0) | 2 (-4) | 14 (+2) | 5 (-3) |

**Actions**

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d4) piercing damage.