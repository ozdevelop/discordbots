"Terror Beast";;;_size_: Small aberration
_alignment_: chaotic evil
_challenge_: "10 (5,900 XP)"
_languages_: "Telepathy 120 ft."
_skills_: "Deception +8, Intimidation +8, Performance +8"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "30 ft."
_hit points_: "88 (16d8 + 16)"
_armor class_: "16 (natural armor)"
_stats_: | 6 (-2) | 16 (+3) | 12 (+1) | 10 (+0) | 14 (+2) | 19 (+4) |

___Illusory Appearance.___ The terror beast makes itself appear
as creatures far more frightening than itself. Whenever
a creature attacks the terror beast, roll a d20. On a
result of 11 or higher, the attack hits part of its illusory
appearance and does no damage. Each time the terror
beast is successfully hit, it changes form to a new
horror. Creatures with blindsight or truesight can see
the terror beast’s true form and are not affected by this
feature. Whenever the terror beast dies, it reverts to its
actual form as a small, seemingly non-threatening
creature.

___Unnatural Insight.___ The terror beast learns a creature’s
deepest fears simply by looking at them.

**Actions**

___Multiattack.___ The terror beast makes two attacks: one
with Terror Assault and one with Tear Asunder.

___Terror Assault.___ Target creature within 90 feet must
make a DC 16 Wisdom saving throw, taking 22 (5d8)
psychic damage on a failed save and becoming
frightened until the end of their next turn on a failed
save, or half as much damage and not frightened on a
successful one.

___Tear Asunder.___ Target creature within 90 feet must make
a DC 16 Intelligence saving throw, taking 22 (5d8)
psychic damage and becoming convinced one of their
limbs have been violently ripped from their body on a
failed save, or half as much damage and their limb
remains intact on a successful one. If they fail the save
by 10 or more, they become convinced their eyes are
torn out. Even though the effects aren’t real, the victim
is convinced it is reality and suffer from the same
detriments as if they actually lost that body part. This
deception ends when the terror beast dies.

___Worst Nightmares (Recharge 5-6).___ The terror beast
unleashes a torrent of nightmarish illusions against its
enemies. Each creature within 30 feet of the terror
beast must make a DC 16 Wisdom saving throw, taking
27 (6d8) psychic damage and becoming paralyzed
with fear until the end of their next turn on a failed
save, or half as much damage and not paralyzed on a
successful one. If a creature is reduced to 0 hit points
from this attack, the terror beast regains 20 hit points.

___Face Death (1/Day).___ The terror beast infests the mind of
a creature within 90 feet with nightmare and forces it
to live out its own death over and over, each death
more gruesome than the last. Have that player make
death saving throws until they reach 3 successes, 3
failures, or rolls a natural 20. If they reached 3 failures,
the creature takes 55 (10d10) psychic damage and has
disadvantage on all attacks, saving throws, and ability
checks for the next 1d4 turns. On 3 successes, the
creature takes half as much psychic damage and does
not have disadvantage. If the player rolls a natural 20
on one of their saves, the effect ends immediately with
no damage dealt as their mind conquers this attack.
