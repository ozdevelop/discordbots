"Uthgardt Shaman";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Bothii, Common"
_skills_: "Medicine +4, Nature +4, Perception +4, Survival +6"
_speed_: "30 ft."
_hit points_: "38 (7d8+7)"
_armor class_: "13"
_stats_: | 14 (+2) | 12 (+1) | 13 (+1) | 10 (0) | 15 (+2) | 12 (+1) |

___Innate Spellcasting.___ The shaman can innately cast the following spells. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks).

* At Will: _dancing lights, message, mage hand, thaumaturgy_

* 1/day: _augury, bestow curse, cordon of arrows, detect magic, hex, prayer of healing, speak with dead, spirit guardians_

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d6+2) piercing damage, or 6 (1d8 + 2) piercing damage if wielded with two hands.

___Shortbow.___ Ranged Weapon Attack: +3 to hit, range 80/320 ft., one target. Hit: 4 (1d6 + 1) piercing damage.
