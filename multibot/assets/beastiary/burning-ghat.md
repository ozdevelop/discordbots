"Burning Ghat";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire"
_damage_resistances_: "acid, lightning"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed"
_speed_: "30 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "12 (natural armor)"
_stats_: | 13 (+1) | 15 (+2) | 14 (+2) | 13 (+1) | 14 (+2) | 14 (+2) |

___Burning Blood.___ Any creature that hits the burning ghat with
a melee attack while within 5 feet of it must make a DC 13
Dexterity saving throw or take 7 (2d6) fire damage.

___Odor.___ Any creature that starts its turn within 5
feet of the burning ghat must succeed on a DC
13 Constitution saving throw or be poisoned
until the start of its next turn. On a successful
saving throw, the creature is immune to the
burning ghat’s odor for 24 hours.

**Actions**

___Multiattack.___ The burning ghat makes two claw
attacks.

___Claws.___ Melee Weapon Attack: +4 to hit,
reach 5 ft., one target. Hit: 6 (1d8 + 2)
slashing damage plus 7 (2d6) fire damage.

___Fire Burst (1/day).___ The burning ghat
unleashes a burst of flames in a 20-foot
sphere centered on itself. Creatures in
the area must make a DC 13 Dexterity
saving throw, taking 17 (5d6) fire damage
on a failed save, or half as much damage on a
successful one.
