"Ravager Beetle";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 10"
_speed_: "30 ft., fly 20 ft."
_hit points_: "26 (4d8 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 11 (+0) | 14 (+2) | 1 (-5) | 10 (+0) | 6 (-2) |

**Actions**

___Gnaw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 6 (1d6 + 3) piercing damage plus 7 (2d6) poison damage. If the target is a Medium or smaller creature, it is grappled (escape DC 13) and restrained. Until this grapple ends, the beetle cannot bite another target. A grappled target takes 7 (2d6) poison damage at the start of each of its turns.
