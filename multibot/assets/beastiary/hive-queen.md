"Hive Queen";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_speed_: "20 ft., fly 60 ft."
_hit points_: "49 (9d8 + 9)"
_armor class_: "14"
_stats_: | 12 (+1) | 18 (+4) | 13 (+1) | 6 (-2) | 13 (+1) | 13 (+1) |

___Hivemind.___ All wasps within 10 miles of their queen
are in constant communication via a telepathic
bond.

**Actions**

___Sting.___ Melee Weapon Attack: +6 to hit, reach 5ft.,
one target. Hit: 6 (1d4 + 4) piercing damage plus 2
poison damage for each giant wasp within 120 feet
that isn’t incapacitated, and the target must
succeed on a DC 13 Constitution saving throw or
become poisoned for 1 minute. The target can
repeat this saving throw at the end of their turn,
ending the effect on a success.

___Command the Hive (Recharge 5-6).___ The queen gives
out a command to all wasps within 120 feet. They
may use their reaction to immediately move up to
half of their movement speed and attack a target if
able.

___Summon the Swarm (1/Day).___ The queen summons
four giant wasps in unoccupied spaces within 60
feet. These wasps last for 1 day, until the wasps die,
or until the queen dismisses them.
