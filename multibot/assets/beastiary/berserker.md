"Berserker";;;_size_: Medium humanoid (any race)
_alignment_: any chaotic alignment
_challenge_: "2 (450 XP)"
_languages_: "any one language (usually Common)"
_speed_: "30 ft."
_hit points_: "67 (9d8+27)"
_armor class_: "13 (hide armor)"
_stats_: | 16 (+3) | 12 (+1) | 17 (+3) | 9 (-1) | 11 (0) | 9 (-1) |

___Reckless.___ At the start of its turn, the berserker can gain advantage on all melee weapon attack rolls during that turn, but attack rolls against it have advantage until the start of its next turn.

**Actions**

___Greataxe.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 9 (1d12 + 3) slashing damage.