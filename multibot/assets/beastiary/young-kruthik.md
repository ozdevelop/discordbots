"Young Kruthik";;;_page_number_: 211
_size_: Small monstrosity
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_languages_: "Kruthik"
_senses_: "darkvision 30 ft., tremorsense 60 ft., passive Perception 10"
_speed_: "30 ft., burrow 10 ft., climb 30 ft."
_hit points_: "9  (2d6 + 2)"
_armor class_: "16 (natural armor)"
_stats_: | 13 (+1) | 16 (+3) | 13 (+1) | 4 (-3) | 10 (0) | 6 (-2) |

___Keen Smell.___ The kruthik has advantage on Wisdom (Perception) checks that rely on smell.

___Pack Tactics.___ The kruthik has advantage on an attack roll against a creature if at least one of the kruthik's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Tunneler.___ The kruthik can burrow through solid rock at half its burrowing speed and leaves a 2 1/2-foot-diameter tunnel in its wake.

**Actions**

___Stab___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.