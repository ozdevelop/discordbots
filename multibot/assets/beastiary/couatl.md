"Couatl";;;_size_: Medium celestial
_alignment_: lawful good
_challenge_: "4 (1,100 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft."
_damage_immunities_: "psychic; bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Con +5, Wis +7, Cha +6"
_speed_: "30 ft., fly 90 ft."
_hit points_: "97 (13d8+39)"
_armor class_: "19 (natural armor)"
_damage_resistances_: "radiant"
_stats_: | 16 (+3) | 20 (+5) | 17 (+3) | 18 (+4) | 20 (+5) | 18 (+4) |

___Innate Spellcasting.___ The couatl's spellcasting ability is Charisma (spell save DC 14). It can innately cast the following spells, requiring only verbal components:

At will: detect evil and good, detect magic, detect thoughts

3/day each: bless, create food and water, cure wounds, lesser restoration, protection from poison, sanctuary, shield

1/day each: dream, greater restoration, scrying

___Magic Weapons.___ The couatl's weapon attacks are magical.

___Shielded Mind.___ The couatl is immune to scrying and to any effect that would sense its emotions, read its thoughts, or detect its location.

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 8 (1d6 + 5) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned for 24 hours. Until this poison ends, the target is unconscious. Another creature can use an action to shake the target awake.

___Constrict.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one Medium or smaller creature. Hit: 10 (2d6 + 3) bludgeoning damage, and the target is grappled (escape DC 15). Until this grapple ends, the target is restrained, and the couatl can't constrict another target.

___Change Shape.___ The couatl magically polymorphs into a humanoid or beast that has a challenge rating equal to or less than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the couatl's choice).

In a new form, the couatl retains its game statistics and ability to speak, but its AC, movement modes, Strength, Dexterity, and other actions are replaced by those of the new form, and it gains any statistics and capabilities (except class features, legendary actions, and lair actions) that the new form has but that it lacks. If the new form has a bite attack, the couatl can use its bite in that form.