"Chaos Beast";;;_size_: Medium aberration
_alignment_: chaotic neutral
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_skills_: "Perception +7"
_senses_: "darkvision 60 ft., passive Perception 17"
_damage_resistances_: "acid, necrotic, slashing"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_speed_: "30 ft."
_hit points_: "120 (16d8 + 48)"
_armor class_: "16 (Natural Armor)"
_stats_: | 17 (+3) | 15 (+2) | 16 (+3) | 10 (+0) | 12 (+1) | 7 (-2) |

___Amorphous.___ The chaos beast can move
through a space as narrow as 1 inch wide without
squeezing.

___Destabilize.___ A creature that touches the chaos
beast or hits it with a melee attack while within 5
feet of it must make a DC 15 Constitution saving throw or be poisoned for
1 minute. A creature poisoned in this way takes 21 (6d6) necrotic damage
at the start of each of its turns. It can repeat the saving throw at the end of
each of its turns, ending the effect on itself on a success. If the creature’s
saving throw is successful or the effect ends for it, the creature is immune
to the chaos beast’s Destabilize for the next 24 hours.

**Actions**

___Multiattack.___ The chaos beast makes two claw attacks.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) slashing damage plus 10 (3d6) necrotic damage. The creature
must make a DC 15 Constitution saving throw or be affected by the chaos
beast’s Destabilize effect.
