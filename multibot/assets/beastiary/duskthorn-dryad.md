"Duskthorn Dryad";;;_size_: Medium fey
_alignment_: chaotic
_challenge_: "3 (700 XP)"
_languages_: "Common, Elvish, Sylvan, Umbral"
_skills_: "Animal Handling +4, Deception +9, Nature +6, Perception +4, Persuasion +9, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Con +3, Wis +4"
_speed_: "30 ft."
_hit points_: "77 (14d8 + 14)"
_armor class_: "17 (natural armor)"
_stats_: | 10 (+0) | 20 (+5) | 13 (+1) | 14 (+2) | 15 (+2) | 24 (+7) |

___Innate Spellcasting.___ The dryad's innate spellcasting ability is Charisma (spell save DC 17). She can innately cast the following spells, requiring no material components:

* At will: _dancing lights, druidcraft_

* 3/day each: _charm person, entangle, invisibility, magic missile_

* 1/day each: _barkskin, counterspell, dispel magic, fog cloud, shillelagh, suggestion, wall of thorns_

___Magic Resistance.___ The dryad has advantage on saving throws against spells and other magical effects.

___Speak with Beasts and Plants.___ The dryad can communicate with beasts and plants as if they shared a language.

___Tree Stride.___ Once on her turn, the dryad can use 10 feet of her movement to step magically into one dead tree within her reach and emerge from a second dead tree within 60 feet of the first tree, appearing in an unoccupied space within 5 feet of the second tree. Both trees must be Large or bigger.

___Tree Dependent.___ The dryad is mystically bonded to her duskthorn vines and must remain within 300 yards of them or become poisoned. If she remains out of range of her vines for 24 hours, she suffers 1d6 Constitution damage, and another 1d6 points of Constitution damage every day that follows - eventually, this separation kills the dryad. A dryad can bond with new vines by performing a 24-hour ritual.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d4 + 5) piercing damage.

___Longbow.___ Ranged Weapon Attack: +7 to hit, range 150/600 ft., one target. Hit: 9 (1d8 + 5) piercing damage.

