"Dire Wolf";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_skills_: "Perception +3, Stealth +4"
_speed_: "50 ft."
_hit points_: "37 (5d10+10)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 15 (+2) | 3 (-4) | 12 (+1) | 7 (-2) |

___Keen Hearing and Smell.___ The wolf has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Pack Tactics.___ The wolf has advantage on an attack roll against a creature if at least one of the wolf's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage. If the target is a creature, it must succeed on a DC 13 Strength saving throw or be knocked prone.