"Steam Golem";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "13 (10000 XP)"
_languages_: "understands its creator's languages but can't speak"
_senses_: "darkvision 120 ft., passive Perception 10"
_damage_immunities_: "fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "171 (18d10 + 72)"
_armor class_: "18 (natural armor)"
_stats_: | 26 (+8) | 12 (+1) | 18 (+4) | 3 (-4) | 10 (+0) | 1 (-5) |

___Boiler Weakness.___ A steam golem that's immersed in water or whose boiler is soaked with at least 20 gallons of water (such as from a water elemental) may be stopped in its tracks by the loss of steam pressure in the boiler. In the case of a water elemental, dousing a steam golem destroys the elemental and the golem must make a DC 20 Constitution saving throw. If it succeeds, the water instantly evaporates and the golem continues functioning normally. If it fails, the golem's fire is extinguished and the boiler loses pressure. The steam golem acts as if affected by a slow spell for 1d3 rounds, then becomes paralyzed until its fire is relit and it spends 15 minutes building up pressure.

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Extend Long Ax.___ A steam golem can extend or retract one arm into long ax form as a bonus action.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

**Actions**

___Multiattack.___ The steam golem makes two ax arm attacks, or one long axe attack.

___Ax Arm.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 22 (4d6 + 8) slashing damage.

___Long Axe.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 40 (5d12 + 8) slashing damage.

___Steam Blast (Recharge 5-6).___ A steam golem can release a blast of steam. The golem chooses whether to affect a 5-foot radius around itself or a 20-foot cube adjacent to itself. Creatures in the affected area take 38 (7d10) fire damage, or half damage with a successful DC 17 Constitution saving throw.

**Reactions**

___Whistle.___ When an opponent within 30 feet of the golem tries to cast a spell, the steam golem can emit a shriek from its twin steam whistles. The spellcaster must make a DC 17 Constitution saving throw. If the save succeeds, the spell is cast normally. If it fails, the spell is not cast; the spell slot is not used, but the caster's action is.

