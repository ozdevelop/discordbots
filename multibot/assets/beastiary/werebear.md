"Werebear";;;_size_: Medium humanoid (human shapechanger)
_challenge_: "5 (1,800 XP)"
_languages_: "Common (can't speak in bear form)"
_skills_: "Perception +7"
_damage_immunities_: "bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_speed_: "30 ft. (40 ft., climb 30 ft. in bear or hybrid form)"
_hit points_: "135 (18d8+54)"
_armor class_: "10 (in humanoid form, 11 in bear and hybrid forms )"
_stats_: | 19 (+4) | 10 (0) | 17 (+3) | 11 (0) | 12 (+1) | 12 (+1) |

___Shapechanger.___ The werebear can use its action to polymorph into a Large bear-humanoid hybrid or into a Large bear, or back into its true form, which is humanoid. Its statistics, other than its size and AC, are the same in each form. Any equipment it. is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Keen Smell.___ The werebear has advantage on WisGlom (Perception) checks that rely on smell.

**Actions**

___Multiattack.___ In bear form, the werebear makes two claw attacks. In humanoid form, it makes two greataxe attacks. In hybrid form, it can attack like a bear or a humanoid.

___Bite (Bear or Hybrid Form Only).___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) piercing damage. If the target is a humanoid, it must succeed on a DC 14 Constitution saving throw or be cursed with were bear lycanthropy.

___Claw (Bear or Hybrid Form Only).___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

___Greataxe (Humanoid or Hybrid Form Only).___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 10 (1d12 + 4) slashing damage.