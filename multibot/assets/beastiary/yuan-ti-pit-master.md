"Yuan-ti Pit Master";;;_size_: Medium monstrosity (shapechanger yuan-ti)
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal, Common, Draconic"
_senses_: "darkvision 120 ft. (penetrates magical darkness)"
_skills_: "Deception +5, Stealth +4"
_damage_immunities_: "poison"
_saving_throws_: "Wis +3, Cha +5"
_speed_: "30 ft."
_hit points_: "88 (16d8+16)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 16 (+3) | 14 (+2) | 13 (+1) | 14 (+2) | 12 (+1) | 16 (+3) |

___Shapechanger.___ The yuan-ti can use its action to polymorph into a Medium snake or back into its true form. Its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It doesn't change form if it dies.

___Innate Spellcasting (Yuan-ti Form Only).___ The yuan-ti's innate spellcasting ability is Charisma (spell save DC 13). The yuan-ti can innately cast the following spells, requiring no material components:

At will: animal friendship (snakes only) 3/day: suggestion

___Magic Resistance.___ The yuan-ti has advantage on saving throws against spells and other magical effects.

___Poison's Disciple (2/Day).___ The first time the yuan-ti hits with a melee attack on its turn, it can deal an extra 16 (3d10) poison damage to the target.

___Spellcasting (Yuan-ti Farm Only).___ The yuan-ti is a 6th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

Cantrips (at will): eldritch blast (range 300 ft., +3 bonus to each damage roll), friends, guidance, mage hand, message, poison spray

1st-3rd level (2 3rd-level slots): command, counterspell, hellish rebuke, invisibility, misty step, unseen servant, vampiric touch

___Variant: Chameleon Skin.___ The yuan-ti has advantage on Dexterity (Stealth) checks made to hide.

___Variant: Shed Skin (1/Day).___ The yuan-ti can shed its skin as a bonus action to free itself from a grapple, shackles, or other restraints. If the yuan-ti spends 1 minute eating its shed skin, it regains hit points equal to half its hit point maximum.

**Actions**

___Multiattack (Yuan-ti Form Only).___ The yuan-ti makes two bite attacks using its snake arms.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4+3) piercing damage plus 7 (2d6) poison damage.

___Merrshaulk's Slumber (1/Day).___ The yuan-ti targets up to five creatures that it can see within 60 feet of it. Each target must succeed on a DC 13 Constitution saving throw or fall into a magical sleep and be unconscious for 10 minutes. A sleeping target awakens if it takes damage or if someone uses an action to shake or slap it awake. This magical sleep has no effect on a creature immune to being charmed.