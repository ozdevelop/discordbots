"Fraz-Urb'luu (B)";;;_page_number_: 146
_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "23 (50,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 24"
_skills_: "Deception +15, Perception +14, Stealth +8"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_saving_throws_: "Dex +8, Con +14, Int +15, Wis +14"
_speed_: "40 ft., fly 40 ft."
_hit points_: "337  (27d10 + 189)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 29 (+9) | 12 (+1) | 25 (+7) | 26 (+8) | 24 (+7) | 26 (+8) |

___Innate Spellcasting.___ Fraz-Urb'luu's spellcasting ability is Charisma (spell save DC 23). Fraz-Urb'luu can innately cast the following spells, requiring no material components:

* At will: _alter self _(can become Medium-sized when changing his appearance)_, detect magic, dispel magic, phantasmal force_

* 3/day each: _confusion, dream, mislead, programmed illusion, seeming_

* 1/day each: _mirage arcane, modify memory, project image_

___Legendary Resistance (3/Day).___ If Fraz-Urb'luu fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Fraz-Urb'luu has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Fraz-Urb'luu's weapon attacks are magical.

___Undetectable.___ Fraz-Urb'luu can't be targeted by divination magic, perceived through magical scrying sensors, or detected by abilities that sense demons or fiends.

**Actions**

___Multiattack___ Fraz-Urb'luu makes three attacks: one with his bite and two with his fists.

___Bite___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 19 (3d6 + 9) piercing damage.

___Fist___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 22 (3d8 + 9) bludgeoning damage.

**Legendary** Actions

Fraz-Urb'luu can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. FrazUrb'luu regains spent legendary actions at the start of his turn.

___Tail___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 20 (2d10 + 9) bludgeoning damage. If the target is a Large or smaller creature, it is also grappled (escape DC 24). The grappled target is also restrained. Fraz-Urb'luu can grapple only one creature with his tail at a time.

___Phantasmal Killer (Costs 2 Actions)___ Fraz-Urb'luu casts phantasmal killer, no concentration required.
