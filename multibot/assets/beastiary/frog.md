"Frog";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 30 ft."
_skills_: "Perception +1, Stealth +3"
_speed_: "20 ft., swim 20 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "11"
_stats_: | 1 (-5) | 13 (+1) | 8 (-1) | 1 (-5) | 8 (-1) | 3 (-4) |

___Amphibious.___ The frog can breathe air and water

___Standing Leap.___ The frog's long jump is up to 10 ft. and its high jump is up to 5 ft., with or without a running start.