"Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "understands all languages it spoke in life but can't speak"
_senses_: "darkvision 60 ft."
_saving_throws_: "Wis +0"
_speed_: "20 ft."
_hit points_: "22 (3d8+9)"
_armor class_: "8"
_condition_immunities_: "poisoned"
_stats_: | 13 (+1) | 6 (-2) | 16 (+3) | 3 (-4) | 6 (-2) | 5 (-3) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5+the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Slam.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) bludgeoning damage.