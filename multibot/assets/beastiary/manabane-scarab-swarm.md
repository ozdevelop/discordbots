"Manabane Scarab Swarm";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +5"
_senses_: "blindsight 10 ft., darkvision 30 ft., tremorsense 30 ft., passive Perception 13"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "20 ft., burrow 5 ft., climb 20ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 3 (-4) | 16 (+3) | 16 (+3) | 1 (-5) | 13 (+1) | 2 (-4) |

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny insect. The swarm can't regain hit points or gain temporary hit points.

___Magic Immunity.___ The manabane scarab swarm is immune to spells and other magical effects.

___Scent Magic.___ The manabane scarab swarm can detect the presence of magical creatures, active spells or spell effects, and magical items within 120 feet.

___Mana Erosion.___ The manabane scarab swarm consumes magic. Unattended magic items in the swarm's space at the end of the swarm's turn have their effects suppressed for 1 minute. Additionally, charged items in the swarm's space lose 1d6 charges at the start of each of the swarm's turns; items with limited uses per day lose one daily use instead, and single-use items such as potions or scrolls are destroyed. Magical effects in the swarm's space are dispelled (as if affected by dispel magic cast with +5 spellcasting ability).

**Actions**

___Bites.___ Melee Weapon Attack: +5 to hit, reach 0 ft., one creature in the swarm's space. Hit: 14 (4d6) piercing damage, or 7 (2d6) piercing damage if the swarm has half of its hit points or fewer. The target must succeed on a DC 15 Dexterity saving throw or one randomly determined magic item in its possession is immediately affected by the Mana Erosion trait. A spellcaster hit by this attack must succeed on a DC 15 Charisma saving throw or one of its lowest-level, unused spell slots is expended.

