"Quaggoth";;;_size_: Medium humanoid (quaggoth)
_alignment_: chaotic neutral
_challenge_: "2 (450 XP)"
_languages_: "Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Athletics +5"
_damage_immunities_: "poison"
_speed_: "30 ft., climb 30 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "13 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 17 (+3) | 12 (+1) | 16 (+3) | 6 (-2) | 12 (+1) | 7 (-2) |

___Wounded Fury.___ While it has 10 hit points or fewer, the quaggoth has advantage on attack rolls. In addition, it deals an extra 7 (2d6) damage to any target it hits with a melee attack.

**Actions**

___Multiattack.___ The quaggoth makes two claw attacks.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.