"Ice Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Aquan, Auran"
_senses_: "darkvision 60 ft."
_skills_: "Perception +2, Stealth +3"
_damage_immunities_: "cold, poison"
_speed_: "30 ft., fly 30 ft."
_hit points_: "21 (6d6)"
_armor class_: "11"
_damage_vulnerabilities_: "bludgeoning, fire"
_condition_immunities_: "poisoned"
_stats_: | 7 (-2) | 13 (+1) | 10 (0) | 9 (-1) | 11 (0) | 12 (+1) |

___Death Burst.___ When the mephit dies, it explodes in a burst of jagged ice. Each creature within 5 ft. of it must make a DC 10 Dexterity saving throw, taking 4 (1d8) slashing damage on a failed save, or half as much damage on a successful one.

___False Appearance.___ While the mephit remains motionless, it is indistinguishable from an ordinary shard of ice.

___Innate Spellcasting (1/Day).___ The mephit can innately cast fog cloud, requiring no material components. Its innate spellcasting ability is Charisma.

**Actions**

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 3 (1d4 + 1) slashing damage plus 2 (1d4) cold damage.

___Frost Breath (Recharge 6).___ The mephit exhales a 15-foot cone of cold air. Each creature in that area must succeed on a DC 10 Dexterity saving throw, taking 5 (2d4) cold damage on a failed save, or half as much damage on a successful one.

___Variant: Summon Mephits (1/Day).___ The mephit has a 25 percent chance of summoning 1d4 mephits of its kind. A summoned mephit appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other mephits. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.