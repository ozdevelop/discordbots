"Vengeful Archangel";;;_size_: Large celestial
_alignment_: chaotic evil
_challenge_: "18 (20,000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Athletics +13, Perception +13"
_senses_: "darkvision 120ft., passive Perception 23"
_saving_throws_: "Str +13, Con +13, Wis +13"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhausted, frightened"
_speed_: "30 ft., fly 120 ft."
_hit points_: "250 (20d10 + 140)"
_armor class_: "20 (natural armor)"
_stats_: | 25 (+7) | 20 (+5) | 24 (+7) | 18 (+4) | 24 (+7) | 22 (+6) |

___Corrupted Blades.___ The archangel's weapon attacks are
magical. When the archangel hits with any weapon, the
weapon deals an extra 2d8 necrotic damage and any
creature hit can't regain hit points until the start of the
archangel’s next turn (included in the attack).

___Innate Spellcasting.___ The archangel's spellcasting ability
is Charisma (spell save DC 20). The seraph can innately
cast the following spells, requiring only verbal
components.

* At will: _detect evil and good, invisibility _(self only)

* 2/day each: _circle of death, harm_

* 1/day each: _finger of death, forcecage, power word stun_

___Magic Resistance.___ The archangel has advantage on
saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The bringer makes four longsword attacks.

___Longsword.___ Melee Weapon Attack: +13 to hit, reach 5
ft., one target. Hit: 16 (2d8 + 7) slashing damage plus
9 (2d8) necrotic damage and that creature can't regain
hit points until the start of the archangel’s next turn.

___Storm of Swords (Recharge 5-6).___ The archangel clashes
its blades together and instantly creates hundreds of
ethereal weapons that rapidly spiral into the air around
it. Each creature within 20 feet of the archangel must
make a DC 20 Dexterity saving throw, taking 28
(5d10) slashing damage and 28 (5d10) force damage
on a failed saving throw, or half as much damage on a
successful one.

___Falling Star (1/Day).___ The angel teleports high into the air,
then comes crashing down with tremendous force at a
point within 120 feet of its starting location. Each
creature within 30 feet of that point must make a DC
20 Constitution saving throw. On a failed save, the
creature takes 55 (10d10) thunder damage and is
pushed 15 feet away from the archangel and knocked
prone. On a successful save, the creature takes half as
much damage and not pushed or knocked prone. This
area becomes difficult terrain until repaired of the
damage caused by this attack. In addition, unsecured
objects that are completely within the area of effect of
this attack are automatically pushed 15 feet away and a
thunderous boom rings out that is audible up to 1000
feet.

**Legendary** Actions

The archangel can take 3 legendary actions, choosing
from the options below. Only one legendary action can
be used at a time and only at the end of another
creature’s turn. The archangel regains spent legendary
actions at the start of its turn.

___Teleport.___ The archangel magically teleports, along with
any equipment it is wearing or carrying, up to 120 feet
to an unoccupied space it can see.

___Condemnation (Costs 2 Actions).___ The archangel extends
a hand and condemns a creature for its actions. Target
creature within 90 feet that can hear the archangel
must succeed on DC 20 Wisdom saving throw or
become frightened of the archangel for 1 minute.
While frightened by this effect, the creature must use
its action to drop to its knees and confess its sins. A
frightened target can repeat the saving throw at the
end of each of its turns, ending the effect on itself on a
success and becoming immune to the archangel's
Condemnation for the next 24 hours.

___Wall of Denial (Costs 3 Actions).___ The archangel
summons a barrier at a location it can see within 120
feet. This wall is 20 feet tall and 90 feet long and must
fill unoccupied spaces. It is created of translucent,
luminescent glass with AC 15 and 40 hit points per
10-foot section. A creature that breaks a portion of this
wall must succeed on a DC 15 Constitution saving
throw or become stunned until the end of its next turn.
