"Aetherspawn";;;_size_: Medium elemental
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "Primordial"
_senses_: "passive Perception 12"
_damage_resistances_: "bludgeoning, piercing, slashing damage from nonmagical weapons; see Elemental Attunement"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "30 ft., fly 30 ft. (hover)"
_hit points_: "26 (4d8 + 8)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 15 (+2) | 15 (+2) | 14 (+2) | 15 (+2) | 8 (-1) |

___Elemental Attunement.___ The guardian is infused with
either arcane, ice, fire, or lightning energy. The
guardian's attacks deal extra damage based upon
this element (included in the attack), it has
resistances to the corresponding element, and can
cast a corresponding elemental spell once per day
requiring no material components. The guardian's
spellcasting ability is Intelligence (spell save DC 12, +4 bonus to hit).

* Arcane – bonus force damage and resistance to force damage; can cast _magic missile_
* Ice - bonus cold damage and resistance to cold damage; can cast _ice knife_
* Fire – bonus fire damage and resistance to fire damage; can cast _burning hands_
* Lightning – bonus lightning damage and resistance to lightning damage; can cast _witch bolt_

**Actions**

___Aether Blades.___ Melee Weapon Attack: +4 to hit,
reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing
damage plus 3 (1d6) damage of the type
corresponding to the guardian's Elemental
Attunement.

___Elemental Shards.___ Ranged Weapon Attack: +4 to hit,
range 30/90 ft., one target. Hit: 4 (1d4 + 2)
piercing damage plus 3 (1d6) damage of the type
corresponding to the guardian's Elemental
Attunement.
