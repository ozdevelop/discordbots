"Conjurer";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "6 (2,300 XP)"
_languages_: "any four languages"
_skills_: "Arcana +6, History +6"
_saving_throws_: "Int +6, Wis +4"
_speed_: "30 ft."
_hit points_: "40 (9d8)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 11 (0) | 17 (+3) | 12 (+1) | 11 (0) |

___Spellcasting.___ The conjurer is a 9th-level spellcaster. Its spellcasting ability is intelligence (spell save DC 14, +6 to hit with spell attacks). The conjurer has the following wizard spells prepared:

* Cantrips (at will): _acid splash, mage hand, poison spray, prestidigitation_

* 1st level (4 slots): _mage armor, magic missile, unseen servant\* _

* 2nd level (3 slots): _cloud of daggers\*, misty step\*, web\* _

* 3rd level (3 slots): _fireball, stinking cloud\* _

* 4th level (3 slots): _Evard's black tentacles\*, stoneskin_

* 5th level (2 slots): _cloudkill\*, conjure elemental\* _

*Conjuration spell of 1st level or higher

___Benign Transportation (Recharges after the Conjurer Casts a Conjuration Spell of 1st Level or Higher).___ As a bonus action, the conjurer teleports up to 30 feet to an unoccupied space that it can see. If it instead chooses a space within range that is occupied by a willing Small or Medium creature, they both teleport, swapping places.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4+2) piercing damage.
