"Star Spawn Larva Mage";;;_page_number_: 235
_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "16 (15,000 XP)"
_languages_: "Deep Speech"
_senses_: "darkvision 60 ft., passive Perception 16"
_skills_: "Perception +6"
_damage_immunities_: "psychic"
_saving_throws_: "Dex +6, Wis +6, Cha +8"
_speed_: "30 ft."
_hit points_: "168  (16d8 + 96)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned, restrained"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 17 (+3) | 12 (+1) | 23 (+6) | 18 (+4) | 12 (+1) | 16 (+3) |

___Innate Spellcasting.___ The larva mage's innate spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _eldritch blast _(3 beams, +3 bonus to each damage roll)_, minor illusion_

* 3/day: _dominate monster_

* 1/day: _circle of death_


___Return to Worms.___ When the larva mage is reduced to 0 hit points, it breaks apart into a swarm of insects in the same space. Unless the swarm is destroyed, the larva mage reforms from it 24 hours later.

**Actions**

___Slam___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 7 (1d8 + 3) bludgeoning damage, and the target must succeed on a DC 19 Constitution saving throw or be poisoned until the end of its next turn.

___Plague of Worms (Recharge 6)___ Each creature other than a star spawn within 10 feet of the larva mage must make a DC 19 Dexterity saving throw. On a failure the target takes 22 (5d8) necrotic damage and is blinded and restrained by masses of swarming worms. The affected creature takes 22 (5d8) necrotic damage at the start of each of the larva mage's turns. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

**Legendary** Actions

The larva mage can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The larva mage regains spent legendary actions at the start of its turn.

___Cantrip (Costs 2 Actions)___ The larva mage casts one cantrip.

___Slam (Costs 2 Actions)___ The larva mage makes one slam attack.

___Feed (Costs 3 Actions)___ Each creature restrained by the larva mage's Plague of Worms takes 13 (3d8) necrotic damage, and the larva mage gains 6 temporary hit points.
