"Rubezahl";;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: "10 (5900 XP)"
_languages_: "Abyssal, Common, telepathy 120 ft."
_skills_: "Deception +8, Perception +5, Survival +5"
_senses_: "blindsight 10 ft., darkvision 120 ft., passive Perception 15"
_saving_throws_: "Dex +6, Con +6, Wis +5"
_damage_immunities_: "lightning, thunder, poison"
_damage_resistances_: "cold, fire; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "poisoned, stunned"
_speed_: "50 ft."
_hit points_: "110 (17d8 + 34)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 15 (+2) | 14 (+2) | 11 (+0) | 12 (+1) | 18 (+4) |

___Counting Compulsion.___ If a creature uses an action to point out an ordered group of objects to the rubezahl, the demon is compelled to count the entire group. Until the end of its next turn, the rubezahl has disadvantage on attack rolls and ability checks and it can't take reactions. Once it has counted a given group of objects, it can't be compelled to count those objects ever again.

___False Tongue.___ The rubezahl has advantage on Charisma (Deception) checks, and magical attempts to discern lies always report that the rubezahl's words are true.

___Innate Spellcasting.___ The rubezahl's innate spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: disguise self (humanoid forms only), fog cloud

3/day each: call lightning, gust of wind, lightning bolt

1/day: control weather

___Sneak Attack (1/turn).___ The rubezahl does an extra 10 (3d6) damage if it hits a target with a weapon attack when it had advantage on the attack roll, or if the target is within 5 feet of an ally of the rubezahl that isn't incapacitated and the rubezahl doesn't have disadvantage on the attack roll.

**Actions**

___Multiattack.___ The rubezahl makes one gore attack and two claw attacks.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 15 (3d6 + 5) slashing damage.

___Gore.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 18 (3d8 + 5) piercing damage and a target creature must succeed on a DC 15 Strength saving throw or be knocked prone.

___Thunderstrike (Recharge 5-6).___ The rubezahl calls a sizzling bolt of lightning out of the sky, or from the air if underground or indoors, to strike a point the rubezahl can see within 150 feet. All creatures within 20 feet of the target point take 36 (8d8) lightning damage, or half damage with a successful DC 16 Dexterity saving throw. A creature that fails its saving throw is stunned until the start of the rubezahl's next turn.

