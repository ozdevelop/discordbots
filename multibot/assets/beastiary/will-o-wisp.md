"Will-o'-Wisp";;;_size_: Tiny undead
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 120 ft."
_damage_immunities_: "lightning, poison"
_speed_: "0 ft., fly 50 ft. (hover)"
_hit points_: "22 (9d4)"
_armor class_: "19"
_condition_immunities_: "exhaustion, grappled, paralyzed, poisoned, prone, restrained, unconscious"
_damage_resistances_: "acid, cold, fire, necrotic, thunder, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 1 (-5) | 28 (+9) | 10 (0) | 13 (+1) | 14 (+2) | 11 (0) |

___Consume Life.___ As a bonus action, the will-o'-wisp can target one creature it can see within 5 ft. of it that has 0 hit points and is still alive. The target must succeed on a DC 10 Constitution saving throw against this magic or die. If the target dies, the will-o'-wisp regains 10 (3d6) hit points.

___Ephemeral.___ The will-o'-wisp can't wear or carry anything.

___Incorporeal Movement.___ The will-o'-wisp can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Variable Illumination.___ The will-o'-wisp sheds bright light in a 5- to 20-foot radius and dim light for an additional number of ft. equal to the chosen radius. The will-o'-wisp can alter the radius as a bonus action.

**Actions**

___Shock.___ Melee Spell Attack: +4 to hit, reach 5 ft., one creature. Hit: 9 (2d8) lightning damage.

___Invisibility.___ The will-o'-wisp and its light magically become invisible until it attacks or uses its Consume Life, or until its concentration ends (as if concentrating on a spell).