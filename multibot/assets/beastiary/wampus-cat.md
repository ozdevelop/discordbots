"Wampus Cat";;;_size_: Medium monstrosity
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Common"
_skills_: "Deception +5, Persuasion +5"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "40 ft., climb 20 ft., swim 20 ft."
_hit points_: "58 (9d8 + 18)"
_armor class_: "14"
_stats_: | 14 (+2) | 18 (+4) | 15 (+2) | 12 (+1) | 14 (+2) | 16 (+3) |

___Focused Animosity.___ The wampus cat has advantage on melee attacks against any male she has seen employ divine magic or wield a holy symbol.

___Innate Spellcasting.___ The wampus cat's innate spellcasting ability is Charisma (spell save DC 13). She can innately cast the following spells, requiring no material components:

* At will: _disguise self _(appearance of a female human)_, mage hand_

* 2/day: _hex_

___Magic Resistance.___ The wampus cat has advantage on saving throws against spells and other magical effects.

**Actions**

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Yowl (Recharge 5-6).___ Intelligent creatures within 60 feet of the cat who are able to hear its voice must make a DC 13 Charisma saving throw. Those who fail find the sound of the wampus cat's voice pleasant and alluring, so that the cat has advantage on Charisma checks against them for 1 minute. The affected characters cannot attack the wampus cat during this time unless they are wounded in that time.

