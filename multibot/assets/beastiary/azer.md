"Azer";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "Ignan"
_damage_immunities_: "fire, poison"
_saving_throws_: "Con +4"
_speed_: "30 ft."
_hit points_: "39 (6d8+12)"
_armor class_: "17 (natural armor, shield)"
_condition_immunities_: "poisoned"
_stats_: | 17 (+3) | 12 (+1) | 15 (+2) | 12 (+1) | 13 (+1) | 10 (0) |

___Heated Body.___ A creature that touches the azer or hits it with a melee attack while within 5 ft. of it takes 5 (1d10) fire damage.

___Heated Weapons.___ When the azer hits with a metal melee weapon, it deals an extra 3 (1d6) fire damage (included in the attack).

___Illumination.___ The azer sheds bright light in a 10-foot radius and dim light for an additional 10 ft..

**Actions**

___Warhammer.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) bludgeoning damage, or 8 (1d10 + 3) bludgeoning damage if used with two hands to make a melee attack, plus 3 (1d6) fire damage.