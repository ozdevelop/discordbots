"Sapphire Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Draconic"
_skills_: "Insight +4, Perception +4"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 16"
_saving_throws_: "Dex +4, Int +4, Wis +4, Cha +4"
_damage_vulnerabilities_: "psychic"
_speed_: "30 ft., fly 60 ft. (hover)"
_hit points_: "55 (10d8 + 10)"
_armor class_: "18 (natural armor)"
_stats_: | 15 (+2) | 15 (+2) | 12 (+1) | 16 (+3) | 14 (+2) | 15 (+2) |

___Awe Aura.___ All creatures within 30 feet must
make a DC 13 Charisma saving throw in order to
attack this dragon. On a failed save, the attacking
creature’s turn ends immediately. On a success,
that creature is immune to the Awe Aura of all
gemstone dragons for 1 week.

**Psionics**

___Charges:___ 10 | ___Recharge:___ 1d4 | ___Fracture:___ 9

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 7 (1d10 + 2) piercing damage.
