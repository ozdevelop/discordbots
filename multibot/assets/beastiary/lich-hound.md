"Lich Hound";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "4 (1100 XP)"
_languages_: "understands Darakhul"
_skills_: "Acrobatics +6, Perception +4"
_senses_: "blindsight 100 ft., passive Perception 14"
_saving_throws_: "Dex +4, Con +4, Cha +3"
_damage_immunities_: "poison"
_damage_resistances_: "piercing and bludgeoning from nonmagical weapons"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft., fly 50 ft."
_hit points_: "119 (14d8 + 56)"
_armor class_: "14"
_stats_: | 10 (+0) | 18 (+4) | 18 (+4) | 6 (-2) | 10 (+0) | 16 (+3) |

___Keen Hearing and Smell.___ The lich hound has advantage on Wisdom (Perception) checks that rely on hearing or smell.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (1d12 + 4) piercing damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be knocked prone.

___Ethereal Jaunt.___ As a bonus action, the lich hound can magically shift from the Material Plane to the Ethereal Plane, or vice versa.

___Gut Rip.___ As a bonus action, the lich hound tears into any adjacent prone creature, inflicting 19 (3d12) slashing damage. The target must succeed on a DC 14 Constitution saving throw or be incapacitated for 1d4 rounds. An incapacitated creature repeats the saving throw at the end of each of its turns; a successful save ends the condition early.

___Howl.___ The eerie howl of lich hounds as they close in on their prey plays havoc on the morale of living creatures that hear it. Howling requires and action, and creatures that hear the howl of a lich hound within 100 feet must succeed on a DC 14 Wisdom saving throw or become frightened for 5 rounds. Creatures that successfully save against this effect cannot be affected by a lich hound's howl for 24 hours.

