"Soot Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Ignan"
_skills_: "Perception +4, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 30 ft."
_hit points_: "21 (6d6)"
_armor class_: "12"
_stats_: | 10 (+0) | 14 (+2) | 10 (+0) | 6 (-2) | 11 (+0) | 15 (+2) |

___Death Burst.___ When the mephit dies, it explodes in a cloud of smoke.
Each creature within 5 feet of the mephit must make a successful DC 10
Constitution saving throw or take 1d4 fire damage and be poisoned for 1
minute. A poisoned creature can repeat the saving throw at the end of each
of its turns, ending the effect on a success.

___Innate Spellcasting.___ The mephit can innately cast _blur_ 1/day, requiring
no material components. Its innate spellcasting ability is Charisma (spell
save DC 12, +4 to hit with spell attacks).

**Actions**

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4
(1d4 + 2) slashing damage plus 2 (1d4) fire damage.

___Sooty Breath (Recharge 5–6).___ The mephit exhales black soot in a 15-
foot cone. All creatures within that area must make a DC 12 Dexterity
saving throw, taking 9 (2d8) fire damage on a failed save, or half as much
damage on a successful one.

___Ember Storm (1/day).___ A soot mephit can create a downpour of whitehot
embers that affects a 20-foot radius sphere centered on itself. All
creatures caught in the storm must make a DC 12 Dexterity saving throw,
taking 10 (3d6) fire damage on a failed save, or half as much damage on
a successful one.
