"Duvessa Shane";;;_size_: Medium humanoid (illuskan human)
_alignment_: lawful good
_challenge_: "0 (10 XP)"
_languages_: "Common, Dwarvish, Giant, Orc"
_skills_: "Deception +5, Insight +3, Persuasion +3"
_speed_: "30 ft."
_hit points_: "9 (2d8)"
_armor class_: "10"
_senses_: " passive Perception 12"
_stats_: | 10 (0) | 11 (0) | 10 (0) | 16 (+3) | 14 (+2) | 16 (+3) |

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +2 to hit, reach 5 ft. or range 20/60ft., one target. Hit: 2 (1d4) piercing damage. Duvessa carries only one dagger.

**Reactions**

___Parry.___ Duvessa adds 2 to her AC against on melee attack that would hit him. To do so, Duvessa must see the attacker and be wielding a melee weapon.

**Roleplaying** Information

The daughter of a Waterdhavian trader and a tavern server, Duvessa has her mother's talent for negotiation and her father's charm. As the first woman to serve as Town Speaker of Bryn Shander, and a young one at that, she has much to prove.

**Ideal:** "The people of Icewind Dale are survivors. They can weather any storm."

**Bond:** "My mother taught me what it means to be a good leader. I won't disappoint her."

**Flaw:** "I don't give an inch in any argument of conflict."