"Ghostwalk Spider";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "9 (5000 XP)"
_languages_: "understands Undercommon but can't speak"
_skills_: "Perception +6"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +9, Cha +3"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "50 ft., climb 50 ft."
_hit points_: "119 (14d10 + 42)"
_armor class_: "15"
_stats_: | 15 (+2) | 20 (+5) | 17 (+3) | 9 (-1) | 14 (+2) | 8 (-1) |

___Ghostwalk.___ As a bonus action, the ghostwalk spider becomes invisible and intangible. Attacking doesn't end this invisibility. While invisible, the ghostwalk spider has advantage on Dexterity (Stealth) checks and gains the following: Damage Resistances acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing damage from nonmagical weapons. Condition Immunities paralyzed, petrified, prone, restrained, stunned. The ghostwalk ends when the spider chooses to end it as a bonus action or when the spider dies

___Incorporeal Movement (During Ghostwalk Only).___ The ghostwalk spider can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Spider Climb.___ The spider can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Web Walker.___ The spider ignores movement restrictions caused by webbing.

**Actions**

___Multiattack.___ The ghostwalk spider makes two bite attacks. It can make a ghostly snare attack in place of one of its bites.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 16 (2d10 + 5) piercing damage plus 13 (3d8) poison damage, or half poison damage with a successful DC 15 Constitution saving throw. If the poison damage reduces the target to 0 hit points, the target is stable but poisoned and paralyzed for 1 hour, even after regaining hit points. While using Ghostwalk, the spider's bite and poison do half damage to targets that aren't affected by Ghostly Snare (see below).

___Ghostly Snare (During Ghostwalk Only, Recharge 5-6).___ Ranged Weapon Attack: +9 to hit, range 40/160 ft., one target. Hit: The target is restrained by ghostly webbing. While restrained in this way, the target is invisible to all creatures except ghostwalk spiders, and it has resistance to acid, cold, fire, lightning, and thunder damage. A creature restrained by Ghostly Snare can escape by using an action to make a successful DC 14 Strength check, or the webs can be attacked and destroyed (AC 10; hp 5).

