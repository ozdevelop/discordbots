"Prehistoric Rhinoceros (Elasmotherium)";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "12 (8,400 XP)"
_languages_: "--"
_skills_: "Perception +9"
_senses_: "passive Perception 19"
_speed_: "50 ft."
_hit points_: "237 (19d10 + 133)"
_armor class_: "15 (natural armor)"
_stats_: | 27 (+8) | 10 (+0) | 25 (+7) | 2 (-4) | 13 (+1) | 2 (-4) |

___Keen Smell.___ The Elasmotherium has advantage on Wisdom
(Perception) checks that rely on smell.

___Siege Monster.___ The Elasmotherium deals double damage to objects and
structures.

___Trample.___ If the Elasmotherium moves at least 20 feet straight toward
a creature and then hits it with a gore attack on the same turn, that target
must succeed on a DC 18 Strength saving throw or be knocked prone. If
the target is prone, the Elasmotherium can make stomp on the target as a
bonus action.

**Actions**

___Gore.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 43
(10d6 + 8) piercing damage.

___Stomp.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit:
34 (4d12 + 8) bludgeoning damage and the target must succeed on a DC
17 Constitution saving throw or be stunned for 1 minute. The stunned
creature can repeat the saving throw on each of its turns, ending the effect
on a success.
