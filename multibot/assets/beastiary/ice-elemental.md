"Ice Elemental";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Aquan"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "thunder, fire"
_damage_immunities_: "poison, cold"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "40 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 10 (+0) | 18 (+4) | 5 (-3) | 10 (+0) | 8 (-1) |

___Frozen Form.___ The ground within a 15ft. radius
sphere centered on the elemental is considered
difficult terrain as it becomes lightly frozen.
Whenever a creature enters or moves within this
area for the first time on a turn, they must succeed
on a DC 14 Dexterity saving throw or fall prone.

**Actions**

___Multiattack.___ The elemental makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5ft.,
one target. Hit: 12 (2d8 + 3) bludgeoning damage.

___Ice Shard.___ Ranged Weapon Attack: +6 to hit, range
30/120 ft., one target. Hit: 12 (2d8 + 3) piercing
damage and 9 (2d8) cold damage.

___Frost Explosion (Recharge 4-6).___ The elemental sends
out a flurry of frozen shards and snow. Each
creature within the area of the elemental’s Frozen
Form feature must make a DC 14 Constitution
saving throw. On a failure, a target takes 22 (5d8)
cold damage and its movement speed is reduced to
0 until the end of its next turn. If the saving throw
is a success, the target takes no damage and has its
speed halved until the end of its next turn.
