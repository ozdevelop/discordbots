"Storm Giant";;;_size_: Huge giant
_alignment_: chaotic good
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Giant"
_skills_: "Arcana +8, Athletics +14, History +8, Perception +9"
_damage_immunities_: "lightning, thunder"
_saving_throws_: "Str +14, Con +10, Wis +9, Cha +9"
_speed_: "50 ft., swim 50 ft."
_hit points_: "230 (20d12+100)"
_armor class_: "16 (scale mail)"
_damage_resistances_: "cold"
_stats_: | 29 (+9) | 14 (+2) | 20 (+5) | 16 (+3) | 18 (+4) | 18 (+4) |

___Amphibious.___ The giant can breathe air and water.

___Innate Spellcasting.___ The giant's innate spellcasting ability is Charisma (spell save DC 17). It can innately cast the following spells, requiring no material components:

* At will: _detect magic, feather fall, levitate, light_

* 3/day each: _control weather, water breathing_

**Actions**

___Multiattack.___ The giant makes two greatsword attacks.

___Greatsword.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target. Hit: 30 (6d6 + 9) slashing damage.

___Rock.___ Ranged Weapon Attack: +14 to hit, range 60/240 ft., one target. Hit: 35 (4d12 + 9) bludgeoning damage.

___Lightning Strike (Recharge 5-6).___ The giant hurls a magical lightning bolt at a point it can see within 500 feet of it. Each creature within 10 feet of that point must make a DC 17 Dexterity saving throw, taking 54 (12d8) lightning damage on a failed save, or half as much damage on a successful one.
