"White Tusk Orc";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Orc"
_skills_: "Intimidation +2"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Dex +2"
_speed_: "30 ft."
_hit points_: "30 (4d8 + 12)"
_armor class_: "14 (studded leather)"
_stats_: | 16 (+3) | 12 (+1) | 16 (+3) | 9 (-1) | 11 (+0) | 10 (+0) |

___Aggressive.___ As a bonus action, the orc can
move up to its speed toward a hostile creature it
can see.

___Minion: Savage Horde.___ After moving at least 20
feet in a straight line toward a creature, the next
attack the orc makes against that creature scores
a critical hit on a roll of 18–20.


**Actions**

___Battleaxe.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing
damage, or 14 (2d10 + 3) slashing damage if used
with two hands.

___Javelin.___ Melee or Ranged Weapon Attack: +5 to
hit, reach 5 ft. or range 30/120 ft., one target. Hit:
10 (2d6 + 3) piercing damage.
