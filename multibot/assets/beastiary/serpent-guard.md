"Serpent Guard";;;_size_: Medium humanoid (human)
_alignment_: lawful neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Draconic"
_skills_: "Athletics +7, Perception +5, Survival +7"
_senses_: "passive Perception 15"
_saving_throws_: "Str +7, Dex 5, Con +7, Wis +5"
_damage_resistances_: "poison"
_speed_: "30 ft."
_hit points_: "76 (9d8 + 36)"
_armor class_: "18 (breastplate, shield)"
_stats_: | 18 (+4) | 14 (+2) | 18 (+4) | 9 (-1) | 14 (+2) | 12 (+1) |

___Capitalize (1/Turn).___ If the Amazon hits a creature
that she can see with a melee weapon attack, she can
use her bonus action to immediately make another
melee weapon attack against the same creature. This
extra attack has disadvantage.

___Fear of Magic.___ If a creature casts a spell or uses another
magical effect within 30 feet of the Amazon and the
Amazon can see it, the Amazon must succeed on a
Wisdom saving throw with a DC equal to the
spellcaster's spell save DC. On a failed saving throw,
the Amazon is frightened of the spellcaster for 1
minute. The Amazon can repeat her saving throw at the
end of each of her turns, ending the frightened effect
on a success. If the Amazon succeeds on her initial
saving throw or the effect ends for her, this trait does
not function for 1 hour.

___Serpent Whisperer.___ Through sounds and gestures, the
Amazon can communicate simple ideas with snakes
and other serpents.

___Shield Guardian.___ When the Amazon takes the Dodge
action and she is wielding a shield, she gains a +3
bonus to her AC.

**Actions**

___Multiattack.___ The Amazon makes three attacks with her
tepoztopilli.

___Tepoztopilli.___ Melee Weapon Attack: +7 to hit, reach 10
ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage, or
9 (1d10 + 4) bludgeoning when wielded with two
hands.

___Sling.___ Ranged Weapon Attack: +5 to hit, range 30/120
ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Shield Bash.___ Melee Weapon Attack: +7 to hit, reach 5
ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.
If the target is a Medium or smaller creature, it must
succeed on a DC 15 Strength saving throw or be
knocked prone.
