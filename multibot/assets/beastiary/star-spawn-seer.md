"Star Spawn Seer";;;_page_number_: 236
_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Deep Speech, Undercommon"
_senses_: "darkvision 60 ft., passive Perception 19"
_skills_: "Perception +9"
_damage_immunities_: "psychic"
_saving_throws_: "Dex +6, Int +11, Wis +9, Cha +8"
_speed_: "30 ft."
_hit points_: "153  (18d8 + 72)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, frightened"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 14 (+2) | 12 (+1) | 18 (+4) | 22 (+6) | 19 (+4) | 16 (+3) |

___Out-of-Phase Movement.___ The seer can move through other creatures and objects as if they were difficult terrain. Each creature it moves through takes 5 (1d10) psychic damage; no creature can take this damage more than once per turn. The seer takes 5 (1d10) force damage if it ends its turn inside an object.

**Actions**

___Multiattack___ The seer makes two comet staff attacks or uses Psychic Orb twice.

___Comet Staff___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 9 (1d6 + 6) bludgeoning damage plus 18 (4d8) psychic damage, or 10 (1d8 + 6) bludgeoning damage plus 18 (4d8) psychic damage, if used with two hands, and the target must succeed on a DC 19 Constitution saving throw or be incapacitated until the end of its next turn.

___Psychic Orb___ Ranged Spell Attack: +11 to hit, range 120 feet, one target. Hit: 27 (5d10) psychic damage.

___Collapse Distance (Recharge 6)___ The seer warps space around a creature it can see within 30 feet of it. That creature must make a DC 19 Wisdom saving throw. On a failed save, the target, along with any equipment it is wearing or carrying, is magically teleported up to 60 feet to an unoccupied space the seer can see, and all other creatures within 10 feet of the target's original space each takes 39 (6d12) psychic damage. On a successful save, the target takes 19 (3d12) psychic damage.