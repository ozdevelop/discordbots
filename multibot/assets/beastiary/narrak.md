"Narrak";;;_size_: Small humanoid (derro)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Arcana +4, Stealth +4"
_speed_: "30 ft."
_hit points_: "40 (9d6+9)"
_armor class_: "12 (15 to mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 13 (+1) | 14 (+2) | 5 (-3) | 16 (+3) |

___Insanity.___ Droki has advantage on saving throws against being charmed or frightened.

___Magic Resistance.___ The derro has advantage on saving throws against spells and other magical effects.

___Spellcasting.___ Narrak is a 5th-level spellcaster. His spellcasting ability is Charisma (Save DC 13, +5 to hit with spell attacks) Narrak has two 2nd-level spell slots, which he regains after finishing a short or long rest, and knows the following Warlock spells:

* Cantrips (at will): _eldritch blast, friends, poison spray_

* 1st level: _armor of Agathys, charm person, hex_

* 2nd level: _hold person, ray of enfeeblement, spider climb_

___Sunlight Sensitivity.___ While in sunlight, the derro has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Armor of Shadows (Recharges after a Short or Long Rest).___ Narrak casts _mage armor_ on himself

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit:5 (1d6 + 2) piercing damage.

**Reactions**

___One with Shadows.___ While he is in a dim light or darkness, Narrak can become invisible. He remains so until he moves or takes an action or reaction.
