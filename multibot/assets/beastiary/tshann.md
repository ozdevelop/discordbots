"T'shann";;;_size_: Small aberration
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 10"
_speed_: "10 ft., burrow 10 ft."
_hit points_: "32 (5d6 + 15)"
_armor class_: "8 (natural armor)"
_stats_: | 10 (+0) | 4 (-3) | 16 (+3) | 2 (-4) | 10 (+0) | 12 (+1) |

___Acidic Secretions.___ A creature who touches the t’shann takes 5 (2d4)
acid damage. Any nonmagical weapon made of metal or wood that hits
the t’shann corrodes. After dealing damage, the weapon takes a permanent
and cumulative –1 penalty to damage rolls. If its penalty drops to –5, the
weapon is destroyed. Nonmagical ammunition made of metal or wood
that hits the t’shann is destroyed after dealing damage.
In addition, nonmagical armor worn by the target of any of the t’shann’s
attacks is partly dissolved and takes a permanent and cumulative –1
penalty to the AC it offers. The armor is destroyed if the penalty reduces
its AC to 10.

___Alien Thoughts.___ When a creature enters or starts its turn within 30 feet
of the t’shann, the creature must make a DC 13 Wisdom saving throw,
unless the t’shann is incapacitated. On a failed saving throw, the creature
can’t take reactions until the start of its next turn and rolls a d8 to determine
what it does during that turn. On a 1–4, the creature does nothing. On a 5
or 6, the creature takes no action but uses all its movement to move in a
random direction. On a 7 or 8, the creature makes one melee attack against
a random creature, or it does nothing if no creature is within reach.

**Actions**

___Slam.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2
(1d4) bludgeoning damage plus 5 (2d4) acid damage.

___Spew Acid.___ Ranged Weapon Attack: +2 to hit, range 10 ft., one target.
Hit: 5 (2d4) acid damage.
