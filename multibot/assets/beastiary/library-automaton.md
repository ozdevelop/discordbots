"Library Automaton";;;_size_: Small construct
_alignment_: lawful neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Machine Speech"
_skills_: "History +4, Investigation +4"
_senses_: "blindsight 60 ft., truesight 10 ft., passive Perception 11"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, poisoned"
_speed_: "30 ft."
_hit points_: "7 (2d6)"
_armor class_: "13 (natural armor)"
_stats_: | 8 (-1) | 13 (+1) | 10 (+0) | 14 (+2) | 12 (+1) | 8 (-1) |

___Extra-Dimensional Book Repository.___ A small door on the chest of the library automaton opens into an extra.dimensional bookcase. This bookcase functions exactly as a bag of holding except that it can store only written materials such as books, scrolls, tomes, parchment, folders, notebooks, spellbooks, and the like.

**Actions**

___Gaze of Confusion.___ The library automaton chooses one creature it can see within 40 feet. The target must succeed on a DC 12 Intelligence saving throw or take 9 (3d4 + 2) psychic damage and have disadvantage on Intelligence-based checks, saving throws, and attacks until the end of its next turn. If the saving throw succeeds, then the target takes half damage and suffers no other effect.

___Bibliotelekinesis.___ This ability functions as the cantrip _mage hand_ but can be used only on books, scrolls, maps, and other printed or written materials.

