"Hobgoblin Iron Shadow";;;_size_: Medium humanoid (goblinoid)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_skills_: "Acrobatics +5, Athletics +4, Stealth +5"
_speed_: "40 ft."
_hit points_: "32 (5d8+10)"
_armor class_: "15"
_stats_: | 14 (+2) | 16 (+3) | 15 (+2) | 14 (+2) | 15 (+2) | 11 (0) |

___Spellcasting.___ The hobgoblin is a 2nd-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 12, +4 to hit with spell attacks). It has the following wizard spells prepared:

* Cantrips (at will): _minor illusion, prestidigitation, true strike_

* 1st level (3 slots): _charm person, disguise self, expeditious retreat, silent image_

___Unarmored Defense.___ While the hobgoblin is wearing no armor and wielding no shield, its AC includes its Wisdom modifier.

**Actions**

___Multiattack.___ The hobgoblin makes four attacks, each of which can be an unarmed strike or a dart attack. It can also use Shadow Jaunt once, either before or after one of the attacks.

___Unarmed Strike.___ Melee Weapon Attack: +5 to hit, reach 5 it, one target. Hit: 5 (1d4+3) bludgeoning damage.

___Dart.___ Ranged Weapon Attack: +5 to hit, range 20/60 it, one target. Hit: 5 (1d4+3) piercing damage.

___Shadow Jaunt.___ The hobgoblin magically teleports, along with any equipment it is wearing or carrying, up to 30 feet to an unoccupied space it can see. Both the space it is leaving and its destination must be in dim light or darkness.
