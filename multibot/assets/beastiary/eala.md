"Eala";;;_size_: Small monstrosity
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +5"
_damage_immunities_: "fire"
_speed_: "10 ft., fly, 60 ft."
_hit points_: "40 (9d6 + 9)"
_armor class_: "15 (natural)"
_stats_: | 10 (+0) | 16 (+3) | 12 (+1) | 2 (-4) | 12 (+1) | 16 (+3) |

**Actions**

___Multiattack.___ The eala makes two attacks with its wing blades.

___Wing Blades.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage.

___Fire Breath (recharge 5-6).___ The eala breathes fire in a 20-foot cone. Every creature in the area must make a DC 11 Dexterity saving throw, taking 10 (3d6) fire damage on a failed save or half as much on a successful one. The eala's fire breath ignites flammable objects and melts soft metals in the area that aren't being worn or carried.

**Reactions**

___Swan Song.___ When the eala is reduced to 0 hit points, it can use its last breath sing a plaintive and beautiful melody. Creatures within 20 feet that can hear the eala must succeed on a DC 13 Charisma saving throw or be incapacitated for 1 round. A creature incapacitated in this way has its speed reduced to 0.

