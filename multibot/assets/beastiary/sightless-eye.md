"Sightless Eye";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Deep Speech"
_skills_: "Perception +4"
_senses_: "darkvision 120 ft., truesight 60 ft., passive Perception 14"
_speed_: "30 ft."
_hit points_: "117 (18d8 + 36)"
_armor class_: "14"
_stats_: | 14 (+2) | 10 (+0) | 14 (+2) | 12 (+1) | 13 (+1) | 13 (+1) |

___Spider Climb.___ The sightless eye can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Hyper-Awareness.___ A sightless eye's eye stalks allow it to see in all
directions at once. It cannot be surprised.

___Stun Cone.___ A sightless eye’s central eye produces a cone extending
straight ahead from its front to a range of 30 feet. At the start of each
of its turns, the sightless eye decides which way the cone faces and
whether the cone is active. All creatures in this area must succeed on a DC
15 Constitution saving throw or be stunned for 1 minute. A creature can
repeat the saving throw at the end of each of its turns, ending the effect on
itself on a success.

**Actions**

___Multiattack.___ The sightless eye makes three attacks: one with its bite
and two with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12
(3d6 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit:
15 (3d8 + 2) bludgeoning damage. The target is grappled (escape DC 12)
if the sightless eye isn’t already grappling a creature, and the target is
restrained until the grapple ends.

___Eye Rays.___ The sightless eye creates the following two eye rays. The sightless eye can aim both of its eye rays in any direction and they have a range of 150 feet.

1. _Paralytic Ray._ The sightless eye unleashes a
powerful paralytic beam. The target must make a DC 15 Wisdom saving
throw or be paralyzed for 1 minute. A creature can repeat the saving throw
at the end of each of its turns, ending the effect on itself on a success.

2. _Enfeeblement Ray._ The sightless eye unleashes
a powerful ray of enfeeblement. The target must make a DC 15 Wisdom
saving throw or deal half damage with all attacks that use Strength for 1
minute. A creature can repeat the saving throw at the end of each of its
turns, ending the effect on itself on a success.
