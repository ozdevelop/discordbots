"Zanskaran Viper";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "30 ft., climb 10 ft., swim 30 ft."
_hit points_: "38 (4d10 + 16)"
_armor class_: "14 (natural armor)"
_stats_: | 12 (+1) | 11 (+0) | 18 (+4) | 2 (-4) | 13 (+1) | 2 (-4) |

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 10 (2d8 + 1) piercing damage, and the target must make a successful DC 14 Constitution saving throw or become poisoned. While poisoned this way, the target is blind and takes 7 (2d6) poison damage at the start of each of its turns. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself with a success.

