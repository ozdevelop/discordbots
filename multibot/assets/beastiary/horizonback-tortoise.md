"Horizonback Tortoise";;;_size_: Gargantuan monstrosity
_alignment_: unaligned
_challenge_: "7 (3,900 XP)"
_languages_: "understands Goblin but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Str +12, Con +10"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "20 ft."
_hit points_: "227 (13d20 + 91)"
_armor class_: "17 (natural armor), 22 while in its shell"
_stats_: | 28 (+9) | 3 (-4) | 25 (+7) | 4 (-3) | 10 (+0) | 5 (-3) |

___Amphibious.___ The tortoise can breathe air and water.

___Massive Frame.___ The tortoise can carry up to 20,000 pounds of weight atop its shell, but moves at half speed if the weight exceeds 10,000 pounds. Medium or smaller creatures can move underneath the tortoise while it’s not prone.

Any creature under the tortoise when it falls prone is grappled (escape DC 18). Until the grapple ends, the creature is prone and restrained.

**Actions**

___Bite.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 28 (3d12 + 9) bludgeoning damage.

___Shell Defense (Recharge 4–6).___ The tortoise withdraws into its shell, falls prone, and gains a +5 bonus to AC. While the tortoise is in its shell, its speed is 0 and can’t increase. The tortoise can emerge from its shell as an action, whereupon it is no longer prone.

