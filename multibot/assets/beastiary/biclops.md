"Biclops";;;_size_: Huge giant
_alignment_: neutral
_challenge_: "14 (11,500 XP)"
_languages_: "Giant"
_skills_: "Athletics +15, Perception +7"
_senses_: "passive Perception 17"
_saving_throws_: "Dex +5, Con +12, Wis +7"
_speed_: "30 ft."
_hit points_: "283 (21d12 + 147)"
_armor class_: "16 (chainmail)"
_stats_: | 26 (+8) | 10 (+0) | 25 (+7) | 10 (+0) | 14 (+2) | 10 (+0) |

___Improved Critical.___ Longsword and spear attacks score a critical hit on
a roll of 19 or 20.

___Two-Weapon Master.___ When attacking with two weapons, a biclops
may reroll any 1 on a damage dice, keeping the second result.

___Two Heads.___ The biclops has advantage on Wisdom (Perception)
checks and on saving throws against being blinded, charmed, deafened,
frightened, stunned, and knocked unconscious.

___Wakeful.___ When one head of the biclops is asleep, the other head is
awake.

**Actions**

___Multiattack.___ The biclops makes three attacks with its two longswords.
It can choose to hurl two spears or throw two rocks instead.

___Longsword.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target.
Hit: 26 (4d8 + 8) slashing damage.

___Spear.___ Melee Weapon Attack: +13 to hit, reach 15 ft. or range 20/60 ft.,
one target. Hit: 22 (4d6 + 8) piercing damage.

___Rock.___ Ranged Weapon Attack: +13 to hit, range 30/120 ft., one target.
Hit: 30 (4d10 + 8) bludgeoning damage.
