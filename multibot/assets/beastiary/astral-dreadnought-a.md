"Astral Dreadnought (A)";;;_page_number_: 117
_size_: Gargantuan titan
_alignment_: unaligned
_challenge_: "21 (33,000 XP)"
_languages_: "
_stats_: | 28 (+9) | 7 (-2) | 25 (+7) | 5 (-3) | 14 (+2) | 18 (+4) |"
_senses_: "Darkvision 120 ft., passive Perception 19"
_skills_: "Perception +9"
_saving_throws_: "Dex +5, Wis +9"
_speed_: "15 ft., fly 80 ft. (hover)"
_hit points_: "297 (17d20+119)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "Charmed, exhaustion, frightened, paralyzed, petrified, poisoned, prone, stunned"