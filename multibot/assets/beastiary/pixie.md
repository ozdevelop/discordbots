"Pixie";;;_size_: Tiny fey
_alignment_: neutral good
_challenge_: "1/4 (50 XP)"
_languages_: "Sylvan"
_skills_: "Perception +4, Stealth +7"
_speed_: "10 ft., fly 30 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "15"
_stats_: | 2 (-4) | 20 (+5) | 8 (-1) | 10 (0) | 14 (+2) | 15 (+2) |

___Magic Resistance.___ The pixie has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The pixie's innate spellcasting ability is Charisma (spell save DC 12). It can innately cast the following spells, requiring only its pixie dust as a component:

* At will: _druidcraft_

* 1/day each: _confusion, dancing lights, detect evil and good, detect thoughts, dispel magic, entangle, fly, phantasmal force, polymorph, sleep_

**Actions**

___Superior Invisibility.___ The pixie magically turns invisible until its concentration ends (as if concentrating on a spell). Any equipment the pixie wears or carries is invisible with it.
