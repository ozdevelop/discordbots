"Bone Crawler";;;_size_: Huge aberration
_alignment_: neutral
_challenge_: "13 (10,000 XP)"
_languages_: "Deep Speech"
_senses_: "blindsight 60 ft., passive Perception 13"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., climb 10 ft."
_hit points_: "161 (14d12 + 70)"
_armor class_: "16 (natural armor)"
_stats_: | 23 (+6) | 16 (+3) | 20 (+5) | 9 (-1) | 15 (+3) | 9 (-1) |

___Improved Critical.___ The bone crawler’s bone blade attacks score a
critical hit on a roll of 19 or 20.

___Spiked Bone Armor.___ The bone crawler is encased in a shell of ironhard
bones that can absorb 150 hit points worth of damage. Against area
of effect attacks, the bone crawler’s bone armor will still take damage
regardless of whether the bone crawler succeeded on the save. If the bone
crawler succeeds on the save, the armor takes half the total damage dealt
by the attack. If the bone crawler fails its saving throw, the armor takes
double damage from the area effect.

For every 10 hit points of damage the armor takes, the armor gains an
additional bone spike. A creature that starts its turn within 5 ft. of the bone
crawler takes damage due to the bone spikes. For every bone spike the
armor has accumulated, the adjacent creatures take 1d4 piercing damage.
The bone crawler can repair its armor over a 24 hour period by absorbing
additional bones into its hulking mass. As the bone crawler absorbs bones,
it secretes a substance that will harden, coating the bones, and create the
iron-hard shell. For every 3 hit points of bone absorbed by the crawler, 1
hit point of damage is repaired on the armor. For example, if the crawler
absorbs the skeleton of a creature that had 75 hit points it would repair 25
hit points of damage on its armor.

___False Appearance.___ While motionless, a bone crawler is indistinguishable
from a mound of bones.


**Actions**

___Multiattack.___ The crawler can make up to 6 attacks using its bone blades
and bone whips.

___Bone Blade.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target.
Hit: 17 (1d10 + 8) slashing damage.

___Bone Whip.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target.
Hit: 11 (1d6 + 8) bludgeoning damage.

___Whirling Frenzy (Recharge 5–6).___ The bone crawler spins its bone
blades in a swirling storm of sharpened edges. Creatures within 10 feet
of the crawler must succeed on a DC 15 Dexterity saving throw, taking
15 (2d8 + 6) slashing damage on a failure, or half as much damage on a
successful one.
