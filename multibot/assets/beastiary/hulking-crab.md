"Hulking Crab";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft."
_skills_: "Stealth +2"
_speed_: "20 ft., swim 30 ft."
_hit points_: "76 (8d12+24)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 8 (-1) | 16 (+3) | 3 (-4) | 11 (0) | 3 (-4) |

___Amphibious.___ The crab can breathe air and water.

___Shell Camouflage.___ While the crab remains motionless with its eyestalks and pincers tucked close to it's body, it resembles a natural formation or a pile of detritus. A creature within 30 feet of it can discren its true nature with a successful DC 15 intelligence (Nature) check.

**Actions**

___Multiattack.___ The crab makes two attacks with its claws.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 20 (3d10+4) bludgeoning damage, and the target is grappled (escape DC 15). The crab has two claws, each of which can grapple only one target.
