"Heart-Piercer Manticore";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "50 ft."
_hit points_: "136 (16d10 + 48)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 16 (+3) | 5 (-3) | 12 (+1) | 6 (-2) |

**Actions**

___Multiattack.___ The manticore makes two attacks, one with its bite and
one with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) slashing damage.

___Stinger.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target.
Hit: 11 (2d6 + 4) piercing damage. The target must make a DC 14
Constitution saving throw, taking 24 (7d6) poison damage on a
failed save, or half as much damage on a successful one.
