"Urochar (Strangling Watcher)";;;_size_: Huge aberration
_alignment_: chaotic evil
_challenge_: "17 (18000 XP)"
_languages_: "understands Darakhul and Void Speech"
_skills_: "Perception +8, Stealth +8"
_senses_: "truesight 120 ft., passive Perception 19"
_saving_throws_: "Dex +8, Con +13, Wis +9, Cha +11"
_damage_immunities_: "thunder"
_damage_resistances_: "cold, lightning; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "frightened"
_speed_: "40 ft., climb 20 ft."
_hit points_: "256 (19d12 + 133)"
_armor class_: "19 (natural armor)"
_stats_: | 24 (+7) | 15 (+2) | 24 (+7) | 14 (+2) | 14 (+2) | 20 (+5) |

___Death Throes.___ When a strangling watcher dies, it releases all the fear it consumed in its lifetime in a single, soul-rending wave. All creatures within 60 feet of it must succeed on a DC 19 Charisma saving throw or become frightened. A frightened creature takes 13 (2d12) psychic damage at the start of each of its turns from the centuries of accumulated dread. It can repeat the Charisma saving throw at the end of each of its turns, ending the effect on a success.

___Innate Spellcasting.___ The watcher's innate spellcasting ability is Charisma (spell save DC 19). It can cast the following spells, requiring no material components:

At will: feather fall

3/day each: blur, meld into stone, phantasmal killer

1/day each: black tentacles, eyebite, greater invisibility

___Spider Climb.___ The watcher can climb any surface, including upside down on ceilings, without making an ability check.

___Squeeze.___ Despite their size, strangling watchers have slender, boneless bodies, enabling them to squeeze through passages only a Small-sized creature could fit through, without affecting their movement or combat capabilities.

**Actions**

___Multiattack.___ The watcher makes four attacks with its tentacles.

___Tentacle.___ Melee Weapon Attack: +13 to hit, reach 20 ft., one target. Hit: 20 (3d8 + 7) bludgeoning damage, and the target is grappled (escape DC 17). Until this grapple ends, the target is restrained. Each of its four tentacles can grapple one target.

___Paralyzing Gaze (Recharge 5-6).___ The watcher can target one creature within 60 feet with its eerie gaze. The target must succeed on a DC 19 Wisdom saving throw or become paralyzed for 1 minute. The paralyzed target can repeat the saving throw at the end of each of its turns, ending the effect on a success. If a target's saving throw is successful or the effect ends for it, the target is immune to the watcher's gaze for the next 24 hours.

**Legendary** Actions

___The urochar can take 3 legendary actions, choosing from the options below.___ Only one option can be used at a time and only at the end of another creature's turn. The strangling watcher regains spent legendary actions at the start of its turn.

___Crush Attack.___ The urochar crushes one creature grappled by its tentacle. The target takes 25 (4d8 + 7) bludgeoning damage.

___Tentacle Attack.___ The watcher makes one tentacle attack.

___Tentacle Leap (Costs 2 Actions).___ Using a tentacle, the urochar moves up to 20 feet to an unoccupied space adjacent to a wall, ceiling, floor, or other solid surface. This move doesn't trigger reactions. The urochar must have at least one tentacle free (not grappling a creature) to use this action. Grappled creatures move with the urochar.

