"Mule";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_speed_: "40 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "10"
_stats_: | 14 (+2) | 10 (0) | 13 (+1) | 2 (-4) | 10 (0) | 5 (-3) |

___Beast of Burden.___ The mule is considered to be a Large animal for the purpose of determining its carrying capacity.

___Sure-Footed.___ The mule has advantage on Strength and Dexterity saving throws made against effects that would knock it prone.

**Actions**

___Hooves.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) bludgeoning damage.