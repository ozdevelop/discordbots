"Elemental Defender";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "passive Perception 10"
_damage_resistances_: "poison, See <i>Elemental Adaptation</i>"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "65 (7d10 + 28)"
_armor class_: "16 (natural armor, shield)"
_stats_: | 16 (+3) | 8 (-1) | 18 (+4) | 1 (-5) | 10 (+0) | 4 (-3) |

___Elemental Adaptation.___ Each defender is given life
from a crystal charged with elemental power. They
start off with resistance to either cold, fire, or
lightning damage and deal additional damage of the
same type. Whenever the defender is damaged by
another of these elements, it may use its reaction
to shift its elemental properties, changing its
resistance and extra damage to the new type
instead as well as gaining 10 temporary hit points.
The crystal in its chest changes color to reflect this
shift.

___Panic Mode.___ When the defender is reduced below
15 hit points, it enters a panic mode. While in this
mode, the defender makes an additional longsword
attack on each of its turns. The defender must also
make a DC 10 Constitution saving throw at the end
of each of its turns. On a failure, the crystal in its
chest cannot handle the extra stress and explodes,
shutting down the defender.

**Actions**

___Multiattack.___ The defender makes two attacks with
its longsword.

___Longsword.___ Melee Weapon Attack: +5 to hit, reach
5ft., one target. Hit: 7 (1d8 + 3) slashing damage,
or 8 (1d10 + 3) slashing damage if being wielded
with two hands, plus 3 (1d6) elemental damage.
