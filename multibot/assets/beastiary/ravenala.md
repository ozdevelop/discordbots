"Ravenala";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "5 (1800 XP)"
_languages_: "Common, Druidic, Elvish, Sylvan"
_senses_: ", passive Perception 13"
_saving_throws_: "Wis +6, Cha +4"
_damage_vulnerabilities_: "fire, cold"
_damage_resistances_: "bludgeoning, piercing"
_condition_immunities_: "blinded, deafened"
_speed_: "30 ft."
_hit points_: "126 (12d10 + 60)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 10 (+0) | 20 (+5) | 12 (+1) | 16 (+3) | 12 (+1) |

___Magic Resistance.___ The ravenala has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The ravenala's innate spellcasting ability is Wisdom (spell save DC 14). It can innately cast the following spells, requiring no material components:

At will: entangle, sleep

1/day each: heal, wall of thorns

___Green Walk.___ The ravenala can move across undergrowth, natural or magical, without needing to make an ability check and without expending additional movement.

**Actions**

___Multiattack.___ The ravenala makes two slam attacks or two bursting pod attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage.

___Bursting Pod.___ Melee Ranged Attack: +8 to hit, range 30/120 ft., one target. Hit: 8 (1d6 + 5) bludgeoning damage, and the target and all creatures within 5 feet of it also take 5 (2d4) piercing damage, or half as much piercing damage with a successful DC 15 Dexterity saving throw.

___Lamenting Engulfment.___ The ravenala targets a creature within 5 feet of it. The target must succeed on a DC 13 Dexterity saving throw or be grappled and restrained by the ravenala. While restrained, the creature is engulfed inside the ravenala's trunk. The ravenala can grapple one creature at a time; grappling doesn't prevent it from using other attacks against different targets. The restrained creature must make a DC 14 Wisdom saving throw at the start of each of its turns. On a failure, the creature is compelled to sing a lament of all its various mistakes and misdeeds for as long as it remains restrained. Singing prevents uttering command words, casting spells with a verbal component, or any verbal communication. The restrained creature can still make melee attacks. When the ravenala moves, the restrained creature moves with it. A restrained creature can escape by using an action to make a DC 15 Strength check. On a success, the creature escapes and enters a space of its choice within 5 feet of the ravenala.

