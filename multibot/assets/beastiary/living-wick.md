"Living Wick";;;_size_: Small construct
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "shares a telepathic link with the individual that lit its wick"
_senses_: "sight 20 ft. (blind beyond the radius of its own light), passive Perception 10"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned, unconscious"
_speed_: "20 ft."
_hit points_: "28 (8d6)"
_armor class_: "13 (natural armor)"
_stats_: | 10 (+0) | 10 (+0) | 10 (+0) | 5 (-3) | 5 (-3) | 5 (-3) |

___Controlled.___ Living wicks cannot move, attack, or perform actions when they are not lit. Living wicks only respond to the telepathic commands of the individual that lit them.

___Light.___ Activated living wicks produce light as a torch.

___Melting.___ A living wick loses one hit point for every 24 hours it remains lit.

**Actions**

___Slam.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 3 (1d6) bludgeoning damage.

___Consume Self.___ A living wick can be commanded to rapidly burn through the remains of its wick, creating a devastating fireball. All creatures within 20 feet of the living wick take 7 (2d6) fire damage, or half damage with a successful DC 13 Dexterity saving throw. The fire spreads around corners and ignites flammable objects in the area that aren't being worn or carried. The wick is reduced to a lifeless puddle of wax.

