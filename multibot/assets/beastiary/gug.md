"Gug";;;_size_: Huge giant
_alignment_: neutral evil
_challenge_: "12 (8400 XP)"
_languages_: "Deep Speech, Giant, Undercommon"
_skills_: "Athletics +11, Perception +3, Stealth +4"
_senses_: "darkvision 240 ft., passive Perception 13"
_saving_throws_: "Str +12, Dex +4, Con +11, Cha +6"
_damage_immunities_: "poison"
_condition_immunities_: "confusion, exhaustion, paralysis, poisoned"
_speed_: "40 ft."
_hit points_: "270 (20d12 + 140)"
_armor class_: "17 (natural armor)"
_stats_: | 24 (+7) | 10 (+0) | 25 (+7) | 10 (+0) | 8 (-1) | 14 (+2) |

___Towering Strength.___ A gug can lift items up to 4,000 pounds as a bonus action.

**Actions**

___Multiattack.___ The gug makes two slam attacks, two stomp attacks, or one of each.

___Slam.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one creature. Hit: 16 (2d8 + 7) bludgeoning damage. If a creature is hit by this attack twice in the same turn, the target must make a successful DC 19 Constitution saving throw or gain one level of exhaustion.

___Stomp.___ Melee Weapon Attack. +11 to hit, reach 10 ft. Hit: 20 (2d12 + 7) bludgeoning damage.

**Legendary** Actions

___A gug can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. A gug regains spent legendary actions at the start of its turn.

___Move.___ The gug moves up to half its speed.

___Attack.___ The gug makes one slam or stomp attack.

___Grab.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: the target is grappled (escape DC 17).

___Swallow.___ The gug swallows one creature it has grappled. The creature takes 26 (3d12 + 7) bludgeoning damage immediately plus 13 (2d12) acid damage at the start of each of the gug's turns. A swallowed creature is no longer grappled but is blinded and restrained, and has total cover against attacks and other effects from outside the gug. If the gug takes 75 points of damage in a single turn, the swallowed creature is expelled and falls prone next to the gug. When the gug dies, a swallowed creature can crawl from the corpse by using 10 feet of movement.

___Throw.___ The gug throws one creature it has grappled. The creature is thrown a distance of 2d4 times 10 feet in the direction the gug chooses, and takes 20 (2d12 + 7) bludgeoning damage (plus falling damage if they are thrown into a chasm or off a cliff). A gug can throw a creature up to Large size. Small creatures are thrown twice as far, but the damage is the same.

