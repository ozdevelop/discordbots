"Slithering Tracker";;;_size_: Medium ooze
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "understands languages it knew in its previous form but can't speak"
_senses_: "blindsight 120 ft."
_skills_: "Stealth +8"
_speed_: "30 ft., climb 30 ft., swim 30 ft."
_hit points_: "32 (5d8+10)"
_armor class_: "14"
_damage_vulnerabilities_: "cold, fire"
_condition_immunities_: "blinded, deafened, exhaustion, grappled, paralyzed, petrified, prone, restrained, unconscious"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 16 (+3) | 19 (+4) | 15 (+2) | 10 (0) | 14 (+2) | 11 (0) |

___Ambusher.___ In the first round of a combat, the slithering tracker has advantage on attack rolls against any creature it surprised.

___Damage Transfer.___ While grappling a creature, the slithering tracker takes only haIf the damage dealt to it, and the creature it is grappling takes the other half.

___False Appearance.___ While the slithering tracker remains motionless, it is indistinguishable from a puddle, unless an observer succeeds on a DC 18 Intelligence (Investigation) check.

___Keen Tracker.___ The slithering tracker has advantage on Wisdom checks to track prey.

___Liquid Form.___ The slithering tracker can enter an enemy's space and stop there. It can also move through a space as narrow as 1 inch wide without squeezing.

___Spider Climb.___ The slithering tracker can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Watery Stealth.___ While underwater, the slithering tracker has advantage on Dexterity (Stealth) checks made to hide, and it can take the Hide action as a bonus action.

**Actions**

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10+3) bludgeoning damage.

___Life Leech.___ One Large or smaller creature that the slithering tracker can see within 5 feet of it must succeed on a DC 13 Dexterity saving throw or be grappled (escape DC 13). Until this grapple ends, the target is restrained and unable to breathe unless it can breathe water. In addition, the grappled target takes 16 (3d10) necrotic damage at the start of each of its turns. The slithering tracker can grapple only one target at a time.