"Minotaur Rampager";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal"
_skills_: "Athletics +9, Perception +9"
_senses_: "darkvision 60ft., passive Perception 19"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons (if at or above half of its maximum hit points)"
_speed_: "40 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "15 (natural armor)"
_stats_: | 21 (+5) | 10 (+0) | 19 (+4) | 9 (-1) | 17 (+3) | 12 (+1) |

___Labyrinthine Recall.___ The minotaur can perfectly recall
any path it has traveled.

___Endless Fury.___ The minotaur's rage makes it blind to
the pain being inflicted upon it until it feels the
possibility of death is a reality, at which point it
uses a new surge of adrenaline to bring a new
deadly strength to every strike. As long as the
minotaur is at or above half of its maximum hit
points, it has resistance to bludgeoning, piercing,
and slashing damage from nonmagical weapons. As
long as the minotaur is below half of maximum hit
points, its strength bonus is doubled for all of its
attacks (included in the attacks).

**Actions**

___Greatmaul.___ Melee Weapon Attack: +8 to hit, reach
10 ft., one target. Hit: 18 (2d12 + 5) bludgeoning
damage, or 23 (2d12 + 10) if the minotaur is
below half of its maximum hit points.

___Hoof Stomp.___ Melee Weapon Attack: +8 to hit, reach
5 ft., one prone creature. Hit: 24 (3d12 + 5)
bludgeoning damage or 29 (3d12 + 10) if the
minotaur is below half of its maximum hit points..

___Rampaging Charge (Recharge 5-6).___ The minotaur
expends all of its movement to charge up to 40
feet in a straight line. Each creature in the
minotaur's path must succeed on a DC 16
Dexterity saving throw or be pushed up to 10 feet
to either side of the minotaur's path, knocked
prone, and take 32 (5d12) bludgeoning damage. A
creature that succeeds on the saving throw takes
half as much damage and is pushed but not
knocked prone. This movement by the minotaur
does not provoke attacks of opportunity.
