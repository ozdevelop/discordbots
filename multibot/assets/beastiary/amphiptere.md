"Amphiptere";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: "blindsight 10 ft., passive Perception 15"
_speed_: "20 ft., climb 20 ft., fly 60 ft., swim 20 ft."
_hit points_: "60 (8d8 + 24)"
_armor class_: "15 (natural armor)"
_stats_: | 11 (+0) | 18 (+4) | 17 (+3) | 2 (-4) | 16 (+3) | 6 (-2) |

___Flyby.___ The amphiptere doesn't provoke an opportunity attack when it flies out of an enemy's reach.

___Swarming.___ Up to two amphipteres can share the same space at the same time. The amphiptere has advantage on melee attack rolls if it is sharing its space with another amphiptere that isn't incapacitated.

**Actions**

___Multiattack.___ The amphiptere makes one bite attack and one stinger attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 7 (1d6 + 4) piercing damage.

___Stinger.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 7 (1d6 + 4) piercing damage plus 10 (3d6) poison damage, and the target must make a successful DC 13 Constitution saving throw or be poisoned for 1 hour.

