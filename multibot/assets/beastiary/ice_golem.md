"Ice Golem";;;_size_: Medium construct
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "cold, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "71 (11d8 + 22)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 14 (+2) | 2 (-4) | 10 (+0) | 1 (-5) |

___Icy Destruction.___ When the golem dies, it shatters in an explosion of
jagged ice shards, and each creature within 15 feet of it must make a DC
13 Dexterity saving throw, taking 10 (3d6) piercing damage and 7 (2d6)
cold damage on a failed save, or half as much damage on a successful one.

___Immutable Form.___ The golem is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The golem’s weapon attacks are magical.

**Actions**

___Multiattack.___ The golem can make two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 10
(2d6 + 3) bludgeoning damage plus 7 (2d6) cold damage.

___Cold Breath (Recharge 5–6).___ The golem exhales a blast of freezing
wind in a 15-foot cone. Each creature in that area must make a DC 13
Dexterity saving throw, taking 18 (4d8) cold damage on a failed save, or
half as much damage on a successful one.
