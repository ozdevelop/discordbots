"Drow Elite Warrior";;;_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +4, Stealth +10"
_saving_throws_: "Dex +7, Con +5, Wis +4"
_speed_: "30 ft."
_hit points_: "71 (11d8+22)"
_armor class_: "18 (studded leather, shield)"
_stats_: | 13 (+1) | 18 (+4) | 14 (+2) | 11 (0) | 13 (+1) | 12 (+1) |

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's spellcasting ability is Charisma (spell save DC 12). It can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire, levitate _(self only)

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The drow makes two shortsword attacks.

___Shortsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 10 (3d6) poison damage.

___Hand Crossbow.___ Ranged Weapon Attack: +7 to hit, range 30/120 ft., one target. Hit: 7 (1d6 + 4) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour. If the saving throw fails by 5 or more, the target is also unconscious while poisoned in this way. The target wakes up if it takes damage or if another creature takes an action to shake it awake.

**Reactions**

___Parry.___ The drow adds 3 to its AC against one melee attack that would hit it. To do so, the drow must see the attacker and be wielding a melee weapon.
