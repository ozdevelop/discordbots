"Stygian Fat-Tailed Scorpion";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft., climb 20 ft."
_hit points_: "10 (4d4)"
_armor class_: "14 (natural armor)"
_stats_: | 3 (-4) | 16 (+3) | 10 (+0) | 1 (-5) | 10 (+0) | 2 (-4) |

**Actions**

___Multiattack.___ The scorpion makes three attacks: two with its claws and one with its sting.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 bludgeoning damage.

___Sting.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 piercing damage, plus 21 (6d6) poison damage and the target is poisoned until it completes a short or long rest. A successful DC 10 Constitution saving throw reduces the poison damage to half and prevents the poisoned condition. If the target fails this saving throw while already poisoned, it gains one level of exhaustion in addition to the other effects.

