"Young Remorhaz";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_senses_: "darkvision 60 ft., tremorsense 60 ft."
_damage_immunities_: "cold, fire"
_speed_: "30 ft., burrow 20 ft."
_hit points_: "93 (11d10+33)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 17 (+3) | 3 (-4) | 10 (0) | 4 (-3) |

___Heated Body.___ A creature that touches the remorhaz or hits it with a melee attack while within 5 feet of it takes 7 (2d6) fire damage.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 20 (3d10 + 4) piercing damage plus 7 (2d6) fire damage.