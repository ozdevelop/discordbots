"Bugbear";;;_size_: Medium humanoid (goblinoid)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +6, Survival +2"
_speed_: "30 ft."
_hit points_: "27 (5d8+5)"
_armor class_: "16 (hide armor, shield)"
_stats_: | 15 (+2) | 14 (+2) | 13 (+1) | 8 (-1) | 11 (0) | 9 (-1) |

___Brute.___ A melee weapon deals one extra die of its damage when the bugbear hits with it (included in the attack).

___Surprise Attack.___ If the bugbear surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 7 (2d6) damage from the attack.

**Actions**

___Morningstar.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 11 (2d8 + 2) piercing damage.

___Javelin.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 9 (2d6 + 2) piercing damage in melee or 5 (1d6 + 2) piercing damage at range.