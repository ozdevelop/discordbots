"Rezmir";;;_size_: Medium humanoid (half-black dragon)
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Draconic, Infernal, Giant, Netherese"
_senses_: "blindsight 10 ft., darkvision 120 ft."
_skills_: "Arcana +5, Stealth +9"
_damage_immunities_: "acid"
_saving_throws_: "Dex +6, Wis +4"
_speed_: "30 ft."
_hit points_: "90 (12d8+36)"
_armor class_: "13 (15 with the Black Dragon Mask)"
_condition_immunities_: "charmed, frightened"
_stats_: | 18 (+4) | 16 (+3) | 16 (+3) | 15 (+2) | 12 (+1) | 14 (+2) |

___Special Equipment.___ Rezmir has the Black Dragon Mask, Hazirawn, and an insignia of claws.

___Amphibious.___ Rezmir can breathe air and water.

___Dark Advantage.___ Once per turn, Rezmir can deal an extra 10 (3d6) damage when she hits with a weapon attack, provided Rezmir has advantage on the attack roll.

___Draconic Majesty.___ While wearing no armor and wearing the Black Dragon Mask, Rezmir adds her Charisma bonus to her AC (included).

___Immolation.___ When Rezmir is reduced to 0 hit points, her body disintegrates into a pile of ash.

___Legendary Resistance (1/Day).___ If Rezmir fails a saving throw while wearing the Black Dragon Mask, she can choose to succeed instead.

**Actions**

___Greatsword (Hazirawn).___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage plus 7 (2d6) necrotic damage. If the target is a creature, it can't regain hit points for 1 minute. The target can make a DC 15 Constitution saving throw at the end of each of its turns, ending this effect early on a success.

___Caustic Bolt.___ Ranged Spell Attack: +8 to hit, range 90 ft., one target. Hit: 18 (4d8) acid damage.

___Acid Breath (Recharge 5-6).___ Rezmir breathes acid in a 30-foot line that is 5 feet wide. Each creature in the line must make a DC 14 Dexterity saving throw, taking 22 (5d8) acid damage on a failed save, or half as much damage on a successful one.

**Legendary** Actions

The rezmir can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The rezmir regains spent legendary actions at the start of its turn.

___Darkness (Costs 2 Actions).___ If she is wearing the Black Dragon Mask, Rezmir can take up to two legendary actions between each of her turns, taking the actions all at once or spreading them over the round. A legendary action can be taken only at the start or end of a turn.

A 15-foot radius of magical darkness extends from a point Rezmir can see within 60 feet of her and spreads around corners. The darkness lasts as long as Rezmir maintains concentration, up to 1 minute. A creature with darkvision can't see through this darkness, and no natural light can illuminate it. If any of the area overlaps with a area of light created by a spell of 2nd level or lower, the spell creating the light is dispelled.

___Attack.___ Rezmir makes one melee attack.

___Hide.___ Rezmir takes the Hide action.