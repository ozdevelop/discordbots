"Zimwi";;;_size_: Medium giant
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "Giant"
_skills_: "Perception +1"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "40 ft."
_hit points_: "76 (9d8 + 36)"
_armor class_: "17 (natural armor)"
_stats_: | 13 (+1) | 18 (+4) | 19 (+4) | 6 (-2) | 9 (-1) | 7 (-2) |

**Actions**

___Multiattack.___ The zimwi makes one claws attack and one bite attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage. If the target is a Medium or smaller creature grappled by the zimwi, that creature is swallowed, and the grapple ends. While swallowed, the creature is blinded and restrained, it has total cover against attacks and other effects outside the zimwi, and it takes 14 (4d6) acid damage at the start of each of the zimwi's turns. If the zimwi's stomach takes 20 damage or more on a single turn from a creature inside it, the zimwi must succeed on a DC 14 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 5 feet of the zimwi. Damage done to a zimwi's stomach does not harm the zimwi. The zimwi's stomach is larger on the inside than the outside. It can have two Medium creatures or four Small or smaller creatures swallowed at one time. If the zimwi dies, a swallowed creature is no longer restrained by it and can escape from the corpse using 5 feet of movement, exiting prone.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage, and if the target is a Medium or smaller creature and the zimwi isn't already grappling a creature, it is grappled (escape DC 11).

