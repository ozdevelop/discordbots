"Treacle";;;_size_: Tiny ooze
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_skills_: "Deception +4"
_senses_: "blindsight 60 ft., passive Perception 10"
_speed_: "15 ft., climb 10 ft."
_hit points_: "22 (4d4 + 12)"
_armor class_: "13 (natural armor)"
_stats_: | 4 (-3) | 6 (-2) | 17 (+3) | 1 (-5) | 1 (-5) | 10 (+0) |

___Amorphous.___ The treacle can move through a space as narrow as 1 inch wide without squeezing.

___Charming Presence.___ The treacle has an uncanny ability to sense and to play off of another creature's emotions. It uses Charisma (Deception) to oppose Wisdom (Insight or Perception) skill checks made to see through its ruse, and it has advantage on its check.

**Actions**

___Reshape.___ The treacle assumes the shape of any tiny creature or object. A reshaped treacle gains the movement of its new form but no other special qualities.

___Blood Drain (1/hour).___ A treacle touching the skin of a warm.blooded creature inflicts 4 (1d8) necrotic damage per hour of contact, and the victim's maximum hit points are reduced by the same number. Blood is drained so slowly that the victim doesn't notice the damage unless he or she breaks contact with the treacle (sets it down or hands it to someone else, for example). When contact is broken, the victim notices blood on his or her skin or clothes with a successful DC 13 Wisdom (Perception) check.

