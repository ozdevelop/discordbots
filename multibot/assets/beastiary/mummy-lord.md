"Mummy Lord";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "15 (13,000 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft."
_skills_: "History +5, Religion +5"
_damage_immunities_: "necrotic, poison, bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Con +8, Int +5, Wis +9, Cha +8"
_speed_: "20 ft."
_hit points_: "97 (13d8+39)"
_armor class_: "17 (natural armor)"
_damage_vulnerabilities_: "bludgeoning"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_stats_: | 18 (+4) | 10 (0) | 17 (+3) | 11 (0) | 18 (+4) | 16 (+3) |

___Magic Resistance.___ The mummy lord has advantage on saving throws against spells and other magical effects.

___Rejuvenation.___ A destroyed mummy lord gains a new body in 24 hours if its heart is intact, regaining all its hit points and becoming active again. The new body appears within 5 feet of the mummy lord's heart.

___Spellcasting.___ The mummy lord is a 10th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 17, +9 to hit with spell attacks). The mummy lord has the following cleric spells prepared:

* Cantrips (at will): _sacred flame, thaumaturgy_

* 1st level (4 slots): _command, guiding bolt, shield of faith_

* 2nd level (3 slots): _hold person, silence, spiritual weapon_

* 3rd level (3 slots): _animate dead, dispel magic_

* 4th level (3 slots): _divination, guardian of faith_

* 5th level (2 slots): _contagion, insect plague_

* 6th level (1 slot): _harm_

**Actions**

___Multiattack.___ The mummy can use its Dreadful Glare and makes one attack with its rotting fist.

___Rotting Fist.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 14 (3d6 + 4) bludgeoning damage plus 21 (6d6) necrotic damage. If the target is a creature, it must succeed on a DC 16 Constitution saving throw or be cursed with mummy rot. The cursed target can't regain hit points, and its hit point maximum decreases by 10 (3d6) for every 24 hours that elapse. If the curse reduces the target's hit point maximum to 0, the target dies, and its body turns to dust. The curse lasts until removed by the remove curse spell or other magic.

___Dreadful Glare.___ The mummy lord targets one creature it can see within 60 feet of it. If the target can see the mummy lord, it must succeed on a DC 16 Wisdom saving throw against this magic or become frightened until the end of the mummy's next turn. If the target fails the saving throw by 5 or more, it is also paralyzed for the same duration. A target that succeeds on the saving throw is immune to the Dreadful Glare of all mummies and mummy lords for the next 24 hours.

**Legendary** Actions

The mummy lord can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The mummy lord regains spent legendary actions at the start of its turn.

___Attack.___ The mummy lord makes one attack with its rotting fist or uses its Dreadful Glare.

___Blinding Dust.___ Blinding dust and sand swirls magically around the mummy lord. Each creature within 5 feet of the mummy lord must succeed on a DC 16 Constitution saving throw or be blinded until the end of the creature's next turn.

___Blasphemous Word (Costs 2 Actions).___ The mummy lord utters a blasphemous word. Each non-undead creature within 10 feet of the mummy lord that can hear the magical utterance must succeed on a DC 16 Constitution saving throw or be stunned until the end of the mummy lord's next turn.

___Channel Negative Energy (Costs 2 Actions).___ The mummy lord magically unleashes negative energy. Creatures within 60 feet of the mummy lord, including ones behind barriers and around corners, can't regain hit points until the end of the mummy lord's next turn.

___Whirlwind of Sand (Costs 2 Actions).___ The mummy lord magically transforms into a whirlwind of sand, moves up to 60 feet, and reverts to its normal form. While in whirlwind form, the mummy lord is immune to all damage, and it can't be grappled, petrified, knocked prone, restrained, or stunned. Equipment worn or carried by the mummy lord remain in its possession.
