"Blue Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +2"
_damage_immunities_: "lightning"
_saving_throws_: "Dex +2, Con +4, Wis +2, Cha +4"
_speed_: "30 ft., burrow 15 ft., fly 60 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "17 (natural armor)"
_stats_: | 17 (+3) | 10 (0) | 15 (+2) | 12 (+1) | 11 (0) | 15 (+2) |

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage plus 3 (1d6) lightning damage.

___Lightning Breath (Recharge 5-6).___ The dragon exhales lightning in a 30-foot line that is 5 feet wide. Each creature in that line must make a DC 12 Dexterity saving throw, taking 22 (4d10) lightning damage on a failed save, or half as much damage on a successful one.