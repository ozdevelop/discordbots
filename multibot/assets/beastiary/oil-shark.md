"Oil Shark";;;_size_: Huge monstrosity
_alignment_: neutral
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "blindsight 60 ft., passive Perception 14"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire"
_speed_: "swim 60 ft."
_hit points_: "114 (12d12 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 22 (+6) | 15 (+2) | 17 (+3) | 1 (-5) | 12 (+1) | 2 (-4) |

___Blood Frenzy.___ The shark has advantage on melee attack rolls against
any creature that doesn’t have all its hit points.

___Keen Smell.___ The oil shark has advantage on Wisdom (Perception)
checks that rely on smell.

___Liquid Fire.___ The oil shark is only known to inhabit the Sea of Fire in
the Plane of Molten Skies and the Plane of Fire, and therefore can breathe
in liquid fire and underwater.

**Actions**

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 28
(4d10 + 6) piercing damage plus 14 (4d6) fire damage.
