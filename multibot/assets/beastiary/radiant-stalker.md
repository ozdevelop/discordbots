"Radiant Stalker";;;_size_: Large construct
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Celestial"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "110 (13d10 + 39)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 17 (+3) | 16 (+3) | 17 (+3) | 6 (-2) | 5 (-3) |

___Radiant Aura.___ The stalker sheds bright light in a 15-
foot radius and dim light for an additional 15 feet.

___Charging Strike.___ If the stalker moves at least 20 feet
straight toward a target it begins to glow with
divine energy. If the stalker hits that target with its
impale attack on this turn, the target takes an
additional 9 (2d8) radiant damage and becomes
charged with divine energies. The first time that
target deals damage to the stalker on its next turn,
it takes an additional 9 (2d8) radiant damage as
these energies surge.

**Actions**

___Multiattack.___ The stalker makes two attacks with its
impale or three with its radiant beam.

___Impale.___ Melee Weapon Attack: +6 to hit, reach 10
ft., one target. Hit: 12 (2d8 + 3) piercing damage
plus 4 (1d8) radiant damage.

___Radiant Beam.___ Ranged Spell Attack: +6 to hit, range
120 ft., one target. Hit: 10 (3d6) radiant damage.

___Resplendent Assault (Recharge 5-6).___ The stalker
unleashes twin beams of energy that spiral around
the room. Each creature within 60 feet of the
stalker make two DC 14 Constitution saving
throws, taking 18 (4d8) radiant damage for each
failed save.
