"Drow Priestess of Lolth";;;_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "8 (3,900 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Insight +6, Perception +6, Religion +4, Stealth +5"
_saving_throws_: "Con +4, Wis +6, Cha +7"
_speed_: "30 ft."
_hit points_: "71 (13d8+13)"
_armor class_: "15 (scale mail)"
_stats_: | 10 (0) | 14 (+2) | 12 (+1) | 13 (+1) | 17 (+3) | 18 (+4) |

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's spellcasting ability is Charisma (spell save DC 15. She can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire, levitate _(self only)

___Spellcasting.___ The drow is a 10th-level spellcaster. Her spellcasting ability is Wisdom (save DC 14, +6 to hit with spell attacks). The drow has the following cleric spells prepared:

* Cantrips (at will): _guidance, poison spray, resistance, spare the dying, thaumaturgy_

* 1st level (4 slots): _animal friendship, cure wounds, detect poison and disease, ray of sickness_

* 2nd level (3 slots): _lesser restoration, protection from poison, web_

* 3rd level (3 slots): _conjure animals _(2 giant spiders)_, dispel magic_

* 4th level (3 slots): _divination, freedom of movement_

* 5th level (2 slots): _insect plague, mass cure wounds_

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The drow makes two scourge attacks.

___Scourge.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage plus 17 (5d6) poison damage.

___Summon Demon (1/Day).___ The drow attempts to magically summon a yochlol with a 30 percent chance of success. If the attempt fails, the drow takes 5 (1d10) psychic damage. Otherwise, the summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 10 minutes, until it or its summoner dies, or until its summoner dismisses it as an action.
