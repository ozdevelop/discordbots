"Flumph";;;_size_: Small aberration
_alignment_: lawful good
_challenge_: "1/8 (25 XP)"
_languages_: "understands Undercommon but can't speak, telepathy 60 ft."
_senses_: "darkvision 60 ft."
_skills_: "Arcana +4, History +4, Religion +4"
_speed_: "5 ft., fly 30 ft."
_hit points_: "7 (2d6)"
_armor class_: "12"
_damage_vulnerabilities_: "psychic"
_stats_: | 6 (-2) | 15 (+2) | 10 (0) | 14 (+2) | 14 (+2) | 11 (0) |

___Advanced Telepathy.___ The flumph can perceive the content of any telepathic communication used within 60 feet of it, and it can't be surprised by creatures with any form of telepathy.

___Prone Deficiency.___ If the flumph is knocked prone, roll a die. On an odd result, the flumph lands upside-down and is incapacitated. At the end of each of its turns, the flumph can make a DC 10 Dexterity saving throw, righting itself and ending the incapacitated condition if it succeeds.

___Telepathic Shroud.___ The flumph is immune to any effect that would sense its emotions or read its thoughts, as well as all divination spells.

**Actions**

___Tendrils.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 4 (1d4 + 2) piercing damage plus 2 (1d4) acid damage. At the end of each of its turns, the target must make a DC 10 Constitution saving throw, taking 2 (1d4) acid damage on a failure or ending the recurring acid damage on a success. A lesser restoration spell cast on the target also ends the recurring acid damage.

___Stench Spray (1/Day).___ Each creature in a 15-foot cone originating from the flumph must succeed on a DC 10 Dexterity saving throw or be coated in a foul-smelling liquid. A coated creature exudes a horrible stench for 1d4 hours. The coated creature is poisoned as long as the stench lasts, and other creatures are poisoned while with in 5 feet of the coated creature. A creature can remove the stench on itself by using a short rest to bathe in water, alcohol, or vinegar.