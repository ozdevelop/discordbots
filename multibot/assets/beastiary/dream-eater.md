"Dream Eater";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "5 (1800 XP)"
_languages_: "Celestial, Common, Draconic, Infernal, telepathy 100 ft."
_skills_: "Deception +8, Insight +4, Persuasion +8"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 20 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 18 (+4) | 17 (+3) | 16 (+3) | 13 (+1) | 20 (+5) |

___Shapechanger.___ The dream eater can use its turn to polymorph into a Small or Medium humanoid it has seen, or back into its true form. Its statistics, other than its size, are the same in all forms. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Innate Spellcasting.___ The dream eater's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

At will: command

3/day: suggestion

**Actions**

___Multiattack.___ The dream eater makes one bite attack and one claw attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d8 + 2) piercing damage, and the target is grappled (escape DC 12).

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 24 (4d10 + 2) slashing damage.

___Dream Eater's Caress.___ A creature that ends its turn grappled by a dream eater is restrained until the end of its next turn, it takes 5 (1d4 + 3) psychic damage, and the dream eater gains the same number of temporary hit points.

___Lotus Scent (Recharge 6).___ The dream eater secretes an oily chemical that most creatures find intoxicating. All living creatures within 30 feet must succeed on a DC 14 Constitution saving throw against poison or be poisoned for 2d4 rounds. While poisoned this way, the creature is stunned. Creatures that successfully save are immune to that dream eater's lotus scent for 24 hours.

___Waking Dreams (1/Day).___ Every creature within 20 feet of the dream eater must make a DC 16 Charisma saving throw. Those that fail enter waking dreams and are confused (as the spell) for 6 rounds. On turns when the creature can act normally (rolls 9 or 10 for the confusion effect), it can repeat the saving throw at the end of its turn, and the effect ends early on a successful save.

