"Swamp Troll";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Giant"
_skills_: "Athletics +8, Perception +1, Stealth +4 (+6 in swampy terrain)"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "30 ft., swim 30 ft."
_hit points_: "63 (6d10 + 34)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 20 (+5) | 6 (-2) | 9 (-1) | 4 (-3) |

___Keen Smell.___ The swamp troll has advantage on Wisdom (Perception)
checks that rely on smell.

___Regeneration.___ The troll regains 3 hit points at the start of its turn. If the
troll takes acid or fire damage, this trait doesn’t function at the start of the
troll’s next turn. The troll dies only if it starts its turn with 0 hit points and
doesn’t regenerate.

___Slimy.___ The swamp troll is covered swampy muck. Creatures attempting
to grapple a swamp troll have disadvantage.

___Swamp Stride.___ The swamp troll ignores nonmagical difficult terrain
caused by swamp water or vegetation.

**Actions**

___Multiattack.___ The swamp troll makes one bite attack and two claw
attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 14
(3d6 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 11
(2d6 + 4) slashing damage.
