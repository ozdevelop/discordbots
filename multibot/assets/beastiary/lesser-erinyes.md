"Lesser Erinyes";;;_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "7 (2,900 XP)"
_languages_: "Infernal, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 12"
_saving_throws_: "Dex +5, Con +6, Wis +4, Cha +6"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 60 ft."
_hit points_: "67 (9d8 + 27)"
_armor class_: "18 (plate)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 12 (+1) | 13 (+1) | 17 (+3) |

___Magic Resistance.___ The lesser erinyes has advantage on saving throws against spells and other
magical effects.

___Poisoned Weapons.___ The lesser erinyes’s weapon
attacks are magical and deal an extra 4 (1d8)
poison damage on a hit (included in its attacks).

**Actions**

___Multiattack.___ The lesser erinyes makes
three attacks.

___Longsword.___ Melee Weapon Attack: +6 to hit,
reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing
damage, or 8 (1d10 + 3) slashing damage if used
with two hands, plus 4 (1d8) poison damage.

___Longbow.___ Ranged Weapon Attack: +5 to hit,
range 150/600 ft., one target. Hit: 6 (1d8 + 2)
piercing damage plus 4 (1d8) poison damage,
and the target must succeed on a DC 11 Constitution saving throw or be poisoned. The poison
lasts until it is removed by the lesser restoration
spell or similar magic.

**Reactions**

___Parry.___ The lesser erinyes can add 4 to its AC
against one melee attack that would hit it. To do
so, the lesser erinyes must see the attacker and
be wielding a melee weapon.
