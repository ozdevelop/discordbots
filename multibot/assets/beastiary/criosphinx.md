"Criosphinx";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Sphinx"
_skills_: "Arcana +8, History +8, Perception +8, Religion +8"
_senses_: "truesight 120 ft., passive Perception 18"
_damage_immunities_: "psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "charmed, frightened"
_speed_: "30 ft., fly 60 ft."
_hit points_: "228 (24d10 + 96)"
_armor class_: "18 (natural armor)"
_stats_: | 18 (+4) | 15 (+2) | 18 (+4) | 16 (+3) | 16 (+3) | 16 (+3) |

___Inscrutable.___ The sphinx is immune to any effect that would sense its
emotions or read its thoughts, as well as any divination spell that
it refuses. Wisdom (Insight) checks made to ascertain the sphinx’s
intentions or sincerity have disadvantage.

___Magic Weapons.___ The sphinx’s weapon attacks are magical.

**Actions**

___Multiattack.___ The sphinx makes a ram attack and two claw attacks.

___Ram.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 17
(3d8 + 4) bludgeoning damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) slashing damage.

**Legendary** Actions

The sphinx can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and
only at the end of another creature’s turn. The sphinx regains spent
legendary actions at the start of its turn.

___Ram Attack.___ The sphinx makes one ram attack.

___Teleport (Costs 2 Actions).___ The sphinx magically teleports, along
with any equipment it is wearing or carrying, up to 120 feet to an
unoccupied space it can see.
