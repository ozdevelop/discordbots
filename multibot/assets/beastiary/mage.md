"Mage";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "6 (2,300 XP)"
_languages_: "any four languages"
_skills_: "Arcana +6, History +6"
_saving_throws_: "Int +6, Wis +4"
_speed_: "30 ft."
_hit points_: "40 (9d8)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 11 (0) | 17 (+3) | 12 (+1) | 11 (0) |

___Spellcasting.___ The mage is a 9th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks). The mage has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, light, mage hand, prestidigitation_

* 1st level (4 slots): _detect magic, mage armor, magic missile, shield_

* 2nd level (3 slots): _misty step, suggestion_

* 3rd level (3 slots): _counterspell, fireball, fly_

* 4th level (3 slots): _greater invisibility, ice storm_

* 5th level (1 slot): _cone of cold_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
