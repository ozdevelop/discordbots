"Lurker Above/Below";;;_size_: Huge aberration
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_skills_: "Athletics +7, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_resistances_: "cold, fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "10 ft., climb 5 ft., fly 40 ft. (Lurker Below instead has a 40 ft. swim speed)"
_hit points_: "68 (8d12 + 16)"
_armor class_: "13"
_stats_: | 18 (+4) | 16 (+3) | 15 (+2) | 2 (-4) | 15 (+2) | 9 (-1) |

___Damage Transfer.___ While it is grappling a creature, the lurker above
takes only half the damage dealt to it, and the creature grappled by the
lurker above takes the other half.

___Keen Scent.___ The lurker above has advantage on Wisdom (Perception)
checks based on scent.

___Sunlight Sensitivity.___ While in sunlight, the lurker above has
disadvantage on attack rolls, as well as on Wisdom (Perception) checks
that rely on sight.

**Actions**

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 17
(3d8 + 4) bludgeoning damage and the target is grappled (escape DC 15)
and the lurker above cannot grapple or use its Slam attack on another
target.

___Smother.___ The lurker above wraps itself around one grappled creature
of Large size or smaller, completely enclosing it. The grappled target
is restrained, blinded, no longer able to speak or use spells with verbal
components, and at risk of suffocating. At the start of each of the target’s
turns, the target takes 17 (3d8 + 4) bludgeoning damage.
