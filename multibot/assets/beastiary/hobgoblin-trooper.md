"Hobgoblin Trooper";;;_size_: Medium humanoid (goblinoid)
_alignment_: lawful evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft., Passive Perception 10"
_speed_: "30 ft."
_hit points_: "11 (2d8 + 2)"
_armor class_: "16 (chainmail)"
_stats_: | 13 (+1) | 12 (+1) | 12 (+1) | 10 (+0) | 10 (+0) | 9 (-1) |

___Martial Advantage.___ Once per turn, the hobgoblin
can deal an extra 7 (2d6) damage to a creature it hits
with a weapon attack if that creature is within 5 feet
of an ally of the hobgoblin that isn’t incapacitated.

**Actions**

___Glaive.___ Melee Weapon Attack: +3 to hit, reach 10 ft.,
one target. Hit: 6 (1d10 + 1) slashing damage.

___Longbow.___ Ranged Weapon Attack: +3 to hit, range
150/600 ft., one target. Hit: 5 (1d8 + 1) piercing
damage.
