"Grick";;;_size_: Medium monstrosity
_alignment_: neutral
_challenge_: "2 (450 XP)"
_senses_: "darkvision 60 ft."
_speed_: "30 ft., climb 30 ft."
_hit points_: "27 (6d8)"
_armor class_: "14 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_stats_: | 14 (+2) | 14 (+2) | 11 (0) | 3 (-4) | 14 (+2) | 5 (-3) |

___Stone Camouflage.___ The grick has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.

**Actions**

___Multiattack.___ The grick makes one attack with its tentacles. If that attack hits, the grick can make one beak attack against the same target.

___Tentacles.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage.

___Beak.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.