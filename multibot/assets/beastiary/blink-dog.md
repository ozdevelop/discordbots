"Blink Dog";;;_size_: Medium fey
_alignment_: lawful good
_challenge_: "1/4 (50 XP)"
_languages_: "Blink Dog, understands Sylvan but can't speak it"
_skills_: "Perception +3, Stealth +5"
_speed_: "40 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "13"
_stats_: | 12 (+1) | 17 (+3) | 12 (+1) | 10 (0) | 13 (+1) | 11 (0) |

___Keen Hearing and Smell.___ The dog has advantage on Wisdom (Perception) checks that rely on hearing or smell.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) piercing damage.

___Teleport (Recharge 4-6).___ The dog magically teleports, along with any equipment it is wearing or carrying, up to 40 ft. to an unoccupied space it can see. Before or after teleporting, the dog can make one bite attack.