"Markham Southwell";;;_size_: Medium humanoid (turami human)
_alignment_: lawful good
_challenge_: "0 (10 XP)"
_languages_: "Common"
_skills_: "Perception +5, Survival +5"
_speed_: "30 ft."
_hit points_: "58 (9d8+18)"
_armor class_: "17 (splint)"
_senses_: " passive Perception 15"
_stats_: | 15 (+2) | 13 (+1) | 14 (+2) | 11 (0) | 16 (+3) | 14 (+2) |

**Actions**

___Multiattack.___ Markham makes two melee attacks.

___Longsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8+2) slashing damage, or 7 (1d10+2) slashing damage if used with two hands.

___Heavy Crossbow.___ Ranged Weapon Attack: +3 to hit, range 100/400 ft., one target. Hit: 6 (1d10+1) piercing damage. Markham carries twenty crossbow bolts.

**Roleplaying** Information

Sheriff Markham of Bryn Shander is a brawny, likable man of few words. Nothing is more important to him than protecting Icewind Dale. He judges others by their actions, not their words.

**Ideal:** "All people deserve to be treated with dignity."

**Bond:** "Duvessa is a natural leader, but she needs help. That's my job."

**Flaw:** "I bury my emotions and have no interest in small talk."