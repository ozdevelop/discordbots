"Greater Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "understands the languages it knew in life but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Wis + 1"
_speed_: "30 ft."
_hit points_: "97 (13d8+39)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_damage_resistances_: "cold, necrotic"
_stats_: | 18 (+4) | 10 (0) | 17 (+3) | 4 (-3) | 6 (-2) | 6 (-2) |

___Source.___ tales from the yawning portal,  page 237

___Undead Nature.___ A zombie doesn't require air, food, drink, or sleep.

___Turn Resistance.___ The zombie has advantage on saving throws against any effect that turns undead.

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Multiattack.___ The zombie makes two melee attacks.

___Empowered Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage and 7 (2d6) necrotic damage.