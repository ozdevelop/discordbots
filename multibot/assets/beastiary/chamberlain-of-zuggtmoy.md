"Chamberlain of Zuggtmoy";;;_size_: Large plant
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Abywssal, Undercommon"
_senses_: "darkvision 60 ft."
_speed_: "20 ft."
_hit points_: "45 (6d10+12)"
_armor class_: "13 (natural armor)"
_damage_resistances_: "bludgeoning, piercing"
_stats_: | 17 (+3) | 7 (-2) | 14 (+2) | 11 (0) | 8 (-1) | 12 (+1) |

___Mushroom Portal.___ The chamberlain counts as a mushroom for the Fungus Stride feature of the bridesmaid of Zuggtmoy.

___Poison Spores.___ Whenever the chamberlain takes damage, it releases a cloud of spores. Creatures within 5 feet of the chamberlain when this happens must succeed on a DC 12 Constitution saving throw or be poisoned for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on a success.

**Actions**

___Hallucination Spores.___ The bridesmaid ejects spores at one creature it can see within 5 feet of it. The target must succeed on a DC 10 Constitution saving throw or be poisoned for 1 minute. While poisoned in this way, the target is incapacitated. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Multiattack.___ The chamberlain makes two slam attacks.

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage.

___Infestation Spores (1/Day).___ The chanberlain releases spores that burst out in a cloud that fills a 10-foot-radius sphere centered on it, and the cloud lingers for 1 minute. Any flesh-and-blood creature in the cloud when it appears, or that enters it later, must make a DC 12 Constitution saving throw. On a successful save, the creature can't be infected by these spores for 24 hours. On a failed save, the creature is infected with a disease called the spores of Zuggtmoy and also gains a random form of indefinite madness (determined by rolling on the Madness of Zuggtmoy table in appendix D) that lasts until the creature is cured of the disease or dies. While infected in this way, the creature can't be reinfected, and it must be repeat the saving throw at the end of every 24 hours, ending the infection on a success. On a failure, the infected creature's body is slowly taken over by fungal growth, and after three such failed saves, the creature dies and is reanimated as a spore servant if it's a type of creature that can be (see the "Myconids" entry in the Monster Manual).