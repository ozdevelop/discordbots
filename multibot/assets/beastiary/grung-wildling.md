"Grung Wildling";;;_size_: Small humanoid (grung)
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Grung"
_skills_: "Athletics +2, Perception +4, Stealth +5, Survival +4"
_damage_immunities_: "poison"
_saving_throws_: "Dex +5"
_speed_: "25 ft., climb 25 ft."
_hit points_: "27 (5d6+10)"
_armor class_: "13 (16 with barkskin)"
_condition_immunities_: "poisoned"
_stats_: | 7 (-2) | 16 (+3) | 15 (+2) | 10 (0) | 15 (+2) | 11 (0) |

___Amphibious.___ The grung can breathe air and water.

___Poisonous Skin.___ Any creature that grapples the grung or otherwise comes into direct contact with the grung's skin must succeed on a DC 12 Constitution saving throw or become poisoned for 1 minute. A poisoned creature no longer in direct contact with the grung can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Spellcasting.___ The grung is a 9th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). It knows the following ranger spells:

1st level (4 slots): cure wounds, jump

2nd level (3 slots): barkskin, spike growth

3rd level (2 slots): plant growth

___Standing Leap.___ The grung's long jump is up to 25 feet and its high jump is up to 15 feet, with or without a running start.

___Variant: Grung Poison.___ Grung poison loses its potency 1 minute after being removed from a grung. A similar breakdown occurs if the grung dies.

>A creature poisoned by a grung can suffer an additional effect that varies depending on the grung's skin color. This effect lasts until the creature is no longer poisoned by the grung.

Red. The poisoned creature must use its action to eat if food is within reach.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4+3) piercing damage, and the target must succeed on a DC 12 Constitution saving throw or take 5 (2d4) poison damage.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6+3) piercing damage, and the target must succeed on a DC 12 Constitution saving throw or take 5 (2d4) poison damage.