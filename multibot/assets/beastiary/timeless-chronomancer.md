"Timeless Chronomancer";;;_size_: Medium humanoid
_alignment_: chaotic neutral
_challenge_: "17 (18,000 XP)"
_languages_: "any eight languages"
_skills_: "Arcana +13, History +19, Insight +10, Perception +10"
_senses_: "passive Perception 20"
_saving_throws_: "Int +13, Wis +10, Dex +10"
_speed_: "30 ft. (60 ft. with <i>haste</i>)"
_hit points_: "169 (26d8 + 52)"
_armor class_: "14 (17 with <i>mage armor</i>, 19 with <i>haste</i>)"
_stats_: | 10 (+0) | 18 (+4) | 14 (+2) | 25 (+7) | 19 (+4) | 14 (+2) |

___Magic Resistance.___ The devil has advantage on saving
throws against spells and other magical effects.

___Innate Spellcasting.___ The chronomancer’s innate
spellcasting ability is Intelligence (spell save DC 21).
He can innately cast the following spells, requiring no
components:

* At will: _haste, mage armor, slow, shield_

* 3/Day: _foresight, legend lore, time stop_

**Actions**

___Multiattack.___ The chronomancer uses its Domes of
Distortion ability if able. It then makes one attack with
each of its daggers.

___Dagger of Ages.___ Melee Weapon Attack: +10 to hit,
reach 5ft., one target. Hit: 6 (1d4 + 4) piercing damage
and the target must succeed on a DC 21 Constitution
saving throw or immediately age 1d20 years and take
27 (6d8) force damage. A creature within a Dome of
Vigor has disadvantage on this saving throw. A greater
restoration spell can restore a creature’s age to normal.

___Dagger of Sands.___ Melee Weapon Attack: +10 to hit,
reach 5ft., one target. Hit: 6 (1d4 + 4) piercing
damage, and must succeed on a DC 21 Constitution
saving throw against being magically petrified. A
creature within a Dome of Lethargy has disadvantage
on this saving throw. On a failed save, the creature
becomes restrained. It must repeat the saving throw at
the end of its next turn. On a success, the effect ends.
On a failure, the creature is petrified. Only a greater
restoration or wish spell can restore the creature to
normal.

___Domes of Distortion (Recharge 5-6).___ The chronomancer
creates two 15-foot radius domes at two locations
within 120 feet. These domes last for one next minute
or until the chronomancer uses this ability again. One
dome, the Dome of Vigor, gives the creatures within
the benefits of the Haste spell while the other, the
Dome of Lethargy, gives the detriments of the Slow
spell. The Dome of Haste is a green in color while the
Dome of Lethargy is blue in color. These domes cannot
overlap

___Entombed By Time.___ The chronomancer conjures an
hourglass shaped prison in an attempt to trap a
creature within 120 feet. That creature must succeed
on a DC 21 Dexterity saving throw or be imprisoned.
Sand immediately begins to pour from the top of the
hourglass to the bottom, causing the trapped creature
to age rapidly. A creature that starts its turn within the
hourglass ages 1d20 years. If 30 points of damage are
dealt to the glass walls of the hourglass in a single turn,
the glass shatters and the hourglass disappears.

**Legendary** Actions

The chronomancer can take 3 legendary actions,
choosing from the options below. Only one legendary
action can be used at a time and only at the end of
another creature’s turn. The chronomancer regains
spent legendary actions at the start of its turn.

___Dagger.___ The chronomancer makes an attack with one of
its daggers.

___Teleport (Costs 2 Actions).___ The chronomancer magically
teleports, along with any equipment it is wearing or
carrying, up to 120 feet to an unoccupied space it can
see.

___Entomb (Costs 3 Actions).___ The chronomancer uses its
Entombed By Time ability.
