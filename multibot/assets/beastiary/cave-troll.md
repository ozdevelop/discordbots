"Cave Troll";;;_size_: Medium giant
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Giant"
_skills_: "Perception +1"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "60 ft., climb 20 ft."
_hit points_: "76 (9d8 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 19 (+4) | 18 (+4) | 8 (-1) | 9 (-1) | 7 (-2) |

___Haste.___ When in need, a cave troll is capable of startling bursts of speed.
It has advantage on Dexterity saving throws and gains an additional action
on each of its turns. That action can be used only to take the Attack (one
claw), Dash, Disengage, Hide, or Use an Object action.

___Regeneration.___ The cave troll regains 6 hit points at the start of its turn.
If the cave troll takes acid or fire damage, this trait doesn’t function at the
start of the cave troll’s next turn. The cave troll dies only if starts its turn
with 0 hit points and doesn’t regenerate.

___Spider Climb.___ The cave troll can climb difficult surfaces, including
upside down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack.___ The cave troll makes three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 +4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9
(2d4+4) slashing damage.
