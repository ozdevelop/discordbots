"Hearth Hope";;;_size_: Small undead
_alignment_: lawful good
_challenge_: "2 (450 XP)"
_languages_: "Common, Halfling"
_senses_: "darkvision 60 ft., Passive Perception 11"
_damage_immunities_: "Cold, Necrotic, Poison"
_damage_resistances_: "Acid, Fire, Lightning, Thunder; Bludgeoning, Piercing And Slashing From Nonmagical Attacks"
_condition_immunities_: "Charmed, Exhaustion, Frightened, Grappled, Paralyzed, Petrified, Poisoned, Prone, Restrained"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "21(6d6)"
_armor class_: "15"
_stats_: | 5 (-3) | 15 (+2) | 10 (+0) | 10 (+0) | 12 (+1) | 16 (+3) |

___Ethereal Sight.___ The hope can see 60 feet into the
Ethereal Plane when it is on the Material Plane, and
vice versa.

___Incorporeal Movement.___ The hope can move
through other creatures and objects as if they were
difficult terrain. It takes 5 (1d10) force damage if it
ends its turn inside an object.

___Final Task.___ Hope is a creature with one goal. This
goal is typically simple — delivery of a message, ensuring
proper burial, or one last glimpse of a loved
one. Whatever the task, once completed, hope is no
longer fettered to the mortal realm and fades away
to whatever afterlife awaits.

**Actions**

___Beatific Visage.___ Each non-undead or construct
within 60 feet of the hope that can see it must
succeed on a DC 13 Wisdom saving throw or be
frightened for 1 minute. While frightened this way
the creature is paralyzed. If the save fails by 5 or
more, the target is awestruck and suffers the effect
of a confusion spell for 1 minute, once the frightened
condition ends. A target can repeat the saving
throw at the end of each of its turns, ending the
frightened condition or confusion on itself on a
success. If the target’s saving throw is successful or
the effect ends for it, the target is immune to this
hope’s Beatific Visage for the next 24 hours.

___Possession (Recharge 6).___ One humanoid that the
hope can see within 5 feet of it must succeed on a
DC 13 Charisma saving throw or be possessed by
the hope; the hope then disappears, and the target
is incapacitated and loses control of its body. The
hope now controls the body but doesn’t deprive the
target of awareness. The hope can’t be targeted by
any attack, spell, or other effect, except ones that
turn undead, and it retains its alignment, Intelligence,
Wisdom, Charisma, and immunity to being
charmed and frightened. It otherwise uses the
possessed target’s statistics, but doesn’t gain access
to the target’s knowledge, class features, or proficiencies.
The possession lasts until the body drops
to 0 hit points, the hope ends it as a bonus action,
or the hope is turned or forced out by an effect like
the _dispel evil and good_ spell. When the possession
ends, the hope reappears in an unoccupied space
within 5 feet of the body. The target is immune to
this hope’s Possession for 24 hours after succeeding
on the saving throw or after the possession ends.

___Psychic Feedback.___ Ranged spell attack: +4 to hit,
range 30 feet, one target. Hit: 5 (1d10) psychic damage
as the target is assaulted with images of their
own past failures.
