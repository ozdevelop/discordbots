"Graz'zt (A)";;;_size_: Large fiend (demon
_alignment_: shapechanger)
_challenge_: "24 (62,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft."
_skills_: "Deception +15, Insight +12, Perception +12, Persuasion +15"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing that is nonmagical"
_saving_throws_: "Dex +9, Con +12, Wis +12"
_speed_: "40 ft."
_hit points_: "378 (36d10+180)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 22 (+6) | 15 (+2) | 21 (+5) | 23 (+6) | 21 (+5) | 26 (+8) |

___Shapechanger.___ Graz'zt can use his action to polymorph into a form that resembles a Medium humanoid, or back into his true form. Aside from his size, his statistics are the same in each form, Any equipment he is wearing or carrying isn't transformed.

___Innate Spellcasting.___ Graz'zt's spellcasting ability is Charisma (spell save DC 23). Graz'zt can innately cast the following spells, requiring no material components:

* At will: _charm person, crown of madness, detect magic, dispel magic, dissonant whispers_

* 3/day each: _counterspell, darkness, dominate person, sanctuary, telekinesis, teleport_

* 1/day each: _dominate monster, greater invisibility_

___Legendary Resistance (3/Day).___ If Graz'zt fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Graz'zt has advantage on saving throws against spell and other magic effects.

___Magic Weapon.___ Graz'zt's weapon attacks are magical.

**Actions**

___Multiattack.___ Graz'zt attacks twice with the Wave of Sorrow.

___Wave of Sorrow (Greatsword).___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target, Hit; 20 (4d6 + 6) slashing damage plus 14 (4d6) acid damage.

___Teleport.___ Graz'zt magically teleports, along with any equipment he is wearing or carrying, up to 120 feet to an unoccupied space he can see.

**Legendary** Actions

Graz'zt can take 3 legendary actions, choosing from the options below. Only one legendary action can be used at a time and only at the end of another creautre's turn. Graz'zt regains spent legendary actions at the start of his turn.

___Attack.___ Graz'zt attacks once with the Wave of Sorrow.

___Dance, My Puppet!.___ One creature charmed by Graz'zt that Graz'zt can see must use its reaction to move up to its speed as Graz'zt directs.

___Sow Discord.___ Graz'zt casts crown of madness or dissonant whispers.

___Teleport.___ Graz'zt uses his Teleport action.
