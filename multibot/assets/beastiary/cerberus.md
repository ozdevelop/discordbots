"Cerberus";;;_size_: Huge fiend (devil)
_alignment_: lawful evil
_challenge_: "24 (62,500 XP)"
_languages_: "Infernal, telepathy 120 ft."
_saving_throws_: "Dex +8, Con +11, Wis +8"
_skills_: "Perception +8"
_senses_: "darkvision 120 ft., passive Perception 18"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "60 ft."
_hit points_: "294 (28d12 + 112)"
_armor class_: "18 (natural armor)"
_stats_: | 25 (+7) | 13 (+1) | 18 (+4) | 6 (-2) | 13 (+1) | 6 (-2) |

___Devil’s Sight.___ Magical darkness doesn’t impede Cerberus' darkvision.

___Keen Hearing and Smell.___ Cerberus has advantage on Wisdom
(Perception) checks that rely on hearing or smell.

___Legendary Resistance (3/day).___ If Cerberus fails a saving throw, it can
choose to succeed instead.

___Magic Resistance.___ Cerberus has advantage on saving throws against
spells and other magical effects.

___Multiple Heads.___ Cerberus has three heads. While it has more than one
head, Cerberus has advantage on saving throws against being blinded,
charmed, deafened, frightened, stunned, or knocked unconscious.
Whenever Cerberus takes 40 or more damage in a single turn, one of its
heads dies. If all its heads die, Cerberus dies.

**Actions**

___Multiattack.___ Cerberus makes three bite attacks and one claw attack.

___Bite.___ Melee Weapon Attack: +14 to hit, reach 5 ft., one target. Hit: 23
(3d10 + 7) piercing damage plus 14 (4d6) fire damage.

___Claw.___ Melee Weapon Attack: +14 to hit, reach 10ft., one target. Hit: 20
(3d8 + 7) slashing damage.

___Fire Breath (Recharge 5–6).___ One of Cerberus’ heads releases a 60-foot
cone of flames. Each creature in the area must make a DC 18 Dexterity saving
throw, taking 42 (12d6) fire damage on a failed save, or half as much damage
on a successful saving throw. Each head has its own recharge counter.

**Legendary** Actions

Cerberus can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. Cerberus regains spent legendary
actions at the start of its turn.

___Open/Close the Gate.___ Cerberus causes the Gate of Hell to open if it is
closed, or close if it is open.

___Bite.___ Cerberus makes a Bite attack.

___Fire Breath (Costs 3 Actions).___ Cerberus uses its Fire Breath ability,
even if it has not recharged.
