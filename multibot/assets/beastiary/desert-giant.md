"Desert Giant";;;_size_: Huge giant
_alignment_: neutral
_challenge_: "9 (5000 XP)"
_languages_: "Common, Giant"
_skills_: "Perception +8, Stealth +4, Survival +8"
_senses_: ", passive Perception 18"
_saving_throws_: "Str + 12, Con +10, Cha +6"
_damage_immunities_: "fire"
_speed_: "40 ft."
_hit points_: "175 (14d12 + 84)"
_armor class_: "17 (natural armor)"
_stats_: | 27 (+8) | 10 (+0) | 22 (+6) | 13 (+1) | 18 (+4) | 15 (+2) |

___Sand Camouflage.___ The giant has advantage on Dexterity (Stealth) checks made to hide in sandy terrain.

___Wasteland Stride.___ The giant ignores difficult terrain caused by sand, gravel, or rocks.

**Actions**

___Multiattack.___ The giant makes two falchion attacks.

___Falchion.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 23 (6d4 + 8) slashing damage.

___Rock.___ Ranged Weapon Attack: +12 to hit, range 60/240 ft., one target. Hit: 30 (4d10 + 8) bludgeoning damage.

