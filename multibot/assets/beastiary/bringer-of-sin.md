"Bringer of Sin";;;_size_: Medium celestial
_alignment_: chaotic evil
_challenge_: "12 (8,400 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Acrobatics +9, Athletics +10, Perception +7"
_senses_: "darkvision 120ft., passive Perception 17"
_saving_throws_: "Con +9, Wis +7, Cha +8"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhausted, frightened"
_speed_: "30 ft., fly 90 ft."
_hit points_: "142 (15d8 + 75)"
_armor class_: "18 (natural armor)"
_stats_: | 22 (+6) | 20 (+5) | 20 (+5) | 16 (+3) | 16 (+3) | 18 (+4) |

___Aura of Pain.___ A creature that deals damage to the
bringer while within 30 feet of it takes 5 (1d10)
necrotic damage.

___Light Seeker.___ The bringer knows the exact location of
any good creature within 1000 feet.

___Magic Resistance.___ The bringer has advantage on saving
throws against spells and other magical effects.

**Actions**

___Multiattack.___ The bringer makes two melee attacks, one
of which may be a punishing strike.

___Glaive.___ Melee Weapon Attack: +10 to hit, reach 10 ft.,
one target. Hit: 17 (2d10 + 6) slashing damage plus
22 (5d8) necrotic damage if the creature is good.

___Punishing Strike (3/Day).___ Melee Weapon Attack: +10 to
hit, reach 10 ft., one target. Hit: 17 (2d10 + 6) slashing
damage plus half of the total damage that creature
dealt to the bringer with its last attack as bonus
necrotic damage.

___Wave of Necrosis (Recharge 5-6).___ The bringer lets loose
a blast of necrotic energy in a 60 foot cone. Each
creature allied with the bringer regains 50 hit points
and each enemy must make a DC 17 Constitution
saving throw, taking 45 (10d8) necrotic damage on a
failed save, or half as much damage on a successful
one.
