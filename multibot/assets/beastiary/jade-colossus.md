"Jade Colossus";;;_size_: Gargantuan construct
_alignment_: neutral
_challenge_: "17 (18,000 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "595 (34d20 + 238)"
_armor class_: "19 (natural armor)"
_stats_: | 29 (+9) | 10 (+0) | 24 (+7) | 2 (-4) | 11 (+0) | 1 (-5) |

___Immutable Form.___ The jade colossus is immune to any spell or effect
that would alter its form.

___Magic Resistance.___ The jade colossus has advantage on saving throws
against spell and other magical effects.

___Magic Weapons.___ The jade colossus’ weapon attacks are magical.

___Light Reflection.___ Any creature looking directly at the jade colossus
while the colossus is in bright light must make a DC 12 Constitution
saving throw or be blinded for 1 minute. Additionally, attacks against the
colossus are made with disadvantage, and undead take 22 (4d10) radiant
damage if they start their turn within 60 feet of the colossus.

**Actions**

___Multiattack.___ The jade colossus makes two slam attacks.

___Slam.___ Melee Weapon Attack: +15 to hit, reach 20 ft., one target. Hit: 36 (5d10 + 9) bludgeoning damage.

___Jade Blast (Recharge 5–6).___ The jade colossus unleashes a blast of green
energy in a 60-foot cone. Each creature in that area must make a DC 18
Dexterity saving throw, taking 56 (16d6) force damage on a failed save,
or half as much damage on a successful one. Additionally, the creature
magically begins to turn to jade and is restrained. The restrained target
must repeat the saving throw at the end of its next turn. On a success, the
effect ends on the target. On a failure, the target is petrified until freed by
a greater restoration spell or other magic.

___Energy Ray (if embedded with the Ruby Star of Law).___ Once per round as a bonus action, the Jade
Guardian can fire a ray to a range of 200 feet. Genies hit by this ray
must make a successful DC 18 Constitution saving throw or take
17 (7d4) points of Constitution damage. On a successful save, the
genie takes half damage. Other creatures are affected as follows:
Lawful creatures suffer two levels of exhaustion. Non-lawful
creatures take 28 (8d6) force damage and are stunned for 1 minute.
Should the Ruby Star of Law ever be claimed as a magic item,
treat it as a minor artifact that allows use of the energy ray ability
indicated above up to 3/day, save that it requires a standard action
to activate.
