"Harvest Scarecrow";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "Understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., Passive Perception 11"
_damage_immunities_: "Poison"
_damage_resistances_: "Bludgeoning, Piercing, And Slashing From Nonmagical Attacks"
_damage_vulnerabilities_: "Fire"
_condition_immunities_: "Charmed, exhaustion, frightened, paralyzed, poisoned, unconscious"
_speed_: "30 ft."
_hit points_: "44 (8d8 + 8)"
_armor class_: "12"
_stats_: | 11 (+0) | 15 (+2) | 13 (+1) | 10 (+0) | 12 (+1) | 13 (+1) |

___False Appearance.___ While the harvest scarecrow
remains motionless, it is indistinguishable from an
ordinary, inanimate scarecrow.

___Dust of the Field.___ Whenever a creature successfully
makes a melee attack against the harvest scarecrow,
the creature must succeed on a DC 11 Dexterity saving
throw or be blinded until the end of their next turn.

**Actions**

___Multiattack.___ The harvest scarecrow makes two
attacks with its old tools.

___Old Tools.___ Melee Weapon Attack: +3 to hit, reach 5
ft., one target. Hit: 5 (1d6 + 2) slashing damage. If
the target is a creature, it must succeed on a DC 11
Constitution saving throw or be poisoned for one
hour.

___Hay Fever.___ The harvest scarecrow targets one
creature it can see within 10 feet of it. The target
must succeed on a DC 11 Constitution saving throw
or spend its next action coughing and sneezing
uncontrollably. Creatures that don’t need to breathe
automatically succeed on this saving throw.
