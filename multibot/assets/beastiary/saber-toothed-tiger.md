"Saber-Toothed Tiger";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_skills_: "Perception +3, Stealth +6"
_speed_: "40 ft."
_hit points_: "52 (7d10+14)"
_armor class_: "12"
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 3 (-4) | 12 (+1) | 8 (-1) |

___Keen Smell.___ The tiger has advantage on Wisdom (Perception) checks that rely on smell.

___Pounce.___ If the tiger moves at least 20 ft. straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 14 Strength saving throw or be knocked prone. If the target is prone, the tiger can make one bite attack against it as a bonus action.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (1d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.