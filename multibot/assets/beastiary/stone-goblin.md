"Stone Goblin";;;_size_: Medium humanoid
_alignment_: lawful neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Goblin, Sylvan"
_skills_: "Athletics +5, Perception +5"
_senses_: "darkvision 120 ft., passive Perception 15"
_saving_throws_: "Str +5, Con +6"
_damage_vulnerabilities_: "poison"
_damage_immunities_: "acid"
_damage_resistances_: "fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "20 ft."
_hit points_: "42 (5d8 + 20)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 18 (+4) | 13 (+1) | 16 (+3) | 12 (+1) |

___Innate Spellcasting.___ The stone goblin’s spellcasting
ability is Wisdom (spell save DC 13, +5 to hit with
spell attacks). The stone goblin can innately cast the
following spells, requiring no material components:

* At will: _acid splash, mending, resistance, true strike_

* 3/day each: _bane, bless, command, heroism, sanctuary, shield_

* 1/day: _meld into stone_

___Magic Resistance.___ The stone goblin has advantage on
saving throws against spells and other magical effects.

**Actions**

___Warhammer.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) bludgeoning damage, or 8 (1d10 + 3) bludgeoning damage if used with two hands.

___Light Crossbow.___ Ranged Weapon Attack: +2 to hit, range 80/320 ft., one target. Hit: 6 (1d8 + 2) piercing damage.
