"Corrupting Ooze";;;_size_: Large ooze
_alignment_: neutral evil
_challenge_: "5 (1800 XP)"
_languages_: "--"
_skills_: "Stealth +3"
_senses_: "blindsight 60 ft., tremorsense 60 ft., passive Perception 5"
_damage_immunities_: "acid, fire, poison"
_damage_resistances_: "slashing, bludgeoning"
_condition_immunities_: "exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft., swim 30 ft."
_hit points_: "115 (10d10 + 60)"
_armor class_: "12 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 22 (+6) | 4 (-3) | 2 (-4) | 1 (-5) |

___Corrupting Touch.___ When a corrupting ooze scores a critical hit or starts its turn with a foe grappled, it can dissolve one leather, metal, or wood item of its choosing in the possession of the target creature. A mundane item is destroyed automatically; a magical item is destroyed if its owner fails to make a successful DC 16 Dexterity saving throw.

___Strong Swimmer.___ A corrupting ooze naturally floats on the surface of water. It swims with a pulsating motion that propels it faster than walking speed.

**Actions**

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) bludgeoning damage plus 3 (1d6) acid damage, and the target is grappled (escape DC 13).

