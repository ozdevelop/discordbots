"Gorgimera";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "13 (10,000 XP)"
_languages_: "Draconic"
_skills_: "Perception +11"
_senses_: "darkvision 60 ft., passive Perception 21"
_saving_throws_: "Dex +6, Con +9"
_speed_: "40 ft., fly 50 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 13 (+1) | 19 (+4) | 4 (-3) | 13 (+1) | 10 (+0) |

___Flyby.___ The gorgimera doesn’t provoke an opportunity attack when it
flies out of an enemy’s reach.

___Keen Smell.___ The gorgimera has advantage on Wisdom (Perception)
checks that rely on smell.

___Charge.___ If the gorgimera moves at least 20 feet straight towards
a creature and then hits with a gore attack on the same turn, the target
takes an extra 9 (2d8) piercing damage. If the target is a creature, it must
succeed on a DC 14 Strength saving throw or be pushed up to 10 feet away
and knocked prone.

**Actions**

___Multiattack.___ The gorgimera makes five melee attacks: two with its
claws, two bites, and one gore attack. Alternatively, it can use its two
breath weapons in place of the bite attacks.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 17
(3d8 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) slashing damage.

___Gore.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) piercing damage.

___Dragon Breath (Recharge 5–6).___ The dragon head exhales its breath
based on its color. Each creature in the area affected
must make a DC 18 Dexterity saving throw, taking 31 (7d8) damage on a
failed save, of half as much on a successful one.

* *Black:* 40-foot-long, 5-foot-wide line of acid
* *Blue:* 40-foot-long, 5-foot-wide line of lightning
* *Green:* 20-foot cone of poisonous gas
* *Red:* 20-foot cone of fire
* *White:* 20-foot cone of cold

___Gorgon Breath. (Recharge 5–6).___ The gorgon head exhales petrifying
gas in a 30-foot cone. Each creature in that area must succeed on a DC
18 Constitution saving throw or begin to turn to stone and berestrained.
The restrained target must repeat the saving throw at the end of its next
turn. On a success, the effect ends on the target. On a failure, the target is
petrified until freed by a greater restoration spell or other magic.
