"Rug of Smothering";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_damage_immunities_: "poison, psychic"
_speed_: "10 ft."
_hit points_: "33 (6d10)"
_armor class_: "12"
_condition_immunities_: "blinded, charmed, deafened, frightened, paralyzed, petrified, poisoned"
_stats_: | 17 (+3) | 14 (+2) | 10 (0) | 1 (-5) | 3 (-4) | 1 (-5) |

___Antimagic Susceptibility.___ The rug is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the rug must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute.

___Damage Transfer.___ While it is grappling a creature, the rug takes only half the damage dealt to it, and the creature grappled by the rug takes the other half.

___False Appearance.___ While the rug remains motionless, it is indistinguishable from a normal rug.

**Actions**

___Smother.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one Medium or smaller creature. Hit: The creature is grappled (escape DC 13). Until this grapple ends, the target is restrained, blinded, and at risk of suffocating, and the rug can't smother another target. In addition, at the start of each of the target's turns, the target takes 10 (2d6 + 3) bludgeoning damage.