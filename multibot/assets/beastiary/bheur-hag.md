"Bheur Hag";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Auran, Common, Giant"
_senses_: "darkvision 60 ft."
_skills_: "Nature +4, Perception +4, Stealth +6, Survival +4"
_damage_immunities_: "cold"
_saving_throws_: "Wis +4"
_speed_: "30 ft."
_hit points_: "91 (14d8+28)"
_armor class_: "17 (natural armor)"
_stats_: | 13 (+1) | 16 (+3) | 14 (+2) | 12 (+1) | 13 (+1) | 16 (+3) |

___Graystaff Magic.___ The hag carries a graystaff, a length of gray wood that is a focus for her inner power. She can ride the staff as if it were a broom of flying. While holding the staff, she can cast additional spells with her Innate Spellcasting trait (these spells are marked with an asterisk). If the staff is lost or destroyed, the hag must craft another, which takes a year and a day. Only a bheur hag can use a graystaff.

___Ice Walk.___ The hag can move across and climb icy surfaces without needing to make an ability check. Additionally, difficult terrain composed of ice or snow doesn't cost her extra moment.

___Innate Spellcasting.___ The hag's innate spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). She can innately cast the following spells, requiring no material components:

* At will: _hold person* , ray of frost_

* 3/day each: _cone of cold* , ice storm* , wall of ice* _

* 1/day each: _control weather_

___Hag Coven.___ When hags must work together, they form covens, in spite of their selfish natures. A coven is made up of hags of any type, all of whom are equals within the group. However, each of the hags continues to desire more personal power.

A coven consists of three hags so that any arguments between two hags can be settled by the third. If more than three hags ever come together, as might happen if two covens come into conflict, the result is usually chaos.

___Shared Spellcasting (Coven Only).___ While all three members of a hag coven are within 30 feet of one another, they can each cast the following spells from the wizard's spell list but must share the spell slots among themselves:

* 1st level (4 slots): _identify, ray of sickness_

* 2nd level (3 slots): _hold person, locate object_

* 3rd level (3 slots): _bestow curse, counterspell, lightning bolt_

* 4th level (3 slots): _phantasmal killer, polymorph_

* 5th level (2 slots): _contact other plane, scrying_

* 6th level (1 slot): _eyebite_

For casting these spells, each hag is a 12th-level spellcaster that uses Intelligence as her spellcasting ability. The spell save DC is 12+the hag's Intelligence modifier, and the spell attack bonus is 4+the hag's Intelligence modifier.

___Hag Eye (Coven Only).___ A hag coven can craft a magic item called a hag eye, which is made from a real eye coated in varnish and often fitted to a pendant or other wearable item. The hag eye is usually entrusted to a minion for safekeeping and transport. A hag in the coven can take an action to see what the hag eye sees if the hag eye is on the same plane of existence. A hag eye has AC 10, 1 hit point, and darkvision with a radius of 60 feet. If it is destroyed, each coven member takes 3d10 psychic damage and is blinded for 24 hours.

A hag coven can have only one hag eye at a time, and creating a new one requires all three members of the coven to perform a ritual. The ritual takes 1 hour, and the hags can't perform it while blinded. During the ritual, if the hags take any action other than performing the ritual, they must start over.

___Variant: Death Coven.___ For a death coven, replace the spell list of the hags' Shared Spellcasting trait with the folllowing spells:

* 1st level (4 slots): _false life, inflict wounds_

* 2nd level (3 slots): _gentle repose, ray of enfeeblement_

* 3rd level (3 slots): _animate dead, revivify, speak with dead_

* 4th level (3 slots): _blight, death ward_

* 5th level (2 slots): _contagion, raise dead_

* 6th level (1 slot): _circle of death_

___Variant: Nature Coven.___ For a nature coven, replace the spell list of the hags' Shared Spellcasting trait with the folllowing spells:

* 1st level (4 slots): _entangle, speak with animals_

* 2nd level (3 slots): _flaming sphere, moonbeam, spike growth_

* 3rd level (3 slots): _call lightning, plant growth_

* 4th level (3 slots): _dominate beast, grasping vine_

* 5th level (2 slots): _insect plague, tree stride_

* 6th level (1 slot): _wall of thorns_

___Variant: Prophecy Coven.___ For a prophecy coven, replace the spell list of the hags' Shared Spellcasting trait with the folllowing spells:

* 1st level (4 slots): _bane, bless_

* 2nd level (3 slots): _augury, detect thoughts_

* 3rd level (3 slots): _calirvoyance, dispel magic, nondetection_

* 4th level (3 slots): _arcane eye, locate creature_

* 5th level (2 slots): _geas, legend lore_

* 6th level (1 slot): _true seeing_

**Actions**

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit:10 (2d8+1) bludgeoning damage plus 3 (1d6) cold damage.

___Maddening Feast.___ The hag feasts on the corpse of one enemy within 5 feet of her that died within the past minute. Each creature of the hag's choice that is within 60 feet of her and able to see her must succeed on a DC 15 Wisdom saving throw or be frightened of her for 1 minute. While frightened in this way, a creature is incapacitated, can't understand what others say, can't read, and speaks only in gibberish; the DM controls the creature's movement, which is erratic. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the hag's Maddening Feast for the next 24 hours.
