"Obsidian Minotaur";;;_size_: Large construct
_alignment_: neutral
_challenge_: "8 (3,900 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "acid, fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "76 (8d10 + 32)"
_armor class_: "16 (natural armor)"
_stats_: | 21 (+5) | 10 (+0) | 18 (+4) | 3 (-4) | 11 (+0) | 1 (-5) |

___Charge.___ If the obsidian minotaur moves at least 10 feet straight toward
a target and then hits it with a gore attack on the same turn, the target
takes an extra 9 (2d8) piercing damage. If the target is a creature, it must
succeed on a DC 16 Strength saving throw or be pushed up to 10 feet away
and knocked prone.

___Immutable Form.___ The minotaur is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The minotaur has advantage on saving throws
against spells and other magic effects.

___Magic Weapons.___ The minotaur’s weapon attacks are magical.

**Actions**

___Multiattack.___ The obsidian minotaur makes one gore attack and two
claw attacks.

___Gore.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14
(2d8 + 5) piercing damage plus 7 (2d6) fire damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 12
(2d6 + 5) slashing damage plus 7 (2d6) fire damage.

___Burning Breath (Recharge 5–6).___ The obsidian minotaur expels a cloud
of superheated gas that fills a 10-foot cube adjacent to it.. The gas fades
after the end of the minotaur’s next turn. Creatures who enter the area
or start their turn there must make a DC 16 Constitution saving throw.
On a failed saving throw, the target takes 31 (9d6) fire damage and it is
poisoned for 1 minute. On a successful saving throw, the target takes half
the damage and is not poisoned.
