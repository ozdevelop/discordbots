"White Kobold";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_resistances_: "cold"
_speed_: "30 ft."
_hit points_: "13 (3d6 + 3)"
_armor class_: "14 (hide armor)"
_stats_: | 7 (-2) | 15 (+2) | 12 (+1) | 8 (-1) | 7 (-2) | 8 (-1) |

___Sunlight Sensitivity.___ While in sunlight, the kobold has
disadvantage on attack rolls, as well as on Wisdom
(Perception) checks that rely on sight.

___Pack Tactics.___ The kobold has advantage on an attack
roll against a creature if at least one of the kobold's
allies is within 5 feet of the creature and the ally
isn't incapacitated.

___Snow Tunneler.___ The kobold can burrow through
loose snow at half its walking speed and leaves a 3-
foot-diameter tunnel in its wake.

**Actions**

___Flint Dagger.___ Melee or Ranged Weapon Attack: +4 to
hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4
(1d4 + 2) piercing damage.

___Ice Shard Spear.___ Melee or Ranged Weapon Attack: +4
to hit, reach 5 ft. or range 20/60 ft., one target. Hit:
5 (1d6 + 2) piercing damage plus 3 (1d6) cold
damage.
