"Sahuagin";;;_size_: Medium humanoid (sahuagin)
_alignment_: lawful evil
_challenge_: "1/2 (100 XP)"
_languages_: "Sahuagin"
_senses_: "darkvision 120 ft."
_skills_: "Perception +5"
_speed_: "30 ft., swim 40 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "12 (natural armor)"
_stats_: | 13 (+1) | 11 (0) | 12 (+1) | 12 (+1) | 13 (+1) | 9 (-1) |

___Blood Frenzy.___ The sahuagin has advantage on melee attack rolls against any creature that doesn't have all its hit points.

___Limited Amphibiousness.___ The sahuagin can breathe air and water, but it needs to be submerged at least once every 4 hours to avoid suffocating.

___Shark Telepathy.___ The sahuagin can magically command any shark within 120 feet of it, using a limited telepathy.

**Actions**

___Multiattack.___ The sahuagin makes two melee attacks: one with its bite and one with its claws or spear.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) slashing damage.

___Spear.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d6 + 1) piercing damage, or 5 (1d8 + 1) piercing damage if used with two hands to make a melee attack.