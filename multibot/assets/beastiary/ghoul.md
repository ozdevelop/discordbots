"Ghoul";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "22 (5d8)"
_armor class_: "12"
_condition_immunities_: "poisoned"
_stats_: | 13 (+1) | 15 (+2) | 10 (0) | 7 (-2) | 10 (0) | 6 (-2) |

**Actions**

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one creature. Hit: 9 (2d6 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) slashing damage. If the target is a creature other than an elf or undead, it must succeed on a DC 10 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.