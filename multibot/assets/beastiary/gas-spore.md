"Gas Spore";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_damage_immunities_: "poison"
_speed_: "0 ft., fly 10 ft. (hover)"
_hit points_: "1 (1d10-4)"
_armor class_: "5"
_condition_immunities_: "blinded, deafened, frightened, paralyzed, poisoned, prone"
_stats_: | 5 (-3) | 1 (-5) | 3 (-4) | 1 (-5) | 1 (-5) | 1 (-5) |

___Death Burst.___ The gas spore explodes when it drops to 0 hit points. Each creature within 20 feet of it must succeed on a DC 15 Constitution saving throw or take 10 (3d6) poison damage and become infected with a disease on a failed save. Creatures immune to the poisoned condition are immune to this disease.

Spores invade an infected creature's system, killing the creature in a number of hours equal to 1d12+the creature's Constitution score, unless the disease is removed. In half that time, the creature becomes poisoned for the rest of the duration. After the creature dies, it sprouts 2d4 Tiny gas spores that grow to full size in 7 days.

___Eerie Resemblance.___ The gas spore resembles a beholder. A creature that can see the gas spore can discern its true nature with a successful DC 15 Intelligence (Nature) check.

**Actions**

___Touch.___ Melee Weapon Attack: +0 to hit, reach 5 ft., one creature. Hit: 1 poison damage, and the creature must succeed on a DC 10 Constitution saving throw or become infected with the disease described in the Death Burst trait.