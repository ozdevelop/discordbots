"The Lost";;;_page_number_: 233
_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "7 (2900 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 8"
_skills_: "Athletics +6"
_speed_: "30 ft."
_hit points_: "78  (12d8 + 24)"
_armor class_: "15 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing while in dim light or darkness"
_stats_: | 17 (+3) | 12 (+1) | 15 (+2) | 6 (-2) | 7 (-1) | 5 (-2) |

**Actions**

___Multiattack___ The Lost makes two arm spike attacks.

___Arm Spike___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 14 (2d10 + 3) piercing damage.

___Embrace___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 25 (4d10 + 3) piercing damage, and the target is grappled (escape DC 14) if it is a Medium or smaller creature. Until the grapple ends, the target is frightened, and it takes 27 (6d8) psychic damage at the end of each of its turns. The Lost can embrace only one creature at a time.