"White Abishai";;;_page_number_: 163
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "6 (2300 XP)"
_languages_: "Draconic, Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 11"
_damage_immunities_: "cold, fire, poison"
_saving_throws_: "Str +6, Con +7"
_speed_: "30 ft., fly 40 ft."
_hit points_: "68  (8d8 + 32)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 16 (+3) | 11 (0) | 18 (+4) | 11 (0) | 12 (+1) | 13 (+1) |

___Devil's Sight.___ Magical darkness doesn't impede the abishai's darkvision.

___Magic Resistance.___ The abishai has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The abishai's weapon attacks are magical.

___Reckless.___ At the start of its turn, the abishai can gain advantage on all melee weapon attack rolls during that turn, but attack rolls against it have advantage until the start of its next turn.

**Actions**

___Multiattack___ The abishai makes two attacks: one with its longsword and one with its claw.

___Longsword___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1d10 + 3) slashing damage if used with two hands.

___Claw___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) slashing damage.

___Bite___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage plus 3 (1d6) cold damage.