"Grung Elite Warrior";;;_size_: Small humanoid (grung)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Grung"
_skills_: "Athletics +2, Perception +2, Stealth +5, Survival +2"
_damage_immunities_: "poison"
_saving_throws_: "Dex +5"
_speed_: "25 ft., climb 25 ft."
_hit points_: "49 (9d6+18)"
_armor class_: "13"
_condition_immunities_: "poisoned"
_stats_: | 7 (-2) | 16 (+3) | 15 (+2) | 10 (0) | 11 (0) | 12 (+1) |

___Amphibious.___ The grung can breathe air and water.

___Poisonous Skin.___ Any creature that grapples the grung or otherwise comes into direct contact with the grung's skin must succeed on a DC 12 Constitution saving throw or become poisoned for 1 minute. A poisoned creature no longer in direct contact with the grung can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Standing Leap.___ The grung's long jump is up to 25 feet and its high jump is up to 15 feet, with or without a running start.

___Variant: Grung Poison.___ Grung poison loses its potency 1 minute after being removed from a grung. A similar breakdown occurs if the grung dies.

>A creature poisoned by a grung can suffer an additional effect that varies depending on the grung's skin color. This effect lasts until the creature is no longer poisoned by the grung.

Orange. The poisoned creature is frightened of its allies.

Gold. The poisoned creature is charmed and can speak Grung.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 it, one target. Hit: 5 (1d4+3) piercing damage, and the target must succeed on a DC 12 Constitution saving throw or take 5 (2d4) poison damage.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6+3) piercing damage, and the target must succeed on a DC 12 Constitution saving throw or take 5 (2d4) poison damage.

___Mesmerizing Chirr (Recharge 6).___ The grung makes a chirring noise to which grungs are immune. Each humanoid or beast that is within 15 feet of the grung and able to hear it must succeed on a DC 12 Wisdom saving throw or be stunned until the end of the grung's next turn.