"Fomorian";;;_size_: Huge giant
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "Giant, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +8, Stealth +3"
_speed_: "30 ft."
_hit points_: "149 (13d12+65)"
_armor class_: "14 (natural armor)"
_stats_: | 23 (+6) | 10 (0) | 20 (+5) | 9 (-1) | 14 (+2) | 6 (-2) |

**Actions**

___Multiattack.___ The fomorian attacks twice with its greatclub or makes one greatclub attack and uses Evil Eye once.

___Greatclub.___ Melee Weapon Attack: +9 to hit, reach 15 ft., one target. Hit: 19 (3d8 + 6) bludgeoning damage.

___Evil Eye.___ The fomorian magically forces a creature it can see within 60 feet of it to make a DC 14 Charisma saving throw. The creature takes 27 (6d8) psychic damage on a failed save, or half as much damage on a successful one.

___Curse of the Evil Eye (Recharges after a Short or Long Rest).___ With a stare, the fomorian uses Evil Eye, but on a failed save, the creature is also cursed with magical deformities. While deformed, the creature has its speed halved and has disadvantage on ability checks, saving throws, and attacks based on Strength or Dexterity.

The transformed creature can repeat the saving throw whenever it finishes a long rest, ending the effect on a success.