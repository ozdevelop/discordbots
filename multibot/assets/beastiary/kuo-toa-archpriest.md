"Kuo-Toa Archpriest";;;_size_: Medium humanoid (kuo-toa)
_alignment_: neutral evil
_challenge_: "6 (2,300 XP)"
_languages_: "Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +9, Religion +6"
_speed_: "31 ft., swim 30 ft."
_hit points_: "97 (13d8+39)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 13 (+1) | 16 (+3) | 14 (+2) |

___Amphibious.___ The kuo-toa can breathe air and water.

___Otherwordly Perception.___ The kuo-toa can sense the presence of any creature within 30 feet of it that is invisible or on the Ethereal Plane. It can pinpoint such a creature that is moving.

___Slippery.___ The kuo-toa has advantage on ability checks and saving throws made to escape a grapple.

___Sunlight Sensitivity.___ While in sunlight, the kuo-toa has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Spellcasting.___ The kuo-toa is a 10th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 14, +6 to hit with spell attacks). The kuo-toa has the following cleric spells prepared:

* Cantrips (at will): _guidance, sacred flame, thaumaturgy_

* 1st level (4 slots): _detect magic, sanctuary, shield of faith_

* 2nd level (3 slots): _hold person, spiritual weapon_

* 3rd level (3 slots): _spirit guardians, tongues_

* 4th level (3 slots): _control water, divination_

* 5th level (2 slots): _mass cure wounds, scrying_

**Actions**

___Multiattack.___ The kuo-toa makes two melee attacks.

___Scepter.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage plus 14 (4d6) lightning damage.

___Unarmed Strike.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) bludgeoning damage.
