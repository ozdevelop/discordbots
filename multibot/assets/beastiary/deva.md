"Deva";;;_size_: Medium celestial
_alignment_: lawful good
_challenge_: "10 (5,900 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_skills_: "Insight +9, Perception +9"
_saving_throws_: "Wis +9, Cha +9"
_speed_: "30 ft., fly 90 ft."
_hit points_: "136 (16d8+64)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened"
_damage_resistances_: "radiant, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 18 (+4) | 18 (+4) | 17 (+3) | 20 (+5) | 20 (+5) |

___Angelic Weapons.___ The deva's weapon attacks are magical. When the deva hits with any weapon, the weapon deals an extra 4d8 radiant damage (included in the attack).

___Innate Spellcasting.___ The deva's spellcasting ability is Charisma (spell save DC 17). The deva can innately cast the following spells, requiring only verbal components:

* At will: _detect evil and good_

* 1/day each: _commune, raise dead_

___Magic Resistance.___ The deva has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The deva makes two melee attacks.

___Mace.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage plus 18 (4d8) radiant damage.

___Healing Touch (3/Day).___ The deva touches another creature. The target magically regains 20 (4d8 + 2) hit points and is freed from any curse, disease, poison, blindness, or deafness.

___Change Shape.___ The deva magically polymorphs into a humanoid or beast that has a challenge rating equal to or less than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the deva's choice).

In a new form, the deva retains its game statistics and ability to speak, but its AC, movement modes, Strength, Dexterity, and special senses are replaced by those of the new form, and it gains any statistics and capabilities (except class features, legendary actions, and lair actions) that the new form has but that it lacks.
