"Merrow";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Abyssal, Aquan"
_senses_: "darkvision 60 ft."
_speed_: "10 ft., swim 40 ft."
_hit points_: "45 (6d10+12)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 10 (0) | 15 (+2) | 8 (-1) | 10 (0) | 9 (-1) |

___Amphibious.___ The merrow can breathe air and water.

**Actions**

___Multiattack.___ The merrow makes two attacks: one with its bite and one with its claws or harpoon.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d4 + 4) slashing damage.

___Harpoon.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 11 (2d6 + 4) piercing damage. If the target is a Huge or smaller creature, it must succeed on a Strength contest against the merrow or be pulled up to 20 feet toward the merrow.