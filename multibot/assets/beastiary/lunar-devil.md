"Lunar Devil";;;_size_: Large fiend
_alignment_: lawful evil
_challenge_: "8 (3900 XP)"
_languages_: "Celestial, Draconic, Elvish, Infernal, Sylvan, telepathy 120 ft."
_skills_: "Perception +5"
_senses_: "darkvision 120 ft., passive Perception 15"
_saving_throws_: "Str +8, Dex +8, Con +8, Wis +5"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 60 ft. (hover), lightwalking 80 ft."
_hit points_: "94 (9d10 + 45)"
_armor class_: "16 (natural armor)"
_stats_: | 21 (+5) | 21 (+5) | 20 (+5) | 16 (+3) | 15 (+2) | 18 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Innate Spellcasting.___ The devil's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

At will: fly, major image, planar binding

3/day: greater invisibility

1/day: wall of ice

___Light Incorporeality.___ The devil is semi-incorporeal when standing in moonlight, and is immune to all nonmagical attacks in such conditions. Even when hit by spells or magic weapons, it takes only half damage from a corporeal source, with the exception of force damage. Holy water can affect the devil as it does incorporeal undead.

___Lightwalking.___ Once per round, the lunar devil magically teleports, along with any equipment it is wearing or carrying, from one beam of moonlight to another within 80 feet. This relocation uses half of its speed.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

___Variant: Devil Summoning.___  Summon Devil (1/Day): The devil can attempt a magical summoning. The devil has a 40 percent chance of summoning either 2 chain devils or 1 lunar devil.

**Actions**

___Multiattack.___ The devil makes three attacks: one with its bite, one with its claws, and one with its tail. Alternatively, it can use Hurl Moonlight twice.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 11 (1d12 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

___Tail.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 9 (1d8 + 5) bludgeoning damage.

___Hurl Moonlight.___ Ranged Spell Attack: +7 to hit, range 150 ft., one target. Hit: 19 (3d12) cold damage and the target must succeed on a DC 15 Constitution saving throw or become blinded for 4 rounds.

