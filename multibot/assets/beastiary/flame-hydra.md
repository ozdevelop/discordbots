"Flame Hydra";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "9 (5,000 XP)"
_languages_: "--"
_skills_: "Perception +9"
_senses_: "darkvision 60 ft., passive Perception 19"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "172 (15d12 + 75)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 20 (+5) | 3 (-4) | 12 (+1) | 7 (-2) |

___Multiple Heads.___ The hydra has five heads. While it
has more than one head, the hydra has advantage
on saving throws against being blinded, charmed,
deafened, frightened, stunned, and knocked
unconscious. Whenever the hydra takes 25 or more
damage in a single turn, one of its heads dies. If all
its heads die, the hydra dies. At the end of its turn,
it grows two heads for each of its heads that died
since its last turn, unless it has taken cold damage
since its last turn. The hydra regains 10 hit points
for each head regrown in this way and when a head
grows it releases a burst of flame. Each creature
within 10 feet of the hydra when a head regrows
must succeed on a DC 16 Dexterity saving throw or
take 11 (2d10) fire damage.

___Reactive Heads.___ For each head the hydra has beyond
one, it gets an extra reaction that can be used only
for opportunity attacks.

___Wakeful.___ While the hydra sleeps, at least one of its
heads is awake.

**Actions**

___Multiattack.___ The hydra makes as many bite or fire
blast attacks as it has heads.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft.,
one target. Hit: 10 (1d10 + 5) piercing damage.

___Fire Blast.___ Ranged Spell Attack: +9 to hit, range
60/240 ft., one target. Hit: 9 (2d8) fire damage.

___Inferno (Recharge 5-6).___ The hydra exhales fiery
breath from all of its heads, creating a 30 foot cone
of flame. Each creature in this area must make a DC
16 Dexterity saving throw, taking 7 (2d6) fire
damage for each living head on a failed save, or half
as much damage on a successful one.
