"Twig Blight";;;_size_: Small plant
_alignment_: neutral evil
_challenge_: "1/8 (25 XP)"
_languages_: "understands Common but can't speak"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_skills_: "Stealth +3"
_speed_: "20 ft."
_hit points_: "4 (1d6+1)"
_armor class_: "13 (natural armor)"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "blinded, deafened"
_stats_: | 6 (-2) | 13 (+1) | 12 (+1) | 4 (-3) | 8 (-1) | 3 (-4) |

___False Appearance.___ While the blight remains motionless, it is indistinguishable from a dead shrub.

**Actions**

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage.