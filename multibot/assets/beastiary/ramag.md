"Ramag";;;_size_: Medium humanoid
_alignment_: neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Common"
_skills_: "Arcana +5, Investigation +5"
_senses_: ", passive Perception 11"
_speed_: "30 ft."
_hit points_: "27 (6d8)"
_armor class_: "13 (leather armor)"
_stats_: | 9 (-1) | 14 (+2) | 10 (+0) | 16 (+3) | 12 (+1) | 11 (+0) |

___Magic Resistance.___ The ramag has advantage on saving throws against spells or other magical effects.

**Actions**

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

