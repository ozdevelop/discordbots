"Virulent Sorcerer";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages"
_skills_: "Arcana +4, Deception +6, Sleight of Hand +5"
_senses_: "passive Perception 11"
_saving_throws_: "Con +6, Cha +6"
_speed_: "30 ft."
_hit points_: "45 (6d6 + 24)"
_armor class_: "15 (draconic resilience)"
_stats_: | 10 (+0) | 14 (+2) | 16 (+3) | 12 (+1) | 12 (+1) | 16 (+3) |

___Draconic Resilience.___ The sorcerer's hit points are
increased by 1 per level and its AC is 13 + Dexterity
modifier.

___Poison Affinity.___ The sorcerer has green dragon
ancestry. When the sorcerer casts a spell that deals
poison damage, it deals 3 additional damage. In
addition, the sorcerer can spend one sorcery point
to gain resistance to poison for 1 hour.

___Spellcasting.___ The sorcerer is a 6th-level spellcaster.
Its spellcasting ability is Charisma (spell save DC
14, +6 to hit with spell attacks). The sorcerer has
the following sorcerer spells prepared:

* Cantrips (at will): _acid splash, dancing lights, poison spray_

* 1st level (4 slots): _false life, ray of sickness_

* 2nd level (3 slots): _crown of madness, mirror image, spider climb_

* 3rd level (3 slots): _fly, slow, stinking cloud_

___Sorcery Points.___ The sorcerer has 6 sorcery points. It
can spend 1 or more sorcery points as a bonus
action to gain one of the following benefits:
* Distant Spell. When the sorcerer casts a spell that
has a range of 5 feet or greater, it can spend 1
sorcery point to double the range of the spell.
When the sorcerer casts a spell that has a range
of touch, you can spend 1 sorcery point to make
the range of the spell 30 feet.
* Extended Spell. When the sorcerer casts a spell
that has a duration of 1 minute or longer, it can
spend 1 sorcery point to double its duration, to a
maximum of 24 hours.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 4
(1d4 + 2) piercing damage.
