"Green Hag";;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Draconic, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +3, Deception +4, Perception +4, Stealth +3"
_speed_: "30 ft."
_hit points_: "82 (11d8+33)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 16 (+3) | 13 (+1) | 14 (+2) | 14 (+2) |

___Amphibious.___ The hag can breathe air and water.

___Innate Spellcasting.___ The hag's innate spellcasting ability is Charisma (spell save DC 12). She can innately cast the following spells, requiring no material components:

At will: dancing lights, minor illusion, vicious mockery

___Mimicry.___ The hag can mimic animal sounds and humanoid voices. A creature that hears the sounds can tell they are imitations with a successful DC 14 Wisdom (Insight) check.

___Hag Coven.___ When hags must work together, they form covens, in spite of their selfish natures. A coven is made up of hags of any type, all of whom are equals within the group. However, each of the hags continues to desire more personal power.

A coven consists of three hags so that any arguments between two hags can be settled by the third. If more than three hags ever come together, as might happen if two covens come into conflict, the result is usually chaos.

___Shared Spellcasting (Coven Only).___ While all three members of a hag coven are within 30 feet of one another, they can each cast the following spells from the wizard's spell list but must share the spell slots among themselves.  For casting these spells, each hag is a 12th-level spellcaster that uses Intelligence as her spellcasting ability (spell save DC 13, +5 to hit with spell attacks):

* 1st level (4 slots): _identify, ray of sickness_

* 2nd level (3 slots): _hold person, locate object_

* 3rd level (3 slots): _bestow curse, counterspell, lightning bolt_

* 4th level (3 slots): _phantasmal killer, polymorph_

* 5th level (2 slots): _contact other plane, scrying_

* 6th level (1 slot): _eye bite_

___Hag Eye (Coven Only).___ A hag coven can craft a magic item called a hag eye, which is made from a real eye coated in varnish and often fitted to a pendant or other wearable item. The hag eye is usually entrusted to a minion for safekeeping and transport. A hag in the coven can take an action to see what the hag eye sees if the hag eye is on the same plane of existence. A hag eye has AC 10, 1 hit point, and darkvision with a radius of 60 feet. If it is destroyed, each coven member takes 3d10 psychic damage and is blinded for 24 hours.

A hag coven can have only one hag eye at a time, and creating a new one requires all three members of the coven to perform a ritual. The ritual takes 1 hour, and the hags can't perform it while blinded. During the ritual, if the hags take any action other than performing the ritual, they must start over.

**Actions**

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

___Illusory Appearance.___ The hag covers herself and anything she is wearing or carrying with a magical illusion that makes her look like another creature of her general size and humanoid shape. The illusion ends if the hag takes a bonus action to end it or if she dies.

The changes wrought by this effect fail to hold up to physical inspection. For example, the hag could appear to have smooth skin, but someone touching her would feel her rough flesh. Otherwise, a creature must take an action to visually inspect the illusion and succeed on a DC 20 Intelligence (Investigation) check to discern that the hag is disguised.

___Invisible Passage.___ The hag magically turns invisible until she attacks or casts a spell, or until her concentration ends (as if concentrating on a spell). While invisible, she leaves no physical evidence of her passage, so she can be tracked only by magic. Any equipment she wears or carries is invisible with her.
