"Buraq";;;_size_: Medium celestial
_alignment_: lawful good
_challenge_: "11 (7200 XP)"
_languages_: "Celestial, Common, Primordial, telepathy 120 ft."
_skills_: "History +8, Religion +8"
_senses_: "truesight 120 ft., passive Perception 14"
_saving_throws_: "Con +9, Wis +8, Cha +9"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened"
_speed_: "60 ft., fly 90 ft."
_hit points_: "152 (16d8 + 80)"
_armor class_: "17"
_stats_: | 15 (+2) | 18 (+4) | 20 (+5) | 18 (+4) | 18 (+4) | 20 (+5) |

___Angelic Weapons.___ The buraq's attacks are magical. When the buraq hits with its hooves, it deals an extra 4d8 radiant damage (included in the attack).

___Innate Spellcasting.___ The buraq's innate spellcasting ability is Charisma (spell save DC 17). The buraq can innately cast the following spells, requiring no components:

At will: comprehend languages, detect evil and good, holy aura, pass without trace

3/day each: haste, longstrider

1/day each: plane shift, wind walk

___Magic Resistance.___ The buraq has advantage on saving throws against spells and other magical effects.

___Night Journey.___ When outdoors at night, a buraq's vision is not limited by nonmagical darkness. Once per month, the buraq can declare it is on a night journey; for the next 24 hours, it can use its Teleport once per round. Its destination must always be in an area of nonmagical darkness within its line of sight. At any point during the night journey, as a bonus action, the buraq can return itself and its rider to the location where it began the night journey.

**Actions**

___Multiattack.___ The buraq makes two attacks with its hooves.

___Hooves.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage plus 18 (4d8) radiant damage.

___Teleport (1/Day).___ The buraq magically teleports itself and its rider, along with any equipment it is wearing or carrying, to a location the buraq is familiar with, up to 1 mile away.

