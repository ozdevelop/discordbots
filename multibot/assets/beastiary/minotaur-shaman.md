"Minotaur Shaman";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Abyssal"
_skills_: "Perception +8"
_senses_: "darkvision 60ft., passive Perception 18"
_speed_: "40 ft."
_hit points_: "60 (8d10 + 16)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 11 (+0) | 15 (+2) | 8 (-1) | 18 (+4) | 10 (+0) |

___Labyrinthine Recall.___ The minotaur can perfectly recall
any path it has traveled.

___Spellcasting.___ The shaman is a 5th-level spellcaster.
Its spellcasting ability is Wisdom (spell save DC 14, +6 to hit with spell attacks). The shaman has the
following cleric spells prepared:

* 1st level (4 slots): _bane, faerie fire_

* 2nd level (3 slots): _barkskin, enhance ability, silence_

* 3rd Level (2 slots): _bestow curse, spirit guardians_

**Actions**

___Greatclub.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 10 (2d6 + 3) bludgeoning
damage.

___Gore.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 9 (1d12 + 3) piercing damage.
