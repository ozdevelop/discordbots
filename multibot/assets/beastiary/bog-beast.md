"Bog Beast";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +5, Survival +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "30 ft."
_hit points_: "76 (8d10 + 32)"
_armor class_: "13 (natural armor)"
_stats_: | 20 (+5) | 11 (+0) | 18 (+4) | 5 (-3) | 12 (+1) | 9 (-1) |

___Keen Smell.___ The bog beast has advantage on Wisdom (Perception)
checks based on smell.

**Actions**

___Multiattack.___ The bog beast makes two attacks
with its claws.

___Claws.___ Melee Weapon Attack: +7 to hit,
reach 10 ft., one target. Hit: 12 (2d6 + 5)
slashing damage. If the target is a creature, it
must succeed on a DC 14 Constitution saving
throw against disease or become poisoned until
the disease is cured. Every 24 hours that elapse, the
target must repeat the saving throw, reducing its hit
point maximum by 5 (1d10) on a failure. The disease
is cured on a success. The target dies if the disease
reduces its hit point maximum to 0. This reduction of
the target’s hit point maximum lasts until
the disease is cured.
