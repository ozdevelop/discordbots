"Phooka";;;_size_: Small fey
_alignment_: chaotic neutral
_challenge_: "2 (450 XP)"
_languages_: "Common, Sylvan"
_skills_: "Perception +6, Stealth +5"
_senses_: "passive Perception 16"
_speed_: "30 ft."
_hit points_: "27 (6d6 + 6)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 17 (+3) | 12 (+1) | 13 (+1) | 15 (+2) | 18 (+4) |

___One with Nature.___ When a phooka is slain through violence, all plants
within a 100-foot radius of where it fell die and no new ones grow
naturally in that area for 1 year.

___Tree Stride.___ Once on its turn, the phooka can use 10 feet of its
movement to step magically into one living tree within its reach and
emerge from a second living tree within 60 feet of the first tree, appearing
in an unoccupied space within 5 feet of the second tree. Both trees must
be Large or bigger.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.
