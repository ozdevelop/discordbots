"Sand Silhouette";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "all languages it knew in life"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 11"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, grappled, frightened, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 17 (+3) | 7 (-2) | 12 (+1) | 10 (+0) |

___Camouflage.___ While in desert environments, the sand silhouette can use the Hide action even while under direct observation.

___Sand Form.___ The sand silhouette can move through a space as narrow as 1 inch wide without squeezing.

___Sand Glide.___ The sand silhouette can burrow through nonmagical, loose sand without disturbing the material it is moving through. It is invisible while burrowing this way.

___Vulnerability to Water.___ For every 5 feet the sand silhouette moves while touching water, or for every gallon of water splashed on it, it takes 2 (1d4) cold damage. If the sand silhouette is completely immersed in water, it takes 10 (4d4) cold damage.

**Actions**

___Multiattack.___ The sand silhouette makes two slam attacks.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (3d6 + 4) bludgeoning damage. If both attacks hit a Medium or smaller target, the target is grappled (escape DC 14), and the sand silhouette engulfs it.

___Engulf.___ The sand silhouette engulfs a Medium or smaller creature grappled by it. The engulfed target is blinded and restrained, but no longer grappled. It must make a successful DC 15 Constitution saving throw at the start of each of the sand silhouette's turns or take 14 (3d6 + 4) bludgeoning damage. If the sand silhouette moves, the engulfed target moves with it. The sand silhouette can only engulf one creature at a time.

___Haunted Haboob (Recharge 4-6).___ The sand silhouette turns into a 60-foot radius roiling cloud of dust and sand filled with frightening shapes. A creature that starts its turn inside the cloud must choose whether to close its eyes and be blinded until the start of its next turn, or keep its eyes open and make a DC 15 Wisdom saving throw. If the saving throw fails, the creature is frightened for 1 minute. A frightened creature repeats the Wisdom saving throw at the end of each of its turns, ending the effect on itself on a success.

