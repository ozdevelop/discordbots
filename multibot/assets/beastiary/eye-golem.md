"Eye Golem";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "11 (7200 XP)"
_languages_: "understands the language of its creator, but can't speak"
_skills_: "Perception +8"
_senses_: "truesight 120 ft., passive Perception 18"
_damage_immunities_: "fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhausted, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "157 (15d10 + 75)"
_armor class_: "20 (natural armor)"
_stats_: | 22 (+6) | 9 (-1) | 20 (+5) | 5 (-3) | 11 (+0) | 1 (-5) |

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

**Actions**

___Multiattack.___ The golem makes two melee attacks.

___Slam.___ Melee Weapon Attack: +10 to hit, reach 5ft., one target. Hit: 24 (4d8 + 6) bludgeoning damage.

___Gaze of Ancient Light (Recharge 6).___ The golem emits a burst of blinding light, affecting all opponents within 30 feet who are visible to it. These creatures must make a successful DC 17 Constitution saving throw or be permanently blinded. All affected creatures, including those that save successfully, are stunned until the end of their next turn.

___Primal Voice of Doom (1/Day).___ The golem intones a disturbing invocation of the sun god. Creatures within 30 feet of the golem must make a successful DC 17 Wisdom saving throw or become frightened Deaf or unhearing creatures are unaffected.

___Shoot into the Sun (1 minute/day).___ When roused for combat, the golem opens many of its eyes, emitting blinding light. All ranged attacks, including ranged spells that require a spell attack roll, are made with disadvantage against the golem. The effect persists as long as the eye golem desires, up to a total of 1 minute (10 rounds) per day.

