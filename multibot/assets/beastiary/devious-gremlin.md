"Devious Gremlin";;;_size_: Tiny fiend (devil)
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Infernal, Common"
_skills_: "Animal Handling +2, Deception +2, Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 10"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "7 (2d4 + 2)"
_armor class_: "13"
_stats_: | 5 (-3) | 16 (+3) | 12 (+1) | 12 (+1) | 10 (+0) | 10 (+0) |

___Devil's Sight.___ Magical darkness doesn't impede the
gremlin's vision.

___Extradimensional Pouch.___ The gremlin has a pouch
that acts as a personal extradimensional space. This
pouch can hold up to 8 cubic feet of items
weighing a total of up to 300 pounds. When the
gremlin dies, the contents of its pouch explode out
of its body in a violent eruption.

___Magic Resistance.___ The gremlin has advantage on
saving throws against spells and other magical
effects.

___Mutable Form.___ The gremlin can move through a
space as narrow as one inch wide while squeezing.

___Tamer of Beasts.___ The gremlin has advantage on
Wisdom (Animal Handling) checks and Charisma
(Persuasion) checks when interacting with Small or
smaller beasts. Through sounds and gestures, it can
also communicate with Small or smaller beasts.

**Actions**

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one creature. Hit: 5 (1d4 + 3) slashing damage plus
2 (1d4) poison damage.
