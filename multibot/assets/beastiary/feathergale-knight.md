"Feathergale Knight";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Auran, Common"
_skills_: "Animal Handling +2, History +2"
_speed_: "30 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "16 (scale)"
_stats_: | 14 (+2) | 14 (+2) | 12 (+1) | 11 (0) | 10 (0) | 14 (+2) |

___Spellcasting.___ The knight is a 1st-level spellcaster. Its spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). It knows the following sorcerer spell:

* Cantrips (at will): _gust, light, message, ray of frost_

* 1st level (2 slots): _expeditious retreat, feather fall_

**Actions**

___Multiattack.___ The knight makes two melee attacks.

___Longsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage, or 7 (1d10 + 2) slashing damage if used with two hands.

___Spear.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d6 + 2) piercing damage, or 6 (1d8 + 2) piercing damage if used with two hands to make a melee attack.
