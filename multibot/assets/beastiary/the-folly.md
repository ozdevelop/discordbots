"The Folly";;;_size_: Tiny fey
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Draconic, Sylvan"
_skills_: "Arcana +4, Athletics +5, Perception +1, Persuasion +7"
_senses_: "darkvision 120 ft., passive Perception 11"
_saving_throws_: "Dex +5, Con +6"
_speed_: "15 ft., fly 30 ft."
_hit points_: "71 (13d4 + 39)"
_armor class_: "16 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 17 (+3) | 12 (+1) | 7 (-2) | 19 (+4) |

___Innate Spellcasting.___ The folly’s spellcasting ability is Charisma (spell
save DC 15, +7 to hit with spell attacks). It can innately cast the following
spells, requiring no material components:

* At will: _acid splash, chill touch, eldritch blast, mage hand, minor
illusion, poison spray, prestidigitation, shocking grasp, vicious mockery_

* 3/day each: _disguise self, flaming sphere, hideous laughter, invisibility,
phantasmal killer, silent image, sleep, suggestion_

* 1/day: _dispel magic, fear, fireball, hypnotic pattern_

* 1/week each: _animate objects, arcane gate, polymorph _(10x normal
casting time and requires at least 3 follies participating in the ritual)

* 1/month each: _earthquake, gate, reverse gravity _(100x normal casting
time and requires at least 10 follies participating in the ritual)

___Quick Breeding.___ The follies both breed and mature very quickly, but
they also don’t much care about self-preservation. Therefore, even though
they often do things that get themselves killed, their population never
seems to drop.

___Wild Misperception.___ The follies misunderstand almost anything said
to them in the most horrible possible way. For example, if asked, "Please
make those children new shoes," they will happily slay the children and
use their skins to make new shoes. They don't do this on purpose. They
just get absolutely everything horribly, horribly wrong whenever it is at all
possible to do so. Even when they do properly understand a request, they
will find a horrible way to execute it. For example, if asked, "Please bring
me a glass of water," they might steal a poor family’s only nice glass or
open a gateway to the elemental plane of water, flooding the town, just to
fill the glass. Everything they can do wrong, they will.

**Actions**

___+1 Tiny Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 4 (1d2 + 3) slashing damage.

___+1 Tiny Shortbow.___ Ranged Weapon Attack: +6 to hit, range 40/160 ft., one target. Hit: 4 (1d2 + 3) piercing damage.
