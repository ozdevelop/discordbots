"Wharfling Swarm";;;_size_: Large swarm
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "--"
_skills_: "Perception +3, Sleight of Hand +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "30 ft., climb 30 ft., swim 20 ft."
_hit points_: "63 (14d10 . 14)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 16 (+3) | 8 (-1) | 2 (-4) | 12 (+1) | 7 (-2) |

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a tiny wharfling. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Bites.___ Melee Weapon Attack: +5 to hit, reach 0 ft., one creature in the swarm's space. Hit: 21 (6d6) piercing damage, or 10 (3d6) piercing damage if the swarm has half of its hit points or fewer.

___Locking Bite.___ When a creature leaves a wharfling swarm's space, 1d3 wharflings remain grappled to them (escape DC 10). Each wharfling inflicts 5 (1d4 + 3) piercing damage at the start of the creature's turns until it escapes from the grapples.

___Pilfer.___ A wharfling swarm makes 1d6 Dexterity (Sleight of Hand) checks each round against every creature in the swarm's space. The DC for each check equals 10 plus the target creature's Dexterity modifier. For each successful check, the wharflings steal some small metallic object from the target, and the theft is unnoticed if the same result equals or exceeds the target's passive Perception.

