"Tressym";;;_size_: Tiny beast
_alignment_: chaotic neutral
_challenge_: "0 (10 XP)"
_skills_: "Perception +5, Stealth +4"
_damage_immunities_: "poison"
_speed_: "40 ft., climb 30 ft., fly 40' ft."
_hit points_: "5 (2d4)"
_armor class_: "12"
_condition_immunities_: "poisoned"
_stats_: | 3 (-4) | 15 (+2) | 10 (0) | 11 (0) | 12 (+1) | 12 (+1) |

___Detect Invisibility.___ Within 50 feet of the tressym, magical invisibility fails to conceal anything from the tressym's sight

___Keen Smell.___ The tressym has advantage on Wisdom (Perception) checks that rely on smell.

___Poison Sense.___ The tressym can detect whether a substance is poisonous by taste, touch, or smell.

**Actions**

___Claws.___ Melee Weapon Attack: +0 to hit, reach 5 ft., one target. Hit: 1 slashing damage.