"Choker";;;_page_number_: 123
_size_: Small aberration
_alignment_: chaotic evil
_challenge_: "1 (100 XP)"
_languages_: "Deep Speech"
_senses_: "darkvision 60 ft., passive Perception 11"
_skills_: "Stealth +6"
_speed_: "30 ft."
_hit points_: "13  (3d6 + 3)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 14 (+2) | 13 (+1) | 4 (-3) | 12 (+1) | 7 (-1) |

___Aberrant Quickness (Recharges after a Short or Long Rest).___ The choker can take an extra action on its turn.

___Boneless.___ The choker can move through and occupy a space as narrow as 4 inches wide without squeezing.

___Spider Climb.___ The choker can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack___ The choker makes two tentacle attacks.

___Tentacle___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 5 (1d4 + 3) bludgeoning damage plus 3 (1d6) piercing damage. If the target is a Large or smaller creature, it is grappled (escape DC 15). Until this grapple ends, the target is restrained, and the choker can't use this tentacle on another target. The choker has two tentacles. If this attack is a critical hit, the target also can't breathe or speak until the grapple ends.