"Dark Witness";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "understands Common, Deep Speech, and Undercommon, but cannot speak"
_senses_: "Darkvision 120 ft., Passive Perception 14"
_skills_: "Perception +4"
_condition_immunities_: "Prone"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "45 (6d8 + 18)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (+0) | 14 (+2) | 16 (+3) | 7 (-2) | 14 (+2) | 10 (+0) |

**Actions**

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft.,
Hit: 5 (1d8) piercing damage.

___Aberrant Gaze.___ The dark witness targets up to
two creatures it can see within 120 ft. Eyes from
two of its faces use one of the following effects on
the target. The dark witness can use each effect
only once per turn.
* **Paralyzing Gaze.** The target creature must
succeed on a DC 14 Constitution saving throw or
be paralyzed for 1 minute. The target can repeat
the saving throw at the end of each of its turns,
ending the effect on a success.
* **Fear Gaze.** The target creature must succeed
on a DC 14 Wisdom saving throw or be frightened
of the dark witness for 1 minute. The target can
repeat the saving throw at the end of each of its
turns, ending the effect on a success.
* **Shocking Gaze.** The target creature must succeed
on a DC 14 Dexterity saving throw or take 28
(8d6) lightning damage on a failed save, or half as
much on a successful one.
* **Forceful Gaze.** The target creature must succeed
on a DC 14 Strength saving throw or be
moved up to 30 ft. in any direction. The target is
restrained by the gaze’s telekinetic grip until the
start of the dark witness’s next turn or until the
dark witness is incapacitated.

**Reactions**

___Phase (3/day).___ If a dark witness fails a saving
throw or check that would restrain, paralyze, or
grapple it, it can use its reaction to briefly phase
into an aberrant plane and repeat the saving
throw.
