"Ghast";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft."
_damage_immunities_: "necrotic"
_speed_: "30 ft."
_hit points_: "36 (8d8)"
_armor class_: "13"
_condition_immunities_: "poisoned"
_stats_: | 16 (+3) | 17 (+3) | 10 (0) | 11 (0) | 10 (0) | 8 (-1) |

___Stench.___ Any creature that starts its turn within 5 ft. of the ghast must succeed on a DC 10 Constitution saving throw or be poisoned until the start of its next turn. On a successful saving throw, the creature is immune to the ghast's Stench for 24 hours.

___Turn Defiance.___ The ghast and any ghouls within 30 ft. of it have advantage on saving throws against effects that turn undead.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 12 (2d8 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage. If the target is a creature other than an undead, it must succeed on a DC 10 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.