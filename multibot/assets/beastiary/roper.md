"Roper";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +6, Stealth +5"
_speed_: "10 ft., climb 10 ft."
_hit points_: "93 (11d10+33)"
_armor class_: "20 (natural armor)"
_stats_: | 18 (+4) | 8 (-1) | 17 (+3) | 7 (-2) | 16 (+3) | 6 (-2) |

___False Appearance.___ While the roper remains motionless, it is indistinguishable from a normal cave formation, such as a stalagmite.

___Grasping Tendrils.___ The roper can have up to six tendrils at a time. Each tendril can be attacked (AC 20; 10 hit points; immunity to poison and psychic damage). Destroying a tendril deals no damage to the roper, which can extrude a replacement tendril on its next turn. A tendril can also be broken if a creature takes an action and succeeds on a DC 15 Strength check against it.

___Spider Climb.___ The roper can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack.___ The roper makes four attacks with its tendrils, uses Reel, and makes one attack with its bite.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 22 (4d8 + 4) piercing damage.

___Tendril.___ Melee Weapon Attack: +7 to hit, reach 50 ft., one creature. Hit: The target is grappled (escape DC 15). Until the grapple ends, the target is restrained and has disadvantage on Strength checks and Strength saving throws, and the roper can't use the same tendril on another target.

___Reel.___ The roper pulls each creature grappled by it up to 25 ft. straight toward it.