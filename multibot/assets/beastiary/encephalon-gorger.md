"Encephalon Gorger";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Deep Speech, telepathy 120 ft."
_skills_: "Perception +5, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "30 ft."
_hit points_: "71 (11d8 + 22)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 20 (+5) | 15 (+2) | 15 (+2) |

___Alien Mind.___ Encephalon gorgers can maintain concentration on 3
simultaneous spell effects.

___Mindsense.___ The encephalon gorger is aware of the presence of
creatures within 300 feet of it that have an Intelligence of 3 or higher.
It knows the relative distance and direction of each creature, as well as
the creature’s approximate Intelligence score (within 3 points). Creatures
under the effects of magic that protects the mind cannot be detected by the
encephalon gorger.

___Mind Screen.___ The mind of an encephalon gorger is an alien and
dangerous place. Should a creature attempt to scan the mind or read the
thoughts of an encephalon gorger (with _detect thoughts_, telepathy, or
the like), it must succeed on a DC 15 Intelligence saving throw or be
driven insane, gaining a flaw from the Indefinite Madness table (see the
SRD). On a successful save, the creature is confused for 1 minute (as the
_confusion_ spell).

**Actions**

___Multiattack.___ The encephalon gorger makes two attacks with its claws
and uses Mindfeed if it has a creature grappled.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) slashing damage If the target is Medium or smaller, it is grappled
(escape DC 16). Until this grapple ends, the target is restrained, and the
encephalon gorger can only use its Mindfeed on the grappled creature and
has advantage on attack rolls to do so.

___Mindfeed.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature that
is grappled by the encephalon gorger. Hit: 7 (1d8 + 3) piercing damage,
and the target must succeed on a DC 15 Intelligence saving throw, or take
33 (6d10), and the target’s Intelligence score is reduced by 1d4. The target
dies if this reduces its Intelligence to 0. Otherwise, the reduction lasts until
the target finishes a long rest.

___Adrenal Surge (2/day).___ The encephalon gorger surges with adrenaline
until the end of its turn. While under this effect, it gains a +2 bonus to its
AC, it has advantage on Dexterity saving throw, and it gains an additional
action on its turn (as the haste spell).
