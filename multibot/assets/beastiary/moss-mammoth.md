"Moss Mammoth";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "8 (3,900 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_speed_: "40 ft."
_hit points_: "138 (12d12 + 60)"
_armor class_: "15 (natural armor)"
_stats_: | 25 (+7) | 8 (-1) | 20 (+5) | 6 (-2) | 12 (+1) | 6 (-2) |

___Floral Camouflage.___ While the mammoth remains
motionless and is lying down, it is indistinguishable
from a large moss-covered boulder.

___Sticky Moss Exterior.___ When a creature hits the
mammoth with a melee weapon attack, there is a
chance it becomes entangled in the sticky moss that
coats the mammoth. The attacker must succeed on a
DC 13 Strength saving throw, or the weapon becomes
stuck to the mammoth's moss. If the weapon's wielder
can't or won't let go of the weapon, the wielder is
grappled while the weapon is stuck. While stuck, the
weapon can't be used. A creature can pull the weapon
free by taking an action to make a DC 13 Strength
check and succeeding.

**Actions**

___Multiattack.___ The mammoth makes two attacks: one
with its trunk slam and one with either its gore or
stomp.

___Gore.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one
target. Hit: 20 (3d8 + 7) piercing damage. If the target
is a creature, it must succeed on a DC 15 Constitution
saving throw or become infected with vicious parasitic
spores.

The effects of these spores go unnoticed until three
days have passed, after which the creature will begin to
grow moss around the source of the wound. On each
day following, the moss continues to spread and
reduces the maximum hit points of that creature by 5.
When the creature hits 0 maximum hit points they die
and are consumed by the moss.

This moss can be destroyed via greater restoration or
by exposing the infected creature to continuous
powerful heat for 24 hours which causes the moss to
dry up and die off.

If an infected creature is ever within the range of a
Plant Growth spell, the moss spreads explosively and
consumes the creature instantly.

___Stomp.___ Melee Weapon Attack: +10 to hit, reach 5 ft.,
one prone creature. Hit: 29 (4d10 + 7) bludgeoning
damage.

___Trunk Slam.___ Melee Weapon Attack: +10 to hit, reach 10
ft., one creature. Hit: 21 (4d6 + 7) bludgeoning
damage, and if the creature is of size Medium or
smaller the mammoth can choose to either throw the
target or slam it to the ground. If the mammoth throws
the target, it lands in a space of the mammoth's
choosing within 30 feet and must succeed on a DC 18
Dexterity saving throw or fall prone. If the mammoth
slams the target, it takes an additional 7 (2d6)
bludgeoning damage and is knocked prone at the
mammoth's feet.
