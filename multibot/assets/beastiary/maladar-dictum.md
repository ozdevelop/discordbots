"Maladar Dictum";;;_size_: Huge aberration
_alignment_: chaotic neutral
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Deep Speech, Primordial"
_skills_: "Athletics +9, Deception +7, Insight +4, Intimidation +7"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Str +9, Dex +6, Con +8, Wis +4, Cha +7"
_damage_resistances_: "psychic"
_condition_immunities_: "charmed, petrified"
_speed_: "30 ft."
_hit points_: "210 (20d12 + 80)"
_armor class_: "20 (plate, shield)"
_stats_: | 20 (+5) | 15 (+2) | 19 (+4) | 15 (+2) | 10 (+0) | 16 (+3) |

___Formless Shape.___ Maladar is immune to any spell or
effect that would alter his form.

___Reform.___ Each time Maladar takes damage in
combat, his arm grows another limb, increasing
the damage of his smash attack by 1d8. There is no
limit to the number of limbs, tentacles, and claws
Maladar can grow this way.

**Actions**

___Multiattack.___ Maladar makes two attacks with
Changer plus one smash attack.

___Changer (Greatsword).___ Melee Weapon Attack: +9
to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage plus 7 (2d6) necrotic damage.

___Smash.___ Melee Weapon Attack: +9 to hit, reach 10 ft.,
one target. Hit: 14 (2d8 + 5) bludgeoning damage.

___Lord of Chaos.___ Maladar has a 50% chance of
summoning a member of his court in an adjacent unoccupied space. If successful, roll a d12
and consult the below chart. Each member of his
court may only be summoned once per day in this
manner. If the same member is summoned twice,
nothing happens.
* 1-4: Baron Malgas
* 5–7: Korsoth Vastikan
* 8–9: The Queen of Bones
* 10–11: Lord Rall
* 12: Uursovk
