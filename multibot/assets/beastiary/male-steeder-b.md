"Male Steeder (B)";;;_page_number_: 238
_size_: Medium monstrosity
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "-"
_senses_: "darkvision 120 ft., passive Perception 14"
_skills_: "Stealth +5, Perception +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "13  (2d8 + 4)"
_armor class_: "12 (natural armor)"
_stats_: | 15 (+2) | 12 (+1) | 14 (+2) | 2 (-4) | 10 (0) | 3 (-3) |

___Spider Climb.___ The steeder can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Extraordinary Leap.___ The distance of the steeder's long jumps is tripled; every foot of its walking speed that it spends on the jump allows it to jump 3 feet.

**Actions**

___Bite___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage plus 4 (1d8) poison damage.

___Sticky Leg___ Melee Weapon Attack: +4 to hit, reach 5 ft., one Small or Tiny creature. Hit: The target is stuck to the steeder's leg and is grappled until it escapes (escape DC 12). The steeder can have only one creature grappled at a time.