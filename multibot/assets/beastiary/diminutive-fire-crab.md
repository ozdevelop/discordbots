"Diminutive Fire Crab";;;_size_: Small elemental
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "understands Ignan but can't speak"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 10"
_damage_immunities_: "fire"
_damage_vulnerabilities_: "cold"
_condition_immunities_: "charmed"
_speed_: "20 ft., swim 20 ft."
_hit points_: "22 (4d6 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 14 (+2) | 14 (+2) | 14 (+2) | 2 (-4) | 10 (+0) | 2 (-4) |

___Heated Body.___ A creature that touches the fire crab or hits it with a melee
attack while within 5 feet of it takes 3 (1d6) fire damage.

**Actions**

___Multiattack.___ The diminutive fire crab makes two claw attacks.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4
(1d4 + 2) bludgeoning damage plus 3 (1d6) fire damage.
