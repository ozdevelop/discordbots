"Cultist";;;_size_: Medium humanoid (any race)
_alignment_: any non-good alignment
_challenge_: "1/8 (25 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Deception +2, Religion +2"
_speed_: "30 ft."
_hit points_: "9 (2d8)"
_armor class_: "12 (leather armor)"
_stats_: | 11 (0) | 12 (+1) | 10 (0) | 10 (0) | 11 (0) | 10 (0) |

___Dark Devotion.___ The cultist has advantage on saving throws against being charmed or frightened.

**Actions**

___Scimitar.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 4 (1d6 + 1) slashing damage.