"White Tusk Shaman";;;_size_: Medium humanoid (orc, shapechanger)
_alignment_: chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "Orc, Primordial"
_skills_: "Perception +5, Survival +5"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Dex +4, Wis +5"
_speed_: "30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "14 (hide armor)"
_stats_: | 17 (+3) | 15 (+2) | 14 (+2) | 9 (-1) | 16 (+3) | 8 (-1) |

___Aggressive.___ As a bonus action, the orc can
move up to its speed toward a hostile creature it
can see.

___Minion: Savage Horde.___ After moving at least 20
feet in a straight line toward a creature, the next
attack the orc makes against that creature scores
a critical hit on a roll of 18–20.

___Spirit-Bonded Body (Recharges after a Short
or Long Rest).___ As a bonus action, the orc can
transform into a dire wolf for up to 4 hours. The
orc can choose whether its equipment falls to
the ground, melds with its new form, or is worn
by the new form. The orc reverts to its true form
when its dire wolf form is reduced to 0 hit points.
If this damage would cause its dire wolf form to
drop below 0 hit points, the excess damage is
done to its true form.

The orc also reverts to its true form if it dies
or falls unconscious. The orc can revert to its
true form as a bonus action on its turn. When it
reverts in this way, it returns to the number of hit
points it had before it transformed.

___Spirit-Bonded Mind.___ The orc can cast speak
with animals at will, but can only communicate
with wolves. 


**Actions**

___Multiattack.___ The White Tusk Shaman makes two
blood-searing spear attacks.

___Blood-Searing Spear.___ Melee or Ranged Weapon
Attack: +5 to hit, reach 5 ft. or range 30/60 ft.,
one target. Hit: 6 (1d6 + 3) piercing damage plus
7 (2d6) poison damage. This magical poison only
functions when the spear is wielded by an orc
shaman.
