"Tortle";;;_page_number_: 242
_size_: Medium humanoid (tortle)
_alignment_: lawful good
_challenge_: "1/4 (50 XP)"
_languages_: "Aquan, Common"
_senses_: "passive Perception 11"
_skills_: "Athletics +4, Survival +3"
_speed_: "30 ft."
_hit points_: "22  (4d8 + 4)"
_armor class_: "17 (natural)"
_stats_: | 15 (+2) | 10 (0) | 12 (+1) | 11 (0) | 13 (+1) | 12 (+1) |

___Hold Breath.___ The tortle can hold its breath for 1 hour.

**Actions**

___Claws___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.

___Quarterstaff___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage, or 6 (1d8 + 2) bludgeoning damage if used with two hands.

___Light Crossbow___ Ranged Weapon Attack: +2 to hit, range 80/320 ft., one target. Hit: 4 (1d8) piercing damage.

___Shell Defense___ The tortle withdraws into its shell. Until it emerges, it gains a +4 bonus to AC and has advantage on Strength and Constitution saving throws. While in its shell, the tortle is prone, its speed is 0 and can't increase, it has disadvantage on Dexterity saving throws, it can't take reactions, and the only action it can take is a bonus action to emerge.