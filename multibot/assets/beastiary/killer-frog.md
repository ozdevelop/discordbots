"Killer Frog";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "passive Perception 13"
_speed_: "30 ft., swim 30 ft."
_hit points_: "11 (2d6 + 4)"
_armor class_: "12 (natural armor)"
_stats_: | 12 (+1) | 13 (+1) | 14 (+2) | 2 (-4) | 9 (-1) | 6 (-2) |

___Amphibious.___ The killer frog can breathe air and water.

___Keen Smell.___ The killer frog has advantage on Wisdom (Perception)
checks that rely on smell.

___Standing Leap.___ The frog’s long jump is up to 30 feet and its high jump
is up to 20 feet, with or without a running start.

**Actions**

___Multiattack.___ The killer frog makes two attacks:
one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5
ft., one target. Hit: 3 (1d4 + 1) piercing damage.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3
(1d4 + 1) slashing damage.
