"Stone Juggernaut";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "12 (8,400 XP)"
_senses_: "blindsight 120 ft., passive Perception 10"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned, prone"
_damage_immunities_: "poison, bludgeoning, piercing, and slashing damage from nonmagical attacks not made with adamantine weapons"
_speed_: "50 ft. (in one direction chosen at the start of its turn)"
_hit points_: "157 (15d10 +75)"
_armor class_: "15 (natural armor)"
_stats_: | 22 (+6) | 1 (-5) | 21 (+5) | 2 (-4) | 11 (0) | 3 (-4) |

___Devastating Roll.___ The juggernaut can move through the space of a prone creature. A creature whoses pace the juggernaut enters for the first time on a turn must make a DC 17 Dexterity saving throw, taking 55 (lOdlO) bludgeoning damage on a failed save, or half as much damage on a successful one.

___Immutable Form.___ The juggernaut is immune to any spell or effect that would alter its form.

___Regeneration.___ As long as it has 1 hit point left, the juggernaut magically regains all its hit points daily at dawn. The juggernaut is destroyed and does not regenerate if it drops to 0 hit points.

___Siege Monster.___ The juggernaut deals double damage to objects and structures.

**Actions**

___Slam.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 25 (3dl2 +6) bludgeoning damage. If the target is a Large or smaller creature, it must succeed on a DC 17 Strength saving throw or be knocked prone.