"Animated Table";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_damage_immunities_: "poison, psychic"
_speed_: "40 ft."
_hit points_: "39 (6d10+6)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 18 (+4) | 8 (-1) | 13 (+1) | 1 (-5) | 3 (-4) | 1 (-5) |

___Source.___ tales from the yawning portal,  page 230

___Constructed Nature.___ An animated table doesn't require air, food, drink, or sleep.

___Antimagic Susceptibility.___ The table is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the table must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute.

___False Appearance.___ While the table remains motionless, it is indistinguishable from a normal table.

___Charge.___ If the table moves at least 20 feet straight toward a target and then hits it with a ram attack on the same turn, the target takes an extra 9 (2d8) bludgeoning damage. If the target is a creature, it must succeed on a DC 15 Strength saving throw or be knocked prone.

**Actions**

___Ram.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.