"Hell Moth";;;_size_: Large aberration
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_resistances_: "fire"
_speed_: "10 ft., fly 60 ft."
_hit points_: "52 (7d10 + 14)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 17 (+3) | 15 (+2) | 6 (-2) | 12 (+1) | 10 (+0) |

___Damage Transfer.___ While the hell moth is grappling a creature, the hell
moth takes only half the damage dealt to it, and the creature grappled by
the hell moth takes the other half.

___Keen Scent.___ The hell moth has advantage on Wisdom (Perception)
checks based on scent.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Engulf.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one Medium or
smaller creature. Hit: The creature is grappled (escape DC 15). Until this
grapple ends, the target is restrained and blinded, and the hell moth can’t
engulf another target.

___Immolation (1/Short or Long Rest).___ The hell moth
detonates, dealing 33 (6d10) fire damage to itself and any grappled
creature.
