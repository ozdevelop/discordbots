"Camel";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_speed_: "50 ft."
_hit points_: "15 (2d10+4)"
_armor class_: "9"
_stats_: | 16 (+3) | 8 (-1) | 14 (+2) | 2 (-4) | 8 (-1) | 5 (-3) |

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 2 (1d4) bludgeoning damage.