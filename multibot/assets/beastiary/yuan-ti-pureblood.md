"Yuan-ti Pureblood";;;_size_: Medium humanoid (yuan-ti)
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Abyssal, Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Deception +6, Perception +3, Stealth +3"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "40 (9d8)"
_armor class_: "11"
_condition_immunities_: "poisoned"
_stats_: | 11 (0) | 12 (+1) | 11 (0) | 13 (+1) | 12 (+1) | 14 (+2) |

___Innate Spellcasting.___ The yuan-ti's spellcasting ability is Charisma (spell save DC 12). The yuan-ti can innately cast the following spells, requiring no material components:

At will: animal friendship (snakes only)

3/day each: poison spray, suggestion

___Magic Resistance.___ The yuan-ti has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The yuan-ti makes two melee attacks.

___Scimitar.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) slashing damage.

___Shortbow.___ Ranged Weapon Attack: +3 to hit, range 80/320 ft., one target. Hit: 4 (1d6 + 1) piercing damage plus 7 (2d6) poison damage.