"Corlapis Architect";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "4 (1,100 XP)"
_languages_: "Terran"
_skills_: "Stealth +2"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Con +5"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 10 (+0) | 17 (+3) | 16 (+3) | 10 (+0) | 10 (+0) |

___Hardened Exterior.___ Bludgeoning, piercing, and
slashing damage the corlapis takes from nonmagical
weapons is reduced by 3 to a minimum of 1.

___Innate Spellcasting.___ The corlapis's innate spellcasting
ability is Intelligence (spell save DC 13). It can
innately cast the following spells, requiring no
material components:

* At will: _light, shape stone, stoneskin _(self only)

___Stone Camouflage.___ The corlapis has advantage on
Dexterity (Stealth) checks to hide in rocky terrain.

___Sturdy.___ The corlapis has advantage on saving throws
against effects that would cause it to move or be
knocked prone.

**Actions**

___Multiattack.___ The corlapis makes two attacks with its
warhammer.

___Warhammer.___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning
damage, or 9 (1d10 + 4) bludgeoning damage if
used with two hands.

___Hurl Boulder.___ Ranged Weapon Attack: +6 to hit,
range 30/90 ft., one target. Hit: 15 (2d10 + 4)
bludgeoning damage.

___Stonework Prison (1/Day).___ The corlapis summons a
10-foot diameter cage of stone at a point it can see
within 90 feet. Each side of the cage has 15 hit
points and AC 15. Any creature in the cage's area
must make a DC 13 Dexterity saving throw. On a
successful save, a creature is ejected from that
space to the nearest unoccupied space outside of
it. A Huge or larger creature succeeds on the saving
throw automatically. On a failed save, a creature is
trapped within the bars of the cage. The bars are
wide enough to allow projectiles to pass through it,
but not melee weapons. The cage can hold a
maximum of four Medium or smaller creatures or
one Large creature.
