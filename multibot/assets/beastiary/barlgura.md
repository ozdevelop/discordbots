"Barlgura";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Perception +5, Stealth +5"
_damage_immunities_: "poison"
_saving_throws_: "Dex +5, Con +6"
_speed_: "40 ft., climb 40 ft."
_hit points_: "68 (8d10+24)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 7 (-2) | 14 (+2) | 9 (-1) |

___Innate Spellcasting.___ The barlgura's spellcasting ability is Wisdom (spell save DC 13). The barlgura can innately cast the following spells, requiring no material components:

* 1/day each: _entangle, phantasmal force_

* 2/day each: _disguise self, invisibility _(self only)

___Reckless.___ At the start of its turn, the barlgura can gain advantage on all melee weapon attack rolls it makes during that turn, but attack rolls against it have advantage until the start of its next turn.

___Running Leap.___ The barlgura's long jump is up to 40 feet and its high jump is up to 20 feet when it has a running start.

**Actions**

___Multiattack.___ The barlgura makes three attacks: one with its bite and two with its fists.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage.

___Fist.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) bludgeoning damage.

___Variant: Summon Demon (1/Day).___ The demon chooses what to summon and attempts a magical summoning.

A barlgura has a 30 percent chance of summoning one barlgura.

A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.
