"Stone Giant Dreamwalker";;;_size_: Huge giant (stone giant)
_alignment_: chaotic neutral
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +14, Perception +3"
_saving_throws_: "Dex +6, Con +9, Wis +3"
_speed_: "40 ft."
_hit points_: "161 (14d12+70)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "charmed, frightened"
_stats_: | 23 (+6) | 14 (+2) | 21 (+5) | 10 (0) | 8 (-1) | 12 (+1) |

___Dreamwalker's Charm.___ An enemy that starts its turn within 30 feet of the giant must make a DC 13 Charisma saving throw, provided that the giant isn't incapacitated. On a failed save, the creature is charmed by the giant. A creature charmed in this way can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. Once it succeeds on the saving throw, the creature is immune to this giant's Dreamwalker's Charm for 24 hours.

**Actions**

___Multiattack.___ The giant makes two attacks with its greatclub.

___Greatclub.___ Melee Weapon Attack: +10 to hit, reach 15 ft, one target. Hit: 19 (3d8+6) bludgeoning damage.

___Petrifying Touch.___ The giant touches one Medium or smaller creature within 10 feet of it that is charmed by it. The target must make a DC 17 Constitution saving throw. On a failed save, the target becomes petrified, and the giant can adhere the target to its stony body. Greater restoration spells and other magic that can undo petrification have no effect on a petrified creature on the giant unless the giant is dead, in which case the magic works normally, freeing the petrified creature as well as ending the petrified condition on it.

___Rock.___ Ranged Weapon Attack: +10 to hit, range 60/240 ft, one target. Hit: 28 (4d10+6) bludgeoning damage. If the target is a creature, it must succeed on a DC 17 Strength saving throw or be knocked prone.