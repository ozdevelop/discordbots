"Bonepowder Ghoul";;;_size_: Small undead
_alignment_: neutral evil
_challenge_: "12 (8400 XP)"
_languages_: "Common, Darakhul, Draconic, Dwarvish"
_skills_: "Perception +6, Stealth +9"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +9, Con +8, Wis +6, Cha +8"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "cold, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "30 ft."
_hit points_: "195 (26d6 + 104)"
_armor class_: "18 (natural armor)"
_stats_: | 10 (+0) | 20 (+5) | 18 (+4) | 19 (+4) | 15 (+2) | 18 (+4) |

___Amorphous.___ The bonepowder ghoul can move through a space as narrow as 1 inch wide without squeezing.

___Coalesce.___ Whenever a bonepowder ghoul drains life force from victims with Gravedust, it can use that energy transform its shape into a more solid form and maintain it. The new form is Small and semi-transparent but roughly the shape of a normal ghoul. In this form, the ghoul isn't amorphous and can't form a whirlwind, but it can speak normally and manipulate objects. The altered form lasts for 1 minute for every point of necrotic damage it delivered against living foes.

___Turning Defiance.___ The bonepowder ghoul and any other ghouls within 30 feet of it have advantage on saving throws against effects that turn undead.

___Innate Spellcasting.___ The bonepowder ghoul's innate spellcasting ability is Charisma (spell save DC 18, +10 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: chill touch, darkness, dispel magic, ray of enfeeblement

3/day: blindness/deafness, circle of death (7th level; 10d6)

1/day: finger of death

**Actions**

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 18 (3d8 + 5) plus 1d4 Strength damage, and the target must succeed on a DC 17 Constitution saving throw or be paralyzed for 1d4 + 1 rounds. If the target creature is humanoid, it must succeed on a second DC 19 Constitution saving throw or contract darakhul fever.

___Gravedust.___ A bonepowder ghoul can project a 40-ft. cone of grave dust. All targets within the area must make a DC 19 Dexterity saving throw to avoid taking 4d8 necrotic damage, and must make a second DC 17 Constitution saving throw to avoid being infected with darakhul fever.

___Whirlwind (Recharge 5-6).___ A bonepowder ghoul can generate a whirlwind of bones and teeth. All creatures within a 20-foot cube take 66 (12d10) slashing damage and are drained of 1d6 Strength; a successful DC 17 Dexterity saving throw reduces damage to half and negates the Strength loss. The whirlwind dissipates after one round.

