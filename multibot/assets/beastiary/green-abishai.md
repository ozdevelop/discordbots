"Green Abishai";;;_page_number_: 162
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "15 (13,000 XP)"
_languages_: "Draconic, Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 16"
_skills_: "Deception +9, Insight +6, Perception +6, Persuasion +9"
_damage_immunities_: "fire, poison"
_saving_throws_: "Int +8, Cha +9"
_speed_: "30 ft., fly 40 ft."
_hit points_: "187  (25d8 + 75)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 12 (+1) | 17 (+3) | 16 (+3) | 17 (+3) | 12 (+1) | 19 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the abishai's darkvision.

___Innate Spellcasting.___ The abishai's innate spellcasting ability is Charisma (spell save DC 17). It can innately cast the following spells, requiring no material components:

* At will: _alter self, major image_

* 3/day each: _charm person, detect thoughts, fear_

* 1/day each: _confusion, dominate person, mass suggestion_

___Magic Resistance.___ The abishai has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The abishai's weapon attacks are magical. Actions

___Multiattack.___ The abishai makes two attacks, one with its claws and one with its longsword, or it casts one spell from its Innate Spellcasting trait and makes one claw attack.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) slashing damage, or 6 (1d10 + 1) slashing damage if used with two hands.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) piercing damage. If the target is a creature, it must succeed on a DC 16 Constitution saving throw or take 11 (2d10) poison damage and become poisoned for 1 minute. The poisoned target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
