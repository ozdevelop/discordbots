"The Queen of Bones";;;_size_: Medium aberration
_alignment_: chaotic neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Deep Speech, Primordial"
_skills_: "Insight +8, Intimidation +4, Perception +8, Religion +4"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Con +4, Wis +8, Cha +4"
_damage_resistances_: "psychic"
_condition_immunities_: "charmed, petrified"
_speed_: "30 ft."
_hit points_: "71 (13d8 + 13)"
_armor class_: "15 (natural armor)"
_stats_: | 13 (+1) | 15 (+2) | 13 (+1) | 13 (+1) | 20 (+5) | 12 (+1) |

___Everchanging Changers.___ The Court of All Flesh are
beings of pure chaos. Because their minds are pure
disorder, they cannot be driven mad or charmed
and any attempts to magically compel their behavior fails.

___Formless Shape.___ The Queen is immune to any
spell or effect that would alter his form.

___Spellcasting.___ The Queen is a 9th-level spellcaster.
Her spellcasting ability is Wisdom (spell save DC
16, +8 to hit with spell attacks). She has the following cleric spells prepared:

* Cantrips (at will): _guidance, mending, sacred flame, spare the dying_

* 1st level (4 slots): _bane, inflict wounds, healing word, shield of faith_

* 2nd level (3 slots): _hold person, lesser restoration, magic weapon, silence, spiritual weapon_

* 3rd level (3 slots): _animate dead, bestow curse, dispel magic, protection from energy, revivify, water walk_

* 4th level (3 slots): _banishment, death ward, freedom of movement, stoneskin_

* 5th level (1 slot): _contagion, flame strike, insect plague_

**Actions**

___Multiattack.___ The Queen makes two attacks with
her Bone Bow.

___Bone Bow.___ Ranged Weapon Attack: +5 to hit, range
150/600 ft., one target. Hit: 11 (2d8 + 2) damage.

___Ossuary.___ The Queen targets one creature she
can see within 30 feet of her and turns its own
skeleton into a jail. The target must make a DC 16
Constitution saving throw against this magic. On
a failed save, it takes 14 (4d6) necrotic damage
and is paralyzed for 1 minute. At the end of each of
its turns, the target can repeat this saving throw,
ending the effect on itself on a success. If the
target succeeds on the save, or if the effect ends
on it, the target becomes immune to Ossuary for
24 hours.
