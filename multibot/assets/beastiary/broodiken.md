"Broodiken";;;_size_: Tiny construct
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft., climb 20 ft."
_hit points_: "55 (10d4 + 30)"
_armor class_: "13 (natural armor)"
_stats_: | 8 (-1) | 14 (+2) | 16 (+3) | 2 (-4) | 10 (+0) | 6 (-2) |

___Immutable Form.___ The broodiken is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The broodiken has advantage on saving throws against spells and other magical effects.

___Shared Rage.___ A broodiken cannot speak with its creator telepathically, but it feels strong emotions and recognizes the objects of those emotions. A creator can telepathically order broodiken to hunt for and attack individuals by sending the broodiken an image of the creature and the appropriate emotion. As long as the broodiken is on such a hunt, it can be more than 100 feet away from its master without wailing.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) piercing damage.

___Attach.___ When a broodiken succeeds on a bite attack, its teeth latch on, grappling the target (escape DC 9). On each of its turns, its bite attack hits automatically as long as it can maintain its grapple.

