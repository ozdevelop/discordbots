"Oculider";;;_size_: Tiny monstrosity
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_languages_: "--"
_senses_: "Passive Perception 13"
_skills_: "Perception +3, Stealth +5"
_speed_: "20 ft., climb 10 ft."
_hit points_: "3 (2d4 - 1)"
_armor class_: "13"
_stats_: | 3 (-4) | 16 (+3) | 8 (-1) | 2 (-4) | 12 (+1) | 3 (-4) |

___Mimicry.___ The oculider can mimic any sounds it
has heard, including voices. A creature that hears
the sounds can tell they are imitations with a successful
DC 11 Wisdom (Insight) check.

**Actions**

___Bite.___ Melee Weapon Attack: +0 to hit, reach 5 ft.,
one target. Hit: 1 piercing damage.

___Eye Beam.___ Ranged Weapon Attack: +5 to hit,
reach 30 ft., one target. Hit: 2 (1d4) damage. Roll
1d6 to determine damage type for each attack.
* 1-2   Fire
* 3-4   Lightning
* 5-6   Acid
