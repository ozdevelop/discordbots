"Quadrone";;;_size_: Medium construct
_alignment_: construct
_challenge_: "1 (200 XP)"
_languages_: "Modron"
_senses_: "truesight 120 ft."
_skills_: "Perception +2"
_speed_: "30 ft., fly 30 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 14 (+2) | 12 (+1) | 10 (0) | 10 (0) | 11 (0) |

___Axiomatic Mind.___ The quadrone can't be compelled to act in a manner contrary to its nature or its instructions.

___Disintegration.___ If the quadrone dies, its body disintegrates into dust, leaving behind its weapons and anything else it was carrying.

**Actions**

___Multiattack.___ The quadrone makes two fist attacks or four shortbow attacks.

___Fist.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) bludgeoning damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 2) piercing damage.