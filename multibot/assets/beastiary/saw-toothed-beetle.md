"Saw-Toothed Beetle";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 11 (+0) | 14 (+2) | 1 (-5) | 10 (+0) | 6 (-2) |

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage and the target is grappled (escape DC 12). Until the grapple ends, the target is restrained and the beetle cannot bite another target. A grappled target takes 6 (1d8 + 2) piercing damage at the start of each of the beetle’s turns.
