"Black Pudding";;;_size_: Large ooze
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_damage_immunities_: "acid, cold, lightning, slashing"
_speed_: "20 ft., climb 20 ft."
_hit points_: "85 (10d10+30)"
_armor class_: "7"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_stats_: | 16 (+3) | 5 (-3) | 16 (+3) | 1 (-5) | 6 (-2) | 1 (-5) |

___Amorphous.___ The pudding can move through a space as narrow as 1 inch wide without squeezing.

___Corrosive Form.___ A creature that touches the pudding or hits it with a melee attack while within 5 feet of it takes 4 (1d8) acid damage. Any nonmagical weapon made of metal or wood that hits the pudding corrodes. After dealing damage, the weapon takes a permanent and cumulative -1 penalty to damage rolls. If its penalty drops to -5, the weapon is destroyed. Nonmagical ammunition made of metal or wood that hits the pudding is destroyed after dealing damage. The pudding can eat through 2-inch-thick, nonmagical wood or metal in 1 round.

___Spider Climb.___ The pudding can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage plus 18 (4d8) acid damage. In addition, nonmagical armor worn by the target is partly dissolved and takes a permanent and cumulative -1 penalty to the AC it offers. The armor is destroyed if the penalty reduces its AC to 10.

**Reactions**

___Split.___ When a pudding that is Medium or larger is subjected to lightning or slashing damage, it splits into two new puddings if it has at least 10 hit points. Each new pudding has hit points equal to half the original pudding's, rounded down. New puddings are one size smaller than the original pudding.