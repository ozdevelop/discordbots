"Lizard King/Queen";;;_size_: Medium humanoid (lizardfolk)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4, Stealth +5, Survival +4"
_saving_throws_: "Con +4, Wis +2"
_speed_: "30 ft., swim 30 ft."
_hit points_: "78 (12d8+24)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "frightened"
_stats_: | 17 (+3) | 12 (+1) | 15 (+2) | 11 (0) | 11 (0) | 15 (+2) |

___Hold Breath.___ The lizardfolk can hold its breath for 15 minutes.

___Skewer.___ Once per turn, when the lizardfolk makes a melee attack with its trident and hits, the target takes an extra 10 (3d6) damage, and the lizardfolk gains temporary hit points equal to the extra damage dealt.

**Actions**

___Multiattack.___ The lizardfolk makes two attacks: one with its bite and one with its claws or trident or two melee attacks with its trident.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) slashing damage.

___Trident.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d6 + 3) piercing damage, or 7 (1d8 + 3) piercing damage if used with two hands to make a melee attack.