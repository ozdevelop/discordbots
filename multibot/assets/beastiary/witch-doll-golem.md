"Witch-Doll Golem";;;_size_: Large construct
_alignment_: neutral
_challenge_: "10 (5,900 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 20"
_skills_: "Investigation +2, Perception +10, Survival +10"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "94 (9d10 + 45)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 21 (+5) | 7 (-2) | 15 (+2) | 11 (+0) |

___Find Target.___ The witch-doll golem knows the location of a specific
target creature, or one creature cursed by its witch-doll curse ability, as
long as that creature is within 1,000 feet of it. If the creature is moving, it
knows the direction of that creature’s movement. If the creature is beyond
this distance, the witch-doll golem knows the direction that creature is in.

___Immutable Form.___ The golem is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against
spells and other magical effects.

___Magic Weapon.___ The golem’s weapon attacks are magical.

**Actions**

___Multiattack.___ The witch-doll golem makes up to two attacks with its
slam or its needle attack.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 20
(3d10 + 4) bludgeoning damage.

___Needle.___ Melee or Ranged Weapon Attack: +8 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 14 (3d6 + 4) piercing damage.

___Witch-Doll Curse.___ The witch-doll golem chooses one creature it can
see within 60 feet of it. The target must make a DC 17 Wisdom saving
throw or be cursed. While cursed, the witch-doll golem deals an additional
13 (3d8) necrotic damage to the target when it hits with a weapon attack,
and any damage the witch-doll golem takes from targets other than the
cursed target is halved, and the cursed target takes the other half. The
witch-doll golem can only have one active curse at a time.

The target remains cursed until the golem is destroyed or the target dies;
magic such as remove curse can end the curse early.
