"Kalke";;;_size_: Small fiend
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Abyssal, Common, Infernal"
_skills_: "Perception +0, Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 10"
_speed_: "30 ft., climb 30 ft."
_hit points_: "9 (2d6 + 2)"
_armor class_: "14 (natural armor)"
_stats_: | 8 (-1) | 17 (+3) | 12 (+1) | 13 (+1) | 7 (-2) | 13 (+1) |

___Extinguish Flames.___ Kalkes can extinguish candles, lamps, lanterns and low-burning campfires within 120 feet as a bonus action.

___Detect Spellcasting.___ Kalkes can sense spellcasting in a 5-mile radius, as long as the effect is not innate.

___Magic Resistance.___ Kalkes have advantage on saving throws against spells and magical effects.

**Actions**

___Dagger.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

