"Yestabrod";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, telepathy 60 ft."
_senses_: "darkvision 120 ft."
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "75 (10d10+20)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 12 (+1) | 10 (0) | 14 (+2) | 13 (+1) | 15 (+2) | 10 (0) |

___Legendary Resistance (1/Day).___ If Yestabrod fails a saving throw, it can choose to succeed instead.

**Actions**

___Slam.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 8 (3d4 + 1) bludgeoning damage plus 7 (3d4) poison damage.

___Caustic Spores (1/Day).___ Yestabrod releases spores in a 30-foot cone. Each creature in that area must succeed on a DC 12 Dexterity saving throw ortake 1d6 acid damage at the start of each of Yestbrod's turns. A creature can repeat the saving throw at the end of each of its turn, ending the effect on itself on a success.

___Infestation Spores (1/Day).___ Yestabrod releases spores that burst out in a cloud that fills a 10-foot-radius sphere centered on it, and the cloud lingers for 1 minute. Any flesh-and-blood creature in the cloud when it appears, or that enters it later, must make a DC 12 Constitution saving throw. On a successful save, the creature can't be infected by these spores for 24 hours. On a failed save, the creature is infected with a disease called the spores of Zuggtmoy and also gains a random form of indefinite madness (determined by rolling on the Madness of Zuggtmoy table in appendix D) that lasts until the creature is cured of the disease or dies. While infected in this way, the creature can't be reinfecte, and it must be repeat the saving throw at the end of every 24 hours, ending the infection on a success. On a failure, the infected creature's body is slowly taken over by fungal growth, and after three such failed saves, the creature dies and is reanimated as a spore servant if it's a type of creature that can be (see the "Myconids" entry in the Monster Manual).

**Legendary** Actions

Yestabrod can take 2 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Yestabrod regains spent legendary actions at the start of its turn.

___Corpse Burst.___ Gore, offal, and acid erupt from a corpse within 20 feet of Yestabrod. Creatures within 10 feet of the corpse must succeed on a DC 12 Dexterity saving throw or take 7 (2d6) acid damage.

___Foul Absorption.___ Yestabrod absorbs putrescence from a corpse within 5 feet of it, regaining 1d8 + 2 hit points