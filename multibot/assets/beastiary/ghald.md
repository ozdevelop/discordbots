"Ghald";;;_size_: Large humanoid (sahuagin)
_alignment_: lawful evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Sahuagin"
_senses_: "darkvision 120 ft."
_skills_: "Insight +4, Perception +7"
_saving_throws_: "Dex +6, Con +6, Int +5, Wis +4"
_speed_: "30 ft., swim 50 ft."
_hit points_: "102 (12d10+36)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 17 (+3) | 16 (+3) | 14 (+2) | 13 (+1) | 17 (+3) |

___Assassinate.___ During its first turn, Ghald has advantage on attack rolls against any creature that hasn't taken a turn. Any hit Ghald scores against a surprised creature is a critical hit.

___Limited Amphibiousness.___ Ghald can breathe air and water, but he needs to be submerged at least once every 4 hours to avoid suffocating.

___Shark Telepathy.___ Ghald can magically command any shark within 120 feet of him, using a limited telepathy.

___Sneak Attack.___ Ghald deals an extra 14 (4d6) damage when he hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 feet of an ally of Ghald's that isn't incapacitated and Ghald doesn't have disadvantage on the attack roll.

**Actions**

___Multiattack.___ Ghald makes three attacks, one with his bite and two with his shortswords.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 9 (2d4 + 4) piercing damage.

___Shortsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 11 (2d6 + 4) piercing damage.

___Garrote.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one Medium or Small creature against which Ghald has advantage on the attack roll. Hit: 9 (2d4 + 4) bludgeoning damage, and the target is grappled (escape DC 15). Until the grapple ends, the target can't breathe, and Ghald has advantage on attack rolls against it.