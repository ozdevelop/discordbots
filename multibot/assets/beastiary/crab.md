"Crab";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "blindsight 30 ft."
_skills_: "Stealth +2"
_speed_: "20 ft., swim 20 ft."
_hit points_: "2 (1d4)"
_armor class_: "11 (natural armor)"
_stats_: | 2 (-4) | 11 (0) | 10 (0) | 1 (-5) | 8 (-1) | 2 (-4) |

___Amphibious.___ The crab can breathe air and water.

**Actions**

___Claw.___ Melee Weapon Attack: +0 to hit, reach 5 ft., one target. Hit: 1 bludgeoning damage.