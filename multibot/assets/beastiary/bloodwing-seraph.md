"Bloodwing Seraph";;;_size_: Medium celestial
_alignment_: chaotic evil
_challenge_: "11 (7,200 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Acrobatics +9, Insight +7"
_senses_: "darkvision 120ft., passive Perception 14"
_saving_throws_: "Wis +8, Cha +9"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhausted, frightened"
_speed_: "30 ft., fly 90 ft."
_hit points_: "153 (18d8 + 72)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 20 (+5) | 18 (+4) | 16 (+3) | 19 (+4) | 20 (+5) |

___Corrupted Angelic Weapons.___ The seraph's weapons are
magical. When the seraph hits with any weapon, the
weapon deals an extra 2d8 necrotic damage and the
target must succeed on a DC 17 Constitution saving
throw or become poisoned until the end of its next
turn (included in the attack).

___Innate Spellcasting.___ The seraph's spellcasting ability is
Charisma (spell save DC 17). The seraph can innately
cast the following spells, requiring only verbal
components.

* At will: _animate dead, bane_

* 2/day each: _blight, confusion_

* 1/day each: _destructive wave_ (necrotic only)

___Magic Resistance.___ The seraph has advantage on saving
throws against spells and other magical effects.

**Actions**

___Multiattack.___ The seraph makes two melee attacks, only
one of which can be a decaying touch.

___Glaive.___ Melee Weapon Attack: +8 to hit, reach 10 ft.,
one target. Hit: 9 (1d10 + 4) piercing damage plus 9
(2d8) necrotic damage and the target must succeed on
a DC 17 Constitution saving throw or become
poisoned until the end of its next turn.

___Barrage of Feathers.___ Ranged Weapon Attack: +9 to hit,
reach 30/90 ft., one target. Hit: 18 (4d8) piercing
damage and 18 (4d8) necrotic damage.

___Decaying Touch (3/Day).___ Melee Spell Attack: +9 to hit,
reach 5 ft., one target. Hit: 40 (8d8 + 4) necrotic
damage.

___Corrupting Palm (1/Day).___ Melee Weapon Attack: +9 to
hit, reach 5 ft., one target. Hit: 18 (4d8) necrotic
damage and the target must succeed on a DC 17
Wisdom saving throw or become magically charmed.

While the target is charmed in this way, a blackened
halo appears over its head and causes its eyes to glow
with frenzied hatred. The charmed target must use its
action before moving on each of its turns to make a
melee attack against a creature within range that the
seraph chooses mentally. At the end of the charmed
creatures turns, or whenever that creature takes
damage, it may repeat this saving throw. On a success,
this effect ends.
