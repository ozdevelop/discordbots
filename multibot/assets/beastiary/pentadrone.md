"Pentadrone";;;_size_: Medium construct
_alignment_: construct
_challenge_: "2 (450 XP)"
_languages_: "Modron"
_senses_: "truesight 120 ft."
_skills_: "Perception +4"
_speed_: "40 ft."
_hit points_: "32 (5d10+5)"
_armor class_: "16 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 12 (+1) | 10 (0) | 10 (0) | 13 (+1) |

___Axiomatic Mind.___ The pentadrone can't be compelled to act in a manner contrary to its nature or its instructions.

___Disintegration.___ If the pentadrone dies, its body disintegrates into dust, leaving behind its weapons and anything else it was carrying.

**Actions**

___Multiattack.___ The pentadrone makes five arm attacks.

___Arm.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage.

___Paralysis Gas (Recharge 5-6).___ The pentadrone exhales a 30-foot cone of gas. Each creature in that area must succeed on a DC 11 Constitution saving throw or be paralyzed for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.