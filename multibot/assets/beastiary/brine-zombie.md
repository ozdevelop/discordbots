"Brine Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "understands all languages it spoke in life but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "fire"
_condition_immunities_: "poisoned"
_speed_: "20 ft., swim 30 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "11 (natural armor)"
_stats_: | 14 (+2) | 8 (-1) | 13 (+1) | 2 (-4) | 10 (+0) | 10 (+0) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it
must make a Constitution saving throw with a DC of 5 + the damage
taken, unless the damage is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.

**Actions**

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5
(1d6 + 2) slashing damage.

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft. one target. Hit: 9 (2d6 + 2) bludgeoning damage.
