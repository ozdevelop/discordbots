"Azbara Jos";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Draconic, Infernal, Primordial, Thayan"
_skills_: "Arcana +5, Deception +2, Insight +3, Stealth +5"
_saving_throws_: "Int +5, Wis +3"
_speed_: "30 ft."
_hit points_: "39 (6d8+12)"
_armor class_: "13 (16 with mage armor)"
_stats_: | 9 (-1) | 16 (+3) | 14 (+2) | 16 (+3) | 13 (+1) | 11 (0) |

___Special Equipment.___ Azbara has two scrolls of mage armor.

___Potent Cantrips.___ When Azbara casts an evocation Cantrips and misses, or the target succeeds on its saving throw, the target still takes half the cantrip's damage but suffers no other effect.

___Sculpt Spells.___ When Azbara casts an evocation spell that affects other creatures that he can see, he can choose a number of them equal to 1+the spell's level to succeed on their saving throws against the spell. Those creatures take no damage if they would normally take half damage from the spell.

___Spellcasting.___ Azbara is a 6th-level spellcaster that uses Intelligence as his spellcasting ability (spell save DC 13, +5 to hit with spell attacks). Azbara has the following spells prepared from the wizard spell list:

* Cantrips (at will): _mage hand, prestidigitation, ray of frost, shocking grasp_

* 1st level (4 slots): _fog cloud, magic missile, shield, thunderwave_

* 2nd level (3 slots): _invisibility, misty step, scorching ray_

* 3rd level (3 slots): _counterspell, dispel magic, fireball_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or ranged 20 ft./60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.
