"Baba Yaga";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "15 (13,000 XP) or 17 (18,000 XP) when accompanied by her hut"
_languages_: "Common, Sylvan"
_skills_: "Deception +10, Nature +6, Perception +8, Stealth +10, Survival +8"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Wis +8, Cha +10"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 60 ft."
_hit points_: "153 (18d8 + 72)"
_armor class_: "20"
_stats_: | 13 (+1) | 20 (+5) | 18 (+4) | 12 (+1) | 16 (+3) | 20 (+5) |

___Flying Mortar and Pestle.** Baba Yaga travels in her flying
mortar and pestle, which grants her a flying speed of
60 ft.

___Perfect Mimicry.___ Baba Yaga can mimic animal and
humanoid voices with an unerring quality. A creature
that hears her mimicry can tell they are an imitation
with a successful DC 20 Wisdom (Insight) check.

___Magic Resistance.___ Baba Yaga has advantage on saving
throws against spells and other magical effects.

___A Witch's Best Friend.___ Baba Yaga's Hut acts as both a
companion and her very own coven. In addition to
defending its master and obeying her every command,
the hut acts as Baba Yaga’s source of power. When
within 60 feet of the hut, Baba Yaga gains her access to
the Shared Spellcasting trait below. Should the hut
reach 0 hit points, Baba Yaga's Shared Spellcasting is
no longer available to her.

___Shared Spellcasting.___ While Baba Yaga and her Hut are
within 60 feet of one another, they can each cast the
following spells but must share the spell slots among
themselves. For casting these spells, Baba Yaga is an
18th-level spellcaster. Her spellcasting ability is
Charisma (spell save DC 18, +10 to hit with spell
attacks). She knows the following spells, requiring no
material components:

* Cantrips (at will): _druid craft, minor illusion, resistance, shillelagh_

* 1st level (4 slots): _entangle, speak with animals, ray of sickness_

* 2nd level (3 slots): _flaming sphere, moonbeam, spike growth_

* 3rd level (3 slots): _call lightning, dispel magic, plant growth_

* 4th level (3 slots): _blight, hallucinatory terrain, stoneskin_

* 5th level (3 slots): _antilife shell, gease, scrying_

* 6th level (1 slot): _wall of thorns_

* 7th level (1 slot): _finger of death_

* 8th level (1 slot): _dominate monster_

* 9th level (1 slot): _shapechange_

**Actions**

___Multiattack.___ Baba Yaga makes two attacks with her
claws.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one
target. Hit 1d6+1 slashing damage and 4d8 acid
damage.

___Witch's Brew (Recharge 5-6).___ Baba Yaga pulls a potion
out of her mortar and pestle and throws it to a point
within 60 feet of her. Each creature within a 30-foot
radius of where the potion lands must make a DC 18
Constitution saving throw, taking 56 (16d6) poison
damage, or half as much damage on a successful save.
