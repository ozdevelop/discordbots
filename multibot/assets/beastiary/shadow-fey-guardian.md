"Shadow Fey Guardian";;;_size_: Large humanoid
_alignment_: neutral evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Elvish, Umbral"
_skills_: "Athletics +6, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Str +6, Con +5"
_condition_immunities_: "charmed, frightened"
_speed_: "30 ft."
_hit points_: "110 (13d10 + 39)"
_armor class_: "15 (chain shirt)"
_stats_: | 18 (+4) | 14 (+2) | 16 (+3) | 6 (-2) | 14 (+2) | 8 (-1) |

___Fey Ancestry.___ The shadow fey guardian has advantage on saving throws against being charmed, and magic can't put it to sleep

___Innate Spellcasting.___ The shadow fey's innate spellcasting ability is Charisma. It can cast the following spells innately, requiring no material components.

1/day: misty step (when in shadows, dim light, or darkness only)

___Shadow's Vigil.___ The shadow fey has advantage on Wisdom (Perception) checks, and magical darkness does not inhibit its darkvision.

___Sunlight Sensitivity.___ While in sunlight, the shadow fey has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

___Traveler in Darkness.___ The shadow fey has advantage on Intelligence (Arcana) checks made to know about shadow roads and shadow magic spells or items.

**Actions**

___Multiattack.___ The shadow fey makes two pike attacks.

___Pike.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 15 (2d10 + 4) piercing damage.

___Javelin.___ Ranged Weapon Attack: +6 to hit, range 30/120 ft., one target. Hit: 11 (2d6 + 4) piercing damage.

**Reactions**

___Protect.___ The shadow fey guardian imposes disadvantage on an attack roll against an ally within 5 feet. The guardian must be wielding a melee weapon to use this reaction.

