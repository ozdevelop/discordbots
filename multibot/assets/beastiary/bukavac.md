"Bukavac";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "9 (5000 XP)"
_languages_: "Sylvan"
_skills_: "Perception +10, Stealth +11"
_senses_: "darkvision 60 ft., passive Perception 20"
_saving_throws_: "Dex +7, Con +8"
_damage_immunities_: "thunder"
_speed_: "40 ft., swim 20 ft."
_hit points_: "199 (21d10 + 84)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 17 (+3) | 18 (+4) | 7 (-2) | 15 (+2) | 12 (+1) |

___Hold Breath.___ The bukavac can hold its breath for up to 20 minutes.

___Hop.___ A bukavac can move its enormous bulk with remarkably quick hop of up to 20 feet, leaping over obstacles and foes. It may also use the hop as part of a withdraw action.

**Actions**

___Multiattack.___ The bukavac makes four claw attacks, or two claw attacks and one bite attack, or two claw attacks and one gore attack, or one bite and one gore attack.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 21 (3d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 11 (1d12 + 5) slashing damage and grapples (escape DC15). A bukavac can grapple up to 2 Medium size foes.

___Gore.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 21 (3d10 + 5) piercing damage.

___Croaking Blast (Recharge 5-6).___ A bukavac can emit a howling thunderclap that deafens and damages those nearby. Creatures within 15 feet who fail a DC 17 Constitution saving throw take 36 (8d8) thunder damage and are permanently deafened. Those succeeding on the saving throw take half damage and are not deafened. The deafness can be cured with lesser restoration.

