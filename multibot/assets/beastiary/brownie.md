"Brownie";;;_size_: Tiny fey
_alignment_: lawful good
_challenge_: "2 (450 XP)"
_languages_: "Common, Halfling, Sylvan"
_skills_: "Deception +5, Stealth +6"
_senses_: "passive Perception 12"
_speed_: "25 ft."
_hit points_: "7 (2d4 + 2)"
_armor class_: "15 (natural armor)"
_stats_: | 7 (-2) | 18 (+4) | 12 (+1) | 14 (+2) | 14 (+2) | 16 (+3) |

___Magic Resistance.___ The brownie has advantage on saving throws against
spells and other magical effects.

___Innate Spellcasting.___ The brownie’s innate spellcasting ability is
Charisma (spell save DC 13, +5 to hit with spell attacks). It can
innately cast the following spells, without using any spell
components:

* At will: _druidcraft, minor illusion, prestidigitation, thaumaturgy_

* 3/day each: _color spray, dancing lights, hideous laughter, silent image_

* 1/day each: _dimension door_

**Actions**

___Shortsword.___ Melee Weapon Attack: +6 to hit,
reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing
damage.
