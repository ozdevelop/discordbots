"Carrion Claw";;;_size_: Large monstrosity
_alignment_: Neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "40 ft., climb 40 ft."
_hit points_: "135 (18d10 + 36)"
_armor class_: "14 (Natural Armor)"
_stats_: | 18 (+4) | 14 (+2) | 14 (+2) | 4 (-3) | 12 (+1) | 11 (+0) |

___Spider Climb.___ The carrion claw can climb difficult surfaces, including
upside down on ceilings, without needing to make an ability check.

___Magical Light Sensitivity.___ While in magical light, the carrion claw has
disadvantage on attack rolls, and opponents have advantage on attack rolls
against it.

**Actions**

___Multiattack.___ The carrion claw makes one bite attack and three claw
attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) piercing damage. If the target is a creature other than undead, it
must succeed on a DC 14 Constitution saving throw or be paralyzed for
1 minute. The target can repeat the saving throw at the end of each of its
turns, ending the effect on itself on a success.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 17
(3d8 +4) slashing damage.
