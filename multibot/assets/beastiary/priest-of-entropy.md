"Priest of Entropy";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "12 (8,400 XP)"
_languages_: "any two languages"
_skills_: "Intimidation +6, Religion +4"
_senses_: "passive Perception 13"
_saving_throws_: "Con +6, Wis +7"
_speed_: "30 ft."
_hit points_: "136 (21d8 + 42)"
_armor class_: "14 (scale armor)"
_stats_: | 14 (+2) | 10 (+0) | 14 (+2) | 11 (+0) | 17 (+3) | 15 (+2) |

___Spellcasting.___ The priest is a 12th level spellcaster. Its
spellcasting ability is Wisdom (spell save DC 15, +7 to
hit with spell attacks). It has the following cleric spells
prepared:

* Cantrips (at will): _guidance, light, resistance, sacred flame, thaumaturgy_

* 1st level (4 slots): _bane, detect magic, guiding bolt, healing word, inflict wounds_

* 2nd level (3 slots): _gentle repose, hold person, spiritual weapon, silence_

* 3rd level (3 slots): _animate dead, dispel magic, mass healing word, meld into stone, protection from energy, sending_

* 4th level (3 slots): _banishment, blight, death ward_

* 5th level (2 slots): _contagion, circle of power, hallow, insect plague_

* 6th level (1 slot): _forbiddance, true seeing_

**Actions**

___Spear.___ Melee Weapon Attack: +6 to hit, reach 5 ft. or
ranged 20/60 ft., one creature. Hit: 5 (1d6 + 2) piercing
damage, or 6 (1d8+2) piercing damage if used with two
hands to make a melee attack.

___Arcane Disruption.___ The priest conjures an aura of
disruptive energy that radiates from it in a 15 foot
radius and moves with the priest, centered on it. The
aura lasts until the priest dismisses it as action, or dies.
While inside the aura, any creature that attempts to
cast a spell must first succeed on a DC 15 Constitution
saving throw. If they fail the save, their spell fails to cast
and the spell slot is wasted.
