"Purple Moss";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "charmed, frightened, poisoned, prone, stunned"
_speed_: "0 ft."
_hit points_: "5 (1d6 + 2)"
_armor class_: "5"
_stats_: | 1 (-5) | 1 (-5) | 15 (+2) | 1 (-5) | 10 (+0) | 1 (-5) |

___Fire Vulnerability.___ Any amount of fire damage instantly destroys a patch of purple moss.

___Sweet Smell.___ Purple moss emits a sweet smell detectable to a range of 10 feet.

___Sleep Spores.___ Creatures with fewer than 22 hit points
coming within 10 feet of the purple moss for the first time, or starting their turn there must succeed on a DC 13 Constitution saving throw
or fall asleep like that caused by the _sleep_ spell. Victims that fall asleep
are quickly covered by the moss. It takes 1 full round to cover a creature
of Tiny size and one additional round for each size larger than Tiny. A
creature covered by purple moss begins to suffocate and this does not
cause them to wake up from the sleep effect. Slain victims are digested
in 1d2 hours by acidic secretions from the moss. Purple moss can be
destroyed by fire.
