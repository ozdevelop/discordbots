"Tortle Druid";;;_page_number_: 242
_size_: Medium humanoid (tortle)
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "Aquan, Common"
_senses_: "passive Perception 12"
_skills_: "Animal Handling +4, Nature +2, Survival +4"
_speed_: "30 ft."
_hit points_: "33  (6d8 + 6)"
_armor class_: "17 (natural)"
_stats_: | 14 (+2) | 10 (0) | 12 (+1) | 11 (0) | 15 (+2) | 12 (+1) |

___Hold Breath.___ The tortle can hold its breath for 1 hour.

___Spellcasting.___ The tortle is a 4th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). It has the following druid spells prepared:

* Cantrips (at will): _druidcraft, guidance, produce flame_

* 1st level (4 slots): _animal friendship, cure wounds, speak with animals, thunderwave_

* 2nd level (3 slots): _darkvision, hold person_

**Actions**

___Claws___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.

___Quarterstaff___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage, or 6 (1d8 + 2) bludgeoning damage if used with two hands.

___Shell Defense___ The tortle withdraws into its shell. Until it emerges, it gains a +4 bonus to AC and has advantage on Strength and Constitution saving throws. While in its shell, the tortle is prone, its speed is 0 and can't increase, it has disadvantage on Dexterity saving throws, it can't take reactions, and the only action it can take is a bonus action to emerge.
