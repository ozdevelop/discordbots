"Bandit Lord";;;_size_: Medium Humanoid
_alignment_: any non-lawful
_challenge_: "4 (1100 XP)"
_languages_: "any two languages"
_skills_: "Athletics +5, Deception +4, Intimidation +4"
_senses_: ", passive Perception 10"
_saving_throws_: "Str +5, Dex +4, Wis +2"
_speed_: "30 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "16 (breastplate)"
_stats_: | 16 (+3) | 15 (+2) | 14 (+2) | 14 (+2) | 11 (+0) | 14 (+2) |

___Pack Tactics.___ The bandit lord has advantage on an attack roll against a creature if at least one of the bandit lord's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The bandit lord makes three melee or ranged attacks.

___Greatsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft, one target. Hit: 10 (2d6 + 3) slashing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Leadership (Recharges after a Short or Long Rest).___ For 1 minute, the bandit lord can utter a special command or warning whenever a nonhostile creature that it can see within 30 feet of it makes an attack roll or a saving throw. The creature can add a d4 to its roll provided it can hear and understand the bandit lord. A creature can benefit from only one Leadership die at a time. This effect ends if the bandit lord is incapacitated.

**Reactions**

___Parry.___ The bandit lord adds 2 to its AC against one melee attack that would hit it. To do so the bandit lord must see the attacker and be wielding a weapon.

___Redirect Attack.___ When a creature the bandit lord can see targets it with an attack, the bandit lord chooses an ally within 5 feet of it. The bandit lord and the ally swap places, and the chosen ally becomes the target instead.

