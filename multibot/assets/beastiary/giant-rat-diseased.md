"Giant Rat (Diseased)";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "7 (2d6)"
_armor class_: "12"
_stats_: | 7 (-2) | 15 (+2) | 11 (0) | 2 (-4) | 10 (0) | 4 (-3) |

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage. If the target is a creature, it must succeed on a DC 10 Constitution saving throw or contract a disease. Until the disease is cured, the target can't regain hit points except by magical means, and the target's hit point maximum decreases by 3 (1d6) every 24 hours. If the target's hit point maximum drops to 0 as a result of this disease, the target dies.