"Doppelganger";;;_size_: Medium monstrosity (shapechanger)
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft."
_skills_: "Deception +6, Insight +3"
_damage_immunities_: "charmed"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "14"
_stats_: | 11 (0) | 18 (+4) | 14 (+2) | 11 (0) | 12 (+1) | 14 (+2) |

___Shapechanger.___ The doppelganger can use its action to polymorph into a Small or Medium humanoid it has seen, or back into its true form. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Ambusher.___ In the first round of a combat, the doppelganger has advantage on attack rolls against any creature it surprised

___Surprise Attack.___ If the doppelganger surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 10 (3d6) damage from the attack.

**Actions**

___Multiattack.___ The doppelganger makes two melee attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage.

___Read Thoughts.___ The doppelganger magically reads the surface thoughts of one creature within 60 ft. of it. The effect can penetrate barriers, but 3 ft. of wood or dirt, 2 ft. of stone, 2 inches of metal, or a thin sheet of lead blocks it. While the target is in range, the doppelganger can continue reading its thoughts, as long as the doppelganger's concentration isn't broken (as if concentrating on a spell). While reading the target's mind, the doppelganger has advantage on Wisdom (Insight) and Charisma (Deception, Intimidation, and Persuasion) checks against the target.