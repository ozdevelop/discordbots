"Rath Modar";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Draconic, Infernal, Primordial, Thayan"
_skills_: "Arcana +7, Deception +3, Insight +5, Stealth +6"
_saving_throws_: "Int +7, Wis +5"
_speed_: "30 ft."
_hit points_: "71 (11d8+22)"
_armor class_: "13 (16 with mage armor)"
_stats_: | 11 (0) | 16 (+3) | 14 (+2) | 18 (+4) | 14 (+2) | 10 (0) |

___Special Equipment.___ Rath has a staff of fire, and scrolls of dimension door, feather fall, and fireball.

___Spellcasting.___ Rath is an 11th-level spellcaster who uses Intelligence as his spellcasting ability (spell save DC 15, +7 to hit with spell attacks). Rath has the following spells prepared from the wizard spell list:

* Cantrips (at will): _fire bolt, minor illusion, prestidigitation, shocking grasp_

* 1st level (4 slots): _chromatic orb, color spray, mage armor, magic missile_

* 2nd level (3 slots): _detect thoughts, mirror image, phantasmal force_

* 3rd level (3 slots): _counterspell, fireball, major image_

* 4th level (3 slots): _confusion, greater invisibility_

* 5th level (2 slots): _mislead, seeming_

* 6th level (1 slot): _globe of invulnerability_

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d8) bludgeoning damage.

**Reactions**

___Illusory Self (Recharges on a Short or Long Rest).___ When a creature Rath can see makes an attack roll against him, he can interpose an illusory duplicate between the attacker and him. The attack automatically misses Rath, then the illusion dissipates.
