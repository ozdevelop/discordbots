"Riding Horse";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_speed_: "60 ft."
_hit points_: "13 (2d10+2)"
_armor class_: "10"
_stats_: | 16 (+3) | 10 (0) | 12 (+1) | 2 (-4) | 11 (0) | 7 (-2) |

**Actions**

___Hooves.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) bludgeoning damage.