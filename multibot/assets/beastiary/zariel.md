"Zariel";;;_page_number_: 180
_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "26 (90,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 26"
_skills_: "Intimidation +18, Perception +16"
_damage_immunities_: "necrotic, poison"
_saving_throws_: "Int +16, Wis +16, Cha +18"
_speed_: "50 ft., fly 150 ft."
_hit points_: "580  (40d10 + 360)"
_armor class_: "21 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold, fire, radiant; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 27 (+8) | 24 (+7) | 28 (+9) | 26 (+8) | 27 (+8) | 30 (+10) |

___Devil's Sight.___ Magical darkness doesn't impede Zariel's darkvision.

___Magic Weapons.___ Zariel's weapon attacks are magical. When she hits with any weapon, the weapon deals an extra 36 (8d8) fire damage (included in the weapon attacks below).

___Innate Spellcasting.___ Zariel's innate spellcasting ability is Charisma (spell save DC 26). She can innately cast the following spells, requiring no material components:

* At will: _alter self _(can become Medium when changing her appearance)_, detect evil and good, fireball, invisibility _(self only)_, wall of fire_

* 3/day each: _blade barrier, dispel evil and good, finger of death_

___Legendary Resistance (3/Day).___ If Zariel fails a saving throw, she can choose to succeed instead.

___Magic Resistance.___ Zariel has advantage on saving throws against spells and other magical effects.

___Regeneration.___ Zariel regains 20 hit points at the start of her turn. If she takes radiant damage, this trait doesn't function at the start of her next turn. Zariel dies only if she starts her turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack___ Zariel attacks twice with her longsword or with her javelins. She can substitute Horrid Touch for one of these attacks.

___Longsword___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 17 (2d8 + 8) slashing damage plus 36 (8d8) fire damage, or 19 (2d10 + 8) slashing damage plus 36 (8d8) fire damage if used with two hands.

___Javelin___ Melee or Ranged Weapon Attack: +16 to hit, range 30/120 ft., one target. Hit: 15 (2d6 + 8) piercing damage plus 36 (8d8) fire damage.

___Horrid Touch (Recharge 5-6)___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 44 (8d10) necrotic damage, and the target is poisoned for 1 minute. While poisoned in this way, the target is also blinded and deafened. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Teleport___ Zariel magically teleports, along with any equipment she is wearing and carrying, up to 120 feet to an unoccupied space she can see.

**Legendary** Actions

Zariel can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Zariel regains spent legendary actions at the start of her turn.

___Immolating Gaze (Costs 2 Actions)___ Zariel turns her magical gaze toward one creature she can see within 120 feet of her and commands it to combust. The target must succeed on a DC 26 Wisdom saving throw or take 22 (4d10) fire damage.

___Teleport___ Zariel uses her Teleport action.
