"Brontosaurus";;;_size_: Gargantuan beast
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_saving_throws_: "Con +6"
_speed_: "30 ft."
_hit points_: "121 (9d20+27)"
_armor class_: "15 (natural armor)"
_stats_: | 21 (+5) | 9 (-1) | 17 (+3) | 2 (-4) | 10 (0) | 7 (-2) |

**Actions**

___Stomp.___ Melee Weapon Attack: +8 to hit, reach 20 ft., one target. Hit: 27 (5d8+5) bludgeoning damage, and the target must succed on a DC 14 Strength saving throw or be knocked prone.

___Tail.___ Melee Weapon Attack: +8 to hit, reach 20 ft., one target. Hit: 32 (6d8+5) bludgeoning damage.