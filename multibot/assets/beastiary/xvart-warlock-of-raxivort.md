"Xvart Warlock of Raxivort";;;_size_: Small humanoid (xvart)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Abyssal"
_senses_: "darkvision 30 ft."
_skills_: "Stealth +3"
_speed_: "30 ft."
_hit points_: "22 (5d6+5)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 8 (-1) | 14 (+2) | 12 (+1) | 8 (-1) | 11 (0) | 12 (+1) |

___Innate Spellcasting.___ The xvart's innate spellcasting ability is Charisma. it can innately cast the following spells, requiring no material components:

At will: detect magic, mage armor (self only)

___Spellcasting.___ The xvart is a 3rd-level spellcaster. Its spellcasting ability is Charisma (spell save DC ll, +3 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

Cantrips (at will): eldritch blast, mage hand, minor illusion, poison spray, prestidigitation

1st-2nd level (2 2nd-level slots): burning hands, expeditious retreat, invisibility, scorching ray

___Low Cunning.___ The xvart can take the Disengage action as a bonus action on each of its turns.

___Overbearing Pack.___ The xvart has advantage on Strength (Athletics) checks to shove a creature if at least one of the xvart's allies is within 5 feet of the target and the ally isn't incapacitated.

___Raxivort's Tongue.___ The xvart can communicate with ordinary bats and rats, as well as giant bats and giant rats.

**Actions**

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) slashing damage.