"Asanbosam";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Giant"
_skills_: "Acrobatics +4, Perception +3, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "40 ft., climb 15 ft."
_hit points_: "102 (12d10 + 36)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 17 (+3) | 11 (+0) | 10 (+0) | 5 (-3) |

___Spider Climb.___ The asanbosam can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Arboreal.___ While up in trees, the asanbosam can take the Disengage or Hide action as a bonus action on each of its turns.

**Actions**

___Multiattack.___ The asanbosam makes one bite attack and one claws attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) piercing damage. If the target is a creature, it must succeed on a DC 14 Constitution saving throw against disease. If the saving throw fails, the target takes 11 (2d10) poison damage immediately and becomes poisoned until the disease is cured. Every 24 hours that elapse, the creature must repeat the saving throw and reduce its hit point maximum by 5 (1d10) on a failure. This reduction lasts until the disease is cured. The creature dies if the disease reduces its hit point maximum to 0.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 20 (3d10 + 4) piercing damage, and the target is grappled (escape DC 14). Until this grapple ends, the target is restrained and the asanbosam can't claw a different target. If the target is a creature, it must succeed on a DC 14 Constitution saving throw against disease or contract the disease described in the bite attack.

