"Felidar";;;_size_: Large celestial
_alignment_: lawful good
_challenge_: "5 (1,800 XP)"
_languages_: "Celestial, telepathy 60 ft."
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, paralyzed, poisoned"
_speed_: "50 ft."
_hit points_: "67 (9d10 + 18)"
_armor class_: "12"
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 11 (+0) | 17 (+3) | 16 (+3) |

___Pounce.___ If the felidar moves at least 20 feet straight toward a creature
and then hits it with a claws attack on the same turn, the target must
succeed on a DC 15 Strength saving throw or be knocked prone. If
the target is prone, the felidar can make one bite attack against it as a
bonus action.

___Innate Spellcasting.___ The felidar’s innate spellcasting ability is Charisma
(spell save DC 14). The felidar can innately cast the following spells,
requiring no components:

* At will: _detect evil and good, light, thaumaturgy_

* 1/day each: _calm emotions, daylight, dispel evil and good_

___Magic Resistance.___ The felidar has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The felidar’s weapon attacks are magical.

**Actions**

___Multiattack.___ The felidar makes two attacks with its claws.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) slashing damage.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Healing Touch (3/Day).___ The felidar touches another creature with its
horns. The target magically regains 11 (2d8 + 2) hit points. In addition,
the touch removes all diseases and neutralizes all poisons afflicting
the target.

___Teleport (1/Day).___ The felidar magically teleports itself and up to three
willing creatures it can see within 5 feet of it, along with any equipment
they are wearing or carrying, to a location the felidar is familiar with, up
to 1 mile away.

**Legendary** Actions

The felidar can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The felidar regains spent legendary
actions at the start of its turn.

___Claws.___ The felidar makes one attack with its claws.

___Shimmering Shield (Costs 2 Actions).___ The felidar creates a shimmering
magical field around itself or another creature it can see within 60 feet
of it. The target gains a +2 bonus to AC until the end of the felidar’s
next turn.

___Heal Self (Costs 3 Actions).___ The felidar magically regains 11 (2d8 + 2)
hit points.
