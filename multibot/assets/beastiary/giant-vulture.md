"Giant Vulture";;;_size_: Large beast
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "understands Common but can't speak"
_skills_: "Perception +3"
_speed_: "10 ft., fly 60 ft."
_hit points_: "22 (3d10+6)"
_armor class_: "10"
_stats_: | 15 (+2) | 10 (0) | 15 (+2) | 6 (-2) | 12 (+1) | 7 (-2) |

___Keen Sight and Smell.___ The vulture has advantage on Wisdom (Perception) checks that rely on sight or smell.

___Pack Tactics.___ The vulture has advantage on an attack roll against a creature if at least one of the vulture's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The vulture makes two attacks: one with its beak and one with its talons.

___Beak.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) piercing damage.

___Talons.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage.