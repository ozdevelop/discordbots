"Izek Strazni";;;_page_number_: 231
_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common"
_skills_: "Intimidation +8, Perception +2"
_speed_: "30 ft."
_hit points_: "112 (15d8+45)"
_armor class_: "14 (studded leather armor)"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 10 (0) | 9 (-1) | 15 (+2) |

___Brute.___ A melee weapon deals one extra die of its damage when Izek hits with it (included in the attack).

**Actions**

___Multiattack.___ Izek makes two attacks with his battleaxe.

___Battleaxe.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8+4) slashing damage, or 15 (2d10+4) when used with two hands.

___Hurl Flame.___ Ranged Spell Attack: +5 to hit, range 60 ft., one target. Hit: 10 (3d6) fire damage. If the target is a flammable object that isn't being worn or carried, it catches fire.