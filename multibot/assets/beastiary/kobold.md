"Kobold";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1/8 (25 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "5 (2d6-2)"
_armor class_: "12"
_stats_: | 7 (-2) | 15 (+2) | 9 (-1) | 8 (-1) | 7 (-2) | 8 (-1) |

___Sunlight Sensitivity.___ While in sunlight, the kobold has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Pack Tactics.___ The kobold has advantage on an attack roll against a creature if at least one of the kobold's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Sling.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 4 (1d4 + 2) bludgeoning damage.