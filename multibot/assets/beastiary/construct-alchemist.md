"Construct Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Common"
_senses_: "truesight 30 ft., passive Perception 10"
_saving_throws_: "Str +8, Int +7"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "136 (16d10 + 48)"
_armor class_: "16 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 16 (+3) | 17 (+3) | 10 (+0) | 15 (+2) |

___Weakened Mind.** The alchemist has disadvantage on
Charisma, Intelligence, and Wisdom saving throws.

___Advanced scanners.___ The alchemist uses his mechanical
eye to take in intricate details of his environment
hidden to the human eye. It has truesight out to 30 ft.

**Actions**

___Multiattack.___ The alchemist makes three attacks: two
with its servo-enhanced strike and one with its poison
vial.

___Servo-Enhanced Strike.___ Melee Weapon Attack: +7 to hit,
reach 5ft., one target. Hit: 11 (2d6 + 4) bludgeoning
damage.

___Poison Vial.___ Ranged Weapon Attack: +7 to hit, range
20/60 ft., one target. Hit: The target must make a DC
15 Constitution saving throw, taking 21 (6d6) poison
damage on a failed save, or half as much damage on a
successful one. A creature that fails this saving throw
by 5 or more is poisoned until the end of their next
turn.

___Poison Canister (Recharge 5-6).___ The alchemist launches
a canister filled with deadly poisonous gas at a point
within 30 feet. Each creature in a 10-foot-radius sphere
centered on that point must make a DC 15
Constitution saving throw, taking 38 (7d10) poison
damage on a failed save, or half as much damage on a
successful one. Creatures that failed the save are also
poisoned for 1 minute. A poisoned creature can repeat
the saving throw at the end of each of its turns, ending
the poisoned condition on itself on a success.

**Reactions**

___Automated Reflexes.___ The alchemist adds 3 to its AC
against one melee attack that would hit it. To use this
feature, the alchemist must see the attacker.

**Legendary** Actions

The alchemist can take 3 legendary actions, choosing
from the options below. Only one legendary action can
be used at a time and only at the end of another
creature’s turn. The alchemist regains spent legendary
actions at the start of its turn.

___Strike.___ The alchemist makes a servo-enhanced strike.

___Poison Burst (Costs 2 Actions).___ The alchemist opens up
a side compartment and unleashes a blast of poisonous
gas against each creature within 10 feet. Each creature
in this area must succeed on a DC 15 Constitution
saving throw or take 16 (3d10) poison damage.

___Rapid Repair (Costs 3 Actions).___ The alchemist performs
a swift operation on its damaged component and regains 30 hit points.
