"Guardian Portrait";;;_page_number_: 227
_size_: Medium construct
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "Common, plus up to two other languages"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "0 ft."
_hit points_: "22 (5d8)"
_armor class_: "5 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_stats_: | 1 (-5) | 1 (-5) | 10 (0) | 14 (+2) | 10 (0) | 10 (0) |

___Constructed Nature.___ An animated object doesn't require air, food, drink, or sleep.

>The magic that animates an object is dispelled when the construct drops to 0 hit points. An animated object reduced to 0 hit points becomes inanimate and is too damaged to be of much use or value to anyone.

___Antimagic Susceptibility.___ The portrait is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the portrait must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute.

___Innate Spellcasting.___ The portrait's innate spellcasting ability os Intelligence (spell save DC 12). The portrait can innately cast the following spells, requiring no material components:

* 3/day each: _counterspell, crown of madness, hypnotic pattern, telekinesis_

___False Appearance.___ While the figure in the portrait remains motionless, it is indistinguishable from a normal painting.
