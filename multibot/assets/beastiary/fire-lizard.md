"Fire Lizard";;;_size_: Huge monstrosity
_alignment_: neutral
_challenge_: "12 (8,400 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "fire"
_damage_vulnerabilities_: "cold"
_speed_: "30 ft."
_hit points_: "138 (12d12 + 60)"
_armor class_: "13 (natural armor)"
_stats_: | 25 (+7) | 10 (+0) | 20 (+5) | 2 (-4) | 11 (+0) | 10 (+0) |

___Rampage.___ When the fire lizard reduces a creature to 0 hit points with a
melee attack on its turn, the fire lizard can take a bonus action to move up
to half its speed and make a bite attack.

**Actions**

___Multiattack.___ The fire lizard makes three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 16
(2d8 + 7) piercing damage.

___Claws.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 21
(4d6 +7) slashing damage.

___Fire Breath (Recharge 5–6).___ The fire lizard exhales a blast of fire in
a 20-foot cone. Each creature in that area must make a DC 16 Dexterity
savings throw, taking 28 (8d6) fire damage on a failed save, or half as
much on a successful one.
