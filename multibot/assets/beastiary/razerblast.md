"Razerblast";;;_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Ignan"
_skills_: "Intimidation +4, Perception +3"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "112 (15d8+45)"
_armor class_: "17 (splint)"
_stats_: | 16 (+3) | 11 (0) | 16 (+3) | 9 (-1) | 10 (0) | 13 (+1) |

___Searing Armor.___ The razerblast's armor is hot. Any creature grappling the razerblast or grappled by it takes 5 (1d10) fire damage at the end of that creature's turn.

___Shrapnel Explosion.___ When the razerblast drops to 0 hit points, a flaming orb in its chest explodes, destroying the razerblast's body and scattering its armor as shrapnel. Creatures within 10 feet of the razerblast when it explodes must succeed on a DC 12 Dexterity saving throw, taking 21 (6d6) piercing damage on a failed save, or half as much damage on a successful one.

**Actions**

___Multiattack.___ The razerblast makes three melee attacks.

___Spear.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d6 + 3) piercing damage, or 7 (1d8 + 3) piercing damage if used with two hands to make a melee attack, plus 3 (1d6) fire damage.