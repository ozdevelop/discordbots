"Dwarf Noble";;;_page_number_: 999
_size_: Medium humanoid (dwarf)
_alignment_: any alignment
_challenge_: "1/8 (25 XP)"
_languages_: "any two languages"
_senses_: "darkvision, passive Perception 12"
_skills_: "Deception +5, Insight +4, Persuasion +5"
_speed_: "30 ft."
_hit points_: "9  (2d8)"
_armor class_: "15 (breastplate)"
_damage_resistances_: "poison"
_stats_: | 11 (0) | 12 (+1) | 11 (0) | 12 (+1) | 14 (+2) | 16 (+3) |

___Dwarven Resilience.___ The dwarf has advantage on saving throws against poison and has resistance to poison damage

**Actions**

___Rapier___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) piercing damage.
