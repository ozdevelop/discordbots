"Aetherspawn Paragon";;;_size_: Medium elemental
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "Primordial"
_skills_: "Arcana +7"
_senses_: "passive Perception 12"
_saving_throws_: "Dex +7, Int +7"
_damage_resistances_: "bludgeoning, piercing, slashing damage from nonmagical weapons; see Elemental Attunement"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "30 ft., fly 30 ft. (hover)"
_hit points_: "91 (14d8 + 28)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 19 (+4) | 15 (+2) | 18 (+4) | 15 (+2) | 12 (+1) |

___Elemental Attunement.___ The paragon is infused with
either arcane, ice, fire, or lightning energy. The
paragon's attacks deal extra damage based upon this
element (included in the attack), it has immunity to the
corresponding element, and can cast a corresponding
elemental spell twice per day requiring no material
components. The paragon's spellcasting ability is
Intelligence (spell save DC 15, +7 bonus to hit).

* Arcane – bonus force damage and immunity to force damage; can cast _slow_
* Ice - bonus cold damage and immunity to cold damage; can cast _sleet storm_
* Fire – bonus fire damage and immunity to fire damage; can cast _fireball_
* Lightning – bonus lightning damage and immunity to lightning damage; can cast _lightning bolt_

___Elemental Mastery.___ As a bonus action, the paragon can
shift its Elemental Attunement to another element.
Once the paragon has shifted to this element, it can't
do so again until the next dawn. This does not refresh
the paragon's two spells per day limit, but does change
the spell available to be cast.

___Fracturing Death.___ When the paragon dies, it explodes in
a blast of chromatic energies that attempt to reform in
four unoccupied spaces within 50 feet. At each of
these spaces a glowing orb of pure elemental energy
and runic stone glows and shifts – one orb for each of
the four possible elements of an aetherspawn. These
orbs have 15 hit points and AC 13. Roll for initiative
with no bonus for each of these orbs and add them to
the initiative order. Whenever it is the orb's turn, it
transforms into a aetherspawn guardian of the
corresponding element.

**Actions**

___Multiattack.___ The paragon makes two melee attacks.

___Aether Claws.___ Melee Weapon Attack: +7 to hit, reach 5
ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage
plus 7 (2d6) damage of the type corresponding to the
paragon's Elemental Attunement.

___Elemental Shards.___ Ranged Weapon Attack: +7 to hit,
range 30/90 ft., one target. Hit: 9 (2d4 + 4) piercing
damage plus 7 (2d6) damage of the type
corresponding to the paragon's Elemental Attunement.

___Living Meteor (Recharge 5-6).___ The paragon transforms
into a sphere of pure elemental energy and launches
itself in a straight line to an unoccupied space up to 90
feet away. Each creature in the line must make a DC 15
Dexterity saving throw, taking 22 (5d8) damage of the
type corresponding the paragon's Elemental
Attunement on a failed save, or half as much damage
on a successful one. Creatures within 5 feet of the
paragon's final location make the save with
disadvantage as the meteor erupts before the paragon
returns to its ordinary form.
