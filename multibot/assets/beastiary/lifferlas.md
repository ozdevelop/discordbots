"Lifferlas";;;_size_: Huge plant
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_languages_: "Common"
_speed_: "20 ft."
_hit points_: "59 (7d12+14)"
_armor class_: "13 (natural armor)"
_senses_: " passive Perception 10"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "blugdeoning, piercing"
_stats_: | 19 (+4) | 6 (-2) | 15 (+2) | 10 (0) | 10 (0) | 7 (-2) |

___False Appearance.___ While Lifferlas remains motionless, it is indistinguishable from a normal tree.

**Actions**

___Multiattack.___ Lifferlas makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 14 (3d6+4) bludgeoning damage.

**Roleplaying** Information

A druid of the Emerald Enclave awakened the tree Lifferlas with a spell. Goldenfields is his home, its people his friends. Children like to carve their name and initials into his body and hang from his boughs, and he's happy with that.

**Ideal:** "I exist to protect the people and plants of Goldenfields."

**Bond:** "Children are wonderful. I would do anything to make them feel happy and safe."

**Flaw:** "I can't remember people's names and often get them mixed up."