"Knight of Air";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Auran, Common"
_skills_: "Acrobatics +9, Stealth +9, Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Dex +9, Cha +6"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned"
_speed_: "40 ft., fly 40 ft."
_hit points_: "77 (14d8 + 14)"
_armor class_: "20 (plate, shield)"
_stats_: | 12 (+1) | 22 (+6) | 13 (+1) | 14 (+2) | 14 (+2) | 16 (+3) |

___Innate Spellcasting.___ The Knight’s innate spellcasting
ability is Wisdom (spell save DC 13, +5
to hit with spell attacks). It can innately cast the
following spells, requiring no components:

* Cantrips (at will): _mage hand _(the hand is invisible)

* 3/day each: _invisibility, misty step, gust of wind_

* 1/day each: _wind wall, gaseous form_

**Actions**

___Multiattack.___ The Knight makes three attacks with
the Sword of Clouds.

___The Sword of Clouds.___ Melee Weapon Attack: +9
to hit, reach 5 ft., one target. Hit: 15 (2d8 + 6)
piercing damage.
