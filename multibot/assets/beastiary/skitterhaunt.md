"Skitterhaunt";;;_size_: Large ooze
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 8"
_damage_immunities_: "acid"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_speed_: "30 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 11 (+0) | 19 (+4) | 1 (-5) | 7 (-2) | 1 (-5) |

___Broken Shell.___ A creature that hits the skitterhaunt with a melee attack while within 5 feet of it takes 5 (1d10) acid damage.

___Infest Vermin.___ If the skitterhaunt damages a Medium or smaller beast, it can try to infest it as a bonus action. The damaged creature must succeed on a DC 14 Constitution saving throw against disease or become poisoned until the disease is cured. Every 24 hours that elapse, the target must repeat the saving throw, reducing its hit point maximum by 5 (1d10) on a failure. If the disease reduces its hit point maximum to 0, the skitterhaunt has devoured the creature's insides and the affected becomes a skitterhaunt, retaining its outward shell but replacing its flesh with skitterhaunt ooze.

**Actions**

___Multiattack.___ The skitterhaunt makes two claw attacks and one sting attack.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage plus 5 (1d10) acid damage, and the target is grappled (escape DC 12). The skitterhaunt has two claws, each of which can grapple one target.

___Sting.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 7 (1d10 + 2) piercing damage plus 5 (1d10) acid damage.

___Acid Spray (Recharge 6).___ The skitterhaunt spits acid in a line 30 feet long and 5 feet wide. Each creature in that line takes 18 (4d8) acid damage, or half damage with a successful DC 14 Dexterity saving throw.

