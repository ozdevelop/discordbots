"Chimera";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "understands Draconic but can't speak"
_senses_: "darkvision 60 ft."
_skills_: "Perception +8"
_speed_: "30 ft., fly 60 ft."
_hit points_: "114 (12d10+48)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 11 (0) | 19 (+4) | 3 (-4) | 14 (+2) | 10 (0) |

**Actions**

___Multiattack.___ The chimera makes three attacks: one with its bite, one with its horns, and one with its claws. When its fire breath is available, it can use the breath in place of its bite or horns.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage.

___Horns.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 10 (1d12 + 4) bludgeoning damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Fire Breath (Recharge 5-6).___ The dragon head exhales fire in a 15-foot cone. Each creature in that area must make a DC 15 Dexterity saving throw, taking 31 (7d8) fire damage on a failed save, or half as much damage on a successful one.