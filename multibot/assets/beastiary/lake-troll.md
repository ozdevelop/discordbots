"Lake Troll";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Giant"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "20 ft., swim 40 ft."
_hit points_: "126 (12d10 + 60)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 13 (+1) | 20 (+5) | 8 (-1) | 10 (+0) | 6 (-2) |

___Amphibious.___ The lake troll can breathe air and water.

___Keen Smell.___ The lake troll has advantage on Wisdom (Perception) checks that rely on smell.

___Regeneration.___ The lake troll regains 10 hit points at the start of its turn. If the lake troll takes cold or fire damage, it regains only 5 hit points at the start of its next turn; if it takes both cold and fire damage, this trait doesn't function at the start of the lake troll's next turn. The lake troll dies only if it starts its turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack.___ The lake troll makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) slashing damage.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) piercing damage. If the lake troll hits a creature with both claw attacks in the same turn, the target creature must make a successful DC 16 Dexterity saving throw or its weapon (if any) gains a permanent and cumulative -1 penalty to damage rolls. If the penalty reaches -5, the weapon is destroyed. A damaged weapon can be repaired with appropriate artisan's tools during a long rest.

