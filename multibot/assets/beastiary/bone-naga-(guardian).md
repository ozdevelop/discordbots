"Bone Naga (Guardian)";;;_size_: Large undead
_alignment_: lawful evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common plus one other language"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "58 (9d10+9)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "charmed, exhaustion, paralyzed, poisoned"
_stats_: | 15 (+2) | 16 (+3) | 12 (+1) | 15 (+2) | 15 (+2) | 16 (+3) |

___Spellcasting.___ The naga is a 5th-level spellcaster (spell save DC 12, +4 to hit with spell attacks) that needs only verbal components to cast its spells. Its spellcasting ability is Wisdom, and it has the following cleric spells prepared:

* Cantrips (at will): _mending, sacred flame, thaumaturgy_

* 1st level (4 slots): _command, shield of faith_

* 2nd level (3 slots): _calm emotions, hold person_

* 3rd level (2 slots): _bestow curse_

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one creature. Hit: 10 (2d6 + 3) piercing damage plus 10 (3d6) poison damage.
