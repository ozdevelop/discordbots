"Hill Giant";;;_size_: Huge giant
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Giant"
_skills_: "Perception +2"
_speed_: "40 ft."
_hit points_: "105 (10d12+40)"
_armor class_: "13 (natural armor)"
_stats_: | 21 (+5) | 8 (-1) | 19 (+4) | 5 (-3) | 9 (-1) | 6 (-2) |

**Actions**

___Squash.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one Medium or Smaller creature. Hit: 26 (6d6 + 5) bludgeoning damage, the giant lands prone in the target's space, and the target is grappled (escape DC 15). Until this grapple ends, the target is prone. The grapple ends early if the giant stands up.

___Multiattack.___ The giant makes two greatclub attacks.

___Greatclub.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 18 (3d8 + 5) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +8 to hit, range 60/240 ft., one target. Hit: 21 (3d10 + 5) bludgeoning damage.