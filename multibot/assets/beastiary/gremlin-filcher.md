"Gremlin Filcher";;;_size_: Small fey
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Goblin, Sylvan"
_skills_: "Acrobatics +6, Arcana +5, Perception +5, Sleight of Hand +9, Stealth +9"
_senses_: "passive Perception 15"
_saving_throws_: "Dex +6, Int +5"
_speed_: "20 ft."
_hit points_: "45 (10d6 + 10)"
_armor class_: "15 (studded leather)"
_stats_: | 7 (-2) | 17 (+3) | 13 (+1) | 14 (+2) | 14 (+2) | 15 (+2) |

___Knot Expert.___ The gremlin has advantage on any check or saving throw
to break free of any effect grappling or restraining it when the effect is
made of rope or rope-like objects.

___Evasion.___ If the gremlin filcher is subjected to an efect that allows it to make
a Dexterity saving throw to take only half damage, the filcher instead takes no
damage if it succeeds on the saving throw, and only half damage if it fails.

___Sneak Attack.___ Once per turn, the filcher deals an extra 14 (4d6) damage
when it hits a target with a weapon attack and has advantage on the attack
roll, or when the target is within 5 feet of an ally of the gremlin that isn’t
incapacitated and the gremlin doesn’t have disadvantage on the attack roll.

___Innate Spellcasting.___ The gremlin’s innate spellcasting ability is
Charisma (spell save DC 13, +5 to hit with spell attacks). It can cast the
following spells without requiring material components.

* At will: _arcane lock, knock_

* 3/day: _find traps_

* 1/day: _passwall_

**Actions**

___Multiattack.___ The filcher makes two shortsword attacks and one bite attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Shortsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

**Bonus** Actions

___Cunning Action.___ The gremlin filcher can take the Dash, Disengage, or
Hide actions.

___Fast Hands.___ The gremlin filcher can use a bonus action to use an object
or to make a Dexterity (Sleight of Hand) check to pickpocket one object
on a target’s body which it is not holding. The target makes an opposed
Dexterity (Acrobatics) check if it is aware of the gremlin’s presence,
avoiding the attempt if the target wins. If the target is unaware of the
gremlin, the filcher’s attempt to pickpocket the target succeeds.
