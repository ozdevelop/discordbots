"Adult Flame Dragon";;;_size_: Huge dragon
_alignment_: chaotic evil
_challenge_: "16 (15000 XP)"
_languages_: "Common, Draconic, Giant, Ignan, Infernal, Orc"
_skills_: "Deception +10, Insight +7, Perception +12, Persuasion +10, Stealth +7"
_senses_: ", passive Perception 22"
_saving_throws_: "Dex +7, Con +11, Wis +7, Cha +10"
_damage_immunities_: "fire"
_speed_: "40 ft., climb 40 ft., fly 80 ft."
_hit points_: "212 (17d12 + 102)"
_armor class_: "19 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 23 (+6) | 17 (+3) | 14 (+2) | 20 (+5) |

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

___Fire Incarnate.___ All fire damage dealt by the dragon ignores fire resistance but not fire immunity.

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 15 (2d10 + 4) piercing damage plus 7 (2d6) fire damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Tail.___ Melee Weapon Attack: +9 to hit, reach 15 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 18 Wisdom saving throw or become frightened for 1 minute. A frightened creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours.

___Fire Breath (Recharge 5-6).___ The dragon exhales fire in a 60-foot cone. Each creature in that area takes 63 (18d6) fire damage, or half damage with a successful DC 19 Dexterity saving throw. Each creature in that area must also succeed on a DC 18 Wisdom saving throw or go on a rampage for 1 minute. A rampaging creature must attack the nearest living creature or smash some object smaller than itself if no creature can be reached with a single move. A rampaging creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Shifting Flames.___ The dragon magically polymorphs into a creature that has immunity to fire damage and a size and challenge rating no higher than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the dragon's choice). In a new form, the dragon retains its alignment, hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence, Wisdom, and Charisma scores, as well as this action. Its statistics and capabilities are otherwise replaced by those of the new form, except any class features or legendary actions of that form.

**Legendary** Actions

___The dragon can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each creature within 15 feet of the dragon must succeed on a DC 17 Dexterity saving throw or take 11 (2d6 + 4) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.

