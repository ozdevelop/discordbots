"Hobgoblin Warlord";;;_size_: Medium humanoid (goblinoid)
_alignment_: lawful evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_saving_throws_: "Int +5, Wis +3, Cha +5"
_speed_: "30 ft."
_hit points_: "97 (13d8+39)"
_armor class_: "20 (plate, shield)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 14 (+2) | 11 (0) | 15 (+2) |

___Martial Advantage.___ Once per turn, the hobgoblin can deal an extra 14 (4d6) damage to a creature it hits with a weapon attack if that creature is within 5 ft. of an ally of the hobgoblin that isn't incapacitated.

**Actions**

___Multiattack.___ The hobgoblin makes three melee attacks. Alternatively, it can make two ranged attacks with its javelins.

___Longsword.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1d10 + 3) slashing damage if used with two hands.

___Shield Bash.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one creature. Hit: 5 (1d4 + 3) bludgeoning damage. If the target is Large or smaller, it must succeed on a DC 14 Strength saving throw or be knocked prone.

___Javelin.___ Melee or Ranged Weapon Attack: +9 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Leadership (Recharges after a Short or Long Rest).___ For 1 minute, the hobgoblin can utter a special command or warning whenever a nonhostile creature that it can see within 30 feet of it makes an attack roll or a saving throw. The creature can add a d4 to its roll provided it can hear and understand the hobgoblin. A creature can benefit from only one Leadership die at a time. This effect ends if the hobgoblin is incapacitated.

**Reactions**

___Parry.___ The hobgoblin adds 3 to its AC against one melee attack that would hit it. To do so, the hobgoblin must see the attacker and be wielding a melee weapon.