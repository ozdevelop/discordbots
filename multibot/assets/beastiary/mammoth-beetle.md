"Mammoth Beetle";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft., passive Perception 10"
_speed_: "30 ft., fly 20 ft."
_hit points_: "116 (11d12 + 44)"
_armor class_: "20 (natural armor)"
_stats_: | 18 (+4) | 7 (-2) | 19 (+4) | 2 (-4) | 10 (+0) | 2 (-4) |

___Soft Underbelly.___ The beetle’s has AC 12 against
attacks made from directly beneath it.

___Acidic Blood.___ A creature that damages the beetle
with a melee attack while within 5 feet of it takes 5
(1d10) acid damage.

___Hulking Wings.___ The beetle has a flight speed of 20
ft. via a set of massive wings. This benefit works
only in short bursts; the beetle falls at the end of its
turn if it is in the air and nothing else is holding it
aloft. Any creature under the beetle when it lands
must make a DC 13 Dexterity saving throw or take
7 (2d6) bludgeoning damage for every 10 feet the
beetle fell.

**Actions**

___Multiattack.___ The beetle makes three attacks: two
with its stomp and one with its mandibles.

___Stomp.___ Melee Weapon Attack: +7 to hit, reach 5ft.,
one target. Hit: 9 (1d10 + 4) bludgeoning damage.

___Mandibles.___ Melee Weapon Attack: +7 to hit, reach
5ft., one target not underneath the beetle. Hit: 13
(2d8 + 4) piercing damage and the target is
grappled (escape DC 15) if it is a Large or smaller
creature and the beetle doesn’t already have a creature
grappled.
