"Cube of Anomalies";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "9 (5,000 XP)"
_languages_: "All"
_senses_: "truesight 60 ft., passive Perception 7"
_saving_throws_: "Con +7, Int +7"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "blinded, charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, restrained, unconscious"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "133 (14d10 + 56)"
_armor class_: "16 (natural armor)"
_stats_: | 10 (+0) | 10 (+0) | 18 (+4) | 18 (+4) | 5 (-3) | 1 (-5) |

___Legendary Resistance (2/Day).___ If the cube fails a saving
throw, it can choose to succeed instead.

**Actions**

___Cubic Assault.___ The anomaly shifts itself around and
unleashes two effects at random (reroll duplicates),
choosing one to two targets it can see within 120 feet
of it.

1. ___Red.___ _Locational Displacement._ The target must succeed
on a DC 15 Charisma saving throw or be teleported 50
feet into the air and luanched downward. They smash
into the ground at the end of their next turn, taking 35
(10d6) bludgeoning damage as they do so.

2. ___Blue.___ _Aquatic Prison._ The target is surrounded by 10
foot cube of water and is considered grappled (escape
DC 15). Until this grapple ends, the target is restrained
and unable to breathe unless it can breathe water. At
the start of each of the grappled creature’s turns, it
takes 14 (2d8 + 5) cold damage as freezing cold water
churns around them.
Another creature within 5 feet of the prison can
attempt to pull the trapped creature free by taking an
action to make a DC 15 Athletics check. On a success,
they free the target from the prison. On a natural 1,
they are pulled into the prison as well.

3. ___Yellow.___ _Blinding Beam._ The targeted creature must
succeed on a DC 15 Constitution saving throw, taking
27 (6d8) radiant damage and being blinded until the
end of their next turn on a failed save, or half as much
damage and not blinded on a success.

4. ___Purple.___ _Mental Onslaught._ The targeted creature must
make a DC 15 Intelligence saving throw, taking 27
(6d8) psychic damage and rolling their first attack on
their next turn with disadvantage on a failed save, or
half as much damage and not given disadvantage on a
successful one.

5. ___Green.___ _Ability Reassessment._ The targeted creature
must succeed on a DC 15 Charisma saving throw. On a
failed save, the target’s highest and lowest ability
scores are swapped. The creature can repeat the saving
throw at the end of each of its turns, ending the effect
on itself on a success.

6. ___Orange.___ _Barbed Bindings._ Tendrils of barbed wire burst
into existence and attempt to surround the targeted
creature. The target must succeed on a DC 15
Dexterity saving throw or take 14 (4d6) piercing
damage and become restrained. At the start of the
restrained creature’s turns it takes an additional 14
(4d6) damage. The creature can repeat the saving
throw at the end of each of its turns, ending the effect
on itself on a success.

**Legendary** Actions

The cube can take 3 legendary actions, choosing the
Continued Assault option below. It can only take one
legendary action at a time and only at the end of another
creature's turn. The cube regains spent legendary actions at
the start of its turn.

___Continued Assault.___ The cube uses one random ability of its Cubic Assault.
