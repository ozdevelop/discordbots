"Sundrop Squirrel";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_languages_: "--"
_senses_: "darkvision 30ft., passive Perception 10"
_speed_: "20 ft."
_hit points_: "1 (1d4 - 1)"
_armor class_: "11"
_stats_: | 2 (-4) | 13 (+1) | 8 (-1) | 3 (-4) | 10 (+0) | 5 (-3) |

___Floral Camouflage.___ While the squirrel remains
motionless, it is indistinguishable from an ordinary
cluster of sunflowers.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft.,
one target. Hit: 1 piercing damage.

___Disorienting Pollen (1/Day).___ The squirrel unleashes a
burst of pollen from the flowers that cover its body.
Each non-floral creature within 10 feet of the
squirrel must make a DC 10 Constitution saving
throw.

On a failed saving throw, the creature takes 2 (1d4)
poison damage and must roll a d8 at the start of its
next turn to determine what it does during that
turn. On a 1 to 2, the creature sneezes
uncontrollably and can move but can’t take an
action or a bonus action. On a 3-4, the creature's
eyes water uncontrollably and it is considered
blinded this turn. On a 5-8, the creature fights the
additional effects of the pollen and can act
normally.
