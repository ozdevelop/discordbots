"Tomb Guardian";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60ft., passive Perception 8"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "13 (natural armor)"
_stats_: | 20 (+5) | 8 (-1) | 19 (+4) | 4 (-3) | 6 (-2) | 1 (-5) |

___Immutable Form.___ The guardian is immune to any
spell or effect that would alter its form.

___Magic Resistance.___ The guardian has advantage on
saving throws against spell and other magical
effects.

___Magic Weapons.___ The guardian's weapon attacks are
magical.

___Bound Regeneration.___ The guardian is bound to an
item or set of items within the tomb it guards. At
the beginning of the guardian's turn, it regains 10
hit points as long as that item is not destroyed. This
regeneration continues even after the guardian is
reduced to 0 hit points and it can only be
permanently killed when this item (or these items)
have been removed.

**Actions**

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one target. Hit: 14 (2d8 + 5) bludgeoning damage.

___Debris Toss.___ Ranged Weapon Attack: +8 to hit,
range 30/90 ft., one target. Hit: 12 (2d6 + 5)
bludgeoning damage.

**Legendary** Actions

The guardian can take 2 legendary actions,
choosing from the options below. Only one
legendary action option can be used at a time and
only at the end of another creature's turn. The
guardian regains spent legendary actions at the
start of its turn.

___Pummel.___ The guardian makes a slam attack.

___Spark of Power.___ A spark of energy lashes out from
the item to which the guardian is bound. Target
creature within 10 feet of that item must make a
DC 15 Dexterity saving throw, taking 14 (4d6)
lightning damage on a failed save, or half as much
damage on a successful one.

**Lair** Actions

On initiative count 20 (losing initiative ties), the guardian
takes a lair action to cause one of the following effects; the
guardian can’t use the same effect two rounds in a row:

* Poisonous gas floods out of a crack in the floor or ceiling and fills a 20 foot cube at a point it can see within 120 feet. Any creature that starts its turn within this cloud of gas or enters it for the first time on a turn must succeed on a DC 15 Constitution saving throw or become poisoned until the end of its next turn. This cloud persists for one minute or until dispersed by a powerful wind.
* A bloated corpse within the room rises from the dead and begins to shamble towards the nearest hostile creature. This zombie moves 25 feet per round, moving on initiative count 20 (losing initiative ties). This zombie his 15 hit points and AC 10. When the zombie reaches a target or is reduced to 0 hit points, it swells and then explodes in a gruesome wave of putrid organs and bile. Each creature within 5 feet of the zombie must make a DC 15 Dexterity saving throw, taking 14 (4d6) necrotic damage on a failed save, or have as much damage on a successful one.
* The spirits of the dead rise to haunt a creature within 120 feet that the guardian can see. That creature must succeed on a DC 15 Wisdom saving throw or become frightened until the end of its next turn. While frightened, that creature’s movement speed is reduced to 0.
