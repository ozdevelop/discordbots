"Boarfolk";;;_size_: Large humanoid (boarfolk)
_alignment_: lawful neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Boarfolk, Common"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "16 (3d10)"
_armor class_: "10"
_stats_: | 12 (+1) | 10 (+0) | 10 (+0) | 10 (+0) | 10 (+0) | 11 (+0) |

___Created Race.___ The boarfolk has advantage on saving throws against
spells and effects which would alter its form.

___Keen Scent.___ A boarfolk has advantage on Wisdom (Perception)
checks based on scent.

**Actions**

___Gore.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4
(1d6 + 1) piercing damage.

___Greatclub.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit:
5 (1d8 + 1) bludgeoning damage.
