"Sly Songwriter";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Acrobatics +4, Deception +5, History +3, Performance +7, Sleight of Hand +6"
_senses_: "passive Perception 11"
_saving_throws_: "Dex +4, Cha +5"
_speed_: "30 ft."
_hit points_: "24 (4d8 + 4)"
_armor class_: "14 (studded leather)"
_stats_: | 10 (+0) | 14 (+2) | 12 (+1) | 12 (+1) | 12 (+1) | 16 (+3) |

___Spellcasting.___ The songwriter is a 3th-level spellcaster.
Its spellcasting ability is Charisma (spell save DC
13, +5 to hit with spell attacks). The songwriter has
the following bard spells prepared:

* Cantrips (at will): _dancing lights, message, vicious mockery_

* 1st level (4 slots): _charm person, comprehend languages, tasha's hideous laughter_

* 2nd level (2 slots): _enhance ability, hold person, silence_

___Cutting Words (3/Short Rest).___ When a creature that
the songwriter can see within 50 feet makes an
attack roll, ability check, or a damage roll, the
songwriter sings a quick disruptive melody. Roll a d8
and subtract the number from the creature's roll.

**Actions**

___Rapier.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 6 (1d8 + 2) piercing damage.
