"Ettercap";;;_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +4, Survival +3"
_speed_: "30 ft., climb 30 ft."
_hit points_: "44 (8d8+8)"
_armor class_: "13 (natural armor)"
_stats_: | 14 (+2) | 15 (+2) | 13 (+1) | 7 (-2) | 12 (+1) | 8 (-1) |

___Spider Climb.___ The ettercap can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Web Sense.___ While in contact with a web, the ettercap knows the exact location of any other creature in contact with the same web.

___Web Walker.___ The ettercap ignores movement restrictions caused by webbing.

**Actions**

___Multiattack.___ The ettercap makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 6 (1d8 + 2) piercing damage plus 4 (1d8) poison damage. The target must succeed on a DC 11 Constitution saving throw or be poisoned for 1 minute. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) slashing damage.

___Web (Recharge 5-6).___ Ranged Weapon Attack: +4 to hit, range 30/60 ft., one Large or smaller creature. Hit: The creature is restrained by webbing. As an action, the restrained creature can make a DC 11 Strength check, escaping from the webbing on a success. The effect ends if the webbing is destroyed. The webbing has AC 10, 5 hit points, is vulnerable to fire damage and immune to bludgeoning damage.

___Variant: Web Garrote.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one Medium or Small creature against which the ettercap has advantage on the attack roll. Hit: 4 (1d4 + 2) bludgeoning damage, and the target is grappled (escape DC 12). Until this grapple ends, the target can't breathe, and the ettercap has advantage on attack rolls against it.