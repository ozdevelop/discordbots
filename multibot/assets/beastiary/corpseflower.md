"Corpse Flower";;;_page_number_: 127
_size_: Large plant
_alignment_: chaotic evil
_challenge_: "8 (3900 XP)"
_languages_: "-"
_senses_: "blindsight 120 ft. (blind beyond this radius), passive Perception 12"
_speed_: "20 ft., climb 20 ft."
_hit points_: "127  (15d10 + 45)"
_armor class_: "12"
_condition_immunities_: "blinded, deafened"
_stats_: | 14 (+2) | 14 (+2) | 16 (+3) | 7 (-1) | 15 (+2) | 3 (-3) |

___Corpses.___ When first encountered, a corpse flower contains the corpses of 1d6 + 3 humanoids. A corpse flower can hold the remains of up to nine dead humanoids. These remains have total cover against attacks and other effects outside the corpse flower. If the corpse flower dies, the corpses within it can be pulled free.
While it has at least one humanoid corpse in its body, the corpse flower can use a bonus action to do one of the following:
> The corpse flower digests one humanoid corpse in its body and instantly regains 11 (2d10) hit points. Nothing of the digested body remains. Any equipment on the corpse is expelled from the corpse flower in its space.
> The corpse flower animates one dead humanoid in its body, turning it into a zombie. The zombie appears in an unoccupied space within 5 feet of the corpse flower and acts immediately after it in the initiative order. The zombie acts as an ally of the corpse flower but isn't under its control, and the flower's stench clings to it (see the Stench of Death trait).

___Spider Climb.___ The corpse flower can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Stench of Death.___ Each creature that starts its turn within 10 feet of the corpse flower or one of its zombies must make a DC 14 Constitution saving throw, unless the creature is a construct or undead. On a failed save, the creature is incapacitated until the end of the turn. Creatures that are immune to poison damage or the poisoned condition automatically succeed on this saving throw. On a successful save, the creature is immune to the stench of all corpse flowers for 24 hours.

**Actions**

___Multiattack___ The corpse flower makes three tentacle attacks.

___Tentacle___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage, and the target must succeed on a DC 14 Constitution saving throw or take 14 (4d6) poison damage.

___Harvest the Dead___ The corpse flower grabs one unsecured dead humanoid within 10 feet of it and stuffs the corpse into itself, along with any equipment the corpse is wearing or carrying. The remains can be used with the Corpses trait.