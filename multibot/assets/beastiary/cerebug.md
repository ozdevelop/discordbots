"Cerebug";;;_size_: Tiny aberration
_alignment_: lawful evil
_challenge_: "2 (700 XP)"
_languages_: "understands Common, Telepathy 60 ft."
_senses_: "Blindsight 30 ft., Tremorsense 30 ft., Passive Perception 12"
_skills_: "Perception +2, Stealth +4"
_condition_immunities_: "Blinded"
_speed_: "20 ft., burrow 40 ft., climb 20 ft."
_hit points_: "21 (6d4 + 6)"
_armor class_: "14 (natural armor)"
_stats_: | 8 (-1) | 14 (+2) | 13 (+1) | 12 (+1) | 10 (+0) | 6 (-2) |

___Hunger for the Warm Blooded.___ A cerebug can smell the
presence of warm blooded creatures even when buried
below the surface to a range of 30 ft. even if those scents
are covered by perfumes, rot or other strong odors.

___Spider Climb.___ A cerebug can climb difficult surfaces,
including upside down on ceilings, without
needing to make an ability check.

**Actions**

___Multiattack.___ The cerebug makes one attack with its
burrowing claws and one with its sonic drill.

___Burrowing Claws.___ Melee Weapon Attack: +2 to hit,
reach 5 ft., one target, Hit: 5 (2d4) slashing damage.

___Parasitic Domination.___ The cerebug targets a stunned
creature within 5 ft. and attempts to burrow into its
skull cavity. The cerebug makes an attack with its burrowing
claws, and any successful attack is a critical hit.
If this damage reduces the target to 0 hit points, the
cerebug burrows into the skull and eats the brain, killing
the creature. It then occupies the skull cavity and
takes control of the host body. The host body begins to
rot within 24 hours, and the cerebug attempts to use
the host body to lure new victims and consume more
brains. If the cerebug leaves the host body, the dead
creature can be restored to life with a resurrection spell
or similar magic to restore the missing brain.

___Sonic Drill.___ The cerebug targets one creature it can
sense within 10 ft. It emits a focused droning beam
of sonic energy intended to stun the target. The
target must succeed on a DC 12 Intelligence saving
throw or take 11 (2d10) thunder damage and be
stunned until the end of the cerebug’s next turn.
