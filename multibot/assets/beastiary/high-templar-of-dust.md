"High Templar of Dust";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "10 (5,900 XP)"
_languages_: "Aquan, Auran, Common, Ignan, Terran"
_senses_: "darkvision 60 ft., passive Perception 17"
_saving_throws_: "Str +9, Con +9, Wis +7, Cha +5"
_damage_immunities_: "fire, lightning, cold, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "30 ft."
_hit points_: "189 (18d10 + 90)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 22 (+6) | 20 (+5) | 17 (+3) | 16 (+3) | 13 (+1) |

___Sandstorm.___ At the start of each of the Templar’s
turns, each creature within 5 feet of it takes 5
(1d10) piercing damage.

___Scour (Recharge 5–6).___ The Templar moves up to
its speed without provoking opportunity attacks.
It can move through creatures, and each creature
it passes through must make a DC 15 Constitution
saving throw or take 10 (3d6) force damage.

**Actions**

___Multiattack.___ The Templar makes four attacks with
its halberd, Lightning, or Fire. Lightning and Fire
can only be used twice each.

___Halberd.___ Melee Weapon Attack: +9 to hit,
reach 10 ft., one target. Hit: 10 (1d10 + 5) slashing
damage.

___Lightning.___ Ranged Spell Attack: +7 to hit, range
60 ft., one target. Hit: 10 (3d6) lightning damage.

___Fire.___ The Templar hurls a ball of fire that
explodes at a point it can see within 60 feet.
Each creature within a sphere of 10-foot radius
centered on that point must make a DC 15 Dexterity
saving throw. The sphere spreads around
corners. On a failed save, a creature takes 10
(3d6) fire damage, or half as much damage on a
successful one.
