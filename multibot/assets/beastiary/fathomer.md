"Fathomer";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Aquan, Common"
_skills_: "Arcana +2, Perception +4, Stealth +4"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "10 (13 with mage armor)"
_stats_: | 14 (+2) | 11 (0) | 14 (+2) | 11 (0) | 11 (0) | 15 (+2) |

___Shapechanger (2/day).___ The fathomer can use its action to polymorph into a Medium serpent composed of water, or back into its true form. Anything the fathomer is wearing or carrying is subsumed into the serpent form during the change, inaccessible until the fathomer returns to its true form. The fathomer reverts to its true form after 4 hours, unless it can expend another use of this trait. If the fathomer is knocked unconscious or dies, it also reverts to its true form.

While in serpent form, the fathomer gains a swimming speed of 40 feet, the ability to breathe underwater, immunity to poison damage, as well as resistance to fire damage and bludgeoning, piercing, and slashing damage from nonmagical weapons. It also has immunity to the following conditions: exhaustion, grappled, paralyzed, poisoned, restrained, prone, unconscious. The serpent form can enter a hostile creature's space and stop there. In addition, if water can pass through a space, the serpent can do so without squeezing.

___Olhydra's Armor (Human Form Only).___ The fathomer can cast mage armor at will, without expending material components.

___Spellcasting (Human Form Only).___ The fathomer is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). It has two 3rd-level spell slots, which it regains after finishing a short or long rest, and knows the following warlock spells:

* Cantrips (at will): _chill touch, eldritch blast, mage hand_

* 1st level: _armor of Agathys, expeditious retreat, hex_

* 2nd level: _invisibility_

* 3rd level: _vampiric touch_

**Actions**

___Constrict (Serpent Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage. If the target is Medium or smaller, it is grappled (escape DC 12). Until the grapple ends, the target is restrained, and the fathomer can't constrict another target.

___Dagger (Human Form Only).___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
