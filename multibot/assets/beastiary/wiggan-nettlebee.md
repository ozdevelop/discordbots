"Wiggan Nettlebee";;;_size_: Small humanoid (halfling)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Halfling"
_skills_: "Deception +3, Insight +4"
_speed_: "30 ft."
_hit points_: "36 (8d6+8)"
_armor class_: "11 (16 with barkskin)"
_stats_: | 8 (-1) | 12 (+1) | 12 (+1) | 14 (+2) | 15 (+2) | 13 (+1) |

___Brave Devotion.___ Wiggan has advantage on saving throws against being charmed or frightened.

___Spellcasting.___ Wiggan is a 4th-level spellcaster. His spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). He has the following cleric spells prepared:

* Cantrips (at will): guidance, light, mending, shillelagh, thaumaturgy

* 1st Level (4 slots): animal friendship, cure wounds, healing word, inflict wounds, speak with animals

* 2nd Level (3 slots): barkskin, spike growth, spiritual weapon

**Actions**

___Multiattack.___ Wiggan makes two attacks with his wooden cane.

___Wooden Cane.___ Melee Weapon Attack: +0 to hit (+4 to hit with shillelagh), reach 5 ft., one target. Hit: 1 (1d4-1) bludgeoning damage, or 6 (1d8 + 2) bludgeoning damage with shillelagh.