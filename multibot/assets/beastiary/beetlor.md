"Beetlor";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 120 ft., tremorsense 60 ft., passive Perception 10"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft., burrow 15 ft."
_hit points_: "60 (8d8 + 24)"
_armor class_: "16 (natural armor)"
_stats_: | 22 (+6) | 18 (+4) | 17 (+3) | 10 (+0) | 10 (+0) | 8 (-1) |

___Hypnotic Gaze.___ When a creature starts its turn within 30 feet of the beetlor and is able
to see the beetlor’s multifaceted eyes, the beetlor can force the creature to
make a DC 15 Charisma saving throw if the beetlor isn’t incapacitated.
On a failure, the creature can’t take reactions until the start of its next
turn and rolls a d8 to determine what it does during its turn:

* 1-4: The creature does nothing.
* 5-6: The creature takes no action or bonus action and uses all its movement to move in a randomly determined direction. 
* 7-8: The creature makes a melee attack against a randomly determined creature within its reach or does nothing if it can’t make such an attack.

A creature that isn’t surprised can avert its eyes
to avoid the saving throw at the start of its turn. If
the creature does so, it can’t see the beetlor until the
start of its next turn, when it can avert its eyes again.
If the creature looks at the beetlor in the meantime, it
must immediately make the save.

___Burrow.___ The beetlor can burrow through solid rock,
leaving a 6 foot-wide, 10-foot-high tunnel its wake.

**Actions**

___Multiattack.___ The beetlor makes three attacks: one with its
bite and two with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 11 (1d10 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 13 (2d6 + 6) slashing damage.
