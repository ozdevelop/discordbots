"Gnoll Flesh Gnawer";;;_size_: Medium humanoid (gnoll)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Gnoll"
_senses_: "darkvision 60 ft."
_saving_throws_: "Dex +4"
_speed_: "30 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "14 (studded leather armor)"
_stats_: | 12 (+1) | 14 (+2) | 12 (+1) | 8 (-1) | 10 (0) | 8 (-1) |

___Rampage.___ When the gnoll reduces a creature to 0 hit points with a melee attack on its turn, the gnoll can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Multiattack.___ The gnoll makes three attacks: one with its bite and two with its shortsword.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4+2) piercing damage.

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft, one target. Hit: 5 (1d6+2) piercing damage.

___Sudden Rush.___ Until the end of the turn, the gnoll's speed increases by 60 feet and it doesn't provoke opportunity attacks.