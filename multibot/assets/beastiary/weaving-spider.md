"Weaving Spider";;;_size_: Tiny construct
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "understands Common"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "poison"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, poisoned"
_speed_: "40 ft., climb 40 ft."
_hit points_: "25 (10d4)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (+0) | 16 (+3) | 10 (+0) | 9 (-1) | 8 (-1) | 8 (-1) |

___Immutable Form.___ The weaving spider is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The weaving spider has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The weaving spider makes two trimming blade attacks or two needle shuttle attacks.

___Trimming Blade.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage and possible unmaking.

___Poisoned Needle Shuttle.___ Ranged Weapon Attack: +5 to hit, range 30 ft., one target. Hit: 7 (1d8 + 3) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or become paralyzed. The target repeats the saving throw at the end of each of its turns, ending the effect on itself with a success.

___Unmaking.___ The weaving spider's speed and its slim, sharp blade can slice cloth, leather, and paper into scraps very quickly. Whenever a weaving spider's trimming blade attack roll exceeds the target's armor class by 5 or more, the target must succeed on a DC 13 Dexterity saving throw or one of their possessions becomes unusable or damaged until repaired (DM's choice)

