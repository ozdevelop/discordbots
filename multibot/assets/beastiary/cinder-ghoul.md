"Cinder Ghoul";;;_size_: Large undead
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "fire"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, paralyzed, poisoned, unconscious"
_speed_: "fly 40 ft. (hover)"
_hit points_: "90 (12d10 + 24)"
_armor class_: "18 (natural armor)"
_stats_: | 16 (+3) | 20 (+5) | 15 (+2) | 4 (-3) | 12 (+1) | 19 (+4) |

___Gaseous Form.___ While in this form, the cinder ghoul can’t take any
actions, speak, or manipulate objects. It is weightless, has a flying speed
of 40 feet, can hover, and can enter a hostile creature’s space and stop
there. In addition, if air can pass through a space, the mist can do so
without squeezing. It cannot pass through water. It has advantage on
Strength, Dexterity, and Constitution saving throws, and it is immune to
all nonmagical damage.

___Magic Resistance.___ The cinder ghoul has advantage on saving throws
against spells and other magical effects.

**Actions**

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14
(2d8 + 5) bludgeoning damage plus 7 (2d6) fire damage.

___Smoke Inhalation.___ One creature that isn’t a construct or undead and
is in the cinder ghoul’s space must make a DC 15 Constitution saving
throw. On a failed save, the target takes 10 (3d6) fire damage and its hit
point maximum is reduced by an amount equal to the fire damage taken.
The target dies if this reduces its hit point maximum to 0. This reduction
of the target’s hit point maximum lasts until the target finishes a long rest.
