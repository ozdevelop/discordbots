"Swarm of Poisonous Snakes";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "blindsight 10 ft."
_speed_: "30 ft., swim 30 ft."
_hit points_: "36 (8d8)"
_armor class_: "14"
_condition_immunities_: "charmed, frightened, grappled, paralyzed, petrified, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, slashing"
_stats_: | 8 (-1) | 18 (+4) | 11 (0) | 1 (-5) | 10 (0) | 3 (-4) |

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny snake. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Bites.___ Melee Weapon Attack: +6 to hit, reach 0 ft., one creature in the swarm's space. Hit: 7 (2d6) piercing damage, or 3 (1d6) piercing damage if the swarm has half of its hit points or fewer. The target must make a DC 10 Constitution saving throw, taking 14 (4d6) poison damage on a failed save, or half as much damage on a successful one.