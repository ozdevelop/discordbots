"Breex Vandermast";;;_size_: Medium humanoid (half-orc)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Abyssal, Common, Orc"
_skills_: "Animal Handling +2, Athletics +5, Deception +4, Persuasion +4"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "71 (11d8 + 21)"
_armor class_: "16 (breastplate)"
_stats_: | 16 (+3) | 14 (+2) | 14 (+2) | 11 (+0) | 10 (+0) | 15 (+2) |

___Relentless Endurance (1/Long Rest).___ When Breex is reduced to 0 hit points, but not killed outright, he can drop to 1 hit point instead.

___Savage Attack.___ When Breex scores a critical hit with a melee weapon attack, he can roll one of the weapon's damage dice one additional time and add it to the extra damage of the critical hit.

___Spellcasting.___ Breex is a 5th-level spellcaster. His spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). Breex knows the following sorcerer spells:

* Cantrips (at will): _chill touch, firebolt, message, minor image, shocking grasp_

* 1st level (4 slots): _expeditious retreat, feather fall, sleep_

* 2nd level (3 slots): _blur, misty step_

* 3rd level (2 slots): _haste_

**Actions**

___Multiattack.___ Breex makes two melee attacks.

___Greatsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage.

**Reactions**

___Parry.___ Breex adds 2 to his AC against one melee attack that would hit him. To do so, Breex must see the attacker and be wielding a melee weapon.
