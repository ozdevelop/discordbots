"Gnoll Havoc Runner";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Gnoll"
_skills_: "Athletics +5, Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "30 ft."
_hit points_: "58 (9d8 + 18)"
_armor class_: "15 (chain shirt)"
_stats_: | 16 (+3) | 14 (+2) | 14 (+2) | 8 (-1) | 12 (+1) | 9 (-1) |

___Harrying Attacks.___ If the gnoll attacks two creatures in the same turn, the first target has disadvantage on attack rolls until the end of its next turn.

___Pack Tactics.___ The gnoll has advantage on its attack rolls against a target if at least one of the gnoll's allies is within 5 feet of the target and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The gnoll makes one bite attack and two battleaxe attacks.

___Battleaxe.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage or 8 (1d10 + 3) slashing damage if used in two hands.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

**Bonus** Actions

___Lightning Lope.___ The gnoll takes the Dash or Disengage action.
