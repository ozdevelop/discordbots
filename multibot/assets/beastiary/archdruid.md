"Archdruid";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "12 (8,400 XP)"
_languages_: "Druidic plus any two languages"
_skills_: "Medicine +9, Nature +5, Perception +9"
_saving_throws_: "Int +5, Wis +9"
_speed_: "30 ft."
_hit points_: "132 (24d8+24)"
_armor class_: "16 (hide armor, shield)"
_stats_: | 10 (0) | 14 (+2) | 12 (+1) | 12 (+1) | 20 (+5) | 11 (0) |

___Spellcasting.___ The archdruid is an 18th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 17, +9 to hit with spell attacks). It has the following druid spells prepared:

* Cantrips (at will): _druidcraft, mending, poison spray, produce flame_

* 1st level (4 slots): _cure wounds, entangle, faerie fire, speak with animals_

* 2nd level (3 slots): _animal messenger, beast sense, hold person_

* 3rd level (3 slots): _conjure animals, meld into stone, water breathing_

* 4th level (3 slots): _dominate beast, locate creature, stoneskin, wall of fire_

* 5th level (3 slots): _commune with nature, mass cure wounds, tree stride_

* 6th level (1 slot): _heal, heroes' feast, sunbeam_

* 7th level (1 slot): _fire storm_

* 8th level (1 slot): _animal shapes_

* 9th level (1 slot): _foresight_

**Actions**

___Scimitar.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) slashing damage.

___Change Shape (2/Day).___ The archdruid magically polymorphs into a beast or elemental with a challenge rating of 6 or less, and can remain in this form for up to 9 hours. The archdruid can choose whether its equipment falls to the ground, melds with its new form, or is worn by the new form. The archdruid reverts to its true form if it dies or falls unconscious. The archdruid can revert to its true form using a bonus action on its turn.

While in a new form, the archdruid retains its game statistics and ability to speak, but its AC, movement modes, Strength, and Dexterity are replaced by those of the new form, and it gains any special senses, proficiencies, traits, actions, and reactions (except class features, legendary actions, and lair actions) that the new form has but that it lacks. It can cast its spells with verbal or somatic components in its new form.

The new form's attacks count as magical for the purpose of overcoming resistances and immunity to nonmagical attacks.
