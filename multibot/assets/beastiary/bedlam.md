"Bedlam";;;_size_: Large aberration
_alignment_: chaotic neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Common, choice of two others"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "petrification"
_speed_: "fly 50 ft."
_hit points_: "112 (15d10 + 30)"
_armor class_: "17 (natural armor)"
_stats_: | 17 (+3) | 20 (+5) | 15 (+2) | 15 (+2) | 15 (+2) | 12 (+1) |

___Chaotic Resonance.___ The bedlam emanates an aura of pure chaos. The
aura deals 7 (2d6) psychic damage to any creature that ends its turn within
30 feet of the bedlam. Creatures attempting to cast a spell or use a magic
item in this area have a 50% chance of the spell fizzling or the item not
functioning. Creatures that are chaotic in nature ignore the effect of the
bedlam’s chaotic resonance.

___Magic Resistance.___ The bedlam has advantage on saving throws against
spells and other magic effects.

___Sense Law.___ The bedlam can sense the presence and location of any
creature within 120 feet of it that is lawfully aligned, regardless of
interposing barriers, unless the creature is protected by a spell such as
nondetection.

**Actions**

___Multiattack.___ The bedlam makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 14
(2d8 + 5) bludgeoning damage.

___Chaos Burst (Recharge 5–6).___ The bedlam releases a burst of crackling
gray energy in a 20-foot radius. Creatures in this area must make a DC 14
Wisdom saving throw, taking 22 (5d8) psychic damage on a failed save,
and half as much damage on a successful one. Creatures that are chaotic
in nature ignore the bedlam’s chaos burst.
