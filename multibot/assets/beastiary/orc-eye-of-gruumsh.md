"Orc Eye of Gruumsh";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +3, Religion +1"
_speed_: "30 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "16 (ring mail, shield)"
_stats_: | 16 (+3) | 12 (+1) | 17 (+3) | 9 (-1) | 13 (+1) | 12 (+1) |

___Aggressive.___ As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.

___Gruumsh's Fury.___ The orc deals an extra 4 (1d8) damage when it hits with a weapon attack (included in the attacks).

___Spellcasting.___ The orc is a 3rd-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 11, +3 to hit with spell attacks). The orc has the following cleric spells prepared:

* Cantrips (at will): _guidance, resistance, thaumaturgy_

* 1st level (4 slots): _bless, command_

* 2nd level (2 slots): _augury, spiritual weapon _(spear)

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 11 (1d6 + 3 plus 1d8) piercing damage, or 12 (2d8 + 3) piercing damage if used with two hands to make a melee attack.
