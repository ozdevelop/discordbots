"Living Trap";;;_size_: Large construct
_alignment_: neutral evil
_challenge_: "12 (8,400 XP)"
_languages_: "Understands Common but speaks only through the use of its Mimicry trait"
_skills_: "Stealth +4"
_senses_: "darkvision 120 ft., passive Perception 10"
_damage_immunities_: "poison, psychic; bludgeoning, piercing and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "210 (20d10 + 100)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 10 (+0) | 20 (+5) | 5 (-3) | 11 (+0) | 1 (-5) |

___Shapechanger.___ The living trap can use its action to
polymorph into a section of ruined terrain no larger than
10 foot radius sphere. This form can include objects and
structural features such as doors and pillars. Its statistics
are the same in each form. It reverts to its true form if it
dies.

___False Appearance (terrain form only).___ The living trap is indistinguishable from an ordinary section of ruined terrain.

___Mimicry.___ The living trap can mimic any sounds it has
heard, including voices. A creature that hears the
sounds can tell they are imitations with a successful DC
17 Wisdom (Insight) check.

**Actions**

___Multiattack.___ The living trap makes two melee attacks.

___Slam.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one
target. Hit: 19 (3d8 + 6) bludgeoning damage.

**Legendary** Actions

The living trap can take 3 legendary actions, choosing
from the options below. Only one legendary only can be
used at a time and only at the end of another creature’s
turn. The living trap regains spent legendary actions at
the start of its turn.

___Detect.___ The living trap makes a Wisdom (Perception) check.

___Slam Attack.___ The living trap makes a slam attack.

___Entangle (Costs 2 Actions).___ Each creature within 15 feet
of the living trap must succeed on a DC 17 Dexterity
saving throw or it is grappled (Escape DC 16).

**Lair** Actions

On initiative count 20 (losing initiative ties), the living trap takes a lair action to cause one of the following effects; the living trap can’t use the same effect two rounds in a row:

* Walls that the living trap can see within 120 feet sprout slashing blades. Any creature within 5 feet of such a wall must make a DC 17 Dexterity saving throw, taking 19 (3d8+6) slashing damage on a failed save, and half as much damage on a successful one.

* A 40-foot-deep pit opens at a point the living trap chooses within 120 feet of it, occupying a 10-foot-radius area. Creatures standing over the pit when it appears must succeed on a DC 17 Dexterity saving throw to leap clear. The pit remains until the living trap dismisses it as an action, uses this lair action again, or dies. Any creatures inside the pit when it disappears are transported to the surface.

* Magical darkness spreads from a point the living trap chooses within 60 feet of it, filling a 15-foot-radius sphere until the living trap dismisses it as an action, uses this lair action again, or dies. The darkness spreads around corners. A creature with darkvision can’t see through this darkness, and nonmagical light can’t illuminate it. If any of the effect’s area overlaps with an aura of light created by a spell of 2nd level or lower, the spell that created the light is dispelled.

**Regional** Effects

The region containing the living trap’s lair is warped by wild magic, which creates one or more of the following effects:

* If a creature takes damage within one mile of the living trap’s lair, the living trap is immediately aware of its location.

* Masonry and unattended objects within 1 mile of the lair appear cracked and old. Any damage dealt to break objects in this area is doubled.

* When a creature within one mile of the living trap’s lair rolls a natural 1 on a spell attack, roll on the Wild Magic Surge table to create a magical effect (see chapter 3 of the Player’s Handbook).

If the living trap dies, the last two effects fade over the course of 3d10 days.
