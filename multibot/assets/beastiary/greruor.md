"Greruor (Frog Demon)";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "11 (7,200 XP)"
_languages_: "Abyssal, Common, telepathy 120 ft."
_skills_: "Intimidation +7, Perception +6"
_senses_: "truesight 120 ft., passive Perception 16"
_saving_throws_: "Dex +7, Con +9, Cha +7"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "136 (13d10 + 65)"
_armor class_: "17 (natural armor)"
_stats_: | 23 (+6) | 17 (+3) | 21 (+5) | 14 (+2) | 14 (+2) | 16 (+3) |

___Innate Spellcasting.___ The demon’s spellcasting ability is Charisma
(spell save DC 15, +7 to hit with spell attacks). The demon can innately
cast the following spells, requiring no material components:

* At will: _darkness, detect evil and good, shatter_

* 3/day each: _confusion, hold person_

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

**Actions**

___Multiattack.___ The demon makes three attacks: one with its bite and two
with its ranseur. It can attack with its tongue instead of making a bite attack.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 17 (2d10 + 6) piercing damage.

___Ranseur.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 20 (4d6 + 6) piercing damage.

___Tongue.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) slashing damage, and the target is grappled (escape DC 16).
Until this grapple ends, the target is restrained, and the demon cannot
attack with its tongue.

___Combustible Spittle (Recharge 5–6).___ The demon spits a line of acid in a
30-foot line that is 5 feet wide. Each creature in that line must make a DC
17 Dexterity saving throw, taking 27 (6d8) acid damage on a failed save,
or half as much on a successful one. At the start of the demon’s next turn,
the acid ignites and any targets hit by it burst into flames, taking 10 (3d6)
fire damage per round until extinguished.
