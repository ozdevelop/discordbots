"Wolf";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_skills_: "Perception +3, Stealth +4"
_speed_: "40 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "13 (natural armor)"
_stats_: | 12 (+1) | 15 (+2) | 12 (+1) | 3 (-4) | 12 (+1) | 6 (-2) |

___Keen Hearing and Smell.___ The wolf has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Pack Tactics.___ The wolf has advantage on an attack roll against a creature if at least one of the wolf's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) piercing damage. If the target is a creature, it must succeed on a DC 11 Strength saving throw or be knocked prone.