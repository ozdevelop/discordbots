"Derro Savant (A)";;;_size_: Small humanoid (derro)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Stealth +4"
_speed_: "30 ft."
_hit points_: "49 (11d6+11)"
_armor class_: "13 (leather armor)"
_stats_: | 9 (-1) | 14 (+2) | 12 (+1) | 11 (0) | 5 (-3) | 14 (+2) |

___Insanity.___ The derro has advantage on saving throws against being charmed or frightened.

___Magic Resistance.___ The derro has advantage on saving throws against spells and other magical effects.

___Sunlight Sensitivity.___ While in sunlight, the derro has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Spellcasting.___ The derro is a 5th-level spellcaster. It's spellcasting ability is Charisma (save DC 12, +4 to hit with spell attacks). The derro knows the following sorcerer spells:

* Cantrips (at will): _acid splash, light, mage hand, message, ray of frost_

* 1st level (4 slots): _burning hands, chromatic orb, sleep_

* 2nd level (3 slots): _invisibility, spider climb_

* 3rd level (2 slots): _blink, lightning bolt_

**Actions**

___Hooked Shortspear.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 1 (1d4-1) piercing damage. If the target is a creature, the derro can choose to deal no damage and try to trip the target instead, in which case the target must succeed on a DC 9 Strength saving throw or fall prone.

___Light Repeating Crossbow.___ Ranged Weapon Attack: +4 to hit, range 40/160 ft., one target. Hit: 6 (1d8+2) piercing damage.
