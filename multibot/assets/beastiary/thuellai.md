"Thuellai";;;_size_: Huge elemental
_alignment_: chaotic neutral
_challenge_: "10 (5900 XP)"
_languages_: "Common, Dwarvish, Primordial"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Int +4, Wis +4, Cha +6"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "poison"
_damage_resistances_: "lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "0 ft., fly 100 ft. (hover)"
_hit points_: "149 (13d12 + 65)"
_armor class_: "17"
_stats_: | 22 (+6) | 24 (+7) | 20 (+5) | 10 (+0) | 11 (+0) | 14 (+2) |

___Air Mastery.___ Airborne creatures have disadvantage on attack rolls against the thuellai.

___Snow Vision.___ The thuellai see perfectly well in snowy conditions. It does not suffer Wisdom (Perception) penalties from snow, whiteout, or snow blindness.

**Actions**

___Multiattack.___ The thuellai makes two claw attacks.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) slashing damage plus 26 (4d12) cold damage. If the target is wearing metal armor, it must make a successful DC 17 Constitution saving throw or gain one level of exhaustion.

___Freezing Breath (Recharge 5-6).___ The thuellai exhales an icy blast in a 40-foot cone. Each target in the area takes 39 (6d12) cold damage, or half damage with a successful DC 17 Constitution saving throw.

___Algid Aura.___ All creatures within 10 feet of a thuellai take 7 (2d6) cold damage at the beginning of the thuellai's turn. Spells or magical effects that protect against cold are affected as if by a dispel magic spell (the theullai's effective spellcasting bonus is +5) if a thuellai is within 20 feet of the target at the start of the theullai's turn, and nonmagical flames within 20 feet of the thuellai are extinguished at the start of its turn.

___Howl of the Maddening Wind (3/day).___ A thuellai's howl can cause creatures to temporarily lose their minds and even to attack themselves or their companions. Each target within 100 feet of the theullai and able to hear the howl must make a successful DC 14 Wisdom saving throw or roll 1d8 and consult the table below at the start of its next turn. An affected creature repeats the saving throw at the end of each of its turns; a success ends the effect on itself, but a failure means it must roll again on the table below at the start of its next turn.

1 - Act normally

2-4 - Do nothing but babble incoherently

5-6 - Do 1d8 damage + Str modifier to self with item in hand

7-8 - Attack nearest target; select randomly if more than one

___Blizzard (1/Day).___ The thuellai creates an icy blizzard in the area around it. A 50-foot radius sphere surrounding the theullai fills with icy fog, whirling snow, and driving ice crystals. Vision is lightly obscured, and creatures have disadvantage on Wisdom (Perception) checks that rely on vision or hearing. The ground in the affected area becomes difficult terrain. The effect lasts for 10 minutes and moves with the theullai.

