"Zmey";;;_size_: Huge dragon
_alignment_: chaotic evil
_challenge_: "14 (11500 XP)"
_languages_: "Common, Draconic, Elvish, Sylvan"
_skills_: "Perception +8"
_senses_: "blindsight 60 ft., darkvision 90 ft., passive Perception 18"
_saving_throws_: "Con +9, Wis +8, Cha +6"
_damage_resistances_: "cold, fire"
_condition_immunities_: "paralyzed, unconscious"
_speed_: "30 ft., fly 50 ft., swim 30 ft."
_hit points_: "189 (18d12 + 72)"
_armor class_: "18 (natural armor)"
_stats_: | 22 (+6) | 13 (+1) | 19 (+4) | 16 (+3) | 16 (+3) | 12 (+1) |

___Amphibious.___ The zmey can breathe air and water.

___Lake Leap.___ A zmey spends much of its time lurking in lakes and ponds. When submerged in a natural pool of standing water, it can transport itself as a bonus action to a similar body of water within 5,000 feet. Rapidly flowing water doesn't serve for this ability, but the zmey can leap to or from a river or stream where the water is calm and slow-moving.

___Legendary Resistance (1/Day).___ If the zmey fails a saving throw, it can count it as a success instead.

___Multiheaded.___ The zmey normally has three heads. While it has more than one head, the zmey has advantage on saving throws against being blinded, charmed, deafened, frightened, and stunned. If the zmey takes 40 or more damage in a single turn (and the damage isn't poison or psychic), one of its heads is severed. If all three of its heads are severed, the zmey dies.

___Regeneration.___ The zmey regains 15 hit points at the start of its turn. If the zmey takes acid or fire damage, this trait doesn't function at the start of the zmey's next turn. Regeneration stops functioning when all heads are severed. It takes 24 hours for a zmey to regrow a functional head.

___Spawn Headling.___ The severed head of a zmey grows into a zmey headling 2d6 rounds after being separated from the body. Smearing at least a pound of salt on the severed head's stump prevents this transformation.

**Actions**

___Multiattack.___ The zmey makes one bite attack per head and one claws attack.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 19 (2d12 + 6) piercing damage.

___Claws.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 19 (2d12 + 6) slashing damage.

___Tail.___ Melee Weapon Attack: +11 to hit, reach 20 ft., one target. Hit: 15 (2d8 + 6) bludgeoning damage.

___Fire Breath (Recharge 5-6).___ The zmey breathes up to three 60-foot cones of fire, one from each of its heads. Creatures in a cone take 16 (3d10) fire damage, or half damage with a successful DC 16 Dexterity saving throw. If cones overlap, their damage adds together but each target makes only one saving throw. A zmey can choose whether this attack harms plants or plant creatures.

**Legendary** Actions

___The zmey can take 1 legendary action per head, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The zmey regains spent legendary actions at the start of its turn.

___Bite.___ The zmey makes a bite attack.

___Tail Attack.___ The zmey makes a tail attack.

___Trample.___ The zmey moves up to half its land speed. It can enter enemy-occupied spaces but can't end its move there. Creatures in spaces the zmey enters must make successful DC 14 Dexterity saving throws or take 10 (1d8 + 6) bludgeoning damage and fall prone.

