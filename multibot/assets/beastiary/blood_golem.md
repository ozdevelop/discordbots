"Blood Golem";;;_size_: Large humanoid
_alignment_: neutral
_challenge_: "8 (3,900 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft."
_hit points_: "102 (12d10 + 36)"
_armor class_: "17 (natural armor)"
_stats_: | 14 (+2) | 19 (+4) | 16 (+3) | 2 (-4) | 14 (+2) | 1 (-5) |

___Amorphous.___ The blood golem can move through a space as narrow as
1 inch wide without squeezing.

___Berserk.___ When the blood golem starts its turn with 40 hit points or
fewer, roll a d6. On a 6, the golem goes berserk. On each of its turns while
berserk, the golem attacks the nearest creature it can see. If no creature
is near enough to move to and attack, the golem attacks an object, with
preference for an object smaller than itself. Once the blood golem goes
berserk, it continues to do so until it is destroyed or regains all its hit
points.

The golem’s creator, if within 60 feet of the berserk golem, can try
to calm it by speaking firmly and persuasively. The golem must be able
to hear its creator, who must take an action to make a DC 15 Charisma
(Persuasion) check. If the check succeeds, the golem ceases being berserk.
If it takes damage while still at 40 hit points or fewer, the golem might go
berserk again.

___Blood Ooze.___ The blood golem takes up its entire space. Other creatures
can enter the space, but a creature that does so is subjected to the blood
golem’s Engulf and has disadvantage on the saving throw.

Creatures inside the blood golem can’t be seen and have total cover.
A creature within 5 feet of the blood golem can take an action to pull
a creature or object out of the cube. Doing so requires a successful DC
12 Strength check, and the creature making the attempt takes 10 (3d6)
necrotic damage.

The blood golem can hold only one Large creature or up to four Medium
or smaller creatures inside it at a time.

___Blood Sense.___ The blood golem can magically sense the presence of
blood in living creatures up to 1 mile away. It knows the general direction
they’re in but not their exact locations.

___Blood Splatter.___ Any time the golem is hit in combat, a gout of blood
erupts from its body. All creatures within 10 feet of the golem must
succeed on a DC 15 Dexterity saving throw or be blinded until the end of
the creature’s next turn.

___Immutable Form.___ The golem is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The golem’s weapon attacks are magical.

___Split.___ When a blood golem reaches its maximum hit points for its Hit
Dice, it splits into two identical golems. Each golem has hit points equal
to half the maximum hit points of the original golem. New golems are one
size smaller than the original golem.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit:
13 (2d8 + 4) bludgeoning damage, and the target must make a DC 15
Constitution saving throw. On a failed save, the target takes 22 (4d8 +
4) bludgeoning damage plus 14 (3d6 + 4) necrotic damage; its hit point
maximum is reduced by the amount equal to the necrotic damage, and the
blood golem regains hit points equal to that amount. The reduction in the
target’s hit point maximum lasts until the target finishes a long rest. The
target dies if this reduces its hit point maximum to 0.

___Engulf.___ The blood golem moves up to its speed. While doing so, it can
enter Large or smaller creatures’ spaces. Whenever the blood golem enters
a creature’s space, the creature must make a DC 12 Dexterity saving throw.

On a successful save, the creature can choose to be pushed 5 feet back
or to the side of the blood golem. A creature that chooses not to be pushed
suffers the consequences of a failed saving throw.

On a failed save, the blood golem enters the creature’s space, and the
creature takes 10 (3d6) necrotic damage and is engulfed. The engulfed
creature can’t breathe, is restrained, and takes 21 (6d6) necrotic damage at
the start of each of the blood golem’s turns. When the blood golem moves,
the engulfed creature moves with it.

An engulfed creature can try to escape by taking an action to make a
DC 12 Strength check. On a success, the creature escapes and enters a
space of its choice within 5 feet of the blood golem.
