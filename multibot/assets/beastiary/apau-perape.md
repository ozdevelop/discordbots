"Apau Perape";;;_size_: Large fiend
_alignment_: chaotic evil
_challenge_: "6 (2300 XP)"
_languages_: "Ape, Infernal, telepathy 120 ft."
_skills_: "Intimidation +5, Perception +4, Stealth +7"
_senses_: "darkvision 120 ft., passive Perception 14"
_saving_throws_: "Dex +7, Con +7, Wis +4"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "poison"
_damage_resistances_: "fire, lightning, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "frightened, poisoned"
_speed_: "30 ft., climb 30 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "16 (natural armor)"
_stats_: | 21 (+5) | 18 (+4) | 19 (+4) | 10 (+0) | 12 (+1) | 15 (+2) |

___Diseased Ichor.___ Every time the apau perape takes piercing or slashing damage, a spray of caustic blood spurts from the wound toward the attacker. This spray forms a line 10 feet long and 5 feet wide. The first creature in the line must make a successful DC 15 Constitution saving throw against disease or be infected by Mechuiti's Ichor disease. The creature is poisoned until the disease is cured. Every 24 hours that elapse, the target must repeat the Constitution saving throw or reduce its hit point maximum by 5 (2d4). The disease is cured on a success. The target dies if the disease reduces its hit point maximum to 0. This reduction to the target's hit point maximum lasts until the disease is cured.

___Innate Spellcasting.___ The apau perape is an innate spellcaster. Its spellcasting ability is Charisma (spell save DC 13). The apau perape can innately cast the following spells, requiring no material components:

* 1/day each: _fear, wall of fire_

___Magic Resistance.___ The apau perape has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The apau perape makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 12 (2d6 + 5) piercing damage.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) slashing damage.

___Variant: Demon Summoning.___ Some apau perapes have an action option that allows them to summon other demons.

Summon Demon (1/Day): The apau perape chooses what to summon and attempts a magical summoning

The apau perape has a 50 percent chance of summoning one apau perape or one giant ape.

