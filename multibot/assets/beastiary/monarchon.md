"Monarchon";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Elven, Sylvan"
_skills_: "History +4, Insight +7, Medicine +7, Nature +4, Perception + 6, Religion +4"
_senses_: "passive Perception 20"
_saving_throws_: "Wis +7, Cha +5"
_speed_: "30 ft., fly 30 ft. (hover)"
_hit points_: "110 (13d10 + 39)"
_armor class_: "17 (natural armor)"
_stats_: | 11 (+0) | 15 (+2) | 16 (+3) | 12 (+1) | 19 (+4) | 14 (+2) |

___Fey Ancestry.___ Magic cannot put the Monarchon
to sleep.

___Magic Resistance.___ The Monarchon has advantage
on saving throws against spells and other
magical effects.

___Command Fey.___ As a member of the Court of
Arcadia, the Monarchon can cast _dominate
monster_ (DC 15) at will on any fey creature or elf.

___Innate Spellcasting.___ The Monarchon’s innate
spellcasting ability is Wisdom (spell save DC 15).
The Monarchon can innately cast the following
spells, requiring no material components:

* At will: _enthrall, sleep_

___Spellcasting.___ The Monarchon is a 7th-level spellcaster.
Her spellcasting ability is Wisdom (spell
save DC 15, +7 to hit with spell attacks). The
Monarchon has the following cleric spells prepared:

* Cantrips (at will): _guidance, sacred flame_

* 1st level (4 slots): _bane, bless, command_

* 2nd level (3 slots): _hold person, zone of truth_

* 3rd level (3 slots): _dispel magic, revivify_

* 4th level (1 slot): _banishment, death ward_

**Actions**

___Withering Touch.___ Melee Spell Attack: +7 to
hit, reach 5 ft., one creature. Hit: 15 (2d10 + 4)
necrotic damage.

___Hypnotic Display.___ The Monarchon spreads
her wings and their false eyes pulsate with a
rainbow of colors, targeting one creature she
can see within 30 feet. If the target can see the
Monarchon, the target must succeed on a DC
15 Wisdom saving throw against this magic or
be charmed until the end of the Monarchon’s
next turn. The charmed target is stunned. If the
target’s saving throw is successful, the target is
immune to the Monarchon’s gaze for 24 hours.
