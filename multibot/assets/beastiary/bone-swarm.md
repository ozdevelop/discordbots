"Bone Swarm";;;_size_: Large swarm
_alignment_: chaotic evil
_challenge_: "10 (5900 XP)"
_languages_: "Common, Void Speech"
_skills_: "Acrobatics +8, Perception +6, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +8, Wis +6, Cha +9"
_damage_vulnerabilities_: "bludgeoning"
_damage_immunities_: "poison"
_damage_resistances_: "piercing and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned, prone, restrained, stunned"
_speed_: "20 ft., fly 60 ft."
_hit points_: "198 (36d10)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 18 (+4) | 10 (+0) | 9 (-1) | 15 (+2) | 20 (+5) |

___Strength of Bone.___ A bone swarm can choose to deal bludgeoning, piercing, or slashing damage, and adds 1.5x its Strength bonus on swarm damage rolls as bits and pieces of broken skeletons claw, bite, stab, and slam at the victim.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a human skull. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Multiattack.___ The bone swarm can attack every hostile creature in its space with swirling bones.

___Swirling Bones.___ Melee Weapon Attack: +10 to hit, reach 0 ft., one creature in the swarm's space. Hit: 31 (5d8 + 9) bludgeoning, piercing, or slashing damage (includes Strength of Bone special ability).

___Death's Embrace (Recharge 5-6).___ Melee Weapon Attack: +10 to hit, reach 0 ft., one creature in the swarm's space. Hit: the target is grappled (escape DC 16) and enveloped within the swarm's bones. The swarm can force the creature to move at its normal speed wherever the bone swarm wishes. Any non-area attack against the bone swarm has a 50 percent chance of hitting a creature grappled in Death's Embrace instead.

