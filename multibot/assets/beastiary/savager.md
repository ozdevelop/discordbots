"Savager";;;_size_: Large beast
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +5, Con +9"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, frightened"
_speed_: "40 ft., climb 20 ft."
_hit points_: "115 (1d10 + 60)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 14 (+2) | 22 (+6) | 2 (-4) | 10 (+0) | 13 (+1) |

___Mighty Swing.___ When a savager attacks without moving during its turn, it makes its claw attack with advantage.

___Quills.___ A creature takes 4 (1d8) piercing damage at the start of its turn while it is grappling a savager.

**Actions**

___Multiattack.___ The savager makes one bite attack and one claw attack.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 19 (2d12 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 38 (5d12 + 6) slashing damage.

