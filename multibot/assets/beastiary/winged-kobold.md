"Winged Kobold";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft."
_speed_: "30 ft., fly 30 ft."
_hit points_: "7 (3d6-3)"
_armor class_: "13"
_stats_: | 7 (-2) | 16 (+3) | 9 (-1) | 8 (-1) | 7 (-2) | 8 (-1) |

___Sunlight Sensitivity.___ While in sunlight, the kobold has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Pack Tactics.___ The kobold has advantage on an attack roll against a creature if at least one of the kobold's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Dagger.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Dropped Rock.___ Ranged Weapon Attack: +5 to hit, one target directly below the kobold. Hit: 6 (1d6 + 3) bludgeoning damage.