"Qorgeth, Demon Lord Of The Devouring Worm";;;_size_: Gargantuan fiend
_alignment_: chaotic evil
_challenge_: "23 (50000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Perception +11"
_senses_: "blindsight 120 ft., tremorsense 120 ft., passive Perception 21"
_saving_throws_: "Dex +5, Con +15, Wis +11, Cha +11"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "blinded, charmed, exhaustion, frightened, poisoned"
_speed_: "50 ft., burrow 50 ft., climb 30 ft."
_hit points_: "370 (20d20 + 160)"
_armor class_: "21 (natural armor)"
_stats_: | 29 (+9) | 6 (-2) | 26 (+8) | 9 (-1) | 19 (+4) | 18 (+4) |

___Innate Spellcasting.___ Qorgeth's innate spellcasting ability is Charisma (spell save DC 19). It can innately cast the following spells, requiring no material or somatic components.

At will: detect magic, black tentacles

3/day each: dispel magic, fear, insect plague (biting worms)

1/day each: earthquake, teleport

___Legendary Resistance (3/Day).___ If Qorgeth fails a saving throw, it can choose to succeed instead.

___Tunneler.___ Qorgeth can burrow through solid stone at its full speed. It leaves a 15-foot-diameter tunnel in its wake.

___Magic Resistance.___ Qorgeth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Qorgeth's weapon attacks are magical.

___Qorgeth's Lair.___ On initiative count 20 (losing initiative ties), Qorgeth takes a lair action to cause one of the following effects; Qorgeth can't use the same effect two rounds in a row:

- Until initiative count 20 on the following round, Qorgeth twists space through the tunnels of its lair. Any creature other than a demon that tries to move must succeed on a DC 15 Charisma saving throw or move half its speed in a random direction before getting its bearings; it can then finish moving as it wants.

- A section of ceiling in the lair collapses, raining debris onto a 20-foot-radius area. Each creature in the area must make a successful DC 15 Dexterity saving throw or take 18 (4d8) bludgeoning damage and be restrained until the end of its next turn.

- Thick tangles of demonic worms erupt in the space of up to three creatures Qorgeth can see within 60 feet. Each targeted creature is attacked once by the worms (Melee Weapon Attack: +7 to hit, reach 0 ft., one target; Hit: 14 (4d6) piercing).

___Regional Effects.___ The region containing Qorgeth's lair is warped by the demon lord's magic, which creates one or more of the following effects:

- Tunnels within 5 miles of the lair attract all manner of worms and vermin, including purple worms. These creatures are ravenous and violent.

- Within 1 mile of the lair, food rots and spontaneously erupts with maggots. One day worth of food carried by creatures spoils every 24 hours it remains in the area. It is impossible to forage for food in this area.

- Dead bodies within 1 mile of the lair decay quickly. Any corpse is reduced to bones in 24 hours. Magic that prevents decay staves off this decomposition normally. Anointing the body with holy water prevents decomposition for one day but no longer.

If Qorgeth dies, conditions in the area surrounding the lair return to normal over the course of 1d10 days.

**Actions**

___Multiattack.___ Qorgeth makes one bite attack, two crush attacks, and one stinger attack.

___Bite.___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 22 (2d12 + 9) piercing damage. A target creature of Large size or smaller must succeed on a DC 24 Dexterity saving throw or be swallowed by Qorgeth. A swallowed creature is blinded and restrained, and takes 16 (3d10) necrotic damage at the start of each of Qorgeth's turns. Qorgeth can have any number of creatures swallowed at one time. If Qorgeth takes 50 damage or more in a single turn from a creature it has swallowed, it must succeed on a DC 20 Constitution saving throw or regurgitate all swallowed creatures, who land prone within 10 feet of Qorgeth. If Qorgeth dies, a swallowed creature is no longer restrained, and can escape the corpse by spending 30 feet of movement, exiting prone.

___Crush.___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 20 (2d10 + 9) bludgeoning damage. A target creature is also grappled and restrained (escape DC 19) until Qorgeth moves. Qorgeth can grapple up to two creatures at once; at least one of Qorgeth's crush attacks must be directed against each creature it has grappled.

___Stinger.___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 23 (4d6 + 9) piercing damage, and the target takes 33 (6d10) poison damage and is poisoned for 1 hour; a successful DC 23 Constitution saving throw reduces poison damage by half and negates the poisoned condition. A creature that fails the save by 10 or more is also paralyzed for as long as the poisoned condition lasts.

**Legendary** Actions

___Qorgeth can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. Qorgeth regains spent legendary actions at the start of its turn.

___Shriek.___ All creatures within a 60-foot cone that can hear Qorgeth must succeed on a DC 19 Wisdom saving throw or be frightened for 1 minute. A frightened creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Death Roll (2 actions).___ Qorgeth moves half its speed and makes one Crush attack. Any structures or objects in spaces Qorgeth moves through take double Crush damage automatically.

___Devour (3 actions).___ Qorgeth makes one bite attack.

