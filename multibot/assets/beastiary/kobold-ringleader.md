"Kobold Ringleader";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1 (100 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft., Passive Perception 11"
_skills_: "Insight,+3 Intimidation +3, Investigation +4, Persuasion +2"
_speed_: "30 ft."
_hit points_: "17 (5d6)"
_armor class_: "13 (leather armor)"
_stats_: | 7 (-2) | 15 (+2) | 10 (+0) | 14 (+2) | 12 (+1) | 10 (+0) |

___Bossy Pants.** The kobold targets one ally it can see
within 30 feet of it. If the target can see and hear
the kobold, the target can make one weapon attack
as a reaction and gains advantage on the attack roll.

___Chatty Ring.___ As a bonus action the kobold ringleader
can activate this magic ring and one of its
recorded messages is played. The ring can store up
to 11 messages. The messages replay in the order
they were recorded. Any new recordings replace the
oldest recording stored in the ring.

___Sunlight Sensitivity.___ While in sunlight, the kobold
has disadvantage on attack rolls, as well as on Wisdom
(Perception) checks that rely on sight.

___Pack Tactics.___ The kobold has advantage on an
attack roll against a creature if at least one of the
kobold’s allies is within 5 feet of the creature and
the ally isn’t incapacitated.

**Actions**

___Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 4 (1d4 + 2) piercing damage.

___Sling.___ Ranged Weapon Attack: +4 to hit, range
30/120 ft., one target. Hit: 4 (1d4 + 2) bludgeoning
damage.
