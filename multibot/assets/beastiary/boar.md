"Boar";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_speed_: "40 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "11 (natural armor)"
_stats_: | 13 (+1) | 11 (0) | 12 (+1) | 2 (-4) | 9 (-1) | 5 (-3) |

___Charge.___ If the boar moves at least 20 ft. straight toward a target and then hits it with a tusk attack on the same turn, the target takes an extra 3 (1d6) slashing damage. If the target is a creature, it must succeed on a DC 11 Strength saving throw or be knocked prone.

___Relentless (Recharges after a Short or Long Rest).___ If the boar takes 7 damage or less that would reduce it to 0 hit points, it is reduced to 1 hit point instead.

**Actions**

___Tusk.___ Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) slashing damage.