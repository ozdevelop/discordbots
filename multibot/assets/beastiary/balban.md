"Balban (Brute Demon)";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "Abyssal"
_skills_: "Intimidation +6, Perception +6"
_senses_: "darkvision 120 ft., passive Perception 16"
_saving_throws_: "Con +10, Wis +6, Cha +6"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "126 (11d10 + 66)"
_armor class_: "17 (natural armor)"
_stats_: | 25 (+7) | 14 (+2) | 22 (+6) | 6 (-2) | 14 (+2) | 14 (+2) |

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Innate Spellcasting.___ The demon’s spellcasting ability is Wisdom (spell
save DC 14, +6 to hit with spell attacks). The demon can innately cast the
following spells, requiring no material components: 

* At will: _fear, darkness, dispel magic, teleport, see invisibility_

___Charge.___ If the demon moves at least 20 feet straight toward a creature
and then hits it with a bite attack on the same turn, that target must
succeed on a DC 17 Strength saving throw or be knocked prone. If the
target is prone, the demon can make one slam attack against it as a
bonus action.

**Actions**

___Multiattack.___ The demon can use its smash, make one bite attack
and two claw attacks.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit:
16 (2d8 + 7) piercing damage.

___Slam.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit:
17 (3d6 + 7) bludgeoning damage. If the target is a Medium or smaller
creature, it is grappled (escape DC 17). Until this grapple ends, the target
is restrained and the demon cannot use this attack against another target.

___Smash.___ One Medium or smaller creature grappled by the demon is
smashed into nearby solid objects or a solid surface. The target takes 25
(4d8 + 7) bludgeoning damage and must succeed on a DC 16 Constitution
saving throw or be stunned until the end of its next turn.
