"Ghoul, Imperial";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Darakhul, Undercommon"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft., burrow 15 ft."
_hit points_: "93 (17d8 + 17)"
_armor class_: "16 (breastplate)"
_stats_: | 16 (+3) | 14 (+2) | 12 (+1) | 13 (+1) | 14 (+2) | 14 (+2) |

___Turning Defiance.___ The iron ghoul and any ghouls within 30 feet of it have advantage on saving throws against effects that turn undead.

**Actions**

___Multiattack.___ The imperial ghoul makes one bite attack and one claws attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) piercing damage, and if the target creature is humanoid it must succeed on a DC 11 Constitution saving throw or contract darakhul fever.

___Claws.___ Melee Weapon Attack: +5 to hit, reach, one target. Hit: 17 (4d6 + 3) slashing damage. If the target is a creature other than an elf or undead, it must succeed on a DC 11 Constitution saving throw or be paralyzed for 1 minute. The target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Light Crossbow.___ Ranged Weapon Attack: +4 to hit, range 80/320, one target. Hit: 8 (1d8 + 2) piercing damage.

