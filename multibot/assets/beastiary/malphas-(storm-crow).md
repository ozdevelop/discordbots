"Malphas (Storm Crow)";;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Giant, Ravenfolk, Sylvan"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +7, Con +6, Wis +4, Cha +5"
_speed_: "40 ft., fly 30 ft."
_hit points_: "120 (16d8 + 48)"
_armor class_: "16 (studded leather)"
_stats_: | 19 (+4) | 19 (+4) | 16 (+3) | 14 (+2) | 13 (+1) | 14 (+2) |

___Innate Spellcasting.___ The storm crow's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no material components:

At will: magic missile

1/day: haste

___Sunlight Sensitivity.___ While in sunlight, the malphas has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Night Terror (1/round).___ As a bonus action immediately after making a successful melee attack, a malphas storm crow can cast magic missile through his or her sword at the same target.

___Swordtrained.___ Malphas are trained from youth in combat. They are proficient with all martial melee and ranged weapons.

**Actions**

___Multiattack.___ The malphas makes three longsword attacks.

___Longsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage.

**Reactions**

___Shadow Call.___ A malphas can cast magic missile as a reaction when it is hit by a ranged weapon attack.

