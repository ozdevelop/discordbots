"Giant Wasp (NEB)";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_speed_: "15 ft., fly 30 ft."
_hit points_: "14 (4d6)"
_armor class_: "12"
_stats_: | 6 (-2) | 15 (+2) | 10 (+0) | 3 (-4) | 10 (+0) | 4 (-3) |

___Hivemind.___ All wasps within 10 miles of their queen
are in constant communication via a telepathic
bond.

___Hive Tactics.___ The wasp has advantage on an attack
roll against a creature if at least one of the wasp’s
allies is within 5 feet of the creature and the ally
isn’t incapacitated.

**Actions**

___Sting.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 1 piercing damage plus 4 (1d8)
poison damage.
