"Guardian Naga";;;_size_: Large monstrosity
_alignment_: lawful good
_challenge_: "10 (5,900 XP)"
_languages_: "Celestial, Common"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Dex +8, Con +7, Int +7, Wis +8, Cha +8"
_speed_: "40 ft."
_hit points_: "127 (15d10+45)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "charmed, poisoned"
_stats_: | 19 (+4) | 18 (+4) | 16 (+3) | 16 (+3) | 19 (+4) | 18 (+4) |

___Rejuvenation.___ If it dies, the naga returns to life in 1d6 days and regains all its hit points. Only a wish spell can prevent this trait from functioning.

___Spellcasting.___ The naga is an 11th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 16, +8 to hit with spell attacks), and it needs only verbal components to cast its spells. It has the following cleric spells prepared:

* Cantrips (at will): _mending, sacred flame, thaumaturgy_

* 1st level (4 slots): _command, cure wounds, shield of faith_

* 2nd level (3 slots): _calm emotions, hold person_

* 3rd level (3 slots): _bestow curse, clairvoyance_

* 4th level (3 slots): _banishment, freedom of movement_

* 5th level (2 slots): _flame strike, geas_

* 6th level (1 slot): _true seeing_

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one creature. Hit: 8 (1d8 + 4) piercing damage, and the target must make a DC 15 Constitution saving throw, taking 45 (10d8) poison damage on a failed save, or half as much damage on a successful one.

___Spit Poison.___ Ranged Weapon Attack: +8 to hit, range 15/30 ft., one creature. Hit: The target must make a DC 15 Constitution saving throw, taking 45 (10d8) poison damage on a failed save, or half as much damage on a successful one.
