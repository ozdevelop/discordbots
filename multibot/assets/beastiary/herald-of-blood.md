"Herald Of Blood";;;_size_: Huge fiend
_alignment_: neutral evil
_challenge_: "12 (8400 XP)"
_languages_: "Common, Draconic, Infernal, Void Speech"
_skills_: "Arcana +6, Perception +7"
_senses_: "darkvision 240 ft., passive Perception 17"
_saving_throws_: "Str +10, Con +9, Wis +7"
_damage_immunities_: "fire, poison"
_damage_resistances_: "piercing, lightning"
_condition_immunities_: "exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., swim 30 ft, fly 50 ft."
_hit points_: "115 (10d12 + 50)"
_armor class_: "15 (natural armor)"
_stats_: | 22 (+6) | 12 (+1) | 20 (+5) | 14 (+2) | 17 (+3) | 16 (+3) |

___Blood Armor.___ The herald of blood takes no damage from the first attack against it each round and ignores any nondamaging effects of the attack.

___Gift of Blood.___ As an action, the herald of blood can transform any fey, human, or goblin into a red hag, if the target willingly accepts this transformation.

___Grant Blood Rage.___ As a bonus action, the herald of blood can grant a single living creature blood rage, giving it advantage on attacks for 3 rounds. At the end of this time, the target gains 1 level of exhaustion and suffers 13 (2d12) necrotic damage from blood loss.

___Humanoid Form.___ A herald of blood can assume a humanoid form at will as a bonus action, and dismiss this form at will.

___Melting Touch.___ When a herald of blood scores a critical hit or starts its turn with a foe grappled, it can dissolve one metal or wood item of its choosing in that foe's possession. A mundane item is destroyed automatically; a magical item survives if its owner makes a successful DC 17 Dexterity saving throw.

**Actions**

___Engulfing Protoplasm.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 19 (2d12 + 6) slashing damage and the target must make a successful DC 17 Dexterity saving throw or be grappled by the herald of blood (escape DC 16). While grappled this way, the creature takes 39 (6d12) acid damage at the start of each of the herald's turns. The herald can have any number of creatures grappled this way.

**Legendary** Actions

___A herald of blood can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. A herald of blood regains spent legendary actions at the start of its turn.

___Move (1 Action).___ The herald of blood moves up to half its speed.

___Call of Blood (2 Actions).___ Melee Weapon Attack. +10 to hit, reach 5 ft., all creatures in reach. Hit: 39 (6d12) necrotic damage and each target must make a successful DC 17 Constitution saving throw or gain 1 level of exhaustion.

___Majesty of Ragnarok (3 Actions).___ The herald of blood emits a terrifying burst of eldritch power. All creatures within 100 feet and in direct line of sight of the herald take 32 (5d12) necrotic damage, gain 2 levels of exhaustion, and are permanently blinded. Targets that make a successful DC 15 Charisma saving throw are not blinded and gain only 1 level of exhaustion.

