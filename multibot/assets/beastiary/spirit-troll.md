"Spirit Troll";;;_page_number_: 244
_size_: Large giant
_alignment_: chaotic evil
_challenge_: "11 (7200 XP)"
_languages_: "Giant"
_senses_: "darkvision 60 ft., passive Perception 13"
_skills_: "Perception +3"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_speed_: "30 ft."
_hit points_: "97  (15d10 + 15)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, prone, restrained, unconscious"
_damage_resistances_: "acid, cold, fire, lightning, thunder"
_stats_: | 1 (-4) | 17 (+3) | 13 (+1) | 8 (-1) | 9 (0) | 16 (+3) |

___Incorporeal Movement.___ The troll can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Regeneration.___ The troll regains 10 hit points at the start of each of its turns. If the troll takes psychic or force damage, this trait doesn't function at the start of the troll's next turn. The troll dies only if it starts its turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack___ The troll makes three attacks: one with its bite and two with its claws.

___Bite___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 19 (3d10 + 3) psychic damage, and the target must succeed on a DC 15 Wisdom saving throw or be stunned for 1 minute. The stunned target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Claws___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 14 (2d10 + 3) psychic damage.