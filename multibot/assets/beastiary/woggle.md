"Woggle";;;_size_: Medium monstrosity
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "passive Perception 14"
_speed_: "30 ft."
_hit points_: "18 (4d8)"
_armor class_: "11"
_stats_: | 13 (+1) | 12 (+1) | 10 (+0) | 2 (-4) | 9 (-1) | 13 (+1) |

___Woggle Woggle.___ The woggle babbles in its bizarre
woggle-speak while it can see any creature and isn’t
incapacitated. Each creature that starts its turn within
20 feet of the woggle and can hear it must succeed
on a DC 11 Intelligence saving throw or be cursed
with woggle-speak. The cursed target loses all of its
languages and can only speak and write the word
"woggle." While cursed, casting a spell that includes
a verbal component is impossible. Woggle-speak is
gibberish and can’t be understood with a
comprehend languages spell or similar magic, nor
understood by others with the same curse.
Telepathy, mind-reading, and other spells used for
non-verbal communication similarly don’t work as
the cursed creature can only create images of the
word “woggle” or a physical woggle in its mind. The
curse lasts until removed by the remove curse spell
or other magic. Creatures immune to being charmed
automatically succeed on the saving throw. A
creature that succeeds on its saving throw is
immune to this effect for 24 hours.

___Two Heads.___ The woggle has advantage on Wisdom
(Perception) checks and on saving throws against
being blinded, charmed, deafened, frightened,
stunned, and knocked unconscious.

**Actions**

___Claw.___ Melee Weapon Attack: +3 to hit, reach 5 ft.,
one target. Hit: 4 (1d6 + 1) slashing damage.
