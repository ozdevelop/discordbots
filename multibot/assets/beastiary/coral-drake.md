"Coral Drake";;;_size_: Medium dragon
_alignment_: neutral evil
_challenge_: "7 (2900 XP)"
_languages_: "Draconic"
_skills_: "Acrobatics +6, Perception +4, Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 17"
_saving_throws_: "Dex +6"
_damage_resistances_: "cold"
_condition_immunities_: "paralyzed, poisoned, prone, unconscious"
_speed_: "30 ft., swim 60 ft."
_hit points_: "127 (15d8 + 60)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 1 (-5) | 18 (+4) | 10 (+0) | 13 (+1) | 10 (+0) |

___Camouflage.___ A coral drake's coloration and shape lend to its stealth, granting the creature advantage on all Stealth checks while it's underwater.

___Water Breathing.___ The coral drake can breathe only underwater.

**Actions**

___Multiattack.___ The coral drake makes one bite attack, one claw attack, and one stinger attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 22 (4d8 +4) slashing damage.

___Stinger.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) piercing damage, and the target must succeed on a DC 15 Constitution saving throw or take 7 (2d6) poison damage at the start of each of its turns for 4 rounds. The creature can repeat the saving throw at the end of its turns, ending the effect on itself on a success.

___Breath Weapon (Recharge 5-6).___ Coral drakes nurture their offspring in specialized throat sacs. They can pressurize these sacs to spew forth a 15-foot cone of spawn. Each target in this area takes 21 (6d6) piercing damage from thousands of tiny bites and is blinded for 1d4 rounds; a successful DC 15 Dexterity saving throw reduces the damage by half and negates the blindness.

