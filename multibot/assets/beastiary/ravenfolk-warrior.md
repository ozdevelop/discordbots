"Ravenfolk Warrior";;;_size_: Medium humanoid
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Feather Speech, Huginn"
_skills_: "Deception +2, Perception +5, Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 15"
_saving_throws_: "Dex+5, Wis +3, Cha +2"
_speed_: "30 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "15 (studded leather armor)"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 10 (+0) | 13 (+1) | 10 (+0) |

___Rune Weapons.___ Kept keen with runic magic, runespears and runestaves are two-handed weapons that count as magical, though they provide no bonus to attack. Their magic must be renewed each week by a doom croaker or by Odin's own hand.

___Mimicry.___ Ravenfolk warriors can mimic the voices of others with uncanny accuracy. They have advantage on Charisma (Deception) checks involving audible mimicry.

**Actions**

___Multiattack.___ A ravenfolk warrior makes two runespear attacks, or two longbow attacks, or one ghost wings attack and one runespear attack. It can substitute one peck attack for any other attack.

___Ghost Wings.___ The ravenfolk warrior furiously "beats" a set of phantasmal wings. Every creature within 5 feet of the ravenfolk must make a successful DC 13 Dexterity saving throw or be blinded until the start of the ravenfolk's next turn.

___Longbow.___ Ranged Weapon Attack. +5 to hit, range 150/600 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Peck.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Radiant Runespear.___ Melee Weapon Attack: +3 to hit, reach 10 ft., one target. Hit: 7 (1d12 + 1) piercing damage plus 2 (1d4) radiant damage.

___Rapier.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

**Reactions**

___Odin's Call.___ A ravenfolk warrior can disengage after an attack reduces it to 10 hp or less.

