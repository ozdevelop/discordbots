Abasheen;;;_size_: Large elemental
_alignment_: neutral
_challenge_: 6 (2,300 XP)
_languages_: Auran, Common, Infernal
_skills_: Perception +10
_senses_: darkvision 60 ft., passive Perception 20
_saving_throws_: Dex +5, Wis +7, Cha +6
_damage_immunities_: lightning, thunder
_speed_: 20 ft., fly 50 ft.
_hit points_: 102 (12d10 + 36)
_armor class_: 16 (natural armor)
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 20 (+5) | 18 (+4) | 17 (+3) |

___Air Mastery.___ Airborne creatures attack at disadvantage against abasheen.

___Elemental Demise.___ If the abasheen dies, its body disintegrates into
a warm breeze, leaving behind only the equipment the abasheen was
wearing or carrying.

___Grant Wish.___ The genie can grant one creature’s wish that is within 60
feet of it.

___Innate Spellcasting.___ The abasheen’s innate spellcasting ability is
Intelligence (spell save DC 15, +7 to hit with spell attacks). It can innately
cast the following spells, requiring no material components:

* At will: _command, charm person, plane shift _(willing targets to elemental planes, Astral Plane, or Material Plane only)

* 1/day: _geas_

**Actions**

___Multiattack.___ The abasheen makes two attacks with its falchion.

___Falchion.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit:
12 (2d8 + 3) slashing damage.

___Thunderclap (Recharge 5–6).___ The abasheen claps its hands together
loudly, and a wave of thundering force emanates out from it in a 30-foot
radius. Each creature in this area must make a DC 15 Constitution saving
throw or be pushed back 10 feet and take 18 (4d8) thunder damage on a
failed save, or half as much damage with no pushback on a successful one.
