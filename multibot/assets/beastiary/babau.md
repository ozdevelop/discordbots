"Babau";;;_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal"
_senses_: "darkvision 120 ft."
_skills_: "Perception +5, Stealth +5"
_damage_immunities_: "poison"
_speed_: "40 ft."
_hit points_: "82 (11d8+33)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 19 (+4) | 16 (+3) | 16 (+3) | 11 (0) | 12 (+1) | 13 (+1) |

___Innate Spellcasting.___ The babau's innate spellcasting ability is Wisdom (spell save DC 11). The babau can innately cast the following spells, requiring no material components:

* At will: _darkness, dispel magic, fear, heat metal, levitate_

**Actions**

___Multiattack.___ The babau makes two melee attacks. It can also use Weakening Gaze before or after making these attacks.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft, one target. Hit: 8 (1d8+4) slashing damage.

___Spear.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 it, one target. Hit: 7 (1d6+4) piercing damage, or 8 (1d8+4) piercing damage when used with two hands to make a melee attack.

___Weakening Gaze.___ The babau targets one creature that it can see within 20 feet of it. The target must make a DC 13 Constitution saving throw. On a failed save, the target deals only half damage with weapon attacks that use Strength for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
