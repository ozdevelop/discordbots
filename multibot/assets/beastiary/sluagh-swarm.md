"Sluagh Swarm";;;_size_: Medium swarm
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Sylvan"
_skills_: "Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "30 ft., fly 50 ft."
_hit points_: "54 (12d8)"
_armor class_: "13"
_stats_: | 6 (-2) | 16 (+3) | 11 (+0) | 6 (-2) | 13 (+1) | 10 (+0) |

___Lone Slaughs:___ An individual sluagh has a challenge rating of 1/8 (25 XP), 2 hit points, and does 3 (1d6) cold damage. They travel in swarms for a reason.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny fey. The swarm can't regain hit points or gain temporary hit points.

___Sunlight Weakness.___ While in sunlight, the sluagh swarm has disadvantage on attack rolls, ability checks, and saving throws.

**Actions**

___Chilling Touch.___ Melee Weapon Attack: +5 to hit, reach 0 ft., one creature in the swarm's space. Hit: 28 (8d6) cold damage or 14 (4d6) cold damage if the swarm has half of its hit points or fewer. The target must make a successful DC 13 Constitution saving throw or be unable to regain hit points. An affected creature repeats the saving throw at the end of its turns, ending the effect on itself with a successful save. The effect can also be ended with a greater restoration spell or comparable magic.

**Bonus** Actions

___Shadowy Stealth.___ While in dim light or darkness, the sluagh swarm can take the Hide action as a bonus action.
