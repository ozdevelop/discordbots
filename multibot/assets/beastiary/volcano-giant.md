"Volcano Giant";;;_size_: Huge giant
_alignment_: chaotic neutral
_challenge_: "13 (10,000 XP)"
_languages_: "Giant, Ignan"
_skills_: "Acrobatics +7, Intimidation +9, Nature +8, Perception +9"
_senses_: "passive Perception 19"
_saving_throws_: "Con +11"
_damage_immunities_: "fire"
_damage_vulnerabilities_: "cold"
_speed_: "40 ft."
_hit points_: "187 (15d12 + 90)"
_armor class_: "19 (natural armor)"
_stats_: | 29 (+9) | 15 (+2) | 22 (+6) | 16 (+3) | 18 (+4) | 18 (+4) |

___Heated Body.___ The volcano giant’s attacks deal an additional 7 (2d6) fire
damage (included in the attacks below).

**Actions**

___Multiattack.___ The volcano giant makes one one-handed spear attack and
one slam attack, or two slam attacks.

___Spear.___ Melee or Ranged Weapon Attack: +14 to hit, reach 10 ft. or range
40/120 ft., one target. Hit: 23 (4d6 + 9) piercing damage plus 7 (2d6) fire
damage, or 27 (4d8 + 9) piercing damage plus 7 (2d6) fire damage if used
with two hands to make a melee attack.

___Slam.___ Melee Weapon Attack: +14 to hit, reach 15 ft., one target.
Hit: 19 (3d6 + 9) bludgeoning damage plus 7 (2d6) fire damage.

___Rock.___ Ranged Weapon Attack: +14 to hit, range 60/240 ft., one
target. Hit: 27 (4d8 + 9) bludgeoning damage plus 7 (2d6) fire damage.

___Sulfuric Breath (Recharge 5–6).___ A volcano giant can exhale a cloud
of warm and sulfuric gas in a 30-foot cone. All creatures in the area must
succeed on a DC 19 Constitution saving throw or take 35 (10d6) acid
damage and be poisoned for 1 minute.
