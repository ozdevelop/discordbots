"Merfolk Hydromancer";;;_size_: Medium humanoid (merfolk)
_alignment_: neutral good
_challenge_: "9 (5,000 XP)"
_languages_: "Aquan, Common"
_skills_: "Arcana +8, Nature +8, Persuasion +6"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Int +8, Wis +7, Cha +6"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_speed_: "30 ft., swim 50 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "15 (natural armor)"
_stats_: | 11 (+0) | 14 (+2) | 14 (+2) | 19 (+4) | 17 (+3) | 14 (+2) |

___Amphibious.___ The merfolk can breathe air and water.

___Aquatic Travel.___ The merfolk uses surges of conjured
water to propel its body forward when on land, allowing
it to move rapidly despite its lack of standard ground
travel.

___Spellcasting.___ The merfolk is a 12th-level spellcaster. Its
spellcasting ability is Intelligence (spell save DC 16, +8
to hit with spell attacks). It has the following Wizard
spells prepared:

* Cantrips (at will): _dancing lights, frostbite, shape water_

* 1st level (4 slots): _comprehend languages, shield, silent image_

* 2nd level (3 slots): _blur, hold person_

* 3rd level (3 slots): _tidal wave, wall of water_

* 4th level (3 slots): _control water, greater invisibility, watery sphere_

* 5th level (2 slots): _cone of cold, dominate person_

* 6th level (1 slots): _globe of invulnerability, otiluke’s freezing sphere_

**Actions**

___Multiattack.___ The merfolk makes two attacks with its
staff.

___Staff.___ Melee Weapon Attack: +6 to hit, reach 5ft., one
target. Hit: 6 (1d8 + 2) bludgeoning damage plus 14
(3d8) cold damage.

___Torrent of Water.___ Target creature within 60 feet must
succeed on a DC 16 Dexterity saving throw or take 33
(6d10) cold damage as a powerful torrent of freezing
water flows over them. If a creature fails this saving
throw by 10 or more, its movement speed is reduced
to 10 ft. on its next turn.

___Tether of the Tides (Recharge 5-6).___ The merfolk binds
two targets together with a stream of ice cold water. If
either target tries to move more than 20 feet away
from its bound partner, it must expend 2 feet of
movement for every 1 foot traveled and it takes 11
(2d10) cold damage for every 5 feet traveled.
