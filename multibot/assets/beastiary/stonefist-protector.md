"Stonefist Protector";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Acrobatics +5, Insight +4, Religion +4"
_senses_: "passive Perception 12"
_saving_throws_: "Str +3, Dex +5"
_speed_: "40 ft."
_hit points_: "22 (4d8 + 4)"
_armor class_: "15 (unarmored defense)"
_stats_: | 13 (+1) | 16 (+3) | 12 (+1) | 14 (+2) | 14 (+2) | 8 (-1) |

___Flurry of Blows (2/Short Rest).___ If the protector has
attacked this turn, it can make an additional
unarmed strike as a bonus action. On a hit, the
target must succeed on a DC 13 Dexterity saving
throw or be knocked prone.

___Patient Defense (2/Short Rest).___ The protector can
take the Dodge action as a bonus action.

___Unarmored Defense.___ While not wearing armor, the
protector's AC includes its Wisdom modifier.

**Actions**

___Multiattack.___ The protector makes two attacks, one
with its quarterstaff and one with its unarmed
strike.

___Quarterstaff.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 7 (1d8 + 3) bludgeoning
damage.

___Unarmed Strike.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one target. Hit: 5 (1d4 + 3) bludgeoning
damage.
