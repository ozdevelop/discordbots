"Solar";;;_size_: Large celestial
_alignment_: lawful good
_challenge_: "21 (33,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft."
_skills_: "Perception +14"
_damage_immunities_: "necrotic, poison"
_saving_throws_: "Int +14, Wis +14, Cha +17"
_speed_: "50 ft., fly 150 ft."
_hit points_: "243 (18d10+144)"
_armor class_: "21 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "radiant, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 26 (+8) | 22 (+6) | 26 (+8) | 25 (+7) | 25 (+7) | 30 (+10) |

___Angelic Weapons.___ The solar's weapon attacks are magical. When the solar hits with any weapon, the weapon deals an extra 6d8 radiant damage (included in the attack).

___Divine Awareness.___ The solar knows if it hears a lie.

___Innate Spellcasting.___ The solar's spell casting ability is Charisma (spell save DC 25). It can innately cast the following spells, requiring no material components:

* At will: _detect evil and good, invisibility _(self only)

* 3/day each: _blade barrier, dispel evil and good, resurrection_

* 1/day each: _commune, control weather_

___Magic Resistance.___ The solar has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The solar makes two greatsword attacks.

___Greatsword.___ Melee Weapon Attack: +15 to hit, reach 5 ft., one target. Hit: 22 (4d6 + 8) slashing damage plus 27 (6d8) radiant damage.

___Slaying Longbow.___ Ranged Weapon Attack: +13 to hit, range 150/600 ft., one target. Hit: 15 (2d8 + 6) piercing damage plus 27 (6d8) radiant damage. If the target is a creature that has 190 hit points or fewer, it must succeed on a DC 15 Constitution saving throw or die.

___Flying Sword.___ The solar releases its greatsword to hover magically in an unoccupied space within 5 ft. of it. If the solar can see the sword, the solar can mentally command it as a bonus action to fly up to 50 ft. and either make one attack against a target or return to the solar's hands. If the hovering sword is targeted by any effect, the solar is considered to be holding it. The hovering sword falls if the solar dies.

___Healing Touch (4/Day).___ The solar touches another creature. The target magically regains 40 (8d8 + 4) hit points and is freed from any curse, disease, poison, blindness, or deafness.

**Legendary** Actions

The solar can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The solar regains spent legendary actions at the start of its turn.

___Teleport.___ The solar magically teleports, along with any equipment it is wearing or carrying, up to 120 ft. to an unoccupied space it can see.

___Searing Burst (Costs 2 Actions).___ The solar emits magical, divine energy. Each creature of its choice in a 10 -foot radius must make a DC 23 Dexterity saving throw, taking 14 (4d6) fire damage plus 14 (4d6) radiant damage on a failed save, or half as much damage on a successful one.

___Blinding Gaze (Costs 3 Actions).___ The solar targets one creature it can see within 30 ft. of it. If the target can see it, the target must succeed on a DC 15 Constitution saving throw or be blinded until magic such as the lesser restoration spell removes the blindness.
