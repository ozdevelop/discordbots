"Smaragdine Golem";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "14 (11500 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 120 ft., passive Perception 10"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "231 (22d10 + 110)"
_armor class_: "17 (natural armor)"
_stats_: | 24 (+7) | 11 (+0) | 21 (+5) | 3 (-4) | 11 (+0) | 1 (-5) |

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

___Absorb Magic.___ As a bonus action, the golem targets any creature, object, or magical effect within 10 feet of it. The golem chooses a spell already cast on the target. If the spell is of 3rd level or lower, the golem absorbs the spell and it ends. If the spell is of 4th level or higher, the golem must make a check with a +9 modifier. The DC equals 10 + the spell's level. On a successful check, the golem absorbs the spell and it ends. The golem's body glows when it absorbs a spell, as if under the effect of a light spell. A smaragdine golem can only hold one absorbed spell at a time.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 25 (4d8 + 7) bludgeoning damage.

___Release Spell.___ The golem can release an absorbed spell effect as a blast of green energy, which blasts out as a sphere centered on the golem with a radius of 10 feet per level of the absorbed spell. All creatures in the area of effect other than the golem takes 7 (2d6) lightning damage per level of the absorbed spell, or half damage with a successful DC 18 Dexterity saving throw. Creatures that fail the saving throw are also blinded until the end of the golem's next turn.

