"Invisible Stalker";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Auran, understands Common but doesn't speak it"
_senses_: "darkvision 60 ft."
_skills_: "Perception +8, Stealth +10"
_damage_immunities_: "poison"
_speed_: "50 ft., fly 50 ft. (hover)"
_hit points_: "104 (16d8+32)"
_armor class_: "14"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 16 (+3) | 19 (+4) | 14 (+2) | 10 (0) | 15 (+2) | 11 (0) |

___Invisibility.___ The stalker is invisible.

___Faultless Tracker.___ The stalker is given a quarry by its summoner. The stalker knows the direction and distance to its quarry as long as the two of them are on the same plane of existence. The stalker also knows the location of its summoner.

**Actions**

___Multiattack.___ The stalker makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage.