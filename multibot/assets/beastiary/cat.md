"Cat";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_skills_: "Perception +3, Stealth +4"
_speed_: "40 ft., climb 30 ft."
_hit points_: "2 (1d4)"
_armor class_: "12"
_stats_: | 3 (-4) | 15 (+2) | 10 (0) | 3 (-4) | 12 (+1) | 7 (-2) |

___Keen Smell.___ The cat has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Claws.___ Melee Weapon Attack: +0 to hit, reach 5 ft., one target. Hit: 1 slashing damage.