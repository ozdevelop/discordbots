"Deep Rothe";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "13 (2d8+4)"
_armor class_: "10"
_stats_: | 18 (+4) | 10 (0) | 14 (+2) | 2 (-4) | 10 (0) | 4 (-3) |

___Charge.___ If the rothe moves at least 20 feet straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 7 (2d6) piercing damage.

___Innate Spellcasting.___ The deep rothe's spellcasting ability is Charisma. It can innately cast dancing lights at will, requiring no components.

**Actions**

___Gore.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6+4) piercing damage.