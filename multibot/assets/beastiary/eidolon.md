"Eidolon";;;_page_number_: 194
_size_: Medium undead
_alignment_: any alignment
_challenge_: "12 (8400 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 18"
_skills_: "Perception +8"
_damage_immunities_: "cold, necrotic, poison"
_saving_throws_: "Wis +8"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "63  (18d8 - 18)"
_armor class_: "9"
_condition_immunities_: "charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 7 (-1) | 8 (-1) | 9 (0) | 14 (+2) | 19 (+4) | 16 (+3) |

___Incorporeal Movement.___ The eidolon can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object other than a sacred statue.

___Sacred Animation (Recharge 5-6).___ When the eidolon moves into a space occupied by a sacred statue, the eidolon can disappear, causing the statue to become a creature under the eidolon's control. The eidolon uses the sacred statue's statistics in place of its own.

___Turn Resistance.___ The eidolon has advantage on saving throws against any effect that turns undead.

**Actions**

___Divine Dread___ Each creature within 60 feet of the eidolon that can see it must succeed on a DC 15 Wisdom saving throw or be frightened for 1 minute. While frightened in this way, the creature must take the Dash action and move away from the eidolon by the safest available route at the start of each of its turns, unless there is nowhere for it to move, in which case the creature also becomes stunned until it can move again. A frightened target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a target's saving throw is successful or the effect ends for it, the target is immune to any eidolon's Divine Dread for the next 24 hours.