"Basilisk Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "20 ft."
_hit points_: "60 (8d8 + 16)"
_armor class_: "12 (natural armor)"
_stats_: | 16 (+3) | 8 (-1) | 15 (+2) | 2 (-4) | 8 (-1) | 7 (-2) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it
must make a Constitution saving throw with a DC of 5 + the damage
taken, unless the damage is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) piercing damage plus 7 (2d6) necrotic damage.
