"Quaggoth Spore Servant";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_damage_immunities_: "poison"
_speed_: "20 ft., climb 20 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "13 (natural armor)"
_condition_immunities_: "blinded, charmed, frightened, paralyzed, poisoned"
_stats_: | 17 (+3) | 12 (+1) | 16 (+3) | 2 (-4) | 6 (-2) | 1 (-5) |

**Actions**

___Multiattack.___ The spore servant makes two claw attacks.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.