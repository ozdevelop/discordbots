"Reigon";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Reigon"
_skills_: "Perception +3, Stealth +4"
_senses_: "darkvision 120 ft., passive Perception 13"
_speed_: "30 ft., climb 30 ft."
_hit points_: "59 (7d10 + 21)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 15 (+2) | 16 (+3) | 17 (+3) | 12 (+1) | 10 (+0) |

___Chameleon.___ Whenever the reigon takes the hide action, it has advantage
on Dexterity (Stealth) checks as it uses psionic illusions to assist its
attempt. Creatures with truesight automatically notice the reigon while it
attempts to hide.

___Keen Smell.___ The reigon has advantage on Wisdom (Perception) checks
that rely on smell.

**Actions**

___Multiattack.___ The reigon makes one bite attack, and two greatclub
attacks or two claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 13
(2d8 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 11
(2d6 + 4) slashing damage.

___Greatclub.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit:
13 (2d8 + 4) bludgeoning damage.

___Mindthrust (1/day).___ One creature the reigon can see within 60 feet of it
must make a DC 13 Intelligence saving throw. On a failed saving throw,
the target takes 25 (4d10 + 3) psychic damage, or half as much damage on
a successful saving throw.

___Concussion Blast (1/day).___ The reigon releases a blast of psionic force
in a 15-foot cone. Creatures in the area must make a DC 13 Constitution
saving throw. On a failed saving throw, the target takes 21 (4d8 + 3) force
damage and is stunned until the end of their next turn. On a successful
saving throw, the target takes half damage and is not stunned.
