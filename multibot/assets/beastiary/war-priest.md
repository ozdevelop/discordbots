"War Priest";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "9 (5,000 XP)"
_languages_: "any two languages"
_skills_: "Intimidation +5, Religion +4"
_saving_throws_: "Con +6, Wis +7"
_speed_: "30 ft."
_hit points_: "117 (18d8+36)"
_armor class_: "18 (plate)"
_stats_: | 16 (+3) | 10 (0) | 14 (+2) | 11 (0) | 17 (+3) | 13 (+1) |

___Spellcasting.___ The priest is a 9th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 15, +7 to hit with spell attacks). It has the following cleric spells prepared:

* Cantrips (at will): _light, mending, sacred flame, spare the dying_

* 1st level (4 slots): _divine favor, guiding bolt, healing word, shield of faith_

* 2nd level (3 slots): _lesser restoration, magic weapon, prayer of healing, silence, spiritual weapon_

* 3rd level (3 slots): _beacon of hope, crusader's mantle, dispel magic, revivify, spirit guardians, water wall_

* 4th level (3 slots): _banishment, freedom of movement, guardian of faith, stoneskin_

* 5th level (1 slot): _flame strike, mass cure wounds, hold monster_

**Actions**

___Multiattack.___ The priest makes two melee attacks.

___Maul.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 10 (2d6 +3) bludgeoning damage.

**Reactions**

___Guided Strike (Recharges after a Short or Long Rest).___ The priest grants a +10 bonus to an attack roll made by itself or another creature within 30 feet of it. The priest can make this choice after the roll is made but before it hits or misses.
