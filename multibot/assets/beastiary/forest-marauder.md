"Forest Marauder";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "Giant, Orcish, Sylvan"
_senses_: "darkvision 120 ft., passive Perception 10"
_saving_throws_: "Con +6"
_speed_: "40 ft."
_hit points_: "114 (12d10 +48)"
_armor class_: "15 (natural armor)"
_stats_: | 21 (+5) | 10 (+0) | 18 (+4) | 6 (-2) | 10 (+0) | 7 (-2) |

___Sunlight Sensitivity.___ While in sunlight, the forest marauder has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The forest marauder makes two boar spear attacks.

___Boar Spear.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit:16 (2d10 + 5) piercing damage, and the forest marauder can choose to push the target 10 feet away if it fails a DC 16 Strength saving throw.

___Rock.___ Ranged Weapon Attack: +5 to hit, range 30/120 ft., one target. Hit: 19 (3d8 + 5) bludgeoning damage.

