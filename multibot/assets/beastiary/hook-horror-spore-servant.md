"Hook Horror Spore Servant";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "-"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_speed_: "20 ft., climb 20 ft."
_hit points_: "75 (10d10+20)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "blinded, charmed, frightened, paralyzed"
_damage_resistances_: "poison"
_stats_: | 18 (+4) | 10 (0) | 15 (+2) | 2 (-4) | 6 (-2) | 1 (-5) |

**Actions**

___Multiattack.___ The spore servant makes two hyook attacks.

___Hook.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 11 (2d6 + 4) piercing damage.