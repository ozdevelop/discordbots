"Cyclops";;;_size_: Huge giant
_alignment_: chaotic neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Giant"
_speed_: "30 ft."
_hit points_: "138 (12d12+60)"
_armor class_: "14 (natural armor)"
_stats_: | 22 (+6) | 11 (0) | 20 (+5) | 8 (-1) | 6 (-2) | 10 (0) |

___Poor Depth Perception.___ The cyclops has disadvantage on any attack roll against a target more than 30 ft. away.

**Actions**

___Multiattack.___ The cyclops makes two greatclub attacks.

___Greatclub.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 19 (3d8 + 6) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +9 to hit, range 30/120 ft., one target. Hit: 28 (4d10 + 6) bludgeoning damage.