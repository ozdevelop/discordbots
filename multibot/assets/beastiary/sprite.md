"Sprite";;;_size_: Tiny fey
_alignment_: neutral good
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Perception +3, Stealth +8"
_speed_: "10 ft., fly 40 ft."
_hit points_: "2 (1d4)"
_armor class_: "15 (leather armor)"
_stats_: | 3 (-4) | 18 (+4) | 10 (0) | 14 (+2) | 13 (+1) | 11 (0) |

**Actions**

___Longsword.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 1 slashing damage.

___Shortbow.___ Ranged Weapon Attack: +6 to hit, range 40/160 ft., one target. Hit: 1 piercing damage, and the target must succeed on a DC 10 Constitution saving throw or become poisoned for 1 minute. If its saving throw result is 5 or lower, the poisoned target falls unconscious for the same duration, or until it takes damage or another creature takes an action to shake it awake.

___Heart Sight.___ The sprite touches a creature and magically knows the creature's current emotional state. If the target fails a DC 10 Charisma saving throw, the sprite also knows the creature's alignment. Celestials, fiends, and undead automatically fail the saving throw.

___Invisibility.___ The sprite magically turns invisible until it attacks or casts a spell, or until its concentration ends (as if concentrating on a spell). Any equipment the sprite wears or carries is invisible with it.