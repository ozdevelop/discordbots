"Tabaxi Hunter";;;_size_: Medium humanoid (tabaxi)
_alignment_: chaotic good
_challenge_: "1 (200 XP)"
_Languages_: "Common plus any one language"
_senses_: "darkvision 60 ft., passive Perception 14"
_skills_: "Athletics +2, Perception +4, Stealth +5, Survival +6"
_speed_: "30 ft., climb 20 ft."
_hit points_: "40 (9d8)"
_armor class_: "14 (leather armor)"
_stats_: | 10 (0) | 17 (+3) | 11 (0) | 13 (+1) | 14 (+2) | 15 (+2) |

___Feline agility.___ When the tabaxi moves on its turn in combat, it can double its speed until the end of the turn. Once it uses this ability, the tabaxi can't use it again until it moves 0 feet on one of its turns.

**Actions**

___Multiattack.___ The tabaxi makes two attacks with its claws, its shortsword, or its shortbow.

___Claws.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (ld4) slashing damage.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (l d6 +3) slashing damage.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (ld6 +3) piercing damage.