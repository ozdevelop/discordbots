"Merrenoloth";;;_page_number_: 250
_size_: Medium fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Abyssal, Infernal, telepathy 60 ft."
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 14"
_skills_: "History +5, Nature +5, Perception +4, Survival +4"
_damage_immunities_: "acid, poison"
_saving_throws_: "Dex +5, Int +5"
_speed_: "30 ft., swim 40 ft."
_hit points_: "40  (9d8)"
_armor class_: "13"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 8 (-1) | 17 (+3) | 10 (0) | 17 (+3) | 14 (+2) | 11 (0) |

___Innate Spellcasting.___ The merrenoloth's innate spellcasting ability is Intelligence (spell save DC 13). It can innately cast the following spells, requiring no material components:

* At will: _charm person, darkness, detect magic, dispel magic, gust of wind_

* 3/day: _control water_

* 1/day: _control weather_

___Magic Resistance.___ The merrenoloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The merrenoloth's weapon attacks are magical.

___Teleport.___ As a bonus action, the merrenoloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.

**Actions**

___Multiattack___ The merrenoloth uses Fear Gaze once and makes one oar attack.

___Oar___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) slashing damage.

___Fear Gaze___ The merrenoloth targets one creature it can see within 60 feet of it. The target must succeed on a DC 13 Wisdom saving throw or become frightened for 1 minute. The frightened target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
