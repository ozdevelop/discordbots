"Dralmorrer Borngray";;;_size_: Medium humanoid (high elf)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Bullywug, Draconic, Elvish, Goblin, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +5, Deception +1, Insight +2, Perception +2, Religion +5"
_saving_throws_: "Str +6, Con +4"
_speed_: "30 ft."
_hit points_: "52 (7d10+14)"
_armor class_: "16 (studded leather, shield)"
_stats_: | 18 (+4) | 14 (+2) | 14 (+2) | 16 (+3) | 10 (0) | 8 (-1) |

___Fey Ancestry.___ Dralmorrer has advantage on saving throws against being charmed, and magic can't put him to sleep.

___Spellcasting.___ Dralmorrer is a 7th-level spellcaster that uses Intelligence as his spellcasting ability (spell save DC 13, +5 to hit with spell attacks). Dralmorrer has the following spells prepared from the wizard spell list:

* Cantrips (at will): _fire bolt, prestidigitation, shocking grasp_

* 1st level (4 slots): _longstrider, magic missile, shield, thunderwave_

* 2nd level (2 slots): _magic weapon, misty step_

___War Magic.___ When Dralmorrer uses his action to cast a cantrip, he can also take a bonus action to make one weapon attack.

___Weapon Bond.___ Provided his longsword is on the same plane Dralmorrer can take a bonus action to teleport it to his hand.

**Actions**

___Multiattack.___ Dralmorrer attacks twice, either with his longsword or dagger.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or ranged 20 ft./60 ft., one target. Hit: 6 (1d4 + 4) piercing damage.
