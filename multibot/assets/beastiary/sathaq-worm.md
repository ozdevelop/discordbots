"Sathaq Worm";;;_size_: Huge elemental
_alignment_: neutral evil
_challenge_: "10 (5900 XP)"
_languages_: "understands Deep Speech and Terran, but can't speak"
_skills_: "Perception +5, Stealth +2 (+6 in sand, mud, or"
_senses_: "tremorsense 60 ft., passive Perception 15"
_damage_vulnerabilities_: "thunder"
_damage_immunities_: "acid, poison"
_damage_resistances_: "fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "20 ft., burrow 20 ft., swim 20 ft."
_hit points_: "172 (15d12 + 75)"
_armor class_: "16 (natural armor)"
_stats_: | 22 (+6) | 6 (-2) | 20 (+5) | 5 (-3) | 12 (+1) | 9 (-1) |

___Agonizing Aura.___ The sathaq worms' presence induces pain in creatures native to the Material Plane. Any creature that starts its turn within 30 feet of the sathaq worm must make a DC 17 Fortitude saving throw. On a failed save, the creature is poisoned until the start of its next turn. If a creature's saving throw succeeds, it is immune to the sathaq worm's Agonizing Aura for the next 24 hours.

___Earth Glide.___ The sathaq worm can burrow through nonmagical, unworked earth and stone. While doing so, the elemental doesn't disturb the material it moves through.

___Siege Monster.___ The sathaq worm deals double damage to objects and structures.

**Actions**

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 24 (4d8 + 6) piercing damage. If the target is a Large or smaller creature, it must succeed on a DC 18 Dexterity saving throw or be swallowed by the sathaq worm. A swallowed creature is blinded and restrained, has total cover against attacks and other effects outside the worm, and takes 7 (2d6) bludgeoning damage plus 7 (2d6) slashing damage plus 7 (2d6) acid damage at the start of each of the sathaq worm's turns. The sathaq worm can have only one creature swallowed at a time. If the sathaq worm takes 20 damage or more on a single turn from a creature inside it, the sathaq worm must succeed on a DC 15 Constitution saving throw at the end of that turn or regurgitate the swallowed creature, which falls prone in a space within 5 feet of the sathaq worm. If the sathaq worm dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 10 feet of movement, exiting prone.

