"Gulper Eel";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +4"
_senses_: "blindsight 60 ft., passive Perception 13"
_speed_: "swim 50 ft."
_hit points_: "112 (15d10 + 30)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 14 (+2) | 2 (-4) | 12 (+1) | 2 (-4) |

___Keen Smell.___ The eel has advantage on Wisdom (Perception) checks that rely on smell.

___Water Breathing.___ The eel can only breathe underwater.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 20
(5d6 + 3) piercing damage, and the target is grappled (escape DC 13).

Until this grapple ends, the eel can bite only the grappled creature and has
advantage on attack rolls to do so.

___Swallow.___ The eel makes one bite attack against a Medium or smaller
target it is grappling. If the attack hits, the target is swallowed, and the
grapple ends. The swallowed target is blinded and restrained, it has total
cover against attacks and other effects outside the eel, and it takes 10 (3d6)
acid damage at the start of each of the eel’s turns. The eel can have only
one target swallowed at a time.

If the eel takes 25 damage or more on a single turn from the creature
inside of it, the eel must succeed on a DC 12 Constitution saving throw
at the end of that turn or regurgitate the swallowed creature, which falls
prone in a space within 10 feet of the eel. If the eel dies, a swallowed
creature is no longer restrained by it and can escape from the corpse using
15 feet of movement, exiting prone.
