"Bone Drake";;;_size_: Large undead
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "understands Draconic but can't speak"
_senses_: "darkvision 60 ft., passive Perception 12"
_skills_: "Perception +2"
_speed_: "30 ft., burrow 30 ft., climb 30 ft."
_hit points_: "52 (7d8+21)"
_armor class_: "12 (natural armor)"
_stats_: | 17 (+3) | 11 (+0) | 17 (+3) | 4 (-3) | 10 (+0) | 7 (-2) |

___Pack Tactics.___ The bone drake has advantage on an attack roll against a creature if at least one of the drake's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The bone drake makes two attacks: one with its bite and one with its tail.

___Bite.___ Melee weapon attack: +5 to hit, reach 5 ft., one target.  Hit: 7 (1d8+3)
piercing damage.

___Tail.___ Melee weapon attack: +5 to hit, reach 10 ft., one target.  Hit: 6 (1d6+3)
bludgeoning damage, and the target must succeed on a DC 12 Constitution saving throw or
become poisoned for 1 minute.  The target can repeat the saving throw at the end of each of
its turns, ending the effect on itself on a success.  A creature poisoned in this way has their
speed halved and cannot take reactions.
