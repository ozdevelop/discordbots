"Clubneck";;;_size_: Medium monstrosity
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Perception +2, Stealth +3"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "40 ft."
_hit points_: "22 (4d8 + 4)"
_armor class_: "13 (natural armor)"
_stats_: | 14 (+2) | 13 (+1) | 12 (+1) | 2 (-4) | 10 (+0) | 6 (-2) |

___Charge.___ If the clubneck moves at least 20 feet straight toward a target
and then hits with its beak on the same turn, the target takes an extra 3
(1d6) damage. If the target is a creature, it must succeed on a DC 12
Strength saving throw or be knocked prone.

**Actions**

___Multiattack.___ The clubneck makes three attacks: one with its beak and two with its claws.

___Beak.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.
