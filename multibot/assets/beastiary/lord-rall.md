"Lord Rall";;;_size_: Medium aberration
_alignment_: chaotic neutral
_challenge_: "8 (2,900 XP)"
_languages_: "Common, Deep Speech, Primordial"
_skills_: "Arcana +8, Deception +6, History +8, Insight +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Int +8, Wis +5, Cha +6"
_damage_resistances_: "psychic"
_condition_immunities_: "charmed, petrified"
_speed_: "30 ft., fly 10 ft. (hover)"
_hit points_: "91 (14d8 + 28)"
_armor class_: "12 (15 with <i>mage armor</i>)"
_stats_: | 13 (+1) | 10 (+0) | 14 (+2) | 20 (+5) | 15 (+2) | 16 (+3) |

___Everchanging Changers.___ The Court of All Flesh are
beings of pure chaos. Because their minds are pure
disorder, they cannot be driven mad or charmed
and any attempts to magically compel their behavior fails.

___Formless Shape.___ Lord Rall is immune to any
spell or effect that would alter his form.

___Shapechanger.___ As an action, Lord Rall can polymorph into a Small or Medium creature or back
into his true form. His statistics, other than his size,
are the same in each form. Any worn or carried
equipment isn’t transformed. He reverts to his true
form if he dies.

___Spellcasting.___ Lord Rall is a 10th-level spellcaster.
His spellcasting ability is Intelligence (spell save DC
16, +8 to hit with spell attacks). He has the following wizard spells prepared:

* Cantrips (at will): _mage hand, minor illusion, ray of frost_

* 1st level (4 slots): _mage armor, magic missile, sleep_

* 2nd level (3 slots): _ray of enfeeblement, mirror image_

* 3rd level (3 slots): _lightning bolt, haste_

* 4th level (3 slots): _black tentacles, blight_

* 5th level (2 slots): _cloudkill_

**Actions**

___Multiattack.___ Lord Rall makes two melee attacks
with the Staff of Changing.

___The Staff of Changing (Recharge 5–6).___ Lord Rall’s
staff remakes a target into any shape he desires.
He casts polymorph from it as an action. The range
is 30 feet, and the Wisdom saving throw has a DC
of 16. Lord Rall does not need to concentrate on
this spell to maintain it.

___Greatstaff.___ Melee Weapon Attack: +4 to hit, reach
5 ft., one target. Hit: 8 (2d6 + 1) bludgeoning
damage, or 10 (2d8 + 1) bludgeoning damage if
wielded with two hands.

___Any Pile of Organs Will Do (Recharge 6).___ When he
drops to 0 hit points, Lord Rall can reform himself
from any corpse within 120 feet instead of falling
unconscious. Lord Rall does not need to see the
corpse in order to use it. The process happens in
an instant, and the newly formed Lord Rall appears
with only half of his maximum hit points.
