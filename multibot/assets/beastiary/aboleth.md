Aboleth;;;_size_: Large aberration
_alignment_: lawful evil
_challenge_: 10 (5,900 XP)
_languages_: Deep Speech, telepathy 120 ft.
_senses_: darkvision 120 ft.
_skills_: History +12, Perception +10
_saving_throws_: Con +6, Int +8, Wis +6
_speed_: 10 ft., swim 40 ft.
_hit points_: 135 (18d10+36)
_armor class_: 17 (natural armor)
_stats_: | 21 (+5) | 9 (-1) | 15 (+2) | 18 (+4) | 15 (+2) | 18 (+4) |

___Amphibious.___ The aboleth can breathe air and water.

___Mucous Cloud.___ While underwater, the aboleth is surrounded by transformative mucus. A creature that touches the aboleth or that hits it with a melee attack while within 5 ft. of it must make a DC 14 Constitution saving throw. On a failure, the creature is diseased for 1d4 hours. The diseased creature can breathe only underwater.

___Probing Telepathy.___ If a creature communicates telepathically with the aboleth, the aboleth learns the creature's greatest desires if the aboleth can see the creature.

**Actions**

___Multiattack.___ The aboleth makes three tentacle attacks.

___Tentacle.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage. If the target is a creature, it must succeed on a DC 14 Constitution saving throw or become diseased. The disease has no effect for 1 minute and can be removed by any magic that cures disease. After 1 minute, the diseased creature's skin becomes translucent and slimy, the creature can't regain hit points unless it is underwater, and the disease can be removed only by heal or another disease-curing spell of 6th level or higher. When the creature is outside a body of water, it takes 6 (1d12) acid damage every 10 minutes unless moisture is applied to the skin before 10 minutes have passed.

___Tail.___ Melee Weapon Attack: +9 to hit, reach 10 ft. one target. Hit: 15 (3d6 + 5) bludgeoning damage.

___Enslave (3/day).___ The aboleth targets one creature it can see within 30 ft. of it. The target must succeed on a DC 14 Wisdom saving throw or be magically charmed by the aboleth until the aboleth dies or until it is on a different plane of existence from the target. The charmed target is under the aboleth's control and can't take reactions, and the aboleth and the target can communicate telepathically with each other over any distance.

Whenever the charmed target takes damage, the target can repeat the saving throw. On a success, the effect ends. No more than once every 24 hours, the target can also repeat the saving throw when it is at least 1 mile away from the aboleth.

**Legendary** Actions

The aboleth can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The aboleth regains spent legendary actions at the start of its turn.

___Detect.___ The aboleth makes a Wisdom (Perception) check.

___Tail Swipe.___ The aboleth makes one tail attack.

___Psychic Drain (Costs 2 Actions).___ One creature charmed by the aboleth takes 10 (3d6) psychic damage, and the aboleth regains hit points equal to the damage the creature takes.