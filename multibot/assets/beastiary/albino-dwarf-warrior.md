"Albino Dwarf Warrior";;;_size_: Medium humanoid (dwarf)
_alignment_: any alignment
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Dwarvish"
_senses_: "darkvision 60ft., passive Perception 14"
_damage_resistances_: "poison"
_skills_: "Perception +4, Stealth +3, Survival +4"
_speed_: "25 ft."
_hit points_: "30 (4d8 +12)"
_armor class_: "13 (hide armor)"
_stats_: | 13 (+1) | 13 (+1) | 17 (+3) | 12 (+1) | 14 (+2) | 11 (0) |

___Dwarven Resilience.___ The dwarf has advantage on saving throws against poison.

**Actions**

___Handaxe.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d6 +1) slashing damage.