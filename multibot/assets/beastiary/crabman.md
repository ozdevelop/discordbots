"Crabman";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Crabman, some speak Common"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "30 ft., swim 30 ft."
_hit points_: "37 (5d10 + 10)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 11 (+0) | 15 (+2) | 10 (+0) | 10 (+0) | 8 (-1) |

___Amphibious.___ The crabman can breathe air and water.

**Actions**

___Multiattack.___ The crabman makes two attacks with its pincers.

___Pincers.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit:
10 (2d6 + 3) slashing damage. The target is grappled (escape DC 13) if
it is a Large or smaller creature and the crabman doesn’t have another
creature grappled already. The target is restrained until the grapple ends.
