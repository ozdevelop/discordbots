"Scorpion Cultist";;;_size_: Medium humanoid
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Animal Handling +2, Deception +2, Perception +3, Stealth +4"
_senses_: "passive Perception 15"
_damage_resistances_: "poison"
_speed_: "30 ft."
_hit points_: "19 (3d8 + 6)"
_armor class_: "13 (leather armor)"
_stats_: | 11 (+0) | 14 (+2) | 15 (+2) | 10 (+0) | 13 (+1) | 10 (+0) |

___Keen Senses.___ The scorpion cultist has advantage on Wisdom (Perception) checks.

**Actions**

___Multiattack.___ The scorpion cultist makes two melee attacks or two ranged attacks.

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5 ft, one target. Hit: 5 (1d6 + 2) slashing damage plus 3 (1d6) poison damage.

___Sling.___ Melee Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 4 (1d4 + 2) bludgeoning damage.

