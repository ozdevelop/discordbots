"Hellcat";;;_size_: Tiny aberration
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "understands Common but can’t speak; telepathy 60 ft."
_skills_: "Perception +4, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Int +2, Wis +4"
_speed_: "40 ft., climb 30 ft."
_hit points_: "44 (8d4 + 24)"
_armor class_: "13 (natural armor)"
_stats_: | 3 (-4) | 15 (+2) | 16 (+3) | 11 (+0) | 15 (+2) | 18 (+4) |

___Death Sense.___ The hellcat can sense the exact
location of any humanoid within 120 feet with less
than half its hit points.

___False Appearance.___ Unless it is using
its Death Gaze ability, the hellcat is
indistinguishable from a normal housecat.

___Magic Resistance.___ The hellcat has
advantage on saving throws against
spells and other magical effects.

**Actions**

___Multiattack.___ The hellcat makes one bite attack and one claw attack.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9
(2d6 + 2) slashing damage.

___Death Gaze (Recharge 5–6).___ One target within 30 feet of the hellcat
that it can see must make a DC 14 Constitution saving throw. On a failed
saving throw, the target takes 27 (6d8) necrotic damage. If the creature
drops to 0 hit points from this damage, it dies, and can only be restored to
life by means of a true resurrection or wish spell.
