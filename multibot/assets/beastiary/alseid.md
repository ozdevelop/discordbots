"Alseid";;;_size_: Medium monstrosity
_alignment_: chaotic neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Nature +3, Perception +5, Stealth +5, Survival +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "40 ft."
_hit points_: "49 (9d8 + 9)"
_armor class_: "14 (leather armor)"
_stats_: | 13 (+1) | 17 (+3) | 12 (+1) | 8 (-1) | 16 (+3) | 8 (-1) |

___Woodfriend.___ When in a forest, alseid leave no tracks and automatically discern true north.

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d6 + 1) piercing damage, or 5 (1d8 + 1) piercing damage if used with two hands to make a melee attack.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6+3) piercing damage.

