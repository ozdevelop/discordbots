"Fext";;;_size_: Medium undead
_alignment_: any alignment
_challenge_: "6 (2300 XP)"
_languages_: "the languages spoken by its patron"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +6, Wis +4, Cha +7"
_damage_resistances_: "bludgeoning, piercing, and slashing damage with nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "30 ft."
_hit points_: "60 (11d8+11)"
_armor class_: "17 (natural armor)"
_stats_: | 14 (+2) | 16 (+3) | 1 (-5) | 14 (+2) | 12 (+1) | 18 (+4) |

___Innate Spellcasting.___ The fext's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

At will: hex

3/day each: counterspell, fear, gaseous form

1/day each: hold monster, true seeing

___Magic Resistance.___ The fext has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The fext's weapon attacks are magical.

___Patron Blessing.___ A fext is infused with a portion of their patron's power. They have an Armor Class equal to 10 + their Charisma modifier + their Dexterity modifier.

**Actions**

___Multiattack.___ The fext makes two melee or ranged attacks.

___Eldritch Blade.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 9 (2d6+2) slashing damage plus 16 (3d10) force damage.

___Eldritch Fury.___ Ranged Weapon Attack: +6 to hit, range 60/200 ft., one creature. Hit: 25 (4d10 + 3) force damage.

