"Spinosaurus";;;_size_: Gargantuan beast
_alignment_: unaligned
_challenge_: "13 (10000 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: ", passive Perception 15"
_speed_: "60 ft., swim 40 ft."
_hit points_: "231 (14d20 + 84)"
_armor class_: "15 (natural armor)"
_stats_: | 27 (+8) | 9 (-1) | 22 (+6) | 2 (-4) | 11 (+0) | 10 (+0) |

___Tamed.___ The spinosaurus will never willingly attack any reptilian humanoid, and if forced or magically compelled to do so, it suffers disadvantage on attack rolls. Up to twelve Medium or four Large creatures can ride the spinosaurus. This trait disappears if the spinosaurus spends a month away from any reptilian humanoid.

___Siege Monster.___ The spinosaurus deals double damage to objects and structures.

**Actions**

___Multiattack.___ The spinosaurus makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 34 (4d12 + 8) piercing damage. If the target is a Large or smaller creature, it is grappled (escape DC 18). When the spinosaurus moves, the grappled creature moves with it. Until this grapple ends, the target is restrained and the spinosaurus can't bite another target.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 22 (4d6 + 8) slashing damage.

___Tail.___ Melee Weapon Attack: +13 to hit, reach 20 ft., one target. Hit: 21 (3d8 + 8) bludgeoning damage.

___Frightful Presence.___ Each creature of the spinosaurus' choice that is within 120 feet of the spinosaurus and aware of it must succeed on a DC 18 Wisdom saving throw or become frightened for 1 minute. A frightened creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the spinosaurus' Frightful Presence for the next 24 hours.

**Legendary** Actions

___The spinosaurus can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The spinosaurus regains spent legendary actions at the start of its turn.

___Move.___ The spinosaurus moves up to half its speed.

___Roar.___ The spinosaurus uses Frightful Presence.

___Tail Attack (Costs 2 Actions).___ The spinosaurus makes one tail attack.

