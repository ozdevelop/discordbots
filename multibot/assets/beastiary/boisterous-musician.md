"Boisterous Musician";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages"
_skills_: "Acrobatics +5, Insight +4, Performance +9, Persuasion +6, Sleight of Hand +8"
_senses_: "passive Perception 11"
_saving_throws_: "Dex +5, Cha +6"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "14 (studded leather)"
_stats_: | 10 (+0) | 14 (+2) | 15 (+2) | 12 (+1) | 12 (+1) | 17 (+3) |

___Cutting Words (2/Short Rest).___ When a creature that
the musician can see within 50 feet makes an
attack roll, ability check, or a damage roll, the
musician sings a quick disruptive melody. Roll a d8
and subtract the number from the creature's roll.

___Inspire (2/Short Rest).___ Target uninspired creature
within 60 feet of the musician that can hear it gains
a d8 inspiration die. Once within the next 10
minutes, that creature can roll that die and add the
number rolled to one ability check, attack roll, or
saving throw it makes. This die must be rolled
before it is decided if the roll succeeds or fails.

___Spellcasting.___ The musician is a 6th-level spellcaster.
Its spellcasting ability is Charisma (spell save DC
14, +6 to hit with spell attacks). The musician has
the following bard and wizard spells prepared:

* Cantrips (at will): _friends, mage hand, vicious mockery_

* 1st level (4 slots): _charm person, cure wounds, magic missile, unseen servant_

* 2nd level (3 slots): _enthrall, hold person, shatter_

* 3rd level (3 slots): _fireball, hypnotic pattern, tongues_

**Actions**

___Rapier.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 6 (1d8 + 2) piercing damage.
