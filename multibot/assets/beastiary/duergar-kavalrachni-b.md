"Duergar Kavalrachni (B)";;;_page_number_: 189
_size_: Medium humanoid (dwarf)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 10"
_speed_: "25 ft."
_hit points_: "26  (4d8 + 8)"
_armor class_: "16 (scale mail, shield)"
_damage_resistances_: "poison"
_stats_: | 14 (+2) | 11 (0) | 14 (+2) | 11 (0) | 10 (0) | 9 (0) |

___Cavalry Training.___ When the duergar hits a target with a melee attack while mounted on a female steeder, the steeder can make one melee attack against the same target as a reaction.

___Duergar Resilience.___ The duergar has advantage on saving throws against poison, spells, and illusions, as well as to resist being charmed or paralyzed.

___Sunlight Sensitivity.___ While in sunlight, the duergar has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The duergar makes two war pick attacks.

___War Pick___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage plus 5 (2d4) poison damage.

___Heavy Crossbow___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage.

___Shared Invisibility (Recharges after a Short or Long Rest)___ The duergar magically turns invisible for up to 1 hour or until it attacks, it casts a spell, or its concentration is broken (as if concentrating on a spell). Any equipment the duergar wears or carries is invisible with it. While the invisible duergar is mounted on a female steeder, the steeder is invisible as well. The invisibility ends early on the steeder immediately after it attacks.