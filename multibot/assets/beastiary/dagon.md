"Dagon, Demon Prince of the Sea";;;_size_: Large fiend (demon lord)
_alignment_: chaotic evil
_challenge_: "26 (90,000 XP)"
_languages_: "Abyssal, Celestial, Common, Draconic, Giant, Infernal; telepathy 120 ft."
_skills_: "Athletics +15, History +12, Insight +13, Intimidation +12, Perception +13"
_senses_: "truesight 120 ft., passive Perception 23"
_saving_throws_: "Dex +12, Con +16, Wis +13, Cha +12"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "20 ft., swim 60 ft."
_hit points_: "472 (35d10 + 280)"
_armor class_: "19 (natural armor)"
_stats_: | 25 (+7) | 18 (+4) | 27 (+8) | 18 (+4) | 20 (+5) | 19 (+4) |

___Special Equipment.___ Dagon carries Embrace of the Uncaring Sea, a
powerful magic trident.

___Innate Spellcasting.___ Dagon’s innate spellcasting ability is Charisma
(spell save DC 20, +12 to hit with spell attacks). It can cast the following
spells, requiring no material components.

* At will: _cloudkill (works underwater only), control water, create
and destroy water, darkness, detect evil and good, detect magic, detect
thoughts, magic missile (as the 4th level spell), teleport, telekinesis,
tongues, water breathing_

* 3/day each: _conjure animals (aquatic creatures only), conjure elemental
(water elemental only), dispel magic, dominate beast (aquatic creatures
only), fear_

* 1/day each: _confusion, feeblemind, finger of death, hold person, hold
monster, lightning bolt, stoneskin, time stop, web, wish_

___Legendary Resistance (3/day).___ If Dagon fails a saving throw, it can
choose to succeed instead.

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Unholy Aura.___ An unholy aura surrounds Dagon out to a radius of 40
feet. A creature who enters or begins their turn in the area must make a DC
21 Wisdom saving throw. On a failed saving throw, the target is frightened
for 1 minute. While frightened, they are paralyzed. A frightened target can
repeat the saving throw at the end of each of its turns, ending the effect
on a success.

___Water Mastery.___ Dagon has advantage on attack rolls, ability checks,
and saving throws when even partly immersed in sea water.

**Actions**

___Multiattack.___ Dagon makes three melee weapon attacks.

___Embrace of the Uncaring Sea.___ Melee or Ranged Weapon Attack: +18
to hit, reach 10 ft. or range 20/60 ft., one creature. Hit: 24 (4d6 + 10)
piercing damage, or 28 (4d8 + 10) piercing damage if used with two hands
to make a melee attack, plus 17 (5d6) poison damage. If Dagon rolls a
natural 19 or 20, the target must make a DC 21 Constitution saving throw
or be afflicted by the crushing pressure of the sea. A creature afflicted by
this cannot breathe, use verbal components, or take actions or reactions.
At the end of each of the target’s turns, it can repeat the saving throw,
ending the effect on a success. Magic such as heal can end this affliction
early.

___Claw.___ Melee Weapon Attack: +15 to hit, reach 5 ft., one target. Hit: 25
(4d8 + 7) slashing damage and target is poisoned until the beginning of
Dagon’s next turn.

___Command Aquatic Creature (Recharge 5–6).___ One creature with a
swim speed that Dagon can see within 120 feet of it hears the telepathic
commands of Dagon and must make a DC 21 Wisdom saving throw. On
a failed save, the creature is charmed for 24 hours. While charmed, it
must follow the orders of Dagon to the best of the target’s ability and it is
immune to being charmed or frightened by another effect or spell. A target
can repeat the saving throw when it takes damage, ending the effect on a
success.

**Legendary** Actions

Dagon can take 3 legendary actions, choosing from the options below.
Only one legendary action option can be used at a time and only at the
end of another creature’s turn. Dagon regains spent legendary actions at
the start of its turn.

___Move.___ If at least halfway submerged in water, Dagon can move up to
half its swim speed without provoking opportunity attacks.

___Command Aquatic Creature (Costs 2 Actions).___ Dagon uses its
Command Aquatic Creature ability, even if it has not recharged.

___Ink Cloud (Costs 3 Actions).___ Dagon exhales a 60-foot cone of inky
blackness. Underwater it is as black ink, while above water it manifests
as a thick cloud, and the area counts as heavily obscured. It remains until
dispersed by a strong current or wind (at least 20 miles per hour). Dagon
can end this effect on its turn as a bonus action. Creatures who enter or
begin their turn in the area must make a successful DC 22 Constitution
saving throw or take 18 (4d8) poison damage.

**Lair** Actions

On initiative count 20 (losing initiative ties), Dagon can take a lair
action to cause one of the following effects; Dagon can’t use the same
effect two rounds in a row:

___Maelstrom.___ The currents and eddies of the underwater realm Dagon
occupies swirl in a maelstrom out to a radius of 100 feet. All creatures in
that area, other than Dagon, must make a DC 21 Strength saving throw.
On a failed save, the target is moved in a random direction 30 feet.

___Control Tempature.___ Dagon selects an area of water within 100 feet
no larger than a 20-foot sphere and causes it to heat to boiling or cool
drastically. Any creature within that area must make a DC 21 Constitution
saving throw, taking 27 (6d8) cold or fire damage on a failed save, or half
as much on a successful saving throw.

**Regional** Effects

The region containing Dagon’s lair is warped by its magic, creating one
or more of the following effects:

___Aquatic Bestiary.___ Aquatic creatures of all types are drawn to Dagon’s
presence; often they are only hostile to non-aquatic creatures.

___Unusual Weather.___ Once per day, Dagon can control the weather
surrounding his lair out to a 6-mile radius. The effect is identical to the
control weather spell.

___Adverse Conditions.___ Within 1 mile of the lair, Celestial creatures find
the air or water difficult to breathe and tread. These creatures must make
a DC 18 Constitution saving throw whenever they take a long rest. On a
failed save, the creature does not benefit from that long rest.
If Dagon is slain, changed weather reverts to normal as described in the
spell, and the other effects fade in 1d10 days.

# Embrace of the Uncaring Sea

_Weapon (trident), artifact (requires attunement)_

Barnacles and small lampreys cover this 10-foot-long, deepblue-
green weapon; no matter how many barnacles or lampreys
are removed, the weapon always seems covered in them. The three
mismatched tines of the trident drip salty brine at all times, and the
weapon always appears wet.

___Attunement.___ In order to attune to Embrace of the Uncaring
Sea, you must feed the lampreys that adorn its haft, dealing 8d10
necrotic damage to you, which cannot be resisted in any way.
Oversized. A Medium-sized creature has disadvantage on attack
rolls made with Embrace of the Uncaring Sea unless they wield the
weapon two-handed.

___Magic Weapon.___ You have a +3 bonus to attack and damage rolls
made with Embrace of the uncaring Sea. It also functions as a
trident of fish command.

___Brinetooth.___ You deal an additional 5d6 poison damage on a
successful hit with the trident.

___Embrace of the Depths.___ If you roll a 19 or 20 on an attack with
this trident and you hit, the target must make a DC 21 Constitution
saving throw or be afflicted by the crushing pressure of the sea. A
creature afflicted by this cannot breathe, cast spells using verbal
components, or cannot take actions or reactions. At the end of each
of the target’s turns, it can repeat the saving throw, ending the effect
on a success. Magic such as heal can end this affliction early.
___Destruction.___ You can destroy Embrace of the Uncaring Sea
by stabbing the tines into the highest mountain within the hottest
desert and leaving it to dry in the blazing sun for 100 years, at
which time it crumbles into ash. If it is removed prior to the 100
years elapsing, all progress is lost.
