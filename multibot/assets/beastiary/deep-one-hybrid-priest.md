"Deep One Hybrid Priest";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Void Speech"
_skills_: "Athletics +6, Deception +4, Perception +3"
_senses_: "darkvision 120 ft., passive Perception 13"
_saving_throws_: "Con +5, Wis +3, Cha +4"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "cold"
_speed_: "30 ft., swim 30 ft"
_hit points_: "120 (16d8 + 48)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 16 (+3) | 12 (+1) | 12 (+1) | 15 (+2) |

___Amphibious.___ A deep one priest can breathe air or water with equal ease.

___Frenzied Rage.___ On its next turn after a deep one hybrid priest takes 10 or more damage from a single attack, it has advantage on its melee attacks and adds +4 to spell and claws damage.

___Innate Spellcasting.___ The deep one priest's innate spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: sacred flame, shocking grasp

3/day each: inflict wounds, sanctuary, sleep

1/day each: ice storm, shatter

___Lightless Depths.___ A deep one hybrid priest is immune to the pressure effects of the deep ocean.

___Ocean Change.___ A deep one born to a human family resembles a human child, but transforms into an adult deep one between the ages of 16 and 30.

___Voice of the Deeps.___ A deep one priest may sway an audience of listeners with its rolling, droning speech, fascinating them for 5 minutes and making them dismiss or forget what they've seen recently unless they make a successful DC 13 Wisdom saving throw at the end of that period. If the saving throw succeeds, they remember whatever events the deep one sought to erase.

**Actions**

___Claws.___ Melee Weapon Attack. +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

