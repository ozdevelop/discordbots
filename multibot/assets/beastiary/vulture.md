"Vulture";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_skills_: "Perception +3"
_speed_: "10 ft., fly 50 ft."
_hit points_: "5 (1d8+1)"
_armor class_: "10"
_stats_: | 7 (-2) | 10 (0) | 13 (+1) | 2 (-4) | 12 (+1) | 4 (-3) |

___Keen Sight and Smell.___ The vulture has advantage on Wisdom (Perception) checks that rely on sight or smell.

___Pack Tactics.___ The vulture has advantage on an attack roll against a creature if at least one of the vulture's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Beak.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d4) piercing damage.