"Gravigas";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Infernal, Abyssal"
_skills_: "Arcana +5, Athletics +8, Intimidation +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_saving_throws_: "Str +8, Dex +7, Con +7"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft., fly 50 ft."
_hit points_: "119 (14d10 + 42)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 17 (+3) | 16 (+3) | 12 (+1) | 14 (+2) | 7 (-2) |

___Gravity Well.___ The demon exudes a powerful gravity
field in a 30-foot radius around itself. This area is
considered difficult terrain for all non-evil creatures.
Other creatures within this field cannot jump or fly
and have disadvantage on Strength and Dexterity
checks. Additionally, all ranged weapon attacks
against the demon are made with disadvantage.

**Actions**

___Multiattack.___ The demon makes three attacks: two
with is claws and one with its tail.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5ft.,
one target. Hit: 8 (1d8 + 4) slashing damage plus 9
(2d8) force damage.

___Tail.___ Melee Weapon Attack: +8 to hit, reach 10 ft.,
one target. Hit: 15 (2d10 + 4) slashing damage
plus 9 (2d8) force damage.

___Crushing Forces (Recharge 6).___ The demon causes the
power of the gravitational field to increase tenfold
for a moment. Each creature in the area of the
gravity well must make a DC 15 Strength saving
throw, taking 44 (8d10) bludgeoning damage and
being knocked prone on a failed save, or half as
much damage and not knocked prone on a
successful one.

___Gravity Surge (3/Day).___ The demon causes the
gravitational forces around it to surge with power.
Each creature in the area of the gravity well must
succeed on a DC 15 Strength saving throw or be
pulled to an unoccupied space adjacent to the
demon. Afterwards, the demon makes a claw attack
against each enemy pulled, making a separate
attack roll for each attack.
