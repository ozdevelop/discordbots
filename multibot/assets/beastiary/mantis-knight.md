"Mantis Knight";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Elven, Sylvan"
_skills_: "Acrobatics +7, Deception +4, Nature +3, Persuasion +4, Perception +6, Stealth +7, Survival +4"
_senses_: "passive Perception 17"
_saving_throws_: "Str +6, Wis +4, Cha +4"
_speed_: "30 ft."
_hit points_: "71 (11d8 + 22)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 18 (+4) | 14 (+2) | 11 (+0) | 13 (+1) | 12 (+1) |

___Sunbeetle Armor.___ The Knight’s armor is perpetually
slick. Any attempts to grapple the Knight
have disadvantage.

___Fey Ancestry.___ Magic cannot put the Knight
to sleep.

___Magic Resistance.___ The Knight has advantage
on saving throws against spells and other
magical effects.

___Command Fey.___ As a member of the Court
of Arcadia, the Knight can cast _dominate
monster_ (DC 12) at will on any fey creature or elf.

___Innate Spellcasting.___ The Knight’s innate spellcasting
ability is Charisma (spell save DC 12).
The Knight can innately cast the following spells,
requiring no material components:

* At will: _heroism, shield of faith_

**Actions**

___Multiattack.___ The Knight makes four attacks
with its rapiers, or makes two attacks and casts
heroism or shield of faith on an ally.

___Rapier.___ Melee Weapon Attack: +7 to hit, reach 5
ft., one target. Hit: 8 (1d8 + 4) piercing damage.
