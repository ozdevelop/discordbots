"Giant Constrictor Snake";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "blindsight 10 ft."
_skills_: "Perception +2"
_speed_: "30 ft., swim 30 ft."
_hit points_: "60 (8d12+8)"
_armor class_: "12"
_stats_: | 19 (+4) | 14 (+2) | 12 (+1) | 1 (-5) | 10 (0) | 3 (-4) |

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one creature. Hit: 11 (2d6 + 4) piercing damage.

___Constrict.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 13 (2d8 + 4) bludgeoning damage, and the target is grappled (escape DC 16). Until this grapple ends, the creature is restrained, and the snake can't constrict another target.