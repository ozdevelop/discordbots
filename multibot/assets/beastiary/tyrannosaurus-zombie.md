"Tyrannosaurus Zombie";;;_size_: Huge undead
_alignment_: unaligned
_challenge_: "8 (3,900 XP)"
_senses_: "darkvision 60 ft., passive Perception 6"
_condition_immunities_: "poisoned"
_damage_immunities_: "poison"
_speed_: "40 ft."
_hit points_: "136 (13d12 +52)"
_armor class_: "11 (natural armor)"
_stats_: | 25 (+7) | 6 (-2) | 19 (+4) | 1 (-5) | 3 (-4) | 5 (-3) |

___Disgorge Zombie.___ As a bonus action , the tyrannosaurus zombie can disgorge a normal zombie, which appears in an unoccupied space within 10 feet of it. The disgorged zombie acts on its own initiative count. After a zombie is disgorged, roll a d6. On a roll of l, the tyrannosaurus zombie runs out of zombies to disgorge and loses this trait . If the tyrannosaurus zombie still has this trait when it dies, ld4 normal zombies erupt from its corpse at the start of its next turn. These zombies act on their own initiative count.

___Undead fortitude.___ If damage reduces the tyrannosaurus zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Multiattack.___ The tyrannosaurus zombie makes two attacks: one with its bite and one with it s tail. It can't make both attacks against the same target.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 33 (4d12 +7) piercing damage. If the target is a Medium or smaller creature, it is grappled (escape DC 17). Until this grapple ends, the target is restrained and the tyrannosaurus zombie can't bite another target or disgorg e zombies.

___Tail.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 20 (3d8 +7) bludgeoning damage.