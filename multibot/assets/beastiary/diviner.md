"Diviner";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "8 (3,900 XP)"
_languages_: "any four languages"
_skills_: "Arcana +7, History +7"
_saving_throws_: "Int +7, Wis +4"
_speed_: "30 ft."
_hit points_: "67 (15d8)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 11 (0) | 18 (+4) | 12 (+1) | 11 (0) |

___Spellcasting.___ The diviner is a 15th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). The diviner has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, light, mage hand, message, true strike_

* 1st level (4 slots): _detect magic\*, feather fall, mage armor_

* 2nd level (3 slots): _detect thoughts\*, locate object\*, scorching ray_

* 3rd level (3 slots): _clairvoyance\*, fly, fireball_

* 4th level (3 slots): _arcane eye\*, ice storm, stoneskin_

* 5th level (2 slots): _Rary's telepathic bond\*, seeming\* _

* 6th level (1 slot): _mass suggestion, true seeing\* _

* 7th level (1 slot): _delayed blast fireball, teleport_

* 8th level (1 slot): _maze_

*Divination spell of 1st level or higher

___Portent (Recharges after the Diviner Casts a Divination Spell of 1st Level or Higher).___ When the diviner or a creature it can see makes an attack roll, a saving throw, or an ability check, the diviner can roll a d20 and choose to use this roll in place of the attack roll, saving throw, or ability check.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d6-1) bludgeoning damage, or 3 (1d8-1) bludgeoning damage if used with two hands.
