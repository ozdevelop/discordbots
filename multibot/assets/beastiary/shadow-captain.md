"Shadow Captain";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "12 (8,400 XP)"
_languages_: "Common"
_skills_: "Perception +11, Stealth +10"
_senses_: "darkvision 60 ft., passive Perception 21"
_saving_throws_: "Str +8, Wis +7"
_damage_immunities_: "cold, necrotic, poison"
_damage_resistances_: "lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned, unconscious"
_speed_: "30 ft."
_hit points_: "156 (24d8 + 48)"
_armor class_: "19 (half plate, shield)"
_stats_: | 18 (+4) | 14 (+2) | 14 (+2) | 10 (+0) | 16 (+3) | 10 (+0) |

___Alter Self.___ The shadow captain
can assume a different form at will.
The magic that provides the shadow
captain with this ability is so powerful
that only a true seeing spell can discern
what lies under the illusion.

___Innate Spellcasting.___ The shadow captain’s spellcasting
ability is Wisdom (spell save DC 15, +7 to hit with spell
attacks). The shadow captain can innately cast the
following spells, requiring no material components:

* At will: _alter self, resistance, sacred flame_

* 3/day each: _bane, inflict wounds, shield of faith_

* 1/day each: _animate dead, contagion_

___Regeneration.___ The shadow captain regains 10 hit points at the start of
its turn. If the shadow captain takes acid or fire damage, this trait doesn’t
function at the start of its next turn. The shadow captain dies only if it
starts its turn with 0 hit points and doesn’t regenerate.

**Actions**

___Multiattack.___ The shadow captain makes two longsword attacks.

___Longsword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit:
15 (2d10 + 4) slashing damage plus 22 (4d10) cold damage.

___Draining Touch.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one
target. Hit: 15 (3d8 + 2) cold damage. The target must succeed on a DC
14 Constitution saving throw or its hit point maximum is reduced by an
amount equal to the damage taken. This reduction lasts until the target
finishes a long rest. The target dies if this effect reduces its hit point
maximum to 0.
