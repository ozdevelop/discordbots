"Mantidrake";;;_size_: Large monstrosity
_alignment_: lawful evil
_challenge_: "11 (7,200 XP)"
_languages_: "Common, Draconic"
_skills_: "Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +6, Con +9, Wis +6, Cha +5"
_damage_immunities_: "type associated with the dragon head color"
_speed_: "30 ft., fly 50 ft."
_hit points_: "147 (14d10 + 70)"
_armor class_: "15 (natural armor)"
_stats_: | 23 (+6) | 15 (+2) | 20 (+5) | 9 (-1) | 14 (+2) | 13 (+1) |

___Flyby.___ The mantidrake doesn’t provoke an opportunity attack when it
flies out of an enemy’s reach.

___Keen Smell.___ The mantidrake has advantage on Wisdom (Perception)
checks that rely on smell.

**Actions**

___Multiattack.___ The mantidrake makes three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 15
(2d8 + 6) piercing damage.

___Claws.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 15
(2d8 + 6) slashing damage.

___Spikes.___ Ranged Weapon Attack: +6 to hit, range 5 ft., one target. Hit: 16
(4d6 + 2) piercing damage.

___Dragon Breath (Recharge 5–6).___ The mantidrake exhales its breath
based on its dragon head color. Each creature in the area affected
must make a DC 18 Dexterity saving throw, taking 36 (8d8) damage on a
failed save, of half as much on a successful one.
