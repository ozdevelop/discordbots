"Gnasher Lizard";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "30 ft."
_hit points_: "152 (16d10 + 64)"
_armor class_: "16 (natural armor)"
_stats_: | 22 (+6) | 14 (+2) | 18 (+4) | 2 (-4) | 12 (+1) | 10 (+0) |

___Surprise Attack.___ If the cavern lizard surprises a creature and hits it with
an attack during the first round of combat, the target takes an extra 14
(4d6) damage from the attack.

**Actions**

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 22
(3d10 + 6) piercing damage and the target is grappled (escape DC 16), and
the gnasher lizard can only grapple one creature at a time.

___Thrash.___ The gnasher lizard rapidly shakes its head, and any grappled
target takes 39 (6d10 + 6) slashing damage. If this damage is enough to
drop the target to 0 hit points, the target is dismembered and portions of it
are swallowed. A creature who suffers this cannot be brought back to life
with anything less than true resurrection.
