"Elder Shadow Drake";;;_size_: Large dragon
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Draconic, Umbral"
_skills_: "Perception +5, Stealth +7"
_senses_: "darkvision 120 ft., passive Perception 15"
_saving_throws_: "Dex +4, Con +7, Cha +4"
_damage_vulnerabilities_: "radiant"
_damage_immunities_: "cold"
_condition_immunities_: "paralyzed, unconscious"
_speed_: "20 ft., fly 60 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "16 (natural armor)"
_stats_: | 22 (+6) | 13 (+1) | 18 (+4) | 8 (-1) | 9 (-1) | 13 (+1) |

___Shadow Blend.___ In areas of dim light or darkness, an elder shadow drake is treated as invisible. Artificial illumination, such as a lantern or a light or continual flame spell, does not negate this ability; nothing less than true sunlight or a daylight spell does. The drake cannot use its Speed Surge or its Stygian Breath while invisible. An elder shadow drake can suspend or resume this ability at will, so long as the drake is in dim light or darkness.

___Shadow Jump (3/Day).___ An elder shadow drake can travel between shadows as if by means of a dimension door spell. This magical transport must begin and end in an area of dim light or darkness, and the distance must be no more than 60 feet.

___Speed Surge (3/Day).___ The elder shadow drake takes one additional move action on its turn. It can use only one speed surge per round.

**Actions**

___Multiattack.___ The drake makes one bite attack and one tail slap attack.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 22 (3d10 + 6) piercing damage.

___Tail Slap.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 19 (3d8 + 6) bludgeoning damage.

___Stygian Breath (Recharge 5-6).___ The elder shadow drake exhales a ball of black liquid that travels up to 60 feet before exploding into a cloud of frigid black mist with a 20-foot radius. Each creature in that sphere takes 42 (12d6) cold damage, or half damage with a successful DC 15 Constitution saving throw. Within the area of effect, the mist snuffs out nonmagical light sources and dispels magical light of 1st level or lower.

