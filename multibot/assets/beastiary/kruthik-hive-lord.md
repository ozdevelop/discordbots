"Kruthik Hive Lord";;;_page_number_: 212
_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "5 (1800 XP)"
_languages_: "Kruthik"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 12"
_speed_: "40 ft., burrow 20 ft., climb 40 ft."
_hit points_: "102  (12d10 + 36)"
_armor class_: "20 (natural armor)"
_stats_: | 19 (+4) | 16 (+3) | 17 (+3) | 10 (0) | 14 (+2) | 10 (0) |

___Keen Smell.___ The kruthik has advantage on Wisdom (Perception) checks that rely on smell.

___Pack Tactics.___ The kruthik has advantage on an attack roll against a creature if at least one of the kruthik's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Tunneler.___ The kruthik can burrow through solid rock at half its burrowing speed and leaves a 10-foot-diameter tunnel in its wake.

**Actions**

___Multiattack___ The kruthik makes two stab attacks or two spike attacks.

___Stab___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 9 (1d10 + 4) piercing damage.

___Spike___ Ranged Weapon Attack: +6 to hit, range 30/120 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Acid Spray (Recharge 5-6)___ The kruthik sprays acid in a 15-foot cone. Each creature in that area must make a DC 14 Dexterity saving throw, taking 22 (4d10) acid damage on a failed save, or half as much damage on a successful one.