"Corlapis";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Con +5"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 17 (+3) | 13 (+1) | 10 (+0) | 10 (+0) |

___Hardened Exterior.___ Bludgeoning, piercing, and
slashing damage the corlapis takes from nonmagical
weapons is reduced by 3 to a minimum of 1.

___Stone Camouflage.___ The corlapis has advantage on
Dexterity (Stealth) checks to hide in rocky terrain.
Sturdy. The corlapis has advantage on saving throws
against effects that would cause it to move or be
knocked prone.

**Actions**

___Maul.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 10 (2d6 + 3) bludgeoning damage.
