"Iarno 'Glasstaff' Albrek";;;_size_: Medium humanoid
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic, Dwarvish, Elvish"
_skills_: "Arcana +5, History +5"
_senses_: "passive Perception 11"
_saving_throws_: "Int +5, Wis +3"
_speed_: "30 ft."
_hit points_: "22 (5d8)"
_armor class_: "12"
_stats_: | 9 (-1) | 14 (+2) | 11 (+0) | 17 (+3) | 12 (+1) | 11 (0) |

___Spellcasting.___ Iarno is a 4th level spellcaster who uses Intelligence as his 
spellcasting ability (spell save DC 13; +5 to hit with spell attacks). 
Iarno knows the following spells from the wizard's spell list:

* Cantrips (at will): _light, mage hand, shocking grasp_
* 1st Level (4 slots): _charm person, magic missile_
* 2nd Level (3 slots): _hold person, misty step_

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 3 (1d8 - 1) bludgeoning damage.
