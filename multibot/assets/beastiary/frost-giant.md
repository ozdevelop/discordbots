"Frost Giant";;;_size_: Huge giant
_alignment_: neutral evil
_challenge_: "8 (3,900 XP)"
_languages_: "Giant"
_skills_: "Athletics +9, Perception +3"
_damage_immunities_: "cold"
_saving_throws_: "Con +8, Wis +3, Cha +4"
_speed_: "40 ft."
_hit points_: "138 (12d12+60)"
_armor class_: "15 (patchwork armor)"
_stats_: | 23 (+6) | 9 (-1) | 21 (+5) | 9 (-1) | 10 (0) | 12 (+1) |

**Actions**

___Weighted Net.___ Ranged Weapon Attack: +5 to hit, ranged 20/60 ft., one Small, Medium, or Large creature. Hit: The target is restrained until it escapes the net. Any creature can use its action to make a DC 17 Strength check to free itself or another creature in the net, ending the effect on a success. Dealing 15 slashing damage to the net (AC 12) destroys the net and frees the target.

___Multiattack.___ The giant makes two greataxe attacks.

___Greataxe.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 25 (3d12 + 6) slashing damage.

___Rock.___ Ranged Weapon Attack: +9 to hit, range 60/240 ft., one target. Hit: 28 (4d10 + 6) bludgeoning damage.