"Voidling";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "11 (7200 XP)"
_languages_: "telepathy 60 ft."
_skills_: "Stealth +10"
_senses_: "truesight 60 ft., passive Perception 13"
_saving_throws_: "Con +4, Int +6, Wis +7, Cha +4"
_damage_immunities_: "necrotic"
_condition_immunities_: "exhaustion, petrified, prone"
_speed_: "0 ft., fly 50 ft. (hover)"
_hit points_: "110 (20d10)"
_armor class_: "16"
_stats_: | 15 (+2) | 22 (+6) | 10 (+0) | 14 (+2) | 16 (+3) | 10 (+0) |

___Fed by Darkness.___ A voidling in magical darkness at the start of its turn heals 5 hit points.

___Magic Resistance.___ The voidling has advantage on saving throws against spells and other magical effects except those that cause radiant damage.

___Innate Spellcasting.___ The voidling's innate spellcasting ability is Wisdom (spell save DC 15, spell attack bonus +7). It can innately cast the following spells, requiring no material components:

At will: darkness, detect magic, fear

3/day each: eldritch blast (3 beams), black tentacles

1/day each: phantasmal force, reverse gravity

___Natural Invisibility.___ A voidling in complete darkness is considered invisible to creatures that rely on normal vision or darkvision.

**Actions**

___Multiattack.___ The voidling makes 4 tendril attacks.

___Tendril.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 10 (1d8 + 6) slashing damage plus 11 (2d10) necrotic damage.

___Necrotic Burst (Recharge 5-6).___ The voidling releases a burst of necrotic energy in a 20-foot radius sphere centered on itself. Those in the area take 35 (10d6) necrotic damage, or half damage with a successful DC 17 Constitution saving throw.

