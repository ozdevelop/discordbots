"Drider Spellcaster";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +5, Stealth +9"
_speed_: "30 ft., climb 30 ft."
_hit points_: "123 (13d10+52)"
_armor class_: "19 (natural armor)"
_stats_: | 16 (+3) | 16 (+3) | 18 (+4) | 13 (+1) | 16 (+3) | 12 (+1) |

___Fey Ancestry.___ The drider has advantage on saving throws against being charmed, and magic can't put the drider to sleep.

___Innate Spellcasting.___ The drider's innate spellcasting ability is Wisdom (spell save DC 13). The drider can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire_

___Spider Climb.___ The drider can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Sunlight Sensitivity.___ While in sunlight, the drider has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Web Walker.___ The drider ignores movement restrictions caused by webbing.

___Spellcasting.___ The drider is a 7th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 14, +6 to hit with spell attacks). The drider has the following spells prepared from the cleric spell list:

* Cantrips (at will): _poison spray, thaumaturgy_

* 1st level (4 slots): _bane, detect magic, sanctuary_

* 2nd level (3 slots): _hold person, silence_

* 3rd level (3 slots): _clairvoyance, dispel magic_

* 4th level (2 slots): _divination,freedom of movement_

**Actions**

___Multiattack.___ The drider makes three attacks, either with its longsword or its longbow. It can replace one of those attacks with a bite attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 2 (1d4) piercing damage plus 9 (2d8) poison damage.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1d10 + 3) slashing damage if used with two hands.

___Longbow.___ Ranged Weapon Attack: +6 to hit, range 150/600 ft., one target. Hit: 7 (1d8 + 3) piercing damage plus 4 (1d8) poison damage.
