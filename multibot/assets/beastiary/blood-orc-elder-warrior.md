"Blood Orc Elder Warrior";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Orc"
_skills_: "Intimidation +1, Perception +1, Survival +1"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Str +8, Con +6"
_speed_: "40 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "14 (chain shirt)"
_stats_: | 21 (+5) | 13 (+1) | 16 (+3) | 8 (-1) | 6 (-2) | 6 (-2) |

___Bloodfrenzy.___ When the blood orc begins its turn with half or fewer of
its hit points, it can make a bite attack as a bonus action when it takes
the Attack action, and it has advantage on Intelligence, Wisdom, and
Charisma saving throws against spells and other effects, and the elder warrior has
resistance to bludgeoning, piercing, and slashing damage.

___Brute.___ A melee weapon deals one extra die of its damage when the
elder warrior hits with it (included in the attack).

**Actions**

___Multiattack.___ The elder warrior makes two melee attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 10
(2d4 + 5) piercing damage.

___Greataxe.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit:
18 (2d12 + 5) slashing damage.

___Terrorize (1/day).___ The elder warrior roars and displays its trophies,
which are visible to all creatures within 30 feet that can see it. Creatures
of the elder warrior’s choice within that area must make a DC 16 Wisdom
saving throw or be frightened of the warrior for 1 minute. While frightened,
they are paralyzed. A frightened creature can repeat the saving throw at
the end of each of its turns, ending the effect on a success.
