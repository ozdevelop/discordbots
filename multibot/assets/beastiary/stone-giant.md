"Stone Giant";;;_size_: Huge giant
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Giant"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +12, Perception +4"
_saving_throws_: "Dex +5, Con +8, Wis +4"
_speed_: "40 ft."
_hit points_: "126 (11d12+55)"
_armor class_: "17 (natural armor)"
_stats_: | 23 (+6) | 15 (+2) | 20 (+5) | 10 (0) | 12 (+1) | 9 (-1) |

___Stone Camouflage.___ The giant has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.

**Actions**

___Fling.___ The giant tries to throw a Small or Medium creature within 10 feet of it. The target must succeed on a DC 17 Dexterity saving throw or be hurled up to 60 feet horizontally in a direction of the giant's choice. and land prone, taking 1d6 bludgeoning damage for every 10 feet it was thrown.

___Rolling Rock.___ The giant sends a rock tumbling along the ground in a 30-foot line that is 5 feet wide. Each creature in that line must make a DC 17 Dexterity saving throw, taking 22 (3d10 + 6) bludgeoning damage and falling prone on a failed save

___Multiattack.___ The giant makes two greatclub attacks.

___Greatclub.___ Melee Weapon Attack: +9 to hit, reach 15 ft., one target. Hit: 19 (3d8 + 6) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +9 to hit, range 60/240 ft., one target. Hit: 28 (4d10 + 6) bludgeoning damage. If the target is a creature, it must succeed on a DC 17 Strength saving throw or be knocked prone.

**Reactions**

___Rock Catching.___ If a rock or similar object is hurled at the giant, the giant can, with a successful DC 10 Dexterity saving throw, catch the missile and take no bludgeoning damage from it.