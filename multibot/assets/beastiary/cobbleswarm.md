"Cobbleswarm";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: ", passive Perception 11"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, stunned"
_speed_: "30 ft."
_hit points_: "36 (8d8)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 11 (+0) | 11 (+0) | 5 (-3) | 12 (+1) | 5 (-3) |

___False Appearance.___ While the swarm remains motionless, it is indistinguishable from normal stones.

___Shift and Tumble.___ As a bonus action, the swarm can push a prone creature whose space it occupies 5 feet.

___Shifting Floor.___ Whenever the swarm moves into a creature's space or starts its turn in another creature's space, that other creature must make a successful DC 13 Dexterity saving throw or fall prone. A prone creature must make a successful DC 13 Dexterity (Acrobatics) check to stand up in a space occupied by the swarm.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny stone. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Stings.___ Melee Weapon Attack: +3 to hit, reach 0 ft., one target in the swarm's space. Hit: 10 (4d4) piercing damage, or 5 (2d4) piercing damage if the swarm has half its hit points or fewer.

