"Alhoon";;;_size_: Medium undead
_alignment_: any evil alignment
_challenge_: "10 (5,900 XP)"
_languages_: "Deep Speech, Undercommon, telepathy 120 ft."
_senses_: "truesight 120 ft."
_skills_: "Arcana +8, Deception +7, History +8, Insight +7, Perception +7, Stealth +5"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_saving_throws_: "Con +7, Int +8, Wis +7, Cha +7"
_speed_: "30 ft."
_hit points_: "120 (16d8+48)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_damage_resistances_: "cold, lightning, necrotic"
_stats_: | 11 (0) | 12 (+1) | 16 (+3) | 19 (+4) | 17 (+3) | 17 (+3) |

___Magic Resistance.___ The alhoon has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting (Psionics).___ The alhoon's innate spellcasting ability is Intelligence (spell save DC 16). It can innately cast the following spells, requiring no components:

* At will: _detect thoughts, levitate_

* 1/day each: _dominate monster, plane shift_ (self only)

___Spellcasting.___ The alhoon is a 12th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 16, +8 to hit with spell attacks). The alhoon has the following wizard spells prepared:

* Cantrips (at will): _chill touch, dancing lights, mage hand, prestidigitation, shocking grasp_

* 1st level (4 slots): _detect magic, disguise self, magic missile, shield_

* 2nd level (3 slots): _invisibility, mirror image, scorching ray_

* 3rd level (3 slots): _counterspell, fly, lightning bolt_

* 4th level (3 slots): _confusion, Evard's black tentacles, phantasmal killer_

* 5th level (2 slots): _modify memory, wall of force_

* 6th level (1 slot): _disintegrate, globe of invulnerability_

___Turn Resistance.___ The alhoon has advantage on saving throws against any effect that turns undead.

**Actions**

___Chilling Grasp.___ Melee Spell Attack: +8 to hit, reach 5 ft., one target. Hit: 10 (3d6) cold damage.

___Mind Blast (Recharge 5-6).___ The alhoon magically emits psychic energy in a 60-foot cone. Each creature in that area must succeed on a DC 16 Intelligence saving throw or take 22 (4d8+4) psychic damage and be stunned for 1 minute. A target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
