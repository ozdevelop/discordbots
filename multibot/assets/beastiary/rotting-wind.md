"Rotting Wind";;;_size_: Large undead
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft. (blind beyond this), passive Perception 10"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, grappled, frightened, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "0 ft., fly 60 ft. (hover)"
_hit points_: "82 (11d10 + 22)"
_armor class_: "15"
_stats_: | 14 (+2) | 20 (+5) | 15 (+2) | 7 (-2) | 12 (+1) | 10 (+0) |

___Air Form.___ The rotting wind can enter a hostile creature's space and stop there. It can move through a space as narrow as 1 inch wide without squeezing.

___Befouling Presence.___ All normal plant life and liquid in the same space as a rotting wind at the end of the wind's turn is blighted and cursed. Normal vegetation dies in 1d4 days, while plant creatures take double damage from the wind of decay action. Unattended liquids become noxious and undrinkable.

___Invisibility.___ The rotting wind is invisible as per a greater invisibility spell.

**Actions**

___Wind of Decay.___ Melee Weapon Attack: +8 to hit, reach 0 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage plus 14 (4d6) necrotic damage. If the target is a creature, it must succeed on a DC 15 Constitution saving throw or be cursed with tomb rot. The cursed target can't regain hit points, and its hit point maximum decreases by 10 (3d6) for every 24 hours that elapse. If the curse reduces the target's hit point maximum to 0, the target dies and its body turns to dust. The curse lasts until removed by the remove curse spell or comparable magic.

