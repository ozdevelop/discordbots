"Bard";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "2 (450 XP)"
_languages_: "any two languages"
_skills_: "Acrobatics +4, Perception +5, Performance +6"
_saving_throws_: "Dex +4, Wis +3"
_speed_: "30 ft."
_hit points_: "44 (8d8+8)"
_armor class_: "15 (chain shirt)"
_stats_: | 11 (0) | 14 (+2) | 12 (+1) | 10 (0) | 13 (+1) | 14 (+2) |

___Spellcasting.___ The bard is a 4th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). It has the following bard spells prepared:

* Cantrips (at will): _friends, mage hand, vicious mockery_

* 1st level (4 slots): _charm person, healing word, heroism, sleep, thunderwave_

* 2nd level (3 slots): _invisibility, shatter_

___Song of Rest.___ The bard can perform a song while taking a short rest. Any ally who hears the song regains an extra ld6 hit points if it spends any Hit Dice to regain hit points at the end of that rest. The bard can confer this benefit on itself as well.

___Taunt (2/day).___ The bard can use a bonus action on its turn to target one creature within 30 feet of it. If the target can hear the bard, the target must succeed on a DC 12 Charisma saving throw or have disadvantage on ability checks, attack rolls, and saving throws until the start of the bard's next turn.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) piercing damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one: target. Hit: 5 (1d6+2) piercing damage.
