"Aerisi Kalinoth";;;_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "Auran, Common, Elvish"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +6, History +6, Perception +3"
_speed_: "30 ft."
_hit points_: "66 (12d8+12)"
_armor class_: "13 (16 with mage armor)"
_damage_resistances_: "lightning"
_stats_: | 8 (-1) | 16 (+3) | 12 (+1) | 17 (+3) | 10 (0) | 16 (+3) |

___Fey Ancestry.___ Aerisi has advantage on saving throws against being charmed, and magic can't put her to sleep.

___Howling Defeat.___ When Aerisi drops to 0 hit points, her body disappears in a howling whirlwind that disperses quickly and harmlessly. Anything she is wearing or carrying is left behind.

___Legendary Resistance (2/Day).___ If Aerisi fails a saving throw, she can choose to succeed instead.

___Spellcasting.___ Aerisi is a 12th-level spellcaster. Her spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks). Aerisi has the following wizard spells prepared:

* Cantrips (at will): _gust, mage hand, message, prestidigitation, ray of frost, shocking grasp_

* 1st level (4 slots): _charm person, feather fall, mage armor, thunderwave_

* 2nd level (3 slots): _dust devil, gust of wind, invisibility_

* 3rd level (3 slots): _fly, gaseous form, lightning bolt_

* 4th level (3 slots): _ice storm, storm sphere_

* 5th level (2 slots): _cloudkill, seeming_ (cast each day)

* 6th level (1 slot): _chain lightning_

**Actions**

___Windvane.___ Melee or Ranged Weapon Attack: +9 to hit, reach 5 ft. or range 20 ft./60 ft., one target. Hit: 9 (1d6 + 6) piercing damage, or 10 (1d8 + 6) piercing damage if used with two hands to make a melee attack, plus 3 (1d6) lightning damage.
