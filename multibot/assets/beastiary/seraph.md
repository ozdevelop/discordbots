"Seraph";;;_size_: Large elemental
_alignment_: neutral good
_challenge_: "8 (3,900 XP)"
_languages_: "Celestial, Common, Ignan; telepathy 100 ft."
_skills_: "Arcana +5, Deception +7, Insight +5, Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Int +5, Wis +5"
_damage_immunities_: "fire"
_damage_vulnerabilities_: "cold"
_speed_: "40 ft., fly 30 ft."
_hit points_: "119 (14d10 + 42)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 16 (+3) | 17 (+3) | 15 (+2) | 15 (+2) | 19 (+4) |

___Heat.___ A seraph’s body generates heat. Creatures who touch the genie
take 7 (2d6) fire damage. If the seraph genie uses a metal weapon adds this
additional damage to the weapon’s attacks. A seraph genie can suppress
this effect for 1 hour as a bonus action.

___Innate Spellcasting.___ A seraph genie’s innate spellcasting ability is
charisma (spell save DC 15, +7 to hit with spell attacks). It can cast the
following spells without requiring material components.

* At will: _detect evil and good, detect magic, flame blade, plane shift _(self only)_, produce flame_

* 3/day each: _fireball, flame strike, invisibility, see invisibility, wall of fire_

* 1/day each: _fire storm, greater invisibility_

**Actions**

___Multiattack.___ A seraph genie makes three scimitar attacks.

___Scimitar.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7
(1d6 + 4) slashing damage plus 7 (2d6) fire damage.

___Fire Burst (Recharge 5–6).___ The seraph genie emits a blast of elemental
fire in a 30-foot radius. All creatures in the area must make a DC 15
Dexterity saving throw, taking 28 (8d6) fire damage on a failed saving
throw, or half as much on a successful one.
