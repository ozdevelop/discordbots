"Smoke Dragon";;;_size_: Small dragon
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic"
_skills_: "Nature +2, Perception +5, Stealth +5, Survival +3"
_senses_: "blindsight 10 ft., darkvision 30 ft., passive Perception 15"
_saving_throws_: "Dex +3, Con +3, Wis +3, Cha +2"
_damage_immunities_: "fire"
_condition_immunities_: "poisoned"
_speed_: "25 ft., fly 60 ft."
_hit points_: "27 (6d6 + 6)"
_armor class_: "14 (natural armor)"
_stats_: | 13 (+1) | 12 (+1) | 13 (+1) | 10 (+0) | 12 (+1) | 10 (+0) |

___Blending.___ The smoke dragon has advantage on Dexterity (Stealth)
checks made to hide within areas of smoke or fog.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) piercing damage.

___Claw.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4
(1d6 + 1) slashing damage.

___Smoke Breath (Recharge 5–6).___ The smoke dragon exhales a 20-foot
cone of thick, black smoke that spreads around corners. The area is heavily
obscured and the cloud of smoke remains for 1 minute or until dispersed
by a moderate or stronger wind (at least 10 miles per hour). Creatures that
enter or begin their turn in the area must succeed on a DC 11 Constitution
saving throw or be poisoned. While poisoned, the creature must succeed
on a DC 11 Constitution saving throw at the beginning of its turn, or
spend its turn coughing and retching, preventing the creature from
taking actions or moving. Once a poisoned creature succeeds on three
consecutive saving throws, it is no longer poisoned.

___Smoke Form (1/day).___ The smoke dragon polymorphs into a Medium
cloud of smoke. While in smoke form, the dragon can’t take any actions,
speak, or manipulate objects. It is weightless, has a flying speed of
30 feet, can hover, and can enter a hostile creature’s space and
stop there. In addition, if air can pass through a space, the mist
can do so without squeezing, and it can’t pass through water.
It has advantage on Strength, Dexterity, and Constitution
saving throws, it is immune to all nonmagical damage. The
transformation last for 1 hour unless the smoke dragon chooses
to end it earlier with a bonus action.
