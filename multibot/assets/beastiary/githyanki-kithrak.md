"Githyanki Kith'rak";;;_page_number_: 205
_size_: Medium humanoid (gith)
_alignment_: lawful evil
_challenge_: "12 (8400 XP)"
_languages_: "Gith"
_senses_: "passive Perception 16"
_skills_: "Intimidation +7, Perception +6"
_saving_throws_: "Con +7, Int +7, Wis +6"
_speed_: "30 ft."
_hit points_: "180  (24d8 + 72)"
_armor class_: "18 (plate)"
_stats_: | 18 (+4) | 16 (+3) | 17 (+3) | 16 (+3) | 15 (+2) | 17 (+3) |

___Innate Spellcasting (Psionics).___ The githyanki's innate spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _blur, jump, misty step, nondetection _(self only)

* 1/day each: _plane shift, telekinesis_

___Rally the Troops.___ As a bonus action, the githyanki can magically end the charmed and frightened conditions on itself and each creature of its choice that it can see within 30 feet of it.

**Actions**

___Multiattack___ The githyanki makes three greatsword attacks.

___Greatsword___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 17 (5d6) psychic damage.
