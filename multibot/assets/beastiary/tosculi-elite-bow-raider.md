"Tosculi Elite Bow Raider";;;_size_: Medium humanoid
_alignment_: lawful evil
_challenge_: "5 (1800 XP)"
_languages_: "Gnoll, Infernal, Tosculi"
_skills_: "Perception +6, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 16"
_speed_: "30 ft., fly 60 ft."
_hit points_: "97 (13d8 + 39)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 18 (+4) | 17 (+3) | 12 (+1) | 14 (+2) | 12 (+1) |

___Deadly Precision.___ The tosculi elite bow raider's ranged attacks do an extra 9 (2d8) damage (included below).

___Evasive.___ Ranged weapon attacks against the tosculi elite bow raider have disadvantage.

___Keen Smell.___ The tosculi elite bow raider has advantage on Wisdom (Perception) checks that rely on smell.

___Skirmisher.___ The tosculi elite bow raider can Dash as a bonus action.

**Actions**

___Multiattack.___ The tosculi elite bow raider makes two longbow attacks or two claws attacks.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

___Longbow.___ Ranged Weapon Attack: +7 to hit, range 150/600 ft., one target. Hit: 17 (3d8 + 4) piercing damage.

