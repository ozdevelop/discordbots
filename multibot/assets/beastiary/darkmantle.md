"Darkmantle";;;_size_: Small monstrosity
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 60 ft."
_skills_: "Stealth +3"
_speed_: "10 ft., fly 30 ft."
_hit points_: "22 (5d6+5)"
_armor class_: "11"
_stats_: | 16 (+3) | 12 (+1) | 13 (+1) | 2 (-4) | 10 (0) | 5 (-3) |

___Echolocation.___ The darkmantle can't use its blindsight while deafened.

___False Appearance.___ While the darkmantle remains motionless, it is indistinguishable from a cave formation such as a stalactite or stalagmite.

**Actions**

___Crush.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 6 (1d6 + 3) bludgeoning damage, and the darkmantle attaches to the target. If the target is Medium or smaller and the darkmantle has advantage on the attack roll, it attaches by engulfing the target's head, and the target is also blinded and unable to breathe while the darkmantle is attached in this way.

While attached to the target, the darkmantle can attack no other creature except the target but has advantage on its attack rolls. The darkmantle's speed also becomes 0, it can't benefit from any bonus to its speed, and it moves with the target.

A creature can detach the darkmantle by making a successful DC 13 Strength check as an action. On its turn, the darkmantle can detach itself from the target by using 5 feet of movement.

___Darkness Aura (1/day).___ A 15-foot radius of magical darkness extends out from the darkmantle, moves with it, and spreads around corners. The darkness lasts as long as the darkmantle maintains concentration, up to 10 minutes (as if concentrating on a spell). Darkvision can't penetrate this darkness, and no natural light can illuminate it. If any of the darkness overlaps with an area of light created by a spell of 2nd level or lower, the spell creating the light is dispelled.