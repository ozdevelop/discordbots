"Ash Drake";;;_size_: Small dragon
_alignment_: neutral evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Draconic"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 12"
_saving_throws_: "Dex +4"
_damage_resistances_: "fire"
_condition_immunities_: "paralyzed, unconscious"
_speed_: "30 ft., fly 60 ft."
_hit points_: "117 (18d6 + 54)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 15 (+2) | 16 (+3) | 9 (-1) | 15 (+2) | 10 (+0) |

**Actions**

___Multiattack.___ The ash drake makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) piercing damage + 3 (1d6) fire damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage.

___Ash Cloud.___ An ash drake can beat its wings and create a cloud of ash that extends 10 feet in all directions, centered on itself. This cloud provides half cover, though the ash drake can see normally through its own cloud. Any creature that enters or starts its turn in the cloud must succeed on a DC 14 Constitution saving throw or become blinded for 1d6 rounds.

___Ash Breath (recharge 6).___ An ash drake spews a 20-foot cone of blistering hot, choking ash. Any targets in the path of this spray takes 14 (4d6) fire damage and become poisoned for one minute; a successful DC 13 Dexterity saving throw reduces damage by half and negates the poisoning. A poisoned creature repeats the saving throw at the end of each of its turns, ending the effect on itself with a successful save.

