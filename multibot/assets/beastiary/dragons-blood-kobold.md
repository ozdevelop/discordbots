"Dragon's Blood Kobold";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_resistances_: "see <i>Dragon’s Blood</i> below"
_speed_: "30 ft."
_hit points_: "18 (4d6 + 4)"
_armor class_: "13"
_stats_: | 8 (-1) | 16 (+3) | 12 (+1) | 8 (-1) | 7 (-2) | 8 (-1) |

___Sunlight Sensitivity.___ While in sunlight, the kobold
has disadvantage on attack rolls, as well as on
Wisdom (Perception) checks that rely on sight.

___Pack Tactics.___ The kobold has advantage on an attack
roll against a creature if at least one of the kobold’s
allies is within 5 feet of the creature and the ally
isn’t incapacitated.

___Dragon’s Blood.___ This kobold is a servant of a
chromatic dragon and has taken on some of its
characteristics. It gains resistance to one type of
damage and deals additional damage on each attack
depending on the dragon’s color as shown below.
* ___Black:___ Acid
* ___Blue:___ Lightning
* ___Green:___ Poison
* ___Red:___ Fire
* ___White:___ Cold

**Actions**

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 5 (1d4 + 3) slashing damage plus 2
(1d4) elemental damage (See _Dragon's Blood_ above).

___Sling.___ Ranged Weapon Attack: +5 to hit, range
30/120 ft., one target. Hit: 5 (1d4 + 3)
bludgeoning damage.
