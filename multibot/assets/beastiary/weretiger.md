"Weretiger";;;_size_: Medium humanoid (human shapechanger)
_challenge_: "4 (1,100 XP)"
_languages_: "Common (can't speak in tiger form)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +5, Stealth +4"
_damage_immunities_: "bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_speed_: "30 ft. (40 ft. in tiger form)"
_hit points_: "120 (16d8+48)"
_armor class_: "12"
_stats_: | 17 (+3) | 15 (+2) | 16 (+3) | 10 (0) | 13 (+1) | 11 (0) |

___Shapechanger.___ The weretiger can use its action to polymorph into a tiger-humanoid hybrid or into a tiger, or back into its true form, which is humanoid. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Keen Hearing and Smell.___ The weretiger has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Pounce (Tiger or Hybrid Form Only).___ If the weretiger moves at least 15 feet straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 14 Strength saving throw or be knocked prone. If the target is prone, the weretiger can make one bite attack against it as a bonus action.

**Actions**

___Multiattack (Humanoid or Hybrid Form Only).___ In humanoid form, the weretiger makes two scimitar attacks or two longbow attacks. In hybrid form, it can attack like a humanoid or make two claw attacks.

___Bite (Tiger or Hybrid Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage. If the target is a humanoid, it must succeed on a DC 13 Constitution saving throw or be cursed with weretiger lycanthropy.

___Claw (Tiger or Hybrid Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage.

___Scimitar (Humanoid or Hybrid Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Longbow (Humanoid or Hybrid Form Only).___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.