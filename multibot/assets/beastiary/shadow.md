"Shadow";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +4"
_damage_immunities_: "necrotic, poison"
_speed_: "40 ft."
_hit points_: "16 (3d8+3)"
_armor class_: "12"
_damage_vulnerabilities_: "radiant"
_condition_immunities_: "exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, cold, fire, lightning, thunder, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 6 (-2) | 14 (+2) | 13 (+1) | 6 (-2) | 10 (0) | 8 (-1) |

___Amorphous.___ The shadow can move through a space as narrow as 1 inch wide without squeezing.

___Shadow Stealth.___ While in dim light or darkness, the shadow can take the Hide action as a bonus action. Its stealth bonus is also improved to +6.

___Sunlight Weakness.___ While in sunlight, the shadow has disadvantage on attack rolls, ability checks, and saving throws.

**Actions**

___Strength Drain.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 9 (2d6 + 2) necrotic damage, and the target's Strength score is reduced by 1d4. The target dies if this reduces its Strength to 0. Otherwise, the reduction lasts until the target finishes a short or long rest.

If a non-evil humanoid dies from this attack, a new shadow rises from the corpse 1d4 hours later.