"Young Emerald Dragon";;;_size_: Large dragon
_alignment_: chaotic neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Insight +5, Perception +5"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 18"
_saving_throws_: "Dex +6, Int +6, Wis +5, Cha +6"
_damage_vulnerabilities_: "psychic"
_speed_: "40 ft., fly 80 ft. (hover), swim 40 ft."
_hit points_: "91 (14d10 + 14)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 16 (+3) | 12 (+1) | 16 (+3) | 14 (+2) | 16 (+3) |

___Interference Aura.___ Enemies within 30 feet must
make a DC 14 Intelligence saving throw every
round to maintain spells that require concentration.

**Psionics**

___Charges:___ 14 | ___Recharge:___ 1d6 | ___Fracture:___ 14

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft.,
one target. Hit: 14 (2d10 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 10 (2d6 + 3) slashing damage.
