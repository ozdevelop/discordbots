"Pterafolk";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_Languages_: "Common"
_senses_: "passive Perception 12"
_skills_: "Perception +2, Survival +2"
_speed_: "30 ft., fly 50 ft."
_hit points_: "26 (4d10 +4)"
_armor class_: "12 (natural armor)"
_stats_: | 15 (+2) | 13 (+1) | 12 (+1) | 9 (-1) | 10 (0) | 11 (0) |

___Terror Dive.___ If the pterafolk is flying and dives at least 30 feet straight toward a target, and then hits that target with a melee weapon attack, the target is frightened until the end of its next turn.

**Actions**

___Multiattack.___ The pterafolk makes three attacks: one with its bite and two with its claws. Alternatively, it makes two melee attacks with its javelin.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 +2) piercing damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (ld6 +2) slashing damage.

___Javelin.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 9 (2d6 +2) piercing damage.