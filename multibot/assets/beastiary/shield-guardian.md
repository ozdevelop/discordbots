"Shield Guardian";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "7 (2,900 XP)"
_languages_: "understands commands given in any language but can't speak"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "142 (15d10+60)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_stats_: | 18 (+4) | 8 (-1) | 18 (+4) | 7 (-2) | 10 (0) | 3 (-4) |

___Bound.___ The shield guardian is magically bound to an amulet. As long as the guardian and its amulet are on the same plane of existence, the amulet's wearer can telepathically call the guardian to travel to it, and the guardian knows the distance and direction to the amulet. If the guardian is within 60 feet of the amulet's wearer, half of any damage the wearer takes (rounded up) is transferred to the guardian.

___Regeneration.___ The shield guardian regains 10 hit points at the start of its turn if it has at least 1 hit. point.

___Spell Storing.___ A spellcaster who wears the shield guardian's amulet can cause the guardian to store one spell of 4th level or lower. To do so, the wearer must cast the spell on the guardian. The spell has no effect but is stored within the guardian. When commanded to do so by the wearer or when a situation arises that was predefined by the spellcaster, the guardian casts the stored spell with any parameters set by the original caster, requiring no components. When the spell is cast or a new spell is stored, any previously stored spell is lost.

**Actions**

___Multiattack.___ The guardian makes two fist attacks.

___Fist.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.

**Reactions**

___Shield.___ When a creature makes an attack against the wearer of the guardian's amulet, the guardian grants a +2 bonus to the wearer's AC if the guardian is within 5 feet of the wearer.