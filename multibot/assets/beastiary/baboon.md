"Baboon";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_speed_: "30 ft., climb 30 ft."
_hit points_: "3 (1d6)"
_armor class_: "12"
_stats_: | 8 (-1) | 14 (+2) | 11 (0) | 4 (-3) | 12 (+1) | 6 (-2) |

___Pack Tactics.___ The baboon has advantage on an attack roll against a creature if at least one of the baboon's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Bite.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 1 (1d4 - 1) piercing damage.