"Needle Blight";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "understands Common but can't speak"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_speed_: "30 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "12 (natural armor)"
_condition_immunities_: "blinded, deafened"
_stats_: | 12 (+1) | 12 (+1) | 13 (+1) | 4 (-3) | 8 (-1) | 3 (-4) |

**Actions**

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) piercing damage.

___Needles.___ Ranged Weapon Attack: +3 to hit, range 30/60 ft., one target. Hit: 8 (2d6 + 1) piercing damage.