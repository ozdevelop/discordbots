"Bleeding Horror Minotaur";;;_size_: Large undead
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Giant"
_skills_: "Perception +6, Survival +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +3, Con +5, Wis +3"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "75 (10d10 + 20)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 10 (+0) | 15 (+2) | 7 (-2) | 10 (+0) | 16 (+3) |

___Charge.___ If the minotaur moves at least 10 feet straight toward a target
and then hits it with a gore attack on the same turn, the target takes an
extra 9 (2d8) piercing damage. If the target is a creature, it must succeed
on a DC 15 Strength saving throw or be pushed up to 10 feet away and
knocked prone.

___Horrific Appearance.___ All creatures who directly look at the minotaur
must make a DC 14 Wisdom saving throw or be frightened for 1 minute. A
creature can repeat the saving throw at the end of each of its turns, ending
the effect on itself on a success. If a creature’s saving throw is successful
or the effect ends for it, the creature is immune to the minotaur’s horrific
appearance for the next 24 hours.

___Magic Resistance.___ The minotaur has advantage on saving throws
against spells and other magic effects.

___Magic Weapons.___ The minotaur’s attacks are magical.

**Actions**

___Multiattack.___ The bleeding horror minotaur uses its gore attack, and
makes one attack with its claw and one with its greataxe, or two with its
claws.

___Gore.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 13
(2d8 + 4) piercing damage.

___Greataxe.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit:
11 (2d6 + 4) slashing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 13
(2d8 + 4) slashing damage.
