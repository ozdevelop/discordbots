"Black Orc";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft., passive Perception 10"
_skills_: "Intimidation +3"
_speed_: "30 ft."
_hit points_: "16 (3d8 + 3)"
_armor class_: "15 (scale mail)"
_stats_: | 14 (+2) | 12 (+1) | 12 (+1) | 9 (-1) | 10 (+0) | 9 (-1) |

___Blessing of Orcus.___ Black orcs have advantage on saving throws against
the spells and effects of undead creatures.

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 5 (1d6 + 2) piercing damage, or 6 (1d8 + 2)
piercing damage when used with two hands to make a melee attack.

___Light Crossbow.___ Ranged Weapon Attack: +3 to hit, range 80/320 ft.,
one target. Hit: 5 (1d8 + 1) piercing damage.
