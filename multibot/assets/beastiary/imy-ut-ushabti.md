"Imy-Ut Ushabti";;;_size_: Medium monstrosity
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Common (Ancient Nurian)"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Wis +2"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "bludgeoning"
_condition_immunities_: "exhaustion, frightened"
_speed_: "30 ft."
_hit points_: "97 (15d8 + 30)"
_armor class_: "15 (chain shirt)"
_stats_: | 17 (+3) | 14 (+2) | 15 (+2) | 6 (-2) | 10 (+0) | 5 (-3) |

___Regeneration.___ The imy-ut ushabti regains 5 hit points at the start of its turn if it has at least 1 hit point.

___Rent wrappings.___ A creature that touches or deals slashing or piercing damage to an imy-ut ushabti while within 5 feet of the creature shreds its delicate linen wrappings, releasing a flurry of skittering scarabs. The attacking creature must make a DC 12 Dexterity saving throw to avoid them. On a failure, these beetles flow onto the attacker and deal 3 (1d6) piercing damage to it at the start of each of its turns. A creature can remove beetles from itself or from another affected creature within reach by using an action and making a successful DC 12 Dexterity saving throw. The beetles are also destroyed if the affected creature takes damage from an area effect.

**Actions**

___Ceremonial Greatsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 10 (2d6 + 3) slashing damage, and the target must make a successful DC 13 Constitution saving throw or take 5 (2d4) poison damage at the start of each of its turns. The target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Vomit Swarm (1/Day).___ The imy-ut ushabti parts its wrappings voluntarily and releases a swarm of scarab beetles that follow its mental commands. The statistics of this swarm are identical to a swarm of insects, but with the following attack instead of a swarm of insects' standard bite attack:

___Bites.___  Melee Weapon Attack: +3 to hit, reach 0 ft., one creature. Hit: 10 (4d4) piercing damage, or 5 (2d4) piercing damage if the swarm has half of its hit points or fewer, and the target must make a successful DC 13 Constitution saving throw or take 5 (2d4) poison damage at the start of each of its turns. A poisoned creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

