"Thurl Merosska";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Auran, Common"
_skills_: "Animal Handling +2, Athletics +5, Deception +4, Persuasion +4"
_speed_: "30 ft."
_hit points_: "71 (11d8+22)"
_armor class_: "16 (breastplate)"
_stats_: | 16 (+3) | 14 (+2) | 14 (+2) | 11 (0) | 10 (0) | 15 (+2) |

___Spellcasting.___ Thurl is a 5th-level spellcaster. His spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). Thurl knows the following sorcerer spells:

* Cantrips (at will): _friends, gust, light, message, ray of frost_

* 1st level (4 slots): _expeditious retreat, feather fall, jump_

* 2nd level (3 slots): _levitate, misty step_

* 3rd level (2 slots): _haste_

**Actions**

___Multiattack.___ Thurl makes two melee attacks.

___Greatsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage.

___Lance.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 9 (1d12 + 3) piercing damage.

**Reactions**

___Parry.___ Thurl adds 2 to his AC against one melee attack that would hit him. To do so, Thurl must see the attacker and be wielding a melee weapon.
