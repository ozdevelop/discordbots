"Young Wind Dragon";;;_size_: Large dragon
_alignment_: chaotic neutral
_challenge_: "6 (2300 XP)"
_languages_: "Common, Draconic, Primordial"
_skills_: "Perception +7, Stealth +7"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 17"
_saving_throws_: "Dex +7, Con +7, Wis +4, Cha +6"
_damage_immunities_: "lightning"
_condition_immunities_: "charmed, exhausted, paralyzed, restrained"
_speed_: "40 ft., fly 90 ft."
_hit points_: "150 (16d10 + 62)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 19 (+4) | 18 (+4) | 14 (+2) | 13 (+1) | 16 (+3) |

___Innate Spellcasting.___ The dragon's innate spellcasting ability is Charisma (spell save DC 14). It can innately cast the following spell, requiring no material components:

At will: feather fall

___Fog Vision.___ The dragon sees normally through light or heavy obscurement caused by fog, mist, clouds, or high wind.

___Uncontrollable.___ The dragon's movement is never impeded by difficult terrain, and its speed can't be reduced by spells or magical effects. It can't be restrained (per the condition), and it escapes automatically from any nonmagical restraints (such as chains, entanglement, or grappling) by spending 5 feet of movement. Being underwater imposes no penalty on its movement or attacks.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

___Breath of Gales (Recharge 5-6).___ The dragon exhales a blast of wind in a 30-foot cone. Each creature in that cone takes 11 (2d10) bludgeoning damage and is pushed 25 feet away from the dragon and knocked prone; a successful DC 16 Strength saving throw halves the damage and prevents being pushed and knocked prone. Unprotected flames in the cone are extinguished, and sheltered flames (such as those in lanterns) have a 75 percent chance of being extinguished.

