"Orog";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +5, Survival +2"
_speed_: "30 ft."
_hit points_: "42 (5d8+20)"
_armor class_: "18 (plate)"
_stats_: | 18 (+4) | 12 (+1) | 18 (+4) | 12 (+1) | 11 (0) | 12 (+1) |

___Aggressive.___ As a bonus action, the orog can move up to its speed toward a hostile creature that it can see.

**Actions**

___Multiattack.___ The orog makes two greataxe attacks.

___Greataxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (1d12 + 4) slashing damage.

___Javelin.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 7 (1d6 + 4) piercing damage.