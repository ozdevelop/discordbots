"Rift Swine";;;_size_: Large aberration
_alignment_: chaotic neutral
_challenge_: "5 (1800 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_resistances_: "force, poison"
_speed_: "40 ft."
_hit points_: "110 (13d10 + 39)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 17 (+3) | 4 (-3) | 12 (+1) | 5 (-3) |

___360-Degree Vision.___ The rift swine's extra eyes give it advantage on Wisdom (Perception) checks that rely on sight.

___Chaos mutations.___ 50**Actions**

___Multiattack.___ The rift swine makes one tusks attack and two tentacle attacks.

___Tusks.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

___Tentacle.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage. If the target is a creature, it is grappled (escape DC 14). Until this grapple ends, the target is restrained, and the rift swine can't use this tentacle against another target.

