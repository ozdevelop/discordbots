"Lizard";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 30 ft."
_speed_: "20 ft., climb 20 ft."
_hit points_: "2 (1d4)"
_armor class_: "10"
_stats_: | 2 (-4) | 11 (0) | 10 (0) | 1 (-5) | 8 (-1) | 3 (-4) |

**Actions**

___Bite.___ Melee Weapon Attack: +0 to hit, reach 5 ft., one target. Hit: 1 piercing damage.