"Labyrinth Crawler";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 60ft., passive Perception 11"
_speed_: "40 ft."
_hit points_: "37 (5d8 + 15)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 16 (+3) | 5 (-3) | 12 (+1) | 6 (-2) |

___Labyrinthine Recall.___ The crawler can perfectly recall
any path it has traveled.

___Flawless Tracker.___ The crawler can track down any
creature it has smelled in the last 24 hours unless
that creature's location is concealed through
magical means.

**Actions**

___Gore.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 8 (1d8 + 4) piercing damage.

___Befuddling Screech (1/Day).___ The crawler lets out an
ear piercing screech in a 30-foot cone that
reverberates in frequencies that cause the minds of
most creatures to become scrambled. Each
creature in this cone must succeed on a DC 11
Wisdom saving throw or take 5 (1d10) psychic
damage and lose its sense of time and direction for
1 hour.
