"Xhkarsh";;;_size_: Large aberration
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "Common, Deep Speech, Undercommon"
_skills_: "Insight + 6, Perception +6, Stealth +8"
_senses_: "darkvision 60 ft., tremorsense 120 ft., passive Perception 16"
_saving_throws_: "Cha +5"
_speed_: "50 ft., climb 30 ft."
_hit points_: "133 (14d10 + 56)"
_armor class_: "19 (natural and mystic armor)"
_stats_: | 17 (+3) | 21 (+5) | 18 (+4) | 15 (+2) | 16 (+3) | 15 (+2) |

**Actions**

___Multiattack.___ The xhkarsh makes two claw attacks and two stinger attacks.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

___Stinger.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 12 (2d6 + 5) piercing damage, and the target must succeed on a DC 15 Charisma saving throw or have its fate corrupted. A creature with corrupted fate has disadvantage on Charisma checks and Charisma saving throws, and it is immune to divination spells and to effects that sense emotions or read thoughts. The target's fate can be restored by a dispel evil and good spell or comparable magic.

___Seize Strand.___ The xhkarsh targets one creature within 5 feet of it whose fate has been corrupted. The target creature must succeed on a DC 15 Charisma saving throw or a portion of the xhkarsh's consciousness inhabits its body. The target retains control of its body, but the xhkarsh can control its actions for 1 minute each day and can modify its memories as a bonus action (as if using the modify memory spell, DC 15). The target is unaware of the xhkarsh's presence, but can make a DC 18 Wisdom (Insight) check once every 24 hours to notice the presence of the xhkarsh. This effect lasts until the xhkarsh ends it or the target's fate is restored by a dispel evil and good spell or comparable magic. A creature becomes immune to this effect for 24 hours when it succeeds on the saving throw to resist the effect or after the effect ends on it for any reason. A single xhkarsh can seize up to four strands at the same time.

___Invisibility.___ The xhkarsh turns invisible until it attacks or casts a spell, or until its concentration ends. Equipment the xhkarsh wears or carries becomes invisible with it.

