"Olhydra";;;_size_: Huge elemental
_alignment_: neutral evil
_challenge_: "18 (20,000 XP)"
_languages_: "Aquan"
_senses_: "blindsight 120 ft."
_damage_immunities_: "acid, cold, poison"
_saving_throws_: "Str +11, Con +13, Wis +10"
_speed_: "50 ft., swim 100 ft."
_hit points_: "324 (24d12+168)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 21 (+5) | 22 (+6) | 24 (+7) | 17 (+3) | 18 (+4) | 23 (+6) |

___Empowered Attacks.___ Olhydra's slam attacks are treated as magical for the purpose of bypassing resistance and immunity to nonmagical weapons.

___Innate Spellcasting.___ Olhydra's innate spellcasting ability is Charisma (spell save DC 20, +12 to hit with spell attacks). She can innately cast the following spells, requiring no material components:

* At will: _wall of ice_

* 3/day: _ice storm_

* 1/day: _storm of vengeance_

___Legendary Resistance (3/Day).___ If Olhydra fails a saving throw, she can choose to succeed instead.

___Magic Resistance.___ Olhydra has advantage on saving throws against spells and other magical effects.

___Water Form.___ Olhydra can enter a hostile creature's space and stop there. She can move through a space as narrow as 1 inch wide without squeezing.

**Actions**

___Multiattack.___ Olhydra makes two slam attacks or two water jet attacks.

___Slam.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 21 (3d10 + 5) bludgeoning damage, and the target is grappled (escape DC 19). Olhydra can grapple up to four targets. When Olhydra moves, all creatures she is grappling move with her.

___Water Jet.___ Ranged Weapon Attack: +12 to hit, range 120 ft., one target. Hit: 21 (6d6) bludgeoning damage, and the target is knocked prone if it fails a DC 19 Strength saving throw.

___Summon Elementals (1/Day).___ Olhydra summons up to three water elementals and loses 30 hit points for each elemental she summons. Summoned elementals have maximum hit points, appear within 100 feet of Olhydra, and disappear if Olhydra is reduced to 0 hit points.

**Legendary** Actions

The olhydra can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The olhydra regains spent legendary actions at the start of its turn.

___Crush.___ One creature that Olhydra is grappling is crushed for 21 (3d10 + 5) bludgeoning damage.

___Fling (Costs 2 Actions).___ Olhydra releases one creature she is grappling by flinging the creature up to 60 feet away from her, in a direction of her choice. If the flung creature comes into contact with a solid surface, such as a wall or floor, the creature takes 1d6 bludgeoning damage for every 10 feet it was flung.

___Water to Acid (Costs 3 Actions).___ Olhydra transforms her watery body into acid. This effect lasts until Olhydra's next turn. Any creature that comes into contact with Olhydra or hits her with a melee attack while standing within 5 feet of her takes 11 (2d10) acid damage. Any creature grappled by Olhydra takes 22 (4d10) acid damage at the start of its turn.
