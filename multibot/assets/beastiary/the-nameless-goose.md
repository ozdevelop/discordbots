"The Nameless Goose";;;_size_: Small fiend
_alignment_: chaotic neutral
_challenge_: "7 (3,900 XP)"
_languages_: "Infernal, Abyssal, Deep Speech, Goose"
_skills_: "Intimidation +7, Sleight of Hand +8, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +5 Wis +4, Cha +7"
_damage_immunities_: "psychic"
_condition_immunities_: "charmed, frightened, grappled, prone"
_speed_: "25 ft., fly 60 ft., swim 60 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (+0) | 14 (+2) | 15 (+2) | 12 (+1) | 12 (+1) | 18 (+4) |

___Innate Spellcasting.___ The goose’s innate spellcasting ability is Charisma. It can innately cast the following spells (spell save DC 15), requiring no material or somatic components:

* At will: _arcane lock, shatter, fear, thunderous smite, wrathful smite_

* 1/day each: _divine word, etherealness, power word stun_

___Evasive.___ When the goose is subjected to an effect that allows it to make a Dexterity saving throw to take only half damage, it instead takes no damage if it succeeds on the saving throw, and only half damage if it fails.

___Nimble Escape.___ The goose can take the Disengage or Hide action as a bonus action on each of its turns.

**Actions**

___Multiattack.___ The goose uses its Honk, then makes three attacks with its Beak.

___Beak.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage plus 10 (3d6) psychic damage.

___Honk.___ All creatures of the goose's choice within 120 ft. that can hear it must make a DC 15 Wisdom saving throw. On a fail, the creature is enraged, having disadvantage on attacks against any target other than the goose until the end of the goose's next turn.
