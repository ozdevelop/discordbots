"Shoosuva";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "Abyssal, Gnoll, telepathy 120 ft."
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Dex +4, Con +6, Wis +5"
_speed_: "40 ft."
_hit points_: "110 (13d10+39)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 18 (+4) | 13 (+1) | 17 (+3) | 7 (-2) | 14 (+2) | 9 (-1) |

___Rampage.___ When it reduces a creature to 0 hit points with a melee attack on its turn, the shoosuva can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Multiattack.___ The shoosuva makes two attacks: one with its bite and one with its tail stinger.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft, one target. Hit: 26 (4d10+4) piercing damage.

___Tail Stinger.___ Melee Weapon Attack: +7 to hit, reach 15 ft, one creature. Hit: 13 (2d8+4) piercing damage, and the target must succeed on a DC 14 Constitution saving throw or become poisoned. While poisoned, the target is also paralyzed. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.