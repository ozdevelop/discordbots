"Grimlock";;;_size_: Medium humanoid (grimlock)
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Undercommon"
_senses_: "blindsight 30 ft. or 10 ft. while deafened (blind beyond this radius)"
_skills_: "Athletics +5, Perception +3, Stealth +3"
_damage_immunities_: "blinded"
_speed_: "30 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "11"
_stats_: | 16 (+3) | 12 (+1) | 12 (+1) | 9 (-1) | 8 (-1) | 6 (-2) |

___Blind Senses.___ The grimlock can't use its blindsight while deafened and unable to smell.

___Keen Hearing and Smell.___ The grimlock has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Stone Camouflage.___ The grimlock has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.

**Actions**

___Spiked Bone Club.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) bludgeoning damage plus 2 (1d4) piercing damage.