"Night Scorpion";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft., passive Perception 9"
_speed_: "40 ft."
_hit points_: "90 (12d10 + 24)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 14 (+2) | 1 (-5) | 9 (-1) | 3 (-4) |

**Actions**

___Multiattack.___ The scorpion makes three attacks: two with its claws and one with its sting.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) bludgeoning damage, and the target is grappled (escape DC 12). The scorpion has two claws, each of which can grapple one target.

___Sting.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 7 (1d10 + 2) piercing damage, and the target takes 7 (2d6) poison damage and is blinded for 1d3 hours; a successful DC 12 Constitution saving throw reduces damage by half and prevents blindness. If the target fails the saving throw by 5 or more, the target is also paralyzed while poisoned. The poisoned target can repeat the saving throw at the end of each of its turns, ending the effect on itself with a successful save.

