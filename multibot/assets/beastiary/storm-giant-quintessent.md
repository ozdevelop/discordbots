"Storm Giant Quintessent";;;_size_: Huge giant (storm giant)
_alignment_: chaotic good
_challenge_: "16 (15,000 XP)"
_languages_: "Common, Giant"
_senses_: "truesight 60 ft."
_skills_: "Arcana +8, History +8, Perception +10"
_damage_immunities_: "lightning, thunder"
_saving_throws_: "Str +14, Con +10, Wis +10, Cha +9"
_speed_: "50 ft., fly 50 ft. (hover), swim 50 ft."
_hit points_: "230 (20d12+100)"
_armor class_: "12"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 29 (+9) | 14 (+2) | 20 (+5) | 17 (+3) | 30 (+10) | 19 (+4) |

___Amphibious.___ The giant can breathe air and water.

**Actions**

___Multiattack.___ The giant makes two Lightning Sword attacks or uses Wind Javelin twice.

___Lightning Sword.___ Melee Weapon Attack: +14 to hit, reach 15 ft., one target. Hit: 40 (9d6+9) lightning damage.

___Windjavelin.___ The giant coalesces wind into a javeIin-like form and hurls it at a creature it can see within 600 feet of it. The javelin is considered a magic weapon and deals 19 (3d6+9) piercing damage to the target, striking unerringly. The javelin disappears after it hits.

**Legendary** Actions

The giant can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The giant regains spent legendary actions at the start of its turn.

___Gust.___ The giant targets a creature it can see within 60 feet of it and creates a magical gust of wind around it. The target must succeed on a DC 18 Strength saving throw or be pushed up to 20 feet in any horizontal direction the giant chooses.

___Thunderbolt (2 Actions).___ The giant hurls a thunderbolt at a creature it can see within 600 feet of it. The target must make a DC 18 Dexterity saving throw, taking 22 (4d10) thunder damage on a failed save, or half as much damage on a successful one.

___One with the Storm (3 Actions).___ The giant vanishes, dispersing itself into the storm surrounding its lair. The giant can end this effect at the start of any of its turns, becoming a giant once more and appearing in any location it chooses within its lair. While dispersed, the giant can't take any actions other than lair actions, and it can't be targeted by attacks, spells, or other effects. The giant can't use this ability outside its lair, nor can it use this ability if another creature is using a control weather spell or similar magic to quell the storm.