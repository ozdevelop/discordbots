"Gold Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: lawful good
_challenge_: "3 (700 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +4"
_damage_immunities_: "fire"
_saving_throws_: "Dex +4, Con +5, Wis +2, Cha +5"
_speed_: "30 ft., fly 60 ft., swim 30 ft."
_hit points_: "60 (8d8+24)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 17 (+3) | 14 (+2) | 11 (0) | 16 (+3) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) piercing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Fire Breath. The dragon exhales fire in a 15-foot cone. Each creature in that area must make a DC 13 Dexterity saving throw, taking 22 (4d10) fire damage on a failed save, or half as much damage on a successful one.

Weakening Breath. The dragon exhales gas in a 15-foot cone. Each creature in that area must succeed on a DC 13 Strength saving throw or have disadvantage on Strength-based attack rolls, Strength checks, and Strength saving throws for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.