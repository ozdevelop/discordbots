"Triceratops Zombie";;;_size_: Huge undead
_alignment_: unaligned
_challenge_: "5 (1,700 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "95 (10d12 + 30)"
_armor class_: "12 (natural armor)"
_stats_: | 22 (+6) | 6 (-2) | 17 (+3) | 1 (-5) | 6 (-2) | 5 (-3) |

___Trampling Charge.___ If the zombie moves at least 20
feet straight toward a creature and then hits it with
a gore attack on the same turn, that target must
succeed on a DC 13 Strength saving throw or be
knocked prone. If the target is prone, the zombie
can make one stomp attack against it as a bonus
action.

___Undead Fortitude.___ If damage reduces the zombie
to 0 hit points, it must make a Constitution saving
throw with a DC of 5 + the damage taken, unless
the damage is radiant or from a critical hit. On a
success, the zombie drops to 1 hit point instead.

**Actions**

___Gore.___ Melee Weapon Attack: +9 to hit, reach 5 ft.,
one target. Hit: 24 (4d8 + 6) piercing damage.

___Stomp.___ Melee Weapon Attack: +9 to hit, reach 5 ft.,
one prone creature. Hit: 22 (3d10 + 6) bludgeoning
damage.
