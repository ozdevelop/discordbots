"Skyweaver";;;_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Auran, Common"
_skills_: "Deception +5, Persuasion +5"
_speed_: "30 ft."
_hit points_: "44 (8d8+8)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 8 (-1) | 14 (+2) | 12 (+1) | 11 (0) | 10 (0) | 16 (+3) |

___Spellcasting.___ The Skyweaver is a 6th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _blade ward, light, message, ray of frost, shocking grasp_

* 1st level (4 slots): _feather fall, mage armor, witch bolt_

* 2nd level (3 slots): _gust of wind, invisibility_

* 3rd level (3 slots): _fly, lightning bolt_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 3 (1d4 + 1) piercing damage.
