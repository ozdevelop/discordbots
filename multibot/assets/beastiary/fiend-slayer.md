"Fiend Slayer";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Athletics +5, Insight +3, Intimidation +4"
_senses_: "passive Perception 11"
_saving_throws_: "Wis +1, Cha +4"
_speed_: "30 ft."
_hit points_: "26 (4d10 + 4)"
_armor class_: "18 (chainmail, shield)"
_stats_: | 16 (+3) | 8 (-1) | 12 (+1) | 9 (-1) | 12 (+1) | 14 (+2) |

___Divine Smite.___ When the slayer hits with a melee
weapon attack, it can expend a spell slot to deal an
addition 9 (2d8) radiant damage, in addition to the
weapon's damage.

___Dueling Fighting Style.___ The slayer gains a +2 bonus
to damage rolls while wielding a melee weapon in
one hand and no other weapons (included in the
attack).

___Spellcasting.___ The slayer is a 3th-level spellcaster. Its
spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). The slayer has the
following paladin spells prepared:

* 1st level (3 slots): _bane, cure wounds, searing smite_

**Actions**

___Flail.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 9 (1d8 + 5) bludgeoning damage.

___Lay on Hands (1/Day).___ The slayer touches a creature
and restores 20 hit points to it.
