"Wendigo Spawn";;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Infernal"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +5"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "cold"
_speed_: "30 ft."
_hit points_: "58 (6d8 + 31)"
_armor class_: "16 (chain mail)"
_stats_: | 16 (+3) | 16 (+3) | 16 (+3) | 6 (-2) | 15 (+2) | 8 (-1) |

___Divided Mind.___ The Wendigo Spawn still has some memories from its host body, granting it advantage on all Wisdom saving throws.

**Actions**

___Multiattack.___ The Wendigo Spawn  makes one warhammer and one bite attack against the same target.

___Warhammer.___ +5 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 3) bludgeoning damage.

___Bite.___ +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage.
