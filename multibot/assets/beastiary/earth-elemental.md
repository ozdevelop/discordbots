"Earth Elemental";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., tremorsense 60 ft."
_damage_immunities_: "poison"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "126 (12d10+60)"
_armor class_: "17 (natural armor)"
_damage_vulnerabilities_: "thunder"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 20 (+5) | 8 (-1) | 20 (+5) | 5 (-3) | 10 (0) | 5 (-3) |

___Earth Glide.___ The elemental can burrow through nonmagical, unworked earth and stone. While doing so, the elemental doesn't disturb the material it moves through.

___Siege Monster.___ The elemental deals double damage to objects and structures.

**Actions**

___Multiattack.___ The elemental makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage.