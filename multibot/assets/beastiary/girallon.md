"Girallon";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +5"
_speed_: "40 ft., climb 40 ft."
_hit points_: "59 (7d10+21)"
_armor class_: "13"
_stats_: | 18 (+4) | 16 (+3) | 16 (+3) | 5 (-3) | 12 (+1) | 7 (-2) |

___Source.___ Volo's Guide to Monsters, p. 152

___Aggressive.___ As a bonus action, the girallon can move up to its speed toward a hostile creature that it can see.

**Actions**

___Multiattack.___ The girallon makes five attacks: one with its bite and four with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft, one creature. Hit: 7 (1d6+4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit. reach 10 ft., one target. Hit: 7 (1d6+4) slashing damage.