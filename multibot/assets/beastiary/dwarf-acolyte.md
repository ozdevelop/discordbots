"Dwarf Acolyte";;;_page_number_: 999
_size_: Medium humanoid (dwarf)
_alignment_: any alignment
_challenge_: "1/4 (50 XP)"
_languages_: "any one language (usually Common)"
_senses_: "darkvision, passive Perception 12"
_skills_: "Medicine +4, Religion +2"
_speed_: "30 ft."
_hit points_: "9  (2d8)"
_armor class_: "10"
_damage_resistances_: "poison"
_stats_: | 10 (0) | 10 (0) | 10 (0) | 10 (0) | 14 (+2) | 11 (0) |

___Dwarven Resilience.___ The dwarf has advantage on saving throws against poison and has resistance to poison damage

___Spellcasting.___ The acolyte is a 1st-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). The acolyte has following cleric spells prepared:

* Cantrips (at will): _light, sacred flame, thaumaturgy_

* 1st level (3 slots): _bless, cure wounds, sanctuary_

**Actions**

___Club___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d4) bludgeoning damage.
