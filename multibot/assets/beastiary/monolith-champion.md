"Monolith Champion";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "8 (3900 XP)"
_languages_: "Elvish, Umbral"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison, bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "40 ft."
_hit points_: "102 (12d10 + 36)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 16 (+3) | 10 (+0) | 10 (+0) | 10 (+0) |

___Blatant Dismissal.___ While in a fey court or castle, a monolith champion that scores a successful hit with its greatsword can try to force the substitution of the target with a shadow double. The target must succeed at a DC 14 Charisma saving throw or become invisible, silent, and paralyzed, while an illusory version of itself remains visible and audible.and under the monolith champion's control, shouting for a retreat or the like. Outside fey locales, this ability does not function.

___Fey Flame.___ The ritual powering a monolith champion grants it an inner flame that it can use to enhance its weapon or its fists with additional fire or cold damage, depending on the construct's needs.

**Actions**

___Multiattack.___ The champion makes two greatsword attacks or two slam attacks.

___Greatsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (3d6 + 4) slashing damage plus 11 (2d10) cold or fire damage.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage plus 11 (2d10) cold or fire damage.

