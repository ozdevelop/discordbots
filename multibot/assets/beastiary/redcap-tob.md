"Redcap";;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Sylvan, Undercommon"
_skills_: "Athletics +8, Intimidation +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Con +6"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, frightened"
_speed_: "40 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 10 (+0) | 17 (+3) | 11 (+0) | 13 (+1) | 8 (-1) |

___Clomping Boots.___ The redcap has disadvantage on Dexterity (Stealth) checks.

___Red Cap.___ The redcap must soak its cap in the blood of a humanoid killed no more than an hour ago at least once every three days. If it goes more than 72 hours without doing so, the blood on its cap dries and the redcap gains one level of exhaustion every 24 hours. While the cap is dry, the redcap can't remove exhaustion by any means. All levels of exhaustion are removed immediately when the redcap soaks its cap in fresh blood. A redcap that dies as a result of this exhaustion crumbles to dust.

___Solid Kick.___ The redcap can kick a creature within 5 feet as a bonus action. The kicked creature must make a successful DC 15 Strength saving throw or fall prone.

**Actions**

___Multiattack.___ The redcap makes two pike attacks and one bite attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) piercing damage and the creature is bleeding profusely. A bleeding creature must make a successful DC 15 Constitution saving throw at the start of its turn or take 10 (3d6) necrotic damage and continue bleeding. On a successful save the creature takes no necrotic damage and the effect ends. A creature takes only 10 necrotic damage per turn from this effect no matter how many times it's been bitten, and a single successful saving throw ends all bleeding. Spending an action to make a successful DC 15 Wisdom (Medicine) check or any amount of magical healing also stops the bleeding. Constructs and undead are immune to the bleeding effect.

___Pike.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 10 (1d10 + 5) piercing damage.

