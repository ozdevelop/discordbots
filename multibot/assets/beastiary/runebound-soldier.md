"Runebound Soldier";;;_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common"
_senses_: "passive Perception 9"
_condition_immunities_: "charmed"
_speed_: "30 ft."
_hit points_: "22 (4d8 + 4)"
_armor class_: "16 (chainmail)"
_stats_: | 15 (+2) | 12 (+1) | 12 (+1) | 8 (-1) | 8 (-1) | 6 (-2) |

___Runebound.___ The soldier's body is coated in magical
runes granted by a runespeaker that provide it with
additional strength. The soldier gains one of the
following runic bonuses:
* Rune of Fortitude – The soldier's AC is increased
to 18. In addition, as long as the soldier has at
least 1 hit point, it regenerates 3 hit points at the
start of its turns.
* Rune of Perception – The soldier has advantage
on Wisdom (Perception) checks and can see
invisible creatures.
* Rune of Strength – The soldier's longsword
attacks deal an additional 2 slashing damage and
it has advantage on Strength-based checks and
saving throws.

___Runic Weapon.___ The soldier's longsword is imbued
with powerful arcane runes, causing attacks with
that weapon to deal an additional 4 (1d8) force
damage (included in the attack).

**Actions**

___Runic Longsword.___ Melee Weapon Attack: +4 to hit,
reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing
damage, or 7 (1d10 + 2) slashing damage if used
with two hands, plus 4 (1d8) force damage.
