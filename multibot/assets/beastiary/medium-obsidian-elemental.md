"Medium Obsidian Elemental";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "20 ft."
_hit points_: "66 (7d8 + 35)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 8 (-1) | 20 (+5) | 4 (-3) | 11 (+0) | 11 (+0) |

___Brute.___ A melee weapon attack deals one extra die of its damage when
the obsidian elemental hits with it (included in the attack).

___Death Throes.___ When the obsidian elemental dies, it explodes, and each
creature within 30 feet of it must make a DC 16 Dexterity saving throw,
taking 17 (5d6) slashing damage and 17 (5d6) fire damage on a failed
saving throw, or half as much damage on a successful one.

___Molten Glass.___ A creature that hits the obsidian elemental with a melee
attack while within 5 feet of it takes 7 (2d6) fire damage.

**Actions**

___Multiattack.___ The obsidian elemental makes two claw attacks.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) slashing damage plus 4 (1d8) fire damage.
