"Mushroom Moose";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "passive Perception 12"
_damage_resistances_: "poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "37 (5d10 + 10)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 14 (+2) | 7 (-2) | 15 (+2) | 7 (-2) |

___Floral Camouflage.___ While the moose remains
motionless and is lying down, it is indistinguishable
from a large log covered in mushrooms.

___Poisonous Exterior.___ The mushrooms that coat the
moose's body are poisonous and deadly to most
other creatures. A creature that touches the moose
or hits it with a melee attack while within 5 feet of
it takes 5 (1d10) poison damage.

___Rapid Regrowth.___ The mushroom moose has
unnatural regenerative capabilities. At the
beginning of each of the moose's turns, as long as
it is in direct sunlight and it has at least 1 hit point,
it regains 4 (1d8) hit points.

**Actions**

___Ram.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 11 (2d6 + 4) bludgeoning damage
plus 7 (2d6) poison damage.
