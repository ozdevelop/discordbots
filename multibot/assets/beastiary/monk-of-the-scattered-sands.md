"Monk of the Scattered Sands";;;_size_: Medium humanoid
_alignment_: neutral good
_challenge_: "4 (450 XP)"
_languages_: "any three languages"
_skills_: "Acrobatics +5, Medicine +5, Religion +2"
_senses_: "passive Perception 13"
_saving_throws_: "Dex +6, Wis +6"
_speed_: "40 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "16 (unarmored defense)"
_stats_: | 10 (+0) | 16 (+3) | 14 (+2) | 10 (+0) | 16 (+3) | 11 (+0) |

___Sand Glide.___ The monk has a flight speed of 20 ft. as
sands swirl to carry it. This benefit works only in
short bursts; the monk falls at the end of its turn if
it is in the air and nothing else is holding it aloft.

___Unarmored Defense.___ While the monk is wearing no
armor and wielding no shield, its AC incluces its
Wisdom modifier.

**Actions**

___Multiattack.___ The monk makes three attacks with its
quarterstaff. It can also use Manipulate Sands once,
either before or after one of the attacks.

___Quarterstaff.___ Melee Weapon Attack: +5 to hit, reach
5ft., one target. Hit: 7 (1d8 + 3) bludgeoning
damage.

___Manipulate Sands.___ The monk sends a stream of sand
at a target within 30 ft. The target must make a DC
13 Dexterity saving throw, taking 15 (6d4)
bludgeoning damage and becoming blinded until
the end of their next turn on a fail, or half as much
damage and are not blinded on a success.

**Reactions**

___Dancing Winds (2/Day).___ When the monk is targeted
by a ranged spell attack with no other targets, it can
quickly kick up a spiral of wind that alters its
trajectory away from the monk. The monk takes no
damage from this attack. As part of the same
reaction, the monk makes a ranged spell attack
against another creature within 60 ft. with a +5
bonus to the attack roll. On a hit, the spell strikes
the new target instead.
