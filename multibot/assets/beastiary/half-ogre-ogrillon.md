"Half-Ogre (Ogrillon)";;;_size_: Large giant
_alignment_: any chaotic alignment
_challenge_: "1 (200 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "30 (4d10+8)"
_armor class_: "12 (hide armor)"
_stats_: | 17 (+3) | 10 (0) | 14 (+2) | 7 (-2) | 9 (-1) | 10 (0) |

**Actions**

___Battleaxe.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage, or 14 (2d10 + 3) slashing damage if used with two hands.

___Javelin.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 10 (2d6 + 3) piercing damage.