"Ancient Dungeon Dragon";;;_size_: Gargantuan dragon (shapechanger)
_alignment_: neutral
_challenge_: "21 (33,000 XP)"
_languages_: "all"
_skills_: "Arcana +12, History +12, Insight +10, Investigation +19, Perception +17, Survival +10"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 27"
_saving_throws_: "Dex +8, Con +14, Wis +10, Cha +11"
_speed_: "40 ft., fly 80 ft."
_hit points_: "490 (28d20 + 196)"
_armor class_: "19 (natural armor)"
_stats_: | 27 (+8) | 12 (+1) | 25 (+7) | 20 (+5) | 17 (+3) | 19 (+4) |

___Special Equipment.___ The dragon is attuned to a crystal ball, which is
hidden somewhere within its labyrinthine lair. If a dungeon dragon loses
its crystal ball, it can create a new one over the course of 10 days.

___Innate Spellcasting.___ The dungeon dragon’s innate spellcasting ability
is Charisma (spell save DC 19, +11 to hit with spell attacks). It can cast
the following spells, requiring no material components.

* At will: _alarm, arcane lock, detect magic, detect thoughts, find traps, glyph of warding, identify, knock, scrying, stone shape, wall of stone_

* 3/day each: _dispel magic, hallucinatory terrain, locate creature, locate object, project image_

* 1/day: _find the path, maze, mirage arcane, passwall_

___Labyrinthine Recall.___ The dungeon dragon can perfectly recall any path
it has traveled.

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it can
choose to succeed instead.

___Trap Master.___ The dungeon dragon has advantage on saving throws
against the effects of traps.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one target. Hit: 19
(2d10 + 8) piercing damage.

___Claw.___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target. Hit: 15
(2d6 + 8) slashing damage.

___Tail.___ Melee Weapon Attack: +15 to hit, reach 20 ft., one target. Hit: 17
(2d8 + 8) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon’s choice that is within
120 feet of the dragon and aware of it must succeed on a DC 19 Wisdom
saving throw or become frightened for 1 minute. A creature can repeat the
saving throw at the end of each of its turns, ending the effect on itself on
a success. If a creature’s saving throw is successful or the effect ends for
it, the creature is immune to the dragon’s Frightful Presence for the next
24 hours.

___Change Shape.___ The dragon magically polymorphs into a humanoid
or beast that has a challenge rating no higher than its own, or back
into its true form. It reverts to its true form if it dies. Any equipment
it is wearing or carrying is absorbed or borne by the new form (the
dragon’s choice).

In a new form, the dragon retains its alignment, hit points, Hit Dice,
ability to speak, proficiencies, Legendary Resistance, lair actions, and
Intelligence, Wisdom, and Charisma scores, as well as this action. Its
statistics and capabilities are otherwise replaced by those of the new form,
except for class features or legendary actions of that form.

___Confusion Breath (Recharge 5–6).___ The dragon exhales gas in a 90-foot
cone that spreads around corners. Each creature in that area must succeed
on a DC 22 Constitution saving throw. On a failed save, the creature is
confused for 1 minute. While confused, the creature uses its action to
Dash in a random direction, even if that movement takes the creature into
dangerous areas. A confused creature can repeat the saving throw at the
end of each of its turns, ending the effect on a success.

___Dominate.___ One creature the dragon can see within 60 feet of it must
succeed on a DC 19 Wisdom saving throw or be charmed for 1 hour.
The creature has advantage if the dragon is fighting it. While the creature
is charmed, the dragon has a telepathic link with it as long as it and the
dragon are on the same plane of existence, and the dragon can issue
commands to the creature as long as it is conscious (no action
required), which it does its best to obey.

The dragon can use an action to take total and precise control
of the target. Until the end of the dragon’s next turn, the creature
takes only the actions the dragon dictates and doesn’t do anything
that the dragon does not allow it to. In addition, the dragon can also
cause the creature to use a reaction, but this requires the dragon to use its
reaction as well.

The charmed creature can repeat the saving throw whenever it takes
damage. If the saving throw succeeds, the spell ends.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 23 Dexterity
saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the dragon takes a lair
action to cause one of the following effects; the dragon can’t use the same
effect two rounds in a row:

___Illusory Terrain.___ The dungeon dragon uses illusions to subtly
manipulate creatures within its lair, as if it had cast the hallucinatory
terrain, project image, or mirage arcane spell, targeting any location
within its lair that it can see.

___Changing Maze.___ The dungeon dragon manipulates the stone of its
labyrinth as if it had cast the stone shape spell, targeting any location
within its lair that it can see.

___Stone Walls.___ The dungeon dragon creates walls of stone as if it had cast
the wall of stone spell, targeting any location within its lair that it can see.
When cast in this way, the dragon does not need to concentrate on the spell
and its effects become permanent immediately.

**Regional** Effects

The region containing the ancient dungeon dragon’s lair is changed by
its presence, which creates one or more of the following effects:

___Minotaur Meeting.___ Minotaurs are strangely drawn to the area
surrounding a dungeon dragon’s lair, often seeking to emulate the master
of mazes.

___Weather Alteration.___ Once per day, the dungeon dragon can alter the
weather in a 6-mile radius centered on its lair.

If the dragon dies these effects fade over 1d10 days.
