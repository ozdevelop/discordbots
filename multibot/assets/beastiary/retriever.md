"Retriever";;;_page_number_: 222
_size_: Large construct
_alignment_: lawful evil
_challenge_: "14 (11500 XP)"
_languages_: "understands Abyssal, Elvish, and Undercommon but can't speak"
_senses_: "blindsight 30 ft., darkvision 60 ft., passive Perception 15"
_skills_: "Perception +5, Stealth +8"
_damage_immunities_: "necrotic, poison, psychic; bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_saving_throws_: "Dex +8, Con +10, Wis +5"
_speed_: "40 ft., climb 40 ft."
_hit points_: "210  (20d10 + 100)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_stats_: | 22 (+6) | 16 (+3) | 20 (+5) | 3 (-3) | 11 (0) | 4 (-3) |

___Faultless Tracker.___ The retriever is given a quarry by its master. The quarry can be a specific creature or object the master is personally acquainted with, or it can be a general type of creature or object the master has seen before. The retriever knows the direction and distance to its quarry as long as the two of them are on the same plane of existence. The retriever can have only one such quarry at a time. The retriever also always knows the location of its master.

___Innate Spellcasting.___ The retriever's innate spellcasting ability is Wisdom (spell save DC 13). The retriever can innately cast the following spells, requiring no material components.

* 3/day each: _plane shift _(only self and up to one incapacitated creature which is considered willing for the spell)_, web_

**Actions**

___Multiattack___ The retriever makes two foreleg attacks and uses its force or paralyzing beam once, if available.

___Foreleg___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) slashing damage.

___Force Beam___ The retriever targets one creature it can see within 60 feet of it. The target must make a DC 16 Dexterity saving throw, taking 27 (5d10) force damage on a failed save, or half as much damage on a successful one.

___Paralyzing Beam (Recharge 5-6)___ The retriever targets one creature it can see within 60 feet of it. The target must succeed on a DC 18 Constitution saving throw or be paralyzed for 1 minute. The paralyzed target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
If the paralyzed creature is Medium or smaller, the retriever can pick it up as part of the retriever's move and walk or climb with it at full speed.
