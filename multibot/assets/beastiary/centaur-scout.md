"Centaur Scout";;;_size_: Large monstrosity
_alignment_: neutral good
_challenge_: "1 (200 XP)"
_languages_: "Elvish, Sylvan"
_skills_: "Perception +4, Survival +4"
_senses_: "passive Perception 14"
_speed_: "50 ft."
_hit points_: "22 (4d10 + 6)"
_armor class_: "12"
_stats_: | 16 (+3) | 15 (+2) | 14 (+2) | 8 (-1) | 14 (+2) | 10 (+0) |

___Skilled Tracker.___ The centaur has advantage on
Wisdom (Survival) checks while in the forest.

**Actions**

___Multiattack.___ The centaur makes two attacks: one
with its longsword and one with its hooves or two
with its longbow.

___Longsword.___ Melee Weapon Attack: +5 to hit, reach
5ft., one target. Hit: 7 (1d8 + 3) slashing damage.

___Hooves.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 8 (2d4 + 3) bludgeoning damage.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range
150/600 ft., one target. Hit: 6 (1d8 + 2) piercing
damage.
