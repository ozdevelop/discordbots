"Blood Wight";;;_size_: Large undead
_alignment_: neutral
_challenge_: "8 (3,900 XP)"
_languages_: "the languages it knew in life"
_skills_: "Perception +7, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 17"
_damage_immunities_: "poison"
_damage_resistances_: "fire, necrotic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t silvered"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 15 (+2) | 18 (+4) | 13 (+1) | 13 (+1) | 16 (+3) |

___Magic Weapons.___ The wight’s weapon attacks are magical.

___Sunlight Sensitivity.___ While in sunlight, the wight has disadvantage on
attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The blood wight makes one claw attack and one life
drain attack.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 14
(2d8 + 5) slashing damage. If the target is a Medium or smaller creature, it
is grappled (escape DC 15). The wight can grapple only one target.

___Engulf.___ The wight engulfs one creature it has grappled, and the grapple
ends. While engulfed, the creature is blinded and restrained, it has total
cover against attacks and other effects outside the wight, and it takes 27
(6d8) necrotic damage at the start of each of the wight’s turns. If the wight
takes 30 damage or more on a single turn from a creature inside it, the
wight must succeed on a DC 15 Constitution saving throw at the end of
that turn or regurgitate all engulfed creatures, which fall prone in a space
within 5 feet of the wight. If the wight dies, all engulfed creatures explode
out from the corpse, falling prone 15 feet away.

___Life Drain.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature.
Hit: 11 (2d6 + 4) necrotic damage. The target must succeed on a DC 15
Constitution saving throw or its hit point maximum is reduced by an
amount equal to the damage taken. This reduction lasts until the target
finishes a long rest. The target dies if this effect reduces its hit point
maximum to 0. A humanoid slain by this attack rises 24 hours later as a
zombie under the wight’s control, unless the humanoid is restored to life
or its body is destroyed. The wight can have no more than twelve zombies
under its control at one time.
