"Orobas Devil";;;_size_: Large fiend
_alignment_: lawful evil
_challenge_: "14 (11500 XP)"
_languages_: "Celestial, Darakhul, Draconic, Giant, Infernal, Undercommon, Void Speech; telepathy 100 ft."
_skills_: "Deception +10, History +11, Insight +13, Perception +13, Persuasion +10"
_senses_: "truesight 90 ft., passive Perception 23"
_saving_throws_: "Str +13, Dex +7, Con +14, Wis +13"
_damage_immunities_: "fire, poison"
_damage_resistances_: "acid, cold; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "261 (14d10 + 126)"
_armor class_: "19 (natural armor)"
_stats_: | 26 (+8) | 14 (+2) | 28 (+9) | 23 (+6) | 26 (+8) | 21 (+5) |

___Knowing (3/day).___ An orobas can predict actions and alter chance accordingly. Three times per day, it can choose to have advantage on any attack or skill check.

___Magic Resistance.___ The orobas has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The orobas's weapon attacks are magical.

___Sage Advice.___ An orobas sometimes twists responses to a divination. It softens the answer, leaves crucial information out of the response, manipulates a convoluted answer, or outright lies. An orobas always has advantage on Deception and Persuasion checks when revealing the result of a divination.

___Innate Spellcasting.___ The orobas' spellcasting ability is Charisma (spell save DC 18, +10 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

Constant: detect evil and good

At will: augury, protection from evil and good, teleport (self plus 50 lb of objects only)

5/day each: bestow curse, fireball, scorching ray

3/day each: antimagic field, chain lightning, contact other plane, dimension door, wall of fire

1/day each: eyebite, find the path, foresight

**Actions**

___Multiattack.___ The orobas makes four attacks: one with its bite, one with its claw, one with its flail, and one with its stomp.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 18 (3d6 + 8) piercing damage. The target must succeed on a DC 18 Constitution saving throw or become poisoned. While poisoned in this way, the target can't regain hit points and it takes 14 (4d6) poison damage at the start of each of its turns. The poisoned target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 15 (2d6 + 8) slashing damage.

___Flail.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 17 (2d8 + 8) bludgeoning damage plus 18 (4d8) acid damage.

___Stomp.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 15 (2d6 + 8) bludgeoning damage.

