"Wight";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +4"
_damage_immunities_: "necrotic, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_speed_: "30 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "14 (studded leather)"
_condition_immunities_: "poisoned"
_stats_: | 15 (+2) | 14 (+2) | 16 (+3) | 10 (0) | 13 (+1) | 15 (+2) |

___Sunlight Sensitivity.___ While in sunlight, the wight has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The wight makes two longsword attacks or two longbow attacks. It can use its Life Drain in place of one longsword attack.

___Life Drain.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 5 (1d6 + 2) necrotic damage. The target must succeed on a DC 13 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

A humanoid slain by this attack rises 24 hours later as a zombie under the wight's control, unless the humanoid is restored to life or its body is destroyed. The wight can have no more than twelve zombies under its control at one time.

___Longsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage, or 7 (1d10 + 2) slashing damage if used with two hands.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.