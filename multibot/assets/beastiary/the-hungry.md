"The Hungry";;;_page_number_: 232
_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "11 (7200 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "225  (30d8 + 90)"
_armor class_: "17 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing while in dim light or darkness"
_stats_: | 19 (+4) | 10 (0) | 17 (+3) | 6 (-2) | 11 (0) | 6 (-2) |

___Life Hunger.___ If a creature the Hungry can see regains hit points, the Hungry gains two benefits until the end of its next turn: it has advantage on attack rolls, and its bite deals an extra 22 (4d10) necrotic damage on a hit.

**Actions**

___Multiattack___ The Hungry makes two attacks: one with its bite and one with its claws.

___Bite___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage plus 13 (3d8) necrotic damage.

___Claws___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 18 (4d6 + 4) slashing damage. If the target is Medium or smaller, it is grappled (escape DC 16) and is restrained until the grapple ends. While grappling a creature, the Hungry can't attack with its claws.