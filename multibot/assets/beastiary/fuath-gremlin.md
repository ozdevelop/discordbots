"Fuath Gremlin";;;_size_: Small fey
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Aquan"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_resistances_: "cold"
_speed_: "20 ft., climb 10 ft., swim 30 ft."
_hit points_: "22 (4d6 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 7 (-2) | 13 (+1) | 14 (+2) | 10 (+0) | 13 (+1) | 8 (-1) |

___Sunlight Sensitivity.___ While in sunlight, the gremlin has disadvantage on
attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Water Breathing.___ The fuath gremlin can only breathe underwater.

**Actions**

___Multiattack.___ The fuath gremlin makes two attacks with its claws.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) slashing damage.

___Dart.___ Ranged Weapon Attack: +3 to hit, range 20/60 ft., one target. Hit: 3 (1d4 + 1) piercing damage.

___Congeal Water (1/day).___ One creature of the fuath gremlin’s choice
within 30 feet of it must succeed on a DC 10 Dexterity saving throw or be
coated with a thick, viscous coating of clinging watery fluid for 1 minute.
While coated with this substance, the target is restrained and must hold its
breath to keep from drowning (reference the SRD for Suffocating rules).
The target can repeat the saving throw at the end of each of its turns,
ending the effect on itself on a success.
