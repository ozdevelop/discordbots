"Emerald Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Draconic"
_skills_: "Insight +3, Perception +3"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 15"
_saving_throws_: "Dex +4, Int +4, Wis +3, Cha +3"
_damage_vulnerabilities_: "psychic"
_speed_: "30 ft., fly 60 ft. (hover), swim 30 ft."
_hit points_: "31 (7d8)"
_armor class_: "17 (natural armor)"
_stats_: | 13 (+1) | 14 (+2) | 11 (+0) | 14 (+2) | 12 (+1) | 12 (+1) |

**Psionics**

___Charges:___ 7 | ___Recharge:___ 1d4 | ___Fracture:___ 6

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft.,
one target. Hit: 14 (2d10 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 10 (2d6 + 3) slashing damage.
