"Flying Snake";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_senses_: "blindsight 10 ft."
_speed_: "30 ft., fly 60 ft., swim 30 ft."
_hit points_: "5 (2d4)"
_armor class_: "14"
_stats_: | 4 (-3) | 18 (+4) | 11 (0) | 2 (-4) | 12 (+1) | 5 (-3) |

___Flyby.___ The snake doesn't provoke opportunity attacks when it flies out of an enemy's reach.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 1 piercing damage plus 7 (3d4) poison damage.