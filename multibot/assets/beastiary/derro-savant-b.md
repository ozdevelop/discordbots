"Derro Savant (B)";;;_page_number_: 159
_size_: Small humanoid (derro)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 7"
_skills_: "Stealth +4"
_speed_: "30 ft."
_hit points_: "36  (8d6 + 8)"
_armor class_: "13 (leather armor)"
_stats_: | 9 (0) | 14 (+2) | 12 (+1) | 11 (0) | 5 (-2) | 14 (+2) |

___Magic Resistance.___ The derro savant has advantage on saving throws against spells and other magical effects.

___Spellcasting.___ The derro savant is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). The derro knows the following sorcerer spells:

* Cantrips (at will): _acid splash, mage hand, message, prestidigitation, ray of frost_

* 1st level (4 slots): _burning hands, chromatic orb, sleep_

* 2nd level (3 slots): _invisibility, spider climb_

* 3rd level (2 slots): _lightning bolt_

___Sunlight Sensitivity.___ While in sunlight, the derro savant has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Quarterstaff___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 2 (1d6-1) bludgeoning damage.
