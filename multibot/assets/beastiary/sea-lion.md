"Sea Lion";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_skills_: "Perception +4, Stealth +5"
_speed_: "10 ft., swim 40 ft."
_hit points_: "90 (12d10+24)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 15 (+2) | 3 (-4) | 12 (+1) | 8 (-1) |

___Source.___ tales from the yawning portal,  page 242

___Amphibious.___ The sea lion can breathe air and water.

___Keen Smell.___ The sea lion has advantage on Wisdom (Perception) checks that rely on smell.

___Pack Tactics.___ The sea lion has advantage on an attack roll against a creature if at least one of the sea lion's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Swimming Leap.___ With a 10-foot swimming start, the sea lion can long jump out of or across the water up to 25 feet.

**Actions**

___Multiattack.___ The sea lion makes three attacks: one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) piercing damage