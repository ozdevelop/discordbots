"Darkling";;;_size_: Small fey
_alignment_: chaotic neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Elvish, Sylvan"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Acrobatics +5, Deception +2, Perception +5, Stealth +7"
_speed_: "30 ft."
_hit points_: "13 (3d6+3)"
_armor class_: "14 (leather armor)"
_stats_: | 9 (-1) | 16 (+3) | 12 (+1) | 10 (0) | 12 (+1) | 10 (0) |

___Death Flash.___ When the darkling dies, nonmagical light flashes out from it in a 10-foot radius as its body and possessions, other than metal or magic objects, burn to ash. Any creature in that area and able to see the bright light must succeed on a DC 10 Constitution saving throw or be blinded until the end of the creature's next turn.

___Light Sensitivity.___ While in bright light, the darkling has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4+3) piercing damage. If the darkling has advantage on the attack roll, the attack deals an extra 7 (2d6) piercing damage.