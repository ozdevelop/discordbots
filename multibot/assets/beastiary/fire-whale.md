"Fire Whale";;;_size_: Huge elemental
_alignment_: unaligned
_challenge_: "11 (7,200 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: "blindsight 120 ft., passive Perception 15"
_damage_immunities_: "fire"
_speed_: "swim 40 ft."
_hit points_: "175 (14d12 + 84)"
_armor class_: "14 (natural armor)"
_stats_: | 30 (+10) | 13 (+1) | 22 (+6) | 2 (-4) | 12 (+1) | 6 (-2) |

___Echolocation.___ The whale can’t use its blindsight while deafened.

___Hold Breath.___ The whale can hold its breath for 30 minutes.

___Keen Hearing.___ The whale has advantage on Wisdom (Perception)
checks that rely on hearing.

**Actions**

___Multiattack.___ The fire whale makes two attacks: one with its bite and
one with its tail slap.

___Bite.___ Melee Weapon Attack: +14 to hit, reach 5 ft., one target. Hit: 34
(7d6 + 10) piercing damage.

___Tail Slap.___ Melee Weapon Attack: +14 to hit, reach 15 ft., one target.
Hit: 23 (2d12 + 10) bludgeoning damage.

___Scalding Blast (Recharge 5–6).___ A fire whale can release a blast of
superheated air in a 60-foot cone from its blowhole that scalds or burns
those contacting it. Each creature in that area must make a DC 16 Dexterity
saving throw, taking 49 (14d6) fire damage on a failed save, or half as
much on a successful one.
