"Ankylosaurus";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_speed_: "30 ft."
_hit points_: "68 (8d12+16)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 11 (0) | 15 (+2) | 2 (-4) | 12 (+1) | 5 (-3) |

**Actions**

___Tail.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 18 (4d6 + 4) bludgeoning damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be knocked prone.