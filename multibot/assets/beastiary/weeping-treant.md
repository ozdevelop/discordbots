"Weeping Treant";;;_size_: Huge plant
_alignment_: neutral
_challenge_: "6 (2300 XP)"
_languages_: "Common, Druidic, Elvish, Sylvan"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "bludgeoning and piercing"
_speed_: "30 ft."
_hit points_: "105 (10d12 + 40)"
_armor class_: "17 (natural armor)"
_stats_: | 21 (+5) | 8 (-1) | 20 (+5) | 12 (+1) | 16 (+3) | 11 (+0) |

___Siege Monster.___ The treant deals double damage to objects and structures.

___Treespeech.___ A weeping treant can converse with plants, and most plants greet them with a friendly or helpful attitude.

___Acidic Tears.___ Thick tears of dark sap stream continuously down the treant's face and trunk. These tears are highly acidic - anyone who attacks the treant from a range of 5 feet or less must succeed on a DC 15 Dexterity saving throw or take 6 (1d12) acid damage from splashed tears. This acidic matter continues doing 6 (1d12) acid damage at the start of each of the creature's turns until it or an adjacent ally uses an action to wipe off the tears or three rounds elapse.

**Actions**

___Multiattack.___ The treant makes three slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., single target. Hit: 15 (3d6 + 5) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +8 to hit, range 60/180 ft., one target. Hit: 21 (3d10 + 5) bludgeoning damage.

