"Lantern Goat";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_skills_: "Perception +8, Stealth +7"
_senses_: "darkvision 60 ft. passive Perception 18"
_damage_immunities_: "poison"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "40 ft., fly 60 ft."
_hit points_: "93 (17d8 + 17)"
_armor class_: "16 (natural armor)"
_stats_: | 13 (+1) | 18 (+4) | 13 (+1) | 6 (-2) | 14 (+2) | 17 (+3) |

___Charge.___ If the lantern goat moves at least 20 feet straight toward a
target and then hits it with a head butt attack on the same turn, the target
takes an extra 14 (4d6) bludgeoning damage. If the target is a creature,
it must succeed on a DC 15 Strength saving throw or be knocked prone.

___Fear Light.___ As a bonus action, the lantern goat can emit an ugly yellow
light from the lantern around its neck. Any creature that can see the light
within 30 feet of the lantern goat must make a DC 15 Wisdom saving
throw, unless the lantern goat is incapacitated. On a failed save, the
creature is frightened until the start of its next turn. If a creature’s saving
throw is successful, the creature is immune to the lantern goat’s Fear Light
for the next 24 hours.

___Life Sense.___ The lantern goat can innately sense all living creatures
within 60 feet of it.

**Actions**

___Multiattack.___ The lantern goat makes three attacks: one with its head
butt and two with its hooves.

___Head Butt.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit:
11 (2d6 + 4) bludgeoning damage.

___Hooves.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9
(2d4 + 4) slashing damage.

**Reactions**

___Soul Capture.___ As a reaction, when a creature within 60 feet of the
lantern goat that it can see dies, the lantern goat can draw the soul of that
creature into the lantern around its neck unless the creature succeeds on
a DC 15 Wisdom saving throw. On a failure, the creature’s soul is drawn
into the lantern, where it will be digested over the next 1 hour by the
lantern goat. Once the hour has elapsed, the creature dies and can only
be returned to life by a resurrection, true resurrection, or wish spell. The
lantern can only be removed from the lantern goat or be destroyed — thus
releasing the trapped soul — if the lantern goat is slain.
