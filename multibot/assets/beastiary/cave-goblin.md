"Cave Goblin";;;_size_: Medium humanoid (goblinoid)
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Goblin"
_skills_: "Perception +2"
_senses_: "darkvision 120 ft., passive Perception 12"
_speed_: "30 ft."
_hit points_: "14 (4d6)"
_armor class_: "16 (natural armor)"
_stats_: | 10 (+0) | 14 (+2) | 11 (+0) | 10 (+0) | 11 (+0) | 5 (-3) |

___Moss Covered.___ The goblin has advantage when on
Dexterity (Stealth) checks made to hide in rocky or
cavernous terrain.

___Sunlight Sensitivity.___ While in sunlight, the goblin has
disadvantage on attack rolls as well as on Wisdom
(Perception) checks that rely on sight.

**Actions**

___Sharpened Stalagmite.___ Melee Weapon Attack: +4 to
hit, reach 5ft., one target. Hit: 6 (1d8 + 2) piercing
damage.

___Javelin.___ Melee Weapon Attack: +4 to hit, range
80/320 ft., one target. Hit: 5 (1d6 + 2) piercing
damage.

**Bonus** Actions

___Nimble Escape.___ The goblin can take the Disengage
or Hide action as a bonus action on each of its
turns.
