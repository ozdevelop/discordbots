"Spider";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 30 ft."
_skills_: "Stealth +4"
_speed_: "20 ft., climb 20 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "12"
_stats_: | 2 (-4) | 14 (+2) | 8 (-1) | 1 (-5) | 10 (0) | 2 (-4) |

___Spider Climb.___ The spider can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Web Sense.___ While in contact with a web, the spider knows the exact location of any other creature in contact with the same web.

___Web Walker.___ The spider ignores movement restrictions caused by webbing.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 1 piercing damage, and the target must succeed on a DC 9 Constitution saving throw or take 2 (1d4) poison damage.