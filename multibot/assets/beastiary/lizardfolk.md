"Lizardfolk";;;_size_: Medium humanoid (lizardfolk)
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Draconic"
_skills_: "Perception +3, Stealth +4, Survival +5"
_speed_: "30 ft., swim 30 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "15 (natural armor, shield)"
_stats_: | 15 (+2) | 10 (0) | 13 (+1) | 7 (-2) | 12 (+1) | 7 (-2) |

___Hold Breath.___ The lizardfolk can hold its breath for 15 minutes.

**Actions**

___Multiattack.___ The lizardfolk makes two melee attacks, each one with a different weapon.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Heavy Club.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage.

___Javelin.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Spiked Shield.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.