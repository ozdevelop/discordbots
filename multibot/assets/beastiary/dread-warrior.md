"Dread Warrior";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "l"
_languages_: "Common"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +4, Perception +3"
_damage_immunities_: "poison"
_saving_throws_: "Wis +3"
_speed_: "30 ft."
_hit points_: "37 (5d8+15)"
_armor class_: "18 (chain mail, shield)"
_condition_immunities_: "exhaustion, poisoned"
_stats_: | 15 (+2) | 11 (0) | 16 (+3) | 10 (0) | 12 (+1) | 10 (0) |

___Source.___ tales from the yawning portal,  page 233

___Undead Nature.___ A dread warrior doesn't require air, food, drink, or sleep.

___Undead Fortitude.___ If damage reduces the dread warrior to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken , unless the damage is radiant or from a critical hit. On a success, the dread warrior drops to 1 hit point instead.

**Actions**

___Multiattack.___ The dread warrior makes two melee attacks.

___Battleaxe.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage, or 7 (1d10 + 2) slashing damage if wielded with two hands.

___Javelin.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage.