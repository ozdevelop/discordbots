"Translucent Bulette";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "15 (13,000 XP)"
_languages_: "--"
_skills_: "Perception +10"
_senses_: "blindsight 60 ft., truesight 120 ft., passive Perception 14"
_saving_throws_: "Dex +7, Con +11, Wis +5"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "60 ft., fly 80 ft."
_hit points_: "225 (18d12 + 108)"
_armor class_: "14 (Material Plane) or 18 (Astral Plane)"
_stats_: | 24 (+7) | 14 (+2) | 22 (+6) | 6 (-2) | 10 (+0) | 10 (+0) |

___Astral Ambusher.___ In the first round of
a combat, the bulette has advantage on
attack rolls against any creature it has
surprised.

___Astral Jaunt.___ As a bonus action, the
bulette can magically shift from the
Prime Material Plane to the Astral
Plane, or vice versa.

___Astral Portal.___ On the Prime
Material Plane, any melee attack
against the translucent bulette
that should hit instead misses, and
the attacker risks falling through
the beast’s form, which acts as a
dimensional portal, and into the astral
plane. The attacker must succeed on a DC
14 Dexterity saving throw to avoid falling through the
bulette and into the Astral Plane.

___Detect Evil and Good.___ The bulette knows if there is an
aberration, celestial, elemental, fey, fiend, or undead within 30 feet of
it and where the creature is located. The bulette also knows if there is a
place or object within 30 feet of it that has been consecrated or desecrated.

___Dual Planar Connection.___ Each translucent bulette is tied so thoroughly
to the Astral and Prime Material Planes that its entire body acts as an open
portal between the two planes. Intentional use of a translucent bulette for
purposeful travel between these two planes is possible with a successful
grapple or melee attack against the creature.

___Limited Invulnerability (Prime Material Plane Only).___ While on the
Prime Material Plane, the bulette is immune to all damage.

___Insubstantial Form.___ The translucent bulette cannot be harmed on the
Prime Material Plane because any damaging attacks merely phase through
its insubstantial form. Physical attacks are only possible when attacking
the creature from the Astral Plane.

___Magic Resistance.___ The bulette has advantage on saving throws against
spells and other magical effects.

___Pop goes the Portal.___ If killed, the body of a translucent bulette
immediately vanishes with a popping sound, severing its living portal
connection between planes.

**Actions**

___Bite.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 33
(4d12 + 7) piercing damage and the target is grappled if the bulette is not
already grappling another creature.

___Portal Swallow.___ The bulette makes a bite attack against a Large or
smaller target it is grappling. If the attack hits, the target is swallowed and
transported to the Astral Plane, and the grapple ends.
