"Ancient Wind Dragon";;;_size_: Gargantuan dragon
_alignment_: chaotic neutral
_challenge_: "22 (41000 XP)"
_languages_: "Common, Draconic, Dwarvish, Elvish, Primordial"
_skills_: "Acrobatics +11, Arcana +11, Intimidation +12, Perception +17, Stealth +11"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 27"
_saving_throws_: "Dex +11, Con +15, Wis +10, Cha +12"
_damage_immunities_: "lightning, ranged weapons"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhausted, paralyzed, restrained"
_speed_: "40 ft., fly 120 ft."
_hit points_: "425 (23d20 +184)"
_armor class_: "20 (natural armor)"
_stats_: | 28 (+9) | 19 (+4) | 26 (+8) | 18 (+4) | 17 (+3) | 20 (+5) |

___Innate Spellcasting.___ The dragon's innate spellcasting ability is Charisma (spell save DC 20). It can innately cast the following spells, requiring no material components:

At will: feather fall

5/day each: lightning bolt, ice storm

___Fog Vision.___ The dragon sees normally through light or heavy obscurement caused by fog, mist, clouds, or high wind.

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The dragon has advantage on saving throws against spells and other magical effects.

___Uncontrollable.___ The dragon's movement is never impeded by difficult terrain, and its speed can't be reduced by spells or magical effects. It can't be restrained (per the condition), and it escapes automatically from any nonmagical restraints (such as chains, entanglement, or grappling) by spending 5 feet of movement. Being underwater imposes no penalty on its movement or attacks.

___Whirling Winds.___ Gale-force winds rage around the dragon, making it immune to ranged weapon attacks except for those from siege weapons.

___Wind Dragon's Lair.___ On initiative count 20 (losing initiative ties), the dragon takes a lair action to generate one of the following effects; the dragon can't use the same effect two rounds in a row.

- Sand and dust swirls up from the floor in a 20-foot radius sphere within 120 feet of the dragon at a point the dragon can see. The sphere spreads around corners. The area inside the sphere is lightly obscured, and each creature in the sphere at the start of its turn must make a successful DC 15 Constitution saving throw or be blinded for 1 minute. A blinded creature repeats the saving throw at the start of each of its turns, ending the effect on itself with a success.

- Fragments of ice and stone are torn from the lair's wall by a blast of wind and flung along a 15-foot cone. Creatures in the cone take 18 (4d8) bludgeoning damage, or half damage with a successful DC 15 Dexterity saving throw.

- A torrent of wind blasts outward from the dragon in a 60-foot radius, either racing just above the floor or near the ceiling. If near the floor, it affects all creatures standing in the radius; if near the ceiling, it affects all creatures flying in the radius. Affected creatures must make a successful DC 15 Strength saving throw or be knocked prone and stunned until the end of their next turn.

**Actions**

___Multiattack.___ The wind dragon can use its Frightful Presence and then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 22 (2d12 + 9) piercing damage.

___Claw.___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 18 (2d8 + 9) slashing damage.

___Tail.___ Melee Weapon Attack: +16 to hit, reach 20 ft., one target. Hit: 20 (2d10 + 9) bludgeoning damage.

___Breath of Gales (Recharge 5-6).___ The dragon exhales a blast of wind in a 90-foot cone. Each creature in that cone takes 55 (10d10) bludgeoning damage and is pushed 50 feet away from the dragon and knocked prone; a successful DC 23 Strength saving throw halves the damage and prevents being pushed (but not being knocked prone). All flames in the cone are extinguished.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 20 Wisdom saving throw or become frightened for 1 minute. A creature repeats the saving throw at the end of its turn, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours.

**Legendary** Actions

___The dragon can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each creature within 10 feet of the dragon must succeed on a DC 24 Dexterity saving throw or take 20 (2d10 + 9) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.

