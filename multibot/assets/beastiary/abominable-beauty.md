Abominable Beauty;;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: 11 (7200 XP)
_languages_: Common, Draconic, Elven, Sylvan
_skills_: Deception +12, Perception +7, Performance +12, Persuasion +12
_senses_: , passive Perception 17
_saving_throws_: Dex +8, Con +8, Cha +12
_damage_immunities_: fire
_speed_: 30 ft.
_hit points_: 187 (22d8 + 88)
_armor class_: 18 (natural armor)
_stats_: | 17 (+3) | 18 (+4) | 18 (+4) | 17 (+3) | 16 (+3) | 26 (+8) |

___Burning Touch.___ The abominable beauty's slam attacks do 28 (8d6) fire damage. A creature who touches her also takes 28 (8d6) fire damage.

**Actions**

___Multiattack.___ The abominable beauty makes two slam attacks.

___Slam.___ +8 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) damage plus 28 (8d6) fire damage.

___Blinding Gaze (Recharge 5-6).___ A creature within 30 feet of the abominable beauty who is targeted by this attack and who meets the abominable beauty's gaze must succeed on a DC 17 Charisma saving throw or be blinded. If the saving throw succeeds, the target creature is permanently immune to this abominable beauty's Blinding Gaze.

___Deafening Voice (Recharge 5-6).___ An abominable beauty's voice is lovely, but any creature within 90 feet and able to hear her when she makes her Deafening Voice attack must succeed on a DC 16 Constitution saving throw or be permanently deafened.

