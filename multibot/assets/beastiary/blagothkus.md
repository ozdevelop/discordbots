"Blagothkus";;;_size_: Huge giant (cloud giant)
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Draconic, Giant"
_skills_: "Arcana +7, Insight +6, Intimidation +6, Perception +6"
_saving_throws_: "Con +9, Wis +6, Cha +6"
_speed_: "40 ft."
_hit points_: "138 (12d12+60)"
_armor class_: "17 (splint)"
_stats_: | 26 (+8) | 13 (+1) | 20 (+5) | 16 (+3) | 15 (+2) | 15 (+2) |

___Keen Smell.___ Blagothkus has advantage on Wisdom (Perception) checks that rely on smell.

___Innate Spellcasting.___ Blagothkus can innately cast the following spells (spell save DC 15), requiring no material components:

* 3/day each: _fog cloud, levitate_

___Spellcasting.___ Blagothkus is a 5th-level spellcaster that uses Intelligence as his spellcasting ability (spell save DC 15, +7 to hit with spell attacks). Blagothkus has the following spells prepared from the wizard spell list:

* Cantrips (at will): _light, mage hand, prestidigitation_

* 1st level (4 slots): _detect magic, identify, magic missile, shield_

* 2nd level (3 slots): _gust of wind, misty step, shatter_

* 3rd level (2 slots): _fly, lightning bolt_

**Actions**

___Multiattack.___ Blagothkus attacks twice with his morningstar.

___Morningstar.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 21 (3d8 + 8) piercing damage.
