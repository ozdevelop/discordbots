"Giant Fly";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 60 ft."
_speed_: "30 ft., fly 60 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "11"
_stats_: | 14 (+2) | 13 (+1) | 13 (+1) | 2 (-4) | 10 (0) | 3 (-4) |