"Ooze Master";;;_size_: Huge undead
_alignment_: lawful evil
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Primordial, Thayan"
_senses_: "blindsight 120 ft."
_skills_: "Arcana +7, Insight +4"
_damage_immunities_: "acid, cold, poison"
_saving_throws_: "Int +7, Wis +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "138 (12d12+60)"
_armor class_: "9 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, poisoned, prone"
_damage_resistances_: "lightning, necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 16 (+3) | 1 (-5) | 20 (+5) | 17 (+3) | 10 (0) | 16 (+3) |

___Source.___ tales from the yawning portal,  page 241

___Undead Nature.___ The ooze master doesn't require air, food, drink, or sleep.

___Corrosive Form.___ A creature that touches the Ooze Master or hits it with a melee attack while within 5 feet of it takes 9 (2d8) acid damage. Any nonmagical weapon that hits the Ooze Master corrodes. After dealing damage, the weapon takes a permanent and cumulative -1 penalty to damage rolls. If its penalty drops to -5, the weapon is destroyed. Nonmagical ammunition that hits the Ooze Master is destroyed after dealing damage.

The Ooze Master can eat through 2-inch-thick, nonmagical wood or metal in 1 round.

___Instinctive Attack.___ When the Ooze Master casts a spell with a casting time of 1 action, it can make one pseudopod attack as a bonus action.

___Spellcasting.___ The Ooze Master is a 9th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). It has the following wizard spells prepared:

* Cantrips (at will): _acid splash, friends, mage hand, poison spray_

* 1st level (4 slots): _charm person, detect magic, magic missile, ray of sickness_

* 2nd level (3 slots): _detect thoughts, Melf's acid arrow, suggestion_

* 3rd level (3 slots): _fear, slow, stinking cloud_

* 4th level (3 slots): _confusion, Evard's evard's black tentacles_

* 5th level (1 slot): _cloudkill_

___Spider Climb.___ The Ooze Master can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 13 (3d6 + 3) bludgeoning damage plus 10 (3d6) acid damage.

**Reactions**

___Instinctive Charm.___ If a creature the Ooze Master can see makes an attack roll against it while within 30 feet of it, the Ooze Master can use a reaction to divert the attack if another creature is within the attack's range. The attacker must make a DC 15 Wisdom saving throw. On a failed save, the attacker targets the creature that is closest to it, not including itself or the Ooze Master. If multiple creatures are closest, the attacker chooses which one to target. On a successful save, the attacker is immune to this Instinctive Charm for 24 hours. Creatures that can't be charmed are immune to this effect.
