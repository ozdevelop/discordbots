"Giant Crab";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_senses_: "blindsight 30 ft."
_skills_: "Stealth +4"
_speed_: "30 ft., swim 30 ft."
_hit points_: "13 (3d8)"
_armor class_: "15 (natural armor)"
_stats_: | 13 (+1) | 15 (+2) | 11 (0) | 1 (-5) | 9 (-1) | 3 (-4) |

___Amphibious.___ The crab can breathe air and water.

**Actions**

___Claw.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) bludgeoning damage, and the target is grappled (escape DC 11). The crab has two claws, each of which can grapple only one target.