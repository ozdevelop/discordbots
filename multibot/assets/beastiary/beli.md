"Beli";;;_size_: Small fey
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Dwarvish, Giant"
_skills_: "Perception +4, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +5"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "cold"
_speed_: "30 ft., fly 30 ft."
_hit points_: "45 (10d6 + 10)"
_armor class_: "15 (natural armor)"
_stats_: | 11 (+0) | 16 (+3) | 12 (+1) | 8 (-1) | 11 (+0) | 14 (+2) |

___Arctic Hunter.___ Beli have advantage on Dexterity (Stealth) checks and Wisdom (Perception) checks made in icy, natural surroundings.

___Cold Regeneration.___ As long as the temperature is below freezing, the beli regains 3 hit points at the start of its turn. If the beli takes fire damage, this trait doesn't function at the start of the beli's next turn. The beli dies only if it starts its turn with 0 hit points and it doesn't regenerate.

___Flyby.___ The beli doesn't provoke an opportunity attack when it flies out of an enemy's reach.

___Innate Spellcasting.___ The beli's innate spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _invisibility_

* 3/day: _chill touch_

**Actions**

___Ice Dagger.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage plus 2 (1d4) cold damage.

___Icy Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 5 (1d4 + 3) piercing damage plus 2 (1d4) cold damage, and the target must make a successful DC 13 Constitution saving throw or gain 2 levels of exhaustion from the arrow's icy chill. If the save succeeds, the target also becomes immune to further exhaustion from beli arrows for 24 hours (but any levels of exhaustion already gained remain in effect). A character who gains a sixth level of exhaustion doesn't die automatically but drops to 0 hit points and must make death saving throws as normal. The exhaustion lasts until the target recovers fully from the cold damage.

