"Mehrim (Goat Demon)";;;_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Abyssal"
_skills_: "Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "31 (7d8)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 10 (+0) | 12 (+1) | 13 (+1) | 16 (+3) |

___Keen Smell.___ The demon has advantage on Wisdom (Perception) checks
that rely on smell.

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Innate Spellcasting.___ The demon’s spellcasting ability is Charisma
(spell save DC 13, +5 to hit with spell attacks). It can innately cast each of
the following spells, requiring no material components:

* At will: _darkness, protection from evil and good, see invisibility_

* 1/day each: _dispel evil and good, dispel magic_

**Actions**

___Multiattack___. The mehrim demon makes three attacks: one with its bite
and two with its hooves.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) piercing damage. If the target is a creature, it must succeed on a DC
10 Constitution saving throw against disease or become poisoned until
the disease is cured. Every 24 hours that elapse, the target must repeat the
saving throw, reducing its hit point maximum by 5 (1d10) on a failure.
The disease is cured on a success. The target dies if the disease reduces its
hit point maximum to 0. This reduction to the target’s hit point maximum
lasts until the disease is cured.

___Hooves.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) slashing damage.
