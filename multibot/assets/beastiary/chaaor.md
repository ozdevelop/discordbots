"Chaaor (Beast Demon)";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Abyssal, telepathy 120 ft."
_skills_: "Athletics +12, Intimidation +10, Perception +6, Insight +6, Stealth +7"
_senses_: "truesight 120 ft., passive Perception 16"
_saving_throws_: "Str +12, Con +9"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "105 (10d10 + 50)"
_armor class_: "17 (natural armor)"
_stats_: | 26 (+8) | 17 (+3) | 20 (+5) | 8 (-1) | 14 (+2) | 14 (+2) |

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Innate Spellcasting.___ The demon’s spellcasting ability is Wisdom (spell
save DC 14, +6 to hit with spell attacks). The demon can innately cast the
following spells, requiring no material components:

* At will: _darkness, jump, see invisibility_

* 1/day: _plane shift_ (self only)

**Actions**

___Multiattack.___ The demon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 18 (3d6 + 8) piercing damage.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 17 (2d8 + 8) slashing damage. If the target is a Medium or smaller creature, it is grappled (escape DC 19). The demon has two claws, each of which can grapple only one target.

___Roar (3/day).___ The demon can emit a loud roar in a 60 radius. Each
creature in the area must make a DC 17 Constitution saving throw. On
a failed saving throw, the target takes 14 (4d6) thunder damage and is
stunned until the end of the demon’s next turn. On a successful saving
throw, the target takes half damage and is not stunned.

___Summon Demon (1/day).___ The demon chooses what to summon and
attempts a magical summoning. A chaaor has a 30% chance of summoning
1d3 vrocks, 1d2 hezrous, or one glabrezu. A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can’t summon other
demons. It remains for 1 minute, until it or its summoner dies, or until its
summoner dismisses it as an action.
