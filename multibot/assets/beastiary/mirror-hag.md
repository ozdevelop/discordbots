"Mirror Hag";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "6 (2300 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_resistances_: "thunder"
_condition_immunities_: "charmed, frightened"
_speed_: "30 ft., fly 10 ft."
_hit points_: "168 (16d8 + 96)"
_armor class_: "16 (natural armor)"
_stats_: | 15 (+2) | 16 (+3) | 22 (+6) | 12 (+1) | 14 (+2) | 19 (+4) |

___Innate Spellcasting.___ The hag's innate spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). She can innately cast the following spells, requiring no material components:

At will: disguise self, inflict wounds (4d10), message, ray of enfeeblement

1/day each: detect thoughts, dispel magic, lightning bolt, locate creature, shillelagh, stinking cloud, teleport

___Magic Resistance.___ The hag has advantage on saving throws against spells and other magical effects.

___Confounding Ugliness.___ When confronting a mirror hag at any range, a creature must make a choice at the start of each of its turns: either avert its eyes so that it has disadvantage on attack rolls against the hag until the start of its next turn, or look at the hag and make a DC 15 Constitution saving throw. Failure on the saving throw leaves the character stunned until the start of its next turn.

**Actions**

___Multiattack.___ A mirror hag can use its Reconfiguring Curse and make one melee attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 21 (4d8 + 3) piercing damage, or 39 (8d8 + 3) piercing damage against a stunned target.

___Quarterstaff.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage.

___Reconfiguring Curse.___ The mirror hag curses a living creature within 60 feet, giving it beastly or hideous features. The target of the reconfiguring curse must succeed on a DC 15 Constitution saving throw or take 1d6 Charisma damage. A successful save renders the target immune to further uses of that hag's curse for 24 hours.

