"Mantrap";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_senses_: "tremorsense 30 ft., passive Perception 10"
_condition_immunities_: "blinded, deafened, exhaustion, prone"
_speed_: "5 ft."
_hit points_: "45 (7d10 +7)"
_armor class_: "12"
_stats_: | 15 (+2) | 14 (+2) | 12 (+1) | 1 (-5) | 10 (0) | 2 (-4) |

___Attractive Pollen (1/Day).___ When the mantrap detects any creatures nearby, it can use its reaction to release pollen out to a radius of 30 feet. Any beast or humanoid within the area must succeed on a DC 11 Wisdom saving throw or be forced to use all its movement on its turns to get as close to the mantrap as possible. An affected target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___False appearance.___ While the mantrap remains motionless, it is indistinguishable from an ordinary tropical plant.

**Actions**

___Engulf.___ Melee Weapon Aitack: +4 to hit, reach 5 ft., one Medium or smaller creature. Hit: The target is trapped inside the mantrap's leafy jaws. While trapped in this way, the target is blinded and restrained, has total cover from attacks and other effects outside the mantrap, and takes 14 (4d6) acid damage at the start of each of the target's turns. If the mantrap dies, the creature inside it is no longer restrained by it. A mantrap can engulf only one creature at a time