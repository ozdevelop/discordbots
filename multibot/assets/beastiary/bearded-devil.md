"Bearded Devil";;;_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_damage_immunities_: "fire, poison"
_saving_throws_: "Str +5, Con +4, Wis +2"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "13 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 16 (+3) | 15 (+2) | 15 (+2) | 9 (-1) | 11 (0) | 11 (0) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

___Steadfast.___ The devil can't be frightened while it can see an allied creature within 30 feet of it.

**Actions**

___Multiattack.___ The devil makes two attacks: one with its beard and one with its glaive.

___Beard.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 6 (1d8 + 2) piercing damage, and the target must succeed on a DC 12 Constitution saving throw or be poisoned for 1 minute. While poisoned in this way, the target can't regain hit points. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Glaive.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 8 (1d10 + 3) slashing damage. If the target is a creature other than an undead or a construct, it must succeed on a DC 12 Constitution saving throw or lose 5 (1d10) hit points at the start of each of its turns due to an infernal wound. Each time the devil hits the wounded target with this attack, the damage dealt by the wound increases by 5 (1d10). Any creature can take an action to stanch the wound with a successful DC 12 Wisdom (Medicine) check. The wound also closes if the target receives magical healing.