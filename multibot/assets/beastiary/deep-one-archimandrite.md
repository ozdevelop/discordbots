"Deep One Archimandrite";;;_size_: Large humanoid
_alignment_: chaotic evil
_challenge_: "8 (3900 XP)"
_languages_: "Common, Void Speech"
_skills_: "Arcana +4, Perception +6"
_senses_: "darkvision 240 ft., passive Perception 16"
_saving_throws_: "Dex +5, Wis +6, Cha +7"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "cold, thunder"
_speed_: "40 ft., swim 40 ft."
_hit points_: "153 (18d10 + 54)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 15 (+2) | 17 (+3) | 12 (+1) | 17 (+3) | 19 (+4) |

___Amphibious.___ A deep one can breathe air or water with equal ease.

___Frenzied Rage.___ On its next turn after a deep one archimandrite takes 10 or more damage from a single attack, it has advantage on its attacks, it adds +4 to damage, and it can make one extra unholy trident attack.

___Innate Spellcasting.___ The deep one archimandrite's innate spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: bless, revivify, sacred flame, shocking grasp, suggestion

3/day each: charm person, lightning bolt, sanctuary, shatter

1/day each: chain lightning, cone of cold, ice storm

___Legendary Resistance (1/Day).___ If the deep one archimandrite fails a saving throw, it can count it as a success instead.

___Lightless Depths.___ A deep one hybrid priest is immune to the pressure effects of the deep ocean.

___Voice of the Archimandrite.___ With a ringing shout, the deep one archimandrite summons all deep ones within a mile to come to his aid. This is not a spell but a command that ocean creatures and deep ones heed willingly.

**Actions**

___Multiattack.___ A deep one archimandrite makes one claw attack and 1 unholy trident attack.

___Claw.___ Melee Weapon Attack. +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) slashing damage.

___Unholy Trident.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 14 (2d8 + 5) piercing damage plus 13 (2d12) necrotic damage.

