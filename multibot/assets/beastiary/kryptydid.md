"Kryptydid";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "passive Perception 14"
_speed_: "40 ft."
_hit points_: "75 (8d10 + 8)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 12 (+1) | 2 (-4) | 14 (+2) | 5 (-3) |

___Cranial Crest.___ The kryptydid's cranial crest is a psionic
extrasensory organ which allows it to sense
predators. The kryptydid automatically detects the
presence of any living creatures that can see, smell,
or otherwise perceive the kryptydid that are within
300 feet of it. The kryptydid is aware of the
creature's presence and knows its general direction,
but not its exact location.

This sense does not detect undead or constructs,
or any creature protected from divination magic,
such as by a nondetection spell.

___Standing Leap.___ The kryptydid's long jump is up to 30
feet and its high jump is up to 15 feet, with or
without a running start.

___Innate Spellcasting (Psionics).___ The kryptydid's innate
spellcasting ability is Wisdom (spell save DC 12). It
can innately cast the following spells, requiring no
components:

* At will: _minor illusion, resistance_

* 1/day each: _hypnotic pattern, true seeing_ (self only)

**Actions**

___Multiattack.___ The kryptydid makes two attacks: one
with its bite and one with its kick. It can't make both
attacks against the same target.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 7 (2d4 + 2) piercing damage.

___Kick.___ Melee Weapon Attack: +5 to hit, reach 10 ft.,
one target. Hit: 10 (2d6 + 3) bludgeoning damage,
and if the target is a creature, it must succeed on a
DC 13 Strength saving throw or be knocked prone.
