"Minotaur";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Abyssal"
_senses_: "darkvision 60 ft."
_skills_: "Perception +7"
_speed_: "40 ft."
_hit points_: "76 (9d10+27)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 11 (0) | 16 (+3) | 6 (-2) | 16 (+3) | 9 (-1) |

___Charge.___ If the minotaur moves at least 10 ft. straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 9 (2d8) piercing damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be pushed up to 10 ft. away and knocked prone.

___Labyrinthine Recall.___ The minotaur can perfectly recall any path it has traveled.

___Reckless.___ At the start of its turn, the minotaur can gain advantage on all melee weapon attack rolls it makes during that turn, but attack rolls against it have advantage until the start of its next turn.

**Actions**

___Greataxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 17 (2d12 + 4) slashing damage.

___Gore.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage.