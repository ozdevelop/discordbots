"Onyx Deer";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_languages_: "understands Sylvan but can't speak"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_condition_immunities_: "petrified"
_speed_: "40 ft."
_hit points_: "76 (9d10 + 27)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 15 (+2) | 17 (+3) | 5 (-3) | 12 (+1) | 13 (+1) |

___Keen Smell.___ The onyx deer has advantage on Wisdom (Perception)
checks that rely on smell.

___Onyx Bite.___ Creatures bitten by an onyx deer must succeed on a
Constitution saving throw or slowly begin to turn into onyx (included in
the attack).

**Actions**

___Multiattack.___ The onyx deer makes four attacks: one bite, one gore,
and two with its hooves.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage and the creature must make a DC 14
Constitution saving throw. On a failed save, the creature magically begins
to turn to onyx and is restrained. It must repeat the saving throw at the end
of its next turn. On a success, the effect ends. On a failure, the creature is
petrified in onyx until freed by a greater restoration spell or other magic.

___Gore.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) piercing damage.

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

___Bellow (Recharge 5–6).___ The onyx deer unleashes a rumbling bellow.
Each creature that is within 60 feet of the onyx deer and able to hear it
must succeed on a DC 14 Wisdom saving throw or be frightened for 1
minute. The frightened creature can repeat the saving throw at the end of
each of its turns, ending the effect on itself on a success.
