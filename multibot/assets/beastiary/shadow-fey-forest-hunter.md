"Shadow Fey Forest Hunter";;;_size_: Medium humanoid
_alignment_: lawful evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Elvish, Umbral"
_skills_: "Arcana +3, Perception +4, Stealth +10, Survival +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +7, Con +4, Cha +6"
_speed_: "30 ft."
_hit points_: "104 (19d8 + 19)"
_armor class_: "15 (chain shirt)"
_stats_: | 12 (+1) | 18 (+4) | 12 (+1) | 11 (+0) | 12 (+1) | 16 (+3) |

___Fey Ancestry.___ The shadow fey has advantage on saving throws against being charmed, and magic can't put it to sleep.

___Innate Spellcasting.___ The shadow fey's innate spellcasting ability is Charisma. It can cast the following spells innately, requiring no material components.

3/day: misty step (when in shadows, dim light, or darkness only)

___Sneak Attack (1/turn).___ The shadow fey forest hunter does an extra 7 (2d6) damage when it hits a target with a weapon attack that had advantage, or when the target is within 5 feet of an ally of the forest hunter that isn't incapacitated and the forest hunter doesn't have disadvantage on the attack roll.

___Sunlight Sensitivity.___ While in sunlight, the shadow fey has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

___Traveler in Darkness.___ The shadow fey has advantage on Intelligence (Arcana) checks made to know about shadow roads and shadow magic spells or items.

**Actions**

___Multiattack.___ The shadow fey makes two ranged attacks.

___Rapier.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Longbow.___ Ranged Weapon Attack: +7 to hit, range 150/600 ft., one target. Hit: 8 (1d8 + 4) piercing damage plus 7 (2d6) poison damage.

