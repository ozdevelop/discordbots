"Clockwork Beetle";;;_size_: Tiny construct
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "understands Common, telepathy 100 ft. (creator only)"
_skills_: "Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +5"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., fly 50 ft."
_hit points_: "15 (6d4)"
_armor class_: "14 (natural armor)"
_stats_: | 8 (-1) | 16 (+3) | 10 (+0) | 4 (-3) | 12 (+1) | 7 (-2) |

___Immutable Form.___ The clockwork beetle is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The clockwork beetle has advantage on saving throws against spells and other magical effects.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 5 (2d4) poison damage, or one-half poison damage with a successful DC 10 Constitution saving throw.

