"Tosculi Drone";;;_size_: Small monstrosity
_alignment_: lawful evil
_challenge_: "1/2 (100 XP)"
_languages_: "Tosculi"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "20 ft."
_hit points_: "22 (4d6 + 8)"
_armor class_: "13"
_stats_: | 8 (-1) | 16 (+3) | 14 (+2) | 8 (-1) | 12 (+1) | 4 (-3) |

___Gliding Wings.___ The tosculi drone can use its wings to slowly descend when falling (as if under the effect of the feather fall spell). It can move up to 5 feet horizontally for every foot it falls. The tosculi drone can't gain height with these wings alone. If subjected to a strong wind or lift of any kind, it can use the updraft to glide farther.

___Skittering.___ Up to two tosculi can share the same space at one time. The tosculi has advantage on melee attack rolls while sharing its space with another tosculi that isn't incapacitated.

**Actions**

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) slashing damage.

___Light Crossbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

