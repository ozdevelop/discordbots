"Warrior Prince/Princess";;;_size_: Medium humanoid (any race)
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "any two languages"
_skills_: "Athletics +10, Acrobatics +7, Perception +5, Persuasion +5"
_senses_: "passive Perception 15"
_saving_throws_: "Str +7, Dex +7, Con +5"
_speed_: "40 ft."
_hit points_: "104 (16d8 + 32)"
_armor class_: "18 (breastplate, shield)"
_stats_: | 18 (+4) | 18 (+4) | 14 (+2) | 15 (+2) | 14 (+2) | 14 (+2) |

___Enchanted Armaments.___ The princess's weapons and
shield are magical.

___Supernatural Athletics.___ The princess's long jump is 30
feet and her jump height is 15 feet, with or without a
running start. If the princess leaps at least 20 feet in a
straight line towards a target and hits it with a shield
bash attack on the same turn, that target must succeed
on a DC 15 Constitution saving throw or become
stunned until the start of its next turn.

**Actions**

___Multiattack.___ The princess makes three attacks: two with
her longsword and one with her shield bash or three
with her javelins.

___Longsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 8 (1d8 + 4) slashing damage plus 7
(2d6) poison damage, or 9 (1d10 +4) slashing damage
plus 7 (2d6) poison damage if used with two hands.

___Shield Bash.___ Melee Weapon Attack: +7 to hit, reach 5
ft., one target. Hit: 6 (1d4 + 4) bludgeoning damage
and the target must succeed on a DC 15 Strength
saving throw or be pushed 10 feet away from the
princess and knocked prone.

___Javelin.___ Melee or Ranged Weapon Attack: +7 to hit,
reach 5 ft. or range 30/120 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 7 (2d6) poison damage.

___Shield Toss (Recharge 5-6).___ The princess launches her
shield with expert precision and force towards a target
she can see within 30 feet. The shield then leaps to up
to three other targets, each of which must be within
30 feet of the first target. A target can be a creature or
an object and can only be struck by the shield one
time. Each creature targeted by this ability must
succeed on a DC 15 Dexterity saving throw or take 22
(4d10) bludgeoning damage. At the end of the toss,
the shield returns to the princess and she immediately
readies it for combat.

**Reactions**

___Deflect Projectile.___ The princess uses her expertise with
a shield to block a projectile attack that would
otherwise hit. If the princess is holding her shield, she
can gain a +3 bonus to AC against a projectile attack
that she can see that targets only her.
