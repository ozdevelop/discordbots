"Flame Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Draconic, Ignan"
_skills_: "Deception +5, Insight +3, Perception +5, Persuasion +5, Stealth +4"
_senses_: ", passive Perception 15"
_saving_throws_: "Dex +4, Con +4, Wis +3, Cha +5"
_damage_immunities_: "fire"
_speed_: "30 ft., climb 30 ft., fly 60 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "17 (natural armor)"
_stats_: | 12 (+1) | 14 (+2) | 15 (+2) | 13 (+1) | 12 (+1) | 16 (+3) |

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (1d10+1) piercing damage plus 3 (1d6) fire damage.

___Fire Breath (Recharge 5-6).___ The dragon exhales fire in a 10-foot cone. Each creature in that area takes 24 (7d6) fire damage, or half damage with a successful DC 12 Dexterity saving throw.

