"Triceratops";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_speed_: "50 ft."
_hit points_: "95 (10d12+30)"
_armor class_: "13 (natural armor)"
_stats_: | 22 (+6) | 9 (-1) | 17 (+3) | 2 (-4) | 11 (0) | 5 (-3) |

___Trampling Charge.___ If the triceratops moves at least 20 ft. straight toward a creature and then hits it with a gore attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is prone, the triceratops can make one stomp attack against it as a bonus action.

**Actions**

___Gore.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 24 (4d8 + 6) piercing damage.

___Stomp.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one prone creature. Hit: 22 (3d10 + 6) bludgeoning damage