"Local Champion";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Athletics +5, Intimidation +4, Performance +4"
_senses_: "passive Perception 11"
_saving_throws_: "Str +5, Dex +2"
_speed_: "30 ft."
_hit points_: "30 (4d10 + 8)"
_armor class_: "16 (chainmail)"
_stats_: | 16 (+3) | 11 (+0) | 15 (+2) | 10 (+0) | 12 (+1) | 14 (+2) |

___Great Weapon Fighting.___ When the champion rolls a
1 or 2 on a damage die for an attack with a melee
weapon, it can reroll the die and must use the new
roll.

___Improved Critical.___ The champion's weapon attacks
score a critical hit on a result of 19 or 20.

**Actions**

___Glaive.___ Melee Weapon Attack: +5 to hit, reach 10
ft., one target. Hit: 8 (1d10 + 3) slashing damage.

___Action Surge (1/Short Rest).___ The champion makes
two glaive attacks.
