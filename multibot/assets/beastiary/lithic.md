"Lithic";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Terran"
_skills_: "Perception +2, Stealth +3"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 12"
_damage_vulnerabilities_: "thunder"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "45 (7d8 + 14)"
_armor class_: "17 (natural armor)"
_stats_: | 17 (+3) | 12 (+1) | 15 (+2) | 10 (+0) | 11 (+0) | 10 (+0) |

___Blend With Stone.___ The lythic has advantage on Dexterity (Stealth)
checks in rocky terrain, and can take the Hide action whenever it is within
10 feet of some sort of stone, and remains hidden unless it moves or
attacks.

___Earth Glide.___ The elemental can burrow through nonmagical, unworked
earth and stone. While doing so, the elemental doesn’t disturb the material
it moves through.

___Fury of the Earth.___ Whenever the lythic starts its turn with half its hit
points or fewer, it flies into a berserker-like rage. On each of its turns while
berserk, the lythic deals an additional 7 (2d6) damage with its slam attack.

**Actions**

___Multiattack.___ The lythic makes two slam attacks.

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) bludgeoning damage.
