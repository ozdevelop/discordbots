"Grisha";;;_size_: Medium humanoid (damaran human)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Undercommon"
_skills_: "Persuasion +5, Religion +2"
_saving_throws_: "Wis +4, Cha +5"
_speed_: "30 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "18 (chain mail, shield)"
_stats_: | 14 (+2) | 12 (+1) | 12 (+1) | 11 (0) | 14 (+2) | 16 (+3) |

___Spellcasting.___ Grisha is a 6th-level-spellcaster. His spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). He has the following cleric spells prepared:

* Cantrips (at will): _guidance, light, sacred flame, thaumaturgy_

* 1st level (4 slots): _cure wounds, divine favor, inflict wounds, protection from good, shield of faith_

* 2nd level (3 slots): _continual flame, hold person, magic weapon, spiritual weapon_

* 3rd level (3 slots): _bestow curse, dispel magic, spirit guardian_

**Actions**

___Multiattack.___ Grisha makes two attacks with his +1 flail.

___+1 Flail.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit:7 (1d8 + 3) bludgeoning damage
