"Glass-Infused Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_damage_vulnerabilities_: "bludgeoning, thunder"
_speed_: "40 ft."
_hit points_: "27 (6d6 + 6)"
_armor class_: "12"
_stats_: | 8 (-1) | 14 (+2) | 12 (+1) | 12 (+1) | 10 (+0) | 8 (-1) |

___Shattering Blast.___ When the alchemist dies, it
explodes in a burst of sharp glass. Each creature
within 5 feet of it must make a make a DC 11
Dexterity saving throw, taking 7 (2d6) slashing
damage on a failed save, or half as much damage on
a successful one.

___Reflective Skin.___ The alchemist has advantage on
saving throws against spells and other magical
effects.

**Actions**

___Multiattack.___ The alchemist makes two attacks with
its slashing strike

___Slashing Strike.___ Melee Weapon Attack: +4 to hit,
reach 5ft., one target. Hit: 5 (1d6 + 2) slashing damage.
