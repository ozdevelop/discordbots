"Scorpion";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "blindsight 10 ft."
_speed_: "10 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "11 (natural armor)"
_stats_: | 2 (-4) | 11 (0) | 8 (-1) | 1 (-5) | 8 (-1) | 2 (-4) |

**Actions**

___Sting.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one creature. Hit: 1 piercing damage, and the target must make a DC 9 Constitution saving throw, taking 4 (1d8) poison damage on a failed save, or half as much damage on a successful one.