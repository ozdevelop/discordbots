"Young Blue Dragon";;;_size_: Large dragon
_alignment_: lawful evil
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Perception +9, Stealth +4"
_damage_immunities_: "lightning"
_saving_throws_: "Dex +4, Con +8, Wis +5, Cha +7"
_speed_: "40 ft., burrow 40 ft., fly 80 ft."
_hit points_: "152 (16d10+64)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 10 (0) | 19 (+4) | 14 (+2) | 13 (+1) | 17 (+3) |

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) piercing damage plus 5 (1d10) lightning damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

___Lightning Breath (Recharge 5-6).___ The dragon exhales lightning in an 60-foot line that is 5 feet wide. Each creature in that line must make a DC 16 Dexterity saving throw, taking 55 (10d10) lightning damage on a failed save, or half as much damage on a successful one.