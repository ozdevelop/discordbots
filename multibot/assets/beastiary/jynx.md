"Jynx";;;_size_: Small fey
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Sylvan"
_skills_: "Acrobatics +5, Nature +4, Perception +4, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "25 ft., fly 30 ft."
_hit points_: "27 (6d6 + 6)"
_armor class_: "13"
_stats_: | 7 (-2) | 16 (+3) | 13 (+1) | 15 (+2) | 14 (+2) | 16 (+3) |

___Innate Spellcasting.___ The jynx’s innate spellcasting ability is Charisma
(spell save DC 13, +5 to hit with spell attacks). The jynx can cast the
following spells without requiring material components:

* At will: _detect evil and good, detect magic_

* 1/day each: _color spray, detect thoughts, dispel magic, entangle_

**Actions**

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one
target. Hit: 6 (1d6 + 3) piercing damage.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target.
Hit: 6 (1d6 + 3) piercing damage.

___Jinx (1/Short or Long Rest).___ The jynx chooses one target that it can
see within 60 feet of it to make a DC 13 Wisdom saving throw. On a
failed saving throw, the creature is jinxed for 1 minute. While jinxed,
the creature has disadvantage on all attack rolls, saving throws, and skill
checks until the jinx ends. The jinx can be ended early with remove curse.
