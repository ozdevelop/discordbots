"Blood Orchid";;;_size_: Large aberration
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "telepathy 120 ft."
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "thunder"
_damage_resistances_: "acid, cold, lightning, fire"
_speed_: "5 ft., fly 30 ft."
_hit points_: "76 (9d10 + 27)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 12 (+1) | 16 (+3) | 11 (+0) | 12 (+1) | 13 (+1) |

___Hyper-Awareness.___ A blood orchid cannot be surprised.

___Telepathic Bond.___ Blood orchids have a telepathic link to other
blood orchids that are within 120 feet.

**Actions**

___Multiattack.___ The blood orchid uses Blood Drain. It then makes
up to three attacks with its tentacles.

___Tentacles.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 10 (2d8 + 1) bludgeoning damage and the target must
succeed on a DC 13 Constitution saving throw or be poisoned for
1 hour. The target is also grappled (escape DC 11). While
grappled this way, the creature is restrained. Until the
grapple ends, the blood orchid can’t use this tentacle on
another target. The blood orchid has three tentacles that it
can attack with.

___Blood Drain.___ The blood orchid feeds on the creature it is grappling.
The creature must succeed on a DC 13 Constitution saving throw or its
hit point maximum is reduced by 5 (1d10). This reduction lasts until the
creature finishes a long rest. The target dies if this effect reduces its hit
point maximum to 0.
