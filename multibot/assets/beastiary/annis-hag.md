"Annis Hag";;;_size_: Large fey
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Giant, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Deception +5, Perception +5"
_saving_throws_: "Con +5"
_speed_: "40 ft."
_hit points_: "75 (10d10+20)"
_armor class_: "17 (natural armor)"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 21 (+5) | 12 (+1) | 14 (+2) | 13 (+1) | 14 (+2) | 15 (+2) |

___Innate Spellcasting.___ The hag's innate spellcasting ability is Charisma (spell save DC 13). She can innately cast the following spells:

3/day each: _disguise self_ (including the form of a Medium humanoid), _fog cloud_

___Hag Coven.___ When hags must work together, they form covens, in spite of their selfish natures. A coven is made up of hags of any type, all of whom are equals within the group. However, each of the hags continues to desire more personal power.

A coven consists of three hags so that any arguments between two hags can be settled by the third. If more than three hags ever come together, as might happen if two covens come into conflict, the result is usually chaos.

___Shared Spellcasting (Coven Only).___ While all three members of a hag coven are within 30 feet of one another, they can each cast the following spells from the wizard's spell list but must share the spell slots among themselves. For casting these spells, each hag is a 12th-level spellcaster that uses Intelligence as her spellcasting ability. The spell save DC is 12 + the hag's Intelligence modifier, and the spell attack bonus is 4 + the hag's Intelligence modifier.

* 1st level (4 slots): _identify, ray of sickness_

* 2nd level (3 slots): _hold person, locate object_

* 3rd level (3 slots): _bestow curse, counterspell, lightning bolt_

* 4th level (3 slots): _phantasmal killer, polymorph_

* 5th level (2 slots): _contact other plane, scrying_

* 6th level (1 slot): _eyebite_

___Hag Eye (Coven Only).___ A hag coven can craft a magic item called a hag eye, which is made from a real eye coated in varnish and often fitted to a pendant or other wearable item. The hag eye is usually entrusted to a minion for safekeeping and transport. A hag in the coven can take an action to see what the hag eye sees if the hag eye is on the same plane of existence. A hag eye has AC 10, 1 hit point, and darkvision with a radius of 60 feet. If it is destroyed, each coven member takes 3d10 psychic damage and is blinded for 24 hours.

A hag coven can have only one hag eye at a time, and creating a new one requires all three members of the coven to perform a ritual. The ritual takes 1 hour, and the hags can't perform it while blinded. During the ritual, if the hags take any action other than performing the ritual, they must start over.

___Variant: Death Coven.___ For a death coven, replace the spell list of the hags' Shared Spellcasting trait with the folllowing spells:

* 1st level (4 slots): _false life, inflict wounds_

* 2nd level (3 slots): _gentle repose, ray of enfeeblement_

* 3rd level (3 slots): _animate dead, revivify, speak with dead_

* 4th level (3 slots): _blight, death ward_

* 5th level (2 slots): _contagion, raise dead_

* 6th level (1 slot): _circle of death_

___Variant: Nature Coven.___ For a nature coven, replace the spell list of the hags' Shared Spellcasting trait with the folllowing spells:

* 1st level (4 slots): _entangle, speak with animals_

* 2nd level (3 slots): _flaming sphere, moonbeam, spike growth_

* 3rd level (3 slots): _call lightning, plant growth_

* 4th level (3 slots): _dominate beast, grasping vine_

* 5th level (2 slots): _insect plague, tree stride_

* 6th level (1 slot): _wall of thorns_

___Variant: Prophecy Coven.___ For a prophecy coven, replace the spell list of the hags' Shared Spellcasting trait with the folllowing spells:

* 1st level (4 slots): _bane, bless_

* 2nd level (3 slots): _augury, detect thoughts_

* 3rd level (3 slots): _clairvoyance, dispel magic, nondetection_

* 4th level (3 slots): _arcane eye, locate creature_

* 5th level (2 slots): _geas, legend lore_

* 6th level (1 slot): _true seeing_

**Actions**

___Multiattack.___ The annis makes three attacks: one with her bite and two with her claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 15 (3d6+5) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 15 (3d6+5) slashing damage.

___Crushing Hug.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 36 (9d6+5) bludgeoning damage, and the target is grappled (escape DC 15) if it is a Large or smaller creature. Until the grapple ends, the target takes 36 (9d6+5) bludgeoning damage at the start of each of the hag's turns. The hag can't make attacks while grappling a creature in this way.
