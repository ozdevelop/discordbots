"Dullahan";;;_size_: Large fey
_alignment_: lawful evil
_challenge_: "11 (7200 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Intimidation +7, Perception +6, Persuasion +7, Survival +6"
_senses_: "blindsight 60 ft., passive Perception 16"
_damage_immunities_: "necrotic"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, frightened, exhaustion"
_speed_: "60 ft."
_hit points_: "178 (17d10 + 85)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 18 (+4) | 20 (+5) | 13 (+1) | 15 (+2) | 17 (+3) |

___Baleful Glare.___ When a creature that can see the eyes of the dullahan's severed head starts its turn within 30 feet of the dullahan, the dullahan can force it to make a DC 15 Wisdom saving throw if the dullahan isn't incapacitated and can see the creature. On a failed save, the creature is frightened until the start of its next turn. While frightened in this way the creature must move away from the dullahan, and can only use its action to Dash. If the creature is affected by the dullahan's Deathly Doom trait, it is restrained while frightened instead. Unless surprised, a creature can avert its eyes to avoid the saving throw at the start of its turn. If the creature does so, it can't see the dullahan until the start of its next turn, when it can avert its eyes again. If the creature looks at the dullahan in the meantime, it must immediately make the save.

___Deathly Doom (1/Day).___ As a bonus action, the dullahan magically dooms a creature. The dullahan knows the direction to the doomed creature as long as it is on the same plane.

___Innate Spellcasting.___ The dullahan's innate spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). The dullahan can innately cast the following spells, requiring no material or somatic components:

At will: bane, chill touch, hex, knock

3/day each: false life, see invisibility

1/day: blight

___Relentless Advance.___ The dullahan is unaffected by difficult terrain, and can ride over water and other liquid surfaces.

**Actions**

___Multiattack.___ The dullahan makes two attacks with its spine whip.

___Spine Whip.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 15 (2d10 + 4) slashing damage plus 10 (3d10) necrotic damage. If the target is a creature it must make a DC 15 Constitution saving throw or be wracked with pain and fall prone.

___Seal the Doom.___ The dullahan points at a creature marked by Deathly Doom within 40 feet than it can see. The creature must succeed at a DC 15 Constitution saving throw against this magic or immediately drop to 0 hit points. A creature that successfully saves is immune to this effect for 24 hours.

**Reactions**

___Interposing Glare.___ When the dullahan is hit by a melee attack it can move its severed head in front of the attacker's face. The attacker is affected by the dullahan's Baleful Glare immediately. If the creature is averting its eyes this turn, it must still make the save, but does so with advantage.

