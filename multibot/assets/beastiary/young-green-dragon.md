"Young Green Dragon";;;_size_: Large dragon
_alignment_: lawful evil
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Deception +5, Perception +7, Stealth +4"
_damage_immunities_: "poison"
_saving_throws_: "Dex +4, Con +6, Wis +4, Cha +5"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "136 (16d10+48)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 19 (+4) | 12 (+1) | 17 (+3) | 16 (+3) | 13 (+1) | 15 (+2) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 15 (2d10 + 4) piercing damage plus 7 (2d6) poison damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Poison Breath (Recharge 5-6).___ The dragon exhales poisonous gas in a 30-foot cone. Each creature in that area must make a DC 14 Constitution saving throw, taking 42 (12d6) poison damage on a failed save, or half as much damage on a successful one.