"Velociraptor";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_skills_: "Perception +3"
_speed_: "30 ft."
_hit points_: "10 (3d4+3)"
_armor class_: "13 (natural armor)"
_stats_: | 6 (-2) | 14 (+2) | 13 (+1) | 4 (-3) | 12 (+1) | 6 (-2) |

___Pack Tactics.___ The velociraptor has advantage on an attack roll against a creature if at least one of the velociraptor's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The velociraptor makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 5 (1d6+2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4+2) slashing damage.