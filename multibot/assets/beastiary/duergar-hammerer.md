"Duergar Hammerer";;;_page_number_: 188
_size_: Medium construct
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "understands Dwarvish but can't speak"
_senses_: "darkvision 60 ft., passive Perception 7"
_damage_immunities_: "poison"
_speed_: "20 ft."
_hit points_: "33  (6d8 + 6)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 17 (+3) | 7 (-1) | 12 (+1) | 5 (-2) | 5 (-2) | 5 (-2) |

___Engine of Pain.___ Once per turn, a creature that attacks the hammerer can target the duergar trapped in it. The attacker has disadvantage on the attack roll. On a hit, the attack deals an extra 5 (1d10) damage to the hammerer, and the hammerer can respond by using its Multiattack with its reaction.

___Siege Monster.___ The hammerer deals double damage to objects and structures.

**Actions**

___Multiattack___ The hammerer makes two attacks: one with its claw and one with its hammer.

___Claw___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage.

___Hammer___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage.