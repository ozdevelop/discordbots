"Totivillus, Scribe Of Hell";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "24 (62000 XP)"
_languages_: "Common, Celestial, Draconic, Infernal, Void Speech; telepathy 120 ft."
_skills_: "Arcana +15, History +15, Investigation +15, Perception +13, Religion +15"
_senses_: "truesight 30 ft., darkvision 60 ft., passive Perception 23"
_saving_throws_: "Dex +11, Con +14, Int +15, Wis +13, Cha +11"
_damage_immunities_: "fire, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 60 ft."
_hit points_: "299 (26d8 + 182)"
_armor class_: "19 (natural armor)"
_stats_: | 18 (+4) | 18 (+4) | 24 (+7) | 26 (+8) | 22 (+6) | 18 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Fear Aura.___ Any creature hostile to Totivillus that starts its turn within 20 feet of him must make a DC 21 Wisdom saving throw, unless Totivillus is incapacitated. On a failed save, the creature is frightened until the start of its next turn. If a creature's saving throw is successful, it is immune to the devil's Fear Aura for the next 24 hours.

___Spellcasting.___ Totivillus is a 20th-level spellcaster who uses Intelligence as his spellcasting ability (spell save DC 23, +15 to hit with spell attacks). He requires no material components to cast his spells. Totivillus has the following wizard spells prepared:

Cantrips (at will): chill touch, light, minor illusion, poison spray, prestidigitation

1st level (4 slots): comprehend languages, disguise self, illusory script, magic missile, unseen servant

2nd level (3 slots): blindness/deafness, hold person, mirror image, misty step

3rd level (3 slots): counterspell, dispel magic, haste, protection from energy

4th level (3 slots): banishment, dimension door, greater invisibility, polymorph

5th level (3 slots): dominate person, geas, modify memory, passwall

6th level (2 slots): eyebite, guards and wards, mass suggestion

7th level (2 slots): forcecage, plane shift

8th level (1 slot): feeblemind

9th level (1 slot): time stop

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Totivillus's weapon attacks are magical.

___Meld with Text.___ Totivillus can enter any book, scroll, or other written material and remain hidden there as long as he wishes. If the text is damaged or destroyed, he is ejected from it without suffering damage.

___Trust Aura.___ Totivillus projects a 25-foot-radius trust aura. As long as Totivillus is talking, creatures in that area find his utterances so fascinating and compelling that they must make a DC 23 Wisdom saving throw at the start of each of their turns; if the saving throw fails, they can't attack Totivillus directly until the start of their next turn. This effect ends immediately and can't be renewed for 1 minute if Totivillus attacks physically. Devils are not immune to this aura.

**Actions**

___Multiattack.___ Totivillus makes two claw attacks.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 30 (4d12 + 4) slashing damage.

___Hellfire Bolt (Recharge 5-6).___ Ranged Spell Attack: +15 to hit, range 120 ft., one target. Hit: 65 (10d12) force damage plus 33 (6d10) thunder damage; a successful DC 20 Dexterity saving throw halves thunder damage.

**Legendary** Actions

___Totivillus can take 3 legendary actions, choosing from the options below.___ Only one option can be used at a time and only at the end of another creature's turn. Totivillus regains spent legendary actions at the start of its turn.

___Claw Attack.___ Totivillus makes one claw attack.

___Devil's Mark.___ Totivillus sprays magical ink from his fingertips at a single target within 30 feet. The target must make a successful DC 23 Dexterity saving throw or receive a devil's mark: a tattoo in the shape of Totivillus's personal seal. All devils have advantage on spell attacks made against the devil.marked creature, and the creature has disadvantage on saving throws made against spells and abilities used by devils. The mark can be removed by remove curse if the caster also makes a successful DC 23 spellcasting check. The mark reveals itself as desecrated to detect evil and good. It often shifts its position on the body, especially when it's concealed (and usually at the most inconvenient moment). Because such marks are sometimes placed on those who've made pacts with devils, NPC paladins and clerics might assume that any character bearing a devil's mark is in league with evil forces.

___Cast a Spell (Costs 3 Actions).___ Totivillus casts a spell from its list of prepared spells, using a spell slot as normal.

