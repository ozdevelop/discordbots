"Shambling Mound";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_skills_: "Stealth +2"
_damage_immunities_: "lightning"
_speed_: "20 ft., swim 20 ft."
_hit points_: "136 (16d10+48)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "blinded, deafened, exhaustion"
_damage_resistances_: "cold, fire"
_stats_: | 18 (+4) | 8 (-1) | 16 (+3) | 5 (-3) | 10 (0) | 5 (-3) |

___Lightning Absorption.___ Whenever the shambling mound is subjected to lightning damage, it takes no damage and regains a number of hit points equal to the lightning damage dealt.

**Actions**

___Multiattack.___ The shambling mound makes two slam attacks. If both attacks hit a Medium or smaller target, the target is grappled (escape DC 14), and the shambling mound uses its Engulf on it.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.

___Engulf.___ The shambling mound engulfs a Medium or smaller creature grappled by it. The engulfed target is blinded, restrained, and unable to breathe, and it must succeed on a DC 14 Constitution saving throw at the start of each of the mound's turns or take 13 (2d8 + 4) bludgeoning damage. If the mound moves, the engulfed target moves with it. The mound can have only one creature engulfed at a time.