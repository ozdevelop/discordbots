"Duergar Mind Master";;;_page_number_: 189
_size_: Medium humanoid (dwarf)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft., truesight 30 ft., passive Perception 12"
_skills_: "Perception +2, Stealth +5"
_saving_throws_: "Wis +2"
_speed_: "25 ft."
_hit points_: "39  (6d8 + 12)"
_armor class_: "14 (leather armor)"
_damage_resistances_: "poison"
_stats_: | 11 (0) | 17 (+3) | 14 (+2) | 15 (+2) | 10 (0) | 12 (+1) |

___Duergar Resilience.___ The duergar has advantage on saving throws against poison, spells, and illusions, as well as to resist being charmed or paralyzed.

___Sunlight Sensitivity.___ While in sunlight, the duergar has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The duergar makes two melee attacks. It can replace one of those attacks with a use of Mind Mastery.

___Mind-Poison Dagger___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage and 10 (3d6) psychic damage, or 1 piercing damage and 14 (4d6) psychic damage while reduced.

___Invisibility (Recharge 4-6)___ The duergar magically turns invisible for up to 1 hour or until it attacks, it casts a spell, it uses its Reduce, or its concentration is broken (as if concentrating on a spell). Any equipment the duergar wears or carries is invisible with it.

___Mind Mastery___ The duergar targets one creature it can see within 60 feet of it. The target must succeed on a DC 12 Intelligence saving throw, or the duergar causes it to use its reaction either to make one weapon attack against another creature the duergar can see or to move up to 10 feet in a direction of the duergar's choice. Creatures that can't be charmed are immune to this effect.

___Reduce (Recharges after a Short or Long Rest)___ For 1 minute, the duergar magically decreases in size, along with anything it is wearing or carrying. While reduced, the duergar is Tiny, reduces its weapon damage to 1, and makes attacks, checks, and saving throws with disadvantage if they use Strength. It gains a +5 bonus to all Dexterity (Stealth) checks and a +5 bonus to its AC. It can also take a bonus action on each of its turns to take the Hide action.