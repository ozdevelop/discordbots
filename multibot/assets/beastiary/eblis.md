"Eblis";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_Languages_: "Auran, Common"
_senses_: "passive Perception 14"
_skills_: "Perception +4"
_speed_: "30 ft., fly 40 ft."
_hit points_: "26 (4d10 +4)"
_armor class_: "13"
_stats_: | 11 (+0) | 16 (+3) | 12 (+1) | 12 (+1) | 14 (+2) | 11 (0) |

___Innate Spellcasting.___ The eblis's innate spellcasting ability is Intelligence (spell save DC 11). It can innately cast the following spells, requiring no material components:

* 1/day each: _blur, hypnotic pattern, minor illusion_

**Actions**

___Multiattack.___ The eblis attacks twice with its beak.

___Beak.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 +3) piercing damage.
