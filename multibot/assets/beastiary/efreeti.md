"Efreeti";;;_size_: Large elemental
_alignment_: lawful evil
_challenge_: "11 (7,200 XP)"
_languages_: "Ignan"
_senses_: "darkvision 120 ft."
_damage_immunities_: "fire"
_saving_throws_: "Int +7, Wis +6, Cha +7"
_speed_: "40 ft., fly 60 ft."
_hit points_: "200 (16d10+112)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 12 (+1) | 24 (+7) | 16 (+3) | 15 (+2) | 16 (+3) |

___Elemental Demise.___ If the efreeti dies, its body disintegrates in a flash of fire and puff of smoke, leaving behind only equipment the djinni was wearing or carrying.

___Innate Spellcasting.___ The efreeti's innate spell casting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _detect magic_

* 3/day: _enlarge/reduce, tongues_

* 1/day each: _conjure elemental _(fire elemental only)_, gaseous form, invisibility, major image, plane shift, wall of fire_

___Variant: Genie Powers.___ Genies have a variety of magical capabilities, including spells. A few have even greater powers that allow them to alter their appearance or the nature of reality.

___Disguises.___ Some genies can veil themselves in illusion to pass as other similarly shaped creatures. Such genies can innately cast the disguise self spell at will, often with a longer duration than is normal for that spell. Mightier genies can cast the true polymorph spell one to three times per day, possibly with a longer duration than normal. Such genies can change only their own shape, but a rare few can use the spell on other creatures and objects as well.

___Wishes.___ The genie power to grant wishes is legendary among mortals. Only the most potent genies, such as those among the nobility, can do so. A particular genie that has this power can grant one to three wishes to a creature that isn't a genie. Once a genie has granted its limit of wishes, it can't grant wishes again for some amount of time (usually 1 year). and cosmic law dictates that the same genie can expend its limit of wishes on a specific creature only once in that creature's existence.

To be granted a wish, a creature within 60 feet of the genie states a desired effect to it. The genie can then cast the wish spell on the creature's behalf to bring about the effect. Depending on the genie's nature, the genie might try to pervert the intent of the wish by exploiting the wish's poor wording. The perversion of the wording is usually crafted to be to the genie's benefit.

**Actions**

___Multiattack.___ The efreeti makes two scimitar attacks or uses its Hurl Flame twice.

___Scimitar.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage plus 7 (2d6) fire damage.

___Hurl Flame.___ Ranged Spell Attack: +7 to hit, range 120 ft., one target. Hit: 17 (5d6) fire damage.
