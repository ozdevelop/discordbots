"Young Gray Dragon";;;_size_: Large dragon
_alignment_: neutral evil
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Draconic"
_skills_: "Perception +8, Stealth +5"
_senses_: "blindsight 30 ft., darkvision 120 ft., passive Perception 18"
_saving_throws_: "Dex +5, Con +8, Wis +4, Cha +7"
_damage_immunities_: "fire"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "142 (15d10 + 60)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 13 (+1) | 18 (+4) | 13 (+1) | 11 (+0) | 16 (+3) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 15
(2d10 + 4) piercing damage plus 5 (1d10) fire damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) slashing damage.

___Steam Breath (Recharge 5–6).___ The dragon exhales steam in a 30 cone.
Creatures in the area must make a DC 15 Constitution saving throw. On
a failed saving throw, the creature takes 52 (15d6) fire damage, or half as
much damage on a successful saving throw. Being underwater doesn’t
grant resistance to this damage.
