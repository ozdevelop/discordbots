"Blood Orc";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Orc"
_skills_: "Intimidation +0"
_senses_: "darkvision 60 ft., passive Perception 8"
_speed_: "30 ft."
_hit points_: "13 (2d8 + 4)"
_armor class_: "14 (chain shirt)"
_stats_: | 15 (+2) | 13 (+1) | 14 (+2) | 8 (-1) | 6 (-2) | 6 (-2) |

___Bloodfrenzy.___ When the blood orc begins its turn with half or fewer of
its hit points, it can make a bite attack as a bonus action when it takes
the Attack action, and it has advantage on Intelligence, Wisdom, and
Charisma saving throws against spells and other effects.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Greataxe.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 8 (1d12 + 2) slashing damage.
