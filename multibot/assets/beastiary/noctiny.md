"Noctiny";;;_size_: Medium humanoid
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Common, plus the language spoken by the noctini's master"
_senses_: "assive Perception 10"
_condition_immunities_: "frightened"
_speed_: "30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "13 (studded leather armor)"
_stats_: | 12 (+1) | 13 (+1) | 14 (+2) | 10 (+0) | 11 (+0) | 16 (+3) |

___Magic Resistance.___ The noctiny has advantage on saving throws against spells and other magical effects.

___Pact Wrath.___ One of the noctiny's weapons is infused with power. Its attacks with this weapon count as magical, and it adds its Charisma bonus to the weapon's damage (included below).

___Spellcasting.___ The noctiny is a 3rd-level spellcaster. Its spellcasting ability score is Charisma (save DC 13, +5 to hit with spell attacks). It knows the following warlock spells.

* Cantrips (at will): _chill touch, eldritch blast, poison spray_

* 1st level (4 slots): _armor of agathys, charm person, hellish rebuke, hex_

* 2nd level (2 slots): _crown of madness, misty step_

**Actions**

___Multiattack.___ The noctiny makes two melee attacks.

___Pact Staff.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage or 8 (1d8 + 4) bludgeoning damage if used in two hands.

