"Wendigo";;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP), 9 when bloodied (5,000 XP)"
_languages_: "Infernal"
_skills_: "Stealth +5, Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Str +7 (+9 when bloodied), Dex +5, Con +7"
_damage_immunities_: "cold, poison"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "35 ft."
_hit points_: "130 (20d8 + 40)"
_armor class_: "16 (natural armor)"
_stats_: | 18 (+4) | 15 (+2) | 18 (+4) | 10 (+0) | 14 (+2) | 7 (-2) |

___Bloodied___. When the Wendigo's hit points are reduced to half or fewer (65 HP) it becomes bloodied.  Its Strength increases to 22.  It gets access to Legendar Actions and regains its Legendary Resistance, if it has already used it.

___Legendary Resistance (1/Day)___. If the Wendigo fails a saving throw, it can choose to succeed instead.

___Natural Hunter.___ The Wendigo has advantage on Stealth checks made while it is hunting.

**Actions**

___Multiattack.___ The Wendigo makes one claw and one bite attack.

___Claw.___ +7 to hit (+9 when bloodied), reach 5 ft., one target. Hit: 10 (1d12 + 4) slashing damage; 13 (1d12 + 6) slashing damage when bloodied.

___Bite.___ +7 to hit (+9 when bloodied), reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage; 15 (2d8 + 6) piercing damage when bloodied.

If a target is bitten, it must make a DC 15 Constitution saving throw.  It takes 10 (3d6) necrotic damage on a failure, or half as much on a success.

**Legendary** Actions (bloodied only)

The Wendigo can take 3 legendary actions, choosing from the options below.  Only one legendary action can be used at a time, and only at the end of another creature's turn.  The Wendigo regains all spent legendary actions at the start of its turn.

___Move (costs one point).___ The Wendigo moves up to its speed.

___Bite (costs two points).___ The Wendigo makes a bite attack.

___Shriek (costs three points).___ The Wendigo screams.  Each creature that can hear it within a 30 ft. radius must make a DC 15 Wisdom saving throw.  On a failure, the creature becomes stunned for one minute.  The creature can remake the save at the end of each of its turns, ending the effect on a success.  If a creature succeeds on the save, it is immune to the Wendigo's shriek for the next hour.
