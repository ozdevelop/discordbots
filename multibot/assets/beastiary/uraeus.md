"Uraeus";;;_size_: Tiny celestial
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "understands Celestial and Common but can't speak"
_skills_: "Perception +4"
_senses_: "blindsight 10 ft., passive Perception 14"
_damage_immunities_: "poison"
_damage_resistances_: "fire; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 60 ft."
_hit points_: "40 (9d4 + 18)"
_armor class_: "14 (natural armor)"
_stats_: | 6 (-2) | 15 (+2) | 14 (+2) | 10 (+0) | 14 (+2) | 9 (-1) |

___Flyby.___ The uraeus doesn't provoke opportunity attacks when it flies out of an enemy's reach.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage plus 9 (2d8) poison damage, and the target must make a successful DC 12 Constitution saving throw or be poisoned for 1 minute. While poisoned in this way, the target takes 9 (2d8) fire damage at the start of its turn. A poisoned creature repeats the saving throw at the end of its turn, ending the effect on a success.

___Searing Breath (Recharge 5-6).___ The uraeus exhales a 15-foot cone of fire. Creatures in the area take 10 (3d6) fire damage, or half damage with a successful DC 12 Dexterity saving throw.

**Bonus** Actions

___Ward Bond.___ The uraeus forms a magical bond with a willing creature within 5 feet. Afterward, no matter how great the distance between them, the uraeus knows the distance and direction to its bonded ward and is aware of the creature's general state of health. The bond lasts until the uraeus or the ward dies, or the uraeus ends the bond as an action.

**Reactions**

___Bonded Savior.___ When the uraeus' bonded ward takes damage, the uraeus can transfer the damage to itself instead. The uraeus' damage resistance and immunity don't apply to transferred damage.
