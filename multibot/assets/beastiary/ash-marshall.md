"Ash Marshall";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Elven, Sylvan"
_skills_: "Athletics +9, Insight +7, Intimidation +7, Nature +6, Perception +8, Religion +6"
_senses_: "passive Perception 21"
_saving_throws_: "Str +9, Con +9, Wis +7, Cha +7"
_damage_resistances_: "fire, lightning"
_speed_: "30 ft."
_hit points_: "157 (15d10 + 75)"
_armor class_: "20 (plate, shield)"
_stats_: | 20 (+5) | 12 (+1) | 21 (+5) | 14 (+2) | 17 (+3) | 16 (+3) |

___Fey Ancestry.___ Magic cannot put the Marshall
to sleep.

___Magic Resistance.___ The Marshall has advantage
on saving throws against spells and other
magical effects.

___Command Fey.___ As a member of the Court
of Arcadia, the Marshall can cast _dominate
monster_ (DC 15) at will on any fey creature or elf.

___Innate Spellcasting.___ The Marshall’s innate spellcasting
ability is Charisma (spell save DC 15). The
Marshall can innately cast the following spells,
requiring no material components:

* At will: _hunter’s mark, branding smite_

* 1/day: _raise dead_

___Spellcasting.___ The Marshall is a 9th-level spellcaster.
His spellcasting ability is Charisma (spell
save DC 15, +7 to hit with spell attacks). The Marshall
has the following spells prepared:

* Cantrips (at will): _fire bolt, mage hand, minor illusion, prestidigitation_

* 1st level (4 slots): _detect magic, identify, magic missile_

* 2nd level (3 slots): _detect thoughts, mirror image, gust of wind, misty step_

* 3rd level (3 slots): _counterspell, fly, fireball_

* 4th level (3 slots): _banishment, dimension door_

* 5th level (1 slot): _cone of cold, hold monster_

**Actions**

___Multiattack.___ The Marshall makes three Ashsword
attacks. If two attacks hit the same target, that
target has disadvantage on any melee attacks
that don’t include the Marshall for the next round.

___Ashsword.___ Melee Weapon Attack: +9 to hit, reach
5 ft., one target. Hit: 9 (1d8 + 5) piercing damage.
