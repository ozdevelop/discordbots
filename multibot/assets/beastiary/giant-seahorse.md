"Giant Seahorse";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "0 ft., swim 30 ft."
_hit points_: "26 (4d10 + 4)"
_armor class_: "12"
_stats_: | 16 (+3) | 15 (+2) | 13 (+1) | 2 (-4) | 13 (+1) | 10 (+0) |

___Water Breathing.___ The giant seahorse can only breathe underwater.

**Actions**

___Tail Slap.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7
(1d8 + 3) bludgeoning damage.

___Head Butt.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit:
7 (1d8 + 3) bludgeoning damage.
