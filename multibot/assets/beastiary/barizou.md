"Barizou (Assassin Demon)";;;_size_: Small fiend (demon)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Abyssal"
_skills_: "Perception +2, Stealth +4"
_senses_: "darkvision 120 ft., passive Perception 12"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 50 ft."
_hit points_: "14 (4d6)"
_armor class_: "12 (natural armor)"
_stats_: | 10 (+0) | 15 (+2) | 10 (+0) | 6 (-2) | 6 (-2) | 10 (+0) |

___Chameleon.___ As a bonus action, a demon can alter its coloration to blend
with its surroundings. This grants the demon advantage on Stealth checks.

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Innate Spellcasting.___ The demon’s spellcasting ability is Charisma (spell save DC 10, +2 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _fear, darkness, detect evil and good, see invisibility_

___Sneak Attack (1/turn).___ The demon deals an extra 3 (1d6) damage
when it hits a target with a weapon attack and has advantage on the attack
roll, or when the target is within 5 feet of an ally of the demon that isn’t
incapacitated and the demon doesn’t have disadvantage on the attack roll.

**Actions**

___Multiattack.___ The demon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.
