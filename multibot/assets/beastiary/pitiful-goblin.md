"Pitiful Goblin";;;_size_: Small humanoid (goblinoid)
_alignment_: neutral evil
_challenge_: "1/8 (25 XP)"
_languages_: "Common, Goblin"
_skills_: "Stealth +4"
_senses_: "darkvision 60ft., passive Perception 9"
_speed_: "30 ft."
_hit points_: "7 (2d6)"
_armor class_: "13 (leather armor)"
_stats_: | 6 (-2) | 14 (+2) | 10 (+0) | 8 (-1) | 8 (-1) | 8 (-1) |

___Cowardly.___ Whenever an ally dies within 60 feet of
the goblin that the goblin can see, it must succeed
on a DC 10 Wisdom saving throw or use its
reaction to run its movement speed away from the
creature that dealt the lethal blow. On a result of 5
or lower, the goblin passes out from the fear for
1d4 minutes.

___Nimble Escape.___ The goblin can take the Disengage
or Hide action as a bonus action on each of its
turns.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit,
reach 5 ft. or range 20/60, one target. Hit: 4 (1d4 + 2) piercing damage.

___Sling.___ Ranged Weapon Attack: +4 to hit, range
30/120 ft., one target. Hit: 4 (1d4 + 2)
bludgeoning damage.
