"Sword Wraith Commander";;;_page_number_: 241
_size_: Medium undead
_alignment_: lawful evil
_challenge_: "8 (3900 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 14"
_skills_: "Perception +4"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "127  (15d8 + 60)"
_armor class_: "18 (breastplate, shield)"
_condition_immunities_: "exhaustion, frightened, poisoned, unconscious"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 18 (+4) | 14 (+2) | 18 (+4) | 11 (0) | 12 (+1) | 14 (+2) |

___Martial Fury.___ As a bonus action, the sword wraith can make one weapon attack, which deals an extra 9 (2d8) necrotic damage on a hit. If it does so, attack rolls against it have advantage until the start of its next turn.

___Turning Defiance.___ The sword wraith and any other sword wraiths within 30 feet of it have advantage on saving throws against effects that turn undead.

**Actions**

___Multiattack___ The sword wraith makes two weapon attacks.

___Longsword___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, or 9 (1d10 + 4) slashing damage if used with two hands.

___Longbow___ Ranged Weapon Attack: +5 to hit, range 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Call to Honor (1/Day)___ To use this action, the sword wraith must have taken damage during the current combat. If the sword wraith can use this action, it gives itself advantage on attack rolls until the end of its next turn, and 1d4 + 1 sword wraith warriors appear in unoccupied spaces within 30 feet of it. The warriors last until they drop to 0 hit points, and they take their turns immediately after the commander's turn on the same initiative count.