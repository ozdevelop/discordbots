"Giant Hyena";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_skills_: "Perception +3"
_speed_: "50 ft."
_hit points_: "45 (6d10+12)"
_armor class_: "12"
_stats_: | 16 (+3) | 14 (+2) | 14 (+2) | 2 (-4) | 12 (+1) | 7 (-2) |

___Rampage.___ When the hyena reduces a creature to 0 hit points with a melee attack on its turn, the hyena can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage.