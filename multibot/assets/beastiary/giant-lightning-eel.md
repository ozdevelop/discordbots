"Giant Lightning Eel";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "blindsight 60 ft."
_speed_: "5 ft., swim 30 ft."
_hit points_: "42 (5d10+15)"
_armor class_: "13"
_damage_resistances_: "lightning"
_stats_: | 11 (0) | 17 (+3) | 16 (+3) | 2 (-4) | 12 (+1) | 3 (-4) |

___Source.___ tales from the yawning portal,  page 236

___Water Breathing.___ The eel can breathe only underwater.

**Actions**

___Multiattack.___ The eel makes two bite attacks.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage plus 4 (1d8) lightning damage.

___Lightning jolt (Recharge 5-6).___ One creature the eel touches within 5 feet of it outside water, or each creature within l5 feet of it in a body of water, must make a DC 12 Constitution saving throw. On failed save, a target takes 13 (3d8) lightning damage. If the target takes any of this damage, the target is stunned until the end of the eel's next turn. On a successful save, a target takes half as much damage and isn't stunned.