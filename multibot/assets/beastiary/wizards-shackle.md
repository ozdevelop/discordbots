"Wizard's Shackle";;;_size_: Tiny monstrosity
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft., passive Perception 10"
_speed_: "5 ft."
_hit points_: "16 (3d4 + 9)"
_armor class_: "11"
_stats_: | 1 (-5) | 12 (+1) | 16 (+3) | 1 (-5) | 11 (+0) | 2 (-4) |

___Arcane Sense.___ A wizard’s shackle can automatically
detect the location of any active spell or magical item
within 30 feet not hidden behind 10 feet of dirt, 5 feet of
stone, 1 foot of common metal, or 5 inches of lead.

___Magic Resistance.___ The wizard’s shackle has advantage
on saving throws against spells and magical effects.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature.
Hit: 3 (1d4 + 1) piercing damage and the wizard’s shackle attaches
to the target. While it is attached, any time the target attempts to cast a
cantrip or spell, it takes 10 (3d6) psychic damage and must make a DC 10
Constitution saving throw. On a failed saving throw, the target fails to cast
the spell and if a spell slot was used, it is wasted.

The wizard’s shackle can be removed as an action by the target or
another creature. If a creature or creature to which the wizard’s shackle is
attached attempts to remove the wizard’s shackle, the attached target must
succeed on a DC 13 Constitution saving throw. On a failed saving throw,
the target is unconscious for 1 minute.
