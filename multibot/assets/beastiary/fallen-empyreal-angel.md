"Fallen Empyreal Angel";;;_size_: Large celestial
_alignment_: chaotic evil
_challenge_: "12 (8,400 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Insight +8, Intimidation +9, Perception +8"
_senses_: "darkvision 60 ft., blindsight 30 ft., passive Perception 18"
_saving_throws_: "Dex +8, Con +9"
_damage_immunities_: "acid, cold, necrotic"
_damage_resistances_: "lightning, fire; bludgeoning, piercing, slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "40 ft., fly 70 ft."
_hit points_: "157 (15d10 + 75)"
_armor class_: "20 (natural armor)"
_stats_: | 23 (+6) | 18 (+4) | 20 (+5) | 18 (+4) | 18 (+4) | 20 (+5) |

___Aura of Hopelessness.___ The fallen empyreal can activate
or deactivate this feature as a bonus action. While active, all
creatures within 30 feet of it suffer 10 (3d6) necrotic damage at
the beginning of each of the fallen empyreal’s turns. The first time
a creature enters or starts its turn in the area it must succeed on a DC
16 Wisdom saving throw or be frightened for 1 minute. The target can
repeat the saving throw at the end of each of its turns, ending the effect
on itself on a success.

___Demonic Weapons.___ The empyreal’s weapon attacks are magical. When
the empyreal hits with any weapon, the weapon deals an extra 4d8 necrotic
damage (included in the attack).

___Innate Spellcasting.___ The empyreal’s spellcasting ability is Charisma
(spell save DC 17, +9 to hit with spell attacks). The empyreal can innately
cast the following spells, requiring only verbal components:

At will: detect evil and good, darkness, dispel magic, fear, invisibility,
plane shift

5/day each: bestow curse, inflict wounds

3/day each: blade barrier, flame strike, scorching ray

1/day each: circle of death, create undead

___Magic Resistance.___ The empyreal has advantage on saving throws
against spells and other magical effects.

**Actions**

___Multiattack.___ The fallen empyreal makes three attacks with its flaming
longsword.

___Flaming Longsword.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one
target. Hit: 15 (2d8 + 6) slashing damage plus 18 (4d8) necrotic damage.

___Necrotic Blast (Recharge 5–6).___ A fallen empyreal can unleash a blast
of necrotic energy in a 30-foot radius centered on itself. All creatures
in the area must make a DC 16 Dexterity saving throw, taking 28 (8d6)
necrotic damage on a failed save, or half as much on a successful one.
