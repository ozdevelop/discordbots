"Salt Drake";;;_size_: Large dragon
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Draconic"
_skills_: "Perception +4, Stealth +5, Survival +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "acid"
_speed_: "40 ft., fly 60 ft."
_hit points_: "85 (9d10 + 36)"
_armor class_: "16 (natural armor)"
_stats_: | 17 (+3) | 14 (+2) | 18 (+4) | 4 (-3) | 23 (+1) | 11 (+0) |

___Nictitating Membranes.___ Salt drakes have advantage on saving throws
against being blinded.

**Actions**

___Multiattack.___ The salt drake makes three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 12
(2d8 + 3) piercing damage plus 7 (3d4) acid damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) slashing damage.

___Tail.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 16
(3d8 + 3) bludgeoning damage, and the target must succeed on a DC 15
Strength saving throw or be knocked prone.

___Salt Spray (Recharge 5–6).___ The salt drake releases a spray of razorsharp
salt crystals in a 30-foot cone. Each creature in the area must make
a DC 15 Dexterity saving throw. On a failed saving throw, the target
takes 22 (5d8) acid damage plus 22 (5d8) slashing damage, or half as
much damage on a successful saving throw. In addition, if a creature
takes any damage from the salt spray, it is poisoned until it takes a short
or long rest.
