"Elven Scout";;;_size_: Medium humanoid (elf)
_alignment_: chaotic good
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Elvish"
_senses_: "darkvision 60 ft., Passive Perception 16"
_skills_: "Nature +4, Perception +6, Stealth +7, Survival +6"
_speed_: "30 ft."
_hit points_: "16 (3d8 + 3)"
_armor class_: "14 (leather armor)"
_stats_: | 11 (+0) | 16 (+3) | 12 (+1) | 11 (+0) | 14 (+2) | 11 (+0) |

___Fey Ancestry.___ The elven scout has advantage on
saving throws against being charmed, and magic
can’t put them to sleep.

___Keen Hearing and Sight.___ The elven scout has advantage
on Wisdom (Perception) checks that rely on
hearing or sight.

**Actions**

___Multiattack.___ The elven scout makes two melee attacks
or two ranged attacks.

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach
5 ft., one target. Hit: 5 (1d6 +2) piercing damage.

___Longbow.___ Ranged Weapon Attack: +4 to hit, ranged
150/600 ft., one target. Hit: 6 (1d8 +2) piercing
damage.
