"Sarcosuchus Zombie";;;_size_: Gargantuan undead
_alignment_: unaligned
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft., swim 40 ft."
_hit points_: "137 (10d20 + 32)"
_armor class_: "12 (natural armor)"
_stats_: | 23 (+6) | 6 (-2) | 18 (+4) | 1 (-5) | 8 (-1) | 5 (-3) |

___Undead Fortitude.___ If damage reduces the zombie
to 0 hit points, it must make a Constitution saving
throw with a DC of 5 + the damage taken, unless
the damage is radiant or from a critical hit. On a
success, the zombie drops to 1 hit point instead.

___Water Poisoner.___ Water within 20 ft. of the zombie
is tainted with toxins . A creature that enters or
starts its turn in that water must succeed on a DC
15 Constitution saving throw or it is poisoned until
the start of its next turn.

**Actions**

___Multiattack.___ The zombie can make two attacks:
one with its bite and one with its tail.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft.,
one target. Hit: 28 (4d10 + 6) piercing damage. If
the target is a creature, it is grappled (Escape DC 16). Until this grapple ends, the target is restrained,
and the sarcosuchus zombie can’t grapple an other target.

___Tail.___ Melee Weapon Attack: +9 to hit, reach 15 ft.,
one target. Hit: 19 (3d8 + 6) bludgeoning damage. If
the target is a creature, it must succeed on a DC 17
Strength saving throw or be knocked prone.
