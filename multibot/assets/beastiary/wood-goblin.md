"Wood Goblin";;;_size_: Small humanoid
_alignment_: neutral good
_challenge_: "1 (200 XP)"
_languages_: "Common, Goblin, Sylvan"
_skills_: "Athletics +3, Acrobatics +4, Medicine +5, Perception +5, Sleight of Hand +4, Stealth +4, Survival +5"
_senses_: "darkvision 120 ft., passive Perception 15"
_saving_throws_: "Dex +4, Int +5"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "20 ft."
_hit points_: "18 (4d6 + 4)"
_armor class_: "13 (natural armor)"
_stats_: | 13 (+1) | 15 (+2) | 13 (+1) | 16 (+3) | 16 (+3) | 10 (+0) |

___Innate Spellcasting.___ The wood goblin’s
spellcasting ability is Wisdom (spell save DC
13, +5 to hit with spell attacks). The goblin
can innately cast the following spells,
requiring no material components:

* At will: _detect poison or disease, poison spray, shillelagh, spare the dying_

* 3/day each: _barkskin, cure wounds, entangle, lesser restoration, protection from poison_

* 1/day: _spider climb_

___Herbal Medicine.___ Due to their innate and
magical affinity for plants and herbs, a wood
goblin always has advantage on Wisdom (Medicine) skill checks. In addition, the wood
goblin’s innate cure wounds spells can be imitated
herbally without limitation as long as the wood goblin
has access to wild plants. However, the herbal versions
require 15 minutes to prepare and must be fresh, and thus are
not usable in combat.

___Sneak Attack (1/turn).___ The wood goblin deals an extra 7 (2d6) damage
when it has advantage and hits a target with a weapon on the attack roll,
or when the target is within 5 feet of an ally of the wood goblin that isn’t
incapacitated and the wood goblin doesn’t have disadvantage on the
attack roll.

**Actions**

___Rapier.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one
target. Hit: 5 (1d6 + 2) piercing damage.

___Poison Cloud (1/day).___ A 10-foot radius of thick venomous gas
extends out from the wood goblin. The gas spreads around corners,
and its area is lightly obscured. It lasts for 1 minute or until a strong wind
disperses it. Any creature that starts its turn in that area must succeed on a
DC 11 Constitution saving throw or be poisoned until the start of its next
turn and it takes 7 (2d6) poison damage. While poisoned in this way, the
target can take either an action or a bonus action on its turn, not both, and
can’t take reactions.
