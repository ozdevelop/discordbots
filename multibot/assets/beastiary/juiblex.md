"Juiblex";;;_page_number_: 151
_size_: Huge fiend (demon)
_alignment_: chaotic evil
_challenge_: "23 (50,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 22"
_skills_: "Perception +12"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_saving_throws_: "Dex +7, Con +13, Wis +12"
_speed_: "30 ft."
_hit points_: "350  (28d12 + 168)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained, stunned, unconscious"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 24 (+7) | 10 (0) | 23 (+6) | 20 (+5) | 20 (+5) | 16 (+3) |

___Foul.___ Any creature, other than an ooze, that starts its turn within 10 feet of Juiblex must succeed on a DC 21 Constitution saving throw or be poisoned until the start of the creature's next turn.

___Innate Spellcasting.___ Juiblex's spellcasting ability is Charisma (spell save DC 18, +10 to hit with spell attacks). Juiblex can innately cast the following spells, requiring no material components:

* At will: _acid splash _(17th level)_, detect magic_

* 3/day each: _blight, contagion, gaseous form_

___Legendary Resistance (3/Day).___ If Juiblex fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ Juiblex has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Juiblex's weapon attacks are magical.

___Regeneration.___ Juiblex regains 20 hit points at the start of its turn. If it takes fire or radiant damage, this trait doesn't function at the start of its next turn. Juiblex dies only if it starts its turn with 0 hit points and doesn't regenerate.

___Spider Climb.___ Juiblex can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack___ Juiblex makes three acid lash attacks.

___Acid Lash___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target. Hit: 21 (4d6 + 7) acid damage. Any creature killed by this attack is drawn into Juiblex's body, and the corpse is obliterated after 1 minute.

___Eject Slime (Recharge 5-6)___ Juiblex spews out a corrosive slime, targeting one creature that it can see within 60 feet of it. The target must make a DC 21 Dexterity saving throw. On a failure, the target takes 55 (10d10) acid damage. Unless the target avoids taking any of this damage, any metal armor worn by the target takes a permanent -1 penalty to the AC it offers, and any metal weapon it is carrying or wearing takes a permanent -1 penalty to damage rolls. The penalty worsens each time a target is subjected to this effect. If the penalty on an object drops to -5, the object is destroyed.

**Legendary** Actions

Juiblex can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Juiblex regains spent legendary actions at the start of its turn.

___Acid Splash___ Juiblex casts acid splash.

___Attack___ Juiblex makes one acid lash attack.

___Corrupting Touch (Costs 2 Actions)___ Melee Weapon Attack: +14 to hit, reach 10 ft., one creature. Hit: 21 (4d6 + 7) poison damage, and the target is slimed. Until the slime is scraped off with an action, the target is poisoned, and any creature, other than an ooze, is poisoned while within 10 feet of the target.
