"Githyanki Gish";;;_page_number_: 205
_size_: Medium humanoid (gith)
_alignment_: lawful evil
_challenge_: "10 (5900 XP)"
_languages_: "Gith"
_senses_: "passive Perception 16"
_skills_: "Insight +6, Perception +6, Stealth +6"
_saving_throws_: "Con +6, Int +7, Wis +6"
_speed_: "30 ft."
_hit points_: "123  (19d8 + 38)"
_armor class_: "17 (half plate)"
_stats_: | 17 (+3) | 15 (+2) | 14 (+2) | 16 (+3) | 15 (+2) | 16 (+3) |

___Innate Spellcasting (Psionics).___ The githyanki's innate spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _jump, misty step, nondetection _(self only)

* 1/day each: _plane shift, telekinesis_

___Spellcasting.___ The githyanki is an 8th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). The githyanki has the following wizard spells prepared:

* Cantrips (at will): _blade ward, light, message, true strike_

* 1st level (4 slots): _expeditious retreat, magic missile, sleep, thunderwave_

* 2nd level (3 slots): _blur, invisibility, levitate_

* 3rd level (3 slots): _counterspell, fireball, haste_

* 4th level (2 slots): _dimension door_

___War Magic.___ When the githyanki uses its action to cast a spell, it can make one weapon attack as a bonus action

**Actions**

___Multiattack___ The githyanki makes two longsword attacks.

___Longsword___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage plus 18 (4d8) psychic damage, or 8 (1d10 + 3) slashing damage plus 18 (4d8) psychic damage if used with two hands.
