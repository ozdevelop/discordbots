"Jackalwere";;;_size_: Medium humanoid (shapechanger)
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common (can't speak in jackal form)"
_skills_: "Deception +4, Perception +2, Stealth +4"
_damage_immunities_: "bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_speed_: "40 ft."
_hit points_: "18 (4d8)"
_armor class_: "12"
_stats_: | 11 (0) | 15 (+2) | 11 (0) | 13 (+1) | 11 (0) | 10 (0) |

___Shapechanger.___ The jackalwere can use its action to polymorph into a specific Medium human or a jackal-humanoid hybrid, or back into its true form (that of a Small jackal). Other than its size, its statistics are the same in each form. Any equipment it is wearing or carrying isn 't transformed. It reverts to its true form if it dies.

___Keen Hearing and Smell.___ The jackalwere has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Pack Tactics.___ The jackalwere has advantage on an attack roll against a creature if at least one of the wolf's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Bite (Jackal or Hybrid Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Scimitar (Human or Hybrid Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage.

___Sleep Gaze.___ The jackalwere gazes at one creature it can see within 30 feet of it. The target must make a DC 10 Wisdom saving throw. On a failed save, the target succumbs to a magical slumber, falling unconscious for 10 minutes or until someone uses an action to shake the target awake. A creature that successfully saves against the effect is immune to this jackalwere's gaze for the next 24 hours. Undead and creatures immune to being charmed aren't affected by it.