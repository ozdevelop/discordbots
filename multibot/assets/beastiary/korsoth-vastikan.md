"Korsoth Vastikan";;;_size_: Medium aberration
_alignment_: chaotic neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Deep Speech, Primordial"
_skills_: "Acrobatics +7, Deception +4, Investigation +3, Perception +4, Stealth +7, Survival +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +7, Wis +4"
_damage_resistances_: "psychic"
_condition_immunities_: "charmed, petrified"
_speed_: "40 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "16 (studded leather)"
_stats_: | 13 (+1) | 18 (+4) | 14 (+2) | 11 (+0) | 13 (+1) | 12 (+1) |

___Everchanging Changers.___ The Court of All Flesh are
beings of pure chaos. Because their minds are pure
disorder, they cannot be driven mad or charmed
and any attempts to magically compel their behavior fails.

___Formless Shape.___ Vastikan is immune to any
spell or effect that would alter his form.

___Formkiller.___ If Vastikan hits a target with three
or more arrows in one round, the target must make
a DC 12 Constitution saving throw or lose its
native form. Roll on the Reincarnation table to
determine the target’s new form. The target
reverts to its original form after 1 hour.
 A target that succeeds on its saving throw becomes
immune to Formkiller for 24 hours.

**Actions**

___Multiattack.___ Vastikan makes four
longbow attacks.

___Longbow.___ Ranged Weapon Attack: +7 to hit,
range 150/600 ft., one target. Hit: 8 (1d8 + 4)
piercing damage.

___Shortsword.___ Melee Weapon Attack: +7 to
hit, reach 5 ft., one target. Hit: 8 (1d8 + 4)
piercing damage.
