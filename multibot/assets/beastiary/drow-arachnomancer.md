"Drow Arachnomancer";;;_page_number_: 182
_size_: Medium humanoid (elf)
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Elvish, Undercommon, can speak with spiders"
_senses_: "blindsight 10 ft., darkvision 120 ft., passive Perception 17"
_skills_: "Arcana +9, Nature +9, Perception +7, Stealth +8"
_saving_throws_: "Con +7, Int +9, Cha +8"
_speed_: "30 ft., climb 30 ft."
_hit points_: "162  (25d8 + 50)"
_armor class_: "15 (studded leather)"
_damage_resistances_: "poison"
_stats_: | 11 (0) | 17 (+3) | 14 (+2) | 19 (+4) | 14 (+2) | 16 (+3) |

___Change Shape (Recharges after a Short or Long Rest).___ The drow can use a bonus action to magically polymorph into a giant spider, remaining in that form for up to 1 hour. It can revert to its true form as a bonus action. Its statistics, other than its size, are the same in each form. It can speak and cast spells while in giant spider form. Any equipment it is wearing or carrying in humanoid form melds into the giant spider form. It can't activate, use, wield, or otherwise benefit from any of its equipment. It reverts to its humanoid form if it dies.

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire, levitate _(self only)

___Spellcasting.___ The drow is a 16th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _chill touch, eldritch blast, mage hand, poison spray_

* 5th level (3 slots): _conjure animals _(spiders only)_, crown of madness, dimension door, dispel magic, fear, fly, giant insect, hold monster, insect plague, invisibility, vampiric touch, web, witch bolt_

* 6th Level (1 slot): _eyebite_ - Can only be cast 1/day

* 7th Level (1 slot): _etherealness_ - Can only be cast 1/day

* 8th Level (1 slot): _dominate monster_ - Can only be cast 1/day


___Spider Climb.___ The drow can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Web Walker.___ The drow ignores movement restrictions caused by webbing.

**Actions**

___Multiattack___ The drow makes two poisonous touch attacks or two bite attacks. The first of these attacks that hits each round deals an extra 26 (4d12) poison damage to the target.

___Poisonous Touch (Humanoid Form Only)___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 28 (8d6) poison damage.

___Bite (Giant Spider Form Only)___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) piercing damage, and the target must make a DC 15 Constitution saving throw, taking 26 (4d12) poison damage on a failed save, or half as much damage on a successful one. If the poison damage reduces the target to 0 hit points, the target is stable but poisoned for 1 hour, even after regaining hit points, and is paralyzed while poisoned in this way.

___Web (Giant Spider Form Only Recharge 5-6)___ Ranged Weapon Attack: +8 to hit, range 30/60 ft., one target. Hit: The target is restrained by webbing. As an action, the restrained target can make a DC 15 Strength check, bursting the webbing on a success. The webbing can also be attacked and destroyed (AC 10; hp 5; vulnerability to fire damage; immunity to bludgeoning, poison, and psychic damage).
