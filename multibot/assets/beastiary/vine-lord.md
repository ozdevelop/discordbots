"Vine Lord";;;_size_: Medium plant
_alignment_: lawful neutral
_challenge_: "7 (2900 XP)"
_languages_: "Common"
_senses_: "blindsight 30 ft. (blind beyond this radius), passive Perception 13"
_saving_throws_: "Con +6, Wis +6, Cha +7"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "blinded, deafened"
_speed_: "30 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 20 (+5) | 16 (+3) | 14 (+2) | 16 (+3) | 18 (+4) |

___Green Strider.___ The vine lord ignores movement restrictions and damage caused by natural undergrowth.

___Magic Resistance.___ The vine lord has advantage on saving throws against spells and other magical effects.

___Regeneration.___ The vine lord regains 10 hit points at the start of its turn if it has at least 1 hit point and is within its home forest or jungle.

___Root Mind.___ Within its home forest or jungle, the vine lord's blindsight extends to 60 ft., it succeeds on all Wisdom (Perception) checks, and it can't be surprised.

**Actions**

___Multiattack.___ The vine lord makes two claw attacks and four tendril attacks. A single creature can't be the target of more than one tendril attack per turn.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) slashing damage.

___Spore Sacs (1/week).___ The vine lord can release seeds from specialized sacs on its tendrils. These seeds sprout into 1d4 green spore pods that reach maturity in 3 days. The pods contain noxious spores that are released when the pod is stepped on, picked, or otherwise tampered with. A humanoid or beast that inhales these spores must succeed on a DC 14 Constitution saving throw against disease or tendrils start growing inside the creature's body. If the disease is not cured within 3 months, the tendrils take over the creature's nervous system and the victim becomes a tendril puppet.

___Tendril.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 7 (1d4 + 5) slashing damage plus 3 (1d6) poison damage.

___Awaken the Green (1/Day).___ The vine lord magically animates one or two trees it can see within 60 feet of it. These trees have the same statistics as a treant, except they have Intelligence and Charisma scores of 1, they can't speak, and they have only the Slam action option. An animated tree acts as an ally of the vine lord. The tree remains animate for 1 day or until it dies; until the vine lord dies or is more than 120 feet from the tree; or until the vine lord takes a bonus action to turn it back into an inanimate tree. The tree then takes root if possible.

