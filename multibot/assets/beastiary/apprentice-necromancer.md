"Apprentice Necromancer";;;_size_: Medium humanoid
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Infernal"
_senses_: "passive Perception 10"
_saving_throws_: "Int +5, Wis +2"
_speed_: "30 ft."
_hit points_: "45 (7d6 + 21)"
_armor class_: "12"
_stats_: | 8 (-1) | 14 (+2) | 16 (+3) | 16 (+3) | 10 (+0) | 7 (-2) |

___Spellcasting.___ The necromancer is an 5th-level spellcaster. 
Its spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). 
The necromancer has the following wizard spells prepared: 

* Cantrips (at will): _chill touch, acid splash, mage hand, prestidigitation_

* 1st level (4 slots): _ray of sickness, witch bolt, tasha’s hideous laughter_

* 2nd level (3 slots): _ray of enfeeblement, crown of madness_

* 3rd level (2 slots): _animate dead, vampiric touch_

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +1 to hit, reach 5ft., one target. Hit: 2 (1d6 - 1) bludgeoning damage.
