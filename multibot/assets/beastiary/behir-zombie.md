"Behir Zombie";;;_size_: Huge undead
_alignment_: neutral evil
_challenge_: "11 (7,200 XP)"
_languages_: "--"
_senses_: "darkvision 90 ft., passive Perception 8"
_saving_throws_: "Wis +2"
_damage_immunities_: "lightning, poison"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "50 ft., climb 40 ft."
_hit points_: "168 (16d12 + 64)"
_armor class_: "17 (natural armor)"
_stats_: | 23 (+6) | 10 (+0) | 18 (+4) | 3 (-4) | 7 (-2) | 5 (-3) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it
must make a Constitution saving throw with a DC of 5 + the damage
taken, unless the damage is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.

**Actions**

___Multiattack.___ The zombie makes two attacks: one with its bite and one
with its constrict.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 17
(2d10 + 6) piercing damage.

___Constrict.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one Large or
smaller creature. Hit: 17 (2d10 + 6) bludgeoning damage plus 17 (2d10 +
6) slashing damage. The target is grappled (escape DC 16) if the zombie
isn’t already constricting a creature, and the target is restrained until this
grapple ends.

___Swallow.___ The zombie makes one bite attack against a Medium or smaller
target it is grappling. If the attack hits, the target is also swallowed, and
the grapple ends. While swallowed, the target is blinded and restrained, it
has total cover against attacks and other effects outside the zombie, and it
takes 21 (6d6) necrotic damage at the start of each of the zombie’s turns.
A behir zombie can have only one creature swallowed at a time.

If the zombie dies, a swallowed creature is no longer restrained by it and
can escape from the corpse by using 15 ft. of movement, exiting prone.
