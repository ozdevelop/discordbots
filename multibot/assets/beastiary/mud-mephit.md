"Mud Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Aquan, Terran"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +3"
_damage_immunities_: "poison"
_speed_: "20 ft., fly 20 ft., swim 20 ft."
_hit points_: "27 (6d6+6)"
_armor class_: "11"
_condition_immunities_: "poisoned"
_stats_: | 8 (-1) | 12 (+1) | 12 (+1) | 9 (-1) | 11 (0) | 7 (-2) |

___Death Burst.___ When the mephit dies, it explodes in a burst of sticky mud. Each Medium or smaller creature within 5 ft. of it must succeed on a DC 11 Dexterity saving throw or be restrained until the end of the creature's next turn.

___False Appearance.___ While the mephit remains motionless, it is indistinguishable from an ordinary mound of mud.

**Actions**

___Fists.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 4 (1d6 + 1) bludgeoning damage.

___Mud Breath (Recharge 6).___ The mephit belches viscid mud onto one creature within 5 ft. of it. If the target is Medium or smaller, it must succeed on a DC 11 Dexterity saving throw or be restrained for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Variant: Summon Mephits (1/Day).___ The mephit has a 25 percent chance of summoning 1d4 mephits of its kind. A summoned mephit appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other mephits. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.