"Sap Demon";;;_size_: Small ooze
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "none in its natural form; knows the same languages as a creature it dominates"
_senses_: "blindsight 90 ft. (blind beyond this radius), passive Perception 12"
_damage_immunities_: "bludgeoning; acid, lightning"
_damage_resistances_: "piercing and slashing from nonmagical weapons"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_speed_: "20 ft., climb 20 ft."
_hit points_: "67 (15d6 + 15)"
_armor class_: "13 (natural)"
_stats_: | 14 (+2) | 6 (-2) | 12 (+1) | 10 (+0) | 14 (+2) | 10 (+0) |

___Amorphous.___ The sap demon can move through a space as narrow as 1 inch wide without squeezing.

___Season's Change.___ If a sap demon (or its host) takes at least 10 fire damage, it also gains the effect of a haste spell until the end of its next turn. If it takes at least 10 cold damage, it gains the effect of a slow spell until the end of its next turn.

**Actions**

___Multiattack.___ The sap demon makes two slam attacks.

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 11 (2d8 + 2) bludgeoning damage. If both attacks hit a Medium or smaller target, the target is grappled (escape DC 12), and the sap demon uses Soul Sap on it as a bonus action.

___Soul Sap.___ The sap demon slides down the throat of a sleeping, helpless, or grappled living creature of Medium size or smaller. Once inside, the sap demon takes control of its host, as per the dominate monster spell (Wisdom DC 12 negates). While dominated, the host gains blindsight 90 feet. The host drips blood from its ears, nose, eyes, or from a wound that resembles the injury done to the sap demon's tree (1 damage/ hour). Damage inflicted on the host has no effect on the sap demon. If the host dies or is reduced to 0 hit points, the sap demon must leave the body within one hour.

