"Centaur Mummy";;;_size_: Large undead
_alignment_: lawful evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Sylvan"
_senses_: "darkvision 60 ft."
_damage_immunities_: "necrotic, poison"
_saving_throws_: "Wis +5"
_speed_: "30 ft."
_hit points_: "85 (10d10+30)"
_armor class_: "13 (natural armor)"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 20 (+5) | 12 (+1) | 16 (+3) | 5 (-3) | 14 (+2) | 12 (+1) |

___Source.___ tales from the yawning portal,  page 231

___Undead Nature.___ A mummy doesn't require air, food, drink, or sleep.

___Charge.___ If the centaur mummy moves at least 20 feet straight toward a target and then hits it with a pike attack on the same turn, the target takes an extra 10 (3d6) piercing damage.

**Actions**

___Multiattack.___ The centaur mummy makes two melee attacks, one with its pike and one with its hooves, or it attacks with its pike and uses Dreadful Glare.

___Pike.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 10 (1d10 + 5) piercing damage.

___Hooves.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage plus 10 (3d6) necrotic damage. If the target is a creature, it must succeed on a DC 14 Constitution saving throw or be cursed with mummy rot. The cursed target can't regain hit points, and its hit point maximum decreases by 10 (3d6) for every 24 hours that elapse. If the curse reduces the target's hit point maximum to 0, the target dies, and its body turns to dust. The curse lasts until removed by the remove curse spell or similar magic.

___Dreadful Glare.___ The centaur mummy targets one creature it can see within 60 feet of it. If the target can see the mummy, the target must succeed on a DC 12 wisdom saving throw against this magic or become frightened until the end of the mummy's next turn. If the target fails the saving throw by 5 or more, it is also paralyzed for the same duration. A target that succeeds on the saving throw is immune to the Dreadful Glare of all mummies (but not mummy lords) for tne next 24 hours.