"Chasme";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "blindsight 10 ft., darkvision 120 ft."
_skills_: "Perception +5"
_damage_immunities_: "poison"
_saving_throws_: "Dex +5, Wis +5"
_speed_: "20 ft., fly 60 ft."
_hit points_: "84 (13d10+13)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 15 (+2) | 15 (+2) | 12 (+1) | 11 (0) | 14 (+2) | 10 (0) |

___Drone.___ The chasme produces a horrid droning sound to which demons are immune. Any other creature that starts its turn with in 30 feet of the chasme must succeed on a DC 12 Constitution saving throw or fall unconscious for 10 minutes. A creature that can't hear the drone automatically succeeds on the save. The effect on the creature ends if it takes damage or if another creature takes an action to splash it with holy water. If a creature's saving throw is successful or the effect ends for it, it is immune to the drone for the next 24 hours .

___Magic Resistance.___ The chasme has advantage on saving throws against spells and other magical effects.

___Spider Climb.___ The chasme can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Proboscis.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 16 (4d6 + 2) piercing damage plus 24 (7d6) necrotic damage, and the target's hit point maximum is reduced by an amount equal to the necrotic damage taken. If this effect reduces a creature's hit point maximum to 0, the creature dies. This reduction to a creature's hit point maximum lasts until the creature finishes a long rest or until it is affected by a spell like greater restoration .

___Variant: Summon Demon (1/Day).___ The demon chooses what to summon and attempts a magical summoning.

A chasme has a 30 percent chance of summoning one chasme.

A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.