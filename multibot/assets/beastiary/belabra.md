"Belabra";;;_size_: Medium aberration
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "5 ft., fly 30 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "13 (natural armor)"
_stats_: | 14 (+2) | 15 (+2) | 14 (+2) | 7 (-2) | 12 (+1) | 11 (+0) |

___Acidic Blood.___ Each time the belabra is hit with an attack
that does piercing or slashing damage, all creatures within 10
feet of the belabra must make a DC 12 Dexterity saving throw
or be sprayed with the belabra’s blood. On a failed save, the
creature takes 4 (1d6 + 1) acid damage and has disadvantage
on attacks, saving throws, or ability checks, due to sneezing
and partial blindness, until the end of the belabra’s next
turn.

**Actions**

___Multiattack.___ The belabra makes three attacks: one
with its slam and two with its tentacles. If it is grappling
a target, it will bite the target in place of a tentacle attack.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5
(1d6 + 2) bludgeoning damage.

___Tentacles.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit:
9 (2d6 + 2) piercing damage, and target is grappled (escape DC 12). Until
the grapple ends, the target is restrained and takes 4 (1d6 + 1) piercing
damage from the tentacle barbs at the start of its turn. The belabra can
grapple three targets at the same time.
