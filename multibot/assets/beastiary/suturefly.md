"Suturefly";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_skills_: "Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "10 ft., fly 40 ft. (hover)"
_hit points_: "7 (3d4)"
_armor class_: "14"
_stats_: | 1 (-5) | 19 (+4) | 10 (+0) | 1 (-5) | 12 (+1) | 4 (-3) |

___Camouflage.___ A suturefly in forest surroundings has advantage on Dexterity (Stealth) checks.

___Detect Blasphemy.___ The most common variety of suturefly attacks any creature that blasphemes aloud, which it can detect at a range of 100 feet unless the blasphemer makes a successful DC 13 Charisma saving throw.

**Actions**

___Sew.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 1 piercing damage, and the suturefly sews the target's mouth, nose, or eye closed. With supernatural speed, the suturefly repeatedly pierces the target's face, each time threading a loop of the target's own skin through the previous hole. These skin loops rapidly blacken, shrink, and draw the orifice tightly closed. It takes two actions and a sharp blade to sever the loops and reopen the orifice, and the process causes intense pain and 2 slashing damage. A victim whose mouth and nose have been sewn shut begins suffocating at the start of his or her next turn.

