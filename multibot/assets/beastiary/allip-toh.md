"Allip (ToH)";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Deep Speech"
_skills_: "Perception +3, Stealth +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "fly 30 ft."
_hit points_: "33 (6d8 + 6)"
_armor class_: "18 (armored flight suit)"
_stats_: | 6 (-2) | 13 (+1) | 13 (+1) | 11 (+0) | 11 (+0) | 16 (+3) |

___Babble.___ The allip incoherently mutters to itself, creating a hypnotic effect. All creatures within 30 ft. that aren’t incapacitated must succeed on a DC 11 Wisdom saving throw. On a failed save, the creature becomes charmed for the duration. While charmed by this spell, the creature is
incapacitated and has a speed of 0. The effect ends for an affected creature if it takes any damage or if someone else uses an action to shake the creature out of its stupor.

___Madness.___ Anyone targeting an allip with a spell or effect that would make direct contact with its tortured mind must succeed on a DC 11 Wisdom saving throw or take 7 (2d6) psychic damage.

**Actions**

___Touch of Insanity.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 8 (2d6 + 1) psychic damage.
