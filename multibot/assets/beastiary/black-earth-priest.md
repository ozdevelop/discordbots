"Black Earth Priest";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Terran"
_skills_: "Intimidation +5, Religion +3, Persuasion +5"
_speed_: "30 ft."
_hit points_: "45 (7d8+14)"
_armor class_: "17 (splint)"
_stats_: | 15 (+2) | 11 (0) | 14 (+2) | 12 (+1) | 10 (0) | 16 (+3) |

___Spellcasting.___ The priest is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _acid splash, blade ward, light, mending, mold earth_

* 1st level (4 slots): _earth tremor, expeditious retreat, shield_

* 2nd level (3 slots): _shatter, spider climb_

* 3rd level (2 slots): _slow_

**Actions**

___Multiattack.___ The priest makes two melee attacks.

___Glaive.___ Melee Weapon Attack: +4 to hit, reach 10 ft., one target. Hit: 7 (1d10 + 2) slashing damage.

**Reactions**

___Unyielding.___ When the priest is subjected to an effect that would move it, knock it prone, or both, it can use its reaction to be neither moved nor knocked prone.
