"Prehistoric Honey Badger";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_languages_: "--"
_skills_: "Survival +2"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_resistances_: "poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "frightened"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "51 (6d8 + 24)"
_armor class_: "14 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 18 (+4) | 7 (-2) | 11 (+0) | 5 (-3) |

___Keen Hearing and Smell.___ The prehistoric honey badger has
advantage on Wisdom (Perception) checks based on
hearing or smell.

___Relentless (Recharges after a Short or Long Rest).___ If the
badger takes 14 damage or less that would reduce it to 0
hit points, it is reduced to 1 hit point instead.

**Actions**

___Multiattack.___ The prehistoric honey badger makes one bite attack and
one crunch attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 16
(2d10 + 5) piercing damage, and the target must make a DC 15 Strength
saving throw or be grappled (escape DC 15).

___Crunch.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one grappled
creature. Hit: The target suffers from broken bones and must make a DC
15 Constitution saving throw at the beginning of each of its turns. On a
failed saving throw, the target cannot take any actions or reactions during
that turn. If the target receives magical healing or takes a long rest, the
effect ends.
