"Lizard Shass";;;_size_: Medium humanoid (lizardfolk)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Draconic"
_senses_: "Darkvision 60 ft., Passive Perception 14"
_skills_: "Perception +4, Stealth +5, Survival +4"
_saving_throws_: "Con +4, Wis +2"
_condition_immunities_: "Frightened"
_speed_: "30 ft., swim 30 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 12 (+1) | 15 (+2) | 11 (+0) | 11 (+0) | 15 (+2) |

___Hold Breath.___ The lizardfolk can hold its breath for
15 minutes.

___Inspire Lizardfolk.___ As a bonus action, the lizardfolk
targets an ally it can see within 30 feet that can
see and hear the lizardfolk. The target gains 2d6
temporary hit points.

**Actions**

___Multiattack.___ The lizardfolk makes two attacks: one
with its bite and one with its claws or trident or
two melee attacks with its trident.

___Blackjaw Bite.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing
damage and 2 (1d4) fire damage

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 5 (1d4 + 3) slashing damage.

___Trident.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d6 + 3) piercing damage, or 7 (1d8 + 3) piercing damage if used with two hands to make a melee attack.
