"Quetzalcoatlus";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_skills_: "Perception +2"
_speed_: "10 ft., fly 80 ft."
_hit points_: "30 (4d12+4)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 13 (+1) | 13 (+1) | 2 (-4) | 10 (0) | 5 (-3) |

___Dive Attack.___ If the quetzalcoatlus is flying and dives at least 30 feet toward a target and then hits with a bite attack, the attack deals an extra 10 (3d6) damage to the target.

___Flyby.___ The quetzalcoatlus doesn't provoke an opportunity attack when it flies out of an enemy's reach.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 10 ft., one creature. Hit: 12 (3d6+2) piercing damage.