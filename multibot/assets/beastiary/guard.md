"Guard";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1/8 (25 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Perception +2"
_speed_: "30 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "16 (chain shirt, shield)"
_stats_: | 13 (+1) | 12 (+1) | 12 (+1) | 10 (0) | 11 (0) | 10 (0) |

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d6 + 1) piercing damage or 5 (1d8 + 1) piercing damage if used with two hands to make a melee attack.