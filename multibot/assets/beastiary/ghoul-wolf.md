"Ghoul Wolf";;;_size_: Large undead
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "passive Perception 14"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "50 ft."
_hit points_: "52 (8d8 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 13 (+1) | 13 (+1) | 7 (-2) | 11 (+0) | 8 (-1) |

___Keen Hearing and Smell.___ The ghoul wolf has advantage on Wisdom
(Perception) checks that rely on hearing or smell.

___Pack Tactics.___ The ghoul wolf has advantage on attack rolls against a
creature if at least one of the ghoul wolf’s allies is within 5 feet of the
creature and the ally isn’t incapacitated.

**Actions**

___Multiattack.___ The ghoul wolf makes two attacks: one with its bite and
one with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 10
(2d6 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit:
8 (2d4 + 3) slashing damage. If the target is a creature other than an elf
or undead, it must succeed on a DC 13 Constitution saving throw or be
paralyzed for 1 minute. The target can repeat the saving throw at the end
of each of its turns, ending the effect on itself on a success.
