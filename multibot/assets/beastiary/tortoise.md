"Tortoise";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_speed_: "15 ft., swim 40 ft."
_hit points_: "25 (3d10 + 9)"
_armor class_: "16 (hide armor)"
_stats_: | 14 (+2) | 4 (-3) | 16 (+3) | 8 (-1) | 10 (+0) | 3 (-4) |

___Amphibious.___ The tortoise can breathe air and
water.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 7 (2d4 + 2) piercing damage.

**Reactions**

___Withdraw.___ The tortoise adds 2 to its AC against an
attack that would hit it by quickly withdrawing into
its shell.
