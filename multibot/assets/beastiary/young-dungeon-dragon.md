"Young Dungeon Dragon";;;_size_: Large dragon
_alignment_: neutral
_challenge_: "9 (5,000 XP)"
_languages_: "all"
_skills_: "Arcana +6, Nature +6, Investigation +10, Perception +11, Survival +7"
_senses_: "blindsight 30 ft., darkvision 60 ft., passive Perception 21"
_saving_throws_: "Dex +4, Con +9, Wis +7, Cha +8"
_speed_: "40 ft., fly 80 ft."
_hit points_: "199 (19d10 + 95)"
_armor class_: "16 (natural armor)"
_stats_: | 23 (+6) | 10 (+0) | 21 (+5) | 14 (+2) | 16 (+3) | 19 (+4) |

___Special Equipment.___ The dragon is attuned to a crystal ball, which is
hidden somewhere within its labyrinthine lair. If a dungeon dragon loses
its crystal ball, it can create a new one over the course of 10 days.

___Innate Spellcasting.___ The dungeon dragon’s innate spellcasting ability
is Charisma (spell save DC 16, +8 to hit with spell attacks). It can cast
the following spells, requiring no material components.

* At will: _alarm, arcane lock, detect magic, detect thoughts, find traps, glyph of warding, identify, knock_

* 3/day each: _dispel magic, hallucinatory terrain, locate creature, locate object, scrying, stone shape_

* 1/day each: _wall of stone_

___Labyrinthine Recall.___ The dungeon dragon can perfectly recall any path
it has traveled.

___Trap Master.___ The dungeon dragon has advantage on saving throws
against the effects of traps.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 15 ft., one target. Hit: 17
(2d10 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 13
(2d6 + 6) slashing damage.

___Confusion Breath (Recharge 5–6).___ The dragon exhales gas in a 30-foot
cone that spreads around corners. Each creature in that area must succeed
on a DC 17 Constitution saving throw. On a failed save, the creature is
confused for 1 minute. While confused, the creature uses its action to
Dash in a random direction, even if that movement takes the creature into
dangerous areas. A confused creature can repeat the saving throw at the
end of each of its turns, ending the effect on a success.
