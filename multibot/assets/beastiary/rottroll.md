"Rot Troll";;;_page_number_: 244
_size_: Large giant
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "Giant"
_senses_: "darkvision 60 ft., passive Perception 13"
_skills_: "Perception +3"
_damage_immunities_: "necrotic"
_speed_: "30 ft."
_hit points_: "138  (12d10 + 72)"
_armor class_: "16 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 22 (+6) | 5 (-2) | 8 (-1) | 4 (-3) |

___Rancid Degeneration.___ At the end of each of the troll's turns, each creature within 5 feet of it takes 11 (2d10) necrotic damage, unless the troll has taken acid or fire damage since the end of its last turn.

**Actions**

___Multiattack___ The troll makes three attacks: one with its bite and two with its claws.

___Bite___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 16 (3d10) necrotic damage.

___Claws___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 5 (1d10) necrotic damage.