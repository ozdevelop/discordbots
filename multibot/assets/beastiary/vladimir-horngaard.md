"Vladimir Horngaard";;;_page_number_: 241
_size_: Medium undead
_alignment_: lawful evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Str +7, Con +7, Wis +6, Cha +7"
_speed_: "30 ft."
_hit points_: "192 (16d8+64)"
_armor class_: "17 (half plate)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned, stunned"
_damage_resistances_: "necrotic, psychic"
_stats_: | 18 (+4) | 14 (+2) | 18 (+4) | 13 (+1) | 16 (+3) | 18 (+4) |

___Regeneration.___ Vladimir regains 10 hit points at the start of his turn. If he takes fire or radiant damage, this trait doesn't function at the start of his next turn. Vladimir's body is destroyed only if he starts his turn with 0 hit points and doesn't regenerate.

___Rejuvenation.___ When Vladimir's body is destroyed, his soul lingers. After 24 hours, the soul inhabits and animates another corpse on the same plane of existence and regains all its hit points. While the soul is bodiless, a wish spell can be used to force the soul to go to the afterlife and not return.

___Turn Immunity.___ Vladimir is immune to effects that turn undead.

___Vengeful Tracker.___ Vladimir knows the distance to and direction of any creature against which it seeks revenge, even if the creature and Vladimir are on different planes of existence. If the creature being tracked by Vladimir dies, Vladimir knows.

**Actions**

___Multiattack.___ Vladimir makes two fist attacks or two attacks with his +2 Greatsword.

___Fist.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6+4) bludgeoning damage. If the target is a creature against which Vladimir has sworn vengeance, the target takes an extra 14 (4d6) bludgeoning damage. Instead of dealing damage, Vladimir can grapple the target (escape DC 14) provided the target is Large or smaller.

___Greatsword +2.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 20 (4d6+4) slashing damage. Against Strahd, Vladimir deals an extra 14 (4d6) slashing damage with this weapon.

___Vengeful Glare.___ Vladimir targets one creature he can see within 30 feet of him and against which he has sworn vengeance. The target must make a DC 15 Wisdom saving throw. On a failure, the target is paralyzed until Vladimir deals damage to it, or until the end of Vladimir's next turn. When the paralysis ends, the target is frightened of Vladimir for 1 minute. The frightened target can repeat the saving throw at the end of each of its turns, with disadvantage if it can see Vladimir, ending the frightened condition on itself on a success.