"Balhannoth";;;_page_number_: 119
_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "11 (7200 XP)"
_languages_: "understands Deep Speech, telepathy 1 mile"
_senses_: "blindsight 500 ft. (blind beyond this radius), passive Perception 16"
_skills_: "Perception +6"
_saving_throws_: "Con +8"
_speed_: "25 ft., climb 25 ft."
_hit points_: "114  (12d10 + 48)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "blinded"
_stats_: | 17 (+3) | 8 (-1) | 18 (+4) | 6 (-2) | 15 (+2) | 8 (-1) |

___Legendary Resistance (2/Day).___ If the balhannoth fails a saving throw, it can choose to succeed instead.

**Actions**

___Multiattack___ The balhannoth makes a bite attack and up to two tentacle attacks, or it makes up to four tentacle attacks.

___Bite___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 25 (4d10 + 3) piercing damage.

___Tentacle___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage, and the target is grappled (escape DC 15) and is moved up to 5 feet toward the balhannoth. Until this grapple ends, the target is restrained, and the balhannoth can't use this tentacle against other targets. The balhannoth has four tentacles.

**Legendary** Actions

The balhannoth can take 3 legendary actions, choosing from the options below. Only one legendary action can be used at a time and only at the end of another creature's turn. The balhannoth regains spent legendary actions at the start of its turn.

___Bite Attack___ The balhannoth makes one bite attack against one creature it has grappled.

___Teleport___ The balhannoth magically teleports, along with any equipment it is wearing or carrying and any creatures it has grappled, up to 60 feet to an unoccupied space it can see.

___Vanish___ The balhannoth magically becomes invisible for up to 10 minutes or until immediately after it makes an attack roll.