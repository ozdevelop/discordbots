"Crabstrosity";;;_size_: Gargantuan monstrosity
_alignment_: unaligned
_challenge_: "10 (5,900 XP)"
_languages_: "--"
_skills_: "Athletics +10, Perception +4"
_senses_: "passive Perception 14"
_saving_throws_: "Str +10, Con +9"
_condition_immunities_: "grappled, prone, restrained"
_speed_: "50 ft."
_hit points_: "186 (12d20 + 60)"
_armor class_: "15 (natural armor)"
_stats_: | 23 (+6) | 10 (+0) | 20 (+5) | 3 (-4) | 10 (+0) | 2 (-4) |

___Vicegrip.___ The crabstrosity can have the same target
grappled by both claws. When a creature makes a
check to break free of the crabstrosity's grapple, it is
freed from both claws on a success. A target grappled
by both claws is considered restrained.

___Powerful Claws.___ Creatures may target the crabstrosities
claws individually. The large claw has 75 hit points and
AC 15 while the small claw has 50 hit points and AC
13. If either claw takes more than 25 points of damage
in a single round, it will release any creature it is
currently grappling. When the claw is reduced to 0 hit
points, it falls limply to the ground and can no longer
attack.

**Actions**

___Multiattack.___ The crabstrosity makes three attacks: one
with its big claw, one with its small claw, and one with
its stomp.

___Big Claw.___ Melee Weapon Attack: +10 to hit, reach 10
ft., one target. Hit: 25 (3d12 + 6) piercing damage and
the target is grappled (escape DC 18) if it is Huge or
smaller and the crabstrosity doesn’t have another
creature grappled in its big claw.

___Small Claw.___ Melee Weapon Attack: +10 to hit, reach 10
ft., one target. Hit: 16 (3d6 + 6) piercing damage and
the target is grappled (escape DC 18) if it is Huge or
smaller and the crabstrosity doesn’t have another
creature grappled in its small claw.

___Stomp.___ Melee Weapon Attack: +10 to hit, reach 10 ft.,
one target. Hit: 17 (2d10 + 6) bludgeoning and the
target must succeed on a DC 18 Strength saving throw
or be knocked prone.

___Eviscerate.___ The crabstrosity attempts to eviscerate a
target grappled by both of its claws. The grappled
creature takes 52 (8d12) piercing damage. If a creature
is reduced to 0 hit points from this attack, their body is
brutally torn to pieces.
