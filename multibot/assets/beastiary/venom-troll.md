"Venom Troll";;;_page_number_: 245
_size_: Large giant
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Giant"
_senses_: "darkvision 60 ft., passive Perception 12"
_skills_: "Perception +2"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "94  (9d10 + 45)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 18 (+4) | 13 (+1) | 20 (+5) | 7 (-1) | 9 (0) | 7 (-1) |

___Keen Smell.___ The troll has advantage on Wisdom (Perception) checks that rely on smell.

___Poison Splash.___ When the troll takes damage of any type but psychic, each creature within 5 feet of the troll takes 9 (2d8) poison damage.

___Regeneration.___ The troll regains 10 hit points at the start of each of its turns. If the troll takes acid or fire damage, this trait doesn't function at the start of the troll's next turn. The troll dies only if it starts its turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack___ The troll makes three attacks: one with its bite and two with its claws.

___Bite___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 4 (1d8) poison damage, and the creature is poisoned until the start of the troll's next turn.

___Claws___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 4 (1d8) poison damage.

___Venom Spray (Recharge 6)___ The troll slices itself with a claw, releasing a spray of poison in a 15-foot cube. The troll takes 7 (2d6) slashing damage (this damage can't be reduced in any way). Each creature in the area must make a DC 16 Constitution saving throw. On a failed save, a creature takes 18 (4d8) poison damage and is poisoned for 1 minute. On a successful save, the creature takes half as much damage and isn't poisoned. A poisoned creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.