Abomination from the Deep;;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: 6 (2,300 XP)
_languages_: Abyssal
_skills_: Athletics +7, Perception +4, Stealth +5, Survival +4
_senses_: darkvision 120 ft., passive Perception 14
_speed_: 30 ft., swim 60 ft.
_hit points_: 120 (16d10 + 32)
_armor class_: 15 (natural armor)
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 13 (+1) | 12 (+1) | 7 (-2) |

___Amphibious.___ The abomination can breathe air and water.

___Natural Camouflage.___ The abomination has advantage
on stealth checks in aquatic environments.

**Actions**

___Multiattack.___ The abomination makes three attacks:
two with its claws and one with its bite.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5ft.,
one target. Hit: 8 (1d8 + 4) slashing damage.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5ft.,
one target. Hit: 13 (2d8 + 4) piercing damage, and
the target must succeed on a DC 14 Constitution
saving throw against being paralyzed. On a failed
save, the creature’s body begins to go numb and is
restrained. It must repeat the saving throw at the
end of its next turn. On a success, the effect ends.
On a failure, the creature is paralyzed for 1 hour.

___Putrid Water Spray (Recharge 5-6).___ The abomination
unleashes a spray of foul water in a 30-foot line.
Each creature in that area must make a DC 14
Dexterity saving throw, taking 16 (3d10) necrotic
damage and 16 (3d10) poison damage on a failed
save, or half as much damage on a successful one.
The abomination can track any creature affected by
this ability up to 5 miles away for the next 24
hours.
