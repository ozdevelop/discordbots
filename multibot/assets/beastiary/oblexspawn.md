"Oblex Spawn";;;_page_number_: 217
_size_: Tiny ooze
_alignment_: lawful evil
_challenge_: "1/4 (50 XP)"
_languages_: "-"
_senses_: "blindsight 60 ft. (blind beyond this distance), passive Perception 12"
_saving_throws_: "Int +4, Cha +2"
_speed_: "20 ft."
_hit points_: "18  (4d4 + 8)"
_armor class_: "13"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, prone"
_stats_: | 8 (-1) | 16 (+3) | 15 (+2) | 14 (+2) | 11 (0) | 10 (0) |

___Amorphous.___ The oblex can move through a space as narrow as 1 inch wide without squeezing.

___Aversion to Fire.___ If the oblex takes fire damage, it has disadvantage on attack rolls and ability checks until the end of its next turn.

**Actions**

___Pseudopod___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) bludgeoning damage plus 2 (1d4) psychic damage.