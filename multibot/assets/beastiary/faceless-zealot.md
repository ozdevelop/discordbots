"Faceless Zealot";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "telepathy 120 ft."
_senses_: "blindsight 30 ft., passive Perception 8"
_speed_: "30 ft."
_hit points_: "16 (3d8 + 3)"
_armor class_: "12"
_stats_: | 12 (+1) | 14 (+2) | 13 (+1) | 16 (+3) | 6 (-2) | 7 (-2) |

**Actions**

___Multiattack.___ The zealot makes a dagger attack and
then attempts to grapple a target.

___Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 4 (1d4 + 2) piercing damage.

___Mental Assault (1/Day).___ The zealot attempts to
overwhelm the mind of a creature that it has
grappled. That creature must succeed on a DC 13
Intelligence saving throw or take 5 (1d10) psychic
damage and come under the effects of one of the
following madness effects for one hour:
* Blinding Agony – The creature closes their eyes
and is convinced they have been sealed shut
permanently. The creature is blind until the effect
fades.
* Silence Eternal – The creature closes their mouth
and is convinced it has been sealed shut
permanently. The creature cannot speak until the
effect fades.
* Maddening Gibberish - All words the creature
hears become twisted and garbled, shifting
pitches and order in unpredictable ways. The
creature cannot hear the voices of others until
the effect fades.
