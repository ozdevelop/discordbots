"Star Spawn Mangler";;;_page_number_: 236
_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Deep Speech"
_senses_: "darkvision 60 ft., passive Perception 11"
_skills_: "Stealth +7"
_damage_immunities_: "psychic"
_saving_throws_: "Dex +7, Con +4"
_speed_: "40 ft., climb 40 ft."
_hit points_: "71  (13d8 + 13)"
_armor class_: "14"
_condition_immunities_: "charmed, frightened, prone"
_damage_resistances_: "cold"
_stats_: | 8 (-1) | 18 (+4) | 12 (+1) | 11 (0) | 12 (+1) | 7 (-1) |

___Ambush.___ On the first round of each combat, the mangler has advantage on attack rolls against a creature that hasn't taken a turn yet.

___Shadow Stealth.___ While in dim light or darkness, the mangler can take the Hide action as a bonus action.

**Actions**

___Multiattack___ The mangler makes two claw attacks.

___Claw___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage. If the attack roll has advantage, the target also takes 7 (2d6) psychic damage.

___Flurry of Claws (Recharge 4-6)___ The mangler makes six claw attacks against one target. Either before or after these attacks, it can move up to its speed as a bonus action without provoking opportunity attacks.