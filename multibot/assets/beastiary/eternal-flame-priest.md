"Eternal Flame Priest";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Ignan"
_skills_: "Deception +5, Intimidation +5, Religion +2"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "12 (15 with mage armor)"
_damage_resistances_: "fire"
_stats_: | 12 (+1) | 15 (+2) | 14 (+2) | 10 (0) | 11 (0) | 16 (+3) |

___Spellcasting.___ The priest is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _control flames, create bonfire, fire bolt, light, minor illusion_

* 1st level (4 slots): _burning hands, expeditious retreat, mage armor_

* 2nd level (3 slots): _blur, scorching ray_

* 3rd level (2 slots): _fireball_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
