"Cave Giant";;;_size_: Huge giant
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Giant"
_senses_: "darkvision 120 ft., passive Perception 13"
_skills_: "Perception +3"
_speed_: "40 ft."
_hit points_: "137 (11d12 + 66)"
_armor class_: "14 (natural armor)"
_stats_: | 23 (+6) | 10 (+0) | 22 (+6) | 6 (-2) | 10 (+0) | 7 (-2) |

___Camouflage.___ The giant has advantage on Dexterity (Stealth)
checks to hide in cavernous and rocky underground terrain.

**Actions**

___Multiattack.___ The cave giant makes two greataxe attacks.

___Greataxe.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one
target. Hit: 19 (2d12 + 6) slashing damage.

___Rock.___ Ranged Weapon Attack: +9 to hit, range 60/240 ft., one target.
Hit: 28 (4d10 + 6) bludgeoning damage. If the target is a creature, it must
succeed on a DC 16 Strength saving throw or be knocked prone.

**Reactions**

___Rock Catching.___ If a rock or similar object is hurled at the giant, the
giant can, with a successful DC 10 Dexterity saving throw, catch the
missile and take no bludgeoning damage from it.
