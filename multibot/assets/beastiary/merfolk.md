"Merfolk";;;_size_: Medium humanoid (merfolk)
_alignment_: neutral
_challenge_: "1/8 (25 XP)"
_languages_: "Aquan, Common"
_skills_: "Perception +2"
_speed_: "10 ft., swim 40 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "11"
_stats_: | 10 (0) | 13 (+1) | 12 (+1) | 11 (0) | 11 (0) | 12 (+1) |

___Amphibious.___ The merfolk can breathe air and water.

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +2 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 3 (1d6) piercing damage, or 4 (1d8) piercing damage if used with two hands to make a melee attack.