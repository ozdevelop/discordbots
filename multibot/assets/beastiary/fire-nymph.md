"Fire Nymph";;;_size_: Medium elemental
_alignment_: chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Ignan"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "16 (3d8 + 3)"
_armor class_: "13 (natural armor)"
_stats_: | 10 (+0) | 13 (+1) | 12 (+1) | 16 (+3) | 17 (+3) | 19 (+4) |

___Burn.___ Any creature that touches the nymph or hits it with a melee
attack while within 5 feet of it takes 10 (3d6) fire damage.

___Innate Spellcasting.___ The fire nymph’s spellcasting ability is Charisma
(spell save DC 14, +6 to hit with spell attacks). The fire nymph can innately
cast each of the following spells, requiring no material components:

* At will: _burning hands, flame blade, flaming sphere, produce flame_

* 1/day: _fire shield_

___Magic Resistance.___ The fire nymph has advantage on saving throws
against spells and other magical effects.

**Actions**

___Multiattack.___ The fire nymph makes two attacks: one with its dagger
and one with its fist.

___Dagger.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 3 (1d4 + 1) piercing damage plus 10 (3d6) fire
damage.

___Fist.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) bludgeoning damage plus 10 (3d6) fire damage.
