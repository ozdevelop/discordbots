"Defender Globe";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "lightning"
_damage_resistances_: "cold, fire"
_speed_: "5 ft., fly 40 ft."
_hit points_: "22 (5d6 + 5)"
_armor class_: "14 (natural armor)"
_stats_: | 4 (-3) | 16 (+3) | 12 (+1) | 4 (-3) | 12 (+1) | 14 (+2) |

___Hyper-Awareness.___ The defender globe cannot be
surprised.

___Flight.___ The defender globe’s ability to fly is
magical in nature and does not work in areas
where an antimagic effect is active.

**Actions**

___Electrical Bolt.___ Ranged Weapon Attack: +5 to hit, range 30/60 ft., one target. Hit: 10 (2d6 + 3) lightning damage.
