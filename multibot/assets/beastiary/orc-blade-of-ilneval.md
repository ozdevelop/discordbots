"Orc Blade of Ilneval";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Insight +3, Intimidation +4, Perception +3"
_saving_throws_: "Wis +3"
_speed_: "30 ft."
_hit points_: "60 (8d8+24)"
_armor class_: "18 (chain mail, shield)"
_stats_: | 17 (+3) | 11 (0) | 17 (+3) | 10 (0) | 12 (+1) | 14 (+2) |

___Aggressive.___ As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.

___Foe Smiter of Ilneval.___ The orc deals an extra die of damage when it hits with a longsword attack (included in the attack).

**Actions**

___Multiattack.___ The orc makes two melee attacks with its longsword or two ranged attacks with its javelins. If Ilneval's Command is available to use, the orc can use it after these attacks.

___Longsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8+3) slashing damage, or 14 (2d10+3) slashing damage when used with two hands.

___Javelin.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 6 (1d6+3) piercing damage.

___Ilneval's Command (Recharges 4-6).___ Up to three allied orcs within 120 feet of this orc that can hear it can use their reactions to each make one weapon attack.