"Netherspark";;;_size_: Medium elemental
_alignment_: neutral evil
_challenge_: "6 (2,300 XP)"
_languages_: "Celestial, Common, Draconic, Infernal"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "necrotic"
_damage_resistances_: "radiant"
_condition_immunities_: "charmed, exhaustion, frightened"
_speed_: "30 ft."
_hit points_: "90 (12d8 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 16 (+3) | 16 (+3) | 14 (+2) | 16 (+3) | 14 (+2) |

___Magic Resistance.___ The netherspark has advantage on saving
throws against spells and other magic effects.

___Necrotic Aura.___ Being composed of necrotic energy, a netherspark
radiates an aura of such energy in a 10-foot radius. Any creature
that’s not an undead that enters or starts its turn within this area takes
7 (2d6) necrotic damage. Undead in the area instead regain 7 (2d6)
hit points, up to their maximum hit points at the beginning of each
of their turns.

**Actions**

___Multiattack.___ The netherspark makes three slam
attacks.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 11 (2d6 + 4) necrotic damage.

___Necrotic Ray.___ Ranged Weapon Attack: +6 to hit,
range 40 ft., one target. Hit: 12 (2d8+3) necrotic
damage. Undead take no damage but heal a number of
hit points equal to what the ray would otherwise deal.

___Necrotic Burst (Recharge 5–6).___ A netherspark can
release a burst of necrotic energy in a 20-foot radius
around it. Creatures in the area must make a DC 13
Constitution saving throw, taking 12 (2d8+3) necrotic
damage on a failed save, or half as much damage on
a successful one. Undead take no damage but heal a
number of hit points equal to what the burst would
otherwise deal.
