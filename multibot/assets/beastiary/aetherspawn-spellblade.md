"Aetherspawn Spellblade";;;_size_: Medium elemental
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "Primordial"
_senses_: "passive Perception 12"
_damage_resistances_: "bludgeoning, piercing, slashing damage from nonmagical weapons; see Elemental Attunement"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "30 ft., fly 30 ft. (hover)"
_hit points_: "45 (7d8 + 14)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 17 (+3) | 15 (+2) | 16 (+3) | 15 (+2) | 10 (+0) |

___Elemental Attunement.___ The spellblade is infused with
either arcane, ice, fire, or lightning energy. The
spellblade's attacks deal extra damage based upon
this element (included in the attack), it has
resistances to the corresponding element, and can
cast a corresponding elemental spell twice per day
requiring no material components. The spellblade's
spellcasting ability is Intelligence (spell save DC 13, +5 bonus to hit).

* Arcane – bonus force damage and resistance to force damage; can cast _magic missile_
* Ice - bonus cold damage and resistance to cold damage; can cast _ice knife_
* Fire – bonus fire damage and resistance to fire damage; can cast _burning hands_
* Lightning – bonus lightning damage and resistance to lightning damage; can cast _witch bolt_

___Overcharging Strikes.___ If the spellblade hits the same
target with both of its aether blade attacks in a single
turn, the target detonates with elemental energy. That
creature takes an additional 7 (2d6) damage of a type
corresponding to the guardian's Elemental Attunement.

**Actions**

___Multiattack.___ The spellblade makes two melee attacks.

___Aether Blades.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 6 (1d6 + 3) slashing damage plus 3
(1d6) damage of the type corresponding to the
spellblade's Elemental Attunement.

___Elemental Shards.___ Ranged Weapon Attack: +5 to hit,
range 30/90 ft., one target. Hit: 5 (1d4 + 3) piercing
damage plus 3 (1d6) damage of the type
corresponding to the spellblade's Elemental
Attunement.

**Reactions**

___Deflecting Blades.___ The spellblade raises its aether blades
in an attempt to deflect a single target ranged spell or
weapon attack that would hit it. The spellblade adds 2
to its AC against that attack. To do so, the spellblade
must be able to see the attack.
