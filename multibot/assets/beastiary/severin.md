"Severin";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "11 (7,200 XP)"
_languages_: "Common, Draconic, Infernal"
_senses_: "While wearing the Mask of the Dragon Queen: darkvision 60 ft."
_skills_: "Arcana +7, Religion +7"
_damage_immunities_: "While wearing the mask of the Dragon Queen: fire"
_saving_throws_: "Dex +5, Wis +5"
_speed_: "30 ft."
_hit points_: "150 (20d8+60)"
_armor class_: "16"
_condition_immunities_: "While wearing the mask of the Dragon Queen: charmed, frightened, poisoned"
_damage_resistances_: "While wearing the mask of the Dragon Queen: acid, cold, lightning, poison; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_stats_: | 10 (0) | 13 (+1) | 16 (+3) | 17 (+3) | 12 (+1) | 20 (+5) |

___Special Equipment.___ Severin has the Mask of the Dragon Queen.

___Draconic Majesty.___ Severin adds his Charisma bonus to his AC (included).

___Ignite Enemy.___ If Severin deals fire damage to a creature while wearing the Mask of the Dragon Queen, the target catches fire. At the start of each of its turns, the burning target takes 5 (1d10) fire damage. A creature within reach of the fire can use an action to extinguish it.

___Legendary Resistance (5/Day).___ While wearing the Mask of the Dragon Queen, if Severin fails a saving throw, he can choose to succeed instead.

**Actions**

___Burning Touch.___ Melee Spell Attack: +5 to hit, reach 5 ft., one target. Hit: 18 (4d8) fire damage.

___Flaming Orb.___ Ranged Spell Attack: +5 to hit, range 90 ft., one target. Hit: 40 (9d8) fire damage.

___Scorching Burst.___ Severin chooses a point he can see within 60 feet of him. Each creature within 5 feet of that point must make a DC 17 Dexterity saving throw, taking 18 (4d8) fire damage on a failed save, or half as much damage on a successful one.

**Legendary** Actions

If Severin is wearing the Mask of the Dragon Queen, he can take 3 legendary actions, choosing from the options listed. Only one legendary action option can be used at a time and only at the end of another creature's turn. Severin regains spent legendary actions at the start of his turn.

___Attack.___ Severin makes one attack.

___Fiery Teleport (Costs 2 Actions).___ Severin, along with any objects he is wearing or carrying, teleports up to 60 feet to an unoccupied space he can see. Each creature within 5 feet of Severin before he teleports takes 5 (1d10) fire damage.

___Hellish Chains (Costs 3 Actions).___ Severin targets one creature he can see within 30 feet of him. The target is wrapped in magical chains of fire and restrained. The restrained target takes 21 (6d6) fire damage at the start of each of its turns. At the end of its turns, the target can make a DC 17 Strength saving throw, ending the effect on itself on a success.