"Adult Gray Dragon";;;_size_: Huge dragon
_alignment_: neutral evil
_challenge_: "17 (18,000 XP)"
_languages_: "Abyssal, Aquan, Common, Draconic, Giant"
_skills_: "Perception +14, Stealth +8"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 24"
_saving_throws_: "Dex +8, Con +11, Wis +8, Cha +9"
_damage_immunities_: "fire"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "218 (19d12 + 95)"
_armor class_: "20 (natural armor)"
_stats_: | 24 (+7) | 15 (+2) | 21 (+5) | 15 (+2) | 15 (+2) | 17 (+3) |

___Amphibious.___ The dragon can breathe air and water.

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it
can choose to succeed instead.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 18
(2d10 + 7) piercing damage plus 5 (1d10) fire damage.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 14
(2d6 + 7) slashing damage.

___Tail.___ Melee Weapon Attack: +13 to hit, reach 20 ft., one target. Hit: 16
(2d8 + 7) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon’s choice that is within
120 feet of the dragon and aware of it must succeed on a DC 17 Wisdom
saving throw or become frightened for 1 minute. A creature can repeat the
saving throw at the end of each of its turns, ending the effect on itself on
a success. If a creature’s saving throw is successful of the effect ends for
it, the creature is immune to the dragon’s Frightful Presence for the next
24 hours.

___Steam Breath (Recharge 5–6).___ The dragon exhales steam in a 60 cone.
Creatures in the area must make a DC 19 Constitution saving throw. On
a failed saving throw, the creature takes 63 (18d6) fire damage, or half as
much damage on a successful saving throw. Being underwater doesn’t
grant resistance to this damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 24 Dexterity
saving throw or take 14 (2d6 + 7) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.
