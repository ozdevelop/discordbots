"Mallqui";;;_size_: Medium undead
_alignment_: lawful neutral
_challenge_: "8 (3900 XP)"
_languages_: "the languages it knew in life"
_skills_: "History +3, Insight +6, Religion +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Int +3, Cha +5"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "cold, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "20 ft."
_hit points_: "120 (16d8 + 48)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 9 (-1) | 16 (+3) | 11 (+0) | 16 (+3) | 14 (+2) |

___Regeneration.___ The mallqui regains 10 hit points at the start of its turn. If the mallqui takes damage from its Water Sensitivity trait, its regeneration doesn't function at the start of the mallqui's next turn. The mallqui dies only if it starts its turn with 0 hit points and doesn't regenerate.

___Innate Spellcasting.___ The mallqui's innate spellcasting ability is Wisdom (spell save DC 15, +7 to hit with spell attacks). It can cast the following spells, requiring no material components:

* At will: _druidcraft, produce flame_

* 4/day each: _create or destroy water, entangle_

* 2/day: _locate animals or plants_

* 1/day each: _dispel magic, plant growth, wind wall_

___Water Sensitivity.___ The flesh of a mallqui putrefies and dissolves rapidly when soaked with water in the following ways:

- Splashed with a waterskin or equivalent: 1d10 damage

- Attacked by creature made of water: Normal damage plus an extra 1d10 damage

- Caught in rain: 2d10 damage per round (DC 11 Dexterity saving throw for half)

- Immersed in water: 4d10 damage per round (DC 13 Dexterity saving throw for half)

Alternatively, the saving throw and DC of the spell used to conjure or control the water damaging the mallqui can be used in place of the saving throws above

**Actions**

___Multiattack.___ The mallqui can use its xeric aura and makes two attacks with its desiccating touch.

___Desiccating Touch.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 20 (5d6 + 3) necrotic damage.

___Xeric Aura.___ All creatures within 20 feet of the mallqui must succeed on a DC 15 Constitution saving throw or take 11 (2d10) necrotic damage and gain a level of exhaustion. A creature becomes immune to the mallqui's xeric aura for the next 24 hours after making a successful save against it.

___Xeric Blast.___ Ranged Spell Attack: +7 to hit, range 30/90 ft., one target. Hit: 13 (3d6 + 3) necrotic damage.
