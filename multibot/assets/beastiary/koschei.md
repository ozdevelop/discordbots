"Koschei";;;_size_: Medium fiend
_alignment_: neutral evil
_challenge_: "17 (18000 XP)"
_languages_: "Abyssal, Common, Celestial, Dwarvish, Infernal"
_skills_: "Arcana +9, Insight +7, Perception +7"
_senses_: "darkvision 60 ft., passive Perception 17"
_saving_throws_: "Dex +7, Wis +7, Cha +11"
_damage_immunities_: "necrotic; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_damage_resistances_: "cold, lightning"
_condition_immunities_: "charmed, exhaustion, frightened"
_speed_: "30 ft."
_hit points_: "135 (18d8 + 54)"
_armor class_: "18 (natural armor)"
_stats_: | 22 (+6) | 12 (+1) | 17 (+3) | 17 (+3) | 13 (+1) | 21 (+5) |

___Hidden Soul.___ A creature holding the egg containing Koschei's soul can use an action to compel Koschei as if a dominate monster spell were cast on him and Koschei failed his saving throw. As long as the soul is within the needle, Koschei can't permanently die. If he is killed, his body reforms in his lair in 1d10 days. If the needle is broken, Koschei can be killed like any other creature.

___Innate Spellcasting.___ Koschei's innate spellcasting attribute is Charisma (spell save DC 19, +11 to hit with spell attacks). He can innately cast the following spells, requiring no material components.

At will: detect magic, phantom steed, scorching ray, sending

3/day each: invisibility, magic missile, shield

2/day each: animate objects, cone of cold, hypnotic pattern

1/day each: disintegrate, meteor swarm, true polymorph

___Legendary Resistance (3/day).___ If Koschei fails a saving throw, he can choose to succeed instead.

___Magic Weapons.___ Koschei's weapon attacks are magical and do an extra 14 (4d6) necrotic damage (included below).

___Koschei's Lair Actions.___ On initiative count 20 (losing initiative ties), Koschei takes a lair action to cause one of the following effects; Koschei can't use the same effect two rounds in a row:

- Koschei creates a whirlwind centered on a point he can see within 100 feet. The whirlwind is 10 feet wide and up to 50 feet tall. A creature in the area of the whirlwind when it's created, or who enters the area for the first time on a turn, must make a DC 15 Strength saving throw. On a failed save, the creature is restrained and takes 18 (4d8) bludgeoning damage fromthe buffeting wind. A restrained creature can escape from the

whirlwind by using its action to repeat the saving throw; on a success, it moves 5 feet outside the area of the whirlwind. The whirlwind lasts until Koschei uses this action again or dies

- Tortured spirits appear and attack up to three creatures Koschei can see within the lair. One attack is made against each targeted creature; each attack has +8 to hit and does 10 (3d6) necrotic damage.

- Koschei disrupts the flow of magic in his lair. Until initiative count 20 on the following round, any creature other than a fiend who targets Koschei with a spell must make a DC 15 Wisdom saving throw. On a failure, the creature still casts the spell, but it must target a creature other than Koschei.

___Regional Effects.___ The region containing Koschei's lair is warped by Koschei's magic, which creates one or more of the following effects:

- Rabbits, ducks, and other game animals become hostile toward intruders within 5 miles of the lair. They behave aggressively, but only attack if cornered. Foraging for food by hunting is difficult and only yields half the normal amount of food.

- Wind and snowstorms are common within 5 miles of the lair.

- Koschei is aware of any spell cast within 5 miles of his lair. He knows the source of the magic (innate, the caster's class, or a magic item) and knows the direction to the caster.

If Koschei dies, conditions in the area surrounding his lair return to normal over the course of 1d10 days.

**Actions**

___Multiattack.___ Koschei makes two longsword attacks and one drain life attack.

___Longsword.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 10 (1d8 + 6) slashing damage or 11 (1d10 + 6) slashing damage if used in two hands plus 14 (4d6) necrotic damage.

___Drain Life.___ Melee Spell Attack: +11 to hit, reach 5 ft., one target. Hit: 20 (4d6 + 6) necrotic damage. The target must succeed on a DC 19 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken from this attack, and Koschei regains an equal number of hit points. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

**Legendary** Actions

___Koschei can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. Koschei regains spent legendary actions at the start of its turn.

___Attack.___ Koschei makes one attack with his longsword.

___Teleport.___ Koschei teleports to an unoccupied space he can see within 40 feet.

___Drain (2 actions).___ Koschei makes one attack with Drain Life.

