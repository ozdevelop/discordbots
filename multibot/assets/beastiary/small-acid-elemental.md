"Small Acid Elemental";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Stealth +6"
_senses_: "darkvision 60 ft., passive perception 10"
_damage_immunities_: "acid, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "20 ft., swim 80 ft."
_hit points_: "60 (8d6 + 32)"
_armor class_: "12"
_stats_: | 21 (+5) | 15 (+2) | 19 (+4) | 6 (-2) | 11 (+0) | 11 (+0) |

___Acid.___ A creature that touches the acid elemental or hits it with a melee
attack while within 5 feet of it takes 5 (2d4) acid damage. Any nonmagical
weapon made of metal or wood that hits the acid elemental corrodes.
After dealing damage, the weapon takes a permanent and cumulative
–1 penalty to damage rolls. If its penalty drops to –5, the weapon is
destroyed. Nonmagical ammunition made of metal or wood that hits the
acid elemental is destroyed after dealing damage. The acid elemental can
eat through 2-inch-thick, nonmagical wood or metal in 1 round.

___Fumes.___ Creatures who begin their turn within 5 feet of the acid
elemental must succeed on a DC 15 Constitution saving throw or be
poisoned until the start of their next turn. On a successful saving throw,
the creature is immune to the elemental’s fumes for 24 hours.

___Vulnerability to Water.___ For every 5 feet that the elemental moves in
water, or for every gallon of water splashed on it, it takes 1 fire damage.

**Actions**

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6
(1d8 + 2) bludgeoning damage plus 7 (2d6) acid damage. In addition,
nonmagical armor worn by the target is partly dissolved and takes a
permanent and cumulative –1 penalty to the AC it offers. The armor is
destroyed if the penalty reduces its AC to 10.
