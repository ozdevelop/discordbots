"Tanarukk";;;_size_: Medium fiend (demon
_alignment_: orc)
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal, Common, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +2, Perception +2"
_speed_: "30 ft."
_hit points_: "95 (10d8+50)"
_armor class_: "14 (natural armor)"
_damage_resistances_: "fire, poison"
_stats_: | 18 (+4) | 13 (+1) | 20 (+5) | 9 (-1) | 9 (-1) | 9 (-1) |

___Aggressive.___ As a bonus action, the tanarukk can move up to its speed toward a hostile creature that it can see.

___Magic Resistance.___ The tanarukk has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The tanarukk makes two attacks: one with its bite and one with its greatsword.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8+4) piercing damage.

___Greatsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6+4) slashing damage.

**Reactions**

___Unbridled Fury.___ In response to being hit by a melee attack, the tanarukk can make one melee weapon attack with advantage against the attacker.