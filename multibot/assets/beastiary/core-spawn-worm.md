"Core Spawn Worm";;;_size_: Gargantuan aberration
_alignment_: chaotic evil
_challenge_: "15 (13,000 XP)"
_languages_: "understands Deep Speech but can't speak"
_skills_: "Perception +4"
_saving_throws_: "Con +10, Wis +4"
_senses_: "blindsight 30 ft., tremorsense 60 ft., passive Perception 14"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire, psychic"
_condition_immunities_: "charmed, frightened"
_speed_: "60 ft., burrow 40 ft."
_hit points_: "279 (18d20 + 90)"
_armor class_: "18 (natural armor)"
_stats_: | 26 (+8) | 5 (-3) | 20 (+5) | 6 (-2) | 8 (-1) | 4 (-3) |

___Illumination.___ The worm sheds dim light in a 20-foot radius.

___Radiant Mirror.___ If the worm takes radiant damage, each creature within 20 feet of it takes that damage as well.

___Tunneler.___ The worm can burrow through solid rock at half its burrowing speed and leaves a 10-foot-diameter tunnel in its wake.

**Actions**

___Multiattack.___ The worm makes two attacks: one with its barbed tentacles and one with its bite.

___Barbed Tentacles.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one creature. Hit: 25 (5d6 + 8) piercing damage, and the target is grappled (escape DC 18). Until this grapple ends, the target is restrained. The tentacles can grapple only one creature at a time.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 30 (5d8 + 8) piercing damage. If the target is a Large or smaller creature, it must succeed on a DC 18 Dexterity saving throw or be swallowed by the worm. A swallowed creature is blinded and restrained, has total cover against attacks and other effects outside the worm, and takes 21 (6d6) fire damage at the start of each of the worm’s turns.

If the worm takes 30 damage or more on a single turn from a creature inside it, the worm must succeed on a DC 21 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet of the worm. If the worm dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 20 feet of movement, exiting prone.
