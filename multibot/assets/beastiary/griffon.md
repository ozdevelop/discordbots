"Griffon";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +5"
_speed_: "30 ft., fly 80 ft."
_hit points_: "59 (7d10+21)"
_armor class_: "12"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 2 (-4) | 13 (+1) | 8 (-1) |

___Keen Sight.___ The griffon has advantage on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The griffon makes two attacks: one with its beak and one with its claws.

___Beak.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.