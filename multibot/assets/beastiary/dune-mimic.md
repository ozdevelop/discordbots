"Dune Mimic";;;_size_: Huge monstrosity
_alignment_: neutral
_challenge_: "8 (3900 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., tremorsense 30 ft., passive Perception 14"
_damage_immunities_: "acid"
_condition_immunities_: "prone"
_speed_: "10 ft."
_hit points_: "168 (16d12 + 64)"
_armor class_: "13 (natural armor)"
_stats_: | 20 (+5) | 8 (-1) | 18 (+4) | 9 (-1) | 13 (+1) | 10 (+0) |

___Shapechanger.___ The dune mimic can use its action to polymorph into a Huge object or terrain feature (maximum area 25 x 25 feet) or back into its true, amorphous form. Since its coating of dust, sand, and gravel can't be hidden, it usually disguises itself as a terrain feature or eroded ruin. Its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Adhesive (Object or Terrain Form Only).___ The dune mimic adheres to anything that touches it. A creature adhered to the dune mimic is also grappled by it (escape DC 15). Ability checks made to escape this grapple have disadvantage. The dune mimic can harden its outer surface, so only the creatures it chooses are affected by this trait.

___False Appearance (Object or Terrain Form Only).___ While the dune mimic remains motionless, it is indistinguishable from an ordinary object or terrain feature.

___Grappler.___ The dune mimic has advantage on attack rolls against a creature grappled by it.

**Actions**

___Multiattack.___ The dune mimic makes four pseudopod attacks.

___Pseudopod.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage. If the dune mimic is in object or terrain form, the target is subjected to the mimic's Adhesive trait.

___Engulf.___ The dune mimic engulfs all creatures it has grappled. An engulfed creature can't breathe, is restrained, is no longer grappled, has total cover against attacks and other effects outside the dune mimic, and takes 18 (4d8) acid damage at the start of each of the dune mimic's turns. When the dune mimic moves, the engulfed creature moves with it. An engulfed creature can try to escape by taking an action to make a DC 15 Strength check. On a success, the creature escapes and enters a space of its choice within 5 feet of the dune mimic.

