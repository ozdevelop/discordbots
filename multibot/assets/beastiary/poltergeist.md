"Poltergeist";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "understands all languages it knew in life but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "necrotic, poison"
_speed_: "0 ft., fly 50 ft. (hover)"
_hit points_: "22 (5d8)"
_armor class_: "12"
_condition_immunities_: "charmed, exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_damage_resistances_: "acid, cold, fire, lightning, thunder, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 1 (-5) | 14 (+2) | 11 (0) | 10 (0) | 10 (0) | 11 (0) |

___Incorporeal Movement.___ The poltergeist can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Sunlight Sensitivity.___ While in sunlight, the poltergeist has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Invisibility.___ The poltergeist is invisible.

**Actions**

___Forceful Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft. one creature. Hit: 10 (3d6) force damage.

___Telekinetic Thrust.___ The poltergeist targets a creature or unattended object within 30 feet of it. A creature must be Medium or smaller to be affected by this magic, and an object can weigh up to 150 pounds.

If the target is a creature, the poltergeist makes a Charisma check contested by the target's Strength check. If the poltergeist wins the contest, the poltergeist hurls the target up to 30 feet in any direction, including upward. If the target then comes into contact with a hard surface or heavy object, the target takes 1d6 damage per 10 feet moved.

If the target is an object that isn't being worn or carried, the poltergeist hurls it up to 30 feet in any direction. The poltergeist can use the object as a ranged weapon, attacking one creature along the object's path (+4 to hit) and dealing 5 (2d4) bludgeoning damage on a hit.