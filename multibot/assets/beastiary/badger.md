"Badger";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 30 ft."
_speed_: "20 ft., burrow 5 ft."
_hit points_: "3 (1d4+1)"
_armor class_: "10"
_stats_: | 4 (-3) | 11 (0) | 12 (+1) | 2 (-4) | 12 (+1) | 5 (-3) |

___Keen Smell.___ The badger has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 1 piercing damage.