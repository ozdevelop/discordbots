"Fire Elemental";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Ignan"
_senses_: "darkvision 60 ft."
_damage_immunities_: "fire, poison"
_speed_: "50 ft."
_hit points_: "102 (12d10+36)"
_armor class_: "13"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 10 (0) | 17 (+3) | 16 (+3) | 6 (-2) | 10 (0) | 7 (-2) |

___Fire Form.___ The elemental can move through a space as narrow as 1 inch wide without squeezing. A creature that touches the elemental or hits it with a melee attack while within 5 ft. of it takes 5 (1d10) fire damage. In addition, the elemental can enter a hostile creature's space and stop there. The first time it enters a creature's space on a turn, that creature takes 5 (1d10) fire damage and catches fire; until someone takes an action to douse the fire, the creature takes 5 (1d10) fire damage at the start of each of its turns.

___Illumination.___ The elemental sheds bright light in a 30-foot radius and dim light in an additional 30 ft..

___Water Susceptibility.___ For every 5 ft. the elemental moves in water, or for every gallon of water splashed on it, it takes 1 cold damage.

**Actions**

___Multiattack.___ The elemental makes two touch attacks.

___Touch.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) fire damage. If the target is a creature or a flammable object, it ignites. Until a creature takes an action to douse the fire, the target takes 5 (1d10) fire damage at the start of each of its turns.