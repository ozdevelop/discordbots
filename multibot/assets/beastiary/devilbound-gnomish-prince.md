"Devilbound Gnomish Prince";;;_size_: Small Humanoid
_alignment_: any evil
_challenge_: "9 (5000 XP)"
_languages_: "Common, Infernal, Gnomish"
_skills_: "Arcana +7, Deception +10, History +7, Persuasion +10"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Con +6, Int +7, Cha +10"
_damage_resistances_: "cold, fire, poison; bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "25 ft."
_hit points_: "104 (19d6 + 38)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 10 (+0) | 14 (+2) | 15 (+2) | 16 (+3) | 12 (+1) | 22 (+6) |

___Banishing Word (1/Day).___ When the devilbound gnomish prince hits with an attack, he can choose to banish the target to the Eleven Hells. The target vanishes from its current location and is incapacitated until its return. At the end of the devilbound gnomish prince's next turn, the target returns to the spot it previously occupied or the nearest unoccupied space and takes 55 (10d10) psychic damage.

___Infernal Blessing.___ The devilbound gnomish prince gains 21 temporary hit points when it reduces a hostile creature to 0 hit points.

___Infernal Tie.___ The devilbound gnomish prince can perceive through his imp's senses, communicate telepathically through its mind, and speak through his imp's mouth as long as both of them are on the same plane of existence.

___Innate Spellcasting.___ The devilbound gnomish prince's innate spellcasting ability is Charisma (spell save DC 18, +10 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

At will: detect magic, false life, mage armor

1/rest each: create undead, forcecage, power word stun

___Magic Resistance.___ The devilbound gnomish prince has advantage on all saving throws against spells and magical effects.

___Spellcasting.___ The devilbound gnomish prince is a 15th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 18, +10 to hit with spell attacks). The devilbound gnomish prince has the following warlock spells prepared:

Cantrips (at will): chill touch, eldritch blast, minor illusion, prestidigitation

5th level (3 slots):banishment, command, contact other plane, counterspell, dimension door, fireball, fly, flame strike, hallow, hex, hold monster, invisibility, scorching ray, scrying, wall of fire, witch bolt

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

