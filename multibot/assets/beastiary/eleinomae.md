"Eleinomae";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Aquan, Common, Elvish, Sylvan"
_skills_: "Deception +7, Insight +5, Perception +5"
_senses_: ", passive Perception 15"
_saving_throws_: "Str +4, Dex +7, Con +6, Int +5, Wis +5, Cha +7"
_speed_: "30 ft., swim 30 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "18"
_stats_: | 13 (+1) | 19 (+4) | 16 (+3) | 14 (+2) | 14 (+2) | 19 (+4) |

___Unearthly Grace.___ The eleinomae's Charisma modifier is added to its armor class (included above).

___Reed Walk.___ The eleinomae can move across undergrowth or rivers without making an ability check. Additionally, difficult terrain of this kind doesn't cost it extra moment.

___Innate Spellcasting.___ The eleinomae's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

At will: dancing lights

3/day each: charm person, suggestion

2/day each: hallucinatory terrain, major image

**Actions**

___Multiattack.___ The eleinomae makes three dagger attacks and one reed flower net attack.

___Dagger.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d4 + 4) slashing damage plus 3 (1d6) poison damage.

___Reed Flower Net.___ Ranged Weapon Attack: +7 to hit, range 5/15 ft., one Large or smaller creature. Hit: The target has disadvantage on Wisdom saving throws for 1 minute, and is restrained. A creature can free itself or another creature within reach from restraint by using an action to make a successful DC 15 Strength check or by doing 5 slashing damage to the net (AC 10).

