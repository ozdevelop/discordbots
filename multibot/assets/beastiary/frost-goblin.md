"Frost Goblin";;;_size_: Small humanoid (goblinoid)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Goblin"
_skills_: "Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_speed_: "35 ft."
_hit points_: "13 (3d6 + 3)"
_armor class_: "16 (hide armor, shield)"
_stats_: | 14 (+2) | 18 (+4) | 17 (+3) | 5 (-3) | 10 (+0) | 9 (-1) |

___Nimble Escape.___ The goblin can take the Disengage or
Hide action as a bonus action on each of its turns.

___Snow Tunneler.___ The goblin can burrow through loose
snow at half its walking speed and leaves a 3-footdiameter
tunnel in its wake.

**Actions**

___Flint Dagger.___ Melee or Ranged Weapon Attack: +4 to
hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4
(1d4 + 2) piercing damage.

___Ice Shard Spear.___ Melee or Ranged Weapon Attack: +4
to hit, reach 5 ft. or range 20/60 ft., one target. Hit:
5 (1d6 + 2) piercing damage plus 3 (1d6) cold
damage.
