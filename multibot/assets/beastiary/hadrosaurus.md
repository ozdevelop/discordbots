"Hadrosaurus";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_skills_: "Perception +2"
_speed_: "40 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "11 (natural armor)"
_stats_: | 15 (+2) | 10 (0) | 13 (+1) | 2 (-4) | 10 (0) | 5 (-3) |

**Actions**

___Tail.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10+2) bludgeoning damage.