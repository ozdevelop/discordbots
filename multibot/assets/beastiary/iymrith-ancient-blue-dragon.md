"Iymrith, Ancient Blue Dragon";;;_size_: Gargantuan dragon
_alignment_: lawful evil
_challenge_: "23 (50,000 XP)"
_languages_: "Common, Draconic, Giant, Terran"
_senses_: "blindsight 60 ft., darkvision 120 ft."
_skills_: "Perception +17, Stealth +7"
_damage_immunities_: "lightning"
_saving_throws_: "Dex +7, Con +15, Wis +10, Cha +12"
_speed_: "40 ft., burrow 40 ft., fly 80 ft."
_hit points_: "481 (26d20+208)"
_armor class_: "22 (natural armor)"
_stats_: | 29 (+9) | 10 (0) | 27 (+8) | 18 (+4) | 17 (+3) | 21 (+5) |

___Innate Spellcasting.___ Iymrith's innate spellcasting ability is Charisma (spell save DC 20).  Iymrith's stone shape can create a living gargoyle instead of altering the stone as described in the spell description. She can innately cast the following spells, requiring no material components:

* 1/day each: _counterspell, detect magic, ice storm, stone shape, teleport_

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 20 (2d10 + 9) piercing damage plus 11 (2d10) lightning damage.

___Claw.___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 16 (2d6 + 9) slashing damage.

___Tail.___ Melee Weapon Attack: +16 to hit, reach 20 ft., one target. Hit: 18 (2d8 + 9) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 20 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours.

___Lightning Breath (Recharge 5-6).___ The dragon exhales lightning in a 120-foot line that is 10 feet wide. Each creature in that line must make a DC 23 Dexterity saving throw, taking 88 (16d10) lightning damage on a failed save, or half as much damage on a successful one.

___Change Shape.___ Iymrith magically polymorphs into a female storm giant or back into her true form. She reverts to her true form if she dies. Any equipment she is wearing or carrying is absorbed or borne by the new form (the dragon's choice).

In storm giant form, Iymrith retains her alignment, hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence, Wisdom, and Charisma scores, as well as this action. Her statistics are otherwise replaced by those of the new form.

**Legendary** Actions

Iymrith can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The iymrith, ancient blue dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 24 Dexterity saving throw or take 16 (2d6 + 9) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
