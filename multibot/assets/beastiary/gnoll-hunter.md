"Gnoll Hunter";;;_size_: Medium humanoid (gnoll)
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Gnoll"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +3"
_speed_: "30 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "13 (leather armor)"
_stats_: | 14 (+2) | 14 (+2) | 12 (+1) | 8 (-1) | 12 (+1) | 8 (-1) |

___Rampage.___ When the gnoll reduces a creature to 0 hit points with a melee attack on its turn, the gnoll can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Multiattack.___ The gnoll makes two melee attacks with its spear or two ranged attacks with its longbow.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft, one target. Hit: 4 (1d4+2) piercing damage.

___Spear.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d6+2) piercing damage, or 6 (1d8+2) piercing damage when used with two hands to make a melee attack.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one target. Hit: 6 (1d8+2) piercing damage, and the target's speed is reduced by 10 feet until the end of its next turn.