"Swarm of Bats";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 60 ft."
_speed_: "0 ft., fly 30 ft."
_hit points_: "22 (5d8)"
_armor class_: "12"
_condition_immunities_: "charmed, frightened, grappled, paralyzed, petrified, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, slashing"
_stats_: | 5 (-3) | 15 (+2) | 10 (0) | 2 (-4) | 12 (+1) | 4 (-3) |

___Echolocation.___ The swarm can't use its blindsight while deafened.

___Keen Hearing.___ The swarm has advantage on Wisdom (Perception) checks that rely on hearing.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny bat. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Bites.___ Melee Weapon Attack: +4 to hit, reach 0 ft., one creature in the swarm's space. Hit: 5 (2d4) piercing damage, or 2 (1d4) piercing damage if the swarm has half of its hit points or fewer.