"Bomb Automaton";;;_size_: Small construct
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "understands one language known by its creator but can't speak"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "25 ft."
_hit points_: "13 (2d6 + 6)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 13 (+1) | 16 (+3) | 2 (-4) | 10 (+0) | 1 (-5) |

___Animated Explosive.___ When it takes fire damage, or as
an action on its turn, the automaton's fuse lights. At
the start of each of the automaton's turns, roll a d4.
On a result of 1, the automaton explodes in a 15-
foot radius fireball at the start of its next turn,
forcing each creature in the area to make a DC 12
Dexterity saving throw, taking 4 (1d8) fire damage
plus 4 (1d8) bludgeoning damage on a failed save, or
half as much on a successful one.

If the automaton is killed, the fuse remains lit,
potentially setting off the explosion after its death. If
another creature hits the automaton with an
unarmed strike attack, the creature can choose to
snuff out the fuse instead of dealing damage.

___Immutable Form.___ The automaton is immune to any spell or
effect that would alter its form.

___Siege Monster.___ The automaton deals double damage to
objects and structures.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 4 (1d6 + 1) piercing damage.
