"Gray Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Draconic"
_skills_: "Perception +4, Stealth +4"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +4, Con +3, Wis +2, Cha +3"
_damage_immunities_: "fire"
_speed_: "30 ft., fly 60 ft., swim 30 ft."
_hit points_: "44 (8d8 + 8)"
_armor class_: "16 (natural armor)"
_stats_: | 17 (+3) | 14 (+2) | 13 (+1) | 10 (+0) | 11 (+0) | 13 (+1) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8
(1d10 + 3) piercing damage plus 5 (1d10) fire damage.

___Steam Breath (Recharge 5–6).___ The dragon exhales steam in a 15 cone.
Creatures in the area must make a DC 11 Constitution saving throw. On
a failed saving throw, the creature takes 21 (6d6) fire damage, or half as
much damage on a successful saving throw. Being underwater doesn’t
grant resistance to this damage.
