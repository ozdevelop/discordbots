"Fate Eater";;;_size_: Medium aberration
_alignment_: neutral
_challenge_: "6 (2300 XP)"
_languages_: "telepathy 100 ft."
_skills_: "Arcana +7, History +7, Insight +6, Religion +7"
_senses_: "truesight 60 ft., passive Perception 13"
_saving_throws_: "Con +5"
_condition_immunities_: "charmed, unconscious"
_speed_: "40 ft., climb 40 ft."
_hit points_: "182 (28d8 + 56)"
_armor class_: "16 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 14 (+2) | 18 (+4) | 16 (+3) | 9 (-1) |

___Innate Spellcasting.___ The fate eater's innate spellcasting ability is Intelligence (spell save DC 15). It can innately cast the following spells, requiring no material components:

1/day each: blink, hallucinatory terrain

___Visionary Flesh.___ Eating the flesh of a fate eater requires a DC 15 Constitution saving throw. If successful, the eater gains a divination spell. If failed, the victim vomits blood and fails the next saving throw made in combat.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 26 (5d8 + 4) slashing damage plus 11 (2d10) necrotic damage.

___Spectral Bite.___ When a fate eater scores a critical hit against a target, it damages not only the creature but also the threads of its fate, changing the character's past or future. The target must roll 1d6 on the chart below for each critical hit that isn't negated by a successful DC 15 Charisma saving throw:

1- Seeing the Alternates: Suffers the effects of the confusion spell for 1d4 rounds

2- Untied from the Loom: Character's speed is randomized for four rounds. Roll 3d20 at the start of each of the character's turns to determine his or her speed in feet that turn

3- Shifting Memories: Permanently loses 2 from a random skill and gains 2 in a random untrained skill

4- Not So Fast: Loses the use of one class ability, chosen at random

5- Lost Potential: Loses 1 point from one randomly chosen ability score

6- Took the Lesser Path: The character's current hit point total becomes his or her hit point maximum

Effects 3-6 are permanent until the character makes a successful Charisma saving throw. The saving throw is repeated after every long rest, but the DC increases by 1 after every long rest, as the character becomes more entrenched in this new destiny. Otherwise, these new fates can be undone by nothing short of a wish spell or comparable magic.

