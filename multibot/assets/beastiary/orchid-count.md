"Orchid Count";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Elven, Sylvan"
_skills_: "Acrobatics +7, Athletics +4, Intimidation +5, Perception +6, Survival +3"
_senses_: "passive Perception 16"
_saving_throws_: "Dex +7, Con +6"
_speed_: "30 ft."
_hit points_: "97 (13d8 + 39)"
_armor class_: "16 (studded leather)"
_stats_: | 13 (+1) | 18 (+4) | 16 (+3) | 14 (+2) | 11 (+0) | 15 (+2) |

___Fey Step.___ The Count is unaffected by difficult
terrain.

___Fey Ancestry.___ Magic cannot put the Count
to sleep.

___Magic Resistance.___ The Count has advantage
on saving throws against spells and other
magical effects.

___Command Fey.___ As a member of the Court of
Arcadia, the Count can cast _dominate monster_ (DC 13) at
will on any fey creature or elf.

___Now You See Me….___ If the Count teleports next
to an enemy, he immediately makes two rapier
attacks with advantage against that enemy.

___Tactical Advantage.___ If the Count has advantage
on an attack roll, he deals an extra 7
(2d6) damage.

___Innate Spellcasting.___ The Count’s innate spellcasting
ability is Charisma (spell save DC 13). He can
innately cast the following spells, requiring no
material components:

* At will: _misty step, hunter’s mark_

* 3/day: _blur, mirror image_

**Actions**

___Multiattack.** The Count makes three attacks with
his rapier or longbow in any combination.

___Rapier.___ Melee Weapon Attack: +7 to hit, reach 5
ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Longbow.___ Ranged Weapon Attack: +7 to hit,
range 150/600 ft., one target. Hit: 11 (2d6 + 4)
piercing damage.

**Reactions**

___Now You Don’t… (Recharge 4–6).___ As a reaction,
if an enemy the Count can see makes a successful
melee or ranged attack roll against him, he
casts misty step.
