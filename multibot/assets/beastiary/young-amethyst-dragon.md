"Young Amethyst Dragon";;;_size_: Large dragon
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Insight +6, Perception +6"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 20"
_saving_throws_: "Dex +7, Int +7, Wis +6, Cha +7"
_damage_vulnerabilites_: "psychic"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "97 (15d10 + 15)"
_armor class_: "18 (natural armor)"
_stats_: | 16 (+3) | 16 (+3) | 13 (+1) | 16 (+3) | 14 (+2) | 17 (+3) |

___Feedback Aura.___ Each time the dragon takes
damage, all creatures within 30 feet must succeed
on a DC 15 Intelligence saving throw or else take
7 (2d6) psychic damage.

**Psionics**

___Charges:___ 15 | ___Recharge:___ 1d6 | ___Fracture:___ 18

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft.,
one target. Hit: 14 (2d10 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 10 (2d6 + 3) slashing damage.
