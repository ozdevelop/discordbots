"Sandwyrm";;;_size_: Large dragon
_alignment_: unaligned
_challenge_: "6 (2300 XP)"
_languages_: "--"
_skills_: "Perception +7, Stealth +4"
_senses_: "darkvision 60 ft., tremorsense 120 ft., passive Perception 17"
_speed_: "20 ft., burrow 40 ft."
_hit points_: "142 (15d10 + 60)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 18 (+4) | 5 (-3) | 13 (+1) | 8 (-1) |

___Spine Trap.___ When underground with its spines exposed, the sandwyrm can snap its spines closed on one Large, two Medium, or four Small or Tiny creatures above it. Each creature must make a successful DC 15 Dexterity saving throw or be restrained. A restrained creature can use its action to make a DC 15 Strength check to free itself or another restrained creature, ending the effect on a success. Creatures restrained by this trait move with the sandwyrm. The sandwyrm's stinger attack has advantage against creatures restrained by this trait.

**Actions**

___Multiattack.___ The sandwyrm makes one bite attack and one stinger attack. It can gore in place of the bite attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 12 (2d6 + 5) piercing damage.

___Gore.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 11 (1d12 + 5) piercing damage.

___Stinger.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one creature. Hit: 12 (2d6 + 5) piercing damage plus 24 (7d6) poison damage, or half as much poison damage with a successful DC 15 Constitution saving throw. If the poison damage reduces the target to 0 hit points, the target is stable but poisoned and paralyzed for 1 hour. Regaining hit points doesn't end the poisoning or paralysis.

