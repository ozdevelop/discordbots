"Vampiric Ixitxachitl";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Abyssal, Ixitxachitl"
_senses_: "darkvision 60 ft."
_speed_: "0 ft., swim 30 ft."
_hit points_: "44 (8d8+8)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 18 (+4) | 13 (+1) | 12 (+1) | 13 (+1) | 7 (-2) |

**Actions**

___Vampiric Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage. The target must succeed on a DC 11 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken, and the ixitxachitl regains hit points equal to that amount. The reduction lasts until the target finishes a long rest. The target dies if its hit point maximum is reduced to 0.

**Reactions**

___Barbed Tail.___ When a creature provokes an opportunity attack from the ixitxachitl, the ixitxachitl can make the following attack instead of using its bite.

Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) piercing damage.