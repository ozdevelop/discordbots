"Jötunn Dragon Hunter";;;_size_: Gargantuan giant
_alignment_: neutral evil
_challenge_: "12 (8,400 XP)"
_languages_: "Giant"
_skills_: "Athletics +11, Perception +6, Survival +10"
_senses_: "passive Perception 16"
_saving_throws_: "Str +11, Con +10, Wis +6, Cha +7"
_damage_immunities_: "cold"
_speed_: "50 ft."
_hit points_: "214 (13d20 + 78)"
_armor class_: "15 (natural armor)"
_stats_: | 25 (+7) | 14 (+2) | 23 (+6) | 9 (-1) | 15 (+2) | 16 (+3) |

___Blizzard Stalker.___ The jötunn's vision and hearing are
unimpeded by inclement weather such as wind,
snow, and fog, and it has advantage on all Dexterity
(Stealth) checks it makes in such conditions.

___Ice Walk.___ The jötunn can move across and climb icy
surfaces without needing to make an ability check.
Additionally, difficult terrain composed of ice or
snow doesn't cost it extra movement.

___Ice Weapons.___ Once on each of its turns, the jötunn
can form a weapon of solid ice in its empty hand (no
action required). This weapon can be either a javelin
or a lance, either of which counts as magical for the
purpose of overcoming resistances and immunity to
nonmagical attacks. The weapon lasts indefinitely, as
long as it remains in sub-freezing temperatures.

___Innate Spellcasting (1/Day).___ The jötunn can innately
cast _ice storm_, requiring no material components.
Its innate spellcasting ability is Charisma (spell save
DC 15).

**Actions**

___Multiattack.___ The jötunn makes two attacks with its
battleaxe.

___Battleaxe.___ Melee Weapon Attack: +11 to hit, reach
15 ft., one target. Hit: 26 (3d12 + 7) slashing
damage, or 33 (4d12 + 7) slashing damage if
wielded with two hands.

___Ice Lance.___ Melee Weapon Attack: +11 to hit, reach
15 ft., one target. Hit: 52 (10d8 + 7) piercing
damage plus 22 (5d8) cold damage.

___Ice Javelin.___ Ranged Weapon Attack: +11 to hit, range
80/320 ft., one target. Hit: 34 (6d8 + 7) piercing
damage plus 22 (5d8) cold damage.
