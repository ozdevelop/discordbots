"Kraken Priest";;;_size_: Medium humanoid (any race)
_alignment_: any evil alignment
_challenge_: "5 (1,800 XP)"
_languages_: "any two languages"
_skills_: "Perception +5"
_speed_: "30 ft., swim 30 ft."
_hit points_: "75 (10d8+30)"
_armor class_: "10"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 12 (+1) | 10 (0) | 16 (+3) | 10 (0) | 15 (+2) | 14 (+2) |

___Amphibious.___ The priest can breathe air and water.

___Innate Spellcasting.___ The priest's spellcasting ability is Wisdom (spell save DC 13, +5 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _command, create or destroy water_

* 3/day each: _control water, darkness, water breathing, water walk_

* 1/day each: _call lightning, Evard's black tentacles_

**Actions**

___Thunderous Touch.___ Melee Spell Attack: +5 to hit, reach 5 ft., one creature. Hit: 27 (5d10) thunder damage.

___Voice of the Kraken (Recharges after a Short or Long Rest).___ A kraken speaks through the priest with a thunderous voice audible within 300 feet. Creatures of the priest's choice that can hear the kraken's words (which are spoken in Abyssal, Infernal, or Primordial) must succeed on a DC 14 Charisma saving throw or be frightened for 1 minute. A frightened target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
