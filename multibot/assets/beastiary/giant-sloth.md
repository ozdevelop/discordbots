"Giant Sloth";;;_size_: Huge beast
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_skills_: "Athletics +9"
_senses_: "passive Perception 11"
_speed_: "30 ft., climb 30 ft., swim 30 ft."
_hit points_: "76 (8d12 + 24)"
_armor class_: "16 (natural armor)"
_stats_: | 22 (+6) | 10 (+0) | 17 (+3) | 2 (-4) | 12 (+1) | 5 (-3) |

___Keen Scent.___ The giant sloth has advantage on Wisdom (Perception)
checks based on scent.

**Actions**

___Multiattack.___ The giant sloth makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 15
(2d8 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 17
(2d10 + 6) slashing damage, and the target must make a DC 17 Strength
saving throw or be knocked prone.
