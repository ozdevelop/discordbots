"Dominion";;;_size_: Large celestial
_alignment_: any good alignment
_challenge_: "8 (3,900 XP)"
_languages_: "all, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 20"
_saving_throws_: "Dex +7, Cha +6"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: " charmed, exhaustion, frightened, poisoned"
_speed_: "40 ft., fly 40 ft."
_hit points_: "133 (14d10 + 56)"
_armor class_: "15 (natural armor)"
_stats_: | 16 (+3) | 19 (+4) | 18 (+4) | 17 (+3) | 18 (+4) | 17 (+3) |

___Angelic Weapons.___ The Dominion’s Ray of Salvation is magical. When the Dominion hits with it,
the ray deals an extra 9 (2d8) radiant damage
(included in the attack).

___Magic Resistance.___ The Dominion has advantage
on saving throws against spells and other magical
effects from evil characters and sources.

___Aura of Protection Against Evil.___ Evil creatures
have disadvantage on attack rolls against all
allies within 5 feet of the Dominion. Allies in this
area can’t be charmed, frightened, or possessed
by evil creatures. If an ally is already charmed,
frightened, or possessed by evil magic, the ally
has advantage on any new saving throw against
the relevant effect.

**Actions**

___Multiattack.___ The Dominion makes two attacks
with the Ray of Salvation.

___Ray of Salvation.___ Ranged Spell Attack: +7 to hit,
range 60 ft., one target. Hit: 10 (3d6) fire damage
and 9 (2d8) radiant damage. If the ray does
damage, any allies adjacent to the target gain 10
temporary hit points.
