"Sharkjaw Skeleton";;;_size_: Large undead
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "understands the languages of its creator but can't speak"
_skills_: "Perception +1, Stealth +2"
_senses_: "darkvision 60 ft., blindsense 30 ft., passive Perception 11"
_damage_vulnerabilities_: "bludgeoning"
_damage_immunities_: "cold, necrotic, poison"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft., swim 30 ft."
_hit points_: "45 (6d10 + 12)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 15 (+2) | 6 (-2) | 8 (-1) | 4 (-3) |

**Actions**

___Multiattack.___ The sharkjaw skeleton makes one bite attack and one claw attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage. If the target is a Large or smaller creature, it is grappled (escape DC 13). Until this grapple ends, the sharkjaw skeleton can bite only the grappled creature and has advantage on attack rolls to do so.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage.

