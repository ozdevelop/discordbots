"Broom of Animated Attack";;;_page_number_: 226
_size_: Small construct
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_damage_immunities_: "poison, psychic"
_speed_: "0 ft., fly 50 ft. (hover)"
_hit points_: "17 (5d6)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned, prone"
_stats_: | 10 (0) | 17 (+3) | 10 (0) | 1 (-5) | 5 (-3) | 1 (-5) |

___Constructed Nature.___ An animated object doesn't require air, food, drink, or sleep.

The magic that animates an object is dispelled when the construct drops to 0 hit points. An animated object reduced to 0 hit points becomes inanimate and is too damaged to be of much use or value to anyone.

___Antimagic Susceptibility.___ The broom is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the broom must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute.

___False Appearance.___ While the broom remains motionless and isn't flying, it is indistinguishable from a normal broom.

**Actions**

___Multiattack.___ The broom makes two melee attacks.

___Broomstick.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4+3) bludgeoning damage.

**Reactions**

___Animated Attack.___ If the broom is motionless and a creature grabs hold of it, the broom makes a Dexterity check contested by the creature's Strength check. If the broom wins the contest, it flies out of the creature's grasp and makes a melee attack against it with advantage on the attack roll.
