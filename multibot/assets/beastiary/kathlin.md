"Kathlin";;;_size_: Large monstrosity
_alignment_: neutral good
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "60 ft."
_hit points_: "34 (4d10 + 12)"
_armor class_: "12"
_stats_: | 18 (+4) | 15 (+2) | 17 (+3) | 5 (-3) | 13 (+1) | 8 (-1) |

___Brave.___ A kathlin has advantage on saving throws against being
frightened.

___Great Endurance.___ A kathlin can run at a gallop for up to 4 hours before
succumbing to exhaustion.

___Trampling Charge.___ If the kathlin moves at least 20 feet straight toward
a creature and then hits it with a hooves attack on the same turn, that target
must succeed on a DC 15 Strength saving throw or be knocked prone. If
the target is prone, the kathlin can make another attack with its hooves
against it as a bonus action.

**Actions**

___Multiattack.___ The kathlin makes one hooves attack and one bite attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d4 + 4) piercing damage.

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) bludgeoning damage.
