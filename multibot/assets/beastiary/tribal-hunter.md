"Tribal Hunter";;;_size_: Medium humanoid (any race)
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "any two languages"
_skills_: "Acrobatics +4, Athletics +4"
_senses_: "passive Perception 11"
_speed_: "30 ft."
_hit points_: "32 (5d8 + 10)"
_armor class_: "14 (studded leather)"
_stats_: | 14 (+2) | 15 (+2) | 15 (+2) | 10 (+0) | 12 (+1) | 8 (-1) |

___Natural Resilience.___ A life away from the pleasures of
society has made the hunter tougher than an
ordinary individual. It has advantage on Constitution
and Strength saving throws.

**Actions**

___Multiattack.___ The hunter makes two shortsword attacks.

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 5 (1d6 + 2) piercing damage plus 2 (1d4) poison
damage.

___Javelin.___ Ranged Weapon Attack: +4 to hit, range
30/120 ft., one target. Hit: 5 (1d6 + 2) piercing
damage plus 5 (2d4) poison damage.
