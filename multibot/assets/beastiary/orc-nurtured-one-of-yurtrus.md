"Orc Nurtured One of Yurtrus";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "30 (4d8+12)"
_armor class_: "9"
_stats_: | 15 (+2) | 8 (-1) | 16 (+3) | 7 (-2) | 11 (0) | 7 (-2) |

___Aggressive.___ As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.

___Corrupted Carrier.___ When the orc is reduced to 0 hit points, it explodes, and any creature within 10 feet of it must make a DC 13 Constitution saving throw. On a failed save, the creature takes 14 (4d6) poison damage and becomes poisoned. On a success, the creature takes half as much damage and isn't poisoned. A creature poisoned by this effect can repeat the save at the end of each of its turn, ending the effect on itself on a success. While poisoned by this effect, a creature can't regain hit points.

___Nurtured One of Yurtrus.___ The orc has advantage on saving throws against poison and disease.

**Actions**

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4+2) slashing damage plus 2 (1d4) necrotic damage.

___Corrupted Vengeance.___ The orc reduces itself to 0 hit points, triggering its Corrupted Carrier trait.