"Angatra";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "all languages it knew in life"
_skills_: "Perception +4, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "poison"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "50 ft."
_hit points_: "85 (10d8 + 40)"
_armor class_: "17 (natural armor)"
_stats_: | 14 (+2) | 20 (+5) | 18 (+4) | 8 (-1) | 12 (+1) | 15 (+2) |

___Agonizing Gaze.___ When a creature that can see the angatra's eyes starts its turn within 30 feet of the angatra, it must make a DC 13 Charisma saving throw if the angatra isn't incapacitated and can see the creature. On a failed saving throw, the creature has its pain threshold lowered, so that it becomes vulnerable to all damage types until the end of its next turn. Unless it's surprised, a creature can avoid the saving throw by averting its eyes at the start of its turn. A creature that averts its eyes can't see the angatra for one full round, when it chooses anew whether to avert its eyes again. If the creature looks at the angatra in the meantime, it must immediately make the save.

___Ancestral Wrath.___ The angatra immediately recognizes any individual that is descended from its tribe. It has advantage on attack rolls against such creatures, and those creatures have disadvantage on saving throws against the angatra's traits and attacks.

**Actions**

___Multiattack.___ The angatra makes two attacks with its claws.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 10 (2d4 + 5) piercing damage, and the creature must succeed on a DC 15 Constitution saving throw or be paralyzed by pain until the end of its next turn.

