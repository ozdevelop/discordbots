"Raven";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_skills_: "Perception +3"
_speed_: "10 ft., fly 50 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "12"
_stats_: | 2 (-4) | 14 (+2) | 8 (-1) | 2 (-4) | 12 (+1) | 6 (-2) |

___Mimicry.___ The raven can mimic simple sounds it has heard, such as a person whispering, a baby crying, or an animal chittering. A creature that hears the sounds can tell they are imitations with a successful DC 10 Wisdom (Insight) check.

**Actions**

___Beak.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 1 piercing damage.