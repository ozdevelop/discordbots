"Skin Bat";;;_size_: Small undead
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "10 ft., fly 40 ft."
_hit points_: "14 (4d6)"
_armor class_: "13 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 10 (+0) | 2 (-4) | 13 (+1) | 6 (-2) |

___Summon Bat Swarm.___ The high-frequency cries of a skin bat attract nearby mundane bats. When a skin bat faces danger, 0-3 (1d4-1) swarms of bats arrive within 1d6 rounds. These swarms are not under the skin bat's command, but they tend to reflexively attack whatever the skin bat is fighting.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage and the target must make a successful DC 10 Constitution saving throw or be paralyzed for 1d4 rounds. In addition, the skin bat attaches itself to the target. The skin bat can't bite a different creature while it's attached, and its bite attack automatically hits a creature the skin bat is attached to. Removing a skin bat requires a successful DC 11 Strength check and inflicts 5 (1d4 + 3) slashing damage to the creature the bat is being removed from. A successful save renders the target immune to skin bat poison for 24 hours.

