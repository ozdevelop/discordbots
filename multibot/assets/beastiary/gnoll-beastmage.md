"Gnoll Beastmage";;;_size_: Medium humanoid (gnoll)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Gnoll"
_skills_: "Animal Handling +4, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "30 ft."
_hit points_: "38 (7d8 + 7)"
_armor class_: "15 (hide armor, shield)"
_stats_: | 14 (+2) | 13 (+1) | 12 (+1) | 8 (-1) | 15 (+2) | 8 (-1) |

___Bestial Affinity.___ The gnoll can comprehend and verbally
communicate with beasts, and it has advantage on
Wisdom (Perception) checks that rely on smell.

___Pack Tactics.___ While the gnoll is in a beast form, it has
advantage on an attack roll against a creature if at least
one of the gnoll's allies is within 5 feet of the creature
and the ally isn't incapacitated.

___Rampage.___ When the gnoll reduces a creature to O hit
points with a melee attack on its turn, the gnoll can
take a bonus action to move up to half its speed and
make a bite attack.

___Shapechanger.___ The gnoll can use its action to polymorph
into a giant hyena, a hyena, a jackal, a rat, or a vulture, or
back into its true form. Its statistics, other than its size
and speed, are the same in each form. Any equipment it
is wearing or carrying is transformed along with it. It
reverts to its true form if it dies.

___Spellcasting.___ The gnoll is a 4th-level spellcaster. Its
spellcasting ability is Wisdom (spell save DC 12, +4 to
hit with spell attacks). It has the following druid spells
prepared.
* Cantrips (at-will): _druidcraft, guidance, resistance_
* lst level (4 slots): _animal friendship, cure wounds_
* 2nd level (3 slots): _animal messenger, locate animals or plants, pass without trace_

**Actions**

___Bite (Beak in Vulture Form).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (l d6 + 2) piercing damage.

___Spear (Gnoll Form Only).___ Melee or Ranged Weapon
Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one
target. Hit: 5 (l d6 + 2) piercing damage, or 6 (l d8 + 2)
piercing damage if used with two hands to make a
melee attack.

___Longbow (Gnoll Form Only).___ Ranged Weapon Attack: + 3
to hit, range l 50/600 ft., one target. Hit: 5 (l d8 + l)
piercing damage.

___Wild Cackle (1/Day).___ The gnoll laughs madly and swirls
with primal magic, inspiring each allied gnoll and beast
within 30 feet ofit that can hear it. Affected creatures have their speed increased by l 0 feet and have advantage on attack rolls made using natural weapons. The gnoll can then make one bite attack as a bonus action.
