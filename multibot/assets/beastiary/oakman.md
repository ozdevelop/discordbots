"Oakman";;;_size_: Small fey
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Sylvan"
_skills_: "Perception +4, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "20 ft., climb 30 ft."
_hit points_: "33 (6d6 + 12)"
_armor class_: "12 (16 with <i>barkskin</i>)"
_stats_: | 12 (+1) | 14 (+2) | 14 (+2) | 11 (+0) | 15 (+2) | 14 (+2) |

___Innate Spellcasting.___ The oakman’s spellcasting ability is Charisma
(spell save DC 12, +4 to hit with spell attacks). He can innately cast the
following spells, requiring no material components:

* At will: _druidcraft, shillelagh_

* 3/day each: _entangle, goodberry_

* 1/day each: _barkskin, pass without trace_

___Magic Resistance.___ The oakman has advantage on saving throws against
spells and other magical effects.

___Moss Cakes.___ Using his unique knowledge of plants and herbal mixtures, an
oakman can concoct unusual cakes from tree moss. A typical oakman has
3 (1d6) moss cakes of random type on his person. These moss cakes have
a variety of effects and must be eaten by the target creature to take effect.

1. ___Coloration.___ This moss cake is quite harmless when
eaten and does nothing more than cause the
target’s skin to become spotted. The spots can be of
just about any color, though most tend to be brown,
red, or blue. The spots last for 1 hour before fading.

2. ___Healing.___ This moss cake heals the target of 22 (5d8)
damage.

3. ___Lethargy.___ The target falls into a state of apathy
and becomes sluggish if it fails a DC 12 Constitution
saving throw. On a failed save, the target suffers
from three levels of exhaustion, which can be
removed normally.

4. ___Pain.___ Eating this moss cake wracks the target with
pain for 1 hour if it fails a DC 12 Constitution saving
throw. During this time, the target moves at half
speed, and has disadvantage on all attack rolls,
ability checks, and saving throws for 1 hour.

5. ___Poison.___ Eating this moss cake poisons the target. The
target must make a DC 12 Constitution saving throw.
On a failed saving throw, the target takes 7 (2d6)
poison damage and is poisoned for 1 hour. While
poisoned, the target must repeat the saving throw at
the beginning of each of its turns. On a failed saving
throw, the target spends its action retching and
vomiting.

6. ___Sleep.___ This moss cake puts the target to sleep for 1
hour if it fails a DC 12 Constitution saving throw.

___Tree Stride.___ Once on his turn, the oakman can use 10 feet of his
movement to step magically into one living tree within his reach and
emerge from a second living tree within 60 feet of the first tree, appearing
in an unoccupied space within 5 feet of the second tree. Both trees must
be Large or bigger.

**Actions**

___Club.___ Melee Weapon Attack: +3 to hit (+4 with shillelagh), reach
5 ft., one target. Hit: 3 (1d4 + 1) bludgeoning damage, or 6 (1d8 + 2)
bludgeoning damage.
