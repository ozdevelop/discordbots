"Peryton";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "understands Common and Elvish but can't speak"
_skills_: "Perception +5"
_speed_: "20 ft., fly 60 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "13 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 16 (+3) | 12 (+1) | 13 (+1) | 9 (-1) | 12 (+1) | 10 (0) |

___Dive Attack.___ If the peryton is flying and dives at least 30 ft. straight toward a target and then hits it with a melee weapon attack, the attack deals an extra 9 (2d8) damage to the target.

___Flyby.___ The peryton doesn't provoke an opportunity attack when it flies out of an enemy's reach.

___Keen Sight and Smell.___ The peryton has advantage on Wisdom (Perception) checks that rely on sight or smell.

**Actions**

___Multiattack.___ The peryton makes one gore attack and one talon attack.

___Gore.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Talons.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) piercing damage.