"Tsathogga, the Frog God";;;_size_: Gargantuan fiend (demon lord)
_alignment_: chaotic evil
_challenge_: "35 (330,000 XP)"
_languages_: "Aquan, Abyssal, Common, Giant, Infernal, Terran, telepathy 120 ft."
_skills_: "Arcana +14, Insight +14, Perception +24, Survival +14"
_senses_: "truesight 120 ft., passive Perception 34"
_saving_throws_: "Dex +12, Con +19, Wis +14, Cha +16"
_damage_immunities_: "acid, lightning, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "cold, fire"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "40 ft., swim 20 ft."
_hit points_: "546 (28d20 + 252)"
_armor class_: "20 (natural armor)"
_stats_: | 29 (+9) | 15 (+2) | 28 (+9) | 19 (+4) | 18 (+4) | 23 (+6) |

___Acidic Hide.___ A creature that touches Tsathogga or hits it with a melee
attack while within 10 feet of it takes 35 (10d6) acid damage. Any
nonmagical weapon made of metal or wood that hits Tsathogga instantly
dissolves before dealing damage.

___Amphibious.___ Tsathogga can breathe both water and air.

___Innate Spellcasting.___ Tsathogga’s innate spellcasting ability is Charisma
(spell save DC 24, +16 to hit with spell attacks).

* At will: _acid arrow _(5th level)_, bane, blindness/deafness, blight,
command, detect magic, fog cloud, grease, inflict wounds _(5th level)

* 7/day each: _antilife shell, circle of death, cloudkill, contagion,
counterspell, dispel magic, dominate beast, dominate person, earthquake_

* 3/day each: _geas, harm_

* 1/day each: _control weather, power word kill, storm of vengeance, wish_

___Legendary Resistance (3/day).___ If Tsathogga fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ Tsathogga has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ Tsathogga’s weapon attacks are magical.

___Unholy Aura.___ An unholy aura surrounds Tsathogga out to a radius of 40
feet. A creature who enters or begins their turn in the area must make a DC
20 Wisdom saving throw. On a failed saving throw, the target is frightened
for 1 minute. While frightened, they are paralyzed. A frightened target can
repeat the saving throw at the end of each of its turns, ending the effect
on a success.

**Actions**

___Multiattack.___ Tsathogga uses its Blasphemous Croak ability, then uses
its tongue attack. If there is a creature within range, it can then use its bite
attack and two claw attacks.

___Bite.___ Up to three targets of Huge size or smaller within 10 feet of
Tsathogga must make a DC 27 Dexterity saving throw. On a successful
saving throw, it takes 44 (10d6 + 9) piercing damage, plus 35 (10d6)
acid damage, and is grappled. On a failed saving throw, the target is
swallowed. While swallowed, the creature is blinded and restrained, it has
total cover against attacks and other effects outside of Tsathogga, and it
takes 56 (16d6) acid damage at the start of each of Tsathogga’s turns. If
Tsathogga takes 50 damage or more on a single turn from a creature inside
it, Tsathogga must succeed on a DC 25 Constitution saving throw at the
end of that turn or regurgitate all swallowed creatures, which fall prone in
a space with 10 feet of Tsathogga. If Tsathogga dies, a swallowed creature
is no longer restrained by it and can escape from the corpse using all of its
movement, exiting prone.

___Claw.___ Melee Weapon Attack: +19 to hit, reach 10 ft., one target. Hit: 40
(9d6 + 9) slashing damage plus 35 (10d6) acid damage.

___Tongue.___ Tsathogga chooses one creature that it can see within 40 feet
of it. That target must make a DC 28 Dexterity saving throw. On a failed
saving throw, the target takes 37 (8d6 + 9) bludgeoning damage plus 35
(10d6) acid damage and is grappled. Tsathogga can use a bonus action to
pull a creature grappled by it up to 40 feet towards it.

___Blasphemous Croak.___ Tsathogga utters a blasphemous croak audible to
all creatures within 300 feet of it. Those that can hear it must make a DC
24 Wisdom saving throw. On a failed saving throw, the target takes 28
(8d6) necrotic damage and 28 (8d6) thunder damage and is stunned for 1
minute. On a successful saving throw, the target takes half damage and is
not stunned. A stunned creature can repeat the saving throw at the end of
each of its turns, ending the effect on a success.

___Summon (1/day).___ Tsathogga summons 2d6 hezrous, 2d4 greruors, 1
nalfeshnee, or 1 balor. The summoned demon appears in an unoccupied
space within 60 feet of Tsathogga, and can’t summon other demons. It
remains for 1 minute, until it or Tsathogga is slain, or until Tsathogga
takes an action to dismiss it.

**Legendary** Actions

Tsathogga can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. Tsathogga regains spent legendary
actions at the start of its turn.

___Retract Tongue.___ Tsathogga can pull one creature grappled by its
tongue attack 40 feet towards it.

___Quake.___ Tsathogga causes the area around it to violently shake in a
radius of 100 feet. Creatures in the area must make a DC 24 Dexterity
saving throw or fall prone. Until the beginning of Tsathogga’s next turn,
the ground continues shaking, and creatures must repeat the saving throw
to rise from prone.

___Seeping Darkness (Costs 2 Actions).___ Magical darkness fills the area
surrounding Tsathogga out to a distance of 50 feet. The darkness spreads
around corners, douses any nonmagical lights, and any light-producing
spell of 6th level or lower is immediately dispelled. The magical darkness
remains for 1 hour, or until Tsathogga uses an action to dismiss it.

**Lair** Actions

On initiative count 20 (losing initiative ties), Tsathogga takes a lair
action to cause one of the following magical effects.

___Acid Swamp.___ Tsathogga chooses an area of swamp that it can see within
120 feet of it. The area can be no larger than a 30-foot cube. Creatures in
the area must make a DC 24 Strength saving throw. On a failed saving
throw, the target takes 21 (6d6) acid damage and is grappled (escape DC
24). On a successful saving throw, the target takes half damage and is not
grappled. On the next initiative count 20, the target takes an additional 21
(6d6) acid damage if it is still grappled by this lair action.

___Insect Swarms.___ Tsathogga causes 7 (2d6) insect swarms to converge
on an area of Tsathogga’s choosing that it can see.

___Swamp Mist.___ Tsathogga causes swamp mist to rise. This acts as if
Tsathogga had cast the fog cloud spell as a 9th level spell. Tsathogga does
not need to maintain concentration on this effect.
