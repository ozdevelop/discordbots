"Aetherspawn Rift Walker";;;_size_: Medium elemental
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "Primordial"
_senses_: "passive Perception 12"
_damage_resistances_: "bludgeoning, piercing, slashing damage from nonmagical weapons; see Elemental Attunement"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "30 ft., fly 30 ft. (hover)"
_hit points_: "65 (10d8 + 20)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 17 (+3) | 15 (+2) | 17 (+3) | 15 (+2) | 10 (+0) |

___Aether Eruption.___ When the rift walker dies, it explodes
in a burst of elemental energy. Each creature within 5
feet of it must make a DC 13 Dexterity saving throw,
taking 14 (4d6) damage of the type corresponding to
the rift walker's Elemental Attunement on a failed save,
or half as much damage on a successful one.

___Elemental Attunement.___ The rift walker is infused with
either arcane, ice, fire, or lightning energy. The rift
walker's attacks deal extra damage based upon this
element (included in the attack) and it has resistances
to the corresponding element. In addition, each attack
with a specific element causes an additional effect.

* Arcane – bonus force damage and target must succeed on a DC 13 Wisdom saving throw or be silenced until the rift walker's next turn; resistance to force damage
* Ice - bonus cold damage and target's speed halved until the rift walker's next turn; resistance to cold damage
* Fire – bonus fire damage and if the target is a creature or a flammable object, it ignites. Until a creature takes an action to douse the fire, the target takes 3 (1d6) fire damage at the start of each of its turns; resistance to fire damage
* Lightning – bonus lightning damage and target can't take reactions until the rift walker's next turn; resistance to lightning damage

___Overcharging Strikes.___ If the spellblade hits the same
target with both of its aether blade attacks in a single
turn, the target detonates with elemental energy. That
creature takes an additional 7 (2d6) damage of a type
corresponding to the guardian's Elemental Attunement.

**Actions**

___Multiattack.___ The rift walker makes two melee attacks.

___Aether Strike.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage
plus 7 (2d6) damage of the type corresponding to the
rift walker's Elemental Attunement.

___Elemental Shards.___ Ranged Weapon Attack: +5 to hit,
range 30/90 ft., one target. Hit: 5 (1d4 + 3) piercing
damage plus 7 (2d6) damage of the type
corresponding to the rift walker's Elemental
Attunement.

___Aether Step (Recharge 4-6).___ The rift walker teleports up
to 60 feet to an unoccupied space it can see. It then
makes two aether strike attacks, the first of which is
made with advantage.
