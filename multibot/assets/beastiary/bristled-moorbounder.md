"Bristled Moorbounder";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "70 ft."
_hit points_: "52 (7d10 + 14)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 14 (+2) | 2 (-4) | 13 (+1) | 5 (-3) |

___Bladed Hide.___ At the start of each of its turns, the moorbounder deals 5 (2d4) piercing damage to any creature grappling it.

___Standing Leap.___ The moorbounder’s long jump is up to 40 feet and its high jump is up to 20 feet, with or without a running start.

**Actions**

___Multiattack.___ The moorbounder makes two attacks: one with its blades and one with its claws.

___Blades.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 14 (4d4 + 4) slashing damage.
