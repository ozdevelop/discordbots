"Ancient Cloud Dragon";;;_size_: Gargantuan dragon
_alignment_: neutral
_challenge_: "22 (41,000 XP)"
_languages_: "Auran, Aquan, Common, Draconic, Giant, Sylvan"
_skills_: "Arcana +12, History +12, Insight +11, Nature +12, Perception +18"
_senses_: "blindsight 120 ft., darkvision 120 ft., passive Perception 28"
_saving_throws_: "Dex +8, Con +14, Wis +11, Cha +11"
_damage_immunities_: "cold, lightning"
_speed_: "40 ft., fly 80 ft."
_hit points_: "367 (21d20 + 147)"
_armor class_: "19 (natural armor)"
_stats_: | 27 (+8) | 13 (+1) | 25 (+7) | 21 (+5) | 18 (+4) | 19 (+4) |

___Legendary Resistance (3/day).___ If the dragon fails a saving throw,
it can choose to succeed instead.

___Windborn.___ The dragon cannot be moved or knocked prone by
magical or nonmagical winds of any kind.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes
three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 20 ft., one target.
Hit: 19 (2d10 + 8) piercing damage plus 10 (3d6) cold damage.

___Claw.___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target.
Hit: 22 (4d6 + 8) slashing damage.

___Tail.___ Melee Weapon Attack: +15 to hit, reach 20 ft., one target.
Hit: 17 (2d8 + 8) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon’s choice that is
within 120 feet of the dragon and aware of it must succeed on a DC
19 Wisdom saving throw or become frightened for 1 minute. A creature
can repeat the saving throw at the end of each of its turns, ending the
effect on itself on a success. If a creature’s saving throw is successful
or the effect ends for it, the creature is immune to the dragon’s Frightful
Presence for the next 24 hours.

___Breath Weapons (Recharge 5–6).___ The dragon uses one of the following
breath weapons.

* _Cloud Breath._ The dragon exhales an icy blast in a 90-foot cone that
spreads around corners. Each creature in that area must make a DC 22
Constitution saving throw, taking 72 (16d8) cold damage on a failed
saving throw, or half as much damage on a successful one.

* _Wind Breath._ The dragon exhales gale-force winds in a 90-foot line that
is 10-feet wide. Creatures in this area must make a DC 22 Strength saving
throw, taking 36 (8d8) bludgeoning damage on a failed saving throw and
are pushed 90 feet directly away from the dragon. On a successful saving
throw, the creature takes half damage and is not pushed. A creature who is
pushed and strikes a solid surface takes 1d6 bludgeoning damage per 10
feet traveled. Nonmagical flames are completely extinguished, and magical
flames created by a spell of 5th level or lower are immediately dispelled.

___Cloud Form.___ The cloud dragon polymorphs into a Gargantuan cloud of
mist, or back into its true form. While in mist form, the dragon can’t take
any actions, speak, or manipulate objects. It is weightless, has a flying
speed of 20 feet, can hover, and can enter a hostile creature’s space and
stop there. In addition, if air can pass through a space, the mist can do so
without squeezing, and it can’t pass through water. It has advantage on
Strength, Dexterity, and Constitution saving throws, it is immune to all
nonmagical damage, and still benefits from its Windborn feature.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 23 Dexterity
saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the dragon takes a lair
action to cause one of the following effects; the dragon can’t use the same
effect two rounds in a row:

___Sphere of Fog.___ The cloud dragon causes a 60-foot radius sphere of fog
centered on a point anywhere in its lair. The sphere spreads around corners
and its area is heavily obscured. The fog remains for 1 hour, or until the
dragon dismisses it (no action required), and it cannot be dispersed by
wind. On the next initiative count 20, the dragon can move the sphere up
to 30 feet in any direction.

___Wind Gust.___ The cloud dragon causes a burst of strong wind 60 feet
long and 10 feet wide from it in a direction of the dragon’s choice. The
strong wind lasts until the next initiative count 20. Each creature other
than the cloud dragon that starts its turn or enters the area must succeed
on a DC 19 Strength saving throw or be pushed 15 feet away from it. In
addition, any creature in the area must spend 2 feet of movement for every
1-foot it moves when moving closer to the dragon, and the wind disperses
gas or vapor of the dragon’s choice, and it extinguishes candles, torches,
and similar unprotected flames in the area.

___Freezing Air.___ The dragon causes a 20-foot sphere of air to drop to
below freezing temperatures for 24 hours. Creatures in that area that start
their turn or enter the area must make a DC 19 Constitution saving throw.
On a failed saving throw, the target takes 14 (4d6) cold damage, or half as
much damage on a successful one. The area affected by the spell is also
covered in frost, leaving it difficult terrain for 24 hours.

**Regional** Effects

The region containing the ancient cloud dragon’s lair is changed by its
presence, which creates one or more of the following effects:

___Wind and Fog.___ Strong winds whip and curl around the dragon’s lair,
but fog clouds still linger regardless, cloaking the land within 6 miles of
the dragon’s lair, making it lightly obscured.

___Cold Snaps.___ Frigid cold snaps suddenly appear in areas within 6 miles
of the dragon’s lair, dropping the temperature to below freezing in minutes.

___Thin Air.___ The air thins at ground level and above, as if the inhabitants
stood above altitudes of 10,000 feet or higher, in areas within 6 miles of
the dragon’s lair.

If the dragon dies, these effects fade over 1d10 days.
