"Giant Snapping Turtle";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "30 ft., swim 40 ft."
_hit points_: "75 (10d10 +20)"
_armor class_: "17 (natural armor), 12 while prone"
_stats_: | 19 (+4) | 10 (0) | 14 (+2) | 2 (-4) | 12 (+1) | 5 (-3) |

___Amphibious.___ The turtle can breathe air and water.

___Stable.___ Whenever an effect knocks the turtle prone, it can make a DC 10 Constitution saving throw to avoid being knocked prone. A prone turtle is upside down. To stand up, it must succeed on a DC 10 Dexterity check on its turn and then use all its movement for that turn.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 18 (4d6 +4) slashing damage.