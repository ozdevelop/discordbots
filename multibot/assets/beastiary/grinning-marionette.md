"Grinning Marionette";;;_size_: Tiny construct
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "Infernal, telepathy 120 ft."
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "10 (4d6 - 4)"
_armor class_: "12"
_stats_: | 7 (-2) | 14 (+2) | 8 (-1) | 8 (-1) | 8 (-1) | 12 (+1) |

___Bound Servitude.___ All marionettes serve a Grim
Puppeteer. This master can use its action to see
through the marionette’s eyes and the marionette can
communicate with its master from anywhere. The
marionette is forced to execute the orders of its master
to the best of its abilities. The marionette becomes a
mundane doll if its master is killed.

___False Appearance.___ While the marionette remains
motionless, it is indistinguishable from an ordinary doll.

**Actions**

___Draining Touch.___ Melee Weapon Attack: +4 to hit, reach
5ft., one target. Hit: 4 (1d4 + 2) bludgeoning damage
and the target must succeed on a DC 11 Constitution
saving throw or take an additional 2 (1d4) necrotic
damage.
