"Warrior Priestess";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Insight +5, Religion +3"
_senses_: "passive Perception 13"
_saving_throws_: "Wis +5, Cha +3"
_speed_: "30 ft."
_hit points_: "26 (4d8 + 8)"
_armor class_: "18 (chainmail, shield)"
_stats_: | 14 (+2) | 8 (-1) | 15 (+2) | 12 (+1) | 16 (+3) | 12 (+1) |

___Spellcasting.___ The priestess is a 3th-level spellcaster.
Its spellcasting ability is Wisdom (spell save DC 13, +5 to hit with spell attacks). The priestess has the
following cleric spells prepared:

* Cantrips (at will): _guidance, sacred flame_

* 1st level (4 slots): _bless, cure wounds, divine favor, shield of faith_

* 2nd level (2 slots): _magic weapon, spiritual weapon_

___Guided Strike (1/Short Rest).___ When the priestess
makes an attack roll, it can gain a +10 bonus to the
roll.

**Actions**

___Flail.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 6 (1d8 + 2) bludgeoning damage.
