"Intellect Devourer";;;_size_: Tiny aberration
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "understands Deep Speech but can't speak, telepathy 60 ft."
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_skills_: "Perception +2, Stealth +4"
_speed_: "40 ft."
_hit points_: "21 (6d4+6)"
_armor class_: "12"
_condition_immunities_: "blinded"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 6 (-2) | 14 (+2) | 13 (+1) | 12 (+1) | 11 (0) | 10 (0) |

___Detect Sentience.___ The intellect devourer can sense the presence and location of any creature within 300 feet of it that has an Intelligence of 3 or higher, regardless of interposing barriers, unless the creature is protected by a mind blank spell.

**Actions**

___Multiattack.___ The intellect devourer makes one attack with its claws and uses Devour Intellect.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) slashing damage.

___Devour Intellect.___ The intellect devourer targets one creature it can see within 10 feet of it that has a brain. The target must succeed on a DC 12 Intelligence saving throw against this magic or take 11 (2d10) psychic damage. Also on a failure, roll 3d6: If the total equals or exceeds the target's Intelligence score, that score is reduced to 0. The target is stunned until it regains at least one point of Intelligence.

___Body Thief.___ The intellect devourer initiates an Intelligence contest with an incapacitated humanoid within 5 feet of it that isn't protected by protection from evil and good. If it wins the contest, the intellect devourer magically consumes the target's brain, teleports into the target's skull, and takes control of the target's body. While inside a creature, the intellect devourer has total cover against attacks and other effects originating outside its host. The intellect devourer retains its Intelligence, Wisdom, and Charisma scores, as well as its understanding of Deep Speech, its telepathy, and its traits. It otherwise adopts the target's statistics. It knows everything the creature knew, including spells and languages.

If the host body dies, the intellect devourer must leave it. A protection from evil and good spell cast on the body drives the intellect devourer out. The intellect devourer is also forced out if the target regains its devoured brain by means of a wish. By spending 5 feet of its movement, the intellect devourer can voluntarily leave the body, teleporting to the nearest unoccupied space within 5 feet of it. The body then dies, unless its brain is restored within 1 round.