"Madam Eva";;;_page_number_: 233
_size_: Medium humanoid (human)
_alignment_: chaotic neutral
_challenge_: "10 (5,900 XP)"
_languages_: "Abyssal, Common, Elvish, Infernal"
_skills_: "Arcana +7, Deception +8, Insight +13, Intimidation +8, Perception +9, Religion +7"
_saving_throws_: "Con +5"
_speed_: "20 ft."
_hit points_: "88 (16d8+16)"
_armor class_: "10"
_stats_: | 8 (-1) | 11 (0) | 12 (+1) | 17 (+3) | 20 (+5) | 18 (+4) |

___Spellcasting.___ Madam Eva is a 16th-level spellcaster. Her spellcasting ability is Wisdom (spell save DC 17, +9 to hit with spell attacks). Madam Eva has the following cleric spells prepared:

* Cantrips (at will): _light, mending, sacred flame, thaumaturgy_

* 1st level (4 slots): _bane, command, detect evil and good, protection from evil and good_

* 2nd level (3 slots): _lesser restoration, protection from poison, spiritual weapon_

* 3rd level (3 slots): _create food and water, speak with dead, spirit guardians_

* 4th level (3 slots): _divination, freedom of movement, guardians of faith_

* 5th level (2 slots): _greater restoration, raise dead_

* 6th level (1 slot): _find the path, harm, true seeing_

* 7th level (1 slot): _fire storm, regenerate_

* 8th level (1 slot): _earthquake_

**Actions**

___Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 2 (1d4) piercing damage.

___Curse (Recharges after a Long Rest).___ Madam Eva targets one creature that she can see within 30 feet of her. The target must succeed on a DC 17 Wisdom saving throw or be cursed. While cursed, the target is blinded and deafened. The curse lasts until ended with a greater restoration spell, a remove curse spell, or similar magic. When the curse ends, Madam Eva takes 5d6 psychic damage.

___Evil Eye (Recharges after a Short or Long Rest).___ Madam Eva targets one creature that she can see within 10 feet of her and casts one of the following spells on the target (save DC 17), requiring neither somatic nor material components to do so: animal friendship, charm person, or hold person. If the target succeeds on the initial saving throw, Madam Eva is blinded until the end of her next turn. Once a target succeeds on a saving throw against this effect, it is immune to the Evil Eye power of all Vistani for 24 hours.
