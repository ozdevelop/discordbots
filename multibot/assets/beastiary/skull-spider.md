"Skull Spider";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "20 ft., climb 10 ft."
_hit points_: "2 (1d4)"
_armor class_: "17 (natural armor)"
_stats_: | 6 (-2) | 20 (+5) | 10 (+0) | 2 (-4) | 10 (+0) | 2 (-4) |

___Pack Tactics.___ The spider has advantage on an attack roll against a
creature if at least one of the spider’s allies is within 5 feet of the creature
and the ally isn’t incapacitated.

**Actions**

___Sting.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target.
Hit: 7 (1d4 + 5) piercing damage, and the target must succeed on a
DC 10 Constitution saving throw or become poisoned for 1 hour.
