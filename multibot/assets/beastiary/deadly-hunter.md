"Deadly Hunter";;;_size_: Medium humanoid
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "any four languages"
_skills_: "Animal Handling +6, Perception +6, Stealth +7, Survival +6"
_senses_: "passive Perception 16"
_saving_throws_: "Dex +7, Wis +6"
_speed_: "30 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "16 (studded leather)"
_stats_: | 14 (+2) | 18 (+4) | 16 (+3) | 11 (+0) | 17 (+3) | 7 (-2) |

___Vital Strikes.___ When the hunter hits a creature with a
weapon attack, the creature takes an extra 4 (1d8)
damage if it is below its hit point maximum.

**Actions**

___Multiattack.___ The hunter makes two attacks with its
shortsword or two with its longbow.

___Shortsword.___ Melee Weapon Attack: +7 to hit, reach
5ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Longbow.___ Ranged Weapon Attack: +9 to hit, range
150/600 ft., one target. Hit: 8 (1d8 + 4) piercing
damage.

___Rain of Arrows (Recharge 5-6).___ The hunter fires a
magical arrow at a point within 150 feet. When the
arrow reaches its destination, it explodes into
hundreds of individual arrows that rain down and
coat the battlefield in a 30 foot cone directly
beneath the initial arrow’s location. Each creature in
this area must make a DC 14 Dexterity saving
throw, taking 27 (6d8) piercing damage on a failed
save, or half as much damage on a successful one.

___Hunt Them Down (1/Day).___ All creatures within 300
feet of the hunter come under the effects of the
_hunter’s mark_ spell for the next 24 hours. After the
hunter uses this ability, it makes a longbow attack
against each enemy it can see within 150 feet,
making a separate attack roll for each attack.

**Bonus** Actions

___One With Nature.___ While outdoors, the hunter can
take the Hide action as a bonus action on each of
its turns.

___Strike From The Shadows.___ When the hunter starts its
turn with no one aware of its presence, if the
hunter takes the Attack action this turn it may make
one additional attack with its longbow or
shortsword as a bonus action.
