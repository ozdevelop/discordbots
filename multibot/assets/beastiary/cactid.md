"Cactid";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "understands Sylvan, but can't speak"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 10"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "bludgeoning, piercing"
_condition_immunities_: "blinded, deafened"
_speed_: "5 ft."
_hit points_: "76 (8d10 + 32)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 8 (-1) | 18 (+4) | 7 (-2) | 10 (+0) | 9 (-1) |

___Hail of Needles (1/Day).___ When reduced below 10 hp (even below 0 hp), the cactid releases a hail of needles as a reaction. All creatures within 15 feet take 21 (6d6) piercing damage, or half damage with a successful DC 14 Dexterity saving throw. 

**Actions**

___Multiattack.___ The cactid makes two attacks with its tendrils and uses Reel.

___Tendril.___ Melee Weapon Attack: +5 to hit, reach 15 ft., one creature. Hit: 10 (2d6 + 3) bludgeoning damage plus 3 (1d6) piercing damage, and a Medium or smaller target is grappled (escape DC 13). Until this grapple ends, the target is restrained. If the target is neither undead nor a construct, the cactid drains the target's body fluids; at the start of each of the target's turns, the target must make a DC 13 Constitution saving throw. On a failed save, the creature's hit point maximum is reduced by 3 (1d6). If a creature's hit point maximum is reduced to 0 by this effect, the creature dies. This reduction lasts until the creature finishes a long rest and drinks abundant water or until it receives a greater restoration spell or comparable magic. The cactid has two tendrils, each of which can grapple one target at a time.

___Reel.___ Each creature grappled by the cactid is pulled up to 5 feet straight toward the cactid.

