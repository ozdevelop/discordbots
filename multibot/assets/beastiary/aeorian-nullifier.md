"Aeorian Nullifier";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "12 (8,400 XP)"
_languages_: "understands Draconic but can’t speak"
_skills_: "Perception +6, Survival +6"
_senses_: "darkvision 120 ft., passive Perception 16"
_saving_throws_: "Wis +6, Cha +8"
_damage_immunities_: "radiant, necrotic"
_speed_: "40 ft."
_hit points_: "180 (19d10 + 76)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 18 (+4) | 7 (-2) | 14 (+2) | 18 (+4) |

___Horrid Gnashing.___ The nullifier’s mouths gnash incoherently while it can see any enemies. Each creature that starts its turn within 20 feet of the nullifier and can hear it must make a DC 16 Wisdom saving throw. Unless the save succeeds, the creature rolls a d8 to determine what it does during the current turn:

* 1–4: The creature is stunned until the end of the turn.

* 5–6: The creature is frightened until the end of the turn and uses its movement to get as far as possible from the nullifier.

* 7–8: The creature doesn’t move, and it uses its action to make one melee attack against a random creature (other than itself) if one is within reach. It otherwise does nothing.

___Innate Spellcasting.___ The nullifier’s innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _counterspell _(see “Reactions” below)_, detect magic, dispel magic, see invisibility_

* 1/day: _antimagic field_

___Magic Resistance.___ The nullifier has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The nullifier makes three attacks: one with its bites and two with its claws.

___Bites.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 17 (2d12 + 4) piercing damage plus 11 (2d10) force damage.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 11 (2d10) force damage, and the target is grappled (escape DC 16) if it’s a creature. The nullifier has two claws, each of which can grapple one creature.

**Reactions**

___Counterspell.___ The nullifier attempts to interrupt a creature that it can see within 60 feet in the process of casting a spell. If the creature is casting a spell of 3rd level or lower, its spell fails and has no effect. If it is casting a spell of 4th level or higher, the nullifier makes a Charisma check with a DC equal to 10 + the spell’s level. On a success, the creature’s spell fails and has no effect.
