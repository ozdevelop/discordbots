"Elder Tempest";;;_page_number_: 200
_size_: Gargantuan elemental
_alignment_: neutral
_challenge_: "23 (50,000 XP)"
_languages_: "-"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_immunities_: "lightning, poison, thunder"
_saving_throws_: "Wis +12, Cha +11"
_speed_: "0 ft., fly 120 ft. (hover)"
_hit points_: "264  (16d20 + 96)"
_armor class_: "19"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 23 (+6) | 28 (+9) | 23 (+6) | 2 (-4) | 21 (+5) | 18 (+4) |

___Air Form.___ The tempest can enter a hostile creature's space and stop there. It can move through a space as narrow as 1 inch wide without squeezing.

___Flyby.___ The tempest doesn't provoke opportunity attacks when it flies out of an enemy's reach.

___Legendary Resistance (3/Day).___ If the tempest fails a saving throw, it can choose to succeed instead.

___Living Storm.___ The tempest is always at the center of a storm 1d6 + 4 miles in diameter. Heavy precipitation in the form of either rain or snow falls there, causing the area to be lightly obscured. Heavy rain also extinguishes open flames and imposes disadvantage on Wisdom (Perception) checks that rely on hearing.
In addition, strong winds swirl in the area covered by the storm. The winds impose disadvantage on ranged attack rolls. The winds extinguish open flames and disperse fog.

___Siege Monster.___ The tempest deals double damage to objects and structures.

**Actions**

___Multiattack___ The tempest makes two attacks with its thunderous slam.

___Thunderous Slam___ Melee Weapon Attack: +16 to hit, reach 20 ft., one target. Hit: 23 (4d6 + 9) thunder damage.

___Lightning Storm (Recharge 6)___ All other creatures within 120 feet of the tempest must each make a DC 20 Dexterity saving throw, taking 27 (6d8) lightning damage on a failed save, or half as much damage on a successful one. If a target's saving throw fails by 5 or more, the creature is also stunned until the end of its next turn.

**Legendary** Actions

The tempest can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The tempest regains spent legendary actions at the start of its turn.

___Move___ The tempest moves up to its speed.

___Lightning Strike (Costs 2 Actions)___ The tempest can cause a bolt of lightning to strike a point on the ground anywhere under its storm. Each creature within 5 feet of that point must make a DC 20 Dexterity saving throw, taking 16 (3d10) lightning damage on a failed save, or half as much damage on a successful one.

___Screaming Gale (Costs 3 Actions)___ The tempest releases a blast of thunder and wind in a line that is 1 mile long and 20 feet wide. Objects in that area take 22 (4d10) thunder damage. Each creature there must succeed on a DC 21 Dexterity saving throw or take 22 (4d10) thunder damage and be flung up to 60 feet in a direction away from the line. If a thrown target collides with an immovable object, such as a wall or floor, the target takes 3 (1d6) bludgeoning damage for every 10 feet it was thrown before impact. If the target would collide with another creature instead, that other creature must succeed on a DC 19 Dexterity saving throw or take the same damage and be knocked prone.