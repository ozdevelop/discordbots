"Scheznyki";;;_size_: Small fey
_alignment_: chaotic evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Darakhul, Elvish"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Str +10, Con +10"
_condition_immunities_: "unconscious"
_speed_: "20 ft., climb 15 ft."
_hit points_: "153 (18d6 + 72)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 15 (+2) | 18 (+4) | 15 (+2) | 16 (+3) | 16 (+3) |

___Innate Spellcasting.___ The scheznyki's innate spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: dancing lights, darkness, detect evil and good, faerie fire, invisibility*, fly*, mage hand, ray of frost (*only when wearing a vanisher hat)

5/day each: magic missile, ray of enfeeblement, silent image

3/day each: locate object (radius 3,000 ft to locate a vanisher hat), hideous laughter, web

1/day each: dispel magic, dominate person, hold person

___Magic Resistance.___ The scheznyki has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The scheznyki makes four war pick attacks or two hand crossbow attacks.

___Heavy Pick.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Hand Crossbow.___ Ranged Weapon Attack: +5 to hit, range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

