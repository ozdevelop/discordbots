"Ancient Emerald Dragon";;;_size_: Gargantuan dragon
_alignment_: chaotic neutral
_challenge_: "21 (33,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +14, Insight +10, Perception +10, Religion +14"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 27"
_saving_throws_: "Dex +14, Int +14, Wis +10, Cha +13"
_damage_vulnerabilities_: "psychic"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, slashing, and piercing from nonmagical attacks"
_speed_: "40 ft., fly 80 ft. (hover), swim 40 ft."
_hit points_: "252 (24d20)"
_armor class_: "22 (natural armor)"
_stats_: | 21 (+5) | 24 (+7) | 10 (+0) | 24 (+7) | 16 (+3) | 22 (+6) |

___Legendary Resistance (3/Day).___ If the dragon fails
a saving throw, it can choose to succeed instead.

___Interference Aura.___ Enemies within 30 feet must
make a DC 22 Intelligence saving throw every
round to maintain spells that require concentration.

**Psionics**

___Charges:___ 24 | ___Recharge:___ 1d10 | ___Fracture:___ 27

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 15 ft.,
one target. Hit: 16 (2d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 10 ft.,
one target. Hit: 12 (2d6 + 5) slashing damage.

___Tail.___ Melee Weapon Attack: +12 to hit; reach 20 ft.,
one target. Hit: 14 (2d8 + 5) bludgeoning damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The dragon regains spent
legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception)
Check.

___Psionics.___ The dragon uses a psionic ability.

___Psionic Shift (Costs 2 Actions).___ The dragon
releases a wave of telekinetic energy from its mind.
Every creature within 15 feet must make a DC 24
Intelligence saving throw or take 14 (2d6 + 7) force
damage and be knocked prone. The dragon then
can move up to half its movement speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the
dragon takes a lair action to cause one of the
following effects. The dragon can’t use the same
effect two rounds in a row.

* The dragon summons a water elemental.

* The dragon casts control water (save DC 22).
It does not require concentration to maintain.

* The dragon casts thunderwave with DC 22.
The area is a 30-foot cube, and the damage
is 4d8.

**Regional** Effects

Intelligent creatures who sleep within 12 miles of an
emerald dragon’s lair dream of unending depths,
tidal waves, starfish and crustaceans, or a calm sea
under solid white sky with constellations under
the waters.
