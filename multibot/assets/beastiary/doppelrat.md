"Doppelrat";;;_size_: Tiny monstrosity
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +5"
_speed_: "15 ft., climb 15 ft., swim 15 ft."
_hit points_: "22 (5d4 + 10)"
_armor class_: "13"
_stats_: | 2 (-4) | 17 (+3) | 14 (+2) | 2 (-4) | 13 (+1) | 2 (-4) |

___Keen Smell.___ The doppelrat has advantage on Wisdom (Perception) checks that rely on smell.

___Running Doppelrats.___ The hardest part of this monster is the sheer volume of attacks they generate. To model this, run them in groups of 4 for attack purposes and have those groups all use the same +5 to attack and do 1d4 damage. That way you need not roll 20 times for all of them, and you reduce the number of rolls required by a factor of 4.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 piercing damage.

___Arcane Doubling (recharges after 10 minutes).___ A doppelrat under duress creates clones of itself at the beginning of its turn. Each round for 4 rounds, the number of live doppelrats quadruples but never exceeds 20. For example, when the doppelrat triggers arcane doubling, 1 rat becomes 4; at the start of the rat's next turn, those 4 become 16; and at the start of the rat's third turn, those 16 become 20, the maximum allowed. If one of the duplicates was destroyed between the original doppelrat's 1st and 2nd turns, then the surviving 3 would become 12, and so on. Each duplicate appears in the same space as any other rat, can either move or take an action the round it appears, and has 4 hit points and AC 13. Any surviving duplicates perish 1 minute (10 rounds) after the first ones were created. If the original doppelrat dies, its clones stop duplicating but the preexisting clones remain until their time expires. A creature can identify the original doppelrat from its duplicates as an action by making a successful DC 15 Intelligence (Nature) or Wisdom (Perception) check.

___Doppeling Disease.___ At the end of a dopplerat encounter, every creature bitten by a doppelrat or its duplicates must succeed on a DC 12 Constitution saving throw or contract the degenerate cloning disease. During each long rest, the diseased creature grows and sloughs off a stillborn clone. The doppeling process leaves the diseased creature incapacitated for 1 hour, unable to move and barely able to speak (spellcasting is impossible in this state). When the incapacitation wears off, the creature makes a DC 12 Constitution saving throw; it recovers from the disease when it makes its second successful save. Humanoid clones created by the disease cannot be brought to life in any manner.

