"Emerald Order Cult Leader";;;_size_: Medium Humanoid
_alignment_: lawful neutral or evil
_challenge_: "8 (3900 XP)"
_languages_: "any three languages"
_skills_: "Arcana +5, Deception +5, History +5, Religion +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Int +5, Wis +8, Cha +5"
_damage_resistances_: "cold, fire, lightning"
_speed_: "30 ft."
_hit points_: "117 (18d8+36)"
_armor class_: "14 (breastplate)"
_stats_: | 10 (+0) | 10 (+0) | 14 (+2) | 15 (+2) | 20 (+5) | 15 (+2) |

___Key of Prophecy.___ The Emerald Order cult leader can always act in a surprise round, but if he fails to notice a foe, he is still considered surprised until he takes an action. He receives a +3 bonus on initiative checks.

___Innate Spellcasting.___ The Emerald Order cult leader's innate spellcasting ability is Wisdom (spell save DC 16). He can innately cast the following spells, requiring no material components:

2/day each: detect thoughts, dimension door, haste, slow

1/day each: suggestion, teleport

___Spellcasting.___ The Emerald Order cult leader is a 10th-level spellcaster. His spellcasting ability is Wisdom (spell save DC 16, +8 to hit with spell attacks). The cult leader has the following cleric spells prepared:

Cantrips (at will): guidance, light, sacred flame, spare the dying, thaumaturgy

1st level (4 slots): cure wounds, identify, guiding bolt

2nd level (3 slots): lesser restoration, silence, spiritual weapon

3rd level (3 slots): dispel magic, mass healing word, spirit guardians

4th level (3 slots): banishment, death ward, guardian of faith

5th level (2 slots): flame strike

**Actions**

___Multiattack.___ The Emerald Order cult leader makes one melee attack and casts a cantrip.

___Mace.___ Melee Weapon Attack: +3 to hit, reach 5 ft, one target. Hit: 3 (1d6) bludgeoning damage.

**Reactions**

___Esoteric Vengeance.___ As a reaction when struck by a melee attack, the emerald order cult leader can expend a spell slot to do 10 (3d6) necrotic damage to the attacker. If the emerald order cult leader expends a spell slot of 2nd level or higher, the damage increases by 1d6 for each level above 1st.

