"Demonblood Orc Berserker";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Orc"
_skills_: "Athletics +6, Intimidation +2"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_speed_: "30 ft."
_hit points_: "45 (5d12 + 15)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 16 (+3) | 7 (-2) | 12 (+1) | 10 (+0) |

___Aggressive.___ As a bonus action, the ore can move up
to its speed toward a hostile creature that it can
see.

___Demonblood Rage.___ The orc is in a constant state of
rage due to its demonic blood. It has resistance to
bludgeoning, piercing, and slashing damage from
nonmagical weapons and deals 2 additional damage
with each attack (included in the attacks).

**Actions**

___Multiattack.___ The orc makes two attacks with its
greataxe or its spear.

___Battleaxe.___ Melee Weapon Attack: +6 to hit, reach 5
ft., one target. Hit: 12 (1d12 + 6) slashing damage.

___Spear.___ Melee or Ranged Weapon Attack: +6 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 9
(1d6 + 6) piercing damage, or 10 (1d8 + 6)
piercing damage if used with two hands to make a
melee attack.

___Furious Strikes (1/Day).___ The orc doubles its
Demonblood Rage bonus damage to 4 this turn,
then makes three attacks with its greataxe. Until the
end of the orc’s next turn, all attacks made against
it have advantage.
