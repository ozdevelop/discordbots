"Death Knight";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "17 (18,000 XP)"
_languages_: "Abyssal, Common"
_senses_: "darkvision 120 ft."
_damage_immunities_: "necrotic, poison"
_saving_throws_: "Dex +6, Wis +9, Cha +10"
_speed_: "30 ft."
_hit points_: "180 (19d8+95)"
_armor class_: "20 (plate, shield)"
_condition_immunities_: "exhaustion, frightened, poisoned"
_stats_: | 20 (+5) | 11 (0) | 20 (+5) | 12 (+1) | 16 (+3) | 18 (+4) |

___Undead Nature.___ A death knight doesn't require air, food, drink, or sleep.

___Magic Resistance.___ The death knight has advantage on saving throws against spells and other magical effects.

___Marshal Undead.___ Unless the death knight is incapacitated, it and undead creatures of its choice within 60 feet of it have advantage on saving throws against features that turn undead.

___Spellcasting.___ The death knight is a 19th-level spell caster. Its spellcasting ability is Charisma (spell save DC 18, +10 to hit with spell attacks). It has the following paladin spells prepared:

* 1st level (4 slots): _command, compelled duel, searing smite_

* 2nd level (3 slots): _hold person, magic weapon_

* 3rd level (3 slots): _dispel magic, elemental weapon_

* 4th level (3 slots): _banishment, staggering smite_

* 5th level (2 slots): _destructive wave _(necrotic)

**Actions**

___Multiattack.___ The death knight makes three longsword attacks.

___Longsword.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) slashing damage, or 10 (1d10 + 5) slashing damage if used with two hands, plus 18 (4d8) necrotic damage.

___Hellfire Orb (1/day).___ The death knight hurls a magical ball of fire that explodes at a point it can see within 120 feet of it. Each creature in a 20-foot-radius sphere centered on that point must make a DC 18 Dexterity saving throw. The sphere spreads around corners. A creature takes 35 (10d6) fire damage and 35 (10d6) necrotic damage on a failed save, or half as much damage on a successful one.

**Reactions**

___Parry.___ The death knight adds 6 to its AC against one melee attack that would hit it. To do so, the death knight must see the attacker and be wielding a melee weapon.
