"Wood Golem";;;_size_: Medium construct
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "cold, lightning, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "45 (7d8 + 14)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 15 (+2) | 7 (-2) | 11 (+0) | 5 (-3) |

___Alarm.___ Whenever a creature other than its creator comes within 60
feet of the golem, it releases an audible alarm sound which can
be heard out to a range of 300 feet.

___Immutable Form.___ The golem is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against
spells and other magical effects.

___Magic Weapon.___ The golem’s weapon attacks are magical.

**Actions**

___Multiattack.___ The wood golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12
(2d8 + 3) bludgeoning damage.
