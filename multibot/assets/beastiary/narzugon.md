"Narzugon";;;_page_number_: 167
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 17"
_skills_: "Perception +7"
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +5, Con +8, Cha +9"
_speed_: "30 ft."
_hit points_: "112  (15d8 + 45)"
_armor class_: "20 (plate armor, shield)"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "acid, cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 20 (+5) | 10 (0) | 17 (+3) | 16 (+3) | 14 (+2) | 19 (+4) |

___Diabolical Sense.___ The narzugon has advantage on Wisdom (Perception) checks made to perceive good-aligned creatures.

___Infernal Tack.___ The narzugon wears spurs that are part of infernal tack, which allow it to summon its nightmare companion.

___Magic Resistance.___ The narzugon has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The narzugon uses its Infernal Command or Terrifying Command. It also makes three hellfire lance attacks.

___Hellfire Lance___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 11 (1d12 + 5) piercing damage plus 16 (3d10) fire damage. If this damage kills a creature, the creature's soul rises from the River Styx as a lemure in Avernus in 1d4 hours.
If the creature isn't revived before then, only a wish spell or killing the lemure and casting true resurrection on the creature's original body can restore it to life. Constructs and devils are immune to this effect.

___Infernal Command___ Each ally of the narzugon within 60 feet of it can't be charmed or frightened until the end of the narzugon's next turn.

___Terrifying Command___ Each creature that isn't a fiend within 60 feet of the narzugon that can hear it must succeed on a DC 17 Charisma saving throw or become frightened of it for 1 minute.
A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. A creature that makes a successful saving throw is immune to this narzugon's Terrifying Command for 24 hours.

___Healing (1/Day)___ The narzugon, or one creature it touches, regains up to 100 hit points.