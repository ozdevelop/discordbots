"Ychen Bannog";;;_size_: Gargantuan beast
_alignment_: unaligned
_challenge_: "11 (7200 XP)"
_languages_: "--"
_senses_: ", passive Perception 11"
_damage_resistances_: "bludgeoning"
_condition_immunities_: "exhaustion"
_speed_: "50 ft."
_hit points_: "231 (14d20 + 84)"
_armor class_: "17 (natural armor)"
_stats_: | 28 (+9) | 10 (+0) | 23 (+6) | 3 (-4) | 12 (+1) | 10 (+0) |

___Ever-Sharp Horns.___ The ychen bannog deals triple damage dice when it scores a critical hit with a gore attack.

___Overrun.___ When the ychen bannog takes the Dash action, it can move through the space of a Large or smaller creature, treating the creature's space as difficult terrain. As it moves through the creature's space, the ychen bannog can make a stomp attack as a bonus action.

___Peaceful Creature.___ The ychen bannog abhors combat and flees from it if possible. If unable to flee, the ychen bannog can attack a foe or obstacle to clear a path to safety. As an action, a driver or handler mounted on the ychen bannog or adjacent to it can make a DC 16 Wisdom (Animal Handling) check. On a success, the ychen bannog moves and attacks as directed by the driver. On a failure, the beast flees. The driver or handler must have proficiency in Animal Handling to attempt this check.

**Actions**

___Multiattack.___ The ychen bannog makes one gore attack and one stomp attack.

___Gore.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 27 (4d8 + 9) piercing damage.

___Stomp.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 31 (4d10 + 9) bludgeoning damage. If the target is a creature, it must succeed on a DC 21 Strength saving throw or be knocked prone.

___Destroying Bellow (Recharge 5-6).___ The ychen bannog delivers a fearsome bellow that can be heard up to ten miles away. Structures and unattended objects in a 60-foot cone take 55 (10d10) thunder damage. Creatures in the cone take 27 (5d10) thunder damage and are deafened for 1 hour, or take half damage and aren't deafened with a successful DC 18 Constitution saving throw.

