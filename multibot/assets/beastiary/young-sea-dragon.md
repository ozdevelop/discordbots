"Young Sea Dragon";;;_size_: Large dragon
_alignment_: neutral evil
_challenge_: "9 (5000 XP)"
_languages_: "Common, Draconic"
_skills_: "Perception +9, Stealth +4"
_senses_: "blindsight 30 ft. darkvision 120 ft., passive Perception 19"
_saving_throws_: "Dex +4, Con +8, Wis +5, Cha +7"
_damage_immunities_: "cold"
_speed_: "40 ft., fly 80 ft., swim 50 ft."
_hit points_: "152 (16d10 + 64)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 10 (+0) | 19 (+4) | 15 (+2) | 13 (+1) | 17 (+3) |

___Amphibious.___ The dragon can breathe air and water.

___Siege Monster.___ The dragon deals double damage to objects

___and structures.___ 

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) piercing damage plus 5 (1d10) cold damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

___Tidal Breath (Recharge 5-6).___ The dragon exhales a crushing wave of frigid seawater in a 30-foot cone. Each creature in that area must make a DC 16 Dexterity saving throw. On a failure, the target takes 27 (5d10) bludgeoning damage and 27 (5d10) cold damage, and is pushed 30 feet away from the dragon and knocked prone. On a successful save the creature takes half as much damage and isn't pushed or knocked prone.

