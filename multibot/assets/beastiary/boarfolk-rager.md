"Boarfolk Rager";;;_size_: Large humanoid (boarfolk)
_alignment_: lawful neutral
_challenge_: "9 (5,000 XP)"
_languages_: "Boarfolk, Common"
_skills_: "Athletics +9, Intimidation +3"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Str +9, Dex +6, Con +8"
_damage_resistances_: "bludgeoning, piercing, and slashing damage"
_speed_: "40 ft."
_hit points_: "123 (13d10 + 52)"
_armor class_: "16"
_stats_: | 20 (+5) | 15 (+2) | 18 (+4) | 9 (-1) | 11 (+0) | 10 (+0) |

___Brute.___ A melee weapon deals one extra die of its damage when the
rager hits with it (included in the attack).

___Created Race.___ The boarfolk rager has advantage on saving throws
against spells and effects that would alter its form.

___Keen Scent.___ A boarfolk rager has advantage on Wisdom (Perception)
checks based on scent.

___Reckless.___ At the start of its turn, the rager can gain advantage on all
melee weapon attack rolls during that turn, but attack rolls against it have
advantage until the start of its next turn.

**Actions**

___Multiattack.___ The boarfolk rager makes three melee attacks.

___Tusks.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12
(2d6 + 5) piercing damage.

___Greatclub.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit:
14 (2d8 + 5) bludgeoning damage.
