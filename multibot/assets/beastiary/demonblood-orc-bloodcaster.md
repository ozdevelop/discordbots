"Demonblood Orc Bloodcaster";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft., passive Perception 9"
_speed_: "30 ft."
_hit points_: "67 (9d10 + 18)"
_armor class_: "11"
_stats_: | 12 (+1) | 12 (+1) | 14 (+2) | 10 (+0) | 9 (-1) | 18 (+4) |

___Blood Magic.___ Whenever the orc deals damage to an
enemy with a spell, it gains 5 temporary hit points.
Additionally, the orc can use its bonus action to pay
1d10 hit points to make the next spell it casts act
as if it were cast at one spellslot higher.

___Uncontainable Power.___ When the orc dies, it
explodes violently. Each creature within 5 feet of it
must make a DC 13 Dexterity saving throw, taking
18 (4d8) necrotic damage on a failed save, or half
as much damage on a successful one.

___Spellcasting.___ The orc is an 7th-level spellcaster. Its
spellcasting ability is Charisma (spell save DC 14,
+6 to hit with spell attacks). The orc has the
following Sorcerer spells prepared:

* Cantrips (at will): _acid splash, blade ward, ray of frost, prestidigitation_

* 1st level (4 slots): _burning hands, mage armor, shield_

* 2nd level (3 slots): _hold person, spider climb_

* 3rd level (3 slots): _counterspell, fireball_

* 4th level (2 slots): _wall of fire_

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +3 to hit, reach
5ft., one target. Hit: 4 (1d6 + 1) bludgeoning
damage.
