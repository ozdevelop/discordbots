"Duergar Screamer";;;_page_number_: 190
_size_: Medium construct
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "understands Dwarvish but can't speak"
_senses_: "darkvision 60 ft., passive Perception 7"
_damage_immunities_: "poison"
_speed_: "20 ft."
_hit points_: "38  (7d8 + 7)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 18 (+4) | 7 (-1) | 12 (+1) | 5 (-2) | 5 (-2) | 5 (-2) |

___Engine of Pain.___ Once per turn, a creature that attacks the screamer can target the duergar trapped in it. The attacker has disadvantage on the attack roll. On a hit, the attack deals an extra 11 (2d10) damage to the screamer, and the screamer can respond by using its Multiattack with its reaction.

**Actions**

___Multiattack___ The screamer makes one drill attack and uses its Sonic Scream.

___Drill___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (1d12 + 4) piercing damage.

___Sonic Scream___ The screamer emits destructive energy in a 15-foot cube. Each creature in that area must succeed on a DC 11 Strength saving throw or take 7 (2d6) thunder damage and be knocked prone.