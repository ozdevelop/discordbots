"Ia'affrat";;;_size_: Large swarm
_alignment_: lawful evil
_challenge_: "15 (13000 XP)"
_languages_: "Common, Draconic, Infernal, Primordial"
_skills_: "Arcana +10, Deception +11, Insight +9, Perception +9, Persuasion +11"
_senses_: "blindsight 10 ft., darkvision 120 ft., passive Perception 19"
_saving_throws_: "Dex +10, Con +8, Wis +9, Cha +11"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire, poison; bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, poisoned, restrained, stunned"
_speed_: "5 ft., fly 40 ft. (hover)"
_hit points_: "170 (20d10 + 60)"
_armor class_: "17 (natural armor)"
_stats_: | 3 (-4) | 21 (+5) | 16 (+3) | 20 (+5) | 18 (+4) | 23 (+6) |

___Elemental Swarm.___ Ia'Affrat can occupy another creature's space

and vice versa, and the swarm can move through any opening

___large enough for a Tiny insect.___ 

___Magic Resistance.___ Ia'Affrat has advantage on saving throws against spells and other magical effects

___Innate Spellcasting.___ Ia'Affrat's spellcasting ability is Charisma (spell save DC 19, +11 to hit with spell attacks). Ia'Affrat can innately cast the following spells, requiring no material components:

At will: fire bolt, poison spray

3/day each: burning hands, invisibility, ray of enfeeblement, ray of sickness

1/day each: bestow curse, contagion, harm, insect plague, fireball

___Inhabit.___ Ia'Affrat can enter the body of an incapacitated or dead creature by crawling into its mouth and other orifices. Inhabiting requires 1 minute, and the victim must be Small, Medium, or Large. If Ia'Affrat inhabits a dead body, it can animate it and control its movements, effectively becoming a zombie for as long as it remains inside. Ia'Affrat can abandon the body as an action. Attacks against the host deal half damage to Ia'Affrat as well, but Ia'Affrat's resistances and immunities still apply against this damage. In a living victim, Ia'Affrat can control the victim's movement and actions as if using dominate monster (save DC 19) on the victim. Ia'Affrat can consume a living victim; the target takes 5 (2d4) necrotic damage per hour while Ia'Affrat inhabits its body, and Ia'Affrat regains hit points equal to the damage dealt. When inhabiting a body, Ia'Affrat can choose to have any spell it casts with a range of self, target the inhabited body rather than itself. The skin of a creature inhabited by Ia'Affrat crawls with the forms of the insects inside. Ia'Affrat can hide this telltale sign with a Charisma (Deception) check against a viewer's passive Insight. A greater restoration spell or comparable magic forces Ia'Affrat to abandon the host.

___Smoke Shroud.___ Ia'Affrat is shrouded in a 5-foot-radius cloud of dark smoke. This area is lightly obscured to creatures other than Ia'Affrat. Any creature that needs to breathe that begins its turn in the area must make a successful DC 16 Constitution saving throw or be stunned until the end of its turn.

**Actions**

___Bites.___ Melee Weapon Attack: +10 to hit, reach 0 ft., one creature in the swarm's space. Hit: 21 (6d6) piercing damage plus 14 (4d6) fire damage plus 14 (4d6) poison damage, or 10 (3d6) piercing damage plus 7 (2d6) fire damage plus 7 (2d6) poison damage if Ia'Affrat has half of its hit points or fewer. The target must succeed on a DC 16 Constitution saving throw or be poisoned for 1 minute. A poisoned creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Smoke Jump.___ Ia'Affrat can travel instantly to a space in sight where there's smoke.

___Whirlwind (Recharge 4-6).___ Each creature in Ia'Affrat's space must make a DC 18 Strength saving throw. Each creature that fails takes 28 (8d6) bludgeoning damage plus 14 (4d6) fire damage plus 14 (4d6) poison damage and is flung up 20 feet away from Ia'Affrat in a random direction and knocked prone. If the saving throw is successful, the target takes half the bludgeoning damage and isn't flung away or knocked prone. If a thrown target strikes an object, such as a wall or floor, the target takes 3 (1d6) bludgeoning damage for every 10 feet it traveled. If the target collides with another creature, that creature must succeed on a DC 18 Dexterity saving throw or take the same damage and be knocked prone.

