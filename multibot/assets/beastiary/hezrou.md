"Hezrou";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_damage_immunities_: "poison"
_saving_throws_: "Str +7, Con +8, Wis +4"
_speed_: "30 ft."
_hit points_: "136 (13d10+65)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 19 (+4) | 17 (+3) | 20 (+5) | 5 (-3) | 12 (+1) | 13 (+1) |

___Magic Resistance.___ The hezrou has advantage on saving throws against spells and other magical effects.

___Stench.___ Any creature that starts its turn within 10 feet of the hezrou must succeed on a DC 14 Constitution saving throw or be poisoned until the start of its next turn. On a successful saving throw, the creature is immune to the hezrou's stench for 24 hours.

**Actions**

___Multiattack.___ The hezrou makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Variant: Summon Demon (1/Day).___ The demon chooses what to summon and attempts a magical summoning.

A hezrou has a 30 percent chance of summoning 2d6 dretches or one hezrou.

A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.