"Gibbering Abomination";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "16 (15,000 XP)"
_languages_: "Deep Speech"
_skills_: "Perception +8, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Con +12, Cha +9"
_damage_resistances_: "lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "prone"
_speed_: "10 ft., climb 10 ft."
_hit points_: "200 (16d10 + 112)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 16 (+3) | 24 (+7) | 10 (+0) | 6 (-2) | 19 (+4) |

___Innate Spellcasting.___ The gibbering abomination’s spellcasting ability
is Charisma (spell save DC 17, +9 to hit with spell attacks). It can cast
the following spells at will, requiring no material components:

* _blur, color spray, confusion, dispel magic, fear, ray of frost, ray of enfeeblement, telekinesis_.

___Amorphous.___ The gibbering abomination can move through a space as
narrow as 1 inch wide without squeezing.

___Arcane Frenzy.___ The gibbering abomination can use its innate
spellcasting more frequently than other creatures. During a single turn,
the gibbering abomination can use its action and bonus action to innately
cast a spell. The gibbering abomination cannot cast the same spell twice
during its turn.

___Deathless.___ Unless the gibbering abomination is slain by a disintegrate
spell or its remains are completely incinerated, the abomination returns to
life with 1 hit point in 1 hour.

___Hyper-Awareness.___ The gibbering abomination can’t be surprised.

___Pain Immunity.___ The gibbering abomination is immune to any effect or
condition caused as a result of extreme pain or agony. This ability does not
protect it against damage the abomination would suffer.

___Regeneration.___ The abomination regains 20 hit points at the start of its
turn if it has at least 1 hit point. If the abomination has taken cold damage,
this trait doesn’t function at the start of the abomination’s next turn.

**Actions**

___Multiattack.___ The gibbering abomination makes up to three bite attacks
and uses Blood Drain on grappled creatures.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 22
(4d8 + 4) piercing damage, and the target is grappled (escape DC 14),
and it is restrained until this grapple ends. While grappling the target,
the abomination has advantage on attack rolls against it and can’t use
this attack against other targets. The abomination can grapple up to three
creatures.

___Blood Drain.___ Each creature that is not a construct or undead, and that
is grappled by the abomination must make a DC 18 Constitution saving
throw. On a failed save, the target takes 10 (3d6) necrotic damage, its hit
point maximum is reduced by an amount equal to the necrotic damage
taken, and the abomination regains hit points equal to that amount. It dies
if this effect reduces its hit point maximum to 0. This reduction to the
target’s hit point maximum lasts until the target finishes a long rest.

___Disruptive Cacophony (Recharge 5–6).___ The abomination emits a
horrible quasi-arcane chanting that can disrupt spellcasters within 90 feet
of it. Any creature attempting to cast a spell that can hear the abomination’s
Disruptive Cacophony must succeed on a DC 18 Constitution saving throw
or the spell fails. The disruption lasts until the start of the abomination’s
next turn.
