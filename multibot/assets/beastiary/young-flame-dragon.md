"Young Flame Dragon";;;_size_: Large dragon
_alignment_: chaotic evil
_challenge_: "9 (5000 XP)"
_languages_: "Common, Draconic, Ignan, Giant, Infernal, Orc"
_skills_: "Deception +8, Insight +5, Perception +9, Persuasion +8, Stealth +6"
_senses_: ", passive Perception 19"
_saving_throws_: "Dex +6, Con +8, Wis +5, Cha +8"
_damage_immunities_: "fire"
_speed_: "40 ft., climb 40 ft., fly 80 ft."
_hit points_: "161 (17d10 + 68)"
_armor class_: "18 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 19 (+4) | 15 (+2) | 13 (+1) | 18 (+4) |

___Fire Incarnate.___ All fire damage dealt by the dragon ignores fire resistance but not fire immunity.

**Actions**

___Multiattack.___ The dragon makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 13 (2d10 + 2) piercing damage plus 3 (1d6) fire damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage.

___Fire Breath (Recharge 5-6).___ The dragon exhales fire in a 30-foot cone. Each creature in that area takes 56 (16d6) fire damage, or half damage with a successful DC 16 Dexterity saving throw.

