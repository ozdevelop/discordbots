"Planetar";;;_size_: Large celestial
_alignment_: lawful good
_challenge_: "16 (15,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft."
_skills_: "Perception +11"
_saving_throws_: "Con +12, Wis +11, Cha +12"
_speed_: "40 ft., fly 120 ft."
_hit points_: "200 (16d10+112)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened"
_damage_resistances_: "radiant, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 24 (+7) | 20 (+5) | 24 (+7) | 19 (+4) | 22 (+6) | 25 (+7) |

___Angelic Weapons.___ The planetar's weapon attacks are magical. When the planetar hits with any weapon, the weapon deals an extra 5d8 radiant damage (included in the attack).

___Divine Awareness.___ The planetar knows if it hears a lie.

___Innate Spellcasting.___ The planetar's spellcasting ability is Charisma (spell save DC 20). The planetar can innately cast the following spells, requiring no material components:

* At will: _detect evil and good, invisibility _(self only)

* 3/day each: _blade barrier, dispel evil and good, flame strike, raise dead_

* 1/day each: _commune, control weather, insect plague_

___Magic Resistance.___ The planetar has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The planetar makes two melee attacks.

___Greatsword.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 21 (4d6 + 7) slashing damage plus 22 (5d8) radiant damage.

___Healing Touch (4/Day).___ The planetar touches another creature. The target magically regains 30 (6d8 + 3) hit points and is freed from any curse, disease, poison, blindness, or deafness.
