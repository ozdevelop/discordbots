"Battleforce Angel";;;_size_: Medium celestial
_alignment_: lawful good
_challenge_: "5 (1800 XP)"
_languages_: "all"
_saving_throws_: "Wis +6, Cha +7"
_skills_: "Medicine +4, Religion +2"
_damage_resistances_: "fire, radiant"
_condition_immunities_: "charmed, exhaustion, frightened"
_senses_: "darkvision 120 ft., truesight 120 ft., passive Perception 16"
_speed_: "30 ft., fly 90 ft."
_hit points_: "66 (12d8 + 12)"
_armor class_: "18 (plate)"
_stats_: | 16 (+3) | 12 (+1) | 13 (+1) | 11 (+0) | 17 (+3) | 18 (+4) |

___Flyby.___ The angel doesn’t provoke an opportunity attack when it flies out of an enemy’s reach.

___Magic Resistance.___ The angel has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The angel makes two melee attacks. It also uses Battlefield Inspiration.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1d10 + 3) slashing damage if used with two hands, plus 18 (4d8) radiant damage. If the target is within 5 feet of any of the angel’s allies, the target takes an extra 2 (1d4) radiant damage.

___Battlefield Inspiration.___ The angel chooses up to three creatures it can see within 30 feet of it. Until the end of the angel’s next turn, each target can add a d4 to its attack rolls and saving throws.
