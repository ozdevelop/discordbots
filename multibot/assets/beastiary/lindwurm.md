"Lindwurm";;;_size_: Large dragon
_alignment_: neutral evil
_challenge_: "5 (1800 XP)"
_languages_: "--"
_skills_: "Acrobatics +8, Athletics +8, Perception +4, Stealth +9"
_senses_: "darkvision 60 ft., tremorsense 120 ft. on ice, passive Perception 14"
_saving_throws_: "Str +7, Dex +8, Con +6"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "cold"
_condition_immunities_: "paralyzed, prone, unconscious"
_speed_: "40 ft., swim 20 ft."
_hit points_: "136 (16d10 + 48)"
_armor class_: "15"
_stats_: | 18 (+4) | 20 (+5) | 16 (+3) | 6 (-2) | 12 (+1) | 8 (-1) |

___Lindwurm Fever.___ A creature infected with this disease by a lindwurm's bite gains one level of exhaustion an hour after being infected. The creature must make a DC 14 Constitution saving throw after each long rest. On a failure, the creature gains one level of exhaustion and recovers no hit dice from the long rest. On a success, the creature recovers from one level of exhaustion and regains hit dice normally. If the infected creature reduces itself to zero exhaustion by making successful saving throws, the disease is cured.

___Skittering Skater.___ Lindwurms suffer no penalties from difficult terrain on ice and are immune to the effects of the grease spell.

___Snake Belly.___ When lying with its sensitive stomach on the ice, a lindwurm can sense approaching creatures by the vibrations they cause, granting them tremorsense.

**Actions**

___Multiattack.___ The lindwurm makes one bite attack, one claw attack, and one constrict attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) piercing damage, and the target must succeed on a DC 14 Constitution saving throw or contract lindwurm fever.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 17 (3d8 + 4) slashing damage.

___Constrict.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage, and the target is grappled (escape DC 14). Until this grapple ends, the target is restrained, and the lindwurm can constrict only this target.

