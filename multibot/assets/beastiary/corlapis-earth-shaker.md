"Corlapis Earth Shaker";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "3 (700 XP)"
_languages_: "Terran"
_skills_: "Stealth +2"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Con +5"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "60 (8d8 + 24)"
_armor class_: "18 (natural armor, shield)"
_stats_: | 16 (+3) | 10 (+0) | 17 (+3) | 16 (+3) | 10 (+0) | 10 (+0) |

___Hardened Exterior.___ Bludgeoning, piercing, and
slashing damage the corlapis takes from nonmagical
weapons is reduced by 3 to a minimum of 1.

___Innate Spellcasting.___ The corlapis's innate spellcasting
ability is Intelligence (spell save DC 13). It can
innately cast the following spells, requiring no
material components:

* At will: _light, earth tremor, mold earth_

* 1/day each: _erupting earth, heat metal, meld into stone_

___Stone Camouflage.___ The corlapis has advantage on
Dexterity (Stealth) checks to hide in rocky terrain.

___Sturdy.___ The corlapis has advantage on saving throws
against effects that would cause it to move or be
knocked prone.

**Actions**

___War Pick.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 7 (1d8 + 3) piercing damage.
