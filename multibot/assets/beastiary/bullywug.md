"Bullywug";;;_size_: Medium humanoid (bullywug)
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Bullywug"
_skills_: "Stealth +3"
_speed_: "20 ft., swim 40 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "15 (hide armor, shield)"
_stats_: | 12 (+1) | 12 (+1) | 13 (+1) | 7 (-2) | 10 (0) | 7 (-2) |

___Amphibious.___ The bullywug can breathe air and water.

___Speak with Frogs and Toads.___ The bullywug can communicate simple concepts to frogs and toads when it speaks in Bullywug.

___Swamp Camouflage.___ The bullywug has advantage on Dexterity (Stealth) checks made to hide in swampy terrain.

___Standing Leap.___ The bullywug's long jump is up to 20 ft. and its high jump is up to 10 ft., with or without a running start.

**Actions**

___Multiattack.___ The bullywug makes two melee attacks: one with its bite and one with its spear.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) bludgeoning damage.

___Spear.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d6 + 1) piercing damage, or 5 (1d8 + 1) piercing damage if used with two hands to make a melee attack.