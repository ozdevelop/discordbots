"Marilith";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "16 (15,000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "truesight 120 ft."
_damage_immunities_: "poison"
_saving_throws_: "Str +9, Con +10, Wis +8, Cha +10"
_speed_: "40 ft."
_hit points_: "189 (18d10+90)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 20 (+5) | 20 (+5) | 18 (+4) | 16 (+3) | 20 (+5) |

___Magic Resistance.___ The marilith has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The marilith's weapon attacks are magical.

___Reactive.___ The marilith can take one reaction on every turn in combat.

**Actions**

___Multiattack.___ The marilith can make seven attacks: six with its longswords and one with its tail.

___Longsword.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

___Tail.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one creature. Hit: 15 (2d10 + 4) bludgeoning damage. If the target is Medium or smaller, it is grappled (escape DC 19). Until this grapple ends, the target is restrained, the marilith can automatically hit the target with its tail, and the marilith can't make tail attacks against other targets.

___Teleport.___ The marilith magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.

___Variant: Summon Demon (1/Day).___ The demon chooses what to summon and attempts a magical summoning.

A marilith has a 50 percent chance of summoning 1d6 vrocks, 1d4 hezrous, 1d3 glabrezus, 1d2 nalfeshnees, or one marilith.

A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.

**Reactions**

___Parry.___ The marilith adds 5 to its AC against one melee attack that would hit it. To do so, the marilith must see the attacker and be wielding a melee weapon.