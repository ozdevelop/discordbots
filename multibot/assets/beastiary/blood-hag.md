"Blood Hag";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "11 (7200 XP)"
_languages_: "Common, Giant, Infernal, Sylvan, Trollkin"
_skills_: "Deception +7, Intimidation +7, Perception +9, Stealth +7"
_senses_: "blood sense 90 ft., darkvision 60 ft., passive Perception 19"
_saving_throws_: "Dex +7, Con +8, Cha +7"
_condition_immunities_: "charmed, poisoned"
_speed_: "30 ft., climb 30 ft."
_hit points_: "178 (21d8 + 84)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 16 (+3) | 18 (+4) | 19 (+4) | 21 (+5) | 17 (+3) |

___Blood Sense.___ A blood hag automatically senses the blood of living creatures within 90 feet and can pinpoint their locations within 30 feet.

___Innate Spellcasting.___ The hag's innate spellcasting ability is Charisma (spell save DC 15). She can innately cast the following spells, requiring no material components:

At will: disguise self, knock, minor illusion, misty step, pass without trace, protection from evil and good, tongues, water breathing

3/day each: bestow curse, invisibility, mirror image

1/day each: cloudkill, modify memory

**Actions**

___Multiattack.___ The blood hag makes two claw attacks and one blood-drinking hair attack.

___Blood-Drinking Hair.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 18 (3d8 + 5) piercing damage and a Medium or smaller target is grappled (escape DC 15). A grappled creature takes 13 (2d8 + 3) necrotic damage at the start of the hag's turns, and the hag heals half as many hit points. The hag gains excess healing as temporary hit points. The hag can grapple one or two creatures at a time. Also see Face Peel.

___Claws.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 19 (4d6 + 5) slashing damage.

___Call the Blood.___ The blood hag targets a living creature within 30 feet that she detects with her blood sense and makes the target bleed uncontrollably. The target must make a successful DC 16 Constitution saving throw or suffer one of the effects listed below. A target that saves successfully cannot be affected by this hag's ability again for 24 hours.

___1.___ Blood Choke Curse. The target's mouth fills with blood, preventing any speech or spellcasting with verbal components for 1 minute.

___2.___ Blood Eye. The target's eyes well up with bloody tears. The target is blinded for 1 minute.

___3.___ Heart Like Thunder. The target hears only the rushing of blood and their thumping heart. They are deaf for 1 minute.

___4.___ Rupturing Arteries. The victim suffers 7 (2d6) slashing damage as its veins and arteries burst open. The target repeats the saving throw at the beginning of each of its turns. It takes 3 (1d6) necrotic damage if the saving throw fails, but the effect ends on a successful save.

___Face Peel.___ The blood hag peels the face off one grappled foe. The target must make a DC 17 Dexterity saving throw. If the saving throw fails, the face is torn off; the target takes 38 (8d6 + 10) slashing damage and is stunned until the start of the hag's next turn. If the save succeeds, the target takes half damage and isn't stunned. Heal, regeneration, or comparable magic restores the stolen features; other curative magic forms a mass of scar tissue. The peeled-off face is a tiny, animated object (per the spell-20 HP, AC 18, no attack, Str 4, Dex 18) under the hag's control. It retains the former owner's memories and personality. Blood hags keep such faces as trophies, but they can also wear someone's face to gain advantage on Charisma (Deception) checks made to imitate the face's former owner.

