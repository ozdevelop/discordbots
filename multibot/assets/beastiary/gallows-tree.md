"Gallows Tree";;;_size_: Huge plant
_alignment_: unaligned
_challenge_: "14 (11,500 XP)"
_languages_: "Common, but cannot speak, telepathy 100 ft. with gallows tree zombies"
_skills_: "Perception +7, Stealth +5"
_senses_: "tremorsense 60 ft., passive Perception 17"
_saving_throws_: "Con +11"
_damage_resistances_: "fire"
_condition_immunities_: "frightened, prone, stunned, unconscious"
_speed_: "30 ft."
_hit points_: "287 (23d12 + 138)"
_armor class_: "16 (natural armor)"
_stats_: | 25 (+7) | 10 (+0) | 22 (+6) | 10 (+0) | 14 (+2) | 6 (-2) |

___Create Gallows Tree Zombie.___ When a creature dies within 15 feet of a
gallows tree, The Gallow’s Tree uses a sharpened tendril to slice open the
creature’s abdomen, thereby spilling the corpse’s innards on the ground. The
organs and fluids are then absorbed by the tree’s roots. Corpses of a size other
than Medium or Large are simply left to rot. Medium or Large corpses are filled
with a greenish pollen fired from one of the tree’s branches. The abdominal
wound heals over the next 1d4 days, at which time the slain creature rises as a
gallows tree zombie connected by a tether-vine to the gallows tree that created
it. Gallows tree zombies possess none of their former abilities.

___Gallows Tree Zombies.___ Each gallows tree has several gallows tree
zombies connected to it. A Huge gallows tree may have no more than
seven gallows tree zombies connected to it at one time. See the gallows
tree zombie entry for details on that monster.

**Actions**

___Multiattack.___ The gallows tree makes three slam attacks.

___Slam.___ Melee Weapon Attack: +12 to hit, reach 15 ft., one target. Hit: 28
(6d6 + 7) bludgeoning damage and the target is grappled (escape DC 22)
and restrained.
