"Sidereal Vizier";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Elven, Sylvan"
_skills_: "Arcana +9, Insight +8, Nature +9, Perception +8, Religion +9"
_senses_: "passive Perception 22"
_saving_throws_: "Str +7, Con +7, Wis +8, Cha +8"
_speed_: "30 ft."
_hit points_: "152 (18d10 + 53)"
_armor class_: "19 (natural armor)"
_stats_: | 16 (+3) | 18 (+4) | 16 (+3) | 20 (+5) | 18 (+4) | 19 (+4) |

___Fey Ancestry.___ Magic cannot put the Vizier to sleep.

___Magic Resistance.___ The Vizier has advantage
on saving throws against spells and other
magical effects.

___Command Fey.___ As a member of the Court of
Arcadia, the Vizier can cast _dominate monster_ (DC
17) at will on any fey creature or elf.

___Innate Spellcasting.___ The Vizier’s innate spellcasting
ability is Intelligence (spell save DC 17). He
can innately cast the following spells, requiring no
material components:

* At will: _darkness, dominate person_

* 1/day: _power word stun_

___Spellcasting.___ The Vizier is a 15th-level spellcaster.
His spellcasting ability is Intelligence (spell save DC
17, +9 to hit with spell attacks). The Vizier has the
following wizard spells prepared:

* Cantrips (at will): _fire bolt, mage hand, minor illusion, prestidigitation_

* 1st level (4 slots): _detect magic, identify, magic missile_

* 2nd level (3 slots): _detect thoughts, mirror image, scorching ray, misty step_

* 3rd level (3 slots): _counterspell, fly, fireball_

* 4th level (3 slots): _banishment, dimension door_

* 5th level (2 slots): _cone of cold, hold monster_

* 6th level (1 slot): _chain lightning_

* 7th level (1 slot): _finger of death_

* 8th level (1 slot): _maze_

**Actions**

___Longsword.___ Melee Weapon Attack: +7 to hit, reach
5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.
