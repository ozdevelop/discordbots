"Demilich";;;_size_: Tiny undead
_alignment_: neutral evil
_challenge_: "18 (20,000 XP)"
_senses_: "truesight 120 ft."
_damage_immunities_: "necrotic, poison, psychic, bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Con +6, Int +11, Wis +9, Cha +11"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "80 (20d4)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned, prone, stunned"
_damage_resistances_: "bludgeoning, piercing, and slashing from magic weapons"
_stats_: | 1 (-5) | 20 (+5) | 10 (0) | 20 (+5) | 17 (+3) | 20 (+5) |

___Avoidance.___ If the demilich is subjected to an effect that allows it to make a saving throw to take only half damage, it instead takes no damage if it succeeds on the saving throw, and only half damage if it fails.

___Legendary Resistance (3/Day).___ If the demilich fails a saving throw, it can choose to succeed instead.

___Turn Immunity.___ The demilich is immune to effects that turn undead.

**Actions**

___Howl (Recharge 5-6).___ The demilich emits a bloodcurdling howl. Each creature within 30 feet of the demilich that can hear the howl must succeed on a DC 15 Constitution saving throw or drop to 0 hit points. On a successful save, the creature is frightened until the end of its next turn.

___Life Drain.___ The demilich targets up to three creatures that it can see within 10 feet of it. Each target must succeed on a DC 19 Constitution saving throw or take 21 (6d6) necrotic damage, and the demilich regains hit points equal to the total damage dealt to all targets.

**Legendary** Actions

The demilich can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The demilich regains spent legendary actions at the start of its turn.

___Flight.___ The demilich flies up to half its flying speed.

___Cloud of Dust.___ The demilich magically swirls its dusty remains. Each creature within 10 feet of the demilich, including around a corner, must succeed on a DC 15 Constitution saving throw or be blinded until the end of the demilich's next turn. A creature that succeeds on the saving throw is immune to this effect until the end of the demilich's next turn.

___Energy Drain (Costs 2 Actions).___ Each creature with in 30 feet of the demilich must make a DC 15 Constitution saving throw. On a failed save, the creature's hit point maximum is magically reduced by 10 (3d6). If a creature's hit point maximum is reduced to 0 by this effect, the creature dies. A creature's hit point maximum can be restored with the greater restoration spell or similar magic.

___Vile Curse (Costs 3 Actions).___ The demilich targets one creature it can see within 30 feet of it. The target must succeed on a DC 15 Wisdom saving throw or be magically cursed. Until the curse ends, the target has disadvantage on attack rolls and saving throws. The target can repeat the saving throw at the end of each of its turns, ending the curse on a success.