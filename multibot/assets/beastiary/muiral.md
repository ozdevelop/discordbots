"Muiral";;;_page_number_: 314
_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Dwarvish, Elvish, Goblin, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 16"
_skills_: "Arcana +9, Athletics +9, Perception +6, Stealth +5"
_saving_throws_: "Con +8, Int +9"
_speed_: "50 ft."
_hit points_: "195 (23d10+69)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 11 (0) | 16 (+3) | 18 (+4) | 13 (+1) | 18 (+4) |

___Spellcasting.___ Muiral is a 13th-level spellcaster. His spellcasting ability is Intelligence (spell save DC 17, +9 to hit with spell attacks). He has the following wizard spells prepared:

* Cantrips (at will): _dancing lights, mage hand, prestidigitation, ray of frost, shocking grasp_

* 1st level (4 slots): _expeditious retreat, fog cloud, magic missile, shield_

* 2nd level (3 slots): _darkness, knock, see invisibility, spider climb_

* 3rd level (3 slots): _animate dead, counterspell, lightning bolt_

* 4th level (3 slots): _greater invisibility, polymorph_

* 5th level (2 slots): _animate objects, wall of force_

* 6th level (1 slot): _create undead, flesh to stone_

* 7th level (1 slot): _finger of death_

___Legendary Resistance (3/Day).___ If Muiral fails a saving throw, he can choose to succeed instead.

**Actions**

___Multiattack.___ Muiral makes three attacks: two with his longsword and one with his sting.

___Longsword.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage, or 15 (2d10 + 4) slashing damage if used with two hands.

___Sting.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one creature. Hit: 9 (1d10 + 4) piercing damage. The target must make a DC 16 Constitution saving throw, taking 27 (6d8) poison damage on a failed save, or half as much damage on a successful one.

**Legendary** Actions

Muiral can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature’s turn. Muiral regains spent legendary actions at the start of his turn.

___Cast Cantrip.___ Muiral casts a cantrip.

___Lunging Attack (Costs 2 Actions).___ Muiral makes one longsword attack that has a reach of 10 feet.

___Retreating Strike (Costs 3 Actions).___ Muiral moves up to his speed without provoking opportunity attacks. Before the move, he can make one longsword attack.
