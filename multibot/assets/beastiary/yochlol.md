"Yochlol";;;_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "10 (5,900 XP)"
_languages_: "Abyssal, Elvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Deception +10, Insight +6"
_damage_immunities_: "poison"
_saving_throws_: "Dex +6, Int +5, Wis +6, Cha +6"
_speed_: "30 ft., climb 30 ft."
_hit points_: "136 (16d8+64)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 15 (+2) | 14 (+2) | 18 (+4) | 13 (+1) | 15 (+2) | 15 (+2) |

___Shapechanger.___ The yochlol can use its action to polymorph into a form that resembles a female drow or giant spider, or back into its true form. Its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Magic Resistance.___ The yochlol has advantage on saving throws against spells and other magical effects.

___Spider Climb.___ The yochlol can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Innate Spellcasting.___ The yochlol's spellcasting ability is Charisma (spell save DC 14). The yochlol can innately cast the following spells, requiring no material components:

At will: detect thoughts, web

1/day: dominate person

___Web Walker.___ The yochlol ignores movement restrictions caused by webbing.

**Actions**

___Multiattack.___ The yochlol makes two melee attacks.

___Slam (Bite in Spider Form).___ Melee Weapon Attack: +6 to hit, reach 5 ft. (10 ft. in demon form), one target. Hit: 5 (1d6 + 2) bludgeoning (piercing in spider form) damage plus 21 (6d6) poison damage.

___Mist Form.___ The yochlol transforms into toxic mist or reverts to its true form. Any equipment it is wearing or carrying is also transformed. It reverts to its true form if it dies.

While in mist form, the yochlol is incapacitated and can't speak. It has a flying speed of 30 feet, can hover, and can pass through any space that isn't airtight. It has advantage on Strength, Dexterity, and Constitution saving throws, and it is immune to nonmagical damage.

While in mist form, the yochlol can enter a creature's space and stop there. Each time that creature starts its turn with the yochlol in its space, the creature must succeed on a DC 14 Constitution saving throw or be poisoned until the start of its next turn. While poisoned in this way, the target is incapacitated.

___Variant: Summon Demon (1/Day).___ The demon chooses what to summon and attempts a magical summoning.

A yochlol has a 50 percent chance of summoning one yochlol.

A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.