"Corpse Rook";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "--"
_skills_: "Perception +2, Survival +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "10 ft., fly 60 ft."
_hit points_: "90 (12d10 + 24)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 17 (+3) | 14 (+2) | 5 (-3) | 10 (+0) | 11 (+0) |

___Flyby.___ The corpse rook doesn’t provoke opportunity attacks when it
flies out of an enemy’s reach.

___Three Heads.___ The corpse rook has advantage on Wisdom (Perception)
checks and on saving throws against being blinded, charmed, deafened,
frightened, stunned, or knocked unconscious.

**Actions**

___Multiattack.___ The corpse rook makes three bite attacks and one claw attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage.
