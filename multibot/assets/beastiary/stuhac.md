"Stuhac";;;_size_: Medium fiend
_alignment_: neutral evil
_challenge_: "13 (10000 XP)"
_languages_: "Common, Infernal; telepathy 100 ft."
_skills_: "Deception +12"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Str +11, Dex +9, Con +10, Cha +7"
_damage_immunities_: "cold, poison"
_damage_resistances_: "acid, fire; bludgeoning and piercing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft., climb 40 ft."
_hit points_: "190 (20d8 + 100)"
_armor class_: "18 (natural armor)"
_stats_: | 22 (+6) | 18 (+4) | 20 (+5) | 12 (+1) | 16 (+3) | 15 (+2) |

___Mountain Stride.___ Mountain slopes and stone outcroppings pose no obstacle to a stuhac's movement. In mountainous areas, it scrambles through difficult terrain without hindrance.

___Powerful Leap.___ The stuhac can jump three times the normal distance: 66 feet horizontally or 27 feet vertically with a running start, or half those distances from a stationary start.

___Shapechanger.___ The stuhac can use its action to polymorph into one of two forms: that of an elderly humanoid male, and its natural form. It cannot alter either form's appearance or capabilities using this ability, and damage sustained in one form transfers to the other form.

**Actions**

___Multiattack.___ The stuhac makes two claw attacks and one bite attack, or two claw attacks and one hobble.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 20 (4d6 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 29 (5d8 + 6) slashing damage.

___Hobble.___ A stuhac can cripple a creature by telekinetically tearing its tendons and ligaments. A stuhac can target one creature within 100 feet. The target must make a successful DC 16 Constitution saving throw or take 13 (3d8) force damage and its speed is reduced by 20 feet. Magical movement (flight, teleportation, etc.) is unaffected. This damage can only be cured through magical healing, not by spending hit dice or resting.

