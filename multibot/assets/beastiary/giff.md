"Giff";;;_page_number_: 204
_size_: Medium humanoid
_alignment_: lawful neutral
_challenge_: "3 (700 XP)"
_languages_: "Common"
_senses_: "passive Perception 11"
_speed_: "30 ft."
_hit points_: "60  (8d8 + 24)"
_armor class_: "16 (breastplate)"
_stats_: | 18 (+4) | 14 (+2) | 17 (+3) | 11 (0) | 12 (+1) | 12 (+1) |

___Headfirst Charge.___ The giff can try to knock a creature over; if the giff moves at least 20 feet in a straight line that ends within 5 feet of a Large or smaller creature, that creature must succeed on a DC 14 Strength saving throw or take 7 (2d6) bludgeoning damage and be knocked prone.

___Firearms Knowledge.___ The giff's mastery of its weapons enables it to ignore the loading property of muskets and pistols.

**Actions**

___Multiattack___ The giff makes two pistol attacks.

___Longsword___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, or 9 (1d10 + 4) slashing damage if used with two hands.

___Musket___ Ranged Weapon Attack: +4 to hit, range 40/120 ft., one target. Hit: 7 (1d12 + 2) piercing damage.

___Pistol___ Ranged Weapon Attack: +4 to hit, range 30/90 ft., one target. Hit: 7 (1d10 + 2) piercing damage.

___Fragmentation Grenade (1/day)___ The giff throws a grenade up to 60 feet. Each creature within 20 feet of the grenade's detonation must make a DC 15 Dexterity saving throw, taking 17 (5d6) piercing damage on a failed save, or half as much damage on a successful one.