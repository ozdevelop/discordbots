"Owlbear";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3"
_speed_: "40 ft."
_hit points_: "59 (7d10+21)"
_armor class_: "13 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 17 (+3) | 3 (-4) | 12 (+1) | 7 (-2) |

___Keen Sight and Smell.___ The owlbear has advantage on Wisdom (Perception) checks that rely on sight or smell.

**Actions**

___Multiattack.___ The owlbear makes two attacks: one with its beak and one with its claws.

___Beak.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 10 (1d10 + 5) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) slashing damage.