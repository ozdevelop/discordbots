"Sabrewing";;;_size_: Medium fiend
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Sabrewing"
_skills_: "Perception +5, Survival +4"
_senses_: "darkvision 120 ft., passive Perception 15"
_damage_resistances_: "cold, fire, lighting; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft., fly 60 ft."
_hit points_: "66 (7d8 + 35)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 15 (+2) | 20 (+5) | 12 (+1) | 14 (+2) | 15 (+2) |

___Flyby.___ The sabrewing doesn’t provoke an opportunity attack when it
flies out of an enemy’s reach.

___Magic Weapons.___ The sabrewing’s weapon attacks are magical.

**Actions**

___Multiattack.___ The sabrewing makes two attacks with its wings.

___Wings.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14
(2d8 + 5) slashing damage, and the target begins bleeding out. While the
target is bleeding out, it must succeed on a DC 15 Constitution saving
throw at the beginning of its turns, or its maximum hit points are reduced
by 7 (2d6). Bleeding out continues until the target or another creature uses
an action to make a successful DC 15 Wisdom (Medicine) check, or if the
target receives magical healing.
