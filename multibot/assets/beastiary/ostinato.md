"Ostinato";;;_size_: Medium aberration
_alignment_: chaotic neutral
_challenge_: "4 (1100 XP)"
_languages_: "telepathy 200 ft."
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_vulnerabilities_: "thunder"
_damage_immunities_: "poison"
_damage_resistances_: "acid, cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "fly 50 ft. (hover)"
_hit points_: "39 (6d8 + 12)"
_armor class_: "15"
_stats_: | 1 (-5) | 20 (+5) | 15 (+2) | 5 (-3) | 12 (+1) | 17 (+3) |

___Incorporeal Movement.___ The ostinato can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Invisibility.___ The ostinato is invisible as per a greater invisibility spell.

___Magic Resistance.___ The ostinato has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The ostinato makes two cacophony ray attacks.

___Cacophony Ray.___ Ranged Spell Attack: +7 to hit, range 60 ft., one target. Hit: 10 (3d6) thunder damage.

___Aural Symbiosis (1/Day).___ One humanoid that the ostinato can see within 5 feet of it must succeed on a DC 13 Charisma saving throw or the ostinato merges with it, becoming an enjoyable, repetitive tune in its host's mind. The ostinato can't be targeted by any attack, spell, or other effect. The target retains control of its body and is aware of the ostinato's presence only as a melody, not as a living entity. The target no longer needs to eat or drink, gains the ostinato's Magic Resistance trait, and has advantage on Charisma checks. It also has disadvantage on Wisdom saving throws and it can't maintain concentration on spells or other effects for more than a single turn. The target can make a DC 13 Wisdom (Insight) check once every 24 hours; on a success, it realizes that the music it hears comes from an external entity. The Aural Symbiosis lasts until the target drops to 0 hit points, the ostinato ends it as a bonus action, or the ostinato is forced out by a dispel evil and good spell or comparable magic. When the Aural Symbiosis ends, the ostinato bursts forth in a thunderous explosion of sound and reappears in an unoccupied space within 5 feet of the target. All creatures within 60 feet, including the original target, take 21 (6d6) thunder damage, or half damage with a successful DC 13 Constitution saving throw. The target becomes immune to this ostinato's Aural Symbiosis for 24 hours if it succeeds on the saving throw or after the Aural Symbiosis ends.

___Voracious Aura (1/Day).___ While merged with a humanoid (see Aural Symbiosis), the ostinato feeds on nearby creatures. Up to nine creatures of the ostinato's choice within 60 feet of it can be targeted. Each target must succeed on a DC 13 Charisma saving throw or take 3 (1d6) necrotic damage and have its hit point maximum reduced by the same amount until it finishes a long rest. The target dies if its maximum hit points are reduced to 0. Victims notice this damage immediately, but not its source.

