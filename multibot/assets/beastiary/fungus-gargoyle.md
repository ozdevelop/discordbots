"Fungus Gargoyle";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_skills_: "Perception +6, Stealth +5"
_senses_: "passive Perception 16"
_damage_immunities_: "poison"
_damage_resistances_: "fire; bludgeoning, piercing, and slashing from nonmagical weapons not made of adamantine"
_condition_immunities_: "exhaustion, petrified, poisoned"
_speed_: "30 ft., fly 60 ft."
_hit points_: "67 (9d8 + 27)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 17 (+3) | 6 (-2) | 11 (+0) | 7 (-2) |

___False Appearance.___ While the gargoyle remains motionless, it is
indistinguishable from a weathered, inanimate statue that is covered in
fungus, lichen, and moss.

___Stench.___ Any creature that starts its turn within 10 feet of the gargoyle
must succeed on a DC 13 Constitution saving throw or be poisoned until
the start of its next turn. On a successful saving throw, the creature is
immune to the gargoyle’s stench for 24 hours.

**Actions**

___Multiattack.___ The gargoyle makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Deadly Spores (Recharge 5–6).___ The fungus gargoyle exhales deadly
spores in a 15-foot cone. Each creature in the area must make a DC 15
Constitution saving throw, taking 18 (4d8) poison damage on a failed
save, or half as much damage on a successful one.
