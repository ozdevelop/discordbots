"Slaad Tadpole";;;_size_: Tiny aberration
_alignment_: chaotic neutral
_challenge_: "1/8 (25 XP)"
_languages_: "understands Slaad but can't speak"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +4"
_speed_: "30 ft."
_hit points_: "10 (4d4)"
_armor class_: "12"
_damage_resistances_: "acid, cold, fire, lightning, thunder"
_stats_: | 7 (-2) | 15 (+2) | 10 (0) | 3 (-4) | 5 (-3) | 3 (-4) |

___Magic Resistance.___ The slaad has advantage on saving throws against spells and other magical effects

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.