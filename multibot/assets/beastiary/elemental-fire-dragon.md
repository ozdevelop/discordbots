"Elemental Fire Dragon";;;_size_: Huge elemental
_alignment_: neutral evil
_challenge_: "21 (33,000 XP)"
_languages_: "Ignan, Common"
_skills_: "Arcana +10, Nature +10, Perception +15, Stealth +12"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 25"
_saving_throws_: "Dex +12, Con +14, Wis +8, Cha +12"
_damage_immunities_: "fire, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "40 ft., fly 80 ft."
_hit points_: "445 (33d12 + 231)"
_armor class_: "15"
_stats_: | 27 (+8) | 20 (+5) | 25 (+7) | 16 (+3) | 13 (+1) | 21 (+5) |

___Fiery Aura.___ At the start of each of the dragon’s turns, each creature
within 15 feet of it takes 14 (4d6) fire damage, and flammable objects in
the aura that aren’t being worn or carried ignite. A creature that touches
the dragon or hits it with a melee attack while within 5 feet of it takes 14
(4d6) fire damage.

___Illumination.___ The dragon sheds bright light in a 30-foot radius and dim
light in an additional 30 feet.

___Innate Spellcasting.___ The dragon’s innate spellcasting ability is
Charisma (spell save DC 20, +12 to hit withy spell attacks). It can cast the
following spells, requiring no material components:

* At will: _fireball, heat metal_

* 3/day each: _fire storm_

* 1/day each: _incendiary cloud, plane shift_

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it can
choose to succeed instead.

___Water Susceptibility.___ For every 5 feet that the dragon moves in water,
or for every gallon of water splashed on it, it takes 1 cold damage.

**Actions**

___Multiattack.___ The dragon can make three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target. Hit: 19
(2d10 + 8) piercing damage plus 14 (4d6) fire damage.

___Claw.___ Melee Weapon Attack: +15 to hit, reach 5 ft., one target. Hit: 15
(2d6 + 8) slashing damage.

___Tail.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one target. Hit: 17
(2d8 + 8) bludgeoning damage.

___Elemental Fire Breath (Recharge 5–6).___ The dragon breathes a 60-foot
cone of elemental fire. Creatures in the area must make a DC 22 Dexterity
saving throw, taking 77 (22d6) fire damage on a failed saving throw, or
half as much damage on a successful one.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 23 Dexterity
saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.

___Rain of Fire (Costs 3 Actions).___ The elemental fire dragon beats
its wings, casting fire out in a 100-foot sphere around itself. All
creatures within the area must make a DC 22 Dexterity saving throw,
taking 14 (4d6) fire damage on a failed saving throw, or half as much
damage on a successful one. Objects not held or worn are set alight
and continue burning until extinguished.
