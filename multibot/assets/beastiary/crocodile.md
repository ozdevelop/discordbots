"Crocodile";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_skills_: "Stealth +2"
_speed_: "20 ft., swim 20 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "12 (natural armor)"
_stats_: | 15 (+2) | 10 (0) | 13 (+1) | 2 (-4) | 10 (0) | 5 (-3) |

___Hold Breath.___ The crocodile can hold its breath for 15 minutes.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 7 (1d10 + 2) piercing damage, and the target is grappled (escape DC 12). Until this grapple ends, the target is restrained, and the crocodile can't bite another target