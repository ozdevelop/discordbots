"Lich";;;_size_: Medium undead
_alignment_: any evil alignment
_challenge_: "21 (33,000 XP)"
_languages_: "Common plus up to five other languages"
_senses_: "truesight 120 ft."
_skills_: "Arcana +18, History +12, Insight +9, Perception +9"
_damage_immunities_: "poison, bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Con +10, Int +12, Wis +9"
_speed_: "30 ft."
_hit points_: "135 (18d8+54)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_damage_resistances_: "cold, lightning, necrotic"
_stats_: | 11 (0) | 16 (+3) | 16 (+3) | 20 (+5) | 14 (+2) | 16 (+3) |

___Legendary Resistance (3/Day).___ If the lich fails a saving throw, it can choose to succeed instead.

___Rejuvenation.___ If it has a phylactery, a destroyed lich gains a new body in 1d10 days, regaining all its hit points and becoming active again. The new body appears within 5 feet of the phylactery.

___Spellcasting.___ The lich is an 18th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 20, +12 to hit with spell attacks). The lich has the following wizard spells prepared:

* Cantrips (at will): _mage hand, prestidigitation, ray of frost_

* 1st level (4 slots): _detect magic, magic missile, shield, thunderwave_

* 2nd level (3 slots): _detect thoughts, invisibility, Melf's acid arrow, mirror image_

* 3rd level (3 slots): _animate dead, counterspell, dispel magic, fireball_

* 4th level (3 slots): _blight, dimension door_

* 5th level (3 slots): _cloudkill, scrying_

* 6th level (1 slot): _disintegrate, globe of invulnerability_

* 7th level (1 slot): _finger of death, plane shift_

* 8th level (1 slot): _dominate monster, power word stun_

* 9th level (1 slot): _power word kill_

___Turn Resistance.___ The lich has advantage on saving throws against any effect that turns undead.

**Actions**

___Paralyzing Touch.___ Melee Spell Attack: +12 to hit, reach 5 ft., one creature. Hit: 10 (3d6) cold damage. The target must succeed on a DC 18 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

**Legendary** Actions

The lich can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The lich regains spent legendary actions at the start of its turn.

___Cantrip.___ The lich casts a cantrip.

___Paralyzing Touch (Costs 2 Actions).___ The lich uses its Paralyzing Touch.

___Frightening Gaze (Costs 2 Actions).___ The lich fixes its gaze on one creature it can see within 10 feet of it. The target must succeed on a DC 18 Wisdom saving throw against this magic or become frightened for 1 minute. The frightened target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a target's saving throw is successful or the effect ends for it, the target is immune to the lich's gaze for the next 24 hours.

___Disrupt Life (Costs 3 Actions).___ Each non-undead creature within 20 feet of the lich must make a DC 18 Constitution saving throw against this magic, taking 21 (6d6) necrotic damage on a failed save, or half as much damage on a successful one.
