"Gnoll";;;_size_: Medium humanoid (gnoll)
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Gnoll"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "22 (5d8)"
_armor class_: "15 (hide armor, shield)"
_stats_: | 14 (+2) | 12 (+1) | 11 (0) | 6 (-2) | 10 (0) | 7 (-2) |

___Rampage.___ When the gnoll reduces a creature to 0 hit points with a melee attack on its turn, the gnoll can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 4 (1d4 + 2) piercing damage.

___Spear.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d6 + 2) piercing damage, or 6 (1d8 + 2) piercing damage if used with two hands to make a melee attack.

___Longbow.___ Ranged Weapon Attack: +3 to hit, range 150/600 ft., one target. Hit: 5 (1d8 + 1) piercing damage.