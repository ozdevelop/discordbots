"Death Cow";;;_size_: Large monstrosity
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Bovine"
_saving_throws_: "Str +6, Con +5"
_skills_: "Animal Handling +4, Athletics +6, Perception +4"
_senses_: "passive Perception 14"
_speed_: "30 ft."
_hit points_: "90 (12d10 + 24)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 11 (+0) | 14 (+2) | 8 (-1) | 12 (+1) | 10 (+0) |

___Bovine Master.___ A death cow can take control of 1d6 cows located
within 30 feet of it as per the dominate monster spell. Cows controlled
in this fashion do not receive a Wisdom save, but are controlled
automatically.

___Cattle Guise.___ A death cow can voluntarily appear as a normal
quadrupedal cow, indistinguishable from a normal cow. It can transform
between cow and death cow form instantly. Its weapons only manifest in
bipedal form. The true seeing spell will reveal the death cow’s true form.

___Death Throes.___ When the death cow dies, it explodes, showering all
creatures within a 30-foot radius with scalding blood and chunks of
burning meat. Each creature in this area must make a DC 15 Dexterity
saving throw, taking 14 (4d6) fire damage on a failed save, or half as much
on a successful one.

___Secret Speech.___ Death cows speak in a strange mooing tongue called
Bovine that normally cannot be learned by non-cows. Those who wish to
learn the language must succeed on a DC 20 Intelligence check and have
a willing death cow as an instructor.

**Actions**

___Multiattack.___ The death cow makes one bite and one greatsword attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 3) piercing damage.

___Greatsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target.
Hit: 10 (2d6 + 3) slashing damage.

___War Moo (Recharge 5–6).___ The death cow unleashes a deafening moo.
All non-bovine creatures within 30 feet of the death cow that can hear the
moo, must attempt a DC 13 Constitution saving throw and be deafened
for 1 minute and take 28 (8d6) thunder damage on a failed save, or half as
much damage without being deafened on a successful one.
