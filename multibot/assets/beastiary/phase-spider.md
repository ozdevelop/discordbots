"Phase Spider";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +6"
_speed_: "30 ft., climb 30 ft."
_hit points_: "32 (5d10+5)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 15 (+2) | 12 (+1) | 6 (-2) | 10 (0) | 6 (-2) |

___Ethereal Jaunt.___ As a bonus action, the spider can magically shift from the Material Plane to the Ethereal Plane, or vice versa.

___Spider Climb.___ The spider can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Web Walker.___ The spider ignores movement restrictions caused by webbing.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 7 (1d10 + 2) piercing damage, and the target must make a DC 11 Constitution saving throw, taking 18 (4d8) poison damage on a failed save, or half as much damage on a successful one. If the poison damage reduces the target to 0 hit points, the target is stable but poisoned for 1 hour, even after regaining hit points, and is paralyzed while poisoned in this way.