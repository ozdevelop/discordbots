"River Troll";;;_size_: Large humanoid
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Giant"
_skills_: "Athletics +5, Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_speed_: "30 ft., swim 40 ft."
_hit points_: "57 (6d10 + 24)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 14 (+2) | 19 (+4) | 6 (-2) | 9 (-1) | 7 (-2) |

___Amphibious.___ The river troll can breathe both air and water.

___Keen Smell.___ The river troll has advantage on Wisdom (Perception)
checks that rely on smell.

___Poison Skin.___ The river troll’s skin contains a powerful poison. Any
time the troll touches another creature, the creature takes 7 (2d6) poison
damage (included in attacks).

___Regeneration.___ The river troll regains 10 hit points at the start of its
turn as long as it is at least partially submerged in water; strong rainfall
also allows the troll’s regeneration to function. If the troll takes acid or
lightning damage, this trait doesn’t function at the start of the troll’s next
turn. The troll only dies if it starts its turn with 0 hit points and doesn’t
regenerate.

**Actions**

___Multiattack.___ The river troll makes three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 7 (2d6) poison damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) slashing damage plus 7 (2d6) poison damage.
