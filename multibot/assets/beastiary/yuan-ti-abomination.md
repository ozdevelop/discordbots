"Yuan-ti Abomination";;;_size_: Large monstrosity (shapechanger yuan-ti)
_challenge_: "7 (2,900 XP)"
_languages_: "Abyssal, Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Perception +5, Stealth +6"
_damage_immunities_: "poison"
_speed_: "40 ft."
_hit points_: "127 (15d10+45)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 19 (+4) | 16 (+3) | 17 (+3) | 17 (+3) | 15 (+2) | 18 (+4) |

___Shapechanger.___ The yuan-ti can use its action to polymorph into a Large snake, or back into its true form. Its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It doesn't change form if it dies.

___Innate Spellcasting (Abomination Form Only).___ The yuan_ti's innate spellcasting ability is Charisma (spell save DC 15). The yuan_ti can innately cast the following spells, requiring no material components:

At will: animal friendship (snakes only)

3/day: suggestion

1/day: fear

___Magic Resistance.___ The yuan-ti has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack (Abomination Form Only).___ The yuan_ti makes two ranged attacks or three melee attacks, but can use its bite and constrict attacks only once each.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 10 (3d6) poison damage.

___Constrict.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage, and the target is grappled (escape DC 14). Until this grapple ends, the target is restrained, and the yuan_ti can't constrict another target.

___Scimitar (Abomination Form Only).___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Longbow (Abomination Form Only).___ Ranged Weapon Attack: +6 to hit, range 150/600 ft., one target. Hit: 12 (2d8 + 3) piercing damage plus 10 (3d6) poison damage.