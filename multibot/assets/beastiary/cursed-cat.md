"Cursed Cat";;;_size_: Tiny beast
_alignment_: neutral evil
_challenge_: "1/8 (25 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "40 ft., climb 30 ft."
_hit points_: "5 (2d4)"
_armor class_: "12"
_stats_: | 4 (-3) | 15 (+2) | 10 (+0) | 6 (-2) | 13 (+1) | 8 (-1) |

___Keen Smell.___ The cat has advantage on Wisdom
(Perception) checks that rely on smell.

___Aura of Misfortune.___ Whenever a non-evil creature
within 15 feet of the cat makes a skill check, they
subtract a d4 from the result. Whenever the cat is
killed, the creature that killed it must succeed on a
DC 9 Charisma saving throw or by affected by the
_bane_ spell for the next 10 minutes.

**Actions**

___Claws.___ Melee Weapon Attack: +2 to hit, reach 5 ft.,
one target. Hit: 2 (1d4) slashing damage.
