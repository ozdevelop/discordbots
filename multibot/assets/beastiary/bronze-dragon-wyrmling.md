"Bronze Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: lawful good
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +2"
_damage_immunities_: "lightning"
_saving_throws_: "Dex +2, Con +4, Wis +2, Cha +4"
_speed_: "30 ft., fly 60 ft., swim 30 ft."
_hit points_: "32 (5d8+10)"
_armor class_: "17 (natural armor)"
_stats_: | 17 (+3) | 10 (0) | 15 (+2) | 12 (+1) | 11 (0) | 15 (+2) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Lightning Breath. The dragon exhales lightning in a 40-foot line that is 5 feet wide. Each creature in that line must make a DC 12 Dexterity saving throw, taking 16 (3d10) lightning damage on a failed save, or half as much damage on a successful one.

Repulsion Breath. The dragon exhales repulsion energy in a 30-foot cone. Each creature in that area must succeed on a DC 12 Strength saving throw. On a failed save, the creature is pushed 30 feet away from the dragon.