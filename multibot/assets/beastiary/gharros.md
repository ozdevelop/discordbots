"Gharros (Scorpion Demon)";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "16 (15,000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_skills_: "Athletics +13, Perception +9, Survival +9"
_senses_: "truesight 120 ft., passive Perception 19"
_saving_throws_: "Con +13, Wis +9, Cha +1"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "270 (20d10 + 160)"
_armor class_: "20 (natural armor)"
_stats_: | 26 (+8) | 18 (+4) | 26 (+8) | 18 (+4) | 18 (+4) | 20 (+5) |

___Improved Critical.___ The demon’s attacks score a critical hit on a roll of
19 or 20.

___Innate Spellcasting.___ The demon’s spellcasting ability is Wisdom (spell
save DC 17, +9 to hit with spell attacks). It can innately cast the following
spells requiring no material components:

* At will: _darkness, detect evil and good, detect magic, mirror image_

* 3/day each: _hallow, telekinesis, teleport_

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Rampage.___ When the demon reduces a creature to 0 hit points with a
melee attack on its turn, the demon can take a bonus action to move up to
half its speed and make a halberd attack.

**Actions**

___Multiattack.___ The demon makes three attacks: one halberd attack and
two sting attacks.

___Halberd.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 19 (2d10 + 8) slashing damage.

___Sting.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 17 (2d8 + 8) piercing damage, and the target must make a DC 18 Constitution saving throw, taking 17 (5d6) poison damage on a failed save, or half as much damage on a successful one.

___Summon (1/day).___ The demon chooses what to summon and attempts a
magical summoning.
A gharros has a 50% chance of summoning 1d6 vrocks, 1d4 hezrous,
1d3 glabrezus, 1d2 nalfeshnees, or one marilith.
A summoned demon appears in an unoccupied space within 60 feet of
its summoner, acts as an ally of its summoner, and can’t summon other
demons. It remains for 1 minute, until it or its summoner dies, or until its
summoner dismisses it as an action.
