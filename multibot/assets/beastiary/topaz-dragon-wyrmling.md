"Topaz Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral good
_challenge_: "2 (450 XP)"
_languages_: "Common, Draconic, telepathy 60 ft."
_skills_: "Arcana +3, Insight +2, Perception +2, Religion +3"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 14"
_saving_throws_: "Dex +3, Int +3, Wis +2, Cha +3"
_damage_vulnerabilites_: "psychic"
_speed_: "30 ft., fly 60 ft. (hover)"
_hit points_: "27 (6d8)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 13 (+1) | 10 (+0) | 13 (+1) | 11 (+0) | 12 (+1) |

___Uplift Aura.___ All allies within 30 feet gain +1 on
Intelligence checks and saving throws.

**Psionics**

___Charges:___ 6 | ___Recharge:___ 1d4 | ___Fracture:___ 5

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft.,
one target. Hit: 6 (1d10 + 1) piercing damage.
