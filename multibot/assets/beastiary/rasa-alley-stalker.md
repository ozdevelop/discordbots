"Rasa Alley Stalker";;;_size_: Medium humanoid
_alignment_: any evil alignment
_challenge_: "1 (200 XP)"
_languages_: "Common"
_skills_: "Deception +5, Stealth +7"
_senses_: "passive Perception 9"
_speed_: "30 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "14 (leather)"
_stats_: | 12 (+1) | 16 (+3) | 13 (+1) | 14 (+2) | 8 (-1) | 13 (+1) |

___Innate Spellcasting.___ The rasa's innate spellcasting
ability is Intelligence (spell save DC 12). It can
innately cast the following spells, requiring no
components:

* At will: _mage hand, message_

* 1/day each: _disguise self, silent image_

___Mask of Shadows.___ The alley stalker has a mask
enchanted to make them a threat in shadowed
areas and when striking the first blow on an enemy.
The alley stalker has advantage on stealth checks
and its attacks against surprised enemies are always
considered a critical hit.

___Inscrutable Intentions.___ The rasa is immune to any
magical effects to determine if it is lying.

___Sight of the Rasa.___ The rasa share a special sight that
is invisible to all non-rasa creatures. As an action, a
rasa can touch a creature and place a secret mark
on that creature's chest. This mark lets other rasa
know whether or not this creature should be
regarded as a friend or a foe.

**Actions**

___Multiattack.___ The rasa makes two melee attacks.

___Poisoned Dagger.___ Melee or Ranged Weapon Attack:
+5 to hit, reach 5 ft. or range 20/60 ft., one target.
Hit: 5 (1d4 + 3) piercing damage plus 2 (1d4)
poison damage.
