"Horned Devil";;;_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "11 (7,200 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_damage_immunities_: "fire, poison"
_saving_throws_: "Str +10, Dex +7, Wis +7, Cha +7"
_speed_: "20 ft., fly 60 ft."
_hit points_: "178 (17d10+85)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 22 (+6) | 17 (+3) | 21 (+5) | 12 (+1) | 16 (+3) | 17 (+3) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The devil makes three melee attacks: two with its fork and one with its tail. It can use Hurl Flame in place of any melee attack.

___Fork.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) piercing damage.

___Tail.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 10 (1d8 + 6) piercing damage. If the target is a creature other than an undead or a construct, it must succeed on a DC 17 Constitution saving throw or lose 10 (3d6) hit points at the start of each of its turns due to an infernal wound. Each time the devil hits the wounded target with this attack, the damage dealt by the wound increases by 10 (3d6). Any creature can take an action to stanch the wound with a successful DC 12 Wisdom (Medicine) check. The wound also closes if the target receives magical healing.

___Hurl Flame.___ Ranged Spell Attack: +7 to hit, range 150 ft., one target. Hit: 14 (4d6) fire damage. If the target is a flammable object that isn't being worn or carried, it also catches fire.