"Ice Elemental (Homebrew)";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Aquan, Auran"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "cold, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_damage_vulnerabilites_: "fire, thunder"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "30 ft., climb 30 ft., swim 30 ft."
_hit points_: "126 (12d10 + 60)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 10 (+0) | 20 (+5) | 5 (-3) | 10 (+0) | 7 (-2) |

___Heat Susceptibility.___ At the end of each hour that the
elemental spends in temperatures above freezing, its
hit point maximum decreases by 10. The elemental
dies if its hit point maximum is reduced to 0. The
elemental's hit point maximum is restored when it
spends the course of an entire long rest in sub-freezing
temperatures.

___Ice Sense.___ While in icy or snowy terrain, the elemental
has tremorsense out to a range of of 60 feet.

___Ice Walk.___ The elemental can move across and climb icy
surfaces without needing to make an ability check.
Additionally, difficult terrain composed of ice or snow
doesn't cost it extra movement.

**Actions**

___Multiattack.___ The elemental makes two attacks with its
claws.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 10 ft.,
one target. Hit: 11 (2d6 + 4) slashing damage plus 7
(2d6) cold damage.

___Icicle Darts (Recharge 4–6).___ The elemental bristles with
countless tiny icicles which it then launches at a 10-
foot cube area it can see within 30 feet of it. Each
creature in the area must make a DC 15 Dexterity
saving throw, taking 7 (3d4) piercing damage plus 7
(3d4) cold damage on a failed save, or half as much on
a successful one.
