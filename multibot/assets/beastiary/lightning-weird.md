"Lightning Weird";;;_size_: Large elemental
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Auran, Common, Weirdling"
_senses_: "blindsight 30 ft., passive Perception 11"
_damage_immunities_: "acid, lightning, poison, thunder"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "50 ft."
_hit points_: "90 (12d10 + 24)"
_armor class_: "18 (natural armor)"
_stats_: | 17 (+3) | 20 (+5) | 15 (+2) | 10 (+0) | 12 (+1) | 14 (+2) |

___Electricity.___ If a creature attacks the lighting
weird with a melee weapon, that creature takes 9
(2d8) lightning damage.

___Lightning Mote.___ A lightning weird’s mote is a
crackling, dancing, arcing, ball of electricity that
occupies a 5-foot space. Creatures that start their
turn within 5 feet of the lightning mote take 13
(3d8) lightning damage; creatures wearing metal
armor must make a successful DC 15 Constitution
saving throw if they take lightning damage from being
near the mote. On a failed saving throw, the target is
stunned until the end of its next turn. The lightning can
move its mote up to 30 ft. as a bonus action. The mote
must remain within 90 ft.

___Reform.___ When reduced to 0 hit points, a lightning weird collapses
back into its pool. Four rounds later, it reforms at full strength minus any
damage taken from fire-based attacks and effects (including attacks by
earth or fire elemental creatures).

___Transparent.___ Even when the lightning weird is in plain sight, it takes
a successful DC 15 Wisdom (Perception) check to spot a lightning weird
that has neither moved nor attacked. A creature that tries to enter the
lightning weird’s space while unaware of the lightning weird is surprised
by the lightning weird.

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14
(2d8 + 5) piercing damage plus 9 (2d8) lightning damage.

___Command Elemental.___ One air elemental that the lightning weird can
see within 60 feet of it must make a DC 13 Wisdom saving throw. On
a failed saving throw, the air elemental is charmed for 1 minute. While
charmed, the air elemental follows the lightning weird’s commands.
