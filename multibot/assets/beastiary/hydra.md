"Hydra";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "8 (3,900 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +6"
_speed_: "30 ft., swim 30 ft."
_hit points_: "172 (15d12+75)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 20 (+5) | 2 (-4) | 10 (0) | 7 (-2) |

___Hold Breath.___ The hydra can hold its breath for 1 hour.

___Multiple Heads.___ The hydra has five heads. While it has more than one head, the hydra has advantage on saving throws against being blinded, charmed, deafened, frightened, stunned, and knocked unconscious.

Whenever the hydra takes 25 or more damage in a single turn, one of its heads dies. If all its heads die, the hydra dies.

At the end of its turn, it grows two heads for each of its heads that died since its last turn, unless it has taken fire damage since its last turn. The hydra regains 10 hit points for each head regrown in this way.

___Reactive Heads.___ For each head the hydra has beyond one, it gets an extra reaction that can be used only for opportunity attacks.

___Wakeful.___ While the hydra sleeps, at least one of its heads is awake.

**Actions**

___Multiattack.___ The hydra makes as many bite attacks as it has heads.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 10 (1d10 + 5) piercing damage.