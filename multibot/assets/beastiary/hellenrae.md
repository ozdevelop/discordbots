"Hellenrae";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Terran"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_skills_: "Acrobatics +7, Athletics +4, Insight +5, Perception +5"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "78 (12d8+24)"
_armor class_: "16"
_condition_immunities_: "blinded, poisoned"
_stats_: | 13 (+1) | 18 (+4) | 14 (+2) | 10 (0) | 15 (+2) | 13 (+1) |

___Evasion.___ If Hellenrae is subjected to an effect that allows her to make a Dexterity saving throw to take only half damage, she instead takes no damage if she succeeds on the saving throw, and only half damage if she fails.

___Stunning Strike (Recharge 5-6).___ When Hellenrae hits a target with a melee weapon attack, the target must succeed on a DC 13 Constitution saving throw or be stunned until the end of Hellenrae's next turn.

___Unarmored Defense.___ While Hellenrae is wearing no armor and wielding no shield, her AC includes her Wisdom modifier.

___Unarmored Movement.___ While Hellenrae is wearing no armor and wielding no shield, her speed increases by 20 feet (included in her speed).

**Actions**

___Multiattack.___ Hellenrae makes three melee attacks.

___Unarmed Strike.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) bludgeoning damage.

**Reactions**

___Parry and Counter.___ Hellenrae adds 3 to her AC against one melee or ranged weapon attack that would hit her. To do so, she must be able to sense the attacker with her blindsight. If the attack misses, Hellenrae can make one melee attack against the attacker if it is within her reach.