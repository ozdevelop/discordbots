"Swarm of Spiders";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 10 ft."
_speed_: "20 ft., climb 20 ft."
_hit points_: "22 (5d8)"
_armor class_: "12 (natural armor)"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, slashing"
_stats_: | 3 (-4) | 13 (+1) | 10 (0) | 1 (-5) | 7 (-2) | 1 (-5) |

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny insect. The swarm can't regain hit points or gain temporary hit points.

___Spider Climb.___ The swarm can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Web Sense.___ While in contact with a web, the swarm knows the exact location of any other creature in contact with the same web.

___Web Walker.___ The swarm ignores movement restrictions caused by webbing.

**Actions**

___Bites.___ Melee Weapon Attack: +3 to hit, reach 0 ft., one target in the swarm's space. Hit: 10 (4d4) piercing damage, or 5 (2d4) piercing damage if the swarm has half of its hit points or fewer.