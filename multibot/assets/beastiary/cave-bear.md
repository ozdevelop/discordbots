"Cave Bear";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3"
_speed_: "40 ft., swim 30 ft."
_hit points_: "42 (5d10+15)"
_armor class_: "12 (natural armor)"
_stats_: | 20 (+5) | 10 (0) | 16 (+3) | 2 (-4) | 13 (+1) | 7 (-2) |

___Keen Smell.___ The bear has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Multiattack.___ The bear makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.