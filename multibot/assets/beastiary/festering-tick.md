"Festering Tick";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 9"
_speed_: "25 ft."
_hit points_: "13 (2d8 + 4)"
_armor class_: "10"
_stats_: | 12 (+1) | 11 (+0) | 14 (+2) | 1 (-5) | 9 (-1) | 1 (-5) |

___Explosive Death.___ When the tick dies, it explodes in a
shower of blood. Each creature within 5 feet of it
must succeed on a DC 10 Dexterity saving throw or
take 5 (2d4) necrotic damage.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5ft.,
one target. Hit: 3 (1d4 + 1) piercing damage and
the tick attaches to the target. While attached, the
tick doesn’t attack. Instead, at the start of each of
the tick’s turns, the creature it is attached to takes
3 (1d6) necrotic damage and the tick gains the
same amount of temporary hit points. While the
tick is attached, a creature may use its action to
make a DC 10 Athletics check, prying the tick loose
on a success.
