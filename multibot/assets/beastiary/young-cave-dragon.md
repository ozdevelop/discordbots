"Young Cave Dragon";;;_size_: Large dragon
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "Common, Darakhul, Draconic"
_skills_: "Perception +4, Stealth +4"
_senses_: "blindsight 120 ft., passive Perception 14"
_saving_throws_: "Dex +4, Con +8, Wis +4, Cha +7"
_damage_immunities_: "acid, poison, thunder"
_condition_immunities_: "poisoned"
_speed_: "40 ft., climb 20 ft., fly 20 ft."
_hit points_: "157 (15d10 + 75)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 12 (+1) | 20 (+5) | 10 (+0) | 12 (+1) | 18 (+4) |

___Tunneler.___ The cave dragon can burrow through solid rock at half its burrowing speed and leaves a 10-foot wide, 5-foot high tunnel in its wake.

___Innate Spellcasting.___ The dragon's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

1/day each: blur, counterspell, web

3/day: darkness

**Actions**

___Multiattack.___ The dragon makes three attacks; one with its biteand two with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 13 (2d6 + 6) piercing damage plus 3 (1d6) poison damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Poison Breath (Recharge 5-6).___ The dragon exhales a cone of black poison gas in a 30-foot cone. Each creature in that area must make a DC 16 Constitution saving throw, taking 45 (13d6) poison damage on a failed save and becoming poisoned if it is a creature. The poisoned condition lasts until the target takes a long or short rest or removes the condition with lesser restoration or comparable magic. If the save is successful, the target takes half damage and is not poisoned.

**Reactions**

___Ruff Spikes.___ When a creature tries to enter a space adjacent to a cave dragon, the dragon flares its many feelers and spikes. The creature cannot enter a space adjacent to the dragon unless it makes a successful DC 16 Dexterity saving throw. If the saving throw fails, the creature can keep moving but only into spaces that aren't within 5 feet of the dragon and takes 4 (1d8) piercing damage from spikes.

