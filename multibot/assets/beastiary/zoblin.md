"Zoblin";;;_size_: Small undead (goblinoid)
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "22 (4d6 + 8)"
_armor class_: "10 (leather armor)"
_stats_: | 12 (+1) | 8 (-1) | 15 (+2) | 5 (-3) | 7 (-2) | 6 (-2) |

___Undead Fortitude.___ If damage reduces the zoblin to 0 hit
points, it must make a Constitution saving throw with a
DC of 5 + the damage taken, unless the damage is
radiant or from a critical hit. On a success, the zoblin
drops to 1 hit point instead.

**Actions**

___Vicious Bite.___ Melee Weapon Attack: +3 to hit, reach
5ft., one target. Hit: 3 (1d4 + 1) piercing damage, and
the zoblin attaches to the target. While attached, the
zoblin doesn’t attack. Instead, at the start of each of
the zoblin’s turns, that creature takes 5 (2d4) necrotic
damage. While the zoblin is attached, that creature’s
movement speed is also reduced by 10 ft. A creature
may use its action to make a DC 11 Athletics check,
prying the zoblin loose on a success.
