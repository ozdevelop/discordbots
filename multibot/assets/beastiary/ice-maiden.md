"Ice Maiden";;;_size_: Medium fey
_alignment_: lawful evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Giant, Sylvan"
_skills_: "Deception +9, Persuasion +9, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Con +5, Cha +9"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "cold"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30ft."
_hit points_: "84 (13d8 + 26)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 17 (+3) | 15 (+2) | 19 (+4) | 13 (+1) | 23 (+6) |

___Chilling Presence.___ Cold air surrounds the ice maiden. Small non-magical flames are extinguished in her presence and water begins to freeze. Unprotected characters spending more than 10 minutes within 15 feet of her must succeed on a DC 15 Constitution saving throw or suffer as if exposed to severe cold. Spells that grant protection from cold damage are targeted by an automatic dispel magic effect.

___Cold Eyes.___ Ice maidens see perfectly in snowy conditions, including driving blizzards, and are immune to snow blindness.

___Ice Walk.___ Ice maidens move across icy and snowy surfaces without penalty.

___Snow Invisibility.___ In snowy environments, the ice maiden can turn invisible as a bonus action.

___Magic Resistance.___ The ice maiden has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The ice maiden's innate spellcasting ability is Charisma (spell save DC 17). She can innately cast the following spells:

At will: chill touch, detect magic, light, mage hand, prestidigitation, resistance

5/day each: endure elements (cold only), fear, fog cloud, misty step

3/day each: alter self, protection from energy, sleet storm

1/day: ice storm

**Actions**

___Multiattack.___ The frost maiden makes two ice dagger attacks.

___Ice Dagger.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d4 + 3) piercing damage plus 3 (1d6) cold damage.

___Flurry-Form.___ The ice maiden adopts the form of a swirling snow cloud. Her stats are identical to an air elemental that deals cold damage instead of bludgeoning.

___Icy Entangle.___ Ice and snow hinder her opponent's movement, as the entangle spell (DC 17).

___Kiss of the Frozen Heart.___ An ice maiden may kiss a willing individual, freezing the target's heart. The target falls under the sway of a dominate spell, his or her alignment shifts to LE, and he or she gains immunity to cold. The ice maiden can have up to three such servants at once. The effect can be broken by dispel magic (DC 17), greater restoration, or the kiss of someone who loves the target.

___Snowblind Burst.___ In a snowy environment, the ice maiden attempts to blind all creatures within 30 feet of herself. Those who fail a DC 17 Charisma saving throw are blinded for 1 hour. Targets that are immune to cold damage are also immune to this effect.

