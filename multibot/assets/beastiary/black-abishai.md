"Black Abishai";;;_page_number_: 160
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "7 (2900 XP)"
_languages_: "Draconic, Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 16"
_skills_: "Perception +6, Stealth +6"
_damage_immunities_: "acid, fire, poison"
_saving_throws_: "Dex +6, Wis +6"
_speed_: "30 ft., fly 40 ft."
_hit points_: "58  (9d8 + 18)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 14 (+2) | 17 (+3) | 14 (+2) | 13 (+1) | 16 (+3) | 11 (0) |

___Devil's Sight.___ Magical darkness doesn't impede the abishai's darkvision.

___Magic Resistance.___ The abishai has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The abishai's weapon attacks are magical.

___Shadow Stealth.___ While in dim light or darkness, the abishai can take the Hide action as a bonus action.

**Actions**

___Multiattack___ The abishai makes three attacks: two with its scimitar and one with its bite.

___Scimitar___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Bite___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage plus 9 (2d8) acid damage.

___Creeping Darkness (Recharge 6)___ The abishai casts darkness at a point within 120 feet of it, requiring no components. Wisdom is its spellcasting ability for this spell. While the spell persists, the abishai can move the area of darkness up to 60 feet as a bonus action.
