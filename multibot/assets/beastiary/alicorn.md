"Alicorn";;;_size_: Large celestial
_alignment_: lawful good
_challenge_: "5 (1,800 XP)"
_languages_: "Celestial, Elvish, Sylvan, telepathy 60 ft."
_skills_: "Perception +6"
_senses_: "truesight 60 ft., passive Perception 16"
_saving_throws_: "Dex +6, Wis +6, Cha +6"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, paralyzed, poisoned"
_speed_: "60 ft., fly 90 ft."
_hit points_: "76 (9d10 + 27)"
_armor class_: "13"
_stats_: | 19 (+4) | 16 (+3) | 16 (+3) | 11 (+0) | 17 (+3) | 16 (+3) |

___Charge.___ If the alicorn moves at least 20 feet straight
toward a target and then hits it with a horn attack
on the same turn, the target takes an extra 9 (2d8),
Unit piercing damage. If the target is a creature, it
must succeed on a DC 15 Strength saving throw or
be knocked prone.

___Innate Spellcasting.___ The alicorn’s innate spellcasting
ability is Charisma (spell save DC 14). It can innately
cast the following spells, requiring no components:

* At will: _detect evil and good, druidcraft, pass without trace_

* 1/day each: _calm emotions, dispel evil and good, entangle_

___Magic Resistance.___ The alicorn has advantage on saving
throws against spells and other magical effects.

___Magic Weapons.___ The alicorn’s weapon attacks
are magical.


**Actions**

___Multiattack.___ The alicorn makes one attack with its
hooves and one attack with its horn.

___Hooves.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 11 (2d6 + 4) bludgeoning damage.

___Horn.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 8 (1d8 + 4) piercing damage.

___Healing Touch (3/Day).___ The alicorn touches another
creature with its horn. The target magically restores
11 (2d8 + 2) hit points. In addition, the touch
removes all diseases and neutralizes all poisons
afflicting the target.

___Teleport (1/Day).___ The alicorn magically teleports
itself and up to three willing creatures it can see
within 5 feet of it, along with any equipment they
are wearing or carrying, to a location the alicorn is
familiar with, up to 1 mile away.

**Legendary** Actions

The alicorn can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The alicorn regains spent
legendary actions at the start of its turn.

___Hooves.___ The alicorn makes one attack with
its hooves.

___Prismatic Barrier (Costs 2 Actions).___ The alicorn
creates a prismatic, magical field around itself or
another creature it can see within 60 feet of it. The
target gains a +2 bonus to AC until the end of the
alicorn’s next turn.

___Heal Self (Costs 3 Actions).___ The alicorn magically
restores 11 (2d8 + 2) hit points.
