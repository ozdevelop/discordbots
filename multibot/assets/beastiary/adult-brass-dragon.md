"Adult Brass Dragon";;;_size_: Huge dragon
_alignment_: chaotic good
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 60 ft., darkvision 120 ft."
_skills_: "History +7, Perception +11, Persuasion +8, Stealth +5"
_damage_immunities_: "fire"
_saving_throws_: "Dex +5, Con +10, Wis +6, Cha +8"
_speed_: "40 ft., burrow 40 ft., fly 80 ft."
_hit points_: "172 (15d12+75)"
_armor class_: "18 (natural armor)"
_stats_: | 23 (+6) | 10 (0) | 21 (+5) | 14 (+2) | 13 (+1) | 17 (+3) |

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +11 to hit, reach,.0 ft., one target. Hit: 17 (2d10 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Tail.___ Melee Weapon Attack: +11 to hit, reach 15 ft., one target. Hit: 15 (2d8 + 6) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Fire Breath. The dragon exhales fire in an 60-foot line that is 5 feet wide. Each creature in that line must make a DC 18 Dexterity saving throw, taking 45 (13d6) fire damage on a failed save, or half as much damage on a successful one.

Sleep Breath. The dragon exhales sleep gas in a 60-foot cone. Each creature in that area must succeed on a DC 18 Constitution saving throw or fall unconscious for 10 minutes. This effect ends for a creature if the creature takes damage or someone uses an action to wake it.