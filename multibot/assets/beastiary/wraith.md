"Wraith";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft."
_damage_immunities_: "necrotic, poison"
_speed_: "0 ft., fly 60 ft. (hover)"
_hit points_: "67 (9d8+27)"
_armor class_: "13"
_condition_immunities_: "charmed, exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, cold, fire, lightning, thunder, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 6 (-2) | 16 (+3) | 16 (+3) | 12 (+1) | 14 (+2) | 15 (+2) |

___Incorporeal Movement.___ The wraith can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Sunlight Sensitivity.___ While in sunlight, the wraith has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Life Drain.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 21 (4d8 + 3) necrotic damage. The target must succeed on a DC 14 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

___Create Specter.___ The wraith targets a humanoid within 10 feet of it that has been dead for no longer than 1 minute and died violently. The target's spirit rises as a specter in the space of its corpse or in the nearest unoccupied space. The specter is under the wraith's control. The wraith can have no more than seven specters under its control at one time.