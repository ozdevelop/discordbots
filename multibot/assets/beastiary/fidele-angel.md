"Fidele Angel";;;_size_: Medium celestial
_alignment_: lawful good
_challenge_: "5 (1800 XP)"
_languages_: "Common, Celestial, Infernal"
_skills_: "Insight +6, Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +7, Con +5, Int +5, Wis +6, Cha +7"
_damage_immunities_: "acid, cold"
_damage_resistances_: "fire, lightning, poison; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, petrified, poisoned"
_speed_: "40 ft., fly 40 ft. (angelic form), or 10 ft., fly 80 ft. (eagle form)"
_hit points_: "104 (16d8 + 32)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 18 (+4) | 14 (+2) | 14 (+2) | 16 (+3) | 18 (+4) |

___Shapechange.___ The angel can change between winged celestial form, its original mortal form, and that of a Medium-sized eagle. Its statistics are the same in each form, with the exception of its attacks in eagle form.

___Ever Touching.___ Fidele angels maintain awareness of their mate's disposition and health. Damage taken by one is split evenly between both, with the original target of the attack taking the extra point when damage doesn't divide evenly. Any other baneful effect, such as ability damage, affects both equally.

___Innate Spellcasting.___ The angel's innate spellcasting ability is Wisdom (spell save DC 14, +6 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: guidance, light, purify food and drink, spare the dying

3/day: cure wounds, scorching ray (5 rays)

1/day: bless, daylight, detect evil and good, enhance ability, hallow, protection from evil and good

___Magic Resistance.___ The angel has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The angel's weapon attacks are magical while it is in eagle form.

___To My Lover's Side.___ If separated from its mate, each fidele angel can use both plane shift and teleport 1/day to reunite.

___Unshakeable Fidelity.___ Fidele angels are never voluntarily without their partners. No magical effect or power can cause a fidele angel to act against its mate, and no charm or domination effect can cause them to leave their side or to change their feelings of love and loyalty toward each other.

**Actions**

___Multiattack.___ The angel makes two longsword attacks or two longbow attacks; in eagle form, it instead makes two talon attacks and one beak attack.

___+1 Longsword (Mortal or Angel Form Only).___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 10 (1d8 + 6) slashing damage or 11 (1d10+ 6) slashing damage if used with two hands.

___+1 Longbow (Mortal or Angel Form Only).___ Ranged Weapon Attack: +8 to hit, range 150/600 ft., one target. Hit: 9 (1d8 + 5) piercing damage.

___Beak (Eagle Form Only).___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) piercing damage.

___Talons (Eagle Form Only).___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

