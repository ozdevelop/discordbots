"Jackal";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_skills_: "Perception +3"
_speed_: "40 ft."
_hit points_: "3 (1d6)"
_armor class_: "12"
_stats_: | 8 (-1) | 15 (+2) | 11 (0) | 3 (-4) | 12 (+1) | 6 (-2) |

___Keen Hearing and Smell.___ The jackal has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Pack Tactics.___ The jackal has advantage on an attack roll against a creature if at least one of the jackal's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Bite.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 1 (1d4 - 1) piercing damage.