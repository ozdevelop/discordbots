"Imbued Magma Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Ignan, Terran"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 30 ft."
_hit points_: "34 (8d6 + 8)"
_armor class_: "12"
_stats_: | 10 (+0) | 14 (+2) | 12 (+1) | 8 (-1) | 12 (+1) | 10 (+0) |

___Death Burst.___ When the mephit dies, it explodes in a
burst of lava. Each creature within 5 feet of it must
make a DC 12 Dexterity saving throw, taking 14
(4d6) fire damage on a failed save, or half as much
damage on a successful one.

___False Appearance.___ While the mephit remains
motionless, it is indistinguishable from an ordinary
mound of magma.

**Actions**

___Multiattack.___ The mephit makes two claw attacks.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 5 (1d6 + 2) piercing damage plus 5
(2d4) fire damage.

___Magma Spout (Recharge 6).___ The mephit causes
magma to burst forward from a point on a solid
surface within 50 ft. All creatures a 15-foot line in
front of that point must make a DC 12 dexterity
saving throw, taking 13 (3d8) fire damage on a
failed save, or half as much damage on a successful
one.
