"Half-Red Dragon Veteran";;;_size_: Medium humanoid (human)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Athletics +5, Perception +2"
_saving_throws_: "Str +5, Con +4"
_speed_: "30 ft."
_hit points_: "65 (10d8+20)"
_armor class_: "18 (plate)"
_damage_resistances_: "fire"
_stats_: | 16 (+3) | 13 (+1) | 14 (+2) | 10 (0) | 11 (0) | 10 (0) |

**Actions**

___Multiattack.___ The veteran makes two longsword attacks. If it has a shortsword drawn, it can also make a shortsword attack.

___Longsword.___ Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1d10 + 3) slashing damage if used with two hands.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +3 to hit, range 100/400 ft., one target. Hit: 6 (1d10 + 1) piercing damage.

___Fire Breath (Recharge 5-6).___ The veteran exhales fire in a 15-foot cone. Each creature in that area must make a DC 15 Dexterity saving throw, taking 24 (7d6) fire damage on a failed save, or half as much damage on a successful one.