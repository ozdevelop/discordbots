"Map Mimic";;;_size_: Tiny aberration
_alignment_: neutral
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "acid"
_condition_immunities_: "prone"
_speed_: "30 ft., fly 15 ft."
_hit points_: "32 (5d8 + 10)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 15 (+2) | 14 (+2) | 13 (+1) | 15 (+2) | 16 (+3) |

___Shapechanger.___ The mimic can use its action to polymorph into an object or back into its true, amorphous form. Its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Constrict Face.___ When a map mimic touches a Medium or smaller creature or vice versa, it adheres to the creature, enveloping the creature's face and covering its eyes and ears and airways (escape DC 13). The target creature is immediately blinded and deafened, and it begins suffocating at the beginning of the mimic's next turn.

___False Appearance (Object Form Only).___ While the mimic remains motionless, it is indistinguishable from an ordinary object.

___Mimic Page.___ The mimic can disguise itself as any tiny, flat object.a piece of leather, a plate.not only a map. In any form, a map mimic can make a map on its skin leading to its mother mimic.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage. If the mimic is in object form, the target is subjected to its Constrict Face trait.

