"Ixitxachitl Cleric";;;_size_: Small aberration
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "Abyssal, Ixitxachitl"
_senses_: "darkvision 60 ft."
_speed_: "0 ft., swim 30 ft."
_hit points_: "18 (4d6+4)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 13 (+1) | 12 (+1) | 13 (+1) | 7 (-2) |

___Spellcasting.___ The ixtachitl is a 5th-level spellcaster that uses Wisdom as its spellcasting ability (Spell save DC 11, +3 to hit with spell attacks). The ixitxachitl has the following cleric spells prepared:

* Cantrips (at will): _guidance, thaumaturgy_

* 1st level (4 slots): _charm person, create or destroy water_

* 2nd level (3 slots): _hold person, silence_

* 3rd level (2 slots): _dispel magic, tongues_

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) piercing damage.

**Reactions**

___Barbed Tail.___ When a creature provokes an opportunity attack from the ixitxachitl, the ixitxachitl can make the following attack instead of using its bite:

Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.
