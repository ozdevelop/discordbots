"Shabti";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "8 (3900 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "102 (12d8 + 48)"
_armor class_: "17 (natural armor)"
_stats_: | 14 (+2) | 20 (+5) | 18 (+4) | 6 (-2) | 11 (+0) | 6 (-2) |

___Immutable Form.___ The shabti is immune to spells and effects that would alter its form.

___Magic Resistance.___ The shabti has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The shabti's weapon attacks are magical.

___Serpentine Armlets.___ As a bonus action, the shabti commands its armlets to drop to the floor, whereupon they become two giant poisonous snakes. The shabti can mentally direct the serpents (this does not require an action). If the snakes are killed, they dissolve into wisps of smoke which reform around the shabti's forearms, and they can't be turned into snakes for 1 week. These armlets are linked to the shabti at the time of its creation and do not function for other creatures.

**Actions**

___Multiattack.___ The shabti uses Telekinesis and makes two attacks with its nabboot.

___Nabboot.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d4 + 5) bludgeoning damage plus 7 (2d6) necrotic damage. If the target is a creature, it must succeed on a DC 15 Constitution saving throw or be cursed with tomb taint. The cursed target's speed is reduced to half, and its hit point maximum decreases by 3 (1d6) for every 24 hours that elapse. If the curse reduces the target's hit point maximum to 0, the target dies and its body turns to dust. The curse lasts until removed by the remove curse spell or comparable magic.

___Telekinesis.___ The shabti targets a creature within 60 feet. The target must succeed on a DC 15 Strength check or the shabti moves it up to 30 feet in any direction (including upward), and it is restrained until the end of the shabti's next turn.

