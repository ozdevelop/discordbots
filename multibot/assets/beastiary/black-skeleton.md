"Black Skeleton";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "the languages it knew in life but can’t speak"
_skills_: "Perception +4, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_vulnerabilities_: "bludgeoning, radiant"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "cold"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "40 ft."
_hit points_: "71 (13d8 + 13)"
_armor class_: "17 (chain shirt)"
_stats_: | 11 (+0) | 19 (+4) | 13 (+1) | 13 (+1) | 10 (+0) | 14 (+2) |

___Shortsword Masters.___ Black skeletons gain defensive bonuses (+2 to
AC) and bonuses to attack (+2 to hit) when wielding two shortswords
(included in the statistics).

**Actions**

___Multiattack.___ The black skeleton makes two claw attacks or two
shortsword attacks.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8
(1d8 + 4) slashing damage, and the target’s Strength score is reduced
by 1d4. The target dies if this reduces its strength to 0. Otherwise, the
reduction lasts until the target finishes a short or long rest.

___Shortsword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target.
Hit: 11 (2d6 + 4) piercing damage, and the target’s Strength score is
reduced by 1d4. The target dies if this reduces its strength to 0. Otherwise,
the reduction lasts until the target finishes a short or long rest
