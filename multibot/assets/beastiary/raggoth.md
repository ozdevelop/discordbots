"Raggoth";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "8 (3,900 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +6, Con +8"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "40 ft."
_hit points_: "136 (13d10 + 65)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 16 (+3) | 20 (+5) | 6 (-2) | 12 (+1) | 14 (+2) |

___Keen Smell.___ The raggoth has advantage on Wisdom
(Perception) checks that rely on smell.

**Actions**

___Multiattack.___ The raggoth makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 15
(2d8 + 6) piercing damage.

___Claws.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 20
(4d6 + 6) slashing damage, and the target is grappled (escape DC 17) and
restrained.

___Tormenting Howl (Recharge 5–6).___ The raggoth unleashes a piercing
howl. All creatures within 60 feet of the raggoth that can hear it must
succeed on a DC 15 Wisdom saving throw or be frightened for 1 minute.
A creature can repeat the saving throw at the end of each of its turns,
ending the effect on itself on a success. If a creature’s saving throw is
successful or the effect ends for it, the creature is immune to the raggoth’s
Tormenting Howl for 24 hours.
