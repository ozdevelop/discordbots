"Lesser Marilith";;;_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 13"
_saving_throws_: "Wis +5, Cha +6"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "97 (13d8 + 39)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 16 (+3) | 15 (+2) | 17 (+3) |

___Magic Resistance.___ The lesser marilith has advantage on saving throws against spells and other
magical effects.

___Magic Weapons.___ The lesser marilith’s weapon attacks
are magical.


**Actions**

___Multiattack.___ The lesser marilith makes three attacks
with its longsword and one attack with its tail.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5
ft., one target. Hit: 7 (1d8 + 3) slashing damage.

___Tail.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one
creature. Hit: 12 (2d8 + 3) bludgeoning damage. If the
target is Medium or smaller, it is grappled (escape DC
14). Until this grapple ends, the target is restrained,
the lesser marilith can automatically hit the target
with its tail, and the lesser marilith cannot make tail
attacks against other targets.

**Reactions**

___Parry.___ The lesser marilith can add 5 to its AC against
one melee attack that would hit it. To do so, the
lesser marilith must see the attacker and be wielding
a melee weapon.
