"Thorn Slinger";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_speed_: "10 ft."
_hit points_: "32 (5d10+5)"
_armor class_: "11"
_condition_immunities_: "blinded, deafened, frightened"
_stats_: | 13 (+1) | 12 (+1) | 12 (+1) | 1 (-5) | 10 (0) | 1 (-5) |

___Source.___ tales from the yawning portal,  page 246

___Adhesive Blossoms.___ The thorn slinger adheres to anything that touches it. A Medium or smaller creature adhered to the thorn slinger is also grappled by it (escape DC 11). Ability checks made to escape this grapple have disadvantage.

At the end of each of the thorn slinger's turns, anything grappled by it takes 3 (1d6) acid damage.

___False Appearance.___ While the thorn slinger remains motionless, it is indistinguishable from an inanimate bush.

**Actions**

___Thorns.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 30 ft., one target. Hit: 8 (2d6 + 1) piercing damage.
