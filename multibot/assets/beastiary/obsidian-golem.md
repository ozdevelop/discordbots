"Obsidian Golem";;;_size_: Large construct
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 120 ft., passive Perception 10"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "142 (15d10 + 60)"
_armor class_: "18 (natural armor)"
_stats_: | 20 (+5) | 8 (-1) | 18 (+4) | 3 (-4) | 10 (+0) | 1 (-5) |

___Magic Resistance.___ The golem has advantage on saving
throws against spell and other magical effects.

___Magic Weapons.___ The golem’s weapons are magical.

___Molten Core.___ Whenever a creature with 5ft. deals 20
damage or more to the golem in a single round of
combat, a spray of lava erupts, dealing 11 (2d10) fire
damage to the attacker.

**Actions**

___Multiattack.___ The golem can use its Molten Surge if
available. It then makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5ft., one
target. Hit: 14 (2d8 + 5) bludgeoning damage, and the
target is grappled (escape DC 15) if it is a Large or
smaller creature. Until this grapple ends, the target is
restrained, and the golem can’t use its slam on another
target. The golem can have one target grappled with
each fist.

___Molten Surge (Recharge 5-6).___ Until the end of its next
turn, the golem deals an additional 10 (3d6) fire
damage with each of its slam attacks.

___Smelt Intruders (1/Day).___ If the golem has a creature
grappled in both of its fists it can use this ability to
attempt to turn them to molten puddle. The golem is
programmed to use this only for max efficiency so it
will wait until it holds 2 targets to use it. Each grappled
creature makes a DC 15 Constitution saving throw,
taking 36 (8d10) fire damage on a failed save, or half
as much as a successful one. If this damage reduces a
creature to 0 hit points, its body becomes a puddle of
molten flesh.
