"Female Steeder (B)";;;_page_number_: 238
_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "-"
_senses_: "darkvision 120 ft., passive Perception 14"
_skills_: "Stealth +7, Perception +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "30  (4d10 + 8)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 16 (+3) | 14 (+2) | 2 (-4) | 10 (0) | 3 (-3) |

___Spider Climb.___ The steeder can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Extraordinary Leap.___ The distance of the steeder's long jumps is tripled; every foot of its walking speed that it spends on the jump allows it to move 3 feet.

**Actions**

___Bite___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage plus 9 (2d8) poison damage.

___Sticky Leg___ Melee Weapon Attack: +5 to hit, reach 5 ft., one Medium or smaller creature. Hit: The target is stuck to the steeder's leg and is grappled until it escapes (escape DC 12). The steeder can have only one creature grappled at a time.