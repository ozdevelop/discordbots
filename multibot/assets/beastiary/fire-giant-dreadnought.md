"Fire Giant Dreadnought";;;_size_: Huge giant (fire giant)
_alignment_: lawful evil
_challenge_: "14 (11,500 XP)"
_languages_: "Giant"
_skills_: "Athletics +13, Perception +5"
_damage_immunities_: "fire"
_saving_throws_: "Dex +4, Con +11, Cha +5"
_speed_: "30 ft."
_hit points_: "187 (15d12+90)"
_armor class_: "21 (plate, shields)"
_stats_: | 27 (+8) | 9 (-1) | 23 (+6) | 8 (-1) | 10 (0) | 11 (0) |

___Dual Shields.___ The giant carries two shields, each of which is accounted for in the giant's AC. The giant must stow or drop one of its shields to hurl rocks.

**Actions**

___Multiattack.___ The giant makes two fireshield attacks.

___Fireshield.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 22 (4d6+8) bludgeoning damage plus 7 (2d6) fire damage plus 7 (2d6) piercing damage.

___Rock.___ Ranged Weapon Attack: +13 to hit, range 60/240 ft, one target. Hit: 30 (4d10+8) bludgeoning damage.

___Shield Charge.___ The giant moves up to 30 feet in a straight line and can move through the space of any creature smaller than Huge. The first time it enters a creature's space during this move, it makes a fireshield attack against that creature. If the attack hits, the target must also succeed on a DC 21 Strength saving throw or be pushed ahead of the giant for the rest of this move. If a creature fails the save by 5 or more, it is also knocked prone and takes 18 (3d6+8) bludgeoning damage, or 29 (6d6+8) bludgeoning damage if it was already prone.