"Chupacabra";;;_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +4"
_senses_: "darkvision 120 ft., passive Perception 13"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_speed_: "30 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 16 (+3) | 6 (-2) | 13 (+1) | 9 (-1) |

___Sunlight Sensitivity.___ While in sunlight, the chupacabra has
disadvantage on attack rolls, as well as on Wisdom (Perception)
checks that rely on sight.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7
(2d4 + 2) piercing damage. If the target is a creature, it must succeed
on a DC 13 Strength saving throw or be knocked prone.

___Drain Blood.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature
that is prone, incapacitated, or restrained. Hit: 5 (1d6 + 2) necrotic
damage. Additionally, the target’s hit point maximum is reduced by
an amount equal to the damage taken, and the chupacabra regains
hit points equal to that amount. The reduction lasts until the target
finishes a long rest. The target dies if this effect reduces its hit point
maximum to 0.

**Reactions**

___Pin.___ If a creature within 5 feet of the chupacabra stands up, the
chupacabra can use its reaction to make a bite attack.
