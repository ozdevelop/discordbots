"Lantern Dragonette";;;_size_: Tiny dragon
_alignment_: lawful neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Draconic, Elvish, Primordial; telepathy 60 ft."
_skills_: "Arcana +5, History +5, Nature +5, Perception +3, Religion +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +3, Wis +3, Cha +3"
_condition_immunities_: "paralyzed, unconscious"
_speed_: "15 ft., fly 40 ft. (hover)"
_hit points_: "28 (8d4 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 7 (-2) | 12 (+1) | 13 (+1) | 16 (+3) | 13 (+1) | 12 (+1) |

___Lantern Belly (1/Day).___ If the dragonette has eaten 8 ounces of candle wax in the last 24 hours, it can emit a continual flame for 3d20 minutes. The continual flame can be dispelled, but the dragonette can resume it with a bonus action except in areas of magical darkness, if the time limit hasn't expired.

___Innate Spellcasting.___ The lantern dragonette's innate spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* 1/day each: _burning hands, color spray, scorching ray_

___Vulnerable to Magical Darkness.___ A lantern dragonette in an area of magical darkness loses its lantern belly ability and its ability to fly. It also suffers 1d6 radiant damage for every minute of exposure.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage.

