"Mechanical Golem";;;_size_: Huge construct
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_languages_: "understands one language known by its creator but can't speak"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "fire; bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "95 (10d12 + 30)"
_armor class_: "18 (natural armor)"
_stats_: | 19 (+4) | 5 (-3) | 17 (+3) | 3 (-4) | 10 (+0) | 1 (-5) |

___Immutable Form.___ The golem is immune to any spell or
effect that would alter its form.

___Magic Resistance.___ The golem has advantage on saving
throws against spells and other magical effects.

___Siege Monster.___ The golem deals double damage to
objects and structures.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one
target. Hit: 14 (3d6 + 4) bludgeoning damage.

___Chest Cannon (Recharge 6).___ Ranged Weapon Attack: +6
to hit, range 40/120 ft., one target. Hit: 10 (3d6)
bludgeoning damage. Hit or miss, the target and each
creature within 20 feet of it must make a DC 14
Dexterity saving throw, taking 10 (3d6) fire damage on a
failed save, or half as much on a successful one.
