"Apprentice Shifter";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages and Druidic"
_skills_: "Animal Handling +5, Medicine +5, Survival +5"
_senses_: "passive Perception 13"
_saving_throws_: "Int +2, Wis +5"
_speed_: "30 ft."
_hit points_: "22 (4d8 + 4)"
_armor class_: "13 (leather)"
_stats_: | 12 (+1) | 15 (+2) | 13 (+1) | 10 (+0) | 16 (+3) | 12 (+1) |

___Spellcasting.___ The shifter is a 3th-level spellcaster. Its
spellcasting ability is Wisdom (spell save DC 13, +5
to hit with spell attacks). The shifter has the
following druid spells prepared:

* Cantrips (at will): _druidcraft, thorn whip_

* 1st level (4 slots): _entangle, faerie fire, fog cloud, speak with animals_

* 2nd level (2 slots): _lesser restoration, moonbeam_

___Wild Shape - Black Bear (2/Short Rest).___ As a bonus
action, the druid transforms into a black bear for up
to two hours. When the bear is reduced to 0 hit
points, the druid reverts to its normal form, with
any excess damage carrying over to the druid's hit
points.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +3 to hit, reach
5 ft., one target. Hit: 4 (1d6 + 1) bludgeoning
damage or 5 (1d8 +1) bludgeoning damage if used
with two hands.
