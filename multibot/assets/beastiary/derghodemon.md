"Derghodemon (Cockroach Demon)";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_skills_: "Intimidation +7, Perception +6, Stealth +7"
_senses_: "truesight 120 ft., passive Perception 20"
_saving_throws_: "Dex +7, Con +9, Wis +6, Cha +7"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "147 (14d10 + 70)"
_armor class_: "18 (natural armor)"
_stats_: | 25 (+7) | 16 (+3) | 21 (+5) | 7 (-2) | 14 (+2) | 16 (+3) |

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Innate Spellcasting.___ The demon’s spellcasting ability is Charisma
(spell save DC 15, +7 to hit with spell attacks). The demon can innately
cast the following spells, requiring no material components:

* At will: _darkness, fear, detect magic_

* 1/day each: _confusion, sleep_

___Rend and Tear.___ The demon has advantage on all melee weapon attacks
against a creature it is grappling.

**Actions**

___Multiattack.___ The demon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 16 (2d8 + 7) piercing damage, and the target is grappled (escape DC 17).
Until this grapple ends, the demon can bite only the grappled creature and
has advantage on attack rolls to do so.

___Claws.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 21 (4d6 + 7) slashing damage.
