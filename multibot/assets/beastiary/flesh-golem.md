"Flesh Golem";;;_size_: Medium construct
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "lightning, poison; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_speed_: "30 ft."
_hit points_: "93 (11d8+44)"
_armor class_: "9"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 19 (+4) | 9 (-1) | 18 (+4) | 6 (-2) | 10 (0) | 5 (-3) |

___Berserk.___ Whenever the golem starts its turn with 40 hit points or fewer, roll a d6. On a 6, the golem goes berserk. On each of its turns while berserk, the golem attacks the nearest creature it can see. If no creature is near enough to move to and attack, the golem attacks an object, with preference for an object smaller than itself. Once the golem goes berserk, it continues to do so until it is destroyed or regains all its hit points.

The golem's creator, if within 60 feet of the berserk golem, can try to calm it by speaking firmly and persuasively. The golem must be able to hear its creator, who must take an action to make a DC 15 Charisma (Persuasion) check. If the check succeeds, the golem ceases being berserk. If it takes damage while still at 40 hit points or fewer, the golem might go berserk again.

___Aversion of Fire.___ If the golem takes fire damage, it has disadvantage on attack rolls and ability checks until the end of its next turn.

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Lightning Absorption.___ Whenever the golem is subjected to lightning damage, it takes no damage and instead regains a number of hit points equal to the lightning damage dealt.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.
