"Shadow Demon";;;_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_skills_: "Stealth +7"
_damage_immunities_: "cold, lightning, poison"
_saving_throws_: "Dex +5, Cha +4"
_speed_: "30 ft., fly 30 ft."
_hit points_: "66 (12d8+12)"
_armor class_: "13"
_damage_vulnerabilities_: "radiant"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, fire, necrotic, thunder, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 1 (-5) | 17 (+3) | 12 (+1) | 14 (+2) | 13 (+1) | 14 (+2) |

___Incorporeal Movement.___ The demon can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Light Sensitivity.___ While in bright light, the demon has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Shadow Stealth.___ While in dim light or darkness, the demon can take the Hide action as a bonus action.

**Actions**

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 10 (2d6 + 3) psychic damage or, if the demon had advantage on the attack roll, 17 (4d6 + 3) psychic damage.