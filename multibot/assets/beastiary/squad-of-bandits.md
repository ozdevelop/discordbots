"Squad of Bandits";;;_size_: Large swarm
_alignment_: any non-lawful alignment
_challenge_: "1 (200 XP)"
_languages_: "Any one language (usually Common)"
_senses_: "passive Perception 10"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "30 ft."
_hit points_: "33 (6d8 + 6)"
_armor class_: "11 (leather)"
_stats_: | 17 (+3) | 10 (+0) | 12 (+1) | 10 (+0) | 10 (+0) | 10 (+0) |

___Swarm.___ The swarm can occupy another creature's
space and vice versa, and the swarm can move
through any opening large enough for a Medium
bandit. The swarm can't regain hit points or gain
temporary hit points.

**Actions**

___Scimitars.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 13 (2d6 + 6) slashing damage,
or 6 (1d6 + 3) slashing damage if the swarm has
half of its hit points or fewer.
