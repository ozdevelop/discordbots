"Magmin";;;_size_: Small elemental
_alignment_: chaotic neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Ignan"
_senses_: "darkvision 60 ft."
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "9 (2d6+2)"
_armor class_: "14 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 7 (-2) | 15 (+2) | 12 (+1) | 8 (-1) | 11 (0) | 10 (0) |

___Death Burst.___ When the magmin dies, it explodes in a burst of fire and magma. Each creature within 10 ft. of it must make a DC 11 Dexterity saving throw, taking 7 (2d6) fire damage on a failed save, or half as much damage on a successful one. Flammable objects that aren't being worn or carried in that area are ignited.

___Ignited Illumination.___ As a bonus action, the magmin can set itself ablaze or extinguish its flames. While ablaze, the magmin sheds bright light in a 10-foot radius and dim light for an additional 10 ft.

**Actions**

___Touch.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d6) fire damage. If the target is a creature or a flammable object, it ignites. Until a creature takes an action to douse the fire, the creature takes 3 (1d6) fire damage at the end of each of its turns.