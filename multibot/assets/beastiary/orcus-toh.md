"Orcus, Demon Prince of the Undead";;;_size_: Huge fiend (demon lord)
_alignment_: chaotic evil
_challenge_: "35 (330,000 XP)"
_languages_: "all, telepathy 360 ft."
_skills_: "History +18, Perception +18, Religion +18"
_senses_: "truesight 120 ft., passive Perception 28"
_saving_throws_: "Dex +15, Con +20, Wis +18, Cha +20"
_damage_immunities_: "lightning, necrotic, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "acid, cold, fire"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "40 ft., fly 60 ft."
_hit points_: "825 (50d12 + 500)"
_armor class_: "25 (natural armor)"
_stats_: | 30 (+10) | 21 (+5) | 30 (+10) | 27 (+8) | 27 (+8) | 30 (+10) |

___Special equipment.___ Orcus wields its iconic rod, the Wand of Orcus.

___Innate Spellcasting.___ Orcus’s spellcasting ability is Charisma
(spell save DC 28, +20 to hit with spell attacks). Orcus can
innately cast the following spells, requiring no material
components:

* At will: _animate dead, chill touch _(17th level)_, detect magic_

* 7/day: _create undead_

* 5/day: _eyebite_

* 3/day each: _circle of death, finger of death_

___Legendary Resistance (3/day).___ If Orcus fails a saving throw, it
can choose to succeed instead. 

___Lord of the Grave.___ Orcus always casts animate dead or create undead at maximum level and without any restrictions. Creatures created by these spells remain under its control
indefinitely.

___Magic Resistance.___ Orcus has advantage on saving throws against spells
and other magical effects.

___Magic Weapons.___ Orcus’s attacks are magical.

**Actions**

___Multiattack.___ Orcus makes two Wand of Orcus attacks.

___Wand of Orcus.___ Melee Weapon Attack: +20 to hit, reach 5 ft., one
target. Hit: 24 (4d6 + 10) bludgeoning damage and the target must make
a DC 17 Charisma saving throw, dropping to 0 hit points on failure, or, on
a success, the target takes 21 (6d6) necrotic damage.

___Tail.___ Melee Weapon Attack: +20 to hit, reach 5 ft., one target. Hit: 24 (4d6 + 10) piercing damage plus 22 (4d10) poison damage.

___Aura of Death.___ Each non-undead creature that starts its turn within 20
feet of Orcus takes 17 (5d6) necrotic damage.

___Aura of Enfeeblement (Recharge 5–6).___ Black beams of negative
energy surround Orcus in a 20-foot radius centered on it. Each creature in
this area must make a DC 24 Constitution saving throw. On a failure, the
target deals only half damage with weapon attacks that use Strength for 1
minute. A weakened target can repeat the saving throw at the end of each
of its turns, ending the effect on itself on a success. If a target’s saving
throw is successful or the effect ends for it, the target is immune to Orcus’s Aura of Enfeeblement for the next 24 hours.

___Teleport.___ Orcus magically teleports, along with any equipment it is
wearing or carrying, up to 120 feet to an unoccupied space it can see.

**Legendary** Actions

Orcus can take 3 legendary actions, choosing from the options below.
Only one legendary action can be used at a time and only at the end of
another creature’s turn. Orcus regains spent legendary actions at the start
of its turn.

___Tail.___ Orcus makes one tail attack.

___Devouring Darkness (Costs 2 Actions).___ Orcus chooses a point that it
can see within 100 feet of it. A cloud of darkness erupts from that point
and lasts for 1 minute. The area is heavily obscured and each creature
in the area must make a DC 24 Constitution saving throw, taking 28
(8d6) necrotic damage on a failure, or half as much damage on a success.
Creatures slain by the devouring darkness rise as ghouls under the
command of Orcus within 1d4 rounds.

___Charnel Wind (Costs 3 Actions).___ A putrid, carrion wind erupts from
Orcus in a 30-foot radius centered on it and lasts until the end of Orcus’s
next turn. All non-undead creatures within this area must succeed on a DC
24 Constitution saving throw against disease or be poisoned until the end
of Orcus’s next turn. While poisoned in this way, the creature can take
either an action or a bonus action on its turn, not both, and it can’t take
reactions.

**Lair** Actions

On Initiative count 20 (losing initiative ties), Orcus can take a lair
action to cause one of the following effects; it can’t use the same effect
two rounds in a row:

___Beckon Army of the Dead.___ While in its lair, Orcus can use the wand to
summon an undead army that consists of:
* 10 skeletons
* 5 zombies
* 3 ghouls
* 3 specters
* 2 wights
* 1 mummy

These undead magically rise up from the ground or otherwise form
in unoccupied spaces within 100 feet of Orcus and obey its commands
until they are destroyed or until it dismisses them as an action. Orcus can
only use this lair action every 24 hours unless it is in the Palace of Bones
on Thanatos, the level of the Abyss it rules. Orcus may use it on each
subsequent initiative 20 at will if on that plane.

___Bone Cage.___ Orcus causes all bones within the lair to form tight
cages around two creatures of its choice. The cages can be attacked and
destroyed (AC 15; hp 30; vulnerability to bludgeoning damage; resistance
to piercing, poison, slashing, and psychic damage). While in a bone-cage,
a creature is restrained.

___Fountain of Blood.___ Orcus chooses a point on the ground that it can see
within 100 feet of it. A geyser of caustic blood erupts from the ground
at that point and rains down in a 40-foot high, 10-foot cylinder. Each
creature in that area must make a DC 24 Dexterity saving throw, taking
24 (7d6) acid damage on a failure, or half as much damage on a success.

**Regional** Effects

The region containing Orcus’s lair is warped by its magic. If a creature
within 10 miles of Orcus’ lair dies, roll a d20. On a 19 or 20, the creature
rises as a zombie under Orcus’ control.

If Orcus dies, this effect fade over the course of 1d10 days.

# Wand of Orcus

_Rod, artifact (requires attunement)_

The mighty Wand of Orcus is a huge, black, skull-tipped rod,
fully 6-feet in length. Its head is an ancient, bleached jawless skull,
whose eye sockets glow with ruddy, red light. It exudes an aura of
primeval menace, chilling the soul and the body with the cold of
the grave.

___Aura of Death.___ Any living creature is unable to take short or
long rests within 300 feet of the Wand of Orcus. If any creature
who is not Orcus touches the Wand of Orcus, it must succeed on
a DC 17 Charisma saving throw or drop to 0 hit points and begin
dying. On a successful saving throw, the creature takes 6d6 necrotic
damage. A creature who succeeds on the saving throw is immune
to this effect for 24 hours. Orcus can suppress this effect from any
locations.

___Attunement.___ To attune to the artifact, you must bathe yourself
and the Wand of Orcus in the blood of a Solar Angel.

___Magic Weapon.___ The Wand of Orcus functions as a magic mace
that has a +3 bonus to attack and damage rolls made with this
weapon. It deals 4d6 bludgeoning damage on a hit. It also functions
as a sword of wounding.

___Spellcasting.___ You can use an action to cast the following spells at
will: _blight, darkness,_ or _speak with dead_. The Wand of Orcus has
20 charges, which can be used to cast the following spells: _animate
dead _(3 or more charges)_, bestow curse _(2 charges)_, circle of death
_(6 charges)_, contagion _(5 charges)_, create undead _(6 or more
charges)_, finger of death _(7 charges)_, harm _(6 charges), or _power
word kill _(9 charges). The Wand of Orcus recovers all charges each
midnight.

___Sentience.___ The Wand of Orcus is a sentient chaotic evil magic
item, with an Intelligence of 20, a Wisdom of 18, and a Charisma
of 23. It has truesight out to 120 feet and understands Abyssal and
 Infernal. It can communicate with its wielder telepathically. Its
goal is the subjugation and destruction of all realms under the all encompassing
gaze of Orcus. It will attempt to sway or control an
attuned creature to further the goals of Orcus.

The Wand of Orcus is evil, wholly so. The wand’s telepathic
messages are quiet and breathy, almost a wet wheeze through
diseased lungs. It is not averse to assisting an attuned wielder’s
temporary goals, as long as they are not inimical to its long-term
goal of turning all planes of existence into fields of undead.

___Destruction.___ The Wand of Orcus can only be destroyed if Orcus
is truly slain and destroyed. If Orcus is permanently slain, the Wand
of Orcus becomes brittle and can be easily smashed into dust.
