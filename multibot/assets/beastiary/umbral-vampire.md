"Umbral Vampire";;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Umbral, Void Speech"
_skills_: "Perception +5, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Dex +7, Cha +7"
_damage_immunities_: "cold, necrotic, poison"
_damage_resistances_: "acid, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "84 (13d8 + 26)"
_armor class_: "14"
_stats_: | 1 (-5) | 18 (+4) | 15 (+2) | 14 (+2) | 14 (+2) | 19 (+4) |

___Incorporeal Movement.___ The umbral vampire can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Innate Spellcasting.___ The umbral vampire's innate spellcasting ability is Charisma (spell save DC 15). The umbral vampire can innately cast the following spells, requiring no material components:

* At will: _mirror image, plane shift _(plane of shadows only)

* 1/day each: _bane _(when in dim light or darkness only)_, black tentacles_

___Shadow Blend.___ When in dim light or darkness, the umbral vampire can Hide as a bonus action, even while being observed.

___Strike from Shadow.___ The reach of the umbral vampire's umbral grasp attack increases by 10 feet and its damage increases by 4d6 when both the umbral vampire and the target of the attack are in dim light or darkness and the umbral vampire is hidden from its target.

___Sunlight Sensitivity.___ While in direct sunlight, the umbral vampire has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

**Actions**

___Umbral Grasp.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 18 (4d6 + 4) cold damage and the target's Strength score is reduced by 1d6. The target dies if this reduces its Strength to 0. Otherwise, the reduction lasts until the target finishes a short or long rest. If a non-evil humanoid dies from this attack, a shadow rises from the corpse 1d4 hours later.

