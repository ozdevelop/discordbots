"Shroud";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "1/8 (25 XP)"
_languages_: "Common"
_skills_: "Stealth +3"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "radiant"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "9 (2d8)"
_armor class_: "13 (natural armor)"
_stats_: | 4 (-3) | 13 (+1) | 10 (+0) | 2 (-4) | 10 (+0) | 8 (-1) |

___Amorphous.___ The shroud can move through a space as narrow as 1 inch wide without squeezing.

___Shadow Evolution.___ Shrouds instantly become shadows once they cause a total of 12 damage. Any damage they've suffered is subtracted from the shadow's total hit points or abilities.

___Shroud Stealth.___ When in dim light or darkness, the shroud can take the Hide action as a bonus action.

___Sunlight Weakness.___ While in sunlight, the shroud has disadvantage on attack rolls, ability checks, and saving throws.

**Actions**

___Strength Drain.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 3 (1d4 + 1) necrotic damage, and the target's Strength score is reduced by one-half that amount. The target dies if this reduces its Strength to 0. Otherwise, the reduction lasts until the target finishes a short or long rest. If a non-evil humanoid dies from this attack, a new shadow rises from the corpse 1d4 hours later.

