"Stone Delver";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Terran"
_senses_: "darkvision 120 ft., tremorsense 120 ft., passive Perception 10"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "19 (natural armor)"
_stats_: | 21 (+5) | 14 (+2) | 18 (+4) | 7 (-2) | 10 (+0) | 9 (-1) |

___Six Limb Charge.___ If the stone delver moves at least 20 feet straight
towards a target and then hits it with a claw attack on the same turn, the
target takes an extra 10 (3d6) slashing damage. If the target is a creature,
it must succeed on a DC 16 Strength saving throw or be knocked prone.

If the stone delver hit its initial target on the same turn and knocked it
prone, it can use a bonus action to make up to four claw attacks against
targets that are adjacent to the initial target.

___Tunneler.___ The stone delver can burrow through solid stone at half its
burrowing speed leaving a 5 foot wide, 10-foot-high tunnel in its wake.

**Actions**

___Multiattack.___ The stone delver makes three claw attacks.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 10
(1d10+5) slashing damage.
