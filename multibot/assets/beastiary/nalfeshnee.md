"Nalfeshnee";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "truesight 120 ft."
_damage_immunities_: "poison"
_saving_throws_: "Con +11, Int +9, Wis +6, Cha +7"
_speed_: "20 ft., fly 30 ft."
_hit points_: "184 (16d10+96)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 21 (+5) | 10 (0) | 22 (+6) | 19 (+4) | 12 (+1) | 15 (+2) |

___Magic Resistance.___ The nalfeshnee has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The nalfeshnee uses Horror Nimbus if it can.  It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 32 (5d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 15 (3d6 + 5) slashing damage.

___Horror Nimbus (Recharge 5-6).___ The nalfeshnee magically emits scintillating, multicolored light. Each creature within 15 feet of the nalfeshnee that can see the light must succeed on a DC 15 Wisdom saving throw or be frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the nalfeshnee's Horror Nimbus for the next 24 hours.

___Teleport.___ The nalfeshnee magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.

___Variant: Summon Demon (1/Day).___ The demon chooses what to summon and attempts a magical summoning.

A nalfeshnee has a 50 percent chance of summoning 1d4 vrocks, 1d3 hezrous, 1d2 glabrezus, or one nalfeshnee.

A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.