"Satyr";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Perception +2, Performance +6, Stealth +5"
_speed_: "40 ft."
_hit points_: "31 (7d8)"
_armor class_: "14 (leather armor)"
_stats_: | 12 (+1) | 16 (+3) | 11 (0) | 12 (+1) | 10 (0) | 14 (+2) |

___Magic Resistance.___ The satyr has advantage on saving throws against spells and other magical effects.

**Actions**

___Ram.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) bludgeoning damage.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1 d6 + 3) piercing damage.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Variant: Panpipes.___ Gentle Lullaby. The creature falls asleep and is unconscious for 1 minute. The effect ends if the creature takes damage or if someone takes an action to shake the creature awake.