"Duergar Keeper of the Flame";;;_size_: Medium humanoid (dwarf)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft."
_speed_: "25 ft."
_hit points_: "26 (4d8+8)"
_armor class_: "16 (scale mail, shield)"
_damage_resistances_: "poison"
_stats_: | 14 (+2) | 11 (0) | 14 (+2) | 11 (0) | 10 (0) | 9 (-1) |

___Innate Spellcasting (Psionics).___ The Keeper of the Flame's innate spellcasting ability is Wisdom (spell save DC 12.) It can innately cast the following spells, requiring no components:

* At will: _friends, message_

* 1/day: _command_

___Spellcasting.___ The Keeper of the Flame is a 3rd-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). The Keeper of the Flame has the following cleric spells prepared:

* Cantrips (at will): _guidance, mending, sacred flame_

* 1st level (4 slots): _bane, inflict wounds, shield of faith_

* 2nd level (2 slots): _enhance ability, spiritual weapon _(hammer)

___Duergar Resilience.___ The duergar has advantage on saving throws against poison, spells, and illusions, as well as to resist being charmed or paralyzed.

___Sunlight Sensitivity.___ While in sunlight, the duergar has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Enlarge (Recharges after a Short or Long Rest).___ For 1 minute, the duergar magically increases in size, along with anything it is wearing or carrying. While enlarged, the duergar is Large, doubles its damage dice on Strength-based weapon attacks (included in the attacks), and makes Strength checks and Strength saving throws with advantage. If the duergar lacks the room to become Large, it attains the maximum size possible in the space available.

___War Pick.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage, or 11 (2d8 + 2) piercing damage while enlarged.

___Javelin.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage, or 9 (2d6 + 2) piercing damage while enlarged.

___Invisibility (Recharges after a Short or Long Rest).___ The duergar magically turns invisible until it attacks, casts a spell, or uses its Enlarge, or until its concentration is broken, up to 1 hour (as if concentrating on a spell). Any equipment the duergar wears or carries is invisible with it .
