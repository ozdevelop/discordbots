"Lion";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_skills_: "Perception +3, Stealth +6"
_speed_: "50 ft."
_hit points_: "26 (4d10+4)"
_armor class_: "12"
_stats_: | 17 (+3) | 15 (+2) | 13 (+1) | 3 (-4) | 12 (+1) | 8 (-1) |

___Keen Smell.___ The lion has advantage on Wisdom (Perception) checks that rely on smell.

___Pack Tactics.___ The lion has advantage on an attack roll against a creature if at least one of the lion's allies is within 5 ft. of the creature and the ally isn't incapacitated.

___Pounce.___ If the lion moves at least 20 ft. straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is prone, the lion can make one bite attack against it as a bonus action.

___Running Leap.___ With a 10-foot running start, the lion can long jump up to 25 ft..

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.