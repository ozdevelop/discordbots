"Eater Of Dust (Yakat-Shi)";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "9 (5000 XP)"
_languages_: "understands Abyssal, Common, Infernal, Void Speech, but cannot speak; telepathy 100 ft."
_skills_: "Athletics +9, Intimidate +7, Perception +6"
_senses_: "blindsight 60 ft, passive Perception 16"
_saving_throws_: "Str +9, Con +9, Cha +7"
_damage_immunities_: "bludgeoning, piercing, poison and slashing from nonmagical weapons"
_damage_resistances_: "acid, cold"
_condition_immunities_: "blindness, lightning, poisoned"
_speed_: "30 ft."
_hit points_: "114 (12d8 + 60)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 20 (+5) | 10 (+0) | 15 (+2) | 17 (+3) |

___Innate Spellcasting.___ The eater of dust's innate spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

3/day each: freedom of movement, inflict wounds, true strike

1/day each: cure wounds (as 3rd level), magic weapon (as 6th level), misty step

___Regeneration.___ The eater of dust regains 5 hit points at the start of its turn. If it takes fire damage, this trait does not function at the start of its next turn. The eater of dust dies only if it starts its turn with 0 hit points and does not regenerate.

___Unending Hunger.___ An eater of dust can devour any substance with its mawblade, regardless of composition, and never get full. It can even gain nourishment from eating dust or soil (hence the name given to the race by various fiends). If an eater of dust's mawblade is ever stolen, lost, or destroyed, it slowly starves to death.

___Weapon Bond.___ A mawblade is part of the eater of dust. It can strike any creature as if it were magically enchanted and made of silver, iron, or other materials required to overcome immunities or resistances. An eater of dust always knows the location of its mawblade as if using the locate creature spell.

**Actions**

___Multiattack.___ The eater of dust makes two mawblade attacks, or makes one mawblade attack and casts inflict wounds.

___Mawblade.___ Melee Weapon Attack: +9 to hit, one target. Hit: 19 (4d6 + 5) piercing damage, and the target must make a successful DC 17 Constitution saving throw or gain one level of exhaustion.

