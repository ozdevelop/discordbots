"Fraughashar";;;_size_: Small fey
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Sylvan"
_skills_: "Stealth +4"
_senses_: ", passive Perception 10"
_damage_immunities_: "cold"
_speed_: "25 ft."
_hit points_: "18 (4d6 + 4)"
_armor class_: "15 (leather armor, shield)"
_stats_: | 8 (-1) | 14 (+2) | 12 (+1) | 10 (+0) | 11 (+0) | 7 (-2) |

___Frost Walker.___ The fraughashar's speed is unimpeded by rocky, snowy, or icy terrain. It never needs to make Dexterity checks to move or avoid falling prone because of icy or snowy ground.

**Actions**

___Multiattack.___ The fraughashar makes one bite attack and one dagger attack.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Sling.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 4 (1d4 + 2) bludgeoning damage.

