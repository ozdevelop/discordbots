"Haloed Reaper";;;_size_: Large celestial
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Acrobatics +9, Perception +10"
_senses_: "darkvision 120ft., passive Perception 19"
_saving_throws_: "Con +9, Wis +9, Cha +8"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhausted, frightened"
_speed_: "30 ft., fly 90 ft."
_hit points_: "178 (17d10 + 85)"
_armor class_: "18 (natural armor)"
_stats_: | 24 (+7) | 18 (+4) | 21 (+5) | 19 (+4) | 20 (+5) | 19 (+4) |

___Blinding Blades.___ The reaper's weapon attacks are
magical. When the reaper hits with any weapon, the
weapon deals an extra 3d8 necrotic damage and if the
target is a creature it must succeed on a DC 15
Constitution saving throw or become blinded until the
reaper's next turn (included in the attack).

___Innate Spellcasting.___ The reaper's spellcasting ability is
Charisma (spell save DC 16). The seraph can innately
cast the following spells, requiring only verbal
components.

* At will: _detect evil and good, hunter's mark _(at 5th level)_, invisibility _(self only)

* 2/day each: _destructive wave _(necrotic only), _dimension door_

* 1/day each: _disintegrate, flesh to stone_

___Magic Resistance.___ The reaper has advantage on saving
throws against spells and other magical effects.

**Actions**

___Multiattack.___ The reaper makes two melee attacks, only
one of which can be a decaying touch.

___Grand Scythe.___ Melee Weapon Attack: +12 to hit, reach
10 ft., one target. Hit: 19 (2d12 + 7) slashing damage
plus 13 (3d8) necrotic damage and if the target is a
creature it must succeed on a DC 18 Constitution
saving throw or become blinded until the reaper's next
turn

___Cleaving Swipe.___ The reaper spins its scythe in an arcing
slash. Make an attack against each enemy within reach
of the reaper's grand scythe, making a separate attack
roll against each target.

___Reap.___ Melee Weapon Attack: +12 to hit, reach 10 ft.,
one target. Hit: 40 necrotic damage. If the target is a
creature that has fewer than 20 hit points after this
attack, it must succeed on a DC 15 Constitution saving
throw or immediately die. If the creature dies as a
result of this attack, the reaper regains 40 hit points.

___Decaying Touch (3/Day).___ Melee Spell Attack: +9 to hit,
reach 5 ft., one target. Hit: 49 (10d8 + 4) necrotic
damage.

___Binding Halo (Recharge 5-6).___ The reaper extends its
scythe and launches a black halo at two creatures it can
see within 120 feet. These creatures must succeed on
a DC 18 Dexterity saving throw or become restrained.
A restrained creature takes 18 (4d8) necrotic damage
at the start of its turn and can make a DC 18 Strength
saving throw at the end of each of its turns, ending the
effect on itself on a success.
