"Troblin";;;_size_: Medium giant
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Giant"
_senses_: "darkvision 60 ft., passive Perception 9"
_speed_: "30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 12 (+1) | 15 (+2) | 8 (-1) | 9 (-1) | 7 (-2) |

___Mutations.___ Each troblin has 1d4 random mutations brought about by its bizarre
regeneration (duplicate mutations stack, as detailed in the mutation).
Increase the troblin’s Challenge Rating to 2 (450 XP) unless otherwise
noted, and roll on the table below for each mutation.
Should a troblin be slain but not prevented from regenerating back
to life, it always gains 1 mutation, and has a 35% chance of gaining an
additional 1d4 more mutations.

* _1-2_ ___Dual Forearm.___ The troblin’s claw attack deals
1d8 damage instead of 1d6. If the troblin
already possesses this mutation, increase
the damage die size an additional time. In
addition, it can wield two-handed weapons
with that arm alone.
* _3-4_ ___Oversized Jaw.___ The troblin’s bite attack deals
2d4 damage instead of 1d4. If the troblin already
possesses this mutation, add an additional 1d4
to the damage for each stacking mutation.
* _5-6_ ___Massive Scarring.___ Increase the troblin’s Armor
Class by 1. This mutation can stack.
* _7_ ___Increased Muscle Mass.___ Each of the troblin’s
melee weapon attacks deals 1d4 additional
damage of the same type. Each time this
mutation is rolled, increase the bonus by one die
step (1d4 to 1d6, 1d6 to 1d8, etc.).
* _8_ ___Redundant Vital Organs.___ Once per short or long
rest, if the troblin takes 14 damage or less that
would reduce it to 0 hit points, it is reduced to 1
hit point instead. Each time this mutation is rolled,
increase the number of times the troblin can use
this ability by 1.
* _9_ ___Shortened Tendons.___ The troblins has proficiency
in the Acrobatics skill, and has advantage
on checks and saving throws against being
grappled.
* _10_ ___Extra Arm.___ The troblin grows an additional arm. It
can make one additional claw when it takes the
attack action. Each time this mutation is rolled,
the number of claw attacks increases.
* _11_ ___Hollow Bones.___ The troblin has vulnerability to
bludgeoning damage, but gains proficiency in
the Stealth skill and has a movement speed of 35
feet. If this mutation is rolled a second time, the
troblin doubles its proficiency bonus when rolling
Dexterity (Stealth) checks. If this mutation is rolled
a third time, reroll.
* _12_ ___Large Eyes.___ The troblin doubles the range of its
darkvision, to a maximum of 120 feet.
* _13_ ___Two Heads.___ The troblin has proficiency in the
Perception skill, and advantage on saving
throws against being blinded. If the troblin
already possesses this mutation, add a vestigial
face to the troblin’s body. This grants the troblin
advantage on Wisdom (Perception) checks that
rely on sight or smell, in addition to the other
benefits. Reroll if this mutation is rolled a third
time.
* _14-19_ ___No Mutation.___
* _20_ ___Greater Regeneration.___ The troblin’s regeneration
trait regains 10 hit points instead of 5 hit points.
For each 10 hit points above the original 5 this
trait regains for the troblin, increase the troblin’s
Challenge Rating by 1.

___Regeneration.___ The troblin regains 5 hit points at the start of its turn. If
the troblin takes acid or fire damage, this trait doesn’t function at the start
of the troblin’s next turn. The troblin dies only if it starts its turn with 0 hit
points and doesn’t regenerate.

**Actions**

___Multiattack.___ The troblin makes one greatclub attack and one bite attack,
or one claws attack and one bite attack.

___Greatclub.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit:
6 (1d8 + 2) bludgeoning damage.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5
(1d6 + 2) slashing damage.
