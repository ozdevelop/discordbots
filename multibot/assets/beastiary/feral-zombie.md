"Feral Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "22 (3d8 + 9)"
_armor class_: "12"
_stats_: | 12 (+1) | 14 (+2) | 16 (+3) | 5 (-3) | 7 (-2) | 6 (-2) |

___Undead Fortitude.___ If damage reduces the zombie to 0
hit points, it must make a Constitution saving throw
with a DC of 5 + the damage taken, unless the damage
is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.

___Aggressive.___ As a bonus action, the zombie can move up
to half its speed toward a hostile creature it can see.

**Actions**

___Ravage.___ Melee Weapon Attack: +4 to hit, reach 5ft., one
target. Hit: 6 (1d8 + 2) slashing damage.
