"Drow Inquisitor";;;_page_number_: 184
_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "14 (11500 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 20"
_skills_: "Insight +10, Perception +10, Religion +8, Stealth +7"
_saving_throws_: "Con +7, Wis +10, Cha +10"
_speed_: "30 ft."
_hit points_: "143  (22d8 + 44)"
_armor class_: "16 (breastplate)"
_condition_immunities_: "frightened"
_stats_: | 11 (0) | 15 (+2) | 14 (+2) | 16 (+3) | 21 (+5) | 20 (+5) |

___Discern Lie.___ The drow knows when she hears a creature speak a lie in a language she knows.

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's innate spellcasting ability is Charisma (spell save DC 18). She can innately cast the following spells, requiring no material components:

* At will: _dancing lights, detect magic_

* 1/day each: _clairvoyance, darkness, detect thoughts, dispel magic, faerie fire , levitate _(self only)_, suggestion_

___Magic Resistance.___ The drow has advantage on saving throws against spells and other magical effects.

___Spellcasting.___ The drow is a 12th-level spellcaster. Her spellcasting ability is Wisdom (spell save DC 18, +10 to hit with spell attacks). She has the following cleric spells prepared:

* Cantrips (at will): _guidance, message, poison spray, resistance, thaumaturgy_

* 1st level (4 slots): _bane, cure wounds, inflict wounds_

* 2nd level (3 slots): _blindness/deafness, silence, spiritual weapon_

* 3rd level (3 slots): _bestow curse, dispel magic, magic circle_

* 4th level (3 slots): _banishment, divination, freedom of movement_

* 5th level (2 slots): _contagion, dispel evil and good, insect plague_

* 6th level (1 slot): _harm, true seeing_

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The drow makes three death lance attacks.

___Death Lance___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) piercing damage plus 18 (4d8) necrotic damage. The target's hit point maximum is reduced by an amount equal to the necrotic damage it takes. This reduction lasts until the target finishes a long rest. The target dies if its hit point maximum is reduced to 0.
