"Alkilith";;;_page_number_: 130
_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "11 (7200 XP)"
_languages_: "understands Abyssal but can't speak"
_senses_: "darkvision 120 ft., passive Perception 10"
_skills_: "Stealth +8"
_damage_immunities_: "poison"
_saving_throws_: "Dex +8, Con +10"
_speed_: "40 ft."
_hit points_: "157  (15d8 + 90)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "acid, cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 12 (+1) | 19 (+4) | 22 (+6) | 6 (-2) | 11 (0) | 7 (-1) |

___Amorphous.___ The alkilith can move through a space as narrow as 1 inch wide without squeezing.

___False Appearance.___ While the alkilith is motionless, it is indistinguishable from an ordinary slime or fungus.

___Foment Madness.___ Any creature that isn't a demon that starts its turn within 30 feet of the alkilith must succeed on a DC 18 Wisdom saving throw, or it hears a faint buzzing in its head for a moment and has disadvantage on its next attack roll, saving throw, or ability check.
If the saving throw against Foment Madness fails by 5 or more, the creature is instead subjected to the confusion spell for 1 minute (no concentration required by the alkilith). While under the effect of that confusion, the creature is immune to Foment Madness.

___Magic Resistance.___ The alkilith has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The alkilith makes three tentacle attacks.

___Tentacle___ Melee Weapon Attack: +8 to hit, reach 15 ft., one target. Hit: 18 (4d6 + 4) acid damage.