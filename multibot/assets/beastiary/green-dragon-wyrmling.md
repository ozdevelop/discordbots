"Green Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +3"
_damage_immunities_: "poison"
_saving_throws_: "Dex +3, Con +3, Wis +2, Cha +3"
_speed_: "30 ft., fly 60 ft., swim 30 ft."
_hit points_: "38 (7d8+7)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 15 (+2) | 12 (+1) | 13 (+1) | 14 (+2) | 11 (0) | 13 (+1) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) piercing damage plus 3 (1d6) poison damage.

___Poison Breath (Recharge 5-6).___ The dragon exhales poisonous gas in a 15-foot cone. Each creature in that area must make a DC 11 Constitution saving throw, taking 21 (6d6) poison damage on a failed save, or half as much damage on a successful one.