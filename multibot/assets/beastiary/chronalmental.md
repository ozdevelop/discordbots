"Chronalmental";;;_size_: Large elemental
_alignment_: unaligned
_challenge_: "8 (3900 XP)"
_languages_: "Celestial, Infernal"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "30 ft."
_hit points_: "152 (16d10 + 64)"
_armor class_: "17 (natural armor)"
_stats_: | 1 (-5) | 20 (+5) | 19 (+4) | 9 (-1) | 13 (+1) | 6 (-2) |

___Temporal Body.___ When a chronalmental is subjected to a slow spell, haste spell, or similar effect, it automatically succeeds on the saving throw and regains 13 (3d8) hit points.

**Actions**

___Multiattack.___ The chronalmental makes 1d4 + 1 slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 16 (2d10 + 5) bludgeoning damage.

___Steal Time (1/Day).___ The chronalmental targets one creature it can see within 30 feet of it. The targeted creature must make a DC 16 Wisdom saving throw. On a failed saving throw, the chronalmental draws some of the creature's time into itself and gains +10 to its position in initiative order. In addition, the target's speed is reduced by half, it can't take reactions, and it can take either an action or a bonus action on its turn, but not both. While it is stealing time, the chronalmental's speed increases by 30 feet, and when it takes the multiattack action, it can make an additional slam attack. The targeted creature can repeat the saving throw at the end of each of its turns, ending the effect on a success.

___Displace (Recharge 5-6).___ The chronalmental targets one creature it can see within 30 feet of it. The target must succeed on a DC 16 Wisdom saving throw or be magically shunted outside of time. The creature disappears for 1 minute. As an action, the displaced creature can repeat the saving throw. On a success, the target returns to its previously occupied space, or the nearest unoccupied space.

**Reactions**

___Step Between Seconds (Recharge 4-6).___ When a creature the chronalmental can see moves within 5 feet of it, the chronalmental can shift itself to a place it occupied in the past, teleporting up to 60 feet to an unoccupied space, along with any equipment it is wearing or carrying.

