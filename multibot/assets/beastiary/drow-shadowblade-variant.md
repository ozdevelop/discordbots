"Drow Shadowblade (variant)";;;_page_number_: 187
_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "11 (7200 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 16"
_skills_: "Perception +6, Stealth +9"
_saving_throws_: "Dex +9, Con +7, Wis +6"
_speed_: "30 ft."
_hit points_: "150  (20d8 + 60)"
_armor class_: "17 (studded leather)"
_stats_: | 14 (+2) | 21 (+5) | 16 (+3) | 12 (+1) | 14 (+2) | 13 (+1) |

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire , levitate _(self only)

___Shadow Step.___ While in dim light or darkness, the drow can teleport as a bonus action up to 60 feet to an unoccupied space it can see that is also in dim light or darkness. It then has advantage on the first melee attack it makes before the end of the turn.

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The drow makes two attacks with its shadow sword. If either attack hits and the target is within 10 feet of a 5-foot cube of darkness created by the shadow sword on a previous turn, the drow can dismiss that darkness and cause the target to take 21 (6d6) necrotic damage. The drow can dismiss darkness in this way no more than once per turn.

___Shadow Sword___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) piercing damage plus 10 (3d6) necrotic damage and 10 (3d6) poison damage. The drow can then fill an unoccupied 5-foot cube within 5 feet of the target with magical darkness, which remains for 1 minute.

___Hand Crossbow___ Ranged Weapon Attack: +9 to hit, range 30/120 ft., one target. Hit: 8 (1d6 + 5) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour. If the saving throw fails by 5 or more, the target is also unconscious while poisoned in this way. The target regains consciousness if it takes damage or if another creature takes an action to shake it.

___Summon Shadow Demon (1/Day)___ The drow attempts to magically summon a shadow demon with a 50 percent chance of success. If the attempt fails, the drow takes 5 (1d10) psychic damage. Otherwise, the summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 10 minutes, until it or its summoner dies, or until its summoner dismisses it as an action.
