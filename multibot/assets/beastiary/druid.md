"Druid";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "2 (450 XP)"
_languages_: "Druidic plus any two languages"
_skills_: "Medicine +4, Nature +3, Perception +4"
_speed_: "30 ft."
_hit points_: "27 (5d8+5)"
_armor class_: "11 (16 with barkskin)"
_stats_: | 10 (0) | 12 (+1) | 13 (+1) | 12 (+1) | 15 (+2) | 11 (0) |

___Spellcasting.___ The druid is a 4th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). It has the following druid spells prepared:

* Cantrips (at will): _druidcraft, produce flame, shillelagh_

* 1st level (4 slots): _entangle, longstrider, speak with animals, thunderwave_

* 2nd level (3 slots): _animal messenger, barkskin_

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +2 to hit (+4 to hit with shillelagh), reach 5 ft., one target. Hit: 3 (1d6) bludgeoning damage, or 6 (1d8 + 2) bludgeoning damage with shillelagh or if wielded with two hands.
