"Small Water Elemental";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Aquan"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "acid"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "30 ft., swim 50 ft."
_hit points_: "22 (3d10 + 6)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 14 (+2) | 4 (-3) | 10 (+0) | 8 (-1) |

___Water Form.___ The elemental can enter a hostile
creature's space and stop there. It can move
through a space as narrow as 1 inch wide without
squeezing.

___Freeze.___ If the elemental takes cold damage, it
partially freezes; its speed is reduced by 20 feet
until the end of its next turn.

**Actions**

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 7 (1d8 + 3) bludgeoning damage.

___Water Blast.___ Ranged Spell Attack: +2 to hit, range
30/120 ft., one target. Hit: 4 (1d8) bludgeoning
damage plus 2 (1d4) cold damage.
