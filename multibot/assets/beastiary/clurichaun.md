"Clurichaun";;;_size_: Tiny fey
_alignment_: chaotic neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Perception +1, Stealth +3"
_senses_: "darkvision 60ft., passive Perception 11"
_saving_throws_: "Con +5"
_condition_immunities_: "frightened, poisoned"
_speed_: "30 ft."
_hit points_: "22 (4d4 + 12)"
_armor class_: "14"
_stats_: | 13 (+1) | 12 (+1) | 16 (+3) | 10 (+0) | 8 (-1) | 16 (+3) |

___Clurichaun's Luck.___ Clurichauns add both their Dexterity and Charisma modifiers to their Armor Class.

___Innate Spellcasting.___ The clurichaun's innate spellcasting ability is Charisma (spell save DC 13). The clurichaun can cast the following spells, requiring only alcohol as a component.

* At will: _friends, mending, minor illusion, purify food and drink, vicious mockery_

* 1/day each: _blur, calm emotions, heroism, sleep, suggestion_

___Magic Resistance.___ The clurichaun has advantage on saving throws against spells and other magical effects.

**Actions**

___Unarmed Strike.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 2 (1 + 1) bludgeoning damage.

___Improvised Weapon.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one creature. Hit: 3 (1d4 + 1) bludgeoning, piercing, or slashing damage, depending on weapon.

