"Vampire Warlock - Variant";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "13 (10000 XP)"
_languages_: "the languages it knew in life"
_skills_: "Perception +7, Stealth +9"
_senses_: "darkvision 120 ft., passive Perception 17"
_saving_throws_: "Dex +9, Wis +7, Cha +9"
_damage_resistances_: "necrotic, bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft."
_hit points_: "144"
_armor class_: "16 (natural armor)"
_stats_: | 18 (+4) | 18 (+4) | 18 (+4) | 17 (+3) | 15 (+2) | 18 (+4) |

___Innate Spellcasting.___ The vampire's spellcasting ability is Charisma (spell save DC 17). It can innately cast the following spells, requiring no material components.

At will: darkness, dominate person, invisibility, misty step

1/day each: arms of hadar, disguise self, dissonant whispers, detect thoughts, hold monster

**Actions**

___Bloody Arms.___ The vampire warlock saturates itself in its own blood, causing 20 poison damage to itself. For 1 minute, its armor class increases to 20 and its unarmed strike and bite attacks do an additional 7 (2d6) poison damage.

___Call the Blood.___ The vampire warlock targets one humanoid it can see within 60 feet. The target must be injured (has fewer than its normal maximum hit points). The target's blood is drawn out of the body and streams through the air to the vampire warlock. The target takes 25 (6d6 + 4) necrotic damage and its hit point maximum is reduced by an equal amount until the target finishes a long rest; a successful DC 17 Constitution saving throw prevents both effects. The vampire warlock regains hit points equal to half the damage dealt. The target dies if this effect reduces its hit point maximum to 0.

___Blood Puppet.___ The vampire warlock targets one humanoid it can see within 30 feet. The target must succeed on a DC 17 Wisdom saving throw or be dominated by the vampire warlock as if it were the target of a dominate person spell. The target repeats the saving throw each time the vampire warlock or the vampire's companions do anything harmful to it, ending the effect on itself on a success. Otherwise, the effect lasts 24 hours or until the vampire warlock is destroyed, is on a different plane of existence than the target, or uses a bonus action to end the effect; the vampire warlock doesn't need to concentrate on maintaining the effect.

___Children of Hell (1/Day).___ The vampire warlock magically calls 2d4 imps or 1 shadow. The called creatures arrive in 1d4 rounds, acting as allies of the vampire warlock and obeying its spoken commands, and remain for 1 hour, until the vampire warlock dies, or until the vampire warlock dismisses them as a bonus action.

**Legendary** Actions

___Misty Step.___ The vampire warlock uses misty step.

___Unarmed Strike.___ The vampire warlock makes one unarmed strike.

___Call the Blood (Costs 2 Actions)..___ The vampire warlock uses call the blood.

