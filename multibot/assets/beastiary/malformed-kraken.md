"Malformed Kraken";;;_size_: Huge monstrosity
_alignment_: chaotic evil
_challenge_: "10 (5,900 XP)"
_languages_: "understands Common but can't speak; telepathy 60 ft."
_senses_: "truesight 60 ft."
_damage_immunities_: "lightning"
_saving_throws_: "Str +11, Con +9, Int +4, Wis +6, Cha +6"
_speed_: "20 ft., swim 40 ft."
_hit points_: "172 (15d12+75)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "frightened, paralyzed"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 25 (+7) | 11 (0) | 20 (+5) | 11 (0) | 15 (+2) | 15 (+2) |

___Source.___ tales from the yawning portal,  page 239

___Amphibious.___ The kraken can breathe air and water.

___Siege Monster.___ The kraken deals double damage to objects and structures.

**Actions**

___Multiattack.___ The kraken makes three tentacle attacks. One of them can be replaced with a bite attack, and any of them can be replaced with Fling.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 16 (2d8 + 7) piercing damage.

___Tentacle.___ Melee Weapon Attack: +11 to hit, reach 20 ft., one target. Hit: 14 (2d6 + 7) bludgeoning damage, and the target is grappled (escape DC 16). Until this grapple ends, the target is restrained. The kraken has ten tentacles, each of which can grapple one target.

___Fling.___ One Medium or smaller object held or creature grappled by the kraken's tentacles is thrown up to 60 feet in a random direction and knocked prone. If a thrown target strikes a solid surface, the target takes 3 (1d6) bludgeoning damage for every 10 feet it was thrown. If the target is thrown at another creature, that creature must succeed on a DC 16 Dexterity saving throw or take the same damage and be knocked prone.

___Lightning Storm.___ The kraken creates three bolts of lightning, each of which can strike a target the kraken can see within 150 feet ofit. A target must make a DC 16 Dexterity saving throw, taking 16 (3d10) lightning damage on a failed save, or half as much damage on a successful one.