"Death Kiss";;;_size_: Large aberration
_alignment_: neutral evil
_challenge_: "10 (5,900 XP)"
_languages_: "Deep Speech, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +5"
_damage_immunities_: "lightning"
_saving_throws_: "Con +8, Wis +5"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "161 (17d10+68)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "prone"
_stats_: | 18 (+4) | 14 (+2) | 18 (+4) | 10 (0) | 12 (+1) | 10 (0) |

___Lightning Blood.___ A creature within 5 feet of the death kiss takes 5 (1d10) lightning damage whenever it hits the death kiss with a melee attack that deals piercing or slashing damage.

**Actions**

___Multiattack.___ The death kiss makes three tentacle attacks. Up to three of these attacks can be replaced by Blood Drain, one replacement per tentacle grappling a creature

___Tentacle.___ Melee Weapon Attack: +8 to hit, reach 20 ft., one target. Hit: 14 (3d6+4) piercing damage, and the target is grappled (escape DC 14) if it is a Huge or smaller creature. Until this grapple ends, the target is restrained, and the death kiss can't use the same tentacle on another target. The death kiss has ten tentacles.

___Blood Drain.___ One creature grappled by a tentacle of the death kiss must make a DC 16 Constitution saving throw. On a failed save, the target takes 22 (4d10) lightning damage, and the death kiss regains half as many hit points.