"Gypsosphinx";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "14 (11500 XP)"
_languages_: "Abyssal, Common, Darakhul, Sphinx"
_skills_: "Arcana +9, History +9, Perception +9, Religion +9"
_senses_: "truesight 90 ft., passive Perception 19"
_damage_immunities_: "psychic, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 70 ft."
_hit points_: "171 (18d10 + 72)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 18 (+4) | 18 (+4) | 18 (+4) | 18 (+4) |

___Inscrutable.___ The sphinx is immune to any effect that would sense its emotions or read its thoughts, as well as any divination spell that it refuses. Wisdom (Insight) checks made to ascertain the sphinx's intentions or sincerity have disadvantage.

___Magic Weapons.___ The sphinx's weapon attacks are magical.

___Mystic Sight.___ A gypsosphinx sees death coming and can foretell the manner of a person's death. This ability does not come with any urge to share that information. Gypsosphinxes are notorious for hinting, teasing, and even lying about a creature's death ("If we fight, I will kill you and eat your heart. I have seen it," is a favorite bluff).

___Spellcasting.___ The sphinx is a 9th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 17, +9 to hit with spell attacks). It requires no material components to cast its spells. The sphinx has the following wizard spells prepared:

Cantrips: (at will): mage hand, mending, minor illusion, poison spray

1st level (4 slots): comprehend languages, detect magic, identify

2nd level (3 slots): blur, darkness, locate object

3rd level (3 slots): dispel magic, glyph of warding, major image

4th level (3 slots): blight, greater invisibility

5th level (1 slot): cloudkill

**Actions**

___Multiattack.___ The sphinx makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 21 (3d10 + 5) piercing damage.

___Claws.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 32 (6d8 + 5) slashing damage.

___Rake.___ If the sphinx succeeds with both claw attacks, it automatically follows up with a rake attack. If the target fails a DC 17 Dexterity check, it is knocked prone and takes 14 (2d8 + 5) slashing damage.

**Legendary** Actions

___The sphinx can take 3 legendary actions, choosing from the options below.___ Only one option can be used at a time and only at the end of another creature's turn. It regains spent legendary actions at the start of its turn.

___Bite Attack.___ The sphinx makes one bite attack.

___Teleport (Costs 2 Actions).___ The sphinx magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.

___Cast a Spell (Costs 3 Actions).___ The sphinx casts a spell from its list of prepared spells, using a spell slot as normal.

