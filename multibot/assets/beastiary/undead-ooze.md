"Undead Ooze";;;_size_: Huge ooze (undead)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft., passive Perception 11"
_damage_immunities_: "cold, necrotic, poison"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, poisoned, prone"
_speed_: "20 ft., climb 20 ft."
_hit points_: "67 (9d12 + 9)"
_armor class_: "5"
_stats_: | 12 (+1) | 1 (-5) | 13 (+1) | 2 (-4) | 12 (+1) | 10 (+0) |

___Amorphous.___ The ooze can move through a space as narrow as one inch
without squeezing.

___Ooze Mass.___ The ooze takes up most of its space. Other creatures can
enter the space, but a creature that does so is subjected to the ooze’s engulf
and has disadvantage on the saving throw.

Creatures inside the ooze can be seen but have total cover.

A creature within 5 feet of the ooze can take an action to pull a creature
or object out of the ooze. Doing so requires a successful DC 15 Strength
check, and the creature making the attempt takes 9 (2d8) necrotic damage.
If a skeleton is pulled out, it animates as if the skeletons ability was used.

___Undeath.___ An undead ooze doesn’t need air, food, drink, or sleep.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +4 to hit, reach 10 ft., one target.
Hit: 14 (3d8 + 1) bludgeoning damage and 9 (2d8) necrotic damage.

___Engulf.___ The ooze moves up to its speed. While doing so, it can enter
Large or smaller creatures’ spaces. Whenever the ooze enters a creature’s
space, the creature must make a DC 15 Dexterity saving throw.
On a successful save, the creature can choose to be pushed 5 feet back
or to the side of the ooze. A creature that chooses not to be pushed suffers the consequences of a failed saving throw.

On a failed save, the ooze enters the creature’s space, and the creature
takes 18 (4d8) necrotic damage and is engulfed. The engulfed creature
can’t breathe, is restrained, and takes 27 (6d8) necrotic damage at the start
of each of the ooze’s turns. When the ooze moves, the engulfed creature
moves with it.

An engulfed creature can try to escape by taking an action to make a
DC 15 Strength check. On a success, the creature escapes and enters a
space of its choice within 5 feet of the ooze.

___Skeletons.___ An undead ooze can expel 1d6 skeletons from its mass, each
appearing within 5 feet of the ooze. Skeletons can act in the round they
are expelled. Slain skeletons are engulfed by the undead ooze and can
be reanimated and expelled again in 1d2 hours. An undead ooze’s form
holds up to 10 skeletons of Medium size. These skeletons are included in
the determination of the undead ooze’s CR and experience points. They
remain active even if the ooze is killed. Some undead oozes have unusual
or larger skeletons inside of them.
