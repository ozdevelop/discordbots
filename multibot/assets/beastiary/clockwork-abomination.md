"Clockwork Abomination";;;_size_: Large construct
_alignment_: lawful evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Infernal"
_skills_: "Athletics +9, Perception +4, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +4 Con +7"
_damage_immunities_: "poison"
_damage_resistances_: "acid, cold, fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., climb 30 ft."
_hit points_: "76 (8d10 + 32)"
_armor class_: "16 (natural armor)"
_stats_: | 21 (+5) | 12 (+1) | 18 (+4) | 10 (+0) | 10 (+0) | 12 (+1) |

___Additional Legs.___ Four legs allow the clockwork abomination to climb at a speed equal to its base speed and to ignore difficult terrain.

___Piston Reach.___ The abomination's melee attacks have a deceptively long reach thanks to the pistons powering them.

___Immutable Form.___ The clockwork abomination is immune to any spell or effect that would alter its form.

___Infernal Power Source.___ When a clockwork abomination falls to 0 hp, its infernal battery explodes. Creatures within 10 feet of the clockwork abomination take 14 (4d6) fire damage, or half damage with a successful DC 14 Dexterity saving throw.

**Actions**

___Multiattack.___ The clockwork abomination makes one bite attack and one slam attack.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) piercing damage.

___Slam.___ Melee Weapon Attack: +9 to hit, reach 15 ft., one target. Hit: 13 (2d6 + 6) bludgeoning damage.

___Breath Weapon (Recharge 5-6).___ The clockwork abomination's Infernal Power Source allows it to breathe fire in a 20-foot cone. Targets in this cone take 22 (4d10) fire damage, or half damage with a successful DC 14 Dexterity saving throw.

