"Death Watch Beetle";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "necrotic"
_speed_: "30 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "14 (natural armor)"
_stats_: | 20 (+1) | 10 (+2) | 16 (+1) | 1 (-5) | 10 (+0) | 9 (-4) |

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12
(2d6 + 5) piercing damage.

___Death Rattle (Recharge 5–6).___ The death watch beetle emits a clicking
noise with a resonance frequency that affects creatures within 30 feet of
it. Creatures in this area must make a DC 13 Constitution saving throw,
taking 10 (3d6) thunder damage on a failed save, or half as much damage
on a successful one.
