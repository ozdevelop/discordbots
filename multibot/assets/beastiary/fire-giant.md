"Fire Giant";;;_size_: Huge giant
_alignment_: lawful evil
_challenge_: "9 (5,000 XP)"
_languages_: "Giant"
_skills_: "Athletics +11, Perception +6"
_damage_immunities_: "fire"
_saving_throws_: "Dex +3, Con +10, Cha +5"
_speed_: "30 ft."
_hit points_: "162 (13d12+78)"
_armor class_: "18 (plate)"
_stats_: | 25 (+7) | 9 (-1) | 23 (+6) | 10 (0) | 14 (+2) | 13 (+1) |

___Siege Monster.___ The giant deals double damage to objects and structures.

___Tackle.___ When the giant enters any enemy's space for the first time on a turn, the enemy must succeed on a DC 19 Strength saving throw or be knocked prone.

**Actions**

___Multiattack.___ The giant makes two greatsword attacks.

___Greatsword.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 28 (6d6 + 7) slashing damage.

___Rock.___ Ranged Weapon Attack: +11 to hit, range 60/240 ft., one target. Hit: 29 (4d10 + 7) bludgeoning damage.