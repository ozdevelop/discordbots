"Margoyle";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "4 (1,800 XP)"
_languages_: "Terran"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons not made of adamantine"
_condition_immunities_: "exhaustion, petrified, poisoned"
_speed_: "30 ft., fly 60 ft."
_hit points_: "68 (8d8 + 32)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 19 (+4) | 6 (-2) | 10 (+0) | 7 (-2) |

___False Appearance.___ While the gargoyle remains motionless, it is
indistinguishable from a weathered, inanimate statue that is covered in
fungus, lichen, and moss.

**Actions**

___Multiattack.___ The margoyle makes three attacks: one with its bite and
two with its claws; it can gore once with its horns instead of using its bite.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft.; one target. Hit: 7 (1d8 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) slashing damage.

___Gore.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7
(1d8 + 3) piercing damage.
