"Goblin";;;_size_: Small humanoid (goblinoid)
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +6"
_speed_: "30 ft."
_hit points_: "7 (2d6)"
_armor class_: "15 (leather armor, shield)"
_stats_: | 8 (-1) | 14 (+2) | 10 (0) | 10 (0) | 8 (-1) | 8 (-1) |

___Nimble Escape.___ The goblin can take the Disengage or Hide action as a bonus action on each of its turns.

**Actions**

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 2) piercing damage.