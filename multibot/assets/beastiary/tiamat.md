"Tiamat";;;_size_: Gargantuan fiend
_alignment_: chaotic evil
_challenge_: "30 (155,000 XP)"
_languages_: "Common, Draconic, Infernal"
_senses_: "darkvision 240 ft., truesight 120 ft."
_skills_: "Arcana +17, Perception +26, Religion +17"
_damage_immunities_: "acid, cold, fire, lightning, poison; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_saving_throws_: "Str +19, Dex +9, Wis +17"
_speed_: "60 ft., fly 120 ft."
_hit points_: "615 (30d20+300)"
_armor class_: "25 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, frightened, poisoned, stunned"
_stats_: | 30 (+10) | 10 (0) | 30 (+10) | 26 (+8) | 26 (+8) | 28 (+9) |

___Discorporation.___ When Tiamat drops to 0 hit points or dies, her body is destroyed but her essence travels back to her domain in the Nine Hells, and she is unable to take physical form for a time.

___Innate Spellcasting (3/Day).___ Tiamat can innately cast _divine word_ (spell save DC 26). Her spellcasting ability is Charisma.

___Legendary Resistance (5/Day).___ If Tiamat fails a saving throw, she can choose to succeed instead.

___Limited Magic Immunity.___ Unless she wishes to be affected, Tiamat is immune to spells of 6th level or lower. She has advantage on saving throws against all other spells and magical effects.

___Magic Weapons.___ Tiamat's weapon attacks are magical.

___Multiple Heads.___ Tiamat can take one reaction per turn, rather than only one per round. She also has advantage on saving throws against being knocked unconscious. If she fails a saving throw against an effect that would stun a creature, one of her unspent legendary actions is spent.

___Regeneration.___ Tiamat regains 30 hit points at the start of her turn.

**Actions**

___Multiattack.___ Tiamat can use her Frightful Presence. She then makes three attacks: two with her claws and one with her tail.

___Claw.___ Melee Weapon Attack: +19 to hit, reach 15 ft., one target. Hit: 24 (4d6 + 10) slashing damage.

___Tail.___ Melee Weapon Attack: +19 to hit, reach 25 ft., one target. Hit: 28 (4d8 + 10) piercing damage.

___Frightful Presence.___ Each creature of Tiamat's choice that is within 240 feet of Tiamat and aware of her must succeed on a DC 26 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to Tiamat's Frightful Presence for the next 24 hours.

**Legendary** Actions

Tiamat can take 5 legendary actions, choosing from the options listed. Only one legendary action option can be used at a time and only at the end of another creature's turn. Tiamat regains spent legendary actions at the start of her turn.

Tiamat's legendary action options are associated with her five dragon heads (a bite and a breath weapon for each). Once Tiamat chooses a legendary action option for one of her heads, she can't choose another one associated with that head until the start of her next turn.

___Bite.___ Melee Weapon Attack: +19 to hit, reach 20 ft., one target. Hit: 32 (4d10 + 10) slashing damage plus 14 (4d6) acid damage (black dragon head), lightning damage (blue dragon head), poison damage (green dragon head), fire damage (red dragon head), or cold damage (white dragon head).

___Black Dragon Head: Acid Breath (Costs 2 Actions).___ Tiamat breathes acid in a 120-foot line that is 10 feet wide. Each creature in that line must make a DC 27 Dexterity saving throw, taking 67 (15d8) acid damage on a failed save, or half as much damage on a successful one.

___Blue Dragon Head: Lightning Breath (Costs 2 Actions).___ Tiamat breathes lightning in a 120-foot line that is 10 feet wide. Each creature in that line must make a DC 27 Dexterity saving throw, taking 88 (16d10) lightning damage on a failed save, or half as much damage on a successful one.

___Green Dragon Head: Poison Breath (Costs 2 Actions).___ Tiamat breathes poisonous gas in a 90-foot cone. Each creature in that area must make a DC 27 Constitution saving throw, taking 77 (22d6) poison damage on a failed save, or half as much damage on a successful one.

___Red Dragon Head: Fire Breath (Costs 2 Actions).___ Tiamat breathes fire in a 90-foot cone. Each creature in that area must make a DC 27 Dexterity saving throw, taking 91 (26d6) fire damage on a failed save, or half as much damage on a successful one.

___White Dragon Head: Cold Breath (Costs 2 Actions).___ Tiamat breathes an icy blast in a 90-foot cone. Each creature in that area must make a DC 27 Dexterity saving throw, taking 72 (16d8) cold damage on a failed save, or half as much damage on a successful one.
