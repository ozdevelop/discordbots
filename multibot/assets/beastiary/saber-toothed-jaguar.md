"Saber-Toothed Jaguar";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +7"
_senses_: "passive Perception 13"
_speed_: "40 ft., climb 30 ft."
_hit points_: "76 (9d10 + 27)"
_armor class_: "13"
_stats_: | 20 (+5) | 16 (+3) | 16 (+3) | 3 (-4) | 12 (+1) | 8 (-1) |

___Keen Smell.___ The jaguar has advantage on Wisdom
(Perception) checks that rely on smell.

___Pounce.___ If the jaguar moves at least 20 feet straight toward
a creature and then hits it with a claw attack on the same turn,
that target must succeed on a DC 15 Strength saving throw or be
knocked prone. If the target is prone, the jaguar can make one bite
attack against it as a bonus action.

___Surprise Attack.___ If the sabertooth jaguar surprises a creature and hits
it with an attack during the first round of combat, the target takes an
extra 10 (3d6) damage from the attack.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 27
(4d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 12
(2d6 + 5) slashing damage.
