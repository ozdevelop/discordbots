"Phoenix Hatchling";;;_size_: Small elemental
_alignment_: lawful good
_challenge_: "1 (200 XP)"
_languages_: "Understands Common and Ignan but can't speak"
_skills_: "Perception +4"
_senses_: "passive Perception 14"
_saving_throws_: "Dex +4, Int +4, Cha +4"
_damage_immunities_: "fire"
_speed_: "10 ft., fly 60 ft."
_hit points_: "27 (5d6 + 10)"
_armor class_: "13"
_stats_: | 12 (+1) | 15 (+2) | 14 (+2) | 15 (+2) | 14 (+2) | 14 (+2) |

___Blazing Feathers.___ The feathers that cover the
phoenix's body are constantly ablaze. These flames
shed bright light in a 10-foot radius and dim light
for an additional 10 feet. Additionally, a creature
that touches the phoenix or hits it with a melee
attack while within 5 feet of it takes 2 (1d4) fire
damage.

___Flyby.___ The phoenix doesn't provoke attacks of
opportunity when it flies out of an enemy's reach.

___Keen Sight.___ The phoenix has advantage on Wisdom
(Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The phoenix makes two attacks: one
with its beak and one with its talons.

___Beak.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 4 (1d4 + 2) piercing damage plus 2
(1d4) fire damage.

___Talons.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 7 (2d4 + 2) slashing damage plus 2
(1d4) fire damage.
