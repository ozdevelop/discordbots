"Vile Barber";;;_size_: Small fey
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Goblin, Sylvan, Umbral"
_skills_: "Athletics +3, Stealth +6"
_senses_: "60 ft., passive Perception 9"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered or made of cold iron"
_condition_immunities_: "frightened"
_speed_: "30 ft."
_hit points_: "28 (8d6)"
_armor class_: "15 (leather armor)"
_stats_: | 12 (+1) | 18 (+4) | 10 (+0) | 10 (+0) | 8 (-1) | 10 (+0) |

___Invasive.___ The vile barber can enter, move through, or even remain in a hostile creature's space regardless of the creature's size, without penalty.

___Close-in Slasher.___ The vile barber has advantage on attack rolls against any creature in the same space with it.

___Inhumanly Quick.___ The vile barber can take two bonus actions on its turn, instead of one. Each bonus action must be different; it can't use the same bonus action twice in a single turn.

**Actions**

___Multiattack.___ The vile barber makes two attacks with its straight razor.

___Straight Razor.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d4 + 4) slashing damage.

___Unclean Cut.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature that is grappled by the vile barber, incapacitated, or restrained. Hit: 6 (1d4 + 4) slashing damage plus 7 (2d6) necrotic damage. The creature and all its allies who see this attack must make successful DC 15 Wisdom saving throws or become frightened for 1d4 rounds.

**Bonus** Actions

___Nimble Escape.___ The vile barber takes the Disengage or Hide action.

___Pilfer.___ The vile barber takes the Use an Object action or makes a Dexterity (Sleight of Hand) check.

___Shadow Step.___ The vile barber magically teleports from an area of dim light or darkness it currently occupies, along with any equipment it is wearing or carrying, up to 80 feet to any other area of dim light or darkness it can see. The barber then has advantage on the first melee attack it makes before the end of the turn.
