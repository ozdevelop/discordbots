"Green Bulette";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "13 (10,000 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing (gaseous form only)"
_speed_: "40 ft., fly 80 ft."
_hit points_: "121 (9d12 + 63)"
_armor class_: "17 (solid) or 20 (gaseous)"
_stats_: | 20 (+5) | 12 (+1) | 24 (+7) | 3 (-4) | 10 (+0) | 6 (-2) |

___Emerald Dust.___ If killed (whether in solid or gaseous form), the body of a green bulette appears to implode out of existence with a thump and inward rush of air. If the immediate area is searched, it is found to be lightly dusted in a fine green powder of emerald dust, which can be collected and used to create both potent poisons and poison-based magics.

___Gaseous Form.___ While in gaseous form, the bulette can occupy another creature’s space and vice versa. In addition, if air can pass through a space, the bulette can pass through it without squeezing.

___Gone With the Wind.___ If a green bulette is reduced to 50% of its hit points, it disperes itself to the winds and escapes, regathering itself at a later time to rest and heal.

___Magic Resistance.___ The bulette has advantage on saving throws against spells and other magical effects.

___Multi-State Existence.___ While appearing as a solid, animal-based creature when stalking or resting, the green bulette instinctively shifts as a reaction into a poisonous gaseous form when attacked, appearing as a roiling cloud of green-tinged smoke with glittering emerald particles
floating within it. The green bulette will maintain its gaseous form and use it to attack, attempting to poison and choke its adversaries. It will only revert to its solid state again once it is safe and free from attack.

___Planar Connection.___ Because their arcane natures tie green bulettes to the Elemental Plane of Air, they also simultaneously exist there as insubstantial and barely visible vaporous apparitions.

**Actions**

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 24 (3d12 + 5) piercing damage.

___Creeping Death.___ While it is in gaseous form, one creature in the bulette’s space must make a DC 17 Constitution saving throw, taking 35 (10d6) poison damage on a failed save, or half as much damage on a successful one.

___State Change.___ As an action, the bulette can transform from its solid form into a cloud of poisonous gas that is 10 feet square.
