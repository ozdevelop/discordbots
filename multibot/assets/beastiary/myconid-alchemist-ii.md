"Myconid Alchemist II";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Undercommon"
_skills_: "Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 12"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "13 (2d8 + 4)"
_armor class_: "14 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 12 (+1) | 10 (+0) | 8 (-1) |

___Stalk-still.___ A myconid that remains perfectly still has advantage on
Dexterity (Stealth) checks made to hide.

**Actions**

___Stone Knuckles.___ Melee weapon attack: +5 to hit. Hit: 6 (1d6+3)
slashing damage.

___Spore Bomb.___ Ranged weapon attack: +3 to hit, range 30/60 ft. Hit: 5
(2d4) poison damage and the target must succeed on a DC 12 Constitution
saving throw or be paralyzed for 1 minute. The affected creature can
attempt the saving throw at the end of each of its turns, ending the effect
on a success.
