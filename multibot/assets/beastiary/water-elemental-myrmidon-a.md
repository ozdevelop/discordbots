"Water Elemental Myrmidon (A)";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Aquan, one language of its creator's choice"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "40 ft., swim 40 ft."
_hit points_: "127 (17d8+51)"
_armor class_: "18 (plate)"
_condition_immunities_: "paralyzed, petrified, poisoned, prone"
_damage_resistances_: "acid; bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 8 (-1) | 10 (0) | 10 (0) |

___Magic Weapons.___ The myrmidon's weapon attacks are magical.

**Actions**

___Multiattack.___ The myrmidon makes three trident attacks.

___Trident.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Freezing Strikes (Recharge 6).___ The myrmidon uses Multiattack. Each attack that hits deals an extra 5 (1d10) cold damage, and the target's speed is reduced by 10 feet until the end of the myrmidon's next turn.