"Oren Yogilvy";;;_size_: Small humanoid (strongheart halfling)
_alignment_: chaotic good
_challenge_: "0 (10 XP)"
_languages_: "Common, Halfling"
_skills_: "Perception +2, Performance +7, Persuasion +5"
_speed_: "25 ft."
_hit points_: "9 (2d6+2)"
_armor class_: "11"
_senses_: " passive Perception 12"
_damage_resistances_: "poison"
_stats_: | 8 (-1) | 13 (+1) | 12 (+1) | 11 (0) | 10 (0) | 16 (+3) |

___Halfling Nimbleness.___ Oren can move through the space of any creature that is of a size large than his.

___Lucky.___ When Oren rolls a 1 on an attack roll, ability check, or saving throw, he can reroll the die and must use the new roll.

___Stout Resilience.___ Oren has advantage on saving throws against poison

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60ft., one target. Hit: 3 (1d4+1) piercing damage. Duvessa carries only one dagger.

**Roleplaying** Information

Oren came to Nurthfurrow's End looking for easy work and found it. He sings for his supper, drinks like a fish, and wanders the fields at night dreaming up new lyrics to entertain the inn's other guests. Oren likes to stir up trouble from time to time, but he doesn't  have a mean bone in his body.

**Ideal:** "Music is food for the soul."

**Bond:** "You had me at "Can I buy you a drink."

**Flaw:** "I have a knack for putting myself in harm's way. Good thing I'm lucky!"