"Ratfolk";;;_size_: Small humanoid
_alignment_: neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Common"
_skills_: "Perception +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "25 ft., swim 10 ft."
_hit points_: "7 (2d6)"
_armor class_: "14 (studded leather armor)"
_stats_: | 7 (-2) | 15 (+2) | 11 (+0) | 14 (+2) | 10 (+0) | 10 (+0) |

___Nimbleness.___ The ratfolk can move through the space of any creature size Medium or larger.

___Pack Tactics.___ The ratfolk has advantage on its attack roll against a creature if at least one of the ratfolk's allies is within 5 feet of the creature and the ally is capable of attacking.

**Actions**

___Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Light crossbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

