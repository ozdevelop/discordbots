"Alseid Grovekeeper";;;_size_: Medium monstrosity
_alignment_: chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Nature +3, Perception +5, Stealth +5, Survival +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "40 ft."
_hit points_: "71 (13d8 + 13)"
_armor class_: "15 (studded leather Armor)"
_stats_: | 13 (+1) | 17 (+3) | 12 (+1) | 8 (-1) | 16 (+3) | 8 (-1) |

___Spellcasting.___ The grovekeeper is a 5th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 13, +5 to hit with spell attacks). It has the following druid spells prepared:

* Cantrips (at will): _druidcraft, guidance, produce flame, shillelagh_

* 1st (4 slots): _animal friendship, cure wounds, faerie fire_

* 2nd (3 slots): _animal messenger, heat metal, lesser restoration_

* 3rd (2 slots): _call lightning, dispel magic_

___Woodfriend.___ When in a forest, alseid leave no tracks and automatically discern true north.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +3 to hit (+5 with _shillelagh_), reach 5 ft., one creature.Hit: 4 (1d6 + 1) bludgeoning damage or 5 (1d8 + 1) bludgeoning damage if used in two hands, or 7 (1d8 + 3) bludgeoning damage with _shillelagh_.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6+3) piercing damage.

