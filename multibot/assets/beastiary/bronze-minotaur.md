"Bronze Minotaur";;;_size_: Large construct
_alignment_: neutral
_challenge_: "10 (5,900 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "fire, poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft."
_hit points_: "110 (13d10 + 39)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 16 (+3) | 3 (-4) | 11 (+0) | 1 (-5) |

___Fire Absorption.___ Whenever the bronze minotaur is subjected to fire
damage, it takes no damage and instead regains a number of hit points
equal to the fire damage dealt.

___Immutable Form.___ The bronze minotaur is immune to any spell or effect
that would alter its form.

___Magic Resistance.___ The bronze minotaur has advantage on saving
throws against spells and other magical effects.

___Magic Weapon.___ The bronze minotaur’s weapon attacks are magical.

**Actions**

___Multiattack.___ The bronze minotaur makes two greataxe attacks.

___Greataxe.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit:
23 (3d12 + 4) slashing damage.

___Breathe Fire (Recharge 5–6).___ The bronze minotaur releases a 30-foot
cone of flame. Creatures in the area must make a DC 16 Dexterity saving
throw, taking 52 (15d6) fire damage on a failed saving throw, or half as
much damage on a successful one.
