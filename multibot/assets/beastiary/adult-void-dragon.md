"Adult Void Dragon";;;_size_: Huge dragon
_alignment_: chaotic neutral
_challenge_: "14 (11500 XP)"
_languages_: "Common, Draconic, Void Speech"
_skills_: "Arcana +13, History +13, Perception +11, Persuasion +10, Stealth +5"
_senses_: ", passive Perception 21"
_saving_throws_: "Dex +5, Con +12, Wis +6, Cha +10"
_damage_immunities_: "cold"
_condition_immunities_: "charmed, frightened"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "229 (17d12 + 119)"
_armor class_: "19 (natural armor)"
_stats_: | 24 (+7) | 10 (+0) | 25 (+7) | 16 (+3) | 13 (+1) | 21 (+5) |

___Chill of the Void.___ Cold damage dealt by the void dragon ignores resistance to cold damage, but not cold immunity.

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

___Void Dweller.___ As ancient void dragon.

**Actions**

___Multiattack.___ The dragon can use its Aura of Madness. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 18 (2d10 + 7) piercing damage plus 7 (2d6) cold damage.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) slashing damage plus 3 (1d6) cold damage.

___Tail.___ Melee Weapon Attack: +12 to hit, reach 15 ft., one target. Hit: 16 (2d8 + 7) bludgeoning damage.

___Aura of Madness.___ As ancient void dragon, with DC 18 Wisdom saving throw.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons:

___Gravitic Breath.___ The dragon exhales a 60-foot cube of powerful localized gravity, originating from the dragon. Falling damage in the area increases to 1d10 per 10 feet fallen. When a creature starts its turn within the area or enters it for the first time in a turn, including when the dragon creates the field, must make a DC 20 Dexterity saving throw. On a failure the creature is restrained. On a success the creature's speed is halved as long as it remains in the field. A restrained creature repeats the saving throw at the end of its turn. The field persists until the dragon's breath recharges, and it can't use gravitic breath twice consecutively.

___Stellar Flare Breath.___ The dragon exhales star fire in a 60- foot cone. Each creature in that area must make a DC 20 Dexterity saving throw, taking 31 (9d6) fire damage and 31 (9d6) radiant damage on a failed save, or half as much damage on a successful one.

___Teleport.___ The dragon magically teleports to any open space within 100 feet.

**Reactions**

___Void Twist.___ When the dragon is hit by a ranged attack it can create a small rift in space to increase its AC by 5 against that attack. If the attack misses because of this increase the dragon can choose a creature within 30 feet to become the new target for that attack. Use the original attack roll to determine if the attack hits the new target.

**Legendary** Actions

___The dragon can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Void Slip (Costs 2 Actions).___ The dragon twists the fabric of space. Each creature within 15 feet of the dragon must succeed on a DC 18 Dexterity saving throw or take 12 (2d6 + 5) bludgeoning damage and be knocked prone. The dragon can then teleport to an unoccupied space within 40 feet.

___Void Cache (Costs 3 Actions).___ The dragon can magically reach into its treasure hoard and retrieve one item. If it is holding an item, it can use this ability to deposit the item into its hoard.

