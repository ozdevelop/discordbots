"Lizardfolk Voice of the Depths";;;_size_: Medium humanoid (lizardfolk)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Abyssal, Draconic, Primordial"
_senses_: "Passive Perception 14"
_skills_: "Perception +4, Stealth +4, Survival +6"
_speed_: "30 ft., swim 40 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 10 (+0) | 13 (+1) | 10 (+0) | 15 (+2) | 8 (-1) |

___Hold Breath.___ The lizardfolk can hold its breath for
15 minutes.

**Actions**

___Multiattack.___ The lizardfolk makes two attacks: one
with its bite and one with its claws.

___Blackjaw Bite.___ Melee Weapon Attack: +4 to hit,
reach 5 ft., one target. Hit: 5 (1d6 +2) piercing
damage and 2 (1d4) fire damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 4 (1d4 + 2) slashing damage.

___Drowned One’s Curse (1/Day).___ The lizardfolk targets
one creature it can see within 30 ft. and calls
upon the Drowned One to reach out from its watery
domain. The target must make a DC 13 Constitution
saving throw or begin suffocating as their lungs fill
with water. It must repeat the saving throw at the
end of its next turn. On a success, the effect ends.
On a failure, the creature drops to 0 hit points and
is dying, and it can’t regain hit points or be stabilized
until it can breathe again. The curse has no
effect on undead, constructs, or creatures that can
breathe underwater.with its bite and one with its claws or trident or
two melee attacks with its trident.

___Song of the Depths.___ When two or more lizardfolk voices of the depths
are within 30 feet of one another they can work
together and sing a hymn to the Drowned One,
calling on its power to cast _entangle_. The DC is
12 + the number of lizardfolk voices of the depths
casting the spell, and concentration is shared by all
lizardfolk who contribute to the casting.
