"Doombat";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "blindsight 60 ft., passive Perception 13"
_speed_: "10 ft., fly 50 ft."
_hit points_: "59 (7d10 + 21)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 18 (+4) | 16 (+3) | 2 (-4) | 12 (+1) | 6 (-2) |

___Yip.___ Doombats constantly yip while in combat, and the noise interferes
with the concentration of those attempting to cast spells. All creatures
within a 30-foot radius that are maintaining concentration on a spell when
the doombat yips must succeed on a DC 10 Constitution saving throw or
lose concentration on that spell.

**Actions**

___Multiattack.___ The doombat makes one bite attack and one tail attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) piercing damage.

___Tail.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 8
(1d8 + 4) bludgeoning damage.

___Shriek (Recharge 5–6).___ The doombat emits a piercing shriek. All
creatures within a 60-foot radius must succeed on a DC 13 Wisdom saving
throw or be poisoned for 1 minute. The target can repeat the saving throw
at the end of each of its turns, ending the effect on itself on a success.
