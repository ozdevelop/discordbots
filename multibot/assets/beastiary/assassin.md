"Assassin";;;_size_: Medium humanoid (any race)
_alignment_: any non-good alignment
_challenge_: "8 (3,900 XP)"
_languages_: "Thieves' cant plus any two languages"
_skills_: "Acrobatics +6, Deception +3, Perception +3, Stealth +9"
_saving_throws_: "Dex +6, Int +4"
_speed_: "30 ft."
_hit points_: "78 (12d8+24)"
_armor class_: "15 (studded leather)"
_damage_resistances_: "poison"
_stats_: | 11 (0) | 16 (+3) | 14 (+2) | 13 (+1) | 11 (0) | 10 (0) |

___Assassinate.___ During its first turn, the assassin has advantage on attack rolls against any creature that hasn't taken a turn. Any hit the assassin scores against a surprised creature is a critical hit.

___Evasion.___ If the assassin is subjected to an effect that allows it to make a Dexterity saving throw to take only half damage, the assassin instead takes no damage if it succeeds on the saving throw, and only half damage if it fails.

___Sneak Attack (1/Turn).___ The assassin deals an extra 13 (4d6) damage when it hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 ft. of an ally of the assassin that isn't incapacitated and the assassin doesn't have disadvantage on the attack roll.

**Actions**

___Multiattack.___ The assassin makes two shortsword attacks.

___Shortsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage, and the target must make a DC 15 Constitution saving throw, taking 24 (7d6) poison damage on a failed save, or half as much damage on a successful one.

___Light Crossbow.___ Ranged Weapon Attack: +6 to hit, range 80/320 ft., one target. Hit: 7 (1d8 + 3) piercing damage, and the target must make a DC 15 Constitution saving throw, taking 24 (7d6) poison damage on a failed save, or half as much damage on a successful one.