"Zoblin Boss";;;_size_: Small undead (goblinoid)
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "33 (6d6 + 12)"
_armor class_: "12 (chain shirt)"
_stats_: | 14 (+2) | 8 (-1) | 14 (+2) | 5 (-3) | 8 (-1) | 6 (-2) |

___Undead Fortitude.___ If damage reduces the zoblin to 0 hit
points, it must make a Constitution saving throw with a
DC of 5 + the damage taken, unless the damage is
radiant or from a critical hit. On a success, the zoblin
drops to 1 hit point instead.

**Actions**

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 5 (1d6 + 2) piercing damage plus 2
(1d4) necrotic damage.

**Reactions**

___Lingering Instincts.___ Whenever an enemy within 5ft.
misses the zoblin with an attack, it can use its reaction
to make a scimitar attack against that creature with
disadvantage.
