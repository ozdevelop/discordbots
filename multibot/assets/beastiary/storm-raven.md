"Storm Raven";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "lightning"
_speed_: "10 ft., fly 40 ft."
_hit points_: "31 (7d6 + 7)"
_armor class_: "13"
_stats_: | 7 (-2) | 17 (+3) | 12 (+1) | 8 (-1) | 14 (+2) | 9 (-1) |

___Flyby.___ The raven doesn't provoke an opportunity
attack when it flies out of an enemy's reach.

___Magic Resistance.___ The raven has advantage on
saving throws against spells and other magical
effects.

___Lightning Charged.___ When the raven hits with a
melee attack, it deals an extra 2 (1d4) lightning
damage (included in the attack).

**Actions**

___Multiattack.___ The raven makes two attacks: one with
its beak and one with its talons. It then summons a
lightning orb.

___Beak.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 5 (1d4 + 3) piercing damage plus 2
(1d4) lightning damage.

___Talons.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 6 (1d6 + 3) piercing damage plus 2
(1d4) lightning damage.

___Summon Lightning Orb.___ The raven creates a 3 inch
diameter sphere of electricity in an unoccupied
space within 60 feet. This sphere persists for 1
minute or until the raven is killed. A creature that
ends its turn in the same space as a lightning orb
takes 3 (1d6) lightning damage. On initiative count
20, the orbs spark a 5-foot wide line of lighting
between all other orbs within 20 feet. Creatures
caught within one of these streaks of lightning
must succeed on a DC 12 Dexterity saving throw or
take 10 (3d6) lightning damage.
