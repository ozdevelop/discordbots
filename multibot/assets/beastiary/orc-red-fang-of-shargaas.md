"Orc Red Fang of Shargaas";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +1, Perception +2, Stealth +5"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "15 (studded leather armor)"
_stats_: | 11 (0) | 16 (+3) | 15 (+2) | 9 (-1) | 11 (0) | 9 (-1) |

___Cunning Action.___ On each of its turns, the orc can use a bonus action to take the Dash, Disengage, or Hide action.

___Hand of Shargaas.___ The orc deals an 2 extra dice of damage when it hits a target with a weapon attack (included in its attacks).

___Shargaas's Sight.___ Magical darkness doesn't impede the orc's darkvision.

___Slayer.___ In the first round of a combat, the orc has advantage on attack rolls against any creature that hasn't taken a turn yet. If the orc hits a creature that round who was surprised, the hit is automatically a critical hit.

**Actions**

___Multiattack.___ The orc makes two scimitar or dart attacks.

___Scimitar.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 13 (3d6+3) slashing damage.

___Dart.___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., one target. Hit: 10 (3d4+3) piercing damage.

___Veil of Shargaas (Recharges after a Short or Long Rest).___ The orc casts darkness without any components. Wisdom is its spellcasting ability.