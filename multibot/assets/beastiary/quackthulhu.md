"Quackthulhu";;;_size_: Gargantuan aberration
_alignment_: chaotic evil
_challenge_: "21 (33,000 XP)"
_languages_: "--"
_skills_: "Intimidation +9, Perception +5"
_senses_: "blindsight 120 ft., darkvision 120 ft., passive Perception 15"
_saving_throws_: "Str +12, Con +11, Wis +7"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_damage_immunities_: "necrotic, poison"
_speed_: "50 ft., swim 100 ft."
_hit points_: "429 (26d20 + 156)"
_armor class_: "18 (natural armor)"
_stats_: | 24 (+7) | 14 (+2) | 22 (+6) | 5 (-3) | 15 (+2) | 20 (+5) |

___Legendary Resistance (3/day).___ If Quackthulhu fails a saving throw, it can choose to succeed instead.

**Actions**

___Multiattack.___ Quackthulhu makes 4 attacks, one of which can be an Infect attack.

___Tentacle.___ Melee Weapon Attack, +11 to hit, reach 15 ft., one target. Hit: 20 (3d8 + 7) bludgeoning damage, and the target must
succeed on a DC 15 Strength saving throw or become grappled.  Quackthulhu has four tentacles.

___Infect.___ Melee Weapon Attack, +11 to hit, reach 5 ft., one target who is grappled.  Hit: 17 (3d6 + 7) piercing damage, and the target
must succeed on a DC 15 Constitution saving throw or become poisoned for one hour.  While poisoned in this way, the target is considered infected.

___Soul-wrenching Quack (Recharge 6).___ All creatures within 60 ft. of Quackthulhu must succeed on a DC 16 Wisdom saving throw or become
paralyzed until the end of Quackthulhu's next turn.

**Legendary** Actions

Quackthulhu can take 3 legendary actions, choosing from the options below.  Only one legendary action can be used at a time and only at
the end of another creature's turn.  Quackthulhu regains spent legendary actions at the start of its turn.

___Tentacle.___ Quackthulhu makes a tentacle attack.

___Ground Slam.___ All creatures within 5 ft. of Quackthulhu must succeed on a DC 14 Strength saving throw or be pushed 10 ft. away.

___Command Infected.___ A creature within 60 ft. of Quackthulhu who is currently infected moves up to half its speed and makes a single
weapon attack against a creature of Quackthulhu's choice.
