"Gar Shatterkeel";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Aquan, Common"
_skills_: "Nature +8, Survival +8"
_speed_: "30 ft., swim 30 ft."
_hit points_: "112 (15d8+45)"
_armor class_: "16 (natural armor)"
_damage_resistances_: "cold"
_stats_: | 15 (+2) | 15 (+2) | 16 (+3) | 12 (+1) | 18 (+4) | 13 (+1) |

___Amphibious.___ Gar can breathe air and water.

___Legendary Resistance (2/Day).___ If Gar fails a saving throw, he can choose to succeed instead.

___Spellcasting.___ Gar is a 9th-level spellcaster. His spellcasting ability is Wisdom (spell save DC 16, +8 to hit with spell attacks). He has the following druid spells prepared:

* Cantrips (at will): _mending, resistance, shape water_

* 1st level (4 slots): _create or destroy water, cure wounds, fog cloud, thunderwave_

* 2nd level (3 slots): _darkvision, hold person, protection from poison_

* 3rd level (3 slots): _call lightning, sleet storm, tidal wave_

* 4th level (3 slots): _control water, ice storm_

* 5th level (1 slot): _scrying_

___Water Walk.___ Gar can stand and move on liquid surfaces as if they were solid ground.

___Watery Fall.___ When Gar drops to 0 hit points, his body collapses into a pool of inky water that rapidly disperses. Anything he was wearing or carrying is left behind.

**Actions**

___Multiattack.___ Gar makes two melee attacks, one with his claw and one with Drown.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage, and the target is grappled (escape DC 13). Until the grapple ends, Gar can't attack other creatures with his claw.

___Drown.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 4 (1d8) cold damage.
