"Disfigured Disciple";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Deep Speech"
_senses_: "passive Perception 8"
_speed_: "30 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 10 (+0) | 14 (+2) | 13 (+1) | 7 (-2) | 5 (-3) |

___Terrorizing Transmogrification.___ The disciple contains
three distinct mutations that affect its combat abilities,
its movements, and its defenses. Choose randomly
from the lists below or choose them yourself to craft
specific monstrosities.

**Head**

___Serpent's Tongue___ - The disciple has a large snake-like
tongue that can smell the air for nearby enemies. The
disciple gains a blindsight of 20 ft.

___Swarming Eyes___ - The disciple's face is covered in over a
dozen extra eyes. It is immune to the blinded condition
and gains darkvision 60 ft.

___Putrid Boils___ - The disciple's face is coated in putrid
boils and blisters. Whenever a creature touches the
disciple or hits it with a melee weapon attack while
within 5 feet, that creature takes 5 (1d10) acid damage
as a boil bursts with vile pus.

**Body**

___Jagged Spine___ - The disciple has massive spikes
protruding from its spine. Any creature that attempts
to grapple the disciple takes 11 (2d10) piercing
damage.

___Winged Horror___ - The disciple has a set of large bat-like
wings, providing it with a 30 ft. fly speed.

___Thick Flesh___ - The disciple's face and body are colored a
deep purple and its flesh becomes as tough as armor.
Non-magical weapon attacks against the disciple deal 3
less damage to a minimum of 1.

**Arms**

___Tentacled___ - The disciple's left arm is a long, sticky
tentacle. The disciple's off-hand attack is the following:
* ___Tentacle.___ Melee Weapon Attack: +4 to hit, reach 10
ft., one target. Hit: 7 (1d10 + 2) bludgeoning
damage and the target is grappled if it is size Large
or smaller. A creature can break free of this grapple
by using its action and succeeding on a DC 12
Strength saving throw.

___Carved Bone___ - The disciple's left hand is sliced off and
the remaining bone has been filed down to a sharp
points. The disciple's off-hand attack is the following:
* ___Bone Spear.___ Melee Weapon Attack: +4 to hit, reach 5
ft., one target. Hit: 5 (1d6 + 2) piercing damage plus
3 (1d6) necrotic damage.

___Dual Claws___ - The disciple's left arm is raw bone that
splits into two distinct forearms at the elbow. At the
end of each of these is a set of vicious claws. The
disciple's off-hand attack is the following:
* ___Dual Claw.___ Melee Weapon Attack: +4 to hit, reach 5
ft., one target. Hit: 9 (2d4 + 4) slashing damage.

**Actions**

___Multiattack.___ The zealot makes one attack with its
longsword and one with its off-hand attack.

___Longsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 6 (1d8 + 2) slashing damage.
