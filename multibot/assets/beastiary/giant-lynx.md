"Giant Lynx";;;_size_: Large celestial
_alignment_: chaotic good
_challenge_: "9 (5,000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Animal Handling +9, Insight +9, Perception +9, Stealth +10"
_senses_: "darkvision 120 ft., passive Perception 19"
_saving_throws_: "Wis +9, Cha +9"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened"
_speed_: "40 ft., climb 30 ft."
_hit points_: "123 (13d10 + 52)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 22 (+6) | 19 (+4) | 17 (+3) | 20 (+5) | 20 (+5) |

___Magic Resistance.___ The giant lynx has advantage on saving throws
against spells and other magical effects.

___Magic Weapons.___ The giant lynx’s attacks are considered magical.

___Spellcasting.___ The giant lynx is a 12th level spellcaster. The giant lynx’s
spellcasting ability is Wisdom (spell save DC 17, +9 to hit with spell
attacks). It can cast the following cleric spells:

* Cantrips (at will): _guidance, light, mending, resistance, sacred flame, thaumaturgy_

* 1st level (4 slots): _bless, cure wounds, detect evil and good, protection from evil and good_

* 2nd level (3 slots): _aid, hold person, lesser restoration, spiritual weapon_

* 3rd level (3 slots): _beacon of hope, dispel magic, revivify_

* 4th level (3 slots): _banishment, death ward, locate creature_

* 5th level (2 slots): _dispel evil and good, raise dead_

* 6th level (1 slot): _heal_

**Actions**

___Multiattack.___ The giant lynx makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) slashing damage.

___Change Shape.___ The giant lynx magically polymorphs into a humanoid
or beast that has a challenge rating equal to or less than its own, or back
into its true form. It reverts to its true form if it dies. Any equipment it is
wearing or carrying is absorbed or borne by the new form (the giant lynx’s
choice). In a new form, the giant lynx retains its game statistics and ability
to speak, but its AC, movement modes, Strength, Dexterity, and special
senses are replaced by those of the new form, and it gains any statistics or
capabilities (except class features, legendary actions, and lair actions) that
the new form has but that it lacks.
