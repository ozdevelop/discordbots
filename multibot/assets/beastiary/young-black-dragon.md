"Young Black Dragon";;;_size_: Large dragon
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Perception +6, Stealth +5"
_damage_immunities_: "acid"
_saving_throws_: "Dex +5, Con +6, Wis +3, Cha +5"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "127 (15d10+45)"
_armor class_: "18 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 17 (+3) | 12 (+1) | 11 (0) | 15 (+2) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 15 (2d10 + 4) piercing damage plus 4 (1d8) acid damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Acid Breath (Recharge 5-6).___ The dragon exhales acid in a 30-foot line that is 5 feet wide. Each creature in that line must make a DC 14 Dexterity saving throw, taking 49 (11d8) acid damage on a failed save, or half as much damage on a successful one.