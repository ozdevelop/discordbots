"Fire Dancer Swarm";;;_size_: Medium swarm
_alignment_: neutral evil
_challenge_: "7 (2900 XP)"
_languages_: "Ignan"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "fire, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, stunned, unconscious"
_speed_: "30 ft., fly 30 ft. (hover)"
_hit points_: "90 (12d8 + 36)"
_armor class_: "15"
_stats_: | 10 (+0) | 20 (+5) | 16 (+3) | 6 (-2) | 10 (+0) | 7 (-2) |

___Fire Form.___ A creature that touches the swarm or hits it with a melee attack while within 5 feet of it takes 5 (1d10) fire damage. In addition, the first time the swarm enters a creature's space on a turn, that creature takes 5 (1d10) fire damage and catches fire; until someone uses an action to douse the fire, the creature takes 5 (1d10) fire damage at the start of each of its turns.

___Illumination.___ The swarm sheds bright light in a 30-foot radius and dim light to an additional 30 feet.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny creature. The swarm can't regain hit points or gain temporary hit points.

___Water Susceptibility.___ For every 5 feet the swarm moves in water, or for every gallon of water splashed on it, it takes 1 cold damage.

**Actions**

___Swarm.___ Melee Weapon Attack: +8 to hit, reach 0 ft., one target in the swarm's space. Hit: 21 (6d6) fire damage, or 10 (3d6) fire damage if the swarm has half or fewer hit points.

