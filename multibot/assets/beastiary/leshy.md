"Leshy";;;_size_: Medium monstrosity
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Deception +5, Perception +4, Stealth +3, Survival +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "30 ft."
_hit points_: "84 (13d8 + 26)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 14 (+2) | 14 (+2) | 15 (+2) | 16 (+3) |

___Innate Spellcasting.___ The leshy's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no material components:

* At will: _animal friendship, pass without trace, speak with animals_

* 1/day each: _entangle, plant growth, shillelagh, speak with plants, hideous laughter_

___Camouflage.___ A leshy has advantage on Stealth checks if it is at least lightly obscured by foliage.

___Mimicry.___ A leshy can mimic the calls and voices of any creature it has heard. To use this ability, the leshy makes a Charisma (Deception) check. Listeners who succeed on an opposed Wisdom (Insight) or Intelligence (Nature).DM's choice.realize that something is mimicking the sound. The leshy has advantage on the check if it's mimicking a general type of creature (a crow's call, a bear's roar) and not a specific individual's voice.

**Actions**

___Multiattack.___ The leshy makes two club attacks.

___Change Size.___ The leshy appears to change its size, becoming as tall as a massive oak (Gargantuan) or as short as a blade of grass (Tiny). The change is entirely illusory, so the leshy's statistics do not change.

___Club.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage.

