"White Tusk Bloodrager";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Orc"
_skills_: "Athletics +5, Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +4"
_speed_: "30 ft."
_hit points_: "60 (8d8 + 24)"
_armor class_: "16 (breastplate)"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 11 (+0) | 12 (+1) | 9 (-1) |

___Cold Fury (Recharges after a Short or Long
Rest).___ As a bonus action, the orc can enter a
cold fury for 1 minute. While in a fury, its melee
weapon attacks deal an extra 3 (1d6) damage.
Also, if the orc is reduced to 0 hit points while
in a cold fury, unless it was dealt with a critical
hit, the orc makes a Constitution saving throw
with a DC of 5 plus the damage taken. On a
success, the orc instead drops to 1 hit point and
its fury ends.

___Aggressive.___ As a bonus action, the orc can
move up to its speed toward a hostile creature it
can see.

___Minion: Savage Horde.___ After moving at least 20
feet in a straight line toward a creature, the next
attack the orc makes against that creature scores
a critical hit on a roll of 18–20.


**Actions**

___Multiattack.___ The White Tusk Bloodrager makes
three falchion attacks.

___Falchion.___ Melee Weapon Attack: +6 to hit, reach 5
ft., one target. Hit: 8 (2d4 + 3) slashing damage. 
