"Baphomet";;;_page_number_: 143
_size_: Huge fiend (demon)
_alignment_: chaotic evil
_challenge_: "23 (50,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 24"
_skills_: "Intimidation +17, Perception +14"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_saving_throws_: "Dex +9, Con +15, Wis +14"
_speed_: "40 ft."
_hit points_: "275  (19d12 + 152)"
_armor class_: "22 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 30 (+10) | 14 (+2) | 26 (+8) | 18 (+4) | 24 (+7) | 16 (+3) |

___Charge.___ If Baphomet moves at least 10 feet straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 16 (3d10) piercing damage. If the target is a creature, it must succeed on a DC 25 Strength saving throw or be pushed up to 10 feet away and knocked prone.

___Innate Spellcasting.___ Baphomet's spellcasting ability is Charisma (spell save DC 18). He can innately cast the following spells, requiring no material components:

* At will: _detect magic_

* 3/day each: _dispel magic, dominate beast, hunter's mark, maze, wall of stone_

* 1/day: _teleport_

___Labyrinthine Recall.___ Baphomet can perfectly recall any path he has traveled, and he is immune to the maze spell.

___Legendary Resistance (3/Day).___ If Baphomet fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Baphomet has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Baphomet's weapon attacks are magical.

___Reckless.___ At the start of his turn, Baphomet can gain advantage on all melee weapon attack rolls during that turn, but attack rolls against him have advantage until the start of his next turn.

**Actions**

___Multiattack___ Baphomet makes three attacks: one with Heartcleaver, one with his bite, and one with his gore attack.

___Heartcleaver___ Melee Weapon Attack: +17 to hit, reach 15 ft., one target. Hit: 21 (2d10 + 10) slashing damage.

___Bite___ Melee Weapon Attack: +17 to hit, reach 10 ft., one target. Hit: 19 (2d8 + 10) piercing damage.

___Gore___ Melee Weapon Attack: +17 to hit, reach 10 ft., one target. Hit: 17 (2d6 + 10) piercing damage.

___Frightful Presence___ Each creature of Baphomet's choice within 120 feet of him and aware of him must succeed on a DC 18 Wisdom saving throw or become frightened for 1 minute. A frightened creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. These later saves have disadvantage if Baphomet is within line of sight of the creature.
If a creature succeeds on any of these saves or the effect ends on it, the creature is immune to Baphomet's Frightful Presence for the next 24 hours.

**Legendary** Actions

Baphomet can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Baphomet regains spent legendary actions at the start of his turn.

___Heartcleaver Attack___ Baphomet makes a melee attack with Heartcleaver.

___Charge (Costs 2 Actions)___ Baphomet moves up to his speed, then makes a gore attack.
