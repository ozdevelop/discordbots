"Emerald Eye";;;_size_: Tiny construct
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic, telepathy 250 ft."
_skills_: "Acrobatics +4, Arcana +4, Deception +5, History +4, Perception +3, Persuasion +5, Religion +4"
_senses_: ", passive Perception 13"
_saving_throws_: "Dex +4, Con +4, Int +4"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire; piercing damage"
_condition_immunities_: "blinded, deafened, exhausted, paralyzed, petrified, poisoned, prone, unconscious"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "54 (12d4 + 24)"
_armor class_: "14"
_stats_: | 3 (-4) | 15 (+2) | 14 (+2) | 15 (+2) | 12 (+1) | 16 (+3) |

___Bound.___ An emerald eye cannot move more than 25 feet away from the creature that it is psychically linked to. It begins existence bound to its creator, but a free emerald eye can bind itself to another creature as in the Bind action.

___Immutable Form.___ The emerald eye is immune to any spell or effect that would alter its form.

**Actions**

___Slash.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 14 (5d4 + 2) slashing damage.

___Attraction (Recharge 5-6).___ An emerald eye can compel one creature to move toward a particular person or object. If the creature fails a DC 13 Charisma saving throw, it feels a powerful compulsion to move toward whatever the emerald eye chooses. The target creature must be within 25 feet of the emerald eye when attraction is triggered, but the creature is then free to move beyond this range while remaining under the effect. Nothing seems out of the ordinary to the creature, but it does not knowingly put itself or its allies in harm's way to reach the object. The creature may attempt another DC 13 Charisma saving throw at the start of each of its turns; a success ends the effect.

___Bind (3/Day).___ The emerald eye can bind itself psychically to a creature with an Intelligence of 6 or greater. The attempt fails if the target succeeds on a DC 13 Charisma saving throw. The attempt is unnoticed by the target, regardless of the result.

___Telepathic Lash (3/Day).___ An emerald eye can overwhelm one humanoid creature within 25 feet with emotions and impulses the creature is hard-pressed to control. If the target fails a DC 13 Wisdom saving throw, it is stunned for 1 round.

