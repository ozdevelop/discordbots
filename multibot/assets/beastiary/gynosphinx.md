"Gynosphinx";;;_size_: Large monstrosity
_alignment_: lawful neutral
_challenge_: "11 (7,200 XP)"
_languages_: "Common, Sphinx"
_senses_: "truesight 120 ft."
_skills_: "Arcana +12, History +12, Perception +8, Religion +8"
_damage_immunities_: "psychic"
_speed_: "40 ft., fly 60 ft."
_hit points_: "136 (16d10+48)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, frightened"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 18 (+4) | 18 (+4) | 18 (+4) |

___Inscrutable.___ The sphinx is immune to any effect that would sense its emotions or read its thoughts, as well as any divination spell that it refuses. Wisdom (Insight) checks made to ascertain the sphinx's intentions or sincerity have disadvantage.

___Magic Weapons.___ The sphinx's weapon attacks are magical.

___Spellcasting.___ The sphinx is a 9th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 16, +8 to hit with spell attacks). It requires no material components to cast its spells. The sphinx has the following wizard spells prepared:

* Cantrips (at will): _mage hand, minor illusion, prestidigitation_

* 1st level (4 slots): _detect magic, identify, shield_

* 2nd level (3 slots): _darkness, locate object, suggestion_

* 3rd level (3 slots): _dispel magic, remove curse, tongues_

* 4th level (3 slots): _banishment, greater invisibility_

* 5th level (1 slot): _legend lore_

**Actions**

___Multiattack.___ The sphinx makes two claw attacks.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

**Legendary** Actions

The gynosphinx can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The gynosphinx regains spent legendary actions at the start of its turn.

___Claw Attack.___ The sphinx makes one claw attack.

___Teleport (Costs 2 Actions).___ The sphinx magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.

___Cast a Spell (Costs 3 Actions).___ The sphinx casts a spell from its list of prepared spells, using a spell slot as normal.
