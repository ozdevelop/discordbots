"Ghost Knight";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "6 (2300 XP)"
_languages_: "Common"
_skills_: "Athletics +6, Animal Handling +3, Perception +3, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_damage_resistances_: "necrotic"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "97 (15d8+30)"
_armor class_: "17 (half plate)"
_stats_: | 17 (+3) | 15 (+2) | 14 (+2) | 8 (-1) | 10 (+0) | 7 (-2) |

___Charge.___ If the ghost knight is mounted and moves at least 30 feet in a straight line toward a target and hits it with a melee attack on the same turn, the target takes an extra 7 (2d6) damage.

___Mounted Warrior.___ When mounted, the ghost knight has advantage on attacks against unmounted creatures smaller than its mount. If the ghost knight's mount is subjected to an effect that allows it to take half damage with a successful Dexterity saving throw, the mount instead takes no damage if it succeeds on the saving throw and half damage if it fails.

___Turning Defiance.___ The ghost knight and all darakhul or ghouls within 30 feet of it have advantage on saving throws against effects that turn undead.

___Undead Nature.___ A ghost knight doesn't require air, food, drink, or sleep

**Actions**

___Multiattack.___ The ghost knight makes three melee attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) slashing damage. If the target is a creature other than an elf or undead, it must succeed on a DC 13 Constitution saving throw or be paralyzed for 1 minute. A paralyzed target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Battleaxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage or 8 (1d10 + 3) slashing damage if used with two hands, plus 10 (3d6) necrotic damage.

___Lance.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 9 (1d12 + 3) piercing damage plus 10 (3d6) necrotic damage.

