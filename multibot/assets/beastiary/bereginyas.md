"Bereginyas";;;_size_: Tiny fey
_alignment_: neutral evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Perception +5, Stealth +9"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Dex +7"
_damage_immunities_: "bludgeoning"
_speed_: "20 ft., fly 60 ft."
_hit points_: "70 (20d4 + 20)"
_armor class_: "15"
_stats_: | 14 (+2) | 20 (+5) | 12 (+1) | 13 (+1) | 12 (+1) | 11 (+0) |

**Actions**

___Multiattack.___ The bereginyas makes two claw attacks. If both attacks hit the same target, the target is grappled (escape DC 12) and the bereginyas immediately uses Smother against it as a bonus action.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) slashing damage.

**Bonus** Actions

___Smother.___ The bereginyas extends a semi-solid gaseous tendril down the grappled target's throat. The target must make a successful DC 14 Strength saving or it is immediately out of breath and begins suffocating. Suffocation ends if the grapple is broken or if the bereginyas is killed.
