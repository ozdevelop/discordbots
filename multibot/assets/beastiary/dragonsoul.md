"Dragonsoul";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Draconic, Infernal"
_skills_: "Deception +6, Stealth +7"
_saving_throws_: "Wis +4"
_speed_: "30 ft."
_hit points_: "110 (17d8+34)"
_armor class_: "16 (studded leather)"
_damage_resistances_: "one of the following: acid, cold, fire, lightning or poison"
_stats_: | 11 (0) | 18 (+4) | 14 (+2) | 13 (+1) | 12 (+1) | 16 (+3) |

___Dragon Fanatic.___ The dragonsoul has advantage on saving throws against being charmed or frightened. While the dragonsoul can see a dragon or higher-ranking Cult of the Dragon cultist friendly to it, the dragonsoul ignores the effects of being charmed or frightened.

___Fanatic Advantage.___ Once per turn, if the dragonsoul makes a weapon attack with advantage on the attack roll and hits, the target takes an extra 10 (3d6) damage.

___Limited Flight.___ The dragonsoul can use a bonus action to gain a flying speed of 30 feet until the end of its turn.

___Pack Tactics.___ The dragonsoul has advantage on an attack roll against a creature if at least one of the dragonclaw's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The Dragonsoul attacks twice with its shortsword.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 10 (3d6) damage of the type to which the dragonsoul has resistance.

___Orb of Dragon's Breath (3/Day).___ Ranged Spell Attack: +7 to hit, range 90 ft., one target. Hit: 27 (6d8) damage of the type to which the dragonsoul has damage resistance.