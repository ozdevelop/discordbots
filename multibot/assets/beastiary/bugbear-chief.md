"Bugbear Chief";;;_size_: Medium humanoid (goblinoid)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +2, Stealth +6, Survival +3"
_speed_: "30 ft."
_hit points_: "65 (10d8+20)"
_armor class_: "17 (chain shirt, shield)"
_stats_: | 17 (+3) | 14 (+2) | 14 (+2) | 11 (0) | 12 (+1) | 11 (0) |

___Brute.___ A melee weapon deals one extra die of its damage when the bugbear hits with it (included in the attack).

___Surprise Attack.___ If the bugbear surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 7 (2d6) damage from the attack.

___Heart of Hruggek.___ The bugbear has advantage on saving throws against being charmed, frightened, paralyzed, poisoned, stunned, or put to sleep.

**Actions**

___Multiattack.___ The bugbear makes two melee attacks

___Morningstar.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 11 (2d8 + 3) piercing damage.

___Javelin.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 9 (2d6 + 3) piercing damage in melee or 5 (1d6 + 3) piercing damage at range.