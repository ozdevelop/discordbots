"Lich Shade";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Infernal, plus up to four other languages"
_skills_: "Arcana +7, History +7, Insight +6, Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_damage_immunities_: "necrotic"
_damage_resistances_: "cold, lightning, poison; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "30 ft."
_hit points_: "85 (9d8 + 45)"
_armor class_: "16 (natural armor)"
_stats_: | 11 (+0) | 16 (+3) | 20 (+5) | 18 (+4) | 16 (+3) | 13 (+1) |

___Death Throes.___ When the lich shade drops to 0 hit points, it explodes in
a cloud of dust in a 10-foot radius. Creatures within this area must make
a DC 16 Constitution saving throw. On a failed saving throw, the creature
takes 22 (4d10) necrotic damage, and the creature’s maximum hit points
are reduced by the same amount. If a creature’s maximum hit points are
reduced to 0, it dies. Magic such as greater restoration is necessary to cure
this effect. On a successful saving throw, the creature takes half damage
and is poisoned for 1 minute, but its maximum hit points are unaffected.
Magic Resistance. The lich shade has advantage on saving throws
against spells and other magical effects.

___Magic Weapon.___ The lich shade’s weapon attacks are magical.

**Actions**

___Multiattack.___ The lich shade makes two claw attacks.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12
(2d8 + 3) slashing damage plus 11 (2d10) cold damage.

**Reactions**

___Spell Leech.___ When a creature the lich shade can see within 30 feet of it
casts a spell of 1st level or higher, the lich shade can counter the spell, as
if the lich shade had cast counterspell. If the lich shade attempts to leech a
spell of 4th level or higher, it must make an Intelligence ability check. The
DC for this check is 10 + the spell’s level.

If the spell leech is successful, the lich shade absorbs the magical
energy and can use it only on its next turn in one of the following ways:

* ___Cast.___ The lich shade can cast the spell as an action on its turn, using the
original caster’s spell save DC and spell attack modifier.
* ___Eldritch Bolt.___ The lich shade chooses one creature it can see within 60
feet of it as an action. That creature must make a DC 16 Dexterity saving
throw, taking 22 (4d10) force damage on a failed saving throw, or half as
much damage on a successful one.
* ___Heal.___ The lich shade uses an action to regain 22 (4d10) hit points, up to
its maximum hit points.

If the lich shade does not use the absorbed magic, it fades at the end of
its next turn.
