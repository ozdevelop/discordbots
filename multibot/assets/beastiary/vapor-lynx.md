"Vapor Lynx";;;_size_: Large monstrosity
_alignment_: chaotic neutral
_challenge_: "5 (1800 XP)"
_languages_: "Common, Sylvan"
_skills_: "Perception +4, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "50 ft., climb 30 ft."
_hit points_: "127 (15d10 + 45)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 18 (+4) | 16 (+3) | 10 (+0) | 13 (+1) | 14 (+2) |

___Innate Spellcasting.___ The lynx's innate spellcasting ability is Charisma. It can cast the following spell, requiring no material components:

3/day: gaseous form

___Smoky Constitution.___ The vapor lynx spends its time in both gaseous and solid form. Its unique constitution makes it immune to all fog- or gas-related spells and attacks, including its own. A vapor lynx sees clearly through light or heavy obscurement caused by fog, mist, or spells such as fog cloud.

**Actions**

___Multiattack.___ The vapor lynx makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 17 (3d8 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

___Poison Breath (Recharge 5-6).___ The vapor lynx exhales a 40- foot radius poison fog, which heavily obscures a spherical area around the lynx. Any breathing creature that ends its turn in the fog must make a DC 14 Constitution saving throw or become poisoned for 1d4 + 1 rounds.

