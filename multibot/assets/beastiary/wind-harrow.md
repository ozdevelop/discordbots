"Windharrow";;;_size_: Medium humanoid (half-elf)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Auran, Common, Elvish"
_senses_: "darkvision 60 ft."
_skills_: "Acrobatics +5, Deception +7, Persuasion +5, Performance +7, Stealth +5"
_speed_: "30 ft."
_hit points_: "55 (10d8+10)"
_armor class_: "15 (studded leather)"
_stats_: | 10 (0) | 16 (+3) | 12 (+1) | 14 (+2) | 10 (0) | 17 (+3) |

___Fey Ancestry.___ Windharrow has advantage on saving throws against being charmed, and magic can't put him to sleep.

___Spellcasting.___ Windharrow is an 8th-level spellcaster. His spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). Windharrow knows the following bard spells:

* Cantrips (at will): _friends, prestidigitation, vicious mockery_

* 1st level (4 slots): _disguise self, dissonant whispers, thunderwave_

* 2nd level (3 slots): _invisibility, shatter, silence_

* 3rd level (3 slots): _nondetection, sending, tongues_

* 4th level (2 slots): _confusion, dimension door_

**Actions**

___Multiattack.___ Windharrow makes two melee attacks.

___Rapier.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 7 (1d8 + 3) piercing damage.
