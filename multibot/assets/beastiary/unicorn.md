"Unicorn";;;_size_: Large celestial
_alignment_: lawful good
_challenge_: "5 (1,800 XP)"
_languages_: "Celestial, Elvish, Sylvan, telepathy 60 ft."
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "50 ft."
_hit points_: "67 (9d10+18)"
_armor class_: "12"
_condition_immunities_: "charmed, paralyzed, poisoned"
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 11 (0) | 17 (+3) | 16 (+3) |

___Charge.___ If the unicorn moves at least 20 ft. straight toward a target and then hits it with a horn attack on the same turn, the target takes an extra 9 (2d8) piercing damage. If the target is a creature, it must succeed on a DC 15 Strength saving throw or be knocked prone.

___Innate Spellcasting.___ The unicorn's innate spellcasting ability is Charisma (spell save DC 14). The unicorn can innately cast the following spells, requiring no components:

* At will: _detect evil and good, druidcraft, pass without trace_

* 1/day each: _calm emotions, dispel evil and good, entangle_

___Magic Resistance.___ The unicorn has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The unicorn's weapon attacks are magical.

**Actions**

___Multiattack.___ The unicorn makes two attacks: one with its hooves and one with its horn.

___Hooves.___ Melee Weapon Attack: +7 to hit, reach 5 ft ., one target. Hit: 11 (2d6 + 4) bludgeoning damage.

___Horn.___ Melee Weapon Attack: +7 to hit, reach 5 ft ., one target. Hit: 8 (1d8 + 4) piercing damage.

___Healing Touch (3/Day).___ The unicorn touches another creature with its horn. The target magically regains 11 (2d8 + 2) hit points. In addition, the touch removes all diseases and neutralizes all poisons afflicting the target.

___Teleport (1/Day).___ The unicorn magically teleports itself and up to three willing creatures it can see within 5 ft. of it, along with any equipment they are wearing or carrying, to a location the unicorn is familiar with, up to 1 mile away.

**Legendary** Actions

The unicorn can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The unicorn regains spent legendary actions at the start of its turn.

___Hooves.___ The unicorn makes one attack with its hooves.

___Shimmering Shield (Costs 2 Actions).___ The unicorn creates a shimmering, magical field around itself or another creature it can see within 60 ft. of it. The target gains a +2 bonus to AC until the end of the unicorn's next turn.

___Heal Self (Costs 3 Actions).___ The unicorn magically regains 11 (2d8 + 2) hit points.
