"Living Unseen Servant";;;_page_number_: 313
_size_: Medium construct
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_languages_: "understands one language (usually Common) but can’t speak"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 12"
_skills_: "Perception +2, Stealth +4"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "4 (1d8)"
_armor class_: "10"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_stats_: | 2 (-4) | 10 (0) | 11 (0) | 1 (-5) | 10 (0) | 1 (-5) |

___Invisibility.___ The unseen servant is invisible.

**Actions**

___Slam.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 1 bludgeoning damage.