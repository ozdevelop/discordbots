"Greater Thaumaturmite";;;_size_: Medium monstrosity
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 7"
_condition_immunities_: "prone"
_speed_: "30 ft., climb 30 ft."
_hit points_: "27 (6d8)"
_armor class_: "15 (natural armor)"
_stats_: | 13 (+1) | 17 (+3) | 10 (+0) | 3 (-4) | 5 (-3) | 5 (-3) |

___Magic Scent.___ The thaumaturmite can pinpoint, by scent, the location of
any magical cloth within 30 feet of it.

___Consume Magic.___ Any magical cloth that the thaumaturmite hits starts
to corrode. Each time the thaumaturmite bites into
a magical cloth which is worn or carried,
the bearer of the item must make
a successful DC 12 Dexterity
saving throw or the item starts
corroding. After two failed
saving throws the item loses
its magical qualities.

___Spider Climb.___ The thaumaturmite can climb difficult surfaces, including
upside down on ceilings, without needing to make an ability check.

___Magic Resistance.___ The thaumaturmite has advantage on saving throws
against spells and other magical effects.

**Actions**

___Bite.___ Melee weapon attack: +6 to hit, reach 5 ft., one target. Hit:
25 (5d8+3) piercing damage.
