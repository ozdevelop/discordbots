"Elizar Dryflagon";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Druidic"
_skills_: "Arcana +3, Deception +3"
_speed_: "30 ft."
_hit points_: "71 (11d8+22)"
_armor class_: "14 (hide)"
_stats_: | 13 (+1) | 15 (+2) | 14 (+2) | 11 (0) | 18 (+4) | 10 (0) |

___Spellcasting.___ Elizar is a 7th-level spellcaster. His spellcasting ability is Wisdom (spell save DC 15, +7 to hit with spell attacks). Elizar has the following druid spells prepared:

* Cantrips (at will): _druidcraft, guidance, poison spray, produce flame_

* 1st level (4 slots): _animal friendship, faerie fire, healing word, jump, thunderwave_

* 2nd level (3 slots): _flame blade, spike growth_

* 3rd level (3 slots): _dispel magic, stinking cloud_

* 4th level (2 slots): _blight, wall of fire_

___Summon Mephits (Recharges after a Long Rest).___ By puffing on his pipe, Elizar can use an action to cast conjure minor elementals. If he does so, he summons four smoke mephits.

**Actions**

___Dagger +1.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.
