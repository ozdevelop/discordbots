"Giant Ape";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "7 (2,900 XP)"
_skills_: "Athletics +9, Perception +4"
_speed_: "40 ft., climb 40 ft."
_hit points_: "157 (15d12+60)"
_armor class_: "12"
_stats_: | 23 (+6) | 14 (+2) | 18 (+4) | 7 (-2) | 12 (+1) | 7 (-2) |

**Actions**

___Multiattack.___ The ape makes two fist attacks.

___Fist.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 22 (3d10 + 6) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +9 to hit, range 50/100 ft., one target. Hit: 30 (7d6 + 6) bludgeoning damage.