"Heir of the Forest";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages and Druidic"
_skills_: "Animal Handling +5, Nature +4, Perception +6, Survival +6"
_senses_: "passive Perception 16"
_saving_throws_: "Int +4, Wis +6"
_speed_: "30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "14 (leather)"
_stats_: | 13 (+1) | 16 (+3) | 14 (+2) | 12 (+1) | 16 (+3) | 12 (+1) |

___Land's Stride.___ Moving through nonmagical difficult
terrain does not cost the heir extra movement.

___Natural Recovery (1/Day).___ When the heir finishes a
short rest, it can regain up to 3 total expended spell
slots.

___Spellcasting.___ The heir is a 6th-level spellcaster. Its
spellcasting ability is Wisdom (spell save DC 14, +6
to hit with spell attacks). The heir has the following
druid spells prepared:

* Cantrips (at will): _create bonfire, druidcraft, shillelagh, thorn whip_

* 1st level (4 slots): _animal friendship, entangle, goodberry_

* 2nd level (3 slots): _animal messenger, barkskin, beast sense, locate animals or plants, spider climb_

* 3rd level (3 slots): _call lightning, plant growth, wind wall_

___Wild Shape - Dire Wolf (2/Day).___ As a bonus action,
the druid transforms into a dire wolf for up to three
hours. When the wolf is reduced to 0 hit points, the
druid reverts to its normal form, with any excess
damage carrying over to the druid's hit points.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +3 to hit, reach
5 ft., one target. Hit: 4 (1d6 + 1) bludgeoning
damage or 5 (1d8 +1) bludgeoning damage if used
with two hands.
