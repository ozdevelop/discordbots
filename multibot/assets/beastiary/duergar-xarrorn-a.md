"Duergar Xarrorn (A)";;;_size_: Medium humanoid (dwarf)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft."
_speed_: "25 ft."
_hit points_: "26 (4d8+8)"
_armor class_: "18 (platemail)"
_damage_resistances_: "poison"
_stats_: | 16 (+3) | 11 (0) | 14 (+2) | 11 (0) | 10 (0) | 9 (-1) |

___Duergar Resilience.___ The duergar has advantage on saving throws against poison, spells, and illusions, as well as to resist being charmed or paralyzed.

___Sunlight Sensitivity.___ While in sunlight, the duergar has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Enlarge (Recharges after a Short or Long Rest).___ For 1 minute, the duergar magically increases in size, along with anything it is wearing or carrying. While enlarged, the duergar is Large, doubles its damage dice on Strength-based weapon attacks (included in the attacks), and makes Strength checks and Strength saving throws with advantage. If the duergar lacks the room to become Large, it attains the maximum size possible in the space available.

___Fire Lance.___ Melee Weapon Attack: +5 to hit (with disadvantage if the target is within 5 feet of the duergar), reach 10 ft., one target. Hit: 9 (1d12 + 3) piercing damage plus 3 (1d6) fire damage, or 16 (2d12 + 3) piercing damage plus 3 (1d6) fire damage while enlarged.

___Fire Spray (Recharge 5-6).___ From its fire lance, the duergar shoots a 15-foot cone of fire or a line of fire 30 feet long and 5 feet wide. Each creature in that area must make a DC 12 Dexterity saving throw, taking 10 (3d6) fire damage on a failed save, or half as much damage on a successful one.

___Invisibility (Recharges after a Short or Long Rest).___ The duergar magically turns invisible until it attacks, casts a spell, or uses its Enlarge, or until its concentration is broken, up to 1 hour (as if concentrating on a spell). Any equipment the duergar wears or carries is invisible with it .