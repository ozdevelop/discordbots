"Miraj Vizann";;;_size_: Medium humanoid (earth genasi)
_alignment_: neutral evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Primordial"
_skills_: "Arcana +4, Deception +7"
_speed_: "30 ft."
_hit points_: "82 (11d8+33)"
_armor class_: "10 (13 with mage armor)"
_stats_: | 12 (+1) | 10 (0) | 17 (+3) | 13 (+1) | 11 (0) | 18 (+4) |

___Earth Walk.___ Moving through difficult terrain made of earth or stone costs Miraj no extra movement.

___Innate Spellcasting.___ Miraj's innate spellcasting ability is Constitution (spell save DC 14). He can innately cast the following spell, requiring no material components:

* 1/day: _pass without trace_

___Spellcasting.___ Miraj is an 11th-level spellcaster. His spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). He knows the following sorcerer spells:

* Cantrips (at will): _acid splash, blade ward, friends, light, message, mold earth_

* 1st level (4 slots): _chromatic orb, mage armor, magic missile_

* 2nd level (3 slots): _Maximilian's earthen grasp, shatter, suggestion_

* 3rd level (3 slots): _counterspell, erupting earth_

* 4th level (3 slots): _polymorph, stoneskin_

* 5th level (2 slots): _wall of stone_

* 6th level (1 slot): _move earth_

**Actions**

___Staff.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) bludgeoning damage, or 5 (1d8 + 1) bludgeoning damage when used with two hands.
