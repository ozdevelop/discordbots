"Ala";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "8 (3900 XP)"
_languages_: "Common, Draconic"
_skills_: "Athletics +8, Perception +9, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 19"
_damage_immunities_: "lightning, poison, thunder"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 40 ft."
_hit points_: "127 (15d8 + 60)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 16 (+3) | 18 (+4) | 10 (+0) | 16 (+3) | 8 (-1) |

___Flyby.___ The ala doesn't provoke an opportunity attack when it flies out of an enemy's reach.

___Poison Flesh.___ The ala's poison infuses its flesh. A creature that makes a successful bite attack against an ala must make a DC 16 Constitution saving throw; if it fails, the creature takes 10 (3d6) poison damage.

___Storm's Strength.___ If an electrical storm is raging around an ala and its target, the saving throw against Lightning's Kiss is made with disadvantage.

**Actions**

___Multiattack.___ The ala makes two claw attacks or one claw and one bite attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 10 (1d10 + 5) piercing damage, and the target must succeed on a DC 16 saving throw or take 10 (3d6) poison damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) slashing damage.

___Lightning's Kiss (Recharge 5-6).___ One target within 50 feet must make a DC 16 Dexterity saving throw. It takes 28 (8d6) lightning damage on a failed save, or half as much damage on a successful one.

