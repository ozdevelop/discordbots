"Stonemelder";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Terran"
_senses_: "tremorsense 30 ft."
_skills_: "Intimidation +5, Perception +2"
_speed_: "30 ft."
_hit points_: "75 (10d8+30)"
_armor class_: "17 (splint)"
_stats_: | 15 (+2) | 10 (0) | 16 (+3) | 12 (+1) | 11 (0) | 17 (+3) |

___Death Burst.___ When the Stonemelder dies, it turns to stone and explodes in a burst of rock shards, becoming a smoking pile of rubble. Each creature within 10 feet of the exploding Stonemelder must make a DC 14 Dexterity saving throw, taking 11 (2d10) bludgeoning damage on a failed save, or half as much damage on a successful one.

___Spellcasting.___ The Stonemelder is a 7th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _acid splash, blade ward, light, mending, mold earth_

* 1st level (4 slots): _expeditious retreat, false life, shield_

* 2nd level (3 slots): _Maximilian's earthen grasp, shatter_

* 3rd level (3 slots): _erupting earth, meld into stone_

* 4th level (1 slot): _stoneskin_

**Actions**

___Black Earth Rod.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage. The Stonemelder can also expend a spell slot to deal extra damage, dealing 2d8 bludgeoning damage for a 1st level slot, plus an additional 1d8 for each level of the slot above 1st.
