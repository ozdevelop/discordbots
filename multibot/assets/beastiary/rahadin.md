"Rahadin";;;_page_number_: 236
_size_: Medium humanoid (elf)
_alignment_: lawful evil
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Elvish"
_senses_: "darkvision 60 ft."
_skills_: "Deception +8, Insight +7, Intimidation +12, Perception +11, Stealth +14"
_saving_throws_: "Con +7, Wis +7"
_speed_: "35 ft."
_hit points_: "135 (18d8+54)"
_armor class_: "18 (studded leather)"
_stats_: | 14 (+2) | 22 (+6) | 17 (+3) | 15 (+2) | 16 (+3) | 18 (+4) |

___Deathly Choir.___ Any creature within 10 feet of Rahadin that isn't protected by a mind blank spell hears in its mind the screams of the thousands of people Rahadin has killed. As a bonus action, Rahadin can force all creatures that can hear the screams to make a DC 16 Wisdom saving throw. Each creature takes 16 (3d10) psychic damage on a failed save, or half as much damage on a successful one.

___Fey Ancestry.___ Rahadin has advantage on saving throws against being charmed, and magic can't put him to sleep.

___Innate Spellcasting.___ Rahadin's innate spellcasting ability is Intelligence (spell save DC 17, +9 to hit with spell attacks). He can innately cast the following spells, requiring no components:

* 3/day: _misty step, phantom steed_

* 1/day: _magic weapon, nondetection_

___Mask of the Wild.___ Rahadin can attempt to hide even when he is only lightly obscured by foliage, heavy rain, falling snow, mist, and other natural phenomena.

**Actions**

___Multiattack.___ Rahadin attacks three times with his scimitar, or twice with his poisoned darts.

___Scimitar.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 9 (1d6+6) slashing damage.

___Poisoned Dart.___ Ranged Weapon Attack: +10 to hit, range 20/60 ft., one target. Hit: 8 (1d4+6) piercing damage plus 5 (2d4) poison damage.
