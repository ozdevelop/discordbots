"Blood Orchid Savant";;;_size_: Large aberration
_alignment_: lawful evil
_challenge_: "7 (2,900 XP)"
_languages_: "telepathy 120 ft."
_skills_: "Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "thunder"
_damage_resistances_: "acid, cold, lightning, fire"
_speed_: "5 ft., fly 30 ft."
_hit points_: "97 (13d10 + 26)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 14 (+2) | 13 (+1) | 16 (+3) | 18 (+4) |

___Hyper-Awareness.___ The blood orchid cannot be surprised.

___Spellcasting.___ The blood orchid is a 4th level spellcaster. Its spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). It can cast the following spells:

* Cantrips (at will): _dancing lights, fire bolt, light, mage hand_

* 1st level (4 slots): _burning hands, color spray, detect magic, magic missile_

* 2nd level (3 slots): _darkness, ray of enfeeblement, scorching ray_

___Telepathic Bond.___ Blood orchids have a telepathic link to other blood orchids that are within 120 feet.

**Actions**

___Multiattack.___ The blood orchid uses Blood Drain. It then makes up to three attacks with its tentacles.

___Tentacles.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 11 (2d8 + 2) bludgeoning damage and the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour. The target is also grappled (escape DC 12). While grappled this way, the creature is restrained. Until the grapple ends, the blood orchid can’t use this tentacle
on another target. The blood orchid has three tentacles that it can attack with.

___Blood Drain.___ The blood orchid feeds on the creature it is grappling. The creature must succeed on a DC 13 Constitution saving throw or its hit point maximum is reduced by 5 (1d10). This reduction lasts until the creature finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.
