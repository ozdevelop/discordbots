"Tlincalli";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Tlincalli"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4, Stealth +4, Survival +4"
_speed_: "40 ft."
_hit points_: "85 (10d10+30)"
_armor class_: "15 (natural armor)"
_stats_: | 16 (+3) | 13 (+1) | 16 (+3) | 8 (-1) | 12 (+1) | 8 (-1) |

**Actions**

___Multiattack.___ The tlincalli makes two attacks: one with its longsword or spiked chain, and one with its sting.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d8+3) slashing damage, or 8 (1d10+3) slashing damage if used with two hands.

___Spiked Chain.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 6 (1d6+3) piercing damage, and the target is grappled (escape DC 11) if it is a Large or smaller creature. Until this grapple ends, the target is restrained, and the tlincalli can't use the spiked chain against another target.

___Sting.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 6 (1d6+3) piercing damage plus 14 (4d6) poison damage, and the target must succeed on a DC 14 Constitution saving throw or be poisoned for 1 minute. If it fails the saving throw by 5 or more, the target is also paralyzed while poisoned. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.