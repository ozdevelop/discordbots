"Allip";;;_page_number_: 116
_size_: Medium undead
_alignment_: neutral evil
_challenge_: "5 (1800 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 15"
_skills_: "Perception +5, Stealth +6"
_damage_immunities_: "cold, necrotic, poison"
_saving_throws_: "Int +6, Wis +5"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "40  (9d8)"
_armor class_: "13"
_condition_immunities_: "charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 6 (-2) | 17 (+3) | 10 (0) | 17 (+3) | 15 (+2) | 16 (+3) |

___Incorporeal Movement.___ The allip can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

**Actions**

___Maddening Touch___ Melee Spell Attack: +6 to hit, reach 5 ft., one target. Hit: 17 (4d6 + 3) psychic damage.

___Whispers of Madness___ The allip chooses up to three creatures it can see within 60 feet of it. Each target must succeed on a DC 14 Wisdom saving throw, or it takes 7 (1d8 + 3) psychic damage and must use its reaction to make a melee weapon attack against one creature of the allip's choice that the allip can see. Constructs and undead are immune to this effect.

___Howling Babble (Recharge 6)___ Each creature within 30 feet of the allip that can hear it must make a DC 14 Wisdom saving throw. On a failed save, a target takes 12 (2d8 + 3) psychic damage, and it is stunned until the end of its next turn. On a successful save, it takes half as much damage and isn't stunned. Constructs and undead are immune to this effect.