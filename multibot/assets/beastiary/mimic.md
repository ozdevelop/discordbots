"Mimic";;;_size_: Medium monstrosity (shapechanger)
_alignment_: neutral
_challenge_: "2 (450 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +5"
_damage_immunities_: "acid"
_speed_: "15 ft."
_hit points_: "58 (9d8+18)"
_armor class_: "12 (natural armor)"
_condition_immunities_: "prone"
_stats_: | 17 (+3) | 12 (+1) | 15 (+2) | 5 (-3) | 13 (+1) | 8 (-1) |

___Shapechanger.___ The mimic can use its action to polymorph into an object or back into its true, amorphous form. Its statistics are the same in each form. Any equipment it is wearing or carrying isn 't transformed. It reverts to its true form if it dies.

___Adhesive (Object Form Only).___ The mimic adheres to anything that touches it. A Huge or smaller creature adhered to the mimic is also grappled by it (escape DC 13). Ability checks made to escape this grapple have disadvantage.

___False Appearance (Object Form Only).___ While the mimic remains motionless, it is indistinguishable from an ordinary object.

___Grappler.___ The mimic has advantage on attack rolls against any creature grappled by it.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) bludgeoning damage. If the mimic is in object form, the target is subjected to its Adhesive trait.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage plus 4 (1d8) acid damage.