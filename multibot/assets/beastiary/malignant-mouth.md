"Malignant Mouth";;;_size_: Medium monstrosity
_alignment_: chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "all"
_skills_: "Performance +8"
_senses_: "tremorsense 120 ft., passive Perception 12"
_damage_resistances_: "necrotic, poison"
_condition_immunities_: "charmed, frightened"
_speed_: "0 ft."
_hit points_: "20 (4d8 + 2)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 6 (-2) | 9 (-1) | 16 (+3) | 14 (+2) | 14 (+2) |

___Magic Resistance.___ The malignant mouth
has advantage on saving throws against spells
and other magical effects.

**Actions**

___Bite.___ Melee weapon attack: +3 to hit,
range 5 ft., one target. Hit: 9 (2d8) slashing damage.

___Focused Shout.___ Ranged weapon attack: +6 to hit, range 60/120 ft., one target. Hit: 7 (2d6) force damage.

___Cackle (Recharge 5–6).___ All creatures
within a 30 ft. radius from mouth have
one of the following effects. Roll 1d6
separately for each target:

1. ___Confusion.___ Target must succeed on a DC 14 Wisdom
saving throw or move in a random direction each
turn, making a melee attack against the first target it
encounters for 1d6 rounds.

2. ___Fear.___ Target must succeed on a DC 14 Wisdom
saving throw or drop what it is holding and use its
action and move to dash away from the mouth. The
target can attempt a new saving throw at the end
of each of its turns, ending the effect on a success.

3. ___Sleep.___ If the target has less than 5d8 hit points, it falls
asleep as per the sleep spell.

4. ___Cure Wounds.___ The target is healed for 1d8+2 hit
points.

5. ___Vomit.___ The target must succeed on a DC 14
Dexterity saving throw or be immobilized for 2d4
rounds. On a successful saving throw, the target
moves at half speed for 1d4 rounds.

6. ___Tongue Lash.___ Ranged Weapon Attack: +8 to hit,
range 10/20 ft., one creature. Hit: the target is pulled
next to the malignant mouth, which can then make
a bite attack against it with advantage.
