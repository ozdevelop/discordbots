"Maiden of Ice";;;_size_: Huge giant
_alignment_: neutral good
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Giant"
_skills_: "Athletics +9, Insight +4 Intimidation +4, Perception +4"
_senses_: "darkvision 120 ft., passive Perception 14"
_saving_throws_: "Str +9, Con +10, Int +6"
_damage_immunities_: "cold"
_speed_: "40 ft."
_hit points_: "175 (14d12 + 84)"
_armor class_: "16 (natural armor)"
_stats_: | 21 (+5) | 16 (+3) | 22 (+6) | 15 (+2) | 11 (+0) | 10 (+0) |

___Aura of Frost.___ The ground in a 15 foot radius circle
around the maiden becomes coated in ice. A creature
that steps on this ice begins to slide. They travel until
they reach the end of the ice, collide with a solid
object, or have gone 30 feet. The creature loses
movement equal to the distance traveled while sliding.
A creature may choose to move at half speed on the
ice to avoid sliding. The ice melts after being outside of
the aura for 1 minute.

**Actions**

___Multiattack.___ The maiden makes two scythe attacks or
two spear attacks.

___Frozen Scythe.___ Melee Weapon Attack: +9 to hit, reach
10 ft., one target. Hit: 10 (1d10 + 5) slashing damage
plus 11 (2d10) cold damage.

___Frozen Spear.___ Melee or Ranged Weapon Attack: +9 to
hit, reach 10 ft. or range 30/90 ft., one target. Hit: 8
(1d6 + 5) piercing damage plus 11 (2d10) cold
damage, or 9 (1d8 + 5) piercing damage plus 11
(2d10) cold damage if used with two hands to make a
melee attack.

___Harvest.___ The maiden swings her scythe in a powerful
sweeping motion. All creatures within 15 feet of the
maiden must succeed on a DC 16 Dexterity saving
throw or take 16 (3d10) slashing damage and 16
(3d10) cold damage and be knocked back 15 feet. If
the creature is knocked back onto ice, they slide for 30
feet, until they collide with a solid object, or until they
reach the end of the ice.

___Blades of Bitter Cold (Recharge 5-6).___ The maiden makes
two powerful vertical sweeps with her scythe,
launching arcs of frozen energy in two 5 foot wide,
120-foot long lines. A creature caught in one of these
areas must make a DC 16 Dexterity saving throw,
taking 27 (5d10) cold damage and gaining one level of
exhaustion on a failed save, or half damage and no
exhaustion on a successful one.

___Cold Snap (1/Day).___ The maiden slams the head of her
scythe into the ground and attempts to freeze
everything within 60 feet. Creatures caught in this area
must make on a DC 16 Constitution saving throw or
take 33 (6d10) cold damage and be paralyzed for 1
minute as they are frozen on a failed save, or half as
much damage and not paralyzed on a successful one. A
paralyzed creature can repeat the saving throw at the
end of each of its turns, ending the paralyzed condition
on itself on a success. Any exposed liquids in the area
are frozen and all fire is snuffed out as well.
