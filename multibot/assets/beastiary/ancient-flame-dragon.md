"Ancient Flame Dragon";;;_size_: Gargantuan dragon
_alignment_: chaotic evil
_challenge_: "24 (62000 XP)"
_languages_: "Common, Draconic, Giant, Ignan, Infernal, Orc"
_skills_: "Deception +13, Insight +10, Perception +17, Persuasion +13, Stealth +9"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 27"
_saving_throws_: "Dex +9, Con +15, Wis +10, Cha +13"
_damage_immunities_: "fire"
_speed_: "40 ft., climb 40 ft., fly 80 ft."
_hit points_: "481 (26d20 + 208)"
_armor class_: "22 (natural armor)"
_stats_: | 23 (+6) | 14 (+2) | 27 (+8) | 19 (+4) | 16 (+3) | 22 (+6) |

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

___Fire Incarnate.___ All fire damage dealt by the dragon ignores fire resistance but not fire immunity.

___Flame Dragon's Lair.___ On initiative count 20 (losing initiative ties), the dragon takes a lair action to cause one of the following effects; the dragon can't use the same effect two rounds in a row.

- A cloud of smoke swirls in a 20-foot-radius sphere centered on a point the dragon can see within 120 feet of it. The cloud spreads around corners and the area is lightly obscured. Each creature in the cloud must succeed on a DC 15 Constitution saving throw or be blinded for 1 minute. A blinded creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

- The ground erupts with volcanic force at a point the dragon can see within 120 feet of it. Any creature within 20 feet of the point must make a successful DC 15 Dexterity saving throw or be knocked prone and trapped in the ground. A creature trapped in this way is restrained and can't stand up. A creature can end the restraint if it or another creature takes an action to make a successful DC 15 Strength check.

- A wall of fire rises up from the ground within 120 feet of the dragon. The wall is up to 60 feet long, 10 feet high, and 5 feet thick, can take any shape the dragon wants, and blocks line of sight. When the wall appears, each creature in its area must make a DC 15 Dexterity saving throw. A creature that fails the saving throw takes 21 (6d6) fire damage. Each creature that enters the wall for the first time each turn or ends its turn there takes 21 (6d6) fire damage. The wall is extinguished when the dragon uses this lair action again or when the dragon dies.

___Regional Effects.___ The region containing a legendary flame dragon's lair is warped by the dragon's magic, which creates one or more of the following effects:

- Arguments and misunderstandings erupt easily within 6 miles of the lair. Friendships are easily broken and criminal acts are common.

- Temperatures rise within 6 miles of the lair. Crops wither, producing famines.

- Sulfur geysers form in and around the dragon's lair. Some of them erupt only once an hour, so they're spotted only with a successful DC 20 Wisdom (Perception) check. A creature on top of an erupting geyser takes 21 (6d6) fire damage, or half damage with a successful DC 15 Dexterity saving throw.

If the dragon dies, the arguments and misunderstandings disappear immediately and the temperatures go back to normal within 1d10 days. Any geysers remain where they are.

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 17 (2d10 + 6) piercing damage plus 14 (4d6) fire damage.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Tail.___ Melee Weapon Attack: +13 to hit, reach 20 ft., one target. Hit: 15 (2d8 + 6) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 21 Wisdom saving throw or become frightened for 1 minute. A frightened creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours.

___Fire Breath (Recharge 5-6).___ The dragon exhales fire in a 90-foot cone. Each creature in that area takes 91 (26d6) fire damage, or half damage with a successful DC 23 Dexterity saving throw. Each creature in that area must also succeed on a DC 21 Wisdom saving throw or go on a rampage for 1 minute. A rampaging creature must attack the nearest living creature or smash some object smaller than itself if no creature can be reached with a single move. A rampaging creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Shifting Flames.___ The dragon magically polymorphs into a creature that has immunity to fire damage and a size and challenge rating no higher than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the dragon's choice). In a new form, the dragon retains its alignment, hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence, Wisdom, and Charisma scores, as well as this action. Its statistics and capabilities are otherwise replaced by those of the new form, except any class features or legendary actions of that form.

**Legendary** Actions

___The dragon can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each creature within 15 feet of the dragon must succeed on a DC 21 Dexterity saving throw or take 13 (2d6 + 6) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.

