"Elemental Air Dragon";;;_size_: Huge elemental
_alignment_: neutral evil
_challenge_: "17 (18,000 XP)"
_languages_: "Auran, Common"
_skills_: "Arcana +9, Nature +9, Perception +14, Stealth +11"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 24"
_saving_throws_: "Dex +11, Con +12, Wis +8, Cha +10"
_damage_immunities_: "poison"
_damage_resistances_: "lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "40 ft., fly 80 ft."
_hit points_: "312 (25d12 + 150)"
_armor class_: "15"
_stats_: | 27 (+8) | 20 (+5) | 23 (+6) | 16 (+3) | 15 (+2) | 19 (+4) |

___Innate Spellcasting.___ The elemental dragon’s innate
spellcasting is Charisma (spell save DC 18, +10 to hit with
spell attacks). It can cast the following spell requiring no
material components.

* At will: _gust of wind_

* 3/day each: _call lightning, wind wall_

* 1/day each: _control weather, plane shift_

___Legendary Resistance (3/day).___ If the dragon fails a saving
throw, it can choose to succeed instead.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target. Hit: 19
(2d10 + 8) piercing damage plus 10 (3d6) fire damage.

___Claw.___ Melee Weapon Attack: +14 to hit, reach 5 ft., one target. Hit: 15
(2d6 + 8) slashing damage.

___Tail.___ Melee Weapon Attack: +14 to hit, reach 15 ft., one target. Hit: 17
(2d8 + 8) bludgeoning damage.

___Scalding Breath (Recharge 5–6).___ The dragon releases a 60-foot cone of
superheated air that wraps around corners. Creatures within the area must
make a DC 21 Constitution saving throw, taking 63 (18d6) fire damage on
a failed saving throw, or half as much damage on a successful one.

___Whirlwind (Recharge 5–6).___ The dragon creates a cyclone in a 30-
foot radius centered on itself. Creatures, other than the dragon, within
the area can only move half their normal movement, nonmagical ranged
attacks automatically fail, and all nonmagical unprotected flames are
automatically extinguished. Large or smaller creatures within the area
must also succeed on a DC 21 Strength saving throw. On a failed saving
throw, the creature takes 20 (3d8 + 7) bludgeoning damage and is knocked
prone and pushed 30 in a random direction. On a successful saving throw,
the creature takes half damage and is not otherwise affected.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 21 Dexterity
saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.
