"Warhorse";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_speed_: "60 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "11"
_stats_: | 18 (+4) | 12 (+1) | 13 (+1) | 2 (-4) | 12 (+1) | 7 (-2) |

___Trampling Charge.___ If the horse moves at least 20 ft. straight toward a creature and then hits it with a hooves attack on the same turn, that target must succeed on a DC 14 Strength saving throw or be knocked prone. If the target is prone, the horse can make another attack with its hooves against it as a bonus action.

**Actions**

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.