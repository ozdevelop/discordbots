"Aerial Assault Gnome";;;_size_: Small humanoid (gnome)
_alignment_: chaotic neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Gnome"
_skills_: "History +6, Perception +6, Investigation +6, Acrobatics +8"
_senses_: "darkvision 60 ft., tremorsense 120 ft., passive Perception 12"
_speed_: "20 ft., fly 60 ft."
_hit points_: "44 (8d6 + 16)"
_armor class_: "18 (armored flight suit)"
_stats_: | 8 (-1) | 16 (+3) | 14 (+2) | 16 (+3) | 10 (+0) | 10 (+0) |

___Agile Flight.___ The gnome may use his mechawings to hover in place and may make hairpin turns, granting him an AC bonus of +2 while flying.

___Gnome Cunning.___ The aerial assault gnome has advantage on all spells and magical effects that require an Intelligence, Wisdom, or Charisma saving throw.

___Armored Flight Suit.___ The aerial assault gnome wears a futuristic suit of lightweight, yet extremely strong, mechanized armor that provides comfort and protection.

**Actions**

___Multiattack.___ The aerial assault gnome can make two attacks: one with his cog thrower and one with Last Resort.

___Cog Thrower.___ Ranged Weapon Attack: +8 to hit, range 80/320 ft., one target. Hit: 10 (3d6) slashing damage. A stream of razor-sharp cogs is propelled from the gnome’s hollow metallic club.

___Last Resort.___ Melee Weapon Attack: +4 to hit, range 5 ft., one target.
Hit: 3 (1d6) bludgeoning damage. The gnome whips his enemy with the still hot metal barrels of his cog thrower.

___Strafing Run (3/day).___ Ranged Weapon Attack: +8 to hit, range 120/400 ft., one target. Hit: a point the gnome can see. A blast of fire emanates from the target point. Any creatures in a 20-foot radius of the target must succeed on a DC 16 Dexterity saving throw, taking 28 (8d6) fire damage
on a failed save, or half as much damage on a successful one.

**Reactions**

___Point Defense.___ Any creature moving within 20 feet of the aerial assault gnome is blasted by his point defense cannons for 7 (3d4) piercing damage.
