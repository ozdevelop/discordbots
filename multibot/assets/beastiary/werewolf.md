"Werewolf";;;_size_: Medium humanoid (human shapechanger)
_challenge_: "3 (700 XP)"
_languages_: "Common (can't speak in wolf form)"
_skills_: "Perception +4"
_damage_immunities_: "bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_speed_: "30 ft. (40 ft. in wolf form)"
_hit points_: "58 (9d8+18)"
_armor class_: "11 (in humanoid form, 12 in wolf or hybrid form)"
_stats_: | 15 (+2) | 13 (+1) | 14 (+2) | 10 (0) | 11 (0) | 10 (0) |

___Shapechanger.___ The werewolf can use its action to polymorph into a wolf-humanoid hybrid or into a wolf, or back into its true form, which is humanoid. Its statistics, other than its AC, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Keen Hearing and Smell.___ The werewolf has advantage on Wisdom (Perception) checks that rely on hearing or smell.

**Actions**

___Multiattack (Humanoid or Hybrid Form Only).___ The werewolf makes two attacks: one with its bite and one with its claws or spear.

___Bite (Wolf or Hybrid Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage. If the target is a humanoid, it must succeed on a DC 12 Constitution saving throw or be cursed with werewolf lycanthropy.

___Claws (Hybrid Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 7 (2d4 + 2) slashing damage.

___Spear (Humanoid Form Only).___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one creature. Hit: 5 (1d6 + 2) piercing damage, or 6 (1d8 + 2) piercing damage if used with two hands to make a melee attack.