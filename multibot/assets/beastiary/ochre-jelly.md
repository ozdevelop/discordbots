"Ochre Jelly";;;_size_: Large ooze
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_damage_immunities_: "lightning, slashing"
_speed_: "10 ft., climb 10 ft."
_hit points_: "45 (6d10+12)"
_armor class_: "8"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_damage_resistances_: "acid"
_stats_: | 15 (+2) | 6 (-2) | 14 (+2) | 2 (-4) | 6 (-2) | 1 (-5) |

___Amorphous.___ The jelly can move through a space as narrow as 1 inch wide without squeezing.

___Spider Climb.___ The jelly can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage plus 3 (1d6) acid damage.

**Reactions**

___Split.___ When a jelly that is Medium or larger is subjected to lightning or slashing damage, it splits into two new jellies if it has at least 10 hit points. Each new jelly has hit points equal to half the original jelly's, rounded down. New jellies are one size smaller than the original jelly.