"The Angry";;;_page_number_: 231
_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "13 (10,000 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 16"
_skills_: "Perception +6"
_speed_: "30 ft."
_hit points_: "255  (30d8 + 120)"
_armor class_: "18 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing while in dim light or darkness"
_stats_: | 17 (+3) | 10 (0) | 19 (+4) | 8 (-1) | 13 (+1) | 6 (-2) |

___Two Heads.___ The Angry has advantage on Wisdom (Perception) checks and on saving throws against being blinded, charmed, deafened, frightened, stunned, or knocked unconscious.

___Rising Anger.___ If another creature deals damage to the Angry, the Angry's attack rolls have advantage until the end of its next turn, and the first time it hits with a hook attack on its next turn, the attack's target takes an extra 19 (3d12) psychic damage.
On its turn, the Angry has disadvantage on attack rolls if no other creature has dealt damage to it since the end of its last turn.

**Actions**

___Multiattack___ The Angry makes two hook attacks.

___Hook___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 16 (2d12 + 3) piercing damage.