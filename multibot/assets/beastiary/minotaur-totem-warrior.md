"Minotaur Totem Warrior";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal"
_skills_: "Athletics +6, Perception +7"
_senses_: "darkvision 60ft., passive Perception 17"
_speed_: "40 ft."
_hit points_: "85 (10d10 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 10 (+0) | 17 (+3) | 6 (-2) | 16 (+3) | 8 (-1) |

___Labyrinthine Recall.___ The minotaur can perfectly recall
any path it has traveled.

___Battering Ram.___ If the minotaur moves at least 10
feet straight towards a target and then hits it with a
totem attack on the same turn, the target takes an
extra 9 (2d8) bludgeoning damage. If the target is a
creature, it must succeed on a DC 14 Strength
saving throw or be pushed up to 20 feet and
knocked prone. If the target is a structure, this
attack deals double damage.

**Actions**

___Totem.___ Melee Weapon Attack: +6 to hit, reach 10
ft., one target. Hit: 17 (2d12 + 4) bludgeoning
damage.

___Totemic Smash (Recharge 5-6).___ The minotaur
smashes its totem on the ground, causing a
shockwave to ripple forth in a 15-foot cone. Each
creature in this area must make a DC 14 Dexterity
saving throw, taking 11 (2d10) bludgeoning
damage and 11 (2d10) thunder damage on a failed
saving throw, or half as much damage on a
successful one. Creatures that fail this saving throw
by 5 or more also fall prone.

**Reactions**

___Totemic Armor.___ The minotaur stands strong and
calls upon its inner strength to toughen its flesh
from incoming attacks. When an enemy hits the
minotaur with an attack that it can see, the
minotaur can use its reaction to halve the damage
of that attack.
