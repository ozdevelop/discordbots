"Manticore";;;_size_: Large monstrosity
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_senses_: "darkvision 60 ft."
_speed_: "30 ft., fly 50 ft."
_hit points_: "68 (8d10+24)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 16 (+3) | 17 (+3) | 7 (-2) | 12 (+1) | 8 (-1) |

___Tail Spike Regrowth.___ The manticore has twenty-four tail spikes. Used spikes regrow when the manticore finishes a long rest.

**Actions**

___Multiattack.___ The manticore makes three attacks: one with its bite and two with its claws or three with its tail spikes.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Tail Spike.___ Ranged Weapon Attack: +5 to hit, range 100/200 ft., one target. Hit: 7 (1d8 + 3) piercing damage.