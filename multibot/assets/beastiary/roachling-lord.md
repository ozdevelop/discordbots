"Roachling Lord";;;_size_: Small humanoid
_alignment_: chaotic neutral
_challenge_: "2 (450 XP)"
_languages_: "Common"
_skills_: "Acrobatics +5, Stealth +7"
_senses_: "darkvision 60 ft., tremorsense 10 ft., passive Perception 9"
_saving_throws_: "Dexterity +5, Constitution +3"
_speed_: "25 ft."
_hit points_: "63 (14d6 + 14)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (+0) | 16 (+3) | 12 (+1) | 10 (+0) | 10 (+0) | 10 (+0) |

___Resistant.___ The roachling skirmisher has advantage on Constitution saving throws.

___Unlovely.___ The skirmisher has disadvantage on Performance and Persuasion checks in interactions with nonroachlings.

**Actions**

___Multiattack.___ The roachling lord makes two melee attacks or throws two darts.

___Begrimed Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 7 (2d6) poison damage.

___Begrimed Dart.___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage plus 7 (2d6) poison damage.

