"Light Devourer";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "blindsight 90 ft., passive Perception 10"
_speed_: "40 ft."
_hit points_: "60 (11d8 + 11)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 16 (+4) | 12 (+1) | 14 (+2) | 10 (+0) | 8 (-1) |

___Shrouded in Darkness.___ The area in a 30 foot area
around the devourer is shrouded in magical
darkness and cannot be pierced by any light,
magical or mundane.

**Actions**

___Multiattack.___ The devourer makes three attacks, one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 7 (1d8 + 3) piercing damage and
the target must succeed on a DC 13 Constitution
saving throw or be blinded until the end of their
next turn.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 6 (1d6 + 3) slashing damage.

___Consume Sight (Recharge 5-6).___ The light devourer
drains any and all light from within its shroud of
darkness. All other creatures in that area must
succeed on a DC 13 Constitution saving throw or
be blinded. A blinded creature can repeat the saving
throw at the end of each of its turns, ending the
blinded condition on itself on a success.
