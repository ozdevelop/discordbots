"Armanite";;;_page_number_: 131
_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 11"
_damage_immunities_: "poison"
_speed_: "60 ft."
_hit points_: "84  (8d10 + 40)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 21 (+5) | 18 (+4) | 21 (+5) | 8 (-1) | 12 (+1) | 13 (+1) |

___Magic Resistance.___ The armanite has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The armanite's weapon attacks are magical.

**Actions**

___Multiattack___ The armanite makes three attacks: one with its hooves, one with its claws, and one with its serrated tail.

___Hooves___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage.

___Claws___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 10 (2d4 + 5) slashing damage.

___Serrated Tail___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) slashing damage.

___Lightning Lance (Recharge 5-6)___ The armanite looses a bolt of lightning in a line 60 feet long and 10 feet wide. Each creature in the line must make a DC 15 Dexterity saving throw, taking 27 (6d8) lightning damage on a failed save, or half as much damage on a successful one.