"Darkling Elder";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "2 (450 XP)"
_languages_: "Elvish, Sylvan"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Acrobatics +5, Deception +3, Perception +6, Stealth +7"
_speed_: "30 ft."
_hit points_: "27 (5d8+5)"
_armor class_: "15 (studded leather armor)"
_stats_: | 13 (+1) | 17 (+3) | 12 (+1) | 10 (0) | 14 (+2) | 13 (+1) |

___Death Burn.___ When the darkling elder dies, magical light flashes out from it in a 10-foot radius as its body and possessions, other than metal or magic objects, burn to ash. Any creature in that area must make a DC 11 Constitution saving throw. On a failure, the creature takes 7 (2d6) radiant damage and, if the creature can see the light, is blinded until the end of its next turn. If the saving throw is successful, the creature takes half the damage and isn't blinded.

**Actions**

___Multiattack.___ The darkling elder makes two melee attacks.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 fit, one target. Hit: 6 (1d6+3) piercing damage. If the darkling elder had advantage on the attack roll, the attack deals as: extra 10 (3d6) piercing damage.

___Darkness (Recharges after a Short or Long Rest).___ The darkling elder casts darkness without any components. Wisdom is its spellcasting ability.