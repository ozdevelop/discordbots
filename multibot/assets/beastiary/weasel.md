"Weasel";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_skills_: "Perception +3, Stealth +5"
_speed_: "30 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "13"
_stats_: | 3 (-4) | 16 (+3) | 8 (-1) | 2 (-4) | 12 (+1) | 3 (-4) |

___Keen Hearing and Smell.___ The weasel has advantage on Wisdom (Perception) checks that rely on hearing or smell.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 1 piercing damage.