"Balor";;;_size_: Huge fiend (demon)
_alignment_: chaotic evil
_challenge_: "19 (22,000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "truesight 120 ft."
_damage_immunities_: "fire, poison"
_saving_throws_: "Str +14, Con +12, Wis +9, Cha +12"
_speed_: "40 ft., fly 80 ft."
_hit points_: "262 (21d12+126)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 26 (+8) | 15 (+2) | 22 (+6) | 20 (+5) | 16 (+3) | 22 (+6) |

___Death Throes.___ When the balor dies, it explodes, and each creature within 30 feet of it must make a DC 20 Dexterity saving throw, taking 70 (20d6) fire damage on a failed save, or half as much damage on a successful one. The explosion ignites flammable objects in that area that aren't being worn or carried, and it destroys the balor's weapons.

___Fire Aura.___ At the start of each of the balor's turns, each creature within 5 feet of it takes 10 (3d6) fire damage, and flammable objects in the aura that aren't being worn or carried ignite. A creature that touches the balor or hits it with a melee attack while within 5 feet of it takes 10 (3d6) fire damage.

___Magic Resistance.___ The balor has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The balor's weapon attacks are magical.

**Actions**

___Multiattack.___ The balor makes two attacks: one with its longsword and one with its whip.

___Longsword.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target. Hit: 21 (3d8 + 8) slashing damage plus 13 (3d8) lightning damage. If the balor scores a critical hit, it rolls damage dice three times, instead of twice.

___Whip.___ Melee Weapon Attack: +14 to hit, reach 30 ft., one target. Hit: 15 (2d6 + 8) slashing damage plus 10 (3d6) fire damage, and the target must succeed on a DC 20 Strength saving throw or be pulled up to 25 feet toward the balor.

___Teleport.___ The balor magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.

___Variant: Summon Demon (1/Day).___ The demon chooses what to summon and attempts a magical summoning.

A balor has a 50 percent chance of summoning 1d8 vrocks, 1d6 hezrous, 1d4 glabrezus, 1d3 nalfeshnees, 1d2 mariliths, or one goristro.

A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.