"Stirge";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_senses_: "darkvision 60 ft."
_speed_: "10 ft., fly 40 ft."
_hit points_: "2 (1d4)"
_armor class_: "14 (natural armor)"
_stats_: | 4 (-3) | 16 (+3) | 11 (0) | 2 (-4) | 8 (-1) | 6 (-2) |

**Actions**

___Blood Drain.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 5 (1d4 + 3) piercing damage, and the stirge attaches to the target. While attached, the stirge doesn't attack. Instead, at the start of each of the stirge's turns, the target loses 5 (1d4 + 3) hit points due to blood loss.

The stirge can detach itself by spending 5 feet of its movement. It does so after it drains 10 hit points of blood from the target or the target dies. A creature, including the target, can use its action to detach the stirge.