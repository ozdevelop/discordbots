"Rakshasa";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Infernal"
_senses_: "darkvision 60 ft."
_skills_: "Deception +10, Insight +8"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "40 ft."
_hit points_: "110 (13d8+52)"
_armor class_: "16 (natural armor)"
_damage_vulnerabilities_: "piercing from magic weapons wielded by good creatures"
_stats_: | 14 (+2) | 17 (+3) | 18 (+4) | 13 (+1) | 16 (+3) | 20 (+5) |

___Limited Magic Immunity.___ The rakshasa can't be affected or detected by spells of 6th level or lower unless it wishes to be. It has advantage on saving throws against all other spells and magical effects.

___Innate Spellcasting.___ The rakshasa's innate spellcasting ability is Charisma (spell save DC 18, +10 to hit with spell attacks). The rakshasa can innately cast the following spells, requiring no material components:

* At will: _detect thoughts, disguise self, mage hand, minor illusion_

* 3/day each: _charm person, detect magic, invisibility, major image, suggestion_

* 1/day each: _dominate person, fly, plane shift, true seeing_

**Actions**

___Multiattack.___ The rakshasa makes two claw attacks

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage, and the target is cursed if it is a creature. The magical curse takes effect whenever the target takes a short or long rest, filling the target's thoughts with horrible images and dreams. The cursed target gains no benefit from finishing a short or long rest. The curse lasts until it is lifted by a remove curse spell or similar magic.
