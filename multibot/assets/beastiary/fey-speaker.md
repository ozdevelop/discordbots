"Fey Speaker";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Deception +5, Intimidation +5, Religion +4"
_senses_: "passive Perception 10"
_saving_throws_: "Wis +2, Cha +5"
_speed_: "30 ft."
_hit points_: "26 (4d8 + 8)"
_armor class_: "11"
_stats_: | 10 (+0) | 13 (+1) | 15 (+2) | 14 (+2) | 10 (+0) | 16 (+3) |

___Fey Presence (1/Short Rest).___ As an action, the
speaker can cause each creature in a 10-foot cube
origination from it to make a DC 13 Wisdom saving
throw. The creatures that fail their save are all either
charmed or frightened by the speaker until the end
of the speaker’s next turn.

___Empowered Eldritch Blasts.___ The speaker adds its
charisma modifier to its eldritch blast attacks. In
addition, creatures hit by the speaker's eldritch
blast are pushed up to 10 feet away from the
speaker in a straight line.

___Spellcasting.___ The speaker is a 3th-level spellcaster.
Its spellcasting ability is Charisma (spell save DC
13, +5 to hit with spell attacks). It regains its
expended spellslots when it finishes a short or long
rest. The speaker has the following warlock spells
prepared:

* Cantrips (at will): _eldritch blast, friends, minor illusion_

* 1st-2nd level (2 2nd-level slots): _calm emotions, faerie fire, hex, mirror image, phantasmal force, sleep, Tasha's hideous laughter_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 4
(1d4 + 2) piercing damage.
