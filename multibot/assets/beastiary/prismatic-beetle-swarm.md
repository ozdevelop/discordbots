"Prismatic Beetle Swarm";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +5"
_senses_: "blindsight 10 ft., darkvision 30 ft., passive Perception 13"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "20 ft., burrow 5 ft., fly 30 ft."
_hit points_: "38 (7d8 + 7)"
_armor class_: "13"
_stats_: | 3 (-4) | 16 (+3) | 12 (+1) | 1 (-5) | 13 (+1) | 2 (-4) |

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny insect. The swarm can't regain hit points or gain temporary hit points.

___Glittering Carapace.___ The glossy, iridescent carapaces of the beetles in the swarm scatter and tint light in a dazzling exhibition of colors. In bright light, a creature within 30 feet that looks at the prismatic beetle swarm must make a successful DC 13 Wisdom saving throw or be blinded until the end of its next turn. If the saving throw fails by 5 or more, the target is also knocked unconscious. Unless it's surprised, a creature can avoid the saving throw by choosing to avert its eyes at the start of its turn. A creature that averts its eyes can't see the swarm until the start of its next turn, when it can choose to avert its eyes again. If the creature looks at the swarm in the meantime, it must immediately make the saving throw. The saving throw is made with advantage if the swarm of prismatic beetles is in dim light, and this ability has no effect if the swarm is in darkness.

**Actions**

___Bites.___ Melee Weapon Attack: +5 to hit, reach 0 ft., one creature in the swarm's space. Hit: 10 (4d4) piercing damage, or 5 (2d4) piercing damage if the swarm has half of its hit points or fewer. The target also takes 10 (4d4) poison damage and becomes euphoric for 1d4 rounds, or takes half as much poison damage and is not euphoric if it makes a successful DC 11 Constitution saving throw. A euphoric creature has disadvantage on saving throws.

