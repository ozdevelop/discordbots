"Tribal Warrior";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1/8 (25 XP)"
_languages_: "any one language"
_speed_: "30 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "12 (hide armor)"
_stats_: | 13 (+1) | 11 (0) | 12 (+1) | 8 (-1) | 11 (0) | 8 (-1) |

___Pack Tactics.___ The warrior has advantage on an attack roll against a creature if at least one of the warrior's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d6 + 1) piercing damage, or 5 (1d8 + 1) piercing damage if used with two hands to make a melee attack.