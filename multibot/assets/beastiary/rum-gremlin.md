"Rum Gremlin";;;_size_: Tiny fey
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 10"
_condition_immunities_: "poisoned"
_speed_: "20 ft., climb 10 ft., swim 10 ft."
_hit points_: "22 (5d4 + 10)"
_armor class_: "13"
_stats_: | 10 (+0) | 16 (+3) | 14 (+2) | 12 (+1) | 9 (-1) | 12 (+1) |

___Innate Spellcasting.___ The gremlin's innate spellcasting ability is Charisma (spell save DC 11). It can innately cast the following spells, requiring no material components:

* At will: _prestidigitation_

* 3/day: _hex_

___Magic Resistance.___ The gremlin has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The goblin makes one claw attack and one bite attack.

___Bite.___ Melee Weapon Attack: +5 to hit, range 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, range 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Aura of Drunkenness.___ A rum gremlin radiates an aura of drunkenness to a radius of 20 feet. Every creature that starts its turn in the aura must make a successful DC 12 Constitution saving throw against poison or be poisoned for one hour.

