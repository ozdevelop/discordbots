"Greater Fire Crab";;;_size_: Huge elemental
_alignment_: unaligned
_challenge_: "10 (5,900 XP)"
_languages_: "understands Ignan but can't speak"
_skills_: "Athletics +10"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 13"
_damage_immunities_: "fire"
_damage_vulnerabilities_: "cold"
_condition_immunities_: "charmed"
_speed_: "40 ft., swim 40 ft."
_hit points_: "173 (15d12 + 75)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 10 (+0) | 20 (+5) | 2 (-4) | 16 (+3) | 2 (-4) |

___Heated Body.___ A creature that touches the fire crab or hits it with a melee
attack while within 5 feet of it takes 14 (4d6) fire damage.

**Actions**

___Multiattack.___ The greater fire crab makes two claw attacks

___Claws.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 12 (1d12 + 6) bludgeoning damage plus 14 (4d6) fire damage. If both claw attacks hit in the same turn and the target is Huge size or smaller, the target is grappled (escape DC 16). While grappled, the target is restrained and takes 14 (4d6) fire damage at the start of its turn. The fire crab can only grapple one target at a time and cannot perform a claw attack while grappling a target.

___Constrict.___ If a target is grappled, the greater fire crab squeezes it and the target
takes 12 (1d12 + 6) bludgeoning damage and 14 (4d6) fire damage.
