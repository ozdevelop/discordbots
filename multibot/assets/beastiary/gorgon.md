"Gorgon";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4"
_damage_immunities_: "petrified"
_speed_: "40 ft."
_hit points_: "114 (12d10+48)"
_armor class_: "19 (natural armor)"
_stats_: | 20 (+5) | 11 (0) | 18 (+4) | 2 (-4) | 12 (+1) | 7 (-2) |

___Trampling Charge.___ If the gorgon moves at least 20 feet straight toward a creature and then hits it with a gore attack on the same turn, that target must succeed on a DC 16 Strength saving throw or be knocked prone. If the target is prone, the gorgon can make one attack with its hooves against it as a bonus action.

**Actions**

___Gore.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 18 (2d12 + 5) piercing damage.

___Hooves.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 16 (2d10 + 5) bludgeoning damage.

___Petrifying Breath (Recharge 5-6).___ The gorgon exhales petrifying gas in a 30-foot cone. Each creature in that area must succeed on a DC 13 Constitution saving throw. On a failed save, a target begins to turn to stone and is restrained. The restrained target must repeat the saving throw at the end of its next turn. On a success, the effect ends on the target. On a failure, the target is petrified until freed by the greater restoration spell or other magic.