"Dissimortuum";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Con +6"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft., climb 30 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 10 (+0) | 16 (+3) | 8 (-1) | 11 (+0) | 18 (+4) |

___Spider Climb.___ The dissimortuum can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack.___ The dissimortuum makes three claw attacks.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 15 (3d8 + 2) slashing damage.

___Terrifying Mask.___ Each non-undead creature within 60 feet of the dissimortuum that can see it must make a successful DC 15 Wisdom saving throw or be frightened for 1d8 rounds. If a target's saving throw is successful or the effect ends for it, the target becomes immune to all dissimortuum's terrifying masks for the next 24 hours.

