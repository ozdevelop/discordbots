"Ogremoch";;;_size_: Gargantuan elemental
_alignment_: neutral evil
_challenge_: "20 (25,000 XP)"
_languages_: "Common, Terran"
_senses_: "blindsight 120 ft., tremorsense 120 ft."
_damage_immunities_: "poison"
_saving_throws_: "Str +14, Con +15, Wis +8"
_speed_: "50 ft., burrow 50 ft."
_hit points_: "526 (27d20+243)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned, prone"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 26 (+8) | 11 (0) | 28 (+9) | 11 (0) | 15 (+2) | 22 (+6) |

___Empowered Attacks.___ Ogremoch's slam attacks are treated as magical and adamantine for the purpose of bypassing resistance and immunity to nonmagical weapons.

___Innate Spellcasting.___ Ogremoch's innate spellcasting ability is Charisma (spell save DC 20, +12 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

* At will: _meld into stone, move earth, wall of stone_

___Legendary Resistance (3/Day).___ If Ogremoch fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Ogremoch has advantage on saving throws against spells and other magical effects.

___Siege Monster.___ Ogremoch deals double damage to objects and structures with his melee and ranged weapon attacks.

**Actions**

___Multiattack.___ Ogremoch makes two slam attacks.

___Slam.___ Melee Weapon Attack: +14 to hit, reach 15 ft., one target. Hit: 30 (4d10 + 8) bludgeoning damage.

___Boulder.___ Ranged Weapon Attack: +6 to hit, range 500 ft., one target. Hit: 46 (7d10 + 8) bludgeoning damage. If the target is a creature, it must succeed on a DC 23 Strength saving throw or be knocked prone.

___Summon Elementals (1/Day).___ Ogremoch summons up to three earth elementals and loses 30 hit points for each elemental he summons. Summoned elementals have maximum hit points, appear within 100 feet of Ogremoch, and disappear if Ogremoch is reduced to 0 hit points.

**Legendary** Actions

The ogremoch can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The ogremoch regains spent legendary actions at the start of its turn.

___Illuminating Crystals.___ Ogremoch's crystalline protrusions flare. Each creature within 30 feet of Ogremoch becomes outlined in orange light, shedding dim light in a 10-foot radius. Any attack roll against an affected creature has advantage if the attacker can see it, and the affected creature can't benefit from being invisible.

___Stomp (Costs 2 Actions).___ Ogremoch stomps the ground, creating an earth tremor that extends in a 30-foot radius. Other creatures standing on the ground in that radius must succeed on a DC 23 Dexterity saving throw or fall prone.

___Create Gargoyle (Costs 3 Actions).___ Ogremoch's hit points are reduced by 50 as he breaks off a chunk of his body and places it on the ground in an unoccupied space within 15 feet of him. The chunk of rock instantly transforms into a gargoyle and acts on the same initiative count as Ogremoch. Ogremoch can't use this action if he has 50 hit points or fewer. The gargoyle obeys Ogremoch's commands and fights until destroyed.
