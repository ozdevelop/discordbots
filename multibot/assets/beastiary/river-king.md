"River King";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "16 (15000 XP)"
_languages_: "Common, Elemental, Elvish, Giant"
_skills_: "Intimidation +8, Nature +7, Perception +6, Stealth +8"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +8, Con +10, Wis +6"
_damage_immunities_: "lightning"
_damage_resistances_: "fire, cold, thunder; bludgeoning, piercing, and slashing damage from weapons that aren't cold iron"
_condition_immunities_: "exhaustion"
_speed_: "30 ft., swim 40 ft."
_hit points_: "152 (16d8 + 80)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 17 (+3) | 20 (+5) | 14 (+2) | 13 (+1) | 17 (+3) |

___Amphibious.___ The River King can breathe air and water.

___Innate Spellcasting.___ The River King's innate spellcasting ability score is Charisma (save DC 16, +8 to hit with spell attacks). The River King can innately cast the following spells, requiring no material components.

At will: create or destroy water, shocking grasp, water breathing

3/day each: freedom of movement, control water

1/day: chain lightning

___Legendary Resistance (3/day).___ If the River King fails a saving throw, he can choose to succeed instead.

___Magic Weapons.___ The River King's weapon attacks are magical and do an extra 10 (3d6) lightning damage (included below).

___The River King's Lair.___ On initiative count 20 (losing initiative ties), the River King takes a lair action to cause one of the following effects; the River King can't use the same effect two rounds in a row:

- The fey river swells and rushes over the land, or the walls of the Hall buckle and allow the torrent in. The River King chooses a 10-foot-wide path up to 60 feet long. Each creature in the path of the water must make a DC 15 Strength saving throw. Creatures that fail the save are swept 20 feet down the path of the water and take 9 (2d8) bludgeoning damage.

- The River King targets a creature he can see within 60 feet. That creature's mouth and throat fill with river water. The creature must make a DC 15 Constitution saving throw. On a success, the creature manages to hold its breath, but on a failure, it begins to suffocate. A suffocating but conscious creature repeats the saving throw at the end of its turn, ending the effect on itself on a success. Another creature within 5 feet can use an action to make a DC 15 Wisdom (Medicine) check. On a success, the suffocating creature coughs up the water and the effect ends. Otherwise the effect lasts until the River King uses this action again or dies.

- The River King targets a creature he can see within 120 feet. His commanding presence overwhelms the creature, who must make a DC 15 Charisma saving throw. On a failure, the creature is charmed by the River King until initiative count 20 on the following round.

___Regional Effects.___ The region containing the River King's lair is warped by its magic, which creates one or more of the following effects:

- The current of rivers and streams within 6 miles of the lair becomes strong and erratic. Creatures without a swim speed who start their turns in running water must succeed on a DC 15 Strength (Athletics) check or be swept 60 feet downriver.

- Lakes, ponds, rivers, and streams within 6 miles of the lair teem with fish and other wildlife.

- Rain and thunderstorms are common within 6 miles of the lair, and often build to torrential downpours that create heavy obscurement and cause waterways to overflow their banks.

If the River King dies, conditions in the area surrounding the lair return to normal over the course of 1d10 days.

**Actions**

___Multiattack.___ The River King makes three attacks with his longsword and/or flood blast, in any combination.

___Longsword.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) slashing damage or 10 (1d10 + 5) slashing damage if used with two hands, plus 10 (3d6) lightning damage.

___Flood Blast.___ Ranged Spell Attack: +8 to hit, range 120 ft., one target. Hit: 18 (4d8) bludgeoning damage. A target creature must succeed on a DC 16 Strength saving throw or be knocked prone and pushed up to 60 feet at the River King's choosing.

___Grasping Whirlpool (Recharge 5-6).___ The River King magically creates a swirling vortex of water centered on a point he can see within 60 feet. The vortex fills a cylinder with a 10-foot radius and 15 feet high. Creatures in the area must make a successful DC 16 Strength saving throw or take 11 (2d10) bludgeoning damage and be restrained and unable to breathe. On a successful save, the creature is pushed to the edge of the area. A restrained creature can escape from the whirlpool by using an action to make a successful DC 16 Strength check. A creature that's in the whirlpool at the end of its turn takes 11 (2d10) bludgeoning damage in addition to any effects from being unable to breathe. Creatures with a swim speed have advantage on the saving throw and the Strength check to escape. The whirlpool lasts for 1 minute or until the River King uses this ability again.

**Reactions**

___Blade Current.___ When a creature within the River King's melee reach stands up from prone, the River King can make a longsword attack on that creature with advantage.

**Legendary** Actions

___The River King can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The River King regains spent legendary actions at the start of his turn.

___Longsword.___ The River King makes a longsword attack.

___Flow.___ The River King moves half his speed without provoking opportunity attacks.

___Ripple (2 Actions).___ The River King magically ripples like sunlight on the surface of water. Until the start of his next turn, all attacks against him have disadvantage.

