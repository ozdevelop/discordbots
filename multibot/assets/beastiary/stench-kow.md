"Stench Kow";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "15 (2d10+4)"
_armor class_: "10"
_damage_resistances_: "cold, fire, poison"
_stats_: | 18 (+4) | 10 (0) | 14 (+2) | 2 (-4) | 10 (0) | 4 (-3) |

___Charge.___ If the kow moves at least 20 feet straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 7 (2d6) piercing damage.

___Stench.___ Any creature other than a stench kow that starts its turn within 5 feet of the stench kow must succeed on a DC 12 Constitution saving throw or be poisoned until the start of the creature's next turn. On a successful saving throw, the creature is immune to the stench of all stench kows for 1 hour.

**Actions**

___Gore.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6+4) piercing damage.