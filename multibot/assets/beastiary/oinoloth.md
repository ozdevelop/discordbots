"Oinoloth";;;_page_number_: 251
_size_: Medium fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "12 (8400 XP)"
_languages_: "Abyssal, Infernal, telepathy 60 ft."
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 17"
_skills_: "Deception +8, Intimidation +8, Perception +7"
_damage_immunities_: "acid, poison"
_saving_throws_: "Con +8, Wis +7"
_speed_: "40 ft."
_hit points_: "126  (12d10 + 60)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 19 (+4) | 17 (+3) | 18 (+4) | 17 (+3) | 16 (+3) | 19 (+4) |

___Bringer of Plagues (Recharge 5-6).___ As a bonus action, the oinoloth blights the area within 30 feet of it. The blight lasts for 24 hours. While blighted, all normal plants in the area wither and die, and the number of hit points restored by a spell to a creature in that area is halved.
Furthermore, when a creature moves into the blighted area or starts its turn there, that creature must make a DC 16 Constitution saving throw. On a successful save, the creature is immune to the oinoloth's Bringer of Plagues for the next 24 hours. On a failed save, the creature takes 14 (4d6) necrotic damage and is poisoned.
The poisoned creature can't regain hit points. After every 24 hours that elapse, the poisoned creature can repeat the saving throw. On a failed save, the creature's hit point maximum is reduced by 5 (1d10). This reduction lasts until the poison ends, and the target dies if its hit point maximum is reduced to 0. The poison ends after the creature successfully saves against it three times.

___Innate Spellcasting.___ The oinoloth's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _darkness, detect magic, dispel magic, invisibility _(self only)

* 1/day each: _feeblemind, globe of invulnerability, wall of fire, wall of ice_

___Magic Resistance.___ The oinoloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The oinoloth's weapon attacks are magical.

**Actions**

___Multiattack___ The oinoloth uses its Transfixing Gaze and makes two claw attacks.

___Claw___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (3d6 + 4) slashing damage plus 22 (4d10) necrotic damage.

___Corrupted Healing (Recharge 6)___ The oinoloth touches one willing creature within 5 feet of it. The target regains all its hit points. In addition, the oinoloth can end one disease on the target or remove one of the following conditions from it: blinded, deafened, paralyzed, or poisoned. The target then gains 1 level of exhaustion, and its hit point maximum is reduced by 7 (2d6). This reduction can be removed only by a wish spell or by casting greater restoration on the target three times within the same hour. The target dies if its hit point maximum is reduced to 0.

___Teleport___ The oinoloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.

___Transfixing Gaze___ The oinoloth targets one creature it can see within 30 feet of it. The target must succeed on a DC 16 Wisdom saving throw against this magic or be charmed until the end of the oinoloth's next turn. While charmed in this way, the target is restrained. If the target's saving throw is successful, the target is immune to the oinoloth's gaze for the next 24 hours.
