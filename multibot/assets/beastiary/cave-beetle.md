"Cave Beetle";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Perception +2"
_senses_: "blindsight 60 ft. (blind beyond this radius), tremorsense 120 ft., passive Perception 12"
_speed_: "30 ft."
_hit points_: "31 (7d6 + 7)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 14 (+2) | 13 (+1) | 1 (-5) | 11 (+0) | 2 (-4) |

___Death Throes.___ When a giant cave beetle reaches 0 hit points, it releases
a strong acidic cloud in a 5-foot radius. Creatures within this area must
make a DC 11 Constitution saving throw, taking 7 (2d6) acid damage on a
failed saving throw, or half as much damage on a successful saving throw.
Every giant cave beetle in 500 feet is capable of detecting the smell of
this acid.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2)
slashing damage.
