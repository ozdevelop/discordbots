"Oni";;;_size_: Large giant
_alignment_: lawful evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +5, Deception +8, Perception +4"
_saving_throws_: "Dex +3, Con +6, Wis +4, Cha +5"
_speed_: "30 ft., fly 30 ft."
_hit points_: "110 (13d10+39)"
_armor class_: "16 (chain mail)"
_stats_: | 19 (+4) | 11 (0) | 16 (+3) | 14 (+2) | 12 (+1) | 15 (+2) |

___Innate Spellcasting.___ The oni's innate spellcasting ability is Charisma (spell save DC 13). The oni can innately cast the following spells, requiring no material components:

* At will: _darkness, invisibility_

* 1/day each: _charm person, cone of cold, gaseous form, sleep_

___Magic Weapons.___ The oni's weapon attacks are magical.

___Regeneration.___ The oni regains 10 hit points at the start of its turn if it has at least 1 hit point.

**Actions**

___Multiattack.___ The oni makes two attacks, either with its claws or its glaive.

___Claw (Oni Form Only).___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage.

___Glaive.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 15 (2d10 + 4) slashing damage, or 9 (1d10 + 4) slashing damage in Small or Medium form.

___Change Shape.___ The oni magically polymorphs into a Small or Medium humanoid, into a Large giant, or back into its true form. Other than its size, its statistics are the same in each form. The only equipment that is transformed is its glaive, which shrinks so that it can be wielded in humanoid form. If the oni dies, it reverts to its true form, and its glaive reverts to its normal size.
