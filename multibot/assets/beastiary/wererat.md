"Wererat";;;_size_: Medium humanoid (human shapechanger)
_challenge_: "2 (450 XP)"
_languages_: "Common (can't speak in rat form)"
_senses_: "darkvision 60 ft. (rat form only)"
_skills_: "Perception +2, Stealth +4"
_damage_immunities_: "bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_speed_: "30 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "12"
_stats_: | 10 (0) | 15 (+2) | 12 (+1) | 11 (0) | 10 (0) | 8 (-1) |

___Shapechanger.___ The wererat can use its action to polymorph into a rat-humanoid hybrid or into a giant rat, or back into its true form, which is humanoid. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Keen Smell.___ The wererat has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Multiattack (Humanoid or Hybrid Form Only).___ The wererat makes two attacks, only one of which can be a bite.

___Bite (Rat or Hybrid Form Only)..___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage. If the target is a humanoid, it must succeed on a DC 11 Constitution saving throw or be cursed with wererat lycanthropy.

___Shortsword (Humanoid or Hybrid Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Hand Crossbow (Humanoid or Hybrid Form Only).___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage.