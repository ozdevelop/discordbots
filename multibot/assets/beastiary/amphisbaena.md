"Amphisbaena";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "20 ft., climb 20 ft., swim 30 ft."
_hit points_: "60 (8d10 + 16)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 14 (+2) | 2 (-4) | 12 (+1) | 2 (-4) |

___Split.___ The amphisbaena functions normally even if cut
in half. If dealt a critical hit with a slashing weapon,
the creature is cut in half and continues to function
as two separate creatures, each with half of the
original amphisbaena’s current hit points. The
split amphisbaena can rejoin its two halves after
completing a short or long rest. If one of the
split creatures is slain, the amphisbaena can
regrow the lost portion over the course of 1d4 + 2 weeks.

**Actions**

___Multiattack.___ The amphisbaena makes one
bite attack with each of its two heads.

___Bite.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one target. Hit: 12 (2d8 + 3)
piercing damage, and the target must succeed
on a DC 13 Constitution saving throw or
become poisoned for 1 hour.
