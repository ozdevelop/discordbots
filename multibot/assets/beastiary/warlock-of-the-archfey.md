"Warlock of the Archfey";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "4 (1,100 XP)"
_languages_: "any two languages (usually Sylvan)"
_skills_: "Arcana +2, Deception +6, Nature +2, Persuasion +6"
_saving_throws_: "Wis +3, Cha +6"
_speed_: "30 ft."
_hit points_: "49 (11d8)"
_armor class_: "11 (14 with mage armor)"
_condition_immunities_: "charmed"
_stats_: | 9 (-1) | 13 (+1) | 11 (0) | 11 (0) | 12 (+1) | 18 (+4) |

___Innate Spellcasting.___ The warlock's innate spellcasting ability is Charisma. It can innately cast the following spells (spell save DC 15), requiring no material components:

* At will: _disguise self, mage armor _(self only)_, silent image, speak with animals_

* 1/day: _conjure fey_

___Spellcasting.___ The warlock is a 11th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _dancing lights, eldritch blast, friends, mage hand, minor illusion, prestidigitation, vicious mockery_

* 1st-5th level (3 5th-level slots): _blink, charm person, dimension door, dominate beast, faerie fire, fear, hold monster, misty step, phantasmal force, seeming, sleep_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4+2) piercing damage.

**Reactions**

___Misty Escape (Recharges after a Short or Long Rest).___ In response to taking damage, the warlock turns invisible and teleports up to 60 feet to an unoccupied space it can see. It remains invisible until the start of its next turn or until it attacks, makes a damage roll, or casts a spell.
