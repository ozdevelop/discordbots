"Shadow Dancer";;;_page_number_: 225
_size_: Medium humanoid (elf)
_alignment_: neutral
_challenge_: "7 (2900 XP)"
_languages_: "Common, Elvish"
_senses_: "darkvision 60 ft., passive Perception 11"
_skills_: "Stealth +6"
_saving_throws_: "Dex +6, Cha +4"
_speed_: "30 ft."
_hit points_: "71  (13d8 + 13)"
_armor class_: "15 (studded leather)"
_condition_immunities_: "charmed, exhaustion"
_damage_resistances_: "necrotic"
_stats_: | 12 (+1) | 16 (+3) | 13 (+1) | 11 (0) | 12 (+1) | 12 (+1) |

___Fey Ancestry.___ The shadow dancer has advantage on saving throws against being charmed, and magic can't put it to sleep.

___Shadow Jump.___ As a bonus action, the shadow dancer can teleport up to 30 feet to an unoccupied space it can see. Both the space it teleports from and the space it teleports to must be in dim light or darkness. The shadow dancer can use this ability between the weapon attacks of another action it takes.

**Actions**

___Multiattack___ The shadow dancer makes three spiked chain attacks.

___Spiked Chain___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 10 (2d6 + 3) piercing damage, and the target must succeed on a DC 14 Dexterity saving throw or suffer one additional effect of the shadow dancer's choice:
* The target is grappled (escape DC 14) if it is a Medium or smaller creature. Until the grapple ends, the target is restrained, and the shadow dancer can't grapple another target.
* The target is knocked prone.
* The target takes 22 (4d10) necrotic damage.
