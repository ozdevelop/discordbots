"Crimson Basilisk";;;_size_: Medium monstrosity
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "20 ft."
_hit points_: "67 (9d8 + 27)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 16 (+3) | 2 (-4) | 12 (+1) | 10 (+0) |

___Blood Frenzy.___ The crimson basilisk has advantage on attack rolls
against any creature that doesn’t have all its hit points.

___Wounding Gaze.___ If a target starts its turn within 30 feet of the basilisk
and the two of them can see each other, the basilisk can force the target to
make a DC 12 Constitution saving throw if the basilisk isn’t incapacitated.
On a failed save, the target takes 7 (2d6) necrotic damage as blood weeps
from the victim’s eyes, ears, nose, and mouth. It must repeat the saving
throw at the end of its next turn. On a success, the effect ends. On a failure,
the target takes an additional 7 (2d6) necrotic damage and will continue
to take recurring damage at the end of each of its turns (without further
chance to save) unless the basilisk is incapacitated or the effect is ended
by the greater restoration spell or other magic.

A creature that isn’t surprised can avert its eyes to avoid the saving
throw at the start of its turn. If it does so, it can’t see the creature until
the start of its next turn, when it can avert its eyes again. If it looks at the
basilisk in the meantime, it must immediately make the save.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6
+ 3) piercing damage and 7 (2d6) acid damage. In addition, nonmagical
armor or a shield being worn or carried is partly dissolved and takes a
permanent and cumulative –1 penalty to the AC it offers. Armor reduced
to an AC of 10 or a shield that drops to a +0 bonus is destroyed.
