"Blister Beetle";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 10"
_damage_immunities_: "acid, poison"
_speed_: "30 ft."
_hit points_: "13 (3d6 + 3)"
_armor class_: "12 (natural armor)"
_stats_: | 11 (+0) | 10 (+0) | 12 (+1) | 1 (-5) | 10 (+0) | 7 (-2) |

**Actions**

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 3 (1d6)
piercing damage plus 3 (1d6) acid damage.

___Blister Spray (Recharge 5–6).___ The beetle exhales a 15-foot cone of
caustic acid, causing painful blisters to form on the skin of any creature
caught in the spray. Creatures in the area must make a DC 12 Dexterity
saving throw, taking 10 (3d6) acid damage on a failed save, or half as
much damage on a successful one.
