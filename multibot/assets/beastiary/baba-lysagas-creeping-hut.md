"Baba Lysaga's Creeping Hut";;;_page_number_: 226
_size_: Gargantuan construct
_alignment_: unaligned
_challenge_: "11 (7,200 XP)"
_languages_: "-"
_senses_: "blindsight 120 ft. (blind beyond this radius)"
_damage_immunities_: "poison, psychic"
_saving_throws_: "Con +9, Wis +0, Cha +0"
_speed_: "30 ft."
_hit points_: "263 (17d10+85)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, prone"
_stats_: | 26 (+8) | 7 (-2) | 20 (+5) | 1 (-5) | 3 (-4) | 3 (-4) |

___Constructed Nature.___ An animated object doesn't require air, food, drink, or sleep.

The magic that animates an object is dispelled when the construct drops to 0 hit points. An animated object reduced to 0 hit points becomes inanimate and is too damaged to be of much use or value to anyone.

___Antimagic Susceptibility.___ The hut is incapacitated while the magic gem that animates it is in the area of an antimagic field. If targeted by dispel magic, the hut must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute.

___Siege Monster.___ The hut deals double damage to objects and structures.

**Actions**

___Multiattack.___ The hut makes three attacks with its roots. It can replace one of these attacks with a rock attack.

___Root.___ Melee Weapon Attack: +12 to hit, reach 60 ft., one target. Hit: 30 (4d10+8) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +12 to hit, range 120 ft., one target. Hit: 21 (3d8+8) bludegoning damage.
