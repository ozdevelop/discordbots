"Eonic Drifter";;;_size_: Medium humanoid
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Eonic, Giant, Sylvan"
_skills_: "Arcana +6, History +6"
_senses_: ", passive Perception 10"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "13 (leather armor)"
_stats_: | 9 (-1) | 14 (+2) | 14 (+2) | 18 (+4) | 11 (+0) | 13 (+1) |

**Actions**

___Multiattack.___ The eonic drifter can either use Drift Backward or make two attacks with its time warping staff. The eonic drifter's future self (if present) can only use Drift Forward.

___Time Warping Staff.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) bludgeoning damage.

___Drift Backward (1/Day).___ A future self of the eonic drifter materializes in an unoccupied space within 30 feet of the drifter. The future self has the eonic drifter's stats and its full hit points, and it takes its turn immediately after its present self. Killing the original eonic drifter makes its future self disappear. If the present self sees its future self die, the eonic drifter must make a DC 13 Wisdom saving throw. There is no effect if the save succeeds. If the saving throw fails, roll 1d6 to determine the effect on the eonic drifter: 1 = frightened, 2 = incapacitated, 3 = paralyzed, 4 = unconscious, 5 or 6 = has disadvantage on attack rolls and ability checks. These effects last 1d4 rounds.

___Drift Forward (2/Day).___ The future self makes a time warping staff attack against a target. If the attack hits, instead of causing bludgeoning damage, both the target and the attacker jump forward through time, effectively ceasing to exist in the present time. They reappear in the same locations 1d4 rounds later, at the end of the present self's turn. Creatures occupying those locations at that moment are pushed 5 feet in a direction of their own choosing. The target of the drift (but not the future self) must then make a DC 13 Wisdom saving throw, with effects identical to those for the eonic drifter witnessing the death of its future self (see Drift Backward). The future self doesn't reappear after using this ability the second time; only the target of the drift reappears from the second use. This does not trigger a saving throw for the present self.

