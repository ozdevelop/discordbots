"Eye of the Deep";;;_size_: Medium aberration
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Aquan, Common, Deep Speech"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "5 ft., swim 20 ft."
_hit points_: "117 (18d8 + 36)"
_armor class_: "14 (natural armor)"
_stats_: | 14 (+2) | 10 (+0) | 14 (+2) | 12 (+1) | 13 (+1) | 13 (+1) |

___Amphibious.___ The eye of the deep can breathe in both air and water.

___Flyby.___ The eye of the deep doesn’t provoke an opportunity attack when
it flies out of an enemy’s reach.

___Hyper-Awareness.___ An eye of the deep’s eye stalks allow it to see in all
directions at once. It cannot be surprised.

___Stun Cone.___ An eye of the deep’s central eye produces a cone extending
straight ahead from its front to a range of 30 feet. At the start of each
of its turns, the eye of the deep decides which way the cone faces and
whether the cone is active. All creatures in this area must succeed on a DC
15 Constitution saving throw or be stunned for 1 minute. A creature can
repeat the saving throw at the end of each of its turns, ending the effect on
itself on a success.

**Actions**

___Multiattack.___ The eye of the deep makes three attacks: one with its bite
and two with its pincers.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12
(3d6 + 2) piercing damage.

___Pincers.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit:
15 (3d8 + 2) bludgeoning damage. The target is grappled (escape DC 12)
if the eye of the deep isn’t already grappling a creature, and the target is
restrained until the grapple ends.

___Eye Rays.___ Each of the creature’s eyes stalks can produce a magical ray
once per round. The creature can aim both of its eye rays in any direction
and they have a range of 150 feet.

1. ___Paralytic Ray.___ Using its left eye, the eye of the deep unleashes a
powerful paralytic beam. The target must make a DC 15 Wisdom saving
throw or be paralyzed for 1 minute. A creature can repeat the saving throw
at the end of each of its turns, ending the effect on itself on a success.

2. ___Enfeeblement Ray.___ Using its right eye, the eye of the deep unleashes
a powerful ray of enfeeblement. The target must make a DC 15 Wisdom
saving throw or deal half damage with all attacks that use Strength for 1
minute. A creature can repeat the saving throw at the end of each of its
turns, ending the effect on itself on a success.

3. ___Major Image.___ The eye of the deep concentrates its eye rays together
to project a major image illusion. The illusion is generated at any point
within range and in the eye of the deep’s line of sight. Seeing through the
illusion requires a successful DC 15 Intelligence (Investigation) check.
