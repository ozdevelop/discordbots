"Tyrannosaurus Rex";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "8 (3,900 XP)"
_skills_: "Perception +4"
_speed_: "50 ft."
_hit points_: "136 (13d12+52)"
_armor class_: "13 (natural armor)"
_stats_: | 25 (+7) | 10 (0) | 19 (+4) | 2 (-4) | 12 (+1) | 9 (-1) |

**Actions**

___Multiattack.___ The tyrannosaurus makes two attacks: one with its bite and one with its tail. It can't make both attacks against the same target.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 33 (4d12 + 7) piercing damage. If the target is a Medium or smaller creature, it is grappled (escape DC 17). Until this grapple ends, the target is restrained, and the tyrannosaurus can't bite another target.

___Tail.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 20 (3d8 + 7) bludgeoning damage.