"Ankylosaurus Zombie";;;_size_: Huge undead
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "darkvision 60 ft., passive Perception 8"
_condition_immunities_: "poisoned"
_damage_immunities_: "poison"
_speed_: "20 ft."
_hit points_: "68 (8d12 +16)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 9 (-1) | 15 (+2) | 2 (-4) | 6 (-2) | 3 (-4) |

___Undead fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Tail.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 18 (4d6 +4) bludgeoning damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be knocked prone.