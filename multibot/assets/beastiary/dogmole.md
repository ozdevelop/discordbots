"Dogmole";;;_size_: Medium beast
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft., passive Perception 11"
_speed_: "30 ft., burrow 10 ft., swim 10 ft."
_hit points_: "71 (11d8 + 22)"
_armor class_: "14 (natural armor)"
_stats_: | 14 (+2) | 17 (+3) | 15 (+2) | 2 (-4) | 12 (+1) | 10 (+0) |

___Burrow.___ Dogmoles cannot burrow into solid rock, but they can move through softer material like soil or loose rubble, leaving a usable tunnel 5 feet in diameter.

___Wormkiller Rage.___ Wild dogmole packs are famed for their battles against monsters in the dark caverns of the world. If the dogmole draws blood against vermin, a purple worm, or other underground invertebrates, it gains a +4 boost to its Strength and Constitution, but suffers a -2 penalty to its AC. The wormkiller rage lasts for 3 rounds. It cannot end the rage voluntarily while the creatures that sent it into a rage still lives.

**Actions**

___Multiattack.___ The dogmole makes one claw attack and one bite attack.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 12 (3d6 +2) slashing damage.

