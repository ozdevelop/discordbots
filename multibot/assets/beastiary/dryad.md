"Dryad";;;_size_: Medium fey
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "Elvish, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4, Stealth +5"
_speed_: "30 ft."
_hit points_: "22 (5d8)"
_armor class_: "11 (16 with barkskin)"
_stats_: | 10 (0) | 12 (+1) | 11 (0) | 14 (+2) | 15 (+2) | 18 (+4) |

___Innate Spellcasting.___ The dryad's innate spellcasting ability is Charisma (spell save DC 14). The dryad can innately cast the following spells, requiring no material components:

* At will: _druidcraft_

* 3/day each: _entangle, goodberry_

* 1/day each: _barkskin, pass without trace, shillelagh_

___Magic Resistance.___ The dryad has advantage on saving throws against spells and other magical effects.

___Speak with Beasts and Plants.___ The dryad can communicate with beasts and plants as if they shared a language.

___Tree Stride.___ Once on her turn, the dryad can use 10 ft. of her movement to step magically into one living tree within her reach and emerge from a second living tree within 60 ft. of the first tree, appearing in an unoccupied space within 5 ft. of the second tree. Both trees must be large or bigger.

**Actions**

___Club.___ Melee Weapon Attack: +2 to hit (+6 to hit with shillelagh), reach 5 ft., one target. Hit: 2 (1 d4) bludgeoning damage, or 8 (1d8 + 4) bludgeoning damage with shillelagh.

___Fey Charm.___ The dryad targets one humanoid or beast that she can see within 30 feet of her. If the target can see the dryad, it must succeed on a DC 14 Wisdom saving throw or be magically charmed. The charmed creature regards the dryad as a trusted friend to be heeded and protected. Although the target isn't under the dryad's control, it takes the dryad's requests or actions in the most favorable way it can.

Each time the dryad or its allies do anything harmful to the target, it can repeat the saving throw, ending the effect on itself on a success. Otherwise, the effect lasts 24 hours or until the dryad dies, is on a different plane of existence from the target, or ends the effect as a bonus action. If a target's saving throw is successful, the target is immune to the dryad's Fey Charm for the next 24 hours.

The dryad can have no more than one humanoid and up to three beasts charmed at a time.
