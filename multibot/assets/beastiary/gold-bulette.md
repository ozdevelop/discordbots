"Gold Bulette";;;_size_: Huge monstrosity
_alignment_: chaotic good
_challenge_: "17 (18,000 XP)"
_languages_: "Common, Draconic, Dwarven, Elven, Goblin, Orc"
_skills_: "Perception +14"
_senses_: "truesight 120 ft., passive Perception 24"
_saving_throws_: "Dex +9, Wis +8"
_speed_: "60 ft., burrow 60 ft."
_hit points_: "232 (16d12 + 128)"
_armor class_: "20 (natural armor)"
_stats_: | 24 (+7) | 16 (+3) | 26 (+8) | 18 (+4) | 14 (+2) | 16 (+3) |

___Detect Evil and Good.___ The bulette knows if there is an aberration, celestial, elemental, fey, fiend, or undead within 30 feet of it and where the creature is located. The bulette also knows if there is a place or object within 30 feet of it that has been consecrated or desecrated.

___Goldbringer.___ If killed, the body of a gold bulette flashes brightly in a nova light that blinds all who can see it permanently (or until cured), which its body condenses into a gold-plated sandstone statue-like version of itself. Stripping off its outer plating and breaking up its remaining body
will yield the equivalent of 50,000 gold pieces and 25–50 gemstones of varying value.

___Karmic Curse.___ Killing a gold bulette permanently entangles all creatures present at the event with tiny planar threads binding them to both the positive and negative planes of existence. This curse will manifest itself as advantage or disadvantage on all future saving throws made by the affected bulette slayers, with a 50% chance at the time each saving throw is made of the effect being beneficial or baleful.
The curse can only be removed by the
successful casting of a remove curse spell
performed while on the astral plane.

___Innate Spellcasting.___ The bulette’s innate
spellcasting ability is Charisma (spell save DC
17, +9 to hit with spell attacks). It can cast the
following spells at will without requiring material
components: 

disintegrate, fireball, fly, heal, lightning bolt, magic missile, teleport.

___Magic Resistance.___ The bulette has advantage on saving throws against spells and other magical effects.

___Multi-Planar Connection.___ Because their arcane natures tie gold bulettes to both the positive and negative planes, they also simultaneously exist on both of those planes as either a mediumsized globule of light or shadow, respectively.

___Unlimited Spell Use.___ The gold bulette has multiple spells encoded into its brain stem, including (but not limited to) the spells listed above. Because the gold bulette’s multi-planar connection acts as a source of magic and power, it can use each spell as many times per days as desired.

**Actions**

___Bite.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 59 (8d12 + 7) piercing damage.
