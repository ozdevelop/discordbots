"Zombie Warrior";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "51 (6d10 + 18)"
_armor class_: "16 (chainmail)"
_stats_: | 16 (+3) | 10 (+0) | 16 (+3) | 6 (-2) | 10 (+0) | 6 (-2) |

___Undead Fortitude.___ If damage reduces the zombie to 0
hit points, it must make a Constitution saving throw
with a DC of 5 + the damage taken, unless the damage
is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.


**Actions**

___Greatsword.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 10 (2d6 + 3) slashing damage.

___Spew Bile (recharge 5-6).___ The zombie spews bile in 15-
foot line. Each creature in that line must make a DC 12
Constitution saving throw, taking 17 (5d6) necrotic
damage on a failed save, or half as much damage on a
successful one.
