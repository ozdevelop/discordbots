"Brass Man";;;_size_: Large construct
_alignment_: neutral
_challenge_: "9 (5,000 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "136 (16d10 + 48)"
_armor class_: "16 (natural armor)"
_stats_: | 21 (+5) | 10 (+0) | 16 (+3) | 2 (-4) | 11 (+0) | 1 (-5) |

___Fire Absorption.___ Whenever the brass man is subjected to fire damage, it
takes no damage and instead regains a number of hit points equal to the
fire damage dealt.

___Immutable Form.___ The brass man is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The brass man has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The brass man’s weapon attacks are magical.

**Actions**

___Multiattack.___ The brass man makes two greatsword attack and one slam
attack.

___Greatsword.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target.
Hit: 18 (4d6 + 5) slashing damage.

___Slam.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 18
(3d8 + 5) bludgeoning damage.

___Molten Breath (Recharge 6).___ The brass man exhales molten brass in a
25-foot line. Each creature along that line must make a DC 13 Dexterity
saving throw, taking 28 (8d6) fire damage on a failed save, or half as much
on a successful one.
