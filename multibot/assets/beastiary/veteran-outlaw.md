"Veteran Outlaw";;;_size_: Medium humanoid
_alignment_: any non-lawful alignment
_challenge_: "3 (700 XP)"
_languages_: "Any one language (usually Common)"
_skills_: "Deception +5, Insight +6, Sleight of Hand +7"
_senses_: "passive Perception 12"
_speed_: "30 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "15 (studded leather)"
_stats_: | 16 (+3) | 16 (+3) | 14 (+2) | 10 (+0) | 14 (+2) | 13 (+1) |

___Experienced Insight.___ The outlaw has advantage on
initiative rolls.

___Throwing Specialty.___ A thrown weapon deals one
extra die of its damage when the outlaw hits with it
(included in the attack).

**Actions**

___Multiattack.___ The outlaw makes two melee attacks or
three ranged attacks with its daggers. No more than
one of these attacks can be a poisoned dagger.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 5
(1d4 + 3) piercing damage or 8 (2d4 + 3) piercing
damage if thrown.

___Poisoned Dagger (3/Day).___ Melee or Ranged Weapon
Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one
target. Hit: 5 (1d4 + 3) piercing damage or 8 (2d4 + 3) piercing damage if thrown, and the target must
succeed on a DC 13 Constitution saving throw or
take 7 (2d6) poison damage
