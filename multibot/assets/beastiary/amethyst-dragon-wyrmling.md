"Amethyst Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Draconic"
_skills_: "Insight +3, Perception +3"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 15"
_saving_throws_: "Dex +4, Int +4, Wis +3, Cha +3"
_damage_vulnerabilites_: "psychic"
_speed_: "30 ft., fly 60 ft. (hover)"
_hit points_: "36 (8d8)"
_armor class_: "17 (natural armor)"
_stats_: | 14 (+2) | 14 (+2) | 11 (+0) | 14 (+2) | 13 (+1) | 13 (+1) |

___Feedback Aura.___ Each time the dragon takes
damage, all creatures within 30 feet must succeed
on a DC 12 Intelligence saving throw or else take
3 (1d6) psychic damage.

**Psionics**

___Charges:___ 8 | ___Recharge:___ 1d4 | ___Fracture:___ 7

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 7 (1d10 + 2) piercing damage.
