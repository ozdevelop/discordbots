"Clockwork Huntsman";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "understands Common"
_skills_: "Perception +4, Survival +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Str +5, Dex +4"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "110 (20d8 + 20)"
_armor class_: "14"
_stats_: | 17 (+3) | 14 (+2) | 12 (+1) | 4 (-3) | 10 (+0) | 1 (-5) |

___Immutable Form.___ The clockwork huntsman is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The clockwork huntsman has advantage on saving throws against spells and other magical effects.

**Actions**

___Longsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage.

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage.

___Net Cannon.___ Ranged Weapon Attack: +4 to hit, range 5/15 ft., one target, size Large or smaller. Hit: the target is restrained. A mechanism within the clockwork huntsman's chest can fire a net with a 20-foot trailing cable anchored within the huntsman's chest. A creature can free itself (or another creature) from the net by using its action to make a successful DC 10 Strength check or by dealing 5 slashing damage to the net. The huntsman can fire up to four nets before it must be reloaded.

___Explosive Core.___ The mechanism that powers the huntsman explodes when the construct is destroyed, projecting superheated steam and shrapnel. Every creature within 5 ft. of the construct takes 10 (3d6) fire damage, or half damage with a successful DC 13 Dexterity saving throw.

