"Dao";;;_size_: Large elemental
_alignment_: neutral evil
_challenge_: "11 (7,200 XP)"
_languages_: "Terran"
_senses_: "darkvision 120 ft."
_saving_throws_: "Int +5, Wis +5, Cha +6"
_speed_: "30 ft., burrow 30 ft., fly 30 ft."
_hit points_: "187 (15d10+105)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "petrified"
_stats_: | 23 (+6) | 12 (+1) | 24 (+7) | 12 (+1) | 13 (+1) | 14 (+2) |

___Earth Glide.___ The dao can burrow through nonmagical, unworked earth and stone. While doing so, the dao doesn't disturb the material it moves through.

___Elemental Demise.___ If the dao dies, its body disintegrates into crystalline powder, leaving behind only equipment the dao was wearing or carrying.

___Innate Spellcasting.___ The dao's innate spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: detect evil and good, detect magic, stone shape_

* 3/day each: _passwall, move earth, tongues_

* 1/day each: _conjure elemental _(earth elemental only)_, gaseous form, invisibility, phantasmal killer, plane shift, wall of stone_

___Sure-Footed.___ The dao has advantage on Strength and Dexterity saving throws made against effects that would knock it prone.

___Variant: Genie Powers.___ Genies have a variety of magical capabilities, including spells. A few have even greater powers that allow them to alter their appearance or the nature of reality.

___Disguises.___

Some genies can veil themselves in illusion to pass as other similarly shaped creatures. Such genies can innately cast the disguise self spell at will, often with a longer duration than is normal for that spell. Mightier genies can cast the true polymorph spell one to three times per day, possibly with a longer duration than normal. Such genies can change only their own shape, but a rare few can use the spell on other creatures and objects as well.

___Wishes.___

The genie power to grant wishes is legendary among mortals. Only the most potent genies, such as those among the nobility, can do so. A particular genie that has this power can grant one to three wishes to a creature that isn't a genie. Once a genie has granted its limit of wishes, it can't grant wishes again for some amount of time (usually 1 year). and cosmic law dictates that the same genie can expend its limit of wishes on a specific creature only once in that creature's existence.

To be granted a wish, a creature within 60 feet of the genie states a desired effect to it. The genie can then cast the wish spell on the creature's behalf to bring about the effect. Depending on the genie's nature, the genie might try to pervert the intent of the wish by exploiting the wish's poor wording. The perversion of the wording is usually crafted to be to the genie's benefit.

**Actions**

___Multiattack.___ The Dao makes two fist attacks or two maul attacks.

___Fist.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 15 (2d8 + 6) bludgeoning damage.

___Maul.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 20 (4d6 + 6) bludgeoning damage. If the target is a Huge or smaller creature, it must succeed on a DC 18 Strength check or be knocked prone.
