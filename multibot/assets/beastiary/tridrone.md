"Tridrone";;;_size_: Medium construct
_alignment_: construct
_challenge_: "1/2 (100 XP)"
_languages_: "Modron"
_senses_: "truesight 120 ft."
_speed_: "30 ft."
_hit points_: "16 (3d8+3)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 13 (+1) | 12 (+1) | 9 (-1) | 10 (0) | 9 (-1) |

___Axiomatic Mind.___ The tridrone can't be compelled to act in a manner contrary to its nature or its instructions.

___Disintegration.___ If the tridrone dies, its body disintegrates into dust, leaving behind its weapons and anything else it was carrying.

**Actions**

___Multiattack.___ The tridrone makes three fist attacks or three javelin attacks.

___Fist.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) bludgeoning damage.

___Javelin.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 4 (1d6 + 1) piercing damage.