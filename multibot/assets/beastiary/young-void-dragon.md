"Young Void Dragon";;;_size_: Large dragon
_alignment_: chaotic neutral
_challenge_: "9 (5000 XP)"
_languages_: "Common, Draconic, Void Speech"
_skills_: "Arcana +10, History +10, Perception +8, Persuasion +8, Stealth +4"
_senses_: ", passive Perception 18"
_saving_throws_: "Dex +4, Con +9, Wis +4, Cha +8"
_damage_immunities_: "cold"
_condition_immunities_: "charmed, frightened"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "157 (15d10 + 75)"
_armor class_: "18 (natural armor)"
_stats_: | 20 (+5) | 10 (+0) | 21 (+5) | 14 (+2) | 11 (+0) | 19 (+4) |

___Chill of the Void.___ Cold damage dealt by the void dragon ignores resistance to cold damage, but not cold immunity.

___Void Dweller.___ As ancient void dragon.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) piercing damage plus 3 (1d6) cold damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons:

___Gravitic Breath.___ The dragon exhales a 30-foot cube of powerful localized gravity, originating from the dragon.

___Falling damage in the area increases to 1d10 per 10 feet fallen.___ When a creature starts its turn within the area or enters it for the first time in a turn, including when the dragon creates the field, it must make a DC 17 Dexterity saving throw. On a failure the creature is restrained. On a success the creature's speed is halved as long as it remains in the field. A restrained creature repeats the saving throw at the end of its turn. The field persists until the dragon's breath recharges, and it can't use gravitic breath twice consecutively.

___Stellar Flare Breath.___ The dragon exhales star fire in a 30- foot cone. Each creature in that area must make a DC 17 Dexterity saving throw, taking 28 (8d6) fire damage and 28 (8d6) radiant damage on a failed save, or half as much damage on a successful one.

**Reactions**

___Void Twist.___ When the dragon is hit by a ranged attack it can create a small rift in space to increase its AC by 4 against that attack. If the attack misses because of this increase the dragon can choose a creature within 30 feet to become the new target for that attack. Use the original attack roll to determine if the attack hits the new target.

