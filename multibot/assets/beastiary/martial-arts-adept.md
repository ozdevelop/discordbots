"Martial Arts Adept";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Acrobatics +5, Insight +5, Stealth +5"
_speed_: "40 ft."
_hit points_: "60 (11d8+11)"
_armor class_: "16"
_stats_: | 11 (0) | 17 (+3) | 13 (+1) | 11 (0) | 16 (+3) | 10 (0) |

___Unarmored Defense.___ While the adept is wearing no armor and wielding no shield, its AC includes its Wisdom modifier.

**Actions**

___Multiattack.___ The adept makes three unarmed strikes or three dart attacks.

___Unarmed Strike.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8+3) bludgeoning damage. If the target is a creature, the adept can choose one of the following additional effects:

* The target must succeed on a DC 13 Strength saving throw or drop one item it is holding (adept's choice).

* The target must succeed on a DC 13 Dexterity saving throw or be knocked prone.

* The target must succeed on a DC 13 Constitution saving throw or be stunned until the end ot'the adept's next turn.

___Dart.___ Ranged Weapon Attack: +5 to hit, range 20/60 ft, one target. Hit: 5 (1d4+3) piercing damage.

**Reactions**

___Deflect Missile.___ In response to being hit by a ranged weapon attack, the adept deflects the missile. The damage it takes from the attack is reduced by 1d10+3. If the damage is reduced to 0, the adept catches the missile if it's small enough to hold in one hand and the adept has a hand free.