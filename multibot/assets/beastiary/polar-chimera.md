"Polar Chimera";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "understands Draconic but can't speak"
_skills_: "Perception +9"
_senses_: "darkvision 60 ft., passive Perception 19"
_saving_throws_: "Con +8"
_damage_immunities_: "cold"
_speed_: "30 ft."
_hit points_: "137 (11d12 + 66)"
_armor class_: "12 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 20 (+5) | 3 (-4) | 16 (+3) | 11 (+0) |

___Ice Walk.___ The chimera can move across and climb icy
surfaces without needing to make an ability check.
Additionally, difficult terrain composed of ice or snow
doesn't cost it extra movement.

**Actions**

___Multiattack.___ The chimera makes three attacks: one with
its bite, one with its horns, and one with its claws.
When its cold breath is available, it can use the breath
in place of its bite or horns.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 11 (2d6 + 4) piercing damage.

___Horns.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 10 (1d12 + 4) bludgeoning damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 11 (2d6 + 4) slashing damage.

___Cold Breath (Recharge 5–6).___ The dragon head exhales
freezing air in a 15-foot cone. Each creature in that area
must make a DC 15 Dexterity saving throw, taking 30
(7d6) cold damage on a failed save, or half as much
damage on a successful one.
