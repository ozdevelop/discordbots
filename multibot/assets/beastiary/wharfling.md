"Wharfling";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_languages_: "--"
_skills_: "Perception +3, Sleight of Hand +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "30 ft., climb 30 ft., swim 20 ft."
_hit points_: "6 (4d4 . 4)"
_armor class_: "13"
_stats_: | 4 (-3) | 16 (+3) | 8 (-1) | 2 (-4) | 12 (+1) | 8 (-1) |

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage, and the target is grappled (escape DC 10). Until this grapple ends, the wharfling can't use its bite on another target. While the target is grappled, the wharfling's bite attack hits it automatically.

___Pilfer.___ A wharfling that has an opponent grappled at the start of its turn can make a Dexterity (Sleight of Hand) check as a bonus action. The DC for this check equals 10 plus the grappled target's Dexterity modifier. If the check is successful, the wharfling steals some small metallic object from the target, and the theft is unnoticed if the same result equals or exceeds the target's passive Perception. A wharfling flees with its treasure.

