"Sky Raider Goblin";;;_size_: Small humanoid (goblinoid)
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Goblin"
_senses_: "Darkvision 60 ft., Passive Perception 9"
_skills_: "Animal Handling +3"
_speed_: "30 ft."
_hit points_: "7 (2d6)"
_armor class_: "14 (leather armor)"
_stats_: | 8 (-1) | 14 (+2) | 10 (+0) | 10 (+0) | 8 (-1) | 8 (-1) |

___Nimble Escape.___ The goblin can take the Disengage
or Hide action as a bonus action on each of its turns.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach
5 ft., one target. Hit: 5 (1d6 +2) piercing damage.

___Light Crossbow.___ Ranged Weapon Attack: +4 to hit, range
80/320 ft., one target. Hit: 6 (1d8 +2) piercing damage.

___Grenado (3/Day).___ As an action the goblin can
throw a grenado to a point it can see within 30 feet.
When a grenado is thrown, roll initiative for it. The
grenado explodes on its next turn. When a grenado
explodes, all creatures within 10 feet must succeed
on a DC 12 Dexterity saving throw, taking 3 (1d6)
piercing and 3 (1d6) fire damage on a failed save, or
half as much on a successful one. The grenado also
ignites flammable materials in the area.

**Reactions**

___Parachute.___ The goblin can deploy the parachute as
a reaction while falling, or as an action otherwise.
The parachute requires at least a 10 foot cube of
unoccupied space in which to deploy, and it doesn’t
open fast enough to slow a fall of less than 60 ft. If
it has sufficient time and space to deploy properly,
the parachute allows its wearer to land without
taking falling damage. Once it has been used, the
parachute takes 10 minutes to repack.
