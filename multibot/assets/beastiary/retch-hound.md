"Retch Hound";;;_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_immunities_: "acid, poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "45 (7d8 + 14)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+4) | 15 (+1) | 15 (+3) | 5 (+0) | 12 (+3) | 4 (+5) |

___Keen Senses.___ The retch hound has advantage on Wisdom (Perception)
checks that rely on hearing, sight, or smell.

___Pack Tactics.___ Retch hounds have advantage on an attack roll against a
creature if at least one of the hound’s allies is within 5 feet of the creature
and the ally isn’t incapacitated.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) piercing damage plus 7 (2d6) acid damage.

___Retch (Recharge 5–6).___ The retch hound exhales acidic bile in a 15-foot
cone. Each creature in that area must make a DC 12 Dexterity saving
throw, taking 21 (6d6) acid damage on a failed save, or half as much on
a successful one.
