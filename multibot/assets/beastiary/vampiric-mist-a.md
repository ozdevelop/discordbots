"Vampiric Mist (A)";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Wis +3"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "45 (6d8+18)"
_armor class_: "13"
_condition_immunities_: "charmed, exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, cold, lightning, necrotic, thunder; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 6 (-2) | 16 (+3) | 16 (+3) | 6 (-2) | 12 (+1) | 7 (-2) |

___Source.___ tales from the yawning portal,  page 247

___Undead Nature.___ A vampiric mist doesn't require air or sleep.

___Blood Sense.___ The vampiric mist can sense living creatures that have blood or similar vital fluids in a radius of 60 feet .

___Forbiddance.___ The vampiric mist can 't enter a residence without an invitation from one of the occupants.

___Misty Form.___ The vampiric mist can occupy another creature's space and vice versa. In addition, if air can pass through a space, the mist can pass through it without squeezing. Each foot of movement in water costs it 2 extra feet, rather than 1 extra foot. The mist can't manipulate objects in any way that requires hands; it can apply simple force only.

___Sunlight Hypersensitivity.___ The vampiric mist takes 20 radiant damage when it starts its turn in sunlight. While in sunlight, the mist has disadvantage on attack rolls and ability checks.

**Actions**

___Blood Drain.___ One creature in the vampiric mist's space must make a DC 13 Constitution saving throw (undead and constructs automatically succeed). On a failed save, the target takes 10 (2d6 + 3) necrotic damage, its hit point maximum is reduced by an amount equal to the necrotic damage taken, and the mist regains hit points equal to that amount.

>This reduction to the target's hit point maximum lasts until the target finishes a long rest. It dies if this effect reduces its hit point maximum to 0.