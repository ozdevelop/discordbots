"Alquam, Demon Lord Of Night";;;_size_: Huge fiend
_alignment_: chaotic evil
_challenge_: "21 (33000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Deception +10, Perception +12, Stealth +11"
_senses_: "truesight 120 ft., passive Perception 22"
_saving_throws_: "Dex +11, Con +13, Wis +12"
_damage_immunities_: "cold, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "fire, lightning"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "20 ft., fly 100 ft."
_hit points_: "350 (28d12 + 168)"
_armor class_: "20 (natural armor)"
_stats_: | 14 (+2) | 19 (+4) | 23 (+6) | 16 (+3) | 20 (+5) | 16 (+3) |

___Born of Darkness.___ Alquam can take the Hide action as a bonus action on each turn while it is in dim light or darkness, even if it is being observed.

___Innate Spellcasting.___ Alquam's innate spellcasting ability is Charisma (save DC 18). It can innately cast the following spells without material components.

At will: darkness, silence

3/day each: fear, invisibility, teleport

1/day: circle of death

___Keen Senses.___ Alquam has advantage on Wisdom (Perception) checks that rely on sight.

___Legendary Resistance (3/day).___ If Alquam fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ Alquam has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Alquam's weapon attacks are magical.

___Alquam's Lair.___ On initiative count 20 (losing initiative ties), Alquam takes a lair action to cause one of the following effects; Alquam can't use the same effect two rounds in a row:

- Alquam snuffs all light sources within the lair. Spells that create light are dispelled, mundane light sources are extinguished, and magical light sources are suppressed. Even light created by artifacts is reduced to dim light with half its normal radius of illumination. This lasts until initiative count 20 on the following round.

- Haunting, maddening music emanates from a point Alquam can see. Creatures within 50 feet of the origin that can hear the music must make a DC 15 Wisdom saving throw. Those that fail are charmed, incapacitated, and have their speed reduced to 0 until they take damage or until initiative count 20 on the following round.

- Black tendrils writhe around up to three creatures Alquam can see within 100 feet. The creatures must succeed on a DC 15 Strength saving throw or be restrained until initiative count 20 on the following round.

___Regional Effects.___ The region containing Alquam's lair is warped by the demon lord's magic, which creates one or more of the following effects:

- Within 1 mile of the lair all light sources except artifacts shed light to only half the usual radius.

- Owls and other nocturnal beasts become enraged and hostile within 5 miles of the lair, attacking intruders individually and in swarms.

- Within 1 mile of the lair, Alquam can cast his senses into any area of dim light or darkness as if using clairvoyance.

If Alquam dies, conditions in the area surrounding the lair return to normal over the course of 1d10 days.

**Actions**

___Multiattack.___ Alquam makes one bite attack, one wing attack, and one talons attack.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 26 (4d10 + 4) piercing damage.

___Wing.___ Melee Weapon Attack: +11 to hit, reach 15 ft., one target. Hit: 17 (3d8 + 4) bludgeoning damage and the target must succeed on a DC 17 Strength saving throw or be knocked prone.

___Talons.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 14 (3d6 + 4) slashing damage and the target is grappled and restrained (escape DC 17). Alquam can grapple one creature at a time if it is on the ground or two if it is flying. Talons grappling a creature can't attack any other creature.

**Legendary** Actions

___Alquam can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. Alquam regains spent legendary actions at the start of its turn.

___Attack.___ Alquam makes one attack.

___Move.___ Alquam flies half its speed without provoking opportunity attacks.

___Shroud (2 actions).___ Alquam radiates magical darkness in a 30.foot radius. The darkness lasts until the start of Alquam's next turn.

