"Giant Diseased Fly";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_skills_: "Perception +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "30 ft., fly 60 ft."
_hit points_: "22 (3d8 + 9)"
_armor class_: "13"
_stats_: | 12 (+1) | 17 (+3) | 16 (+3) | 2 (-4) | 7 (-2) | 2 (-4) |

___Keen Smell.___ The giant fly has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 10
(2d6 + 3) piercing damage. If the target is a creature it must succeed on
a DC 13 Constitution saving throw or be diseased until the condition
is cured. While the creature is diseased it is poisoned.. Every 24 hours
that elapse, the target must repeat the saving throw, reducing its hit point
maximum by 5 (1d10) on a failure. The disease is cured on a success.
The target dies if the disease reduces its hit point maximum to 0. This
reduction to the target’s hit point maximum lasts until the disease is cured.
