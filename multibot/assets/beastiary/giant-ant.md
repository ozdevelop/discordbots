"Giant Ant";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft., passive Perception 9"
_speed_: "40 ft."
_hit points_: "52 (7d10 + 14)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 13 (+1) | 15 (+2) | 1 (-5) | 9 (-1) | 2 (-4) |

___Keen Smell.___ The giant ant has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Multiattack.___ The giant ant makes one bite attack and one sting attack.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) bludgeoning damage and the target is grappled (escape DC 12). Until this grapple ends, the target is restrained and the giant ant can't bite a different target.

___Sting.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage plus 22 (4d10) poison damage, or half as much poison damage with a successful DC 12 Constitution saving throw.

