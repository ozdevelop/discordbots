"Authority";;;_size_: Medium celestial
_alignment_: any good alignment
_challenge_: "6 (2,300 XP)"
_languages_: "all, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 19"
_saving_throws_: "Dex +5, Con +6"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: " charmed, exhaustion, frightened, poisoned"
_speed_: "40 ft., fly 40 ft."
_hit points_: "97 (13d8 + 39)"
_armor class_: "19 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 17 (+3) | 14 (+2) | 16 (+3) | 16 (+3) |

___Angelic Weapons.___ The Authority’s weapon
attacks are magical. When the Authority hits with
any weapon, the weapon deals an extra 9 (2d8)
radiant damage (included in the attack).

___Magic Resistance.___ The Authority has advantage
on saving throws against spells and other magical
effects from evil characters and sources.

___Aura of Protection Against Evil.___ Evil creatures
have disadvantage on attack rolls against all
allies within 5 feet of the Authority. Allies in this
area can’t be charmed, frightened, or possessed
by evil creatures. If an ally is already charmed,
frightened, or possessed by evil magic, the ally
has advantage on any new saving throw against
the relevant effect.

**Actions**

___Multiattack.___ The Authority makes three attacks
with the Brightsword.

___Brightsword.___ Melee Weapon Attack: +7 to hit,
reach 10 ft., one target. Hit: 14 (3d6 + 4) slashing
damage and 9 (2d8) radiant damage. Evil creatures take an extra 9 (2d8) radiant damage.
