"Hell Hound";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "understands Infernal but can't speak it"
_senses_: "darkvision 60 ft."
_skills_: "Perception +5"
_damage_immunities_: "fire"
_speed_: "50 ft."
_hit points_: "45 (7d8+14)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 12 (+1) | 14 (+2) | 6 (-2) | 13 (+1) | 6 (-2) |

___Keen Hearing and Smell.___ The hound has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Pack Tactics.___ The hound has advantage on an attack roll against a creature if at least one of the hound's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage plus 7 (2d6) fire damage.

___Fire Breath (Recharge 5-6).___ The hound exhales fire in a 15-foot cone. Each creature in that area must make a DC 12 Dexterity saving throw, taking 21 (6d6) fire damage on a failed save, or half as much damage on a successful one.