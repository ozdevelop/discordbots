"Young Red Dragon";;;_size_: Large dragon
_alignment_: chaotic evil
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Perception +8, Stealth +4"
_damage_immunities_: "fire"
_saving_throws_: "Dex +4, Con +9, Wis +4, Cha +8"
_speed_: "40 ft., climb 40 ft., fly 80 ft."
_hit points_: "178 (17d10+85)"
_armor class_: "18 (natural armor)"
_stats_: | 23 (+6) | 10 (0) | 21 (+5) | 14 (+2) | 11 (0) | 19 (+4) |

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 17 (2d10 + 6) piercing damage plus 3 (1d6) fire damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Fire Breath (Recharge 5-6).___ The dragon exhales fire in a 30-foot cone. Each creature in that area must make a DC 17 Dexterity saving throw, taking 56 (16d6) fire damage on a failed save, or half as much damage on a successful one.