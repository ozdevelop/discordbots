"Ancient Amethyst Dragon";;;_size_: Gargantuan dragon
_alignment_: neutral evil
_challenge_: "22 (41,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +15, Insight +10, Perception +10, Religion +15"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 27"
_saving_throws_: "Dex +14, Int +15, Wis +10, Cha +13"
_damage_vulnerabilites_: "psychic"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, slashing, and piercing from nonmagical attacks"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "276 (24d20 + 24)"
_armor class_: "22 (natural armor)"
_stats_: | 22 (+6) | 25 (+7) | 12 (+1) | 26 (+8) | 16 (+3) | 23 (+6) |

___Legendary Resistance (3/Day).___ If the dragon fails
a saving throw, it can choose to succeed instead.

___Feedback Aura.___ Each time the dragon takes
damage, all creatures within 30 feet must succeed
on a DC 23 Intelligence saving throw or else take
14 (4d6) psychic damage.

**Psionics**

___Charges:___ 24 | ___Recharge:___ 1d10 | ___Fracture:___ 27

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 15 ft.,
one target. Hit: 17 (2d10 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft.,
one target. Hit: 13 (2d6 + 6) slashing damage.

___Tail.___ Melee Weapon Attack: +13 to hit; reach 20 ft.,
one target. Hit: 15 (2d8 + 6) bludgeoning damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The dragon regains spent
legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) Check.

___Psionics.___ The dragon uses a psionic ability.

___Psionic Shift (Costs 2 Actions).___ The dragon
releases a wave of telekinetic energy from its mind.
Every creature within 15 feet must make a DC 24
Intelligence saving throw or take 15 (2d6 + 8) force
damage and be knocked prone. The dragon then
can move up to half its movement speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the
dragon takes a lair action to cause one of the
following effects. The dragon can’t use the same
effect two rounds in a row.

* The dragon manifests _believe_ at no cost.

* The dragon manifests _forget_ at no cost. If it
forces the target to forget a spell, the dragon
knows that spell and can cast it using Intelligence
as its spellcasting ability, with a DC
of 23.

* The dragon manifests an illusion of a shifting
battlefield. The terrain within 30 feet of it is
considered difficult terrain until the end of the
dragon’s next turn.

**Regional** Effects

Intelligent creatures who sleep within 12 miles of an
amethyst dragon’s lair dream of an opulent party
with a charming host to whom they willingly and
happily reveal their deepest secrets.
