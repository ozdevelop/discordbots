"Rasa";;;_size_: Medium humanoid
_alignment_: any
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Deception +5, Stealth +6"
_senses_: "passive Perception 9"
_speed_: "30 ft."
_hit points_: "16 (3d8 + 3)"
_armor class_: "13 (leather)"
_stats_: | 10 (+0) | 14 (+2) | 12 (+1) | 11 (+0) | 8 (-1) | 12 (+1) |

___Innate Spellcasting.___ The rasa's innate spellcasting
ability is Intelligence (spell save DC 11). It can
innately cast the following spells, requiring no
components:

* At will: _message_

* 1/day each: _disguise self_

___Inscrutable Intentions.___ The rasa is immune to any
magical effects to determine if it is lying.

___Sight of the Rasa.___ The rasa share a special sight that
is invisible to all non-rasa creatures. As an action, a
rasa can touch a creature and place a secret mark
on that creature's chest. This mark lets other rasa
know whether or not this creature should be
regarded as a friend or a foe.

**Actions**

___Multiattack.___ The rasa makes two melee attacks.

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 4
(1d4 + 2) piercing damage.
