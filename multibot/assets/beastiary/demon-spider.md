"Demon Spider";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "--"
_skills_: "Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 11"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "poison"
_damage_resistances_: "cold, lightning"
_condition_immunities_: "poisoned"
_speed_: "30 ft., climb 30 ft."
_hit points_: "102 (12d10 + 36)"
_armor class_: "16 (natural armor)"
_stats_: | 10 (+0) | 17 (+3) | 16 (+3) | 5 (-3) | 12 (+1) | 3 (-4) |

___Magic Resistance.___ The spider has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The spider’s weapon attacks are magical.

___Spider Climb.___ The spider can climb difficult surfaces, including upside
down on ceilings, without needing to make an ability check.

___Web Sense.___ While in contact with a web, the spider knows the exact
location of any other creature in contact with the same web.

___Web Walker.___ The spider ignores movement restrictions caused by
webbing.

**Actions**

___Multiattack.___ The spider can make one bite attack and two puncture
attacks with its legs.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage, and the target must make a DC 12 Constitution
saving throw, taking 18 (4d8) poison damage on a failed save, or half as
much damage on a success. If the poison damage reduces the target to 0
hit points, the target is stable but poisoned for 1 hour, even after regaining
hit points, and is paralyzed while poisoned in this way.

___Puncture.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit:
14 (2d10 + 3) piercing damage.
