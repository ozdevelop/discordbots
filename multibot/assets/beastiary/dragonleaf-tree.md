"Dragonleaf Tree";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "8 (3900 XP)"
_languages_: "can understand the language of its creator or designated master"
_senses_: "blindsight 120 ft., passive Perception 11"
_damage_immunities_: "A dragonleaf tree enjoys the same immunities as its progenitor. Black, copper, and green trees are immune to acid damage; blue and bronze trees are immune to lightning damage; brass, gold, and red trees are immune to fire damage; and silver and white trees are immune to cold damage."
_condition_immunities_: "blinded, deafened"
_speed_: "5 ft."
_hit points_: "152 (16d10 + 64)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 19 (+4) | 3 (-4) | 12 (+1) | 17 (+3) |

___Loyal to Dragon Master.___ A dragonleaf tree only follows commands from its designated master (or from any creatures to whom the master grants control). It has advantage on saving throws against any charm or compulsion spell or effect. Additionally, the tree has advantage on any saving throw to resist Bluff, Diplomacy, or Intimidate checks made to influence it to act against its masters.

___Weaknesses.___ Dragonleaf trees with immunity to fire also have vulnerability to cold, and trees with immunity to cold have vulnerability to fire.

**Actions**

___Slam.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 58 (10d10 + 3) bludgeoning damage.

___Leaves.___ Ranged Weapon Attack: +3 to hit, range 30/60 ft., one target. Hit: 45 (10d8) slashing damage.

___Breath Weapon (Recharge 6).___  dragonleaf tree can issue forth a breath weapon from its leaves appropriate to the dragon it honors. The creature's breath weapon deals 49 (14d6) damage, or half damage to targets that make a successful DC 15 Dexterity saving throw. A black, copper, or green tree breathes a 60-foot line of acid; a blue or bronze tree breathes a 60-foot line of lightning; a brass, gold, or red tree breathes a 30-foot cone of fire; and a silver or white tree breathes a 30-foot cone of cold.

