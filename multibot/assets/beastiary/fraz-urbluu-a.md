"Fraz-Urb'luu (A)";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "23 (50,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft."
_skills_: "Deception +15, Perception +14, Stealth +8"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing that is nonmagical"
_saving_throws_: "Dex +8, Con +15, Int +15, Wis +14"
_speed_: "40 ft., fly 40 ft."
_hit points_: "350 (28d10+196)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 29 (+9) | 12 (+1) | 25 (+7) | 26 (+8) | 24 (+7) | 26 (+8) |

___Innate Spellcasting.___ Fraz-Urb'luu's spellcasting ability is Charisma (spell save DC 23). Fraz-Urb'luu can innately cast the following spells, requiring no material components:

* At will: _alter self _(can become medium-sized when changing his appearance)_, detect magic, dispel magic, phantasamal force_

* 3/day each: _confusion, dream, mislead, programmed illusion, seeming_

* 1/day each: _mirage arcane, modify memory, project image_

___Legendary Resistance (3/Day).___ If Fraz-Urb'luu fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Fraz-Urb'luu has advantage on saving throws against spell and other magic effects.

___Magic Weapon.___ Fraz-Urb'luu's weapon attacks are magical.

___Undetectable.___ Fraz-Urb'luu can't be targeted by divination magic, perceived through magical scrying sensors, or detected by abilities that sense demons or fiends.

**Actions**

___Multiattack.___ Fraz-Urb'luu makes three attacks: one with his bite and two with his fists.

___Bite.___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 23 (4d6 + 9) piercing damage.

___Fist.___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 27 (4d8 + 9) bludgeoning damage.

**Legendary** Actions

Fraz-Urb'luu can take 3 legendary actions, choosing from the options below. Only one legendary action can be used at a time and only at the end of another creautre's turn. Fraz-Urb'luu regains spent legendary actions at the start of his turn.

___Tail.___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 31 (4d10 + 9) bludgeoning damage. If the target is a Large or smaller creature, it is also grappled (escape DC 24). The grappled target is also restrained. Fraz-Urb'luu can grapple only one creature with his tail at a time.

___Phantasmal Killer (Costs 2 Actions).___ Fraz-Urb'luu casts phantasmal killer, no concentration required.
