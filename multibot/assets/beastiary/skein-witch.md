"Skein Witch";;;_size_: Medium celestial
_alignment_: neutral
_challenge_: "12 (8400 XP)"
_languages_: "Celestial, telepathy (100 ft.)"
_skills_: "History +8, Insight +15, Perception +15"
_senses_: "truesight 60 ft., passive Perception 25"
_saving_throws_: "Int +8, Wis +10, Cha +10"
_damage_immunities_: "fire, lightning, psychic"
_damage_resistances_: "radiant"
_speed_: "30 ft., fly 30 ft."
_hit points_: "162 (25d8 +50)"
_armor class_: "20 (natural armor)"
_stats_: | 6 (-2) | 12 (+1) | 14 (+2) | 16 (+3) | 20 (+5) | 20 (+5) |

___Bend Fate (3/day).___ If the skein witch fails a saving throw, she may choose to succeed instead and reflect the effect of the failed saving throw onto one enemy within 30 feet. The skein witch still suffers the effect of a successful saving throw, if any. The new target is entitled to a saving throw as if it were the original target of the attack, but with disadvantage.

___Fear All Cards.___ If a deck of many things is brought within 30 feet of a skein witch, she emits a psychic wail and disintegrates.

___Magic Resistance.___ The skein witch has advantage on saving throws against spells and other magical effects.

___Misty Step (At Will).___ The skein witch can step between places as a bonus action.

___Sealed Destiny (1/Day).___ The skein witch attunes herself to the threads of the PCs' fates. Ask each player to write down their prediction of how the PC to their left will die, and at what level. Collect the notes without revealing the answers. When one of those PCs dies, reveal the prediction. If the character died in the manner predicted, they fulfill their destiny and are immediately resurrected by the gods as a reward. If they died at or within one level of the prediction, they return to life with some useful insight into the destiny of someone important.

**Actions**

___Multiattack.___ The skein witch makes two Inexorable Thread attacks.

___Inexorable Threads.___ Melee Weapon Attack: +9 to hit, reach 30 ft., one target. Hit: 27 (5d8 + 5) radiant damage, and the target is "one step closer to death." If the target is reduced to 0 hit points, it's treated as if it's already failed one death saving throw. This effect is cumulative; each inexorable threads hit adds one unsuccessful death saving throw. If a character who's been hit three or more times by inexorable threads is reduced to 0 hit points, he or she dies immediately. This effect lasts until the character completes a long rest.

___Bind Fates (1/Day).___ One target within 60 feet of the skein witch must make a DC 18 Wisdom saving throw. On a failed save, the target's fate is bound to one random ally of the target. Any damage or condition the target suffers is inflicted on the individual to which they are bound instead, and vice versa. A creature can be bound to only one other creature at a time. This effect lasts until either of the affected creatures gains a level, or until a heal or heroes' feast lifts this binding.

___Destiny Distortion Wave (Recharge 5-6).___ The skein witch projects a 60-foot cone of distortion that frays the strands of fate. All targets in the cone take 55 (10d10) force damage, or half damage with a successful DC 18 Wisdom saving throw. In addition, if more than one target that failed its saving throw is affected by a condition, those conditions are randomly redistributed among the targets with failed saving throws.

