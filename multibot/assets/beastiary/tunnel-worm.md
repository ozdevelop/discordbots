"Tunnel Worm";;;_size_: Huge monstrosity
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 10"
_condition_immunities_: "prone"
_speed_: "20 ft., burrow 20 ft."
_hit points_: "149 (13d12 + 65)"
_armor class_: "13 (natural armor)"
_stats_: | 21 (+5) | 13 (+1) | 21 (+5) | 1 (-5) | 10 (+0) | 6 (-2) |

___Rend Armor.___ When the tunnel worm hits a creature wearing nonmagical
armor or carrying a shield with its bite attack, the armor or shield takes a
permanent –1 penalty to the AC it offers. Armor reduced to an AC of 10 or
a shield that drops to +0 bonus is destroyed. 

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 26
(6d6 + 5) piercing damage, and the target is grappled (escape DC 16).
Until this grapple ends, the target is restrained. The tunnel worm can bite
only the grappled creature and has advantage on attack rolls to do so.
