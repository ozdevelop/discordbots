"Winter Wolf";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Giant, Winter Wolf"
_skills_: "Perception +5, Stealth +3"
_damage_immunities_: "cold"
_speed_: "50 ft."
_hit points_: "75 (10d10+20)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 14 (+2) | 7 (-2) | 12 (+1) | 8 (-1) |

___Keen Hearing and Smell.___ The wolf has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Pack Tactics.___ The wolf has advantage on an attack roll against a creature if at least one of the wolf's allies is within 5 ft. of the creature and the ally isn't incapacitated.

___Snow Camouflage.___ The wolf has advantage on Dexterity (Stealth) checks made to hide in snowy terrain.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be knocked prone.

___Cold Breath (Recharge 5-6).___ The wolf exhales a blast of freezing wind in a 15-foot cone. Each creature in that area must make a DC 12 Dexterity saving throw, taking 18 (4d8) cold damage on a failed save, or half as much damage on a successful one.