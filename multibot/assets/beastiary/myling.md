"Myling";;;_size_: Small undead
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "charmed, frightened, poisoned, stunned, unconscious"
_speed_: "30 ft., burrow 10 ft."
_hit points_: "45 (10d6 + 10)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 10 (+0) | 12 (+1) | 10 (+0) | 12 (+1) | 10 (+0) |

**Actions**

___Multiattack.___ The myling makes one bite and two claw attacks.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) piercing damage, and the target is grappled (escape DC 12). If the target was grappled by the myling at the start of the myling's turn, the bite attack hits automatically.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 8 (2d6 + 1) slashing damage.

___Buried Alive.___ If the myling starts its turn on its chosen burial ground, it sinks into the earth. If it has a creature grappled, that creature sinks with the myling. A Medium or larger creature sinks up to its waist; a Small creature sinks up to its neck. If the myling still has the victim grappled at the start of the myling's next turn, both of them disappear into the earth. While buried this way, a creature is considered stunned. It can free itself with a successful DC 20 Strength (Athletics) check, but only one check is allowed; if it fails, the creature is powerless to aid itself except with magic. The creature must also make a DC 10 Constitution saving throw; if it succeeds, the creature has a lungful of air and can hold its breath for (Constitution modifier + 1) minutes before suffocation begins. Otherwise, it begins suffocating immediately. Allies equipped with digging tools can reach it in four minutes divided by the number of diggers; someone using an improvised tool (a sword, a plate, bare hands) counts as only one-half of a digger.

