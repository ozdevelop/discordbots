"Kua-Lij";;;_size_: Medium humanoid
_alignment_: lawful neutral
_challenge_: "1/8 (25 XP)"
_languages_: "Aquan, Common, Elven, Gnome, Kuah-Lij"
_skills_: "Arcana +5"
_senses_: "passive Perception 11"
_speed_: "30 ft."
_hit points_: "6 (1d8 + 2)"
_armor class_: "11"
_stats_: | 8 (-1) | 13 (+1) | 14 (+2) | 16 (+3) | 13 (+1) | 8 (-1) |

___Gifted Craftsmen.___ A kuah-lij has proficiency in alchemist’s supplies,
jeweler’s tools, and smith’s tools.

___Magical Sight.___ A kuah-lij can sense the presence of magic within 30
feet of it. It can use an action to see a faint aura around any visible creature
or object in the area that bears magic, and it learns its school of magic,
if any.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 3 (1d4 + 1) piercing damage.

___Light Crossbow.___ Ranged Weapon Attack: +3 to hit, range 80/320 ft.,
one target. Hit: 5 (1d8 + 1) piercing damage.
