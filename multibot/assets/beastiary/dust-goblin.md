"Dust Goblin";;;_size_: Small humanoid
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Goblin"
_skills_: "Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 9"
_condition_immunities_: "charmed, frightened"
_speed_: "40 ft."
_hit points_: "5 (1d6 + 2)"
_armor class_: "14 (leather armor)"
_stats_: | 8 (-1) | 16 (+3) | 14 (+2) | 10 (+0) | 8 (-1) | 8 (-1) |

___Twisted.___ When the dust goblin attacks a creature from hiding, its target must make a successful DC 10 Wisdom saving throw or be frightened until the end of its next turn.

**Actions**

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Light Crossbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

