"Blade Dancer";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any three languages"
_skills_: "Acrobatics +8, History +6, Performance +11, Sleight of Hand +12"
_senses_: "passive Perception 10"
_saving_throws_: "Str +4, Con +7"
_speed_: "30 ft."
_hit points_: "81 (6d10 + 4d8 + 30)"
_armor class_: "17 (half plate)"
_stats_: | 10 (+0) | 18 (+4) | 16 (+3) | 14 (+2) | 10 (+0) | 16 (+3) |

___Combat Caster.___ The dancer can perform the somatic
components of spells even when it has weapons or a
shield in one or both hands.

___Combat Maneuvers (3/Short Rest).___ Dancers have trained
all their life to excel in combat and as a result have
mastered special combat maneuvers that they can use
in combat. Whenever the dancer makes a melee
weapon attack, it can choose to execute one of these
maneuvers to add additional effects to the attack. In
addition to these other effects, all maneuvers cause the
attacks to deal an additional 1d8 damage. Each dancer
has two random maneuvers from the list below
available for use:

* Disarming Attack – The target must succeed on a DC
16 Strength saving throw or drop its weapon at its
feet.

* Distracting Strike – The next attack an ally makes
against the target is made with advantage.

* Feinting Attack – The next attack roll you make
against a target within 5 feet is made with advantage.

* Trip Attack – If the target is Large or smaller, it must
succeed on a DC 16 Strength saving throw or be
knocked prone.

___Inspire (3/Short Rest).___ Target uninspired creature within
60 feet of the dancer that can hear it gains a d8
inspiration die. Once within the next 10 minutes, that
creature can roll that die and add the number rolled to
one ability check, attack roll, or saving throw it makes.
This die must be rolled before it is decided if the roll
succeeds or fails.

___Spellcasting.___ The dancer is a 3th-level spellcaster. Its
spellcasting ability is charisma (spell save DC 15, +7 to
hit with spell attacks). The dancer has the following
bard spells prepared:

* Cantrips (at will): _blade ward, mage hand_

* 1st level (4 slots): _heroism, sleep, unseen servant_

* 2nd level (2 slots): _invisibility, suggestion_

___Two-Weapon Fighting Style.___ The dancer adds its ability
modifier to the damage of its off-hand weapon attacks.

**Actions**

___Multiattack.___ The dancer makes three attacks with its
scimitar or three ranged attacks with its dagger.

___Scimitar.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one target. Hit: 7 (1d6 + 4) slashing damage.

___Dagger.___ Ranged Weapon Attack: +8 to hit, range 20/60
ft., one target. Hit: 6 (1d4 + 4) piercing damage.

___Action Surge (1/Short Rest).___ The dancer makes five
scimitar attacks.
