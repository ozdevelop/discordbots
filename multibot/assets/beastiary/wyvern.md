"Wyvern";;;_size_: Large dragon
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4"
_speed_: "20 ft., fly 80 ft."
_hit points_: "110 (13d10+39)"
_armor class_: "13 (natural armor)"
_stats_: | 19 (+4) | 10 (0) | 16 (+3) | 5 (-3) | 12 (+1) | 6 (-2) |

**Actions**

___Multiattack.___ The wyvern makes two attacks: one with its bite and one with its stinger. While flying, it can use its claws in place of one other attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one creature. Hit: 11 (2d6 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

___Stinger.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one creature. Hit: 11 (2d6 + 4) piercing damage. The target must make a DC 15 Constitution saving throw, taking 24 (7d6) poison damage on a failed save, or half as much damage on a successful one.