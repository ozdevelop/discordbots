"Ruby Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: lawful neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Draconic"
_skills_: "Insight +4, Perception +4"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 16"
_saving_throws_: "Dex +4, Int +4, Wis +4, Cha +4"
_damage_vulnerabilities_: "psychic"
_speed_: "30 ft., fly 60 ft. (hover)"
_hit points_: "49 (9d8 + 9)"
_armor class_: "17 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 12 (+1) | 15 (+2) | 14 (+2) | 14 (+2) |

___Amplification Aura.___ Allies’ spells cast within 30
feet have their saving throw DC increased by 1.

**Psionics**

___Charges:___ 9 | ___Recharge:___ 1d4 | ___Fracture:___ 8

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 7 (1d10 + 2) piercing damage.
