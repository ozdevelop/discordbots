"Squad of Thugs";;;_size_: Large swarm
_alignment_: any non-good alignment
_challenge_: "2 (450 XP)"
_languages_: "Any one language (usually Common)"
_senses_: "passive Perception 10"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "11 (leather)"
_stats_: | 19 (+4) | 10 (+0) | 14 (+2) | 10 (+0) | 10 (+0) | 11 (+0) |

___Power in Numbers.___ The swarm has advantage on an
attack roll against a creature if the swarm has at
least half of its maximum hit points.

___Swarm.___ The swarm can occupy another creature's
space and vice versa, and the swarm can move
through any opening large enough for a Medium
bandit. The swarm can't regain hit points or gain
temporary hit points.

**Actions**

___Maces.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 15 (2d6 + 8) bludgeoning damage,
or 7 (1d6 + 4) bludgeoning damage if the swarm
has half of its hit points or fewer.

___Heavy Crossbow Barrage.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 11
(2d10) piercing damage, or 5 (1d10) piercing
damage if the swarm has half of its hit points or
fewer.
