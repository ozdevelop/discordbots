"Yeti";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Yeti"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +3"
_damage_immunities_: "cold"
_speed_: "40 ft., climb 40 ft."
_hit points_: "51 (6d10+18)"
_armor class_: "12 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 16 (+3) | 8 (-1) | 12 (+1) | 7 (-2) |

___Fear of Fire.___ If the yeti takes fire damage, it has disadvantage on attack rolls and ability checks until the end of its next turn.

___Keen Smell.___ The yeti has advantage on Wisdom (Perception) checks that rely on smell.

___Snow Camouflage.___ The yeti has advantage on Dexterity (Stealth) checks made to hide in snowy terrain.

**Actions**

___Multiattack.___ The yeti can use its Chilling Gaze and makes two claw attacks.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage plus 3 (1d6) cold damage.

___Chilling Gaze.___ The yeti targets one creature it can see within 30 ft. of it. If the target can see the yeti, the target must succeed on a DC 13 Constitution saving throw against this magic or take 10 (3d6) cold damage and then be paralyzed for 1 minute, unless it is immune to cold damage. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If the target's saving throw is successful, or if the effect ends on it, the target is immune to the Chilling Gaze of all yetis (but not abominable yetis) for 1 hour.