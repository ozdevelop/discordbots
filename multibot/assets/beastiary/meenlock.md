"Meenlock";;;_size_: Small fey
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "telepathy 120 ft."
_senses_: "darkvision 120 ft."
_skills_: "Perception +4, Stealth +6, Survival +2"
_speed_: "30 ft."
_hit points_: "31 (7d6+7)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "frightened"
_stats_: | 7 (-2) | 15 (+2) | 12 (+1) | 11 (0) | 10 (0) | 8 (-1) |

___Fear Aura.___ Any beast or humanoid that starts its turn within 10 feet of the meenlock must succeed on a DC 11 Wisdom saving throw or be frightened until the start of the creature's next turn.

___Light Sensitivity.___ While in bright light, the meenlock has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Shadow Teleport (Recharge 5-6).___ As a bonus action, the meenlock can teleport to an unoccupied space within 30 feet of it, provided that both the space it's teleporting from and its destination are in dim light or darkness. The destination need not be within line of sight.

**Actions**

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4+2) slashing damage, and the target must succeed on a DC 11 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.