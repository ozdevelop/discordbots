"Ghelryn Foehammer";;;_size_: Medium humanoid (shield dwarf)
_alignment_: lawful good
_challenge_: "0 (10 XP)"
_languages_: "Common, Dwarfish"
_skills_: "Athletics +6, Intimidation +2, Perception +2"
_speed_: "30 ft."
_hit points_: "30 (4d8 + 12)"
_armor class_: "14 (breastplate, shield)"
_senses_: "passive Perception 12"
_stats_: | 18 (+4) | 7 (-2) | 17 (+3) | 10 (0) | 11 (0) | 11 (0) |

**Dwarven Resilience.** Ghelryn has advantage on saving throws against poison.

**Giant Slayer.** Any weapon attack that Ghelryn makes against a giant deals an extra 7 (2d6) damage on a hit

**Actions**

**Multiattack.** Ghelryn makes two battleaxe attacks.

**Battleaxe.** Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, or 9 (1d10 + 4) slashing damage if used with two hands.

**Roleplaying** Information

The blacksmith Ghelryn has a good heart, but he hates orcs and giants - hates them with a fiery passion. He considers it the solemn duty of all dwarves to cave in their skulls!

**Ideal:** It is incumbent upon every dwarf to forge a legacy.

**Bond:** I stand for Clan Foehammer and all dwarvenkind.

**Flaw:** I never run from a figth, especially if it involves killing orcs or giants.