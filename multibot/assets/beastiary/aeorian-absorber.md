"Aeorian Absorber";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "10 (5,900 XP)"
_languages_: "understands Draconic but can’t speak"
_skills_: "Perception +6, Stealth +8, Survival +6"
_senses_: "darkvision 120 ft., passive Perception 16"
_saving_throws_: "Wis +6, Cha +3"
_damage_immunities_: "radiant, necrotic"
_speed_: "40 ft."
_hit points_: "171 (18d10 + 72)"
_armor class_: "15 (natural armor)"
_stats_: | 21 (+5) | 18 (+4) | 18 (+4) | 6 (-2) | 14 (+2) | 8 (-1) |

___Magic Resistance.___ The absorber has advantage on saving throws against spells and other magical effects.

___Pounce.___ If the absorber moves at least 20 feet straight toward a creature and then hits its claws attack on the same turn, that target must succeed on a DC 17 Strength saving throw or be knocked prone. If the target is prone, the absorber can make one bite attack against it as a bonus action.

**Actions**

___Multiattack.___ The absorber makes three attacks: one with its bite or Mind Bolt and two with its claws.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one creature. Hit: 10 (1d10 + 5) piercing damage plus 5 (1d10) force damage.

___Claws.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) slashing damage plus 3 (1d6) force damage.

___Mind Bolt.___ Ranged Spell Attack: +8 to hit, range 120 ft., one creature. Hit: 22 (4d10) psychic damage.

**Reactions**

___Tail Ray.___ When the absorber takes damage from a spell, the absorber takes only half the triggering damage. If the spellcaster is within 60 feet of the absorber, the absorber can force the caster to make a DC 16 Dexterity saving throw. Unless the save succeeds, the caster takes the other half of the damage.
