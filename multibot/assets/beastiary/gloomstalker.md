"Gloomstalker";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "6 (2,300 XP)"
_languages_: "understands Common but can’t speak"
_skills_: "Athletics +9, Perception +6, Intimidation +5, Stealth +6"
_senses_: "darkvision 240 ft., passive Perception 16"
_saving_throws_: "Str +9, Dex +6"
_damage_vulnerabilities_: "radiant"
_speed_: "40 ft., fly 80 ft."
_hit points_: "90 (12d10 + 24)"
_armor class_: "15 (natural armor)"
_stats_: | 22 (+6) | 16 (+3) | 14 (+2) | 5 (-3) | 17 (+3) | 14 (+2) |

___Shadowstep.___ As a bonus action, the gloomstalker can teleport up to 40 feet to an unoccupied space it can see.

___Sunlight Sensitivity.___ While in sunlight, the gloomstalker has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The gloomstalker makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one creature. Hit: 15 (2d8 + 6) piercing damage plus 7 (2d6) necrotic damage.

___Claws.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage plus 7 (2d6) necrotic damage.

___Snatch.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one Medium or smaller creature. Hit: 13 (2d6 + 6) slashing damage plus 7 (2d6) necrotic damage, and the target is grappled (escape DC 17). While grappled in this way, the target is restrained.

___Shriek (Recharge 6).___ The gloomstalker emits a terrible shriek. Each enemy within 60 feet of the gloomstalker that can hear it must succeed on a DC 13 Constitution saving throw or be paralyzed until the end of the enemy’s next turn.

