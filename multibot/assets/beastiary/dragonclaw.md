"Dragonclaw";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic"
_skills_: "Deception +3, Stealth +5"
_saving_throws_: "Wis +2"
_speed_: "30 ft."
_hit points_: "16 (3d8+3)"
_armor class_: "14 (leather armor)"
_stats_: | 9 (-1) | 16 (+3) | 13 (+1) | 11 (0) | 10 (0) | 12 (+1) |

___Dragon Fanatic.___ The dragonclaw has advantage on saving throws against being charmed or frightened. While the dragonclaw can see a dragon or higher-ranking Cult of the Dragon cultist friendly to it, the dragonclaw ignores the effects of being charmed or frightened.

___Fanatic Advantage.___ Once per turn, if the dragonclaw makes a weapon attack with advantage on the attack roll and hits, it deals an extra 7 (2d6) damage.

___Pack Tactics.___ The dragonclaw has advantage on an attack roll against a creature if at least one of the dragonclaw's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The dragonclaw attacks twice with its scimitar.

___Scimitar.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.