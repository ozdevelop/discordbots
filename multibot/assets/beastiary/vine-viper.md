"Vine Viper";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: "blindsight 10ft., passive Perception 11"
_speed_: "30 ft., climb 30 ft."
_hit points_: "18 (4d4 + 8)"
_armor class_: "13"
_stats_: | 5 (-3) | 16 (+3) | 14 (+2) | 2 (-4) | 12 (+1) | 4 (-3) |

___Floral Camouflage.___ While the viper remains
motionless, it is indistinguishable from an ordinary
tree vine.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one creature. Hit: 5 (1d4 + 3) piercing damage, and
the target must succeed on a DC 11 Constitution
saving throw against being magically petrified. On a
failed save, the creature begins to turn to wood and
is restrained. It must repeat the saving throw at the
end of its next turn. On a success, the effect ends.
On a failure, the create is petrified for the next 8
hours.

___Nutrient Absorption.___ The viper drives its tail into a
tree or petrified creature within 5 feet, draining
resources from the target to heal itself. The target
takes 5 (2d4) necrotic damage and the snake
restores hit points equal to the amount of necrotic
damage dealt.
