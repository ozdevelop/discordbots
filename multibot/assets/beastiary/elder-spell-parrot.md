"Elder Spell Parrot";;;_size_: Tiny monstrosity
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "passive Perception 13"
_speed_: "10 ft., fly 50 ft."
_hit points_: "7 (5d4 - 5)"
_armor class_: "12"
_stats_: | 2 (-4) | 14 (+2) | 8 (-1) | 2 (-4) | 12 (+1) | 6 (-2) |

___Spell Mimicry.___ Whenever the spell parrot hears a cantrip or a 1st
through 8th level spell that has a verbal component being cast, it can
attempt to mimic the casting of that spell on its next turn. The spell parrot
ignores any somatic or material component that the spell requires.

When the spell parrot attempts to mimic the spell, roll a d6. If the spell
is a cantrip or a 1st through 3rd level spell, the casting succeeds if the
result is a 3–6. If the spell is 4th through 6th level, the casting succeeds on
the result of a 5 or 6. If the spell is 7th or 8th level, the casting succeeds
only on a 6. Once the spell parrot mimics a spell, it forgets it. The spell
parrot uses the original caster’s spell save DC and spell attack bonus, and
the spell must have a valid target for the spell parrot to use as the target of
the mimicked spell.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 1
piercing damage.
