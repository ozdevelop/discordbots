"Violet Fungus";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_speed_: "5 ft."
_hit points_: "18 (4d8)"
_armor class_: "5"
_condition_immunities_: "blinded, deafened, frightened"
_stats_: | 3 (-4) | 1 (-5) | 10 (0) | 1 (-5) | 3 (-4) | 1 (-5) |

___False Appearance.___ While the violet fungus remains motionless, it is indistinguishable from an ordinary fungus.

**Actions**

___Multiattack.___ The fungus makes 1d4 Rotting Touch attacks.

___Rotting Touch.___ Melee Weapon Attack: +2 to hit, reach 10 ft., one creature. Hit: 4 (1d8) necrotic damage.