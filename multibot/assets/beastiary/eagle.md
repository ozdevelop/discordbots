"Eagle";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_skills_: "Perception +4"
_speed_: "10 ft., fly 60 ft."
_hit points_: "3 (1d6)"
_armor class_: "12"
_stats_: | 6 (-2) | 15 (+2) | 10 (0) | 2 (-4) | 14 (+2) | 7 (-2) |

___Keen Sight.___ The eagle has advantage on Wisdom (Perception) checks that rely on sight.

**Actions**

___Talons.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.