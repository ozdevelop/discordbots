"Orc Claw of Luthic";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +2, Medicine +4, Survival +4"
_speed_: "30 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "14 (hide armor)"
_stats_: | 14 (+2) | 15 (+2) | 16 (+3) | 10 (0) | 15 (+2) | 11 (0) |

___Source.___ Volo's Guide to Monsters, p. 183

___Aggressive.___ As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.

___Spellcasting.___ The orc is a 5th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). The orc has the following cleric spells prepared:

* Cantrips (at will): _guidance, mending, resistance, thaumaturgy_

* 1st level (4 slots): _bane, cure wounds, guiding bolt_

* 2nd level (3 slots): _augury, warding bond_

* 3rd level (2 slots): _bestow curse, create food and water_

**Actions**

___Multiattack.___ The orc makes two claw attacks, or four claw attacks if it has fewer than half of its hit points remaining.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8+2) slashing damage.
