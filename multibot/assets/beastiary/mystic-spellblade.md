"Mystic Spellblade";;;_size_: Medium humanoid
_alignment_: lawful evil (50%) or lawful good (50%)
_challenge_: "5 (1,800 XP)"
_languages_: "Celestial and any three other languages"
_skills_: "Arcana +7, Insight +4, Religion +7"
_senses_: "passive Perception 11"
_saving_throws_: "Str +5, Int +7"
_speed_: "30 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "16 (breastplate)"
_stats_: | 15 (+2) | 10 (+0) | 16 (+3) | 18 (+4) | 12 (+1) | 10 (+0) |

___Spiritual Alignment.___ The spellblade is unarmed, but has
two spectral weapons that follow it constantly, one a
longsword and the other a flail. These weapons act
according to the spellblade’s will. On the spellblade’s
turn, it may give mental commands to these
weapons to have them move up to 30 feet.

**Actions**

___Multiattack.___ The spellblade uses its Spiritual
Suppression if able. It then makes one attack with
its spectral longsword and one attack with its spectral
flail.

___Spectral Longsword.___ Melee Weapon Attack: +7 to hit,
reach 5ft., one target. Hit: 8 (1d8 + 4) force damage.

___Spectral Flail.___ Melee Weapon Attack: +7 to hit, reach
5ft., one target. Hit: 8 (1d8 + 4) force damage.

___Spiritual Suppression (Recharge 5-6).___ The spellblade
summons a set of ethereal bindings to subdue a target
within 90 feet. The target must succeed on a DC 14
Strength saving throw or become restrained for 1
minute. The target can repeat this saving throw at the
end of their turn, ending the effect on a success.

___Relentless Assault (1/Day).___ The spellblade summons 5
new spectral longswords at its side. One at a time it
grabs hold of these blades. When the spellblade grabs a
new longsword, it may teleport up to 120 feet into
an unoccupied space and attack a creature within 5
feet. It makes a separate attack roll for each
longsword attack. Hit or miss, after each attack the
spectral weapon fades away. When all of the swords
have been expended, the spellblade teleports back to its original
location.

**Reactions**

___Spiritual Barrier.___ The spellblade adds 3 to the result of
any spell saving throw by boosting its fortitude with
spiritual energy.
