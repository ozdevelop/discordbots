"Choronzon (Chaos Demon)";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "17 (18,000 XP)"
_languages_: "Abyssal"
_skills_: "Insight +8, Intimidation +10, Perception +8"
_senses_: "darkvision 120 ft., passive Perception 18"
_saving_throws_: "Dex +8, Con +14, Cha +10"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "324 (24d10 + 192)"
_armor class_: "16 (natural armor)"
_stats_: | 28 (+9) | 15 (+2) | 27 (+8) | 12 (+1) | 14 (+2) | 18 (+4) |

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Aura of Confusion.___ The demon can activate or deactivate this feature
as bonus action. While active, any creature that ends its turn within 30 feet
of the demon must make a DC 19 Wisdom saving throw or be stunned
until the end of the demon’s next turn.

___Innate Spellcasting.___ The demon’s spellcasting ability is Charisma
(spell save DC 18, +10 to hit with spell attacks). The demon can innately
cast the following spells, requiring no material components:

* At will: _confusion, fear_

* 3/day: _blight_

**Actions**

___Multiattack.___ The demon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one target. Hit: 22 (3d8 + 9) piercing damage.

___Claws.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one target. Hit: 23 (4d6 + 9) slashing damage. On a successful hit, the target must make a DC 20 Constitution saving throw or be stunned until the start of the demon’s next turn.

___Chaos Breath (Recharge 5–6).___ The demon breathes exhales a 40-foot
cone of bluish gas. Each creature in the area must make a successful DC
19 Constitution saving throw or take 42 (12d6) necrotic damage and
become poisoned. While poisoned in this way, a target takes 7 (2d6)
poison damage at the start of each of its turns. A target can repeat the
saving throw at the end of each of its turns, ending the effect on itself on
a success.

A creature killed by the demon’s Chaos Breath instantly breaks apart
into its individual atomic components. The creature can only be restored
to life by means of a true resurrection or wish spell.

___Teleport.___ The demon magically teleports, along with any equipment it
is wearing or carrying, up to 120 feet to an unoccupied space it can see.

___Summon (1/day).___ The demon chooses what to summon and attempts a
magical summoning.
A choronzon has a 50% chance of summoning 1d8 vrocks, 1d6 hezrous,
1d4 glabrezus, 1d3 nalfeshnees, 1d2 mariliths, or one goristro.
A summoned demon appears in an unoccupied space within 60 feet of
its summoner, acts as an ally of its summoner, and can’t summon other
demons. It remains for 1 minute, until it or its summoner dies, or until its
summoner dismisses it as an action.
