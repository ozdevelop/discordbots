"Scarecrow";;;_size_: Medium construct
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "36 (8d8)"
_armor class_: "11"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned, unconscious"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 11 (0) | 13 (+1) | 11 (0) | 10 (0) | 10 (0) | 13 (+1) |

___False Appearance.___ While the scarecrow remains motionless, it is indistinguishable from an ordinary, inanimate scarecrow.

**Actions**

___Multiattack.___ The scarecrow makes two claw attacks.

___Claw.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) slashing damage. If the target is a creature, it must succeed on a DC 11 Wisdom saving throw or be frightened until the end of the scarecrow's next turn.

___Terrifying Glare.___ The scarecrow targets one creature it can see within 30 feet of it. If the target can see the scarecrow, the target must succeed on a DC 11 Wisdom saving throw or be magically frightened until the end of the scarecrow's next turn. The frightened target is paralyzed.