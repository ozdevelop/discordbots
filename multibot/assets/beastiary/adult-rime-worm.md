"Adult Rime Worm";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "6 (2300 XP)"
_languages_: "--"
_senses_: "darkvision 200 ft., passive Perception 12"
_saving_throws_: "Str +8, Con +8"
_damage_immunities_: "cold, necrotic"
_speed_: "30 ft., swim 30 ft, burrow (snow, ice) 30 ft."
_hit points_: "105 (10d10 + 50)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 20 (+5) | 6 (-2) | 14 (+2) | 3 (-4) |

___Born of Rime.___ A rime worm can breathe air or water with equal ease.

___Ringed by Ice and Death.___ A rime worm is surrounded by an aura of cold, necrotic magic. At the start of the rime worm's turn, enemies within 5 feet take 2 (1d4) cold damage plus 2 (1d4) necrotic damage. If two or more enemies take damage from the aura on a single turn, the rime worm's black ice spray recharges immediately.

**Actions**

___Multiattack.___ The rime worm makes two tendril attacks.

___Tendril.___ Melee Weapon Attack. +8 to hit, reach 10 ft., one target. Hit: 8 (1d6 + 5) slashing damage. If both tendril attacks hit the same target in a single turn, that target is grappled (escape DC 15). The rime worm can grapple one creature at a time, and it can't use its tendril or devour attacks against a different target while it has a creature grappled.

___Devour.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 18 (2d12 + 5) slashing damage. If the target was grappled by the rime worm, it takes an additional 13 (2d12) cold damage.

___Black Ice Spray (Recharge 5-6).___ The rime worm sprays slivers of ice in a line 30 feet long and 5 feet wide. All creatures in the line take 26 (4d12) necrotic damage and are blinded; a successful DC 15 Constitution saving throw prevents the blindness. A blinded creature repeats the saving throw at the end of its turn, ending the effect on itself with a successful save.

