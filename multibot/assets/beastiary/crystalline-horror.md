"Crystalline Horror";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_resistances_: "cold"
_speed_: "30 ft."
_hit points_: "60 (8d8 + 24)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 16 (+3) | 10 (+0) | 12 (+1) | 10 (+0) |

___Crystal Claws.___ The crystalline horror’s attacks are magical.

___Magic Resistance.___ The crystalline
horror has advantage on saving throws
against spells and other magical effects.

**Actions**

___Multiattack.___ The crystalline horror can make two attacks
with its claws.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Shard Spray (Recharge 6).___ The crystalline horror launches a spray
of razor-sharp shards of glass from its body in a 30-foot cone. Each
creature in that area must make a DC 13 Dexterity saving throw, taking
17 (5d6) piercing damage on a failed save, or half as much damage on
a successful one.

**Reactions**

___Bend Light.___ The crystalline horror can refract natural light into a
bright light that makes it harder to be hit by melee attacks.
The crystalline horror adds 3 to its AC against one melee
attack that would hit it. To do so, the crystalline horror
must see the attack. This ability cannot be used in natural
or magical darkness.
