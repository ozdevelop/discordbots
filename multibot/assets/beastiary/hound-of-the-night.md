"Hound Of The Night";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "5 (1800 XP)"
_languages_: "understands Elvish and Umbral but can't speak"
_skills_: "Intimidation + 3, Perception +5, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Dex +6, Con +5, Wis +5"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "cold"
_speed_: "30 ft."
_hit points_: "112 (15d10 + 30)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 16 (+3) | 14 (+2) | 9 (-1) | 14 (+2) | 10 (+0) |

___Blood Scent.___ A hound of the night can follow a scent through phase shifts, ethereal movement, dimension door, and fey steps of any kind. Teleport and plane shift are beyond their ability to follow.

___Innate Spellcasting.___ The hound's innate spellcasting ability is Wisdom (spell save DC 13). It can innately cast the following spells, requiring no material components:

At will: dimension door

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 21 (3d10 + 5) piercing damage, and the target must succeed on a DC 15 Strength saving throw or be knocked prone.

___Frost Breath (Recharge 5-6).___ The hound exhales a 15-foot cone of frost. Those in the area of effect take 44 (8d10) cold damage, or half damage with a successful DC 13 Dexterity saving throw.

