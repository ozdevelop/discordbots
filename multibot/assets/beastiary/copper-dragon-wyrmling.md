"Copper Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: chaotic good
_challenge_: "1 (200 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +3"
_damage_immunities_: "acid"
_saving_throws_: "Dex +3, Con +3, Wis +2, Cha +3"
_speed_: "30 ft., climb 30 ft., fly 60 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "16 (natural armor)"
_stats_: | 15 (+2) | 12 (+1) | 13 (+1) | 14 (+2) | 11 (0) | 13 (+1) |

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) piercing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Acid Breath. The dragon exhales acid in an 20-foot line that is 5 feet wide. Each creature in that line must make a DC 11 Dexterity saving throw, taking 18 (4d8) acid damage on a failed save, or half as much damage on a successful one.

Slowing Breath. The dragon exhales gas in a 1 5-foot cone. Each creature in that area must succeed on a DC 11 Constitution saving throw. On a failed save, the creature can't use reactions, its speed is halved, and it can't make more than one attack on its turn. In addition, the creature can use either an action or a bonus action on its turn, but not both. These effects last for 1 minute. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself with a successful save.