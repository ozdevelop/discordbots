"Githyanki Supreme Commander";;;_page_number_: 206
_size_: Medium humanoid (gith)
_alignment_: lawful evil
_challenge_: "14 (11500 XP)"
_languages_: "Gith"
_senses_: "passive Perception 18"
_skills_: "Insight +8, Intimidation +9, Perception +8"
_saving_throws_: "Con +9, Int +8, Wis +8"
_speed_: "30 ft."
_hit points_: "187  (22d8 + 88)"
_armor class_: "18 (plate)"
_stats_: | 19 (+4) | 17 (+3) | 18 (+4) | 16 (+3) | 16 (+3) | 18 (+4) |

___Innate Spellcasting (Psionics).___ The githyanki's innate spellcasting ability is Intelligence (spell save DC 16, +8 to hit with spell attacks). It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _jump, levitate _(self only)_, misty step, nondetection _(self only)

* 1/day each: _Bigby's hand, mass suggestion, plane shift, telekinesis_

**Actions**

___Multiattack___ The githyanki makes two greatsword attacks.

___Silver Greatsword___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) slashing damage plus 17 (5d6) psychic damage. On a critical hit against a target in an astral body (as with the astral projection spell), the githyanki can cut the silvery cord that tethers the target to its material body, instead of dealing damage.

**Legendary** Actions

The githyanki can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The githyanki regains spent legendary actions at the start of its turn.

___Attack (2 Actions)___ The githyanki makes a greatsword attack.

___Command Ally___ The githyanki targets one ally it can see within 30 feet of it. If the target can see or hear the githyanki, the target can make one melee weapon attack using its reaction and has advantage on the attack roll.

___Teleport___ The githyanki magically teleports, along with any equipment it is wearing and carrying, to an unoccupied space it can see within 30 feet of it. It also becomes insubstantial until the start of its next turn. While insubstantial, it can move through other creatures and objects as if they were difficult terrain. If it ends its turn inside an object, it takes 16 (3d10) force damage and is moved to the nearest unoccupied space.
