"Dire Penguin";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Athletics +5"
_senses_: "passive Perception 10"
_saving_throws_: "Con +5"
_damage_resistances_: "cold"
_speed_: "25 ft., swim 50 ft."
_hit points_: "37 (5d10 + 10)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 15 (+2) | 2 (-4) | 11 (+0) | 5 (-3) |

___Clumsy.___ The penguin is not as graceful on land as it is in
the water. When on standing on solid ground, the
penguin has disadvantage on Dexterity saving throws
and any throws against an effect that would knock it
prone.

___Hold Breath.___ The penguin can hold its breath for 30
minutes.

___Slide.___ If the penguin moves up to 10 feet in a straight
line, it can jump prone as a bonus action, sliding along
icy or otherwise slick surfaces. When it does so, the
penguin immediately, and at the start of each of its
turns moves its entire speed in the same direction it
ran leading up to the slide. The speed of this slide may
increase or decrease based on the incline or surface, as
determined by the GM.
If the penguin collides with a creature or object, the
target takes 3 (1d6) piercing damage for every 10 feet
the penguin's slide would have moved it that turn. The
penguin then takes half as much bludgeoning damage.

___Swimming Leap.___ If the penguin spends at least 20 feet
of its movement swimming in a straight line, it may
immediately end this movement and perform a long
jump up to a distance of 25 feet in the same direction
as its last 20 feet of swimming. This jump may be used
to jump out of the water and onto a solid surface.

**Actions**

___Multiattack.___ The penguin makes two attacks with its
beak.

___Beak.___ Melee Weapon Attack: +5 to hit, range 5 ft., one
target. Hit: 10 (2d6 + 3) bludgeoning damage, and the
target is grappled (escape DC 13). Until this grapple
ends, the penguin can't use its beak on another
creature.

___Fling.___ The penguin flings one Medium or smaller
creature it has grappled. The creature is thrown up to
20 feet in a direction of the penguin's choice (up to 30
feet if the creature is Small or smaller) and knocked
prone. If a thrown target strikes a solid surface, the
target takes 3 (1d6) bludgeoning damage for every 10
feet it was thrown. If the target is thrown at another
creature, that creature must succeed on a DC 15
Dexterity saving throw or take the same damage and be
knocked prone.
