"Magma Mephit King";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Ignan, Terran"
_skills_: "Stealth +5, Deception +4"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 40 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "13"
_stats_: | 10 (+0) | 16 (+3) | 14 (+2) | 9 (-1) | 13 (+1) | 14 (+2) |

___Death Burst.___ When the mephit dies, it explodes in a
burst of lava. Each creature within 5 feet of it must
make a DC 13 Dexterity saving throw, taking 22
(4d10) fire damage on a failed save, or half as much
damage on a successful one.

___False Appearance.___ While the mephit remains
motionless, it is indistinguishable from an ordinary
mound of magma.

**Actions**

___Multiattack.___ The mephit makes two attacks with its
claws or with its bow.

___Magma Claws.___ Melee Weapon Attack: +5 to hit,
reach 5ft., one target. Hit: 6 (1d6 + 3) piercing
damage plus 5 (1d10) fire damage.

___Magma Bow.___ Melee Weapon Attack: +5 to hit, range
80/320 ft., one target. Hit: 6 (1d6 + 3) piercing
damage plus 5 (1d10) fire damage.

___Molten Caltrops.___ The mephit scatters molten
caltrops in a 15-foot square around itself. This area
is considered difficult terrain and creatures take 7
(2d6) fire damage for every 5 feet they move
through this terrain.

___Lava Choke (Recharge 6).___ The mephit causes a ball
of lava to appear in the throat of a creature it can
see within 90 ft. That creature must make a DC 13
Constitution saving throw, taking 27 (5d10) fire
damage on a failed save and the creature is silenced
as their throat is charred, or half as much damage
and not silenced on a successful save. The silence
lasts until the creature takes a short rest or is
healed by lesser restoration or similar magic.
