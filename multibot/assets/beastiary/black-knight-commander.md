"Black Knight Commander";;;_size_: Medium Humanoid
_alignment_: lawful evil
_challenge_: "5 (1800 XP)"
_languages_: "any two languages"
_skills_: "Animal Handling + 4, Athletics +7,  Intimidation +5"
_senses_: "passive Perception 11"
_saving_throws_: "Str +7, Wis +4, Cha +5,"
_speed_: "30 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "18 (plate)"
_stats_: | 18 (+4) | 10 (+0) | 14 (+2) | 12 (+1) | 13 (+1) | 15 (+2) |

___Charge.___ If the black knight commander is mounted and moves at least 30 feet in a straight line toward a target and then hits it with a melee attack on the same turn, the target takes an extra 10 (3d6) damage.

___Hateful Aura.___ The black knight commander and allies within 10 feet of the commander add its Charisma modifier to weapon damage rolls (included in damage below).

___Magic Weapons.___ The black knight commander's weapon attacks are made with magical (+1) weapons.

**Actions**

___Multiattack.___ The black knight commander makes two melee attacks.

___Mace.___ Melee Weapon Attack: +8 to hit, reach 5 ft, one target. Hit: 8 (1d6 + 5) bludgeoning damage.

___Lance.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 11 (1d12 + 5) piercing damage.

___Frightful Charge (Recharges after a Short or Long Rest).___ The black knight commander lets loose a terrifying cry and makes one melee attack at the end of a charge. Whether the attack hits or misses, all enemies within 15 feet of the target and aware of the black knight commander's presence must succeed on a DC 13 Wisdom saving throw or become frightened for 1 minute. A frightened creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

