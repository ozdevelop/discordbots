"Obsidian Drake";;;_size_: Huge dragon
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Draconic"
_skills_: "Deception +8, Insight +7, Perception +7, Persuasion +8, Stealth +6"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 17"
_saving_throws_: "Dex +6, Con + 10, Wis +7, Cha +8"
_damage_immunities_: "fire"
_speed_: "40 ft., fly 80 ft."
_hit points_: "207 (18d12 + 90)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+6) | 12 (+1) | 20 (+5) | 16 (+3) | 14 (+2) | 15 (+2) |

___Molten Core.___ The drake’s melee attacks have a chance
to set their target ablaze (included in the attacks
below).

**Actions**

___Multiattack.___ The drake makes three attacks: one with its
bite and two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft.,
one target. Hit: 16 (2d10 + 5) piercing damage and the
target must succeed on a DC 18 Constitution saving
throw or be set ablaze. Until a creature takes an action
to douse the fire, the target takes 5 (1d10) fire damage
at the start of each of its turns.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 10 ft.,
one target. Hit: 12 (2d6 + 5) slashing damage and the
target must succeed on a DC 18 Constitution saving
throw or be set ablaze. Until a creature takes an action
to douse the fire, the target takes 5 (1d10) fire damage
at the start of each of its turns.

___Breath Weapon (Recharge 5-6).___ The dragon uses one of
the following breath weapons.
* ___Lava Breath.___ The drake exhales lava in a 30-foot cone.
Each creature in that area must make a DC 18 Dexterity
saving throw, taking 42 (12d6) fire damage on a failed
save, or half as much damage on a successful one. This
area becomes coated in lava and is considered difficult
terrain. Creatures that move through an area covered in
lava take 5 (1d10) fire damage for every 5 feet they
move. A creature that ends its turn in this area takes 16
(3d10) fire damage.
* ___Ashen Breath.___ The drake exhales a blast of hot ash and
flame in a 60-foot cone. Each creature in this area must
make a DC 18 Strength saving throw, taking 28 (8d6)
fire damage and being pushed 15 feet on a failed save,
or half as much damage and not pushed on a
successful one. Each creature that failed this save must
also succeed on a DC 18 Constitution saving throw or
be blinded until the end of their next turn.

___Molten Obsidian (3/Day).___ The drake attempts to turn a
burning target it can see to obsidian. That creature
must succeed on a DC 18 Constitution saving throw
against being magically petrified. On a failed save, the
fire on their body turns to lava and begins to envelop
them. The creature is considered restrained and takes
55 (10d10) fire damage. It must repeat the saving
throw at the end of its next turn. On a success, the
effect ends. On a failure, the creature is petrified as
they turn to obsidian. The petrification lasts until the
creature is healed by the greater restoration spell or
other such magic.

___Create Volcano (1/Day).___ The drake slams a claw in the
ground and causes a volcano to rise up at a target
location within 120 ft. This volcano is 30 ft. high with
a 5 foot radius mouth and a 20 foot radius base. The
volcano begins to erupt, going dormant after 1 minute.
While the volcano is erupting, on initiative count 20
(losing ties), the following effects occur:
* Lava flows out 10 feet in every direction from the
mouth of the volcano. This area is considered
difficult terrain. Creatures that move through an area
covered in lava take 11 (2d10) fire damage for every
5 feet they move.
* Boulders erupt from the volcano and into the sky.
One of these boulders flies towards a random
creature within 120 feet of the volcano (including
the drake). That creature must succeed on a DC 10
Dexterity saving throw or take 44 (8d10)
bludgeoning damage.

**Legendary** Actions

The drake can take 3 legendary actions, choosing from
the options below. Only one legendary action can be
used at a time and only at the end of another creature’s
turn. The drake regains spent legendary actions at the
start of its turn.

___Claw or Bite.___ The drake makes one claw or bite attack.

___Detect.___ The drake makes a Wisdom (Perception) check.

___Molten Obsidian (Costs 2 Actions).___ The drake uses its
Molten Obsidian ability.
