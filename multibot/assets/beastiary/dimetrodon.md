"Dimetrodon";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_skills_: "Perception +2"
_speed_: "30 ft., swim 20 ft."
_hit points_: "19 (3d8+6)"
_armor class_: "12 (natural armor)"
_stats_: | 14 (+2) | 10 (0) | 15 (+2) | 2 (-4) | 10 (0) | 5 (-3) |

___Source.___ Volo's Guide to Monsters, p. 139

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6+2) piercing damage.