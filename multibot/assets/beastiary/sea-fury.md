"Sea Fury";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "12 (8,400 XP)"
_languages_: "Aquan, Common, Giant"
_skills_: "Deception +8, Insight +5, Perception +5, Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 15"
_damage_immunities_: "cold, fire, poison; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_condition_immunities_: "paralyzed, poisoned"
_speed_: "30 ft., swim 50 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 15 (+2) | 16 (+3) | 12 (+1) | 12 (+1) | 18 (+4) |

___Amphibious.___ The sea fury can breathe air and water.

___Innate Spellcasting.___ The sea fury’s innate spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _witch bolt_

* 1/day each: _bestow curse, fear, thunderwave_

___Legendary Resistance (3/Day).___ If the sea fury fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The sea fury has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The sea fury makes two attacks with its claws.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

___Death Glare.___ The sea fury targets one frightened creature it can see within 30 feet of it. The target must succeed on a DC 16 Wisdom saving throw or drop to 0 hit points.

**Legendary** Actions

The sea fury can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature’s turn. The sea fury regains spent legendary actions at the start of its turn.

* ___As Water.___ The sea fury transforms into a wave of foaming seawater, along with whatever it is wearing or carrying, and moves up to its speed without provoking opportunity attacks. While in this form, it can’t be grappled or restrained. It reverts to its true form at the end of this movement.

* ___Fearsome Apparition (Costs 2 Actions).___ The sea fury conjures an apparition of one of its dead sisters, which appears in an unoccupied space the sea fury can see within 30 feet of it. Enemies of the sea fury that can see the apparition must succeed on a DC 16 Wisdom saving throw or be frightened of it until it vanishes at the end of the sea fury’s next turn.

* ___Conjure Snakes (Costs 3 Actions).___ The sea fury disgorges a swarm of poisonous snakes, which occupies the same space as the sea fury, acts on its own initiative count, and attacks as directed by the sea fury. The sea fury can control up to three of these swarms at a time.

**Lair** Actions

A sea fury lurks in the caverns where its coven once dwelled, decorating the walls with the bones of its slain kin, as well as baubles stolen from sunken wrecks.

A sea fury encountered in its lair has a challenge rating of 14 (11,500 XP).

On initiative count 20 (losing initiative ties), the sea fury can take a lair action to cause one of the following magical effects, but can’t use the same effect two rounds in a row:

* Caverns, tunnels, and pools of water within 120 feet of the sea fury become foggy or murky, to the extent that the area becomes heavily obscured.

* The sea fury conjures a 15-foot cube of water that fills an unoccupied space it can see within 30 feet of it, then moves the water in a straight line up to 60 feet, after which the water disperses. Any creature that comes into contact with the rushing wave must succeed on a DC 16 Strength saving throw or be knocked prone by it and pushed 15 feet along its course.

* The sea fury calls forth the spirit of a dead sailor or sea hag that met its end in the lair. This spirit has the statistics of a specter and lasts until the sea fury uses another lair action. The specter appears in an unoccupied space within 30 feet of the sea fury and obeys the sea fury’s commands.

**Regional** Effects

The region containing a sea fury’s lair is warped by the sea fury’s magic, which creates the following effects:

* Sea water within 5 miles of the lair becomes coarse and choppy, as if whipped by an unseen wind.

* Sea grass within 1 mile of the lair is imbued with a foul mockery of life, grasping ineffectually at any creature that passes within 5 feet of it.

* Ordinary crabs and octopi within 1 mile of the lair grow in size to become giant crabs and giant octopi, respectively. These creatures serve the sea fury as spies and guards.
