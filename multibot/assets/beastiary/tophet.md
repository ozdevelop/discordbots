"Tophet";;;_size_: Huge construct
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "Common"
_skills_: "Perception +3"
_senses_: "darkvision 200 ft., passive Perception 13"
_saving_throws_: "Str +10, Con +8, Dex +3"
_damage_immunities_: "fire, cold, poison"
_damage_resistances_: "necrotic"
_condition_immunities_: "exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "184 (16d12 + 80)"
_armor class_: "16 (natural armor)"
_stats_: | 24 (+7) | 10 (+0) | 20 (+5) | 6 (-2) | 10 (+0) | 10 (+0) |

___Fiery Heart.___ A tophet's inner fire can be ignited or doused at will. Its heat is such that all creatures have resistance to cold damage while within 30 feet of the tophet.

___Burning Belly.___ Creatures inside a tophet's burning core take 21 (6d6) fire damage at the start of each of the tophet's turns. Escaping from a tophet's belly takes 10 feet of movement and a successful DC 16 Dexterity (Acrobatics) check.

**Actions**

___Multiattack.___ A tophet makes two attacks, no more than one of which can be a gout of flame.

___Slam.___ Melee Weapon Attack. +10 to hit, reach 5 ft., one target.

___Hit: 12 (1d10 + 7) bludgeoning damage.___ The target is also knocked inside the tophet's burning belly if the attack scores a critical hit.

___Gout of Flame.___ The tophet targets a point within 100 feet of itself that it can see. All targets within 10 feet of that point take 22 (4d10) fire damage, or half damage with a successful DC 16 Dexterity saving throw.

