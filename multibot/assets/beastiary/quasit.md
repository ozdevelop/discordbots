"Quasit";;;_size_: Tiny fiend (demon)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Abyssal, Common"
_senses_: "darkvision 120 ft."
_skills_: "Stealth +5"
_damage_immunities_: "poison"
_speed_: "40 ft."
_hit points_: "7 (3d4)"
_armor class_: "13"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold; fire; lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 5 (-3) | 17 (+3) | 10 (0) | 7 (-2) | 10 (0) | 10 (0) |

___Shapechanger.___ The quasit can use its action to polymorph into a beast form that resembles a bat (speed 10 ft. fly 40 ft.), a centipede (40 ft., climb 40 ft.), or a toad (40 ft., swim 40 ft.), or back into its true form . Its statistics are the same in each form, except for the speed changes noted. Any equipment it is wearing or carrying isn't transformed . It reverts to its true form if it dies.

___Magic Resistance.___ The quasit has advantage on saving throws against spells and other magical effects.

___Variant: Familiar.___ The quasit can serve another creature as a familiar, forming a telepathic bond with its willing master. While the two are bonded, the master can sense what the quasit senses as long as they are within 1 mile of each other. While the quasit is within 10 feet of its master, the master shares the quasit's Magic Resistance trait. At any time and for any reason, the quasit can end its service as a familiar, ending the telepathic bond.

**Actions**

___Claw (Bite in Beast Form).___ Melee Weapon Attack: +4 to hit, reach 5 ft ., one target. Hit: 5 (1d4 + 3) piercing damage, and the target must succeed on a DC 10 Constitution saving throw or take 5 (2d4) poison damage and become poisoned for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Scare (1/day).___ One creature of the quasit's choice within 20 ft. of it must succeed on a DC 10 Wisdom saving throw or be frightened for 1 minute. The target can repeat the saving throw at the end of each of its turns, with disadvantage if the quasit is within line of sight, ending the effect on itself on a success.

___Invisibility.___ The quasit magically turns invisible until it attacks or uses Scare, or until its concentration ends (as if concentrating on a spell). Any equipment the quasit wears or carries is invisible with it.