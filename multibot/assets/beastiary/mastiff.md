"Mastiff";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_skills_: "Perception +3"
_speed_: "40 ft."
_hit points_: "5 (1d8+1)"
_armor class_: "12"
_stats_: | 13 (+1) | 14 (+2) | 12 (+1) | 3 (-4) | 12 (+1) | 7 (-2) |

___Keen Hearing and Smell.___ The mastiff has advantage on Wisdom (Perception) checks that rely on hearing or smell.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) piercing damage. If the target is a creature, it must succeed on a DC 11 Strength saving throw or be knocked prone.