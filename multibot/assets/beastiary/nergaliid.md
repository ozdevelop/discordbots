"Nergaliid";;;_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Infernal"
_skills_: "Deception +5, Perception +2, Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 12"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren’t silvered"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "42 (4d10 + 20)"
_armor class_: "12 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 20 (+5) | 12 (+1) | 10 (+0) | 12 (+1) |

___Shadow Stealth.___ While in dim light or darkness, the nergaliid can take the Hide action as a bonus action.

___Standing Leap.___ The nergaliid’s long jump is up to 30 feet and its high jump is up to 20 feet, with or without a running start.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 13 (2d8 + 4) piercing damage, and the target must succeed on a DC 15 Constitution saving throw or become poisoned for 1 minute. The poisoned creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Tongue Lash.___ Melee Weapon Attack: +6 to hit, reach 20 ft., one target. Hit: 10 (1d12 + 4) bludgeoning damage.

___Siphon Life (Recharge 4–6).___ The nergaliid magically draws the life from a humanoid it can see within 40 feet of it. The target must make a DC 15 Wisdom saving throw. An incapacitated target fails the save automatically. On a failed save, the creature takes 10 (3d6) psychic damage, and the nergaliid gains temporary hit points equal to the damage taken. On a successful save, the target takes half as much damage, and the nergaliid doesn’t gain temporary hit points. If this damage kills the target, its body rises at the end of the nergaliid’s current turn as a husk zombie.
