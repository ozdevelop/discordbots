"Cyclops Warlock";;;_size_: Huge giant
_alignment_: chaotic neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Giant"
_senses_: "Can see normally in darkness, both magical and nonmagical 120 ft., Passive Perception 8"
_speed_: "30 ft."
_hit points_: "138 (12d12 + 60)"
_armor class_: "14 (natural armor)"
_stats_: | 22 (+6) | 11 (+0) | 20 (+5) | 8 (-1) | 6 (-2) | 10 (+0) |

___Beast Speech.___ The cyclops warlock can cast _speak
with animals_ at will, without expending a spell slot.

___Spellcasting.___ The cyclops warlock is a 4th-level
spellcaster. Its spellcasting ability is Charisma (spell
save DC 10, +2 to hit with spell attacks). It regains
its expended spell slots when it finishes a short or
long rest. It knows the following warlock spells:

* Cantrips (at will): _mage hand, prestidigitation, true strike_

* 1st-2nd level (2 slots): _darkness, expeditious retreat, find familiar, hex, mirror image_

___Poor Depth Perception.___ The cyclops has disadvantage
on any attack roll against a target more than 30
feet away.

**Actions**

___Multiattack.___ The cyclops makes two greatclub attacks.

___Greatclub.___ Melee Weapon Attack: +9 to hit, reach 10
ft., one target. Hit: 19 (3d8 + 6) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +9 to hit, range
30/120 ft., one target. Hit: 28 (4d10 + 6) bludgeoning
damage.
