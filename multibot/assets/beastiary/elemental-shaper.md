"Elemental Shaper";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any three languages"
_skills_: "Acrobatics +8, Arcana +4, Insight +6, Persuasion +7"
_senses_: "passive Perception 12"
_saving_throws_: "Str +4, Dex +8"
_speed_: "45 ft."
_hit points_: "74 (8d8 + 4d6 + 24)"
_armor class_: "16"
_stats_: | 10 (+0) | 18 (+4) | 14 (+2) | 10 (+0) | 14 (+2) | 17 (+3) |

___Elemental Strikes (6/Short Rest).___ Whenever the
shaper performs an unarmed strike, it can imbue
the attack with elemental magics, granting
additional properties depending on the element
chosen:

* Fire – The strike deals an additional 3 (1d6) fire
damage and has its range increased by 10 feet as
a whip of flame extends from the shaper's hands.

* Ice – The strike inflicts chill into the heart of the
target, reducing its move speed by 10 feet and
preventing it from taking the disengage action
until the end of your next turn.

* Lightning – The strike surges lightning through
the target's body. If that creature attempts to
cast a spell requiring somatic components on its
next turn, it must succeed on a DC 15
Constitution saving throw or be unable to
perform the required motions, causing the spell
to fail.

___Spellcasting.___ The shaper is a 3th-level spellcaster. Its
spellcasting ability is charisma (spell save DC 15, +7 to hit with spell attacks). The shaper has the
following sorcerer spells prepared:

* Cantrips (at will): _firebolt, ray of frost, shocking grasp_

* 1st level (4 slots): _chromatic orb, burning hands, witch bolt_

* 2nd level (2 slots): _enhance ability, scorching ray_

___Unarmored Defense.___ While not wearing armor, the
traveler's AC includes its Wisdom modifier.

**Actions**

___Multiattack.___ The shaper makes three unarmed
strikes.

___Unarmed Strike.___ Melee Weapon Attack: +8 to hit,
reach 5 ft., one target. Hit: 7 (1d6 + 4) bludgeoning
damage.
