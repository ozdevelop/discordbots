"Firenewt Warrior";;;_size_: Medium humanoid (firenewt)
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Draconic, Ignan"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "16 (chain shirt, shield)"
_stats_: | 10 (0) | 13 (+1) | 12 (+1) | 7 (-2) | 11 (0) | 8 (-1) |

___Amphibious.___ The firenewt can breathe air and water.

**Actions**

___Multiattack.___ The firenewt makes two attacks with its scimitar.

___Scimitar.___ Melee Weapon Attack: +3 to hit, reach 5 ft. one target. Hit: 4 (1d6+1) slashing damage.

___Spit Fire (Recharges after a Short or Long Rest).___ The firenewt spits fire at a creature within 10 feet of it. The creature must make a DC 11 Dexterity saving throw, taking 9 (2d8) fire damage on a failed save, or half as much damage on a successful one