"Deep Gnome (Svirfneblin)";;;_size_: Small humanoid (gnome)
_alignment_: neutral good
_challenge_: "1/2 (100 XP)"
_languages_: "Gnomish, Terran, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Investigation +3, Perception +2, Stealth +4"
_speed_: "20 ft."
_hit points_: "16 (3d6+6)"
_armor class_: "15 (chain shirt)"
_stats_: | 15 (+2) | 14 (+2) | 14 (+2) | 12 (+1) | 10 (0) | 9 (-1) |

___Stone Camouflage.___ The gnome has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.

___Gnome Cunning.___ The gnome has advantage on Intelligence, Wisdom, and Charisma saving throws against magic.

___Innate Spellcasting.___ The gnome's innate spellcasting ability is Intelligence (spell save DC 11). It can innately cast the following spells, requiring no material components:

* At will: _nondetection _(self only)

* 1/day each: _blindness/deafness, blur, disguise self_

**Actions**

___War Pick.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Poisoned Dart.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one creature. Hit: 4 (1d4 + 2) piercing damage, and the target must succeed on a DC 12 Constitution saving throw or be poisoned for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success
