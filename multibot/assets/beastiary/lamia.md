"Lamia";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Common"
_senses_: "darkvision 60 ft."
_skills_: "Deception +7, Insight +4, Stealth +3"
_speed_: "30 ft."
_hit points_: "97 (13d10+26)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 13 (+1) | 15 (+2) | 14 (+2) | 15 (+2) | 16 (+3) |

___Innate Spellcasting.___ The lamia's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no material components. At will: disguise self (any humanoid form), major image 3/day each: charm person, mirror image, scrying, suggestion 1/day: geas

**Actions**

___Multiattack.___ The lamia makes two attacks: one with its claws and one with its dagger or Intoxicating Touch.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 14 (2d10 + 3) slashing damage.

___Dagger.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Intoxicating Touch.___ Melee Spell Attack: +5 to hit, reach 5 ft., one creature. Hit: The target is magically cursed for 1 hour. Until the curse ends, the target has disadvantage on Wisdom saving throws and all ability checks.