"Archon of Redemption";;;_size_: Large celestial
_alignment_: lawful neutral
_challenge_: "10 (5,900 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Insight +9, Perception +9"
_senses_: "darkvision 120 ft., passive Perception 19"
_saving_throws_: "Wis +9, Cha +9"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "charmed, exhaustion, frightened"
_speed_: "30 ft., fly 90 ft."
_hit points_: "136 (16d8 + 64)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 18 (+4) | 18 (+4) | 17 (+3) | 20 (+5) | 20 (+5) |

___Angelic Weapons.___ The archon’s weapon attacks are magical. When the
archon hits with any weapon, the weapon deals an extra 3d8 radiant
damage (included in the attack).

___Axiomatic Mind.___ The archon can’t be compelled to act in a manner
contrary to its nature or its understanding of justice.

___Innate Spellcasting.___ The archon’s innate spellcasting ability is Charisma
(spell save DC 17). The archon can innately cast the following spells,
requiring only verbal components:

* At will: _detect evil and good_

* 1/day each: _destructive wave, geas_

___Magic Resistance.___ The archon has advantage on saving throws against
spells and other magical effects.

___One Being.___ Though it appears as a humanoid creature riding a mount,
an archon is a single being. The “rider” can’t be dismounted, and no
other means can separate the two portions of the archon’s being short
of its death.

**Actions**

___Multiattack.___ The archon makes two attacks: one with its sword and
one with its claws.

___Sword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8
(1d8 + 4) slashing damage plus 13 (3d8) radiant damage.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 11
(2d6 + 4) piercing damage plus 13 (3d8) radiant damage.
