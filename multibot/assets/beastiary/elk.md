"Elk";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_speed_: "50 ft."
_hit points_: "13 (2d10+2)"
_armor class_: "10"
_stats_: | 16 (+3) | 10 (0) | 12 (+1) | 2 (-4) | 10 (0) | 6 (-2) |

___Charge.___ If the elk moves at least 20 ft. straight toward a target and then hits it with a ram attack on the same turn, the target takes an extra 7 (2d6) damage. If the target is a creature, it must succeed on a DC 13 Strength saving throw or be knocked prone.

**Actions**

___Ram.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage.

___Hooves.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one prone creature. Hit: 8 (2d4 + 3) bludgeoning damage.