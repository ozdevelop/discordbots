"Mindrot Thrall";;;_size_: Medium plant
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "understands Common but cannot speak"
_senses_: "tremorsense 30 ft., passive Perception 12"
_saving_throws_: "Con +5"
_damage_immunities_: "acid, poison"
_damage_resistances_: "bludgeoning and piercing from nonmagical weapons"
_condition_immunities_: "charmed, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "82 (11d8 + 33)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 17 (+3) | 11 (+0) | 14 (+2) | 6 (-2) |

___Fungal Aura.___ A creature that starts its turn within 5 feet of a mindrot thrall must succeed on a DC 13 Constitution saving throw or become infected with mindrot spores.

**Actions**

___Multiattack.___ The mindrot thrall makes two claw attacks.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage.

___Acid Breath (Recharge 4-6).___ The thrall exhales a blast of acidic spores from its rotten lungs in a 15-foot cone. Each creature in that area takes 36 (8d8) acid damage, or half damage with a successful DC 13 Dexterity saving throw. If the saving throw fails, the creature is also infected with mindrot spores.

___Mindrot Spores.___ Infection occurs when mindrot spores are inhaled or swallowed. Infected creatures must make a DC 13 Constitution saving throw at the end of every long rest; nothing happens if the saving throw succeeds, but if it fails, the creature takes 9 (2d8) acid damage and its hit point maximum is reduced by the same amount. The infection ends when the character makes successful saving throws after two consecutive long rests, or receives the benefits of a _lesser restoration_ spell or comparable magic. A creature slain by this disease becomes a mindrot thrall after 24 hours unless the corpse is destroyed.
