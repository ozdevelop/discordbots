"Zorbo";;;_size_: Small monstrosity
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "passive Perception 1"
_skills_: "Athletics +3"
_speed_: "30 ft., climb 30 ft."
_hit points_: "27 (6d6 +6)"
_armor class_: "10 (see Natural Armor feature)"
_stats_: | 13 (+1) | 11 (0) | 13 (+1) | 3 (-4) | 12 (+1) | 7 (-2) |

___Magic Resistance.___ The zorbo has advantage on saving throws against spells and other magical effects.

___Natural Armor.___ The zorbo magically absorbs the natural strength of its surroundings, adjusting its Armor Class based on the material it is standing or climbing on: AC 15 for wood or bone, AC 17 for earth or stone, or AC 19 for metal. If the zorbo isn't in contact with any of these substances, its AC is 10.

**Actions**

___Destructive Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 8 (2d6 +1) slashing damage, and if the target is a creature wearing armor, carrying a shield, or in possession of a magic item that improves its AC , it must make a DC 11 Dexterity saving throw. On a failed save, one such item worn or carried by the creature (the target's choice) magically deteriorates, taking a permanent and cumulative -1 penalty to the AC it offers, and the zorbo gains a +l bonus to AC until the start of its next turn. Armor reduced to an AC of 10 or a shield or magic item that drops to a 0 AC increase is destroyed.