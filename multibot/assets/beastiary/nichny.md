"Nichny";;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "Elvish, Primordial, Sylvan, Void Speech"
_skills_: "Acrobatics +7, Insight +7, Perception +7"
_senses_: "darkvision 60 ft., passive Perception 17"
_saving_throws_: "Dex +7"
_damage_immunities_: "poison"
_damage_resistances_: "acid, cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "charmed, frightened, paralyzed, poisoned, unconscious"
_speed_: "30 ft., climb 30 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "17 (natural armor)"
_stats_: | 17 (+3) | 19 (+4) | 17 (+3) | 18 (+4) | 18 (+4) | 19 (+4) |

___Freedom of Movement.___ A nichny ignores difficult terrain and cannot be entangled, grappled, or otherwise impeded in its movements as if it is under the effect of a constant freedom of movement spell. This ability is negated for grapple attempts if the attacker is wearing gold or orichalcum gauntlets or using a gold or orichalcum chain as part of its attack.

___Imbue Luck (1/Day).___ Nichny can enchant a small gem or stone to bring good luck. If the nichny gives this lucky stone to another creature, the bearer receives a +1 bonus to all saving throws for 24 hours.

___Innate Spellcasting.___ The nichny's innate spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At Will: detect magic, invisibility, magic missile, ray of frost

3/day each: blink, dimension door, haste, polymorph (self only)

1/day each: teleport, word of recall

___Luck Aura.___ A nichny is surrounded by an aura of luck. All creatures it considers friends within 10 feet of the nichny gain a +1 bonus to attack rolls, saving throws, and ability checks. Creatures that it considers its enemies take a -1 penalty to attack rolls, saving throws, and ability checks. The nichny can activate or suppress this aura on its turn as a bonus action.

___Magic Resistance.___ The nichny has advantage on saving throws against spells and other magical effects.

___Soothsaying.___ Once per week, a nichny can answer up to three questions about the past, present, or future. All three questions must be asked before the nichny can give its answers, which are short and may be in the form of a paradox or riddle. One answer always is false, and the other two must be true.

**Actions**

___Multiattack.___ The nichny makes two claw attacks.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 17 (2d12 + 4) slashing damage.

