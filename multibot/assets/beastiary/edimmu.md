"Edimmu";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "Common but can't speak"
_senses_: "blindsight 60 ft., passive Perception 11"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, grappled, frightened, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "0 ft., fly 60 ft. (hover)"
_hit points_: "75 (10d8 + 30)"
_armor class_: "15"
_stats_: | 1 (-5) | 19 (+4) | 16 (+3) | 12 (+1) | 13 (+1) | 13 (+1) |

___Rejuvenation.___ If destroyed, an edimmu rises again in 2d4 days. Permanently destroying one requires properly burying its mortal remains in consecrated or hallowed ground. Edimmus rarely venture more than a mile from the place of their death.

___Incorporeal Movement.___ The edimmu can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

**Actions**

___Water Siphon.___ Melee Spell Attack: +7 to hit, reach 5 ft., one creature. Hit: 21 (6d6) necrotic damage. The target must succeed on a DC 14 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken and it is stunned for 1 minute and gains one level of exhaustion. A stunned creature repeats the saving throw at the end of each of its turns, ending the stun on itself on a success. The hit point reduction lasts until the creature finishes a long rest and drinks abundant water or until it is affected by greater restoration or comparable magic. The target dies if this effect reduces its hit point maximum to 0.

