"Death Butterfly Swarm";;;_size_: Large swarm
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_vulnerabilities_: "cold, fire"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, petrified"
_speed_: "5 ft., fly 40 ft. (hover)"
_hit points_: "60 (11d10)"
_armor class_: "15 (natural armor)"
_stats_: | 1 (-5) | 13 (+1) | 10 (+0) | 1 (-5) | 12 (+1) | 15 (+2) |

___Potent Poison.___ The death butterfly swarm's poison affects corporeal undead who are otherwise immune to poison.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny insect. The swarm can't regain hit points or gain temporary hit point.

___Weight of Wings.___ A creature in a space occupied by the death butterfly swarm has its speed reduced by half, and must succeed on a DC 13 Dexterity saving throw or become blinded. Both effects end when the creature doesn't share a space with the swarm at the end of the creature's turn. If a creature succeeds on the saving throw, it is immune to the swarm's blindness (but not the speed reduction) for 24 hours.

**Actions**

___Multiattack.___ The swarm makes a Bite attack against every target in its spaces.

___Bites.___ Melee Weapon Attack: +3 to hit, reach 0 ft., every target in the swarm's space. Hit: 22 (6d6 +1) piercing damage, or 11 (3d6 + 1) piercing damage if the swarm has half of its hit points or fewer. The target also takes 10 (3d6) poison damage and becomes poisoned for 1d4 rounds; a successful DC 13 Constitution saving throw reduces poison damage by half and prevents the poisoned condition..

