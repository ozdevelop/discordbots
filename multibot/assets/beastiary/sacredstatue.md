"Sacred Statue";;;_page_number_: 194
_size_: Large construct
_alignment_: as the eidolon's alignment
_challenge_: "0 (0 XP)"
_languages_: "the languages the eidolon knew in life"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "cold, necrotic, poison"
_saving_throws_: "Wis +8"
_speed_: "25 ft."
_hit points_: "95  (10d10 + 40)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_damage_resistances_: "acid, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 19 (+4) | 8 (-1) | 19 (+4) | 14 (+2) | 19 (+4) | 16 (+3) |

___False Appearance.___ While the statue remains motionless, it is indistinguishable from a normal statue.

___Ghostly Inhabitant.___ The eidolon that enters the sacred statue remains inside it until the statue drops to 0 hit points, the eidolon uses a bonus action to move out of the statue, or the eidolon is turned or forced out by an effect such as the dispel evil and good spell. When the eidolon leaves the statue, it appears in an unoccupied space within 5 feet of the statue.

___Inert.___ When not inhabited by an eidolon, the statue is an object.

**Actions**

___Multiattack___ The statue makes two slam attacks.

___Slam___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 43 (6d12 + 4) bludgeoning damage.

___Rock___ Ranged Weapon Attack: +8 to hit, range 60 ft./240 ft., one target. Hit: 37 (6d10 + 4) bludgeoning damage.