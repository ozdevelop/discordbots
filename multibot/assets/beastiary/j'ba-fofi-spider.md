"J'ba Fofi Spider";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Stealth +5"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 11"
_speed_: "40 ft., climb 40 ft."
_hit points_: "75 (10d10 + 20)"
_armor class_: "18 (natural armor)"
_stats_: | 17 (+3) | 17 (+3) | 15 (+2) | 4 (-3) | 13 (+1) | 6 (-2) |

___Jungle Stealth.___ The j'ba fofi spider gains an additional +2 to Stealth (+7 in total) in forest or jungle terrain.

___Camouflaged Webs.___ It takes a successful DC 15 Wisdom (Perception) check to spot the j'ba fofi's web. A creature that fails to notice a web and comes into contact with it is restrained by the web. A restrained creature can pull free from the web by using an action and making a successful DC 12 Strength check. The web can be attacked and destroyed (AC 10; hp 5; vulnerable to fire damage; immune to bludgeoning, poison, and psychic damage).

___Spider Climb.___ The j'ba fofi can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Spider Symbiosis.___ No ordinary spider will attack the j'ba fofi unless magically controlled or the j'ba fofi attacks it first. In addition, every j'ba fofi is accompanied by a swarm of spiders (a variant of the swarm of insects), which moves and attacks according to the j'ba fofi's mental command (commanding the swarm does not require an action by the j'ba fofi).

___Web Sense.___ While in contact with a web, the j'ba fofi knows the exact location of any other creature in contact with the same web.

___Web Walker.___ The j'ba fofi ignores movement restrictions caused by webbing.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 8 (1d10 + 3) piercing damage plus 22 (5d8) poison damage, or half as much poison damage with a successful DC 12 Constitution saving throw. A target dropped to 0 hit points by this attack is stable but poisoned and paralyzed for 1 hour, even after regaining hit points.

