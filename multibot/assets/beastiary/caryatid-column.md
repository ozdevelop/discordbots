"Caryatid Column";;;_size_: Medium construct
_alignment_: Unaligned
_challenge_: "2 (450 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 120 ft., passive Perception 10"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "piercing and slashing damage from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "14 (Natural Armor)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 2 (-4) | 11 (+0) | 1 (-5) |

___Immutable Form.___ The caryatid column is immune to any spell or effect
that would alter its form.

___Magic Resistance.___ The caryatid column has advantage on saving
throws against spells and other magical effects.

___Magic Weapons.___ The caryatid column’s weapon attacks are magical.

___Shatter Weapons.___ Whenever a character strikes a caryatid column with
a non-adamantine, nonmagical weapon, the character must succeed on a
DC 14 Strength saving throw or the weapon shatters into pieces.

**Actions**

___Longsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit:
8 (1d10 + 3) slashing damage.
