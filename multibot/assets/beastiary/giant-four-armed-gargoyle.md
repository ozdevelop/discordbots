"Giant Four-Armed Gargoyle";;;_size_: Large elemental
_alignment_: chaotic evil
_challenge_: "10 (5,900 XP)"
_Languages_: "Terran"
_senses_: "darkvision 60 ft., passive Perception 14"
_condition_immunities_: "exhaustion, petrified, poisoned"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing and slashing damage from nonmagical attacks not made with adamantine weapons"
_skills_: "Perception +4"
_Saving_throws_: "Wis +4"
_speed_: "30 ft., fly 60 ft."
_hit points_: "147 (14d10 +70)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 11 (0) | 20 (+5) | 6 (-2) | 11 (0) | 9 (-1) |

___False appearance.___ While the gargoyle remains motionless, it is indistinguishable from an inanimate statue.

**Actions**

___Multiattack.___ The gargoyle makes five attacks: one with its bite and four with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 11 (2d6 +4) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 9 (2d4 +4) slashing damage.