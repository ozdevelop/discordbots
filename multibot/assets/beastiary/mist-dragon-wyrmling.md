"Mist Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Auran, Aquan, Common, Draconic, Ignan"
_skills_: "Perception +7, Stealth +4, Survival +4"
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 17"
_saving_throws_: "Dex +4, Con +6, Wis +4, Cha +5"
_damage_immunities_: "fire"
_speed_: "30 ft., fly 60 ft., swim 30 ft."
_hit points_: "67 (9d8 + 27)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 17 (+3) | 12 (+1) | 13 (+1) | 15 (+2) |

___Amphibious.___ The mist dragon can breathe both water and air.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9
(1d10 + 4) piercing damage plus 3 (1d6) fire damage.

___Breath Weapon (Recharge 5–6).___ The mist dragon uses one of the
following breath weapons.

1. _Obscuring Mist._ The dragon exhales a murky opaque mist in a 15-foot
cube that spreads around corners. This mist heavily obscures the area and
remains for 1 minute, or until dispersed with a moderate or stronger wind
(at least 10 miles per hour).

2. _Scalding Mist._ The dragon exhales a fiery blast of lingering mist in a 15-
foot cone that spreads around corners. The mist lightly obscures the area
and remains for 1 minute, or until dispersed with a moderate or stronger
wind (at least 10 miles per hour). Each creature that enters the area or
begins their turn there must make a DC 14 Constitution saving throw,
taking 24 (7d6) fire damage on a failed saving throw, or half as much
damage on a successful one. Being underwater doesn’t grant resistance
to this damage.
