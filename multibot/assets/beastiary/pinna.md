"Pinna";;;_size_: Medium humanoid
_alignment_: neutral good
_challenge_: "1 (200 XP)"
_languages_: "Common"
_skills_: "Arcana +6"
_senses_: "passive Perception 11"
_saving_throws_: "Int +6, Wis +2"
_speed_: "30 ft."
_hit points_: "13 (3d6)"
_armor class_: "12"
_stats_: | 6 (-2) | 8 (-1) | 11 (+0) | 16 (+3) | 12 (+1) | 15 (+2) |

___Spellcasting.___ Pinna is a 3rd-level spellcaster. Her
spellcasting ability is Intelligence (spell save DC
13, +5 to hit with spell attacks). Pinna has the
following wizard spells prepared:

* Cantrips (at will): _minor illusion, mage hand, dancing lights, fire bolt_

* 1st level (4 slots): _color spray, silent image, identify, magic missile_

* 2nd level (2 slots): _blur, web_


**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +2 to
hit, reach 5 ft. or range 20/60 ft., one target. Hit:
2 (1d4) piercing damage.
