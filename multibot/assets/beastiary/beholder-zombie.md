"Beholder Zombie";;;_size_: Large undead
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "understands Deep Speech and Undercommon but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Wis +2"
_speed_: "0 ft., fly 20 ft. (hover)"
_hit points_: "93 (11d10+33)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned, prone"
_stats_: | 10 (0) | 8 (-1) | 16 (+3) | 3 (-4) | 8 (-1) | 5 (-3) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5+the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 14 (4d6) piercing damage.

___Eye Ray.___ The zombie uses a random magical eye ray, choosing a target that it can see within 60 feet of it.

1. Paralyzing Ray. The targeted creature must succeed on a DC 14 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

2. Fear Ray. The targeted creature must succeed on a DC 14 Wisdom saving throw or be frightened for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

3. Enervation Ray. The targeted creature must make a DC 14 Constitution saving throw, taking 36 (8d8) necrotic damage on a failed save, or half as much damage on a successful one.

4. Disintegration Ray. If the target is a creature, it must succeed on a DC 14 Dexterity saving throw or take 45 (10d8) force damage. If this damage reduces the creature to 0 hit points, its body becomes a pile of fine gray dust.

If the target is a Large or smaller nonmagical object or creation of magical force, it is disintegrated without a saving throw. If the target is a Huge or larger nonmagical object or creation of magical force, this ray disintegrates a 10-foot cube of it.