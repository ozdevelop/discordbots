"Prism Spider";;;_size_: Small monstrosity
_alignment_: unaligned
_challenge_: "9 (5,000 XP)"
_languages_: "--"
_skills_: "Perception +9, Stealth +11, Survival +9"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 19"
_damage_immunities_: "varies, see below"
_condition_immunities_: "varies, see below"
_speed_: "30 ft., climb 30 ft."
_hit points_: "71 (11d6 + 33)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 16 (+3) | 16 (+3) | 2 (-4) | 13 (+1) | 6 (-2) |

___Shifting Power.___ As the prism spider shifts color, its immunities and
attacks change. Roll a d6 at the start of the spider’s turn and consult the
table below. Use the result in place of one of the spider’s attacks.

1. _Green/Acid_ ___Spit Acid.___ The spider spits a 30-foot
line of acid that is 5 feet wide. Each
creature in the line must make a DC 16
Dexterity saving throw, taking 31 (9d6)
acid damage on a failed save, or half
as much damage on a successful one.

2. _Orange/Fire_ ___Inferno Shield.___ The spider is wreathed in
intense, thick flames, shedding bright
light in a 20-foot radius and dim light
for an additional 20 feet. Whenever a
creature within 5 feet of the spider hits
it with a melee attack before the end
of the prism spider’s next turn, the shield
erupts, dealing 27 (6d8) fire damage to
the creature.

3. _Grey/Disease_ ___Fetid Bite.___ When the prism spider makes
a bite attack, the target must succeed
on a DC 17 Constitution saving throw
against disease or become poisoned
until the disease is cured. Every 24 hours
that elapse, the target must repeat
the saving throw, reducing its hit point
maximum by 16 (3d10) on a failure.
The disease is cured on a success. The
target dies if the disease reduces its hit
point maximum to 0. This reduction to
the target’s hit point maximum lasts until
the disease is cured.

4. _Blue/Lightning_ ___Shocking Aura.___ Until the end of the
spider’s next turn, creatures who enter
or begin their turns within 15 feet of
the prism spider must make a DC 16
Dexterity saving throw, taking 27 (6d8)
lightning damage on a failed saving
throw, or half as much damage on
a successful one. Creatures wearing
metal armor have disadvantage on this
saving throw.

5. _White/Cold_ ___Hoarfrost.___ Until the beginning of the
spider’s next turn, creatures who enter
or begin their turns within 15 feet of
the prism spider must make a DC 16
Constitution saving throw. On a failed
saving throw, the target takes 28 (8d6)
cold damage. A creature who takes
cold damage from this effect has their
movement speed halved, cannot take
reactions, and can only take one action
or one bonus action on their turn. They
can only make one melee or ranged
attack on their turn, regardless of other
class features or magic items.

6. _Purple/--_ ___Heal.___ The spider magically heals
itself for 36 (8d8) damage, and it has
advantage on saving throws against
spells and other magical effects until the
end of its next turn.

___Spider Climb.___ The prism spider can climb difficult surfaces, including
upside down on ceilings, without needing to make an ability check.

___Web Sense.___ While in contact with a web, the spider knows the exact
location of any other creature in contact with the same web.

___Web Walker.___ The spider ignores movement restrictions caused by
webbing.

**Actions**

___Multiattack.___ The spider uses one of its shifting power abilities and
makes a bite attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 13
(2d8 + 4) piercing damage.

___Web (Recharge 5–6).___ Ranged Weapon Attack: +7 to hit, range 30/60
ft., one creature. Hit: The target is restrained by webbing. As an
action, the restrained target can make a DC 14 Strength
check, bursting the webbing on a success. The
webbing can also be attacked and destroyed (AC
10; hp 10; the same resistances that the prism
spider is benefiting from at that time; immunity
to bludgeoning, poison, and psychic damage).
