"Owl";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 120 ft."
_skills_: "Perception +3, Stealth +3"
_speed_: "5 ft., fly 60 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "11"
_stats_: | 3 (-4) | 13 (+1) | 8 (-1) | 2 (-4) | 12 (+1) | 7 (-2) |

___Flyby.___ The owl doesn't provoke opportunity attacks when it flies out of an enemy's reach.

___Keen Hearing and Sight.___ The owl has advantage on Wisdom (Perception) checks that rely on hearing or sight.

**Actions**

___Talons.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 1 slashing damage.