"Sahuagin Baron";;;_size_: Medium humanoid (sahuagin)
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Sahuagin"
_senses_: "darkvision 120 ft."
_skills_: "Perception +7"
_saving_throws_: "Dex +5, Con +6, Int +5, Wis +4"
_speed_: "30 ft., swim 50 ft."
_hit points_: "76 (9d10+27)"
_armor class_: "16 (breastplate)"
_stats_: | 19 (+4) | 15 (+2) | 16 (+3) | 14 (+2) | 13 (+1) | 17 (+3) |

___Blood Frenzy.___ The sahuagin has advantage on melee attack rolls against any creature that doesn't have all its hit points.

___Limited Amphibiousness.___ The sahuagin can breathe air and water, but it needs to be submerged at least once every 4 hours to avoid suffocating.

___Shark Telepathy.___ The sahuagin can magically command any shark within 120 feet of it, using a limited telepathy.

**Actions**

___Multiattack.___ The sahuagin makes three attacks: one with his bite and two with his claws or trident.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (2d4 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Trident.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 11 (2d6 + 4) piercing damage, or 13 (2d8 + 4) piercing damage if used with two hands to make a melee attack.