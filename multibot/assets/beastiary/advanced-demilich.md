"Advanced Demilich";;;_size_: Tiny undead
_alignment_: neutral evil
_challenge_: "24 (62,000 XP)"
_languages_: "All, telepathy 120 ft."
_skills_: "Arcana +13, History +13, Perception +12, Religion +13"
_senses_: "truesight 120 ft., passive Perception 22"
_saving_throws_: "Con +12, Int +13, Wis +12"
_damage_immunities_: "necrotic, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "bludgeoning, piercing, and slashing from magical weapons"
_condition_immunities_: "charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned, prone, stunned"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "172 (23d4 + 115)"
_armor class_: "21 (natural armor)"
_stats_: | 10 (+0) | 10 (+0) | 20 (+5) | 23 (+6) | 20 (+5) | 23 (+6) |

___Annulment.___ If the demi-lich is subjected to an effect that allows it to
make a saving throw to take only half damage, it instead takes no damage
if it succeeds on the saving throw, and only half damage if it fails.

___Legendary Resistance (3/day).___ If the demi-lich fails a saving throw, it
can choose to succeed instead.

___Spellcasting.___ The demi-lich is an 18th level spellcaster. Its
spellcasting ability is Intelligence (spell save DC 21, +13
to hit with spell attacks). It has the following spells
prepared:

Cantrips (at will): _mage hand, prestidigitation, ray of frost_

* 1st level (4 slots): _detect magic, magic missile, shield, thunderwave_

* 2nd level (3 slots): _acid arrow, detect thoughts, invisibility, mirror
image_

* 3rd level (3 slots): _animate dead, counterspell, dispel magic, fireball_

* 4th level (3 slots): _blight, dimension door_

* 5th level (3 slots): _cloudkill, scrying_

* 6th level (1 slot): _disintegrate, globe of invulnerability_

* 7th level (1 slot): _finger of death, plane shift_

* 8th level (1 slot): _dominate monster, power word stun_

* 9th level (1 slot): _power word kill_

___Turn Immunity.___ The demi-lich is immune to effects that turn undead.

**Actions**

___Drain Life.___ Each non-undead creature within 10 feet of the demi-lich
must make a DC 17 Constitution saving throw against this magic, taking
21 (6d6) necrotic damage, and the demi-lich regains hit points equal to the
total dealt to all targets.

___Soul Shatter (Recharge 6).___ The demi-lich emits a string of vile words
of power. All creatures within 30 feet of the demi-lich that it can see must
succeed on a DC 17 Constitution saving throw or drop to 0 hit points.
On a successful save, the creature takes 22 (4d10) psychic damage and is
frightened until the end of its next turn.

**Legendary** Actions

The demi-lich can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only at
the end of another creature’s turn. The demi-lich regains spent legendary
actions at the start of its turn.

___Flight.___ The demi-lich can move up to its full movement speed and does
not invoke opportunity attacks while doing so.

___Bone Dust.___ Blinding bone dust swirls magically around the demi-lich.
Each creature within 5 feet of the demi-lich must succeed on a DC 17
Constitution saving throw or be blinded until the end of the creature’s
next turn.

___Frightening Glare (Costs 2 Actions).___ The demi-lich targets one
creature it can see within 60 feet of it. If the target can see the demilich,
it must succeed on a DC 17 Wisdom saving throw against this magic
or become frightened until the end of the demi-lich’s next turn. If the
target fails the saving throw by 5 or more, it is also paralyzed for the same
duration. A target that succeeds on the saving throw is immune to the
Dreadful Glare effect for the next 24 hours.

___Profane Curse (Costs 3 Actions).___ The demi-lich targets one creature it
can see within 30 feet of it. The target must succeed on a DC 17 Wisdom
saving throw or be magically cursed. Until the curse ends, the target has
disadvantage on ability checks, attack rolls, and saving throws. The target
can repeat the saving throw at the end of each of its turns, ending the curse
on a success.
