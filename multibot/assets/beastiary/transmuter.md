"Transmuter";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "any four languages"
_skills_: "Arcana +6, History +6"
_saving_throws_: "Int +6, Wis +4"
_speed_: "30 ft."
_hit points_: "40 (9d8)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 11 (0) | 17 (+3) | 12 (+1) | 11 (0) |

___Spellcasting.___ The transmuter is a 9th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks). The transmuter has the following wizard spells prepared:

* Cantrips (at will): _light, mending, prestidigitation, ray of frost_

* 1st level (4 slots): _chromatic orb, expeditious retreat\*, mage armor_

* 2nd level (3 slots): _alter self\*, hold person, knock\* _

* 3rd level (3 slots): _blink\*, fireball, slow\* _

* 4th level (3 slots): _polymorph\*, stoneskin_

* 5th level (1 slot): _telekinesis\* _

*Transmutation spell of 1st level or higher

___Transmuter's Stone.___ The transmuter carries a magic stone it crafted that grants its bearer one of the following effects:

* Darkvision out to a range of 60 feet
* An extra 10 feet of speed while the bearer is unencumbered
* Proficiency with Constitution saving throws
* Resistance to acid, cold, fire, lightning, or thunder damage (transmuter's choice whenever the transmuter chooses this benefit)

If the transmuter has the stone and casts a transmutation spell of 1st level or higher, it can change the effect of the stone.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d6-1) bludgeoning damage, or 3 (1d8-1) bludgeoning damage if used with two hands.
