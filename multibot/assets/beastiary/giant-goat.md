"Giant Goat";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_speed_: "40 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "11 (natural armor)"
_stats_: | 17 (+3) | 11 (0) | 12 (+1) | 3 (-4) | 12 (+1) | 6 (-2) |

___Charge.___ If the goat moves at least 20 ft. straight toward a target and then hits it with a ram attack on the same turn, the target takes an extra 5 (2d4) bludgeoning damage. If the target is a creature, it must succeed on a DC 13 Strength saving throw or be knocked prone.

___Sure-Footed.___ The goat has advantage on Strength and Dexterity saving throws made against effects that would knock it prone.

**Actions**

___Ram.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) bludgeoning damage.