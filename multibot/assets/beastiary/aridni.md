"Aridni";;;_size_: Small fey
_alignment_: neutral evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Gnoll, Sylvan, Void Speech"
_skills_: "Acrobatics +11, Perception +3, Stealth +11"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +8"
_speed_: "20 ft., fly 60 ft."
_hit points_: "82 (15d6 + 30)"
_armor class_: "15"
_stats_: | 9 (-1) | 21 (+5) | 14 (+2) | 12 (+1) | 11 (+0) | 16 (+3) |

___Flyby.___ The aridni doesn't provoke an opportunity attack when it flies out of an enemy's reach.

___Magic Resistance.___ The aridni has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The aridni's innate spellcasting ability is Charisma (spell save DC 14). It can innately cast the following spells:

* At will: _dancing lights, detect magic, invisibility_

* 3/day: _charm person, faerie fire, mage armor_

* 1/day: _spike growth_

**Actions**

___Short Sword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) piercing damage.

___Pixie Bow.___ Ranged Weapon Attack: +8 to hit, range 40/160 ft., one target. Hit: 7 (1d4 + 5) piercing damage.

___Slaver Arrows.___ An aridni can add a magical effect in addition to the normal damage done by its arrows. If so, the aridni chooses from the following effects:

* **Confusion.** The target must succeed on a DC 14 Wisdom saving throw or become confused (as the spell) for 2d4-1 rounds.

* **Fear.** The target must succeed on a DC 14 Wisdom saving throw or become frightened for 2d4 rounds.

* **Hideous Laughter.** The target must succeed on a DC 14 Wisdom saving throw or become incapacitated for 2d4 rounds. While incapacitated, the target is prone and laughing uncontrollably.

* **Sleep.** The target must succeed on a DC 14 Wisdom saving throw or fall asleep for 2d4 minutes. The creature wakes up if it takes damage or if another creature takes an action to shake it awake.

