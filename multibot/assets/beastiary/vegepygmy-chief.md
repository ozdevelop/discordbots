"Vegepygmy Chief";;;_size_: Small plant
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Vegepygmy"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +4"
_speed_: "30 ft."
_hit points_: "33 (6d6+12)"
_armor class_: "14 (natural armor)"
_damage_resistances_: "lightning, piercing"
_stats_: | 14 (+2) | 14 (+2) | 14 (+2) | 7 (-2) | 12 (+1) | 9 (-1) |

**Actions**

___Multiattack.___ The vegepygmy makes two attacks with its claws or two melee attacks with its spear.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) slashing damage.

___Spear.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d6+2) piercing damage, or 6 (id8+2) piercing damage if used with two hands to make a melee attack.

___Spores (1/Day).___ A 15-foot-radius cloud of toxic spores extends out from the vegepygmy. The spores spread around corners. Each creature in that area that isn't a plant must succeed on a DC 12 Constitution saving throw or be poisoned. While poisoned in this way, a target takes 9 (2d8) poison damage at the start of each of its turns. A target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.