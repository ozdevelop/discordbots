"Kalka-Kylla";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Olman"
_senses_: "blindsight 30 ft."
_skills_: "Deception +3, Insight +5, Stealth +3"
_speed_: "30 ft., swim 30 ft."
_hit points_: "85 (10d10+30)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 12 (+1) | 16 (+3) | 15 (+2) | 16 (+3) | 12 (+1) |

___Source.___ tales from the yawning portal,  page 238

___Amphibious.___ Kalka-Kylla can breathe air and water.

___False Appearance.___ While Kalka-Kylla remains motionless and hidden in its shell, it is indistinguishable from a polished boulder.

___Shell.___ Kalka-Kylla can use a bonus action to retract into or emerge from its shell. While retracted, Kalka-Kylla gains a +4 bonus to AC, and it has a speed of 0 and can't benefit from bonuses to speed.

**Actions**

___Multiattack.___ Kalka-Kylla makes two claw attacks.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage, and if the target is a Medium or smaller creature, it is grappled (escape DC 13). Until this grapple ends, the target is restrained. Kalka-Kylla has two claws, each of which can grapple only one target.