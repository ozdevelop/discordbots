"Assassin Vine";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "blindsight 30 ft., passive Perception 10"
_condition_immunities_: "blinded, deafened, exhaustion, prone"
_damage_resistances_: "cold, fire"
_speed_: "5 ft., climb 5 ft."
_hit points_: "85 (10d10 +30)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 10 (0) | 16 (+3) | 1 (-5) | 10 (+0) | 1 (-5) |

___False Appearance.___ Whlle the assassin vine remains motionless, it is indistinguishable from a normal plant.

**Actions**

___Constrict.___  Mele Weapon Attack: +6 to hit, reach 20 ft., one target. Hit: The target takes 11 (2d6 +4) bludgeoning damage, and It is grappled (escape DC 14). Until this grapple ends, the target is restrained, and it takes 21 (6d6) poison damage at the start of each of its turns. The vine can constrict only one target at a time.

___Entangling Vines.___ The assassin vine can animate normal vines and roots on the ground in a 15-foot square within 30 feet of it. These plants turn the ground in that area in to difficult terrain. A creature in that area when the effect begins must succeed on a DC 13 Strength saving throw or be restrained by entangling vines and roots. A creature restrained by the plants can use its action to make a DC 13 Strength (Athletics) check, freeing itself on a successful check. The effect ends after 1 minute or when the assassin vine dies or uses Entangling Vines again.