"Sword Wraith Warrior";;;_page_number_: 241
_size_: Medium undead
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "45  (6d8 + 18)"
_armor class_: "16 (chain shirt, shield)"
_condition_immunities_: "exhaustion, frightened, poisoned, unconscious"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 18 (+4) | 12 (+1) | 17 (+3) | 6 (-2) | 9 (0) | 10 (0) |

___Martial Fury.___ As a bonus action, the sword wraith can make one weapon attack. If it does so, attack rolls against it have advantage until the start of its next turn.

**Actions**

___Longsword___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, or 9 (1d10 + 4) slashing damage if used with two hands.

___Longbow___ Ranged Weapon Attack: +3 to hit, range 150/600 ft., one target. Hit: 5 (1d8 + 1) piercing damage.