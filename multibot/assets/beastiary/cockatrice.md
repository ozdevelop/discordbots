"Cockatrice";;;_size_: Small monstrosity
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "darkvision 60 ft."
_speed_: "20 ft., fly 40 ft."
_hit points_: "27 (6d6+6)"
_armor class_: "11"
_stats_: | 6 (-2) | 12 (+1) | 12 (+1) | 2 (-4) | 13 (+1) | 5 (-3) |

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 3 (1d4 + 1) piercing damage, and the target must succeed on a DC 11 Constitution saving throw against being magically petrified. On a failed save, the creature begins to turn to stone and is restrained. It must repeat the saving throw at the end of its next turn. On a success, the effect ends. On a failure, the creature is petrified for 24 hours.