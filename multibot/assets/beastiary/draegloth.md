"Draegloth";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Abyssal, Elvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +3, Stealth +5"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "123 (13d10+52)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 20 (+5) | 15 (+2) | 18 (+4) | 13 (+1) | 11 (0) | 11 (0) |

___Fey Ancestry.___ The draegloth has advantage on saving throws against being charmed, and magic can't put it to sleep.

___Innate Spellcasting.___ The draegloth's innate spellcasting ability is Charisma (spell save DC 11). The draegloth can innately cast the following spells, requiring no material components:

* At will: _darkness_

* 1/day each: _confusion, dancing lights, faerie fire_

**Actions**

___Multiattack.___ The draegloth makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 16 (2d10+5) piercing damage.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 10 ft, one target. Hit: 16 (2d10+5) slashing damage.
