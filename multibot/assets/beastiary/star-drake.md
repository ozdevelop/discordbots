"Star Drake";;;_size_: Large dragon
_alignment_: neutral
_challenge_: "15 (13000 XP)"
_languages_: "Celestial, Common, Draconic, Dwarvish, Elvish, Infernal, Primordial"
_skills_: "Arcana +8, History +8, Insight +12, Perception +12, Religion +8"
_senses_: "truesight 120 ft., passive Perception 22"
_saving_throws_: "Dex +8, Con +10, Int +8, Wis +12, Cha +10"
_damage_immunities_: "cold, fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, frightened, paralyzed, unconscious"
_speed_: "40 ft., fly 100 ft."
_hit points_: "189 (18d10 + 90)"
_armor class_: "19 (natural armor)"
_stats_: | 20 (+5) | 17 (+3) | 21 (+5) | 16 (+3) | 24 (+7) | 20 (+5) |

___Legendary Resistance (2/day).___ If the star drake fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The drake has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The drake's weapon attacks are magical.

___Nimbus of Stars.___ The drake is surrounded by a whirling nimbus of tiny motes of starlight. A sighted creature that starts its turn within 10 feet of the drake must make a successful DC 18 Constitution saving throw or become incapacitated. At the start of a character's turn, a character can choose to avert its eyes and gain immunity against this effect until the start of its next turn, but it must treat the drake as invisible while the character's eyes are averted.

___Innate Spellcasting.___ The drake's innate spellcasting ability is Wisdom (spell save DC 20). It can innately cast the following spells, requiring no material components:

At will: faerie fire, moonbeam

3/day: plane shift

1/day each: gate, planar binding

**Actions**

___Multiattack.___ The drake makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 15 (3d6 + 5) slashing damage.

___Breath Weapon (Recharge 5-6).___ The drake exhales either fire or frigid air in a 40-foot cone. Each creature in that area takes 78 (12d12) fire or cold damage, whichever the drake wishes, or half damage with a successful DC 18 Dexterity saving throw.

___Searing Star (1/Day).___ Ranged Spell Attack: +12 to hit, range 120 ft., one target. Hit: 65 (10d12) force damage, and the target must succeed on a DC 18 Constitution saving throw or be permanently blinded.

**Legendary** Actions

___The drake can take 3 legendary actions, choosing from the options below.___ Only one option can be used at a time and only at the end of another creature's turn. The drake regains spent legendary actions at the start of its turn.

___Bite Attack.___ The drake makes one bite attack.

___Nova (Costs 2 Actions).___ The drake momentarily doubles the radius and intensity of its nimbus of stars. Every sighted creature within 20 feet of the drake must make a successful DC 18 Constitution saving throw or become blinded until the end of its next turn. Characters who are averting their eyes are immune to the nova.

___Pale Sparks.___ The drake casts faerie fire or moonbeam.

