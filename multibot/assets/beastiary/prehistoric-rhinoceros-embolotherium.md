"Prehistoric Rhinoceros (Embolotherium)";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_speed_: "40 ft."
_hit points_: "76 (9d10 + 27)"
_armor class_: "13 (natural armor)"
_stats_: | 22 (+6) | 8 (-1) | 16 (+3) | 2 (-4) | 11 (+0) | 6 (-2) |

___Charge.___ If the embolotherium moves at least 20 feet straight toward a
target and then hits it with a gore attack on the same turn, the target takes
an extra 11 (2d10) bludgeoning damage. If the target is a creature, it must
succeed on a DC 16 Strength saving throw or be knocked prone.

___Siege Monster.___ The embolotherium deals double damage to objects and
structures.

**Actions**

___Gore.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 28
(4d10 + 6) bludgeoning damage.
