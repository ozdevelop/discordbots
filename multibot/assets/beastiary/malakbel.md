"Malakbel";;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: "9 (5000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_skills_: "Perception +7"
_senses_: "truesight 30 ft., passive Perception 17"
_saving_throws_: "Dex +7, Wis +7"
_damage_immunities_: "fire, radiant, poison"
_damage_resistances_: "cold, lightning; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "blinded, poisoned"
_speed_: "40 ft."
_hit points_: "102 (12d8 + 48)"
_armor class_: "14 (natural armor)"
_stats_: | 14 (+2) | 17 (+3) | 19 (+4) | 13 (+1) | 16 (+3) | 20 (+5) |

___Blistering Radiance.___ The malakbel generates a 30-foot-radius aura of searing light and heat. A creature that starts its turn in the aura, or who enters it for the first time on a turn, takes 11 (2d10) radiant damage. The area in the aura is brightly lit, and it sheds dim light for another 30 feet. The aura dispels magical darkness of 3rd level or lower where the areas overlap.

___Distortion.___ Ranged attacks against the malakbel have disadvantage.

___Magic Resistance.___ The malakbel has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The malakbel makes two scorching blast attacks.

___Scorching Blast.___ Ranged Spell Attack: +9 to hit, range 120 ft., one target. Hit: 18 (3d8 + 5) fire damage.

___Searing Flare (Recharge 5-6).___ The malakbel intensifies its Blistering Radiance to withering levels. All creatures in the malakbel's aura take 31 (7d8) radiant damage and gain a level of exhaustion; a successful DC 16 Constitution saving throw reduces damage by half and negates exhaustion.

___Teleport.___ The malakbel magically teleports to an unoccupied space it can see within 100 feet.

