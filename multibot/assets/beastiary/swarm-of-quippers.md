"Swarm of Quippers";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_senses_: "darkvision 60 ft."
_speed_: "0 ft., swim 40 ft."
_hit points_: "28 (8d8-8)"
_armor class_: "13"
_condition_immunities_: "charmed, frightened, grappled, paralyzed, petrified, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, slashing"
_stats_: | 13 (+1) | 16 (+3) | 9 (-1) | 1 (-5) | 7 (-2) | 2 (-4) |

___Blood Frenzy.___ The swarm has advantage on melee attack rolls against any creature that doesn't have all its hit points.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny quipper. The swarm can't regain hit points or gain temporary hit points.

___Water Breathing.___ The swarm can breathe only underwater.

**Actions**

___Bites.___ Melee Weapon Attack: +5 to hit, reach 0 ft., one creature in the swarm's space. Hit: 14 (4d6) piercing damage, or 7 (2d6) piercing damage if the swarm has half of its hit points or fewer.