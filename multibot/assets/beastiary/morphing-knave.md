"Morphing Knave";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any three languages"
_skills_: "Acrobatics +8, Animal Handling +11, Investigation +6, Perception +7, Stealth +12"
_senses_: "passive Perception 17"
_saving_throws_: "Dex +8, Int +6"
_speed_: "30 ft., climb 30 ft."
_hit points_: "66 (12d8 + 12)"
_armor class_: "15 (leather)"
_stats_: | 8 (-1) | 18 (+4) | 12 (+1) | 14 (+2) | 16 (+3) | 10 (+0) |

___Feline Grace.___ The knave has a climb speed equal to
its movement speed and does not take fall damage
from heights less than 30 feet. In addition, the
knave's footsteps are completely silent.

___Sneak Attack (1/Turn).___ The knave deals an extra 14
(4d6) damage when it hits a target with a weapon
attack and has advantage on the attack roll, or when
the target is within 5 feet of an ally of the knave
that isn't incapacitated and the knave doesn't have
disadvantage on the attack roll.

___Spellcasting.___ The knave is a 3th-level spellcaster. Its
spellcasting ability is wisdom (spell save DC 15, +7
to hit with spell attacks). The knave has the
following druid spells prepared:

* Cantrips (at will): _mending, thorn whip_

* 1st level (4 slots): _fog cloud, jump, longstrider_

* 2nd level (2 slots): _darkvision, enhance ability, locate object_

___Wild Shape - Cat (2/Short Rest).___ As a bonus action,
the knave transforms into a cat for up to two hours.
When the cat is reduced to 0 hit points, the knave
reverts to its normal form, with any excess damage
carrying over to the knave's hit points.

**Actions**

___Multiattack.___ The knave makes two attacks with its
claws.

___Steel Claws.___ Melee Weapon Attack: +8 to hit, reach
5 ft., one target. Hit: 9 (2d4 + 4) slashing damage.
