"Fanged Sea Serpent";;;_size_: Large dragon
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Aquan, Draconic"
_skills_: "Athletics +7, Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned, prone"
_speed_: "0 ft., swim 40 ft."
_hit points_: "93 (11d10 + 33)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 16 (+3) | 5 (-3) | 11 (+0) | 6 (-2) |

___Amphibious.___ The fanged serpent can breathe air and water.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 10
(1d12 + 4) piercing damage plus 7 (2d6) poison damage, and the target is
grappled (escape DC 15). Until this grapple ends, the target is restrained
and the sea serpent cannot grapple another target. While the fanged serpent
has a creature grappled in its bite, it cannot bite a different creature, and
has advantage on bite attacks against the grappled creature.

___Roll.___ One target that the fanged serpent has grappled takes 21 (6d6)
slashing damage as the fanged serpent rolls quickly. After this damage is
dealt, the target is no longer grappled.
