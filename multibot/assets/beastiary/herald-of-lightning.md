"Herald of Lightning";;;_size_: Large celestial
_alignment_: chaotic good
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Celestial"
_skills_: "Perception +7, Persuasion +6, Sleight of Hand +8"
_senses_: "darkvision 60 ft., passive Perception 17"
_damage_immunities_: "lightning"
_speed_: "50 ft., fly 50 ft."
_hit points_: "152 (16d10 + 64)"
_armor class_: "18 (breastplate, shield)"
_stats_: | 16 (+3) | 18 (+4) | 19 (+4) | 11 (+0) | 17 (+3) | 14 (+2) |

___Magic Resistance.___ The herald has advantage on saving
throws against spells and other magical effects.

___One With Lightning.___ The herald may choose to expend
50 feet of movement to teleport to an unoccupied
space within 50 feet. When it does so, lightning streaks
in a line between the herald’s starting position and its
new position. Any creature in that area must succeed
on a DC 16 Dexterity saving throw or take 16 (3d10)
lightning damage.

**Actions**

___Multiattack.___ The herald makes two attacks: one with its
scimitar and one with its shield slam.

___Sparking Scimitar.___ Melee Weapon Attack: +8 to hit,
reach 5ft., one target. Hit: 7 (1d6 + 4) slashing damage
plus 16 (3d10) lightning damage.

___Shield Slam.___ Melee Weapon Attack: +7 to hit, reach 5ft.,
one target. Hit: 14 (2d10 + 3) bludgeoning damage
and the target must succeed on a DC 15 Strength
saving throw or be stunned until the end of its next
turn.

___Jolting Blast.___ Ranged Spell Attack: +7 to hit, range 120
ft., one target. Hit: 28 (8d6) lightning damage and the
target must succeed on a DC 16 Constitution saving
throw or be paralyzed until the end of its next turn.

___Lightning Shackles.___ Target creature within 120 feet has
their feet bound to the ground with chains of sparking
metal. That creature may move freely, but takes 5
(1d10) lightning damage for every 5 feet they move. A
creature may use their action to attempt to break the
bindings. That creature makes a DC 16 Athletics check,
smashing the bindings and freeing the bound creature
on a success. On a fail, the creature attempting to
break the bindings takes 11 (2d10) lightning damage.

___Cone of Lightning (Recharge 5-6).___ The herald unleashes
a blast of sparking energy in a 30-foot cone. All
creatures in that area must make on a DC 16 Dexterity
saving throw, taking 38 (7d10) lightning damage on a
failed save, or half as much damage on a successful
one.

___Create Conduits (1/Day).___ The herald turns three
creatures within 120 feet into conduits of lightning for
1 minute. If any of those creatures end their turn within
20 feet of another conduit, lightning arcs between
them, dealing 16 (3d10) lightning damage to each of
them. A creature between two conduits when this
effect occurs must succeed on a DC 16 Dexterity
saving throw or take 16 (3d10) lightning damage.
Creatures are immediately aware of the fact they are a
conduit when they become one, but are not aware of
its effects.
