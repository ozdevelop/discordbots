"Herald Of Darkness";;;_size_: Large fiend
_alignment_: neutral evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Elvish, Goblin, Infernal, Sylvan"
_skills_: "Athletics +8, Deception +8, Perception +5"
_senses_: "darkvision 200 ft., passive Perception 15"
_saving_throws_: "Str +8, Con +8, Cha +8"
_damage_immunities_: "cold, lightning, necrotic, poison"
_damage_resistances_: "bludgeoning, thunder"
_condition_immunities_: "exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., swim 30 ft, fly 50 ft."
_hit points_: "105 (10d10 + 50)"
_armor class_: "15 (chain shirt)"
_stats_: | 20 (+5) | 14 (+2) | 20 (+5) | 12 (+1) | 15 (+2) | 20 (+5) |

___Corrupting Touch.___ A herald of darkness can destroy any wooden, leather, copper, iron, or paper object by touching it as a bonus action. A mundane item is destroyed automatically; a magical item survives if its owner makes a successful DC 16 Dexterity saving throw.

___Gift of Darkness.___ A herald of darkness can transform any fey, human, or goblin into one of the shadow fey, if the target willingly accepts this transformation.

___Shadow Form.___ A herald of darkness can become incorporeal as a shadow as a bonus action. In this form, it has a fly speed of 10 feet; it can enter and occupy spaces occupied by other creatures; it gains resistance to all nonmagical damage; it has advantage on physical saving throws; it can pass through any gap or opening; it can't attack, interact with physical objects, or speak. It can return to its corporeal form also as a bonus action.

**Actions**

___Multiattack.___ The herald of darkness uses Majesty of the Abyss, if it is available, and makes one melee attack.

___Embrace Darkness.___ Melee Weapon Attack. +8 to hit, reach 5 ft., all creatures in reach. Hit: 6 (1d12) necrotic damage and targets are paralyzed until the start of the herald's next turn. Making a DC 17 Constitution saving throw negates the paralysis.

___Majesty of the Abyss (Recharge 4-6).___ The herald of darkness emits a sinister burst of infernal power. All creatures within 30 feet and in direct line of sight of the herald take 19 (3d12) necrotic damage and must make a DC 17 Constitution saving throw. Those who fail the saving throw are blinded for 2 rounds; those who succeed are frightened for 2 rounds.

___Shadow Sword.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 18 (2d12 + 5) slashing damage.

