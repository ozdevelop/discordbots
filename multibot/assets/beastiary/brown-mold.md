"Brown Mold";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_damage_vulnerabilities_: "cold, bludgeoning"
_damage_immunities_: "fire, poison; piercing from nonmagical weapons"
_condition_immunities_: "charmed, frightened, poisoned, stunned"
_speed_: "0 ft."
_hit points_: "5 (1d6 + 2)"
_armor class_: "5"
_stats_: | 1 (-5) | 1 (-5) | 15 (+2) | 1 (-5) | 10 (+0) | 1 (-5) |

___Fire Friend.___ A brown mold does not take damage from fire. Instead,
it heals that amount. If the available healing is greater than the brown
mold’s maximum number of hit points, its maximum hit points increases
by that amount.  Any source of fire brought within 5 feet of a patch causes it to instantly expand outward in the direction of the fire, covering up to a 10-foot-squre area.

___Cold Sensitive.___ Magical cold damage instantly and automatically kills
a brown mold.

___Cold Spores.___ When a creature moves to within 5 feet of the mold for the first time or starts its turn there, it must make a DC 12 Constitution saving throw, taking 22 (4d10) cold damage on a failed save, or half as much on a successful one.

**Actions**

___Spore Burst.___ The brown mold releases spores that burst out in a
cloud that fills a 15-foot cube centered on it, and the cloud lingers for 1
minute. Any creature that ends its turn in the cloud must make a DC 13
Constitution saving throw, taking 36 (8d8) necrotic damage on a failed
save, or half as much damage on a successful one. Additionally, the
target’s hit point maximum is reduced by an amount equal to the damage
taken. This reduction lasts until the target finishes a long rest. The target
dies if this effect reduces its hit point maximum to 0.
