"Star Spawn Hulk";;;_page_number_: 234
_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "10 (5900 XP)"
_languages_: "Deep Speech"
_senses_: "darkvision 60 ft., passive Perception 15"
_skills_: "Perception +5"
_saving_throws_: "Dex +3, Wis +5"
_speed_: "30 ft."
_hit points_: "136  (13d10 + 65)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "charmed, frightened"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 20 (+5) | 8 (-1) | 21 (+5) | 7 (-1) | 12 (+1) | 9 (0) |

___Psychic Mirror.___ If the hulk takes psychic damage, each creature within 10 feet of the hulk takes that damage instead; the hulk takes none of the damage. In addition, the hulk's thoughts and location can't be discerned by magic.

**Actions**

___Multiattack___ The hulk makes two slam attacks. If both attacks hit the same target, the target also takes 9 (2d8) psychic damage and must succeed on a DC 17 Constitution saving throw or be stunned until the end of the target's next turn.

___Slam___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage.

___Reaping Arms (Recharge 5-6)___ The hulk makes a separate slam attack against each creature within 10 feet of it. Each creature that is hit must also succeed on a DC 17 Dexterity saving throw or be knocked prone.