"Grotesque Leech";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Athletics +5"
_senses_: "passive Perception 10"
_speed_: "20 ft., swim 20 ft."
_hit points_: "32 (5d10 + 5)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 13 (+1) | 3 (-4) | 10 (+0) | 1 (-5) |

___Repulsive Slime.___ A creature that ends its turn within
5 feet of the leech must make a DC 13
Constitution saving throw. On a failed save, that
creature spends its action on its next turn retching
and reeling from the putrid aroma produced by the
leech. Creatures that don't need to breathe or are
immune to poison automatically succeed on this
saving throw.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 7 (1d8 + 3) piercing damage, and if
the target is a creature it is grappled (escape DC
13). Until the grapple ends, the target is restrained
and the leech can't bite another target.

___Drain.___ The leech drains the blood of a creature it is
currently grappling, dealing 10 (3d6) necrotic
damage to that creature and restoring that many hit
points to the leech.

___Blood Bile.___ Ranged Weapon Attack: +5 to hit, range
20/60 ft., one target. Hit: 7 (2d6) acid damage and
the target must succeed on a DC 13 Constitution
saving throw or become poisoned until the end of
its next turn.
