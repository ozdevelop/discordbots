"Maw Demon";;;_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "understands Abyssal but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "13 (natural armor)"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 14 (+2) | 8 (-1) | 13 (+1) | 5 (-3) | 8 (-1) | 5 (-3) |

___Rampage.___ When it reduces a creature to 0 hit points with a melee attack on its turn, the maw demon can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 11 (2d8+2) piercing damage.