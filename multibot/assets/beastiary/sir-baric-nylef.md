"Sir Baric Nylef";;;_size_: Medium humanoid (illuskan human)
_alignment_: lawful good
_challenge_: "0 (10 XP)"
_languages_: "Common"
_skills_: "Insight +4, Investigation +2, Medicine +4, Survival +4"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "18 (plate)"
_senses_: " passive Perception 12"
_stats_: | 18 (+4) | 11 (0) | 14 (+2) | 11 (0) | 15 (+2) | 15 (+2) |

___Brave.___ Baric has advantage on saving throws against being frightened.

**Actions**

___Maul.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage. Baric carries twenty crossbow bolts.

**Roleplaying** Information

As a knight of the Order of the Gauntlet, Sir Baric has sworn oaths to catch evildoers and bring them to justice. His current quarry is a dwarf brigand, Worvil "the Weezil" Forkbeard, who is rumored to be hiding in Iceland Dale. In addition to his gear, Sir Baric has an unarmored warhorse, Henry.

**Ideal:** "Evil must not be allowed to thrive in this world."

**Bond:** "Tyr is my lord; the order, my family. Through my actions, I shall honor both."

**Flaw:** "I'm not afraid to die. When Tyr finally calls me, I'll go to him happily."