"Derro Shadow Antipaladin";;;_size_: Small humanoid
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Derro, Undercommon"
_skills_: "Perception +0, Stealth +7"
_senses_: "darkvision 120 ft., passive Perception 10"
_saving_throws_: "Str +3, Wis +0, Cha +5"
_speed_: "30 ft."
_hit points_: "82 (11d6 + 44)"
_armor class_: "18 (breastplate and shield)"
_stats_: | 11 (+0) | 18 (+4) | 18 (+4) | 11 (+0) | 5 (-3) | 14 (+2) |

___Evasive.___ Against effects that allow a Dexterity saving throw for half damage, the derro takes no damage on a successful save, and only half damage on a failed one.

___Insanity.___ The derro has advantage on saving throws against being charmed or frightened.

___Magic Resistance.___ The derro has advantage on saving throws against spells and other magical effects.

___Shadowstrike.___ The derro's weapon attacks deal 9 (2d8) necrotic damage (included in its Actions list).

___Spellcasting.___ The derro is a 5th level spellcaster. Its spellcasting ability is Charisma (save DC 13, +5 to hit with spell attacks). The derro has the following paladin spells prepared:

1st level (4 slots): hellish rebuke, inflict wounds, shield of faith, wrathful smite

2nd level (2 slots): aid, crown of madness, darkness, magic weapon

___Sunlight Sensitivity.___ While in sunlight, the derro shadow antipaladin has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The derro makes two scimitar attacks or two heavy crossbow attacks.

___Scimitar.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage plus 9 (2d8) necrotic damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +7 to hit, range 100/400 ft., one target, Hit: 9 (1d10 + 4) piercing damage plus 9 (2d8) necrotic damage.

___Infectious Insanity (Recharge 5-6).___ The derro chooses a creature it can see within 30 feet and magically assaults its mind. The creature must succeed on a DC 13 Wisdom saving throw or be affected as if by a confusion spell for 1 minute. An affected creature repeats the saving throw at the end of its turns, ending the effect on itself on a success.

