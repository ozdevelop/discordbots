"Orc";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +2"
_speed_: "30 ft."
_hit points_: "15 (2d8+6)"
_armor class_: "13 (hide armor)"
_stats_: | 16 (+3) | 12 (+1) | 16 (+3) | 7 (-2) | 11 (0) | 10 (0) |

___Aggressive.___ As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.

**Actions**

___Greataxe.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 9 (1d12 + 3) slashing damage.

___Javelin.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 6 (1d6 + 3) piercing damage.