"Cambium";;;_size_: Large fiend
_alignment_: neutral evil
_challenge_: "14 (11500 XP)"
_languages_: "Common, Draconic, Infernal"
_skills_: "Arcana +8, Deception +9, Insight +8, Medicine +8, Perception +8, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Dex +8, Con +11, Int +8, Wis +8, Cha +9"
_damage_immunities_: "poison"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "40 ft."
_hit points_: "264 (23d10 + 138)"
_armor class_: "19 (natural armor)"
_stats_: | 21 (+5) | 16 (+3) | 23 (+6) | 17 (+3) | 16 (+3) | 18 (+4) |

___Innate Spellcasting.___ The cambium's innate spellcasting ability is Charisma (spell save DC 17, +9 to hit with spell attacks). The cambium can innately cast the following spells, requiring no material components:

Constant: levitate

At will: alter self, detect thoughts, hold person, plane shift, spare the dying

3/day: cure wounds 21 (4d8 + 3), ray of sickness 18 (4d8), protection from poison, heal

1/day: finger of death

**Actions**

___Multiattack.___ The cambium makes four needle fingers attacks.

___Needle Fingers.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 21 (3d10 + 5) piercing damage. In addition, the target must make a DC 19 Constitution saving throw; if it fails, the cambium can either inflict Ability Damage or Imbalance Humors. A target makes this saving throw just once per turn, even if struck by more than one needle fingers attack.

___Ability Damage (3/Day).___ When the target of the cambium's needle fingers fails its Constitution saving throw, one of its ability scores (cambium's choice) is reduced by 1d4 until it finishes a long rest. If this reduces a score to 0, the creature is unconscious until it regains at least one point.

___Imbalance Humors (3/Day).___ When the target of the cambium's needle fingers fails its Constitution saving throw, apply one of the following effects:

___Sanguine Flux: The target cannot be healed.___ aturally or magically.until after their next long rest.

___Choleric Flux: The target becomes confused (as the spell) for 3d6 rounds.___ The target can repeat the saving throw at the end of each of its turns to shrug off the flux before the duration ends.

___Melancholic Flux: The target is incapacitated for 1d4 rounds and slowed (as the spell) for 3d6 rounds.___ The target can repeat the saving throw at the end of each of its turns to shrug off the flux before the duration ends.

___Phlegmatic Flux: A successful DC 18 Constitution saving throw negates this effect.___ A failed saving throw means the target gains one level of exhaustion which lasts for 3d6 rounds.

