"Howling Hatred Initiate";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "1/8 (25 XP)"
_languages_: "Common"
_skills_: "Deception +2, Religion +2, Stealth +4"
_speed_: "30 ft."
_hit points_: "9 (2d8)"
_armor class_: "13 (leather)"
_stats_: | 10 (0) | 15 (+2) | 10 (0) | 10 (0) | 9 (-1) | 11 (0) |

___Guiding Wind (Recharges after a Short or Long Rest).___ As a bonus action, the initiate gains advantage on the next ranged attack roll it makes before the end of its next turn.

___Hold Breath.___ The initiate can hold its breath for 30 minutes.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.