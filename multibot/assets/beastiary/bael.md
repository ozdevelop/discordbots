"Bael";;;_page_number_: 170
_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "19 (22,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 23"
_skills_: "Intimidation +13, Perception +13, Persuasion +13"
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +9, Con +11, Int +11, Cha +13"
_speed_: "30 ft."
_hit points_: "189  (18d10 + 90)"
_armor class_: "18 (plate)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 24 (+7) | 17 (+3) | 20 (+5) | 21 (+5) | 24 (+7) | 24 (+7) |

___Dreadful.___ Bael can use a bonus action to appear dreadful until the start of his next turn. Each creature, other than a devil, that starts its turn within 10 feet of Bael must succeed on a DC 22 Wisdom saving throw or be frightened until the start of the creature's next turn.

___Innate Spellcasting.___ Bael's innate spellcasting ability is Charisma (spell save DC 21, +13 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

* At will: _alter self _(can become Medium when changing his appearance)_, animate dead, charm person, detect magic, inflict wounds _(as an 8th-level spell)_, invisibility _(self only)_, major image_

* 3/day each: _counterspell, dispel magic, fly, suggestion, wall of fire_

* 1/day each: _dominate monster, symbol _(stunning only)

___Legendary Resistance (3/Day).___ If Bael fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Bael has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Bael's weapon attacks are magical.

___Regeneration.___ Bael regains 20 hit points at the start of his turn. If he takes cold or radiant damage, this trait doesn't function at the start of his next turn. Bael dies only if he starts his turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack___ Bael makes two melee attacks.

___Hellish Morningstar___ Melee Weapon Attack: +13 to hit, reach 20 ft., one target. Hit: 16 (2d8 + 7) piercing damage plus 13 (3d8) necrotic damage.

___Infernal Command___ Each ally of Bael's within 60 feet of him can't be charmed or frightened until the end of his next turn. Teleport. Bael magically teleports, along with any equipment he is wearing and carrying, up to 120 feet to an unoccupied space he can see.

**Legendary** Actions

Bael can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Bael regains spent legendary actions at the start of his turn.

___Attack (Cost 2 Actions)___ Bael attacks once with his hellish morningstar.

___Awaken Greed___ Bael casts charm person or major image.

___Infernal Command___ Bael uses his Infernal Command action.

___Teleport___ Bael uses his Teleport action.
