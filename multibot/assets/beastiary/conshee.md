"Conshee";;;_size_: Tiny fey
_alignment_: chaotic neutral
_challenge_: "1/8 (25 XP)"
_languages_: "Common, Sylvan"
_skills_: "Acrobatics +4, Stealth +4"
_senses_: "darkvision 120 ft., passive Perception 11"
_speed_: "20 ft., fly 50 ft."
_hit points_: "7 (3d4)"
_armor class_: "12"
_stats_: | 6 (-2) | 15 (+2) | 11 (+0) | 12 (+1) | 13 (+1) | 13 (+1) |

___Innate Spellcasting.___ The conshee’s innate spellcasting ability is
Charisma (spell save DC 11, +3 to hit with spell attacks). It can cast the
following spells, requiring no material components.

* At will: _detect evil and good, light_

* 1/day each: _faerie fire, silent image_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one
target. Hit: 5 (1d6 + 2) piercing damage.
