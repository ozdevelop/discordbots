"Hydroloth";;;_page_number_: 249
_size_: Medium fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Abyssal, Infernal, telepathy 60 ft."
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 14"
_skills_: "Insight +4, Perception +4"
_damage_immunities_: "acid, poison"
_speed_: "20 ft., swim 40 ft."
_hit points_: "135  (18d8 + 54)"
_armor class_: "15"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 12 (+1) | 21 (+5) | 16 (+3) | 19 (+4) | 10 (0) | 14 (+2) |

___Amphibious.___ The hydroloth can breathe air and water.

___Innate Spellcasting.___ The hydroloth's innate spellcasting ability is Intelligence (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _darkness, detect magic, dispel magic, invisibility _(self only)_, water walk_

* 3/day each: _control water, crown of madness, fear, phantasmal killer, suggestion_

___Magic Resistance.___ The hydroloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The hydroloth's weapon attacks are magical.

___Secure Memory.___ The hydroloth is immune to the waters of the River Styx as well as any effect that would steal or modify its memories or detect or read its thoughts.

___Watery Advantage.___ While submerged in liquid, the hydroloth has advantage on attack rolls.

**Actions**

___Multiattack___ The hydroloth makes two melee attacks. In place of one of these attacks, it can cast one spell that takes 1 action to cast.

___Claws___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) slashing damage.

___Bite___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 16 (2d10 + 5) piercing damage.

___Steal Memory (1/Day)___ The hydroloth targets one creature it can see within 60 feet of it. The target takes 4d6 psychic damage, and it must make a DC 16 Intelligence saving throw. On a successful save, the target becomes immune to this hydroloth's Steal Memory for 24 hours. On a failed save, the target loses all proficiencies, it can't cast spells, it can't understand language, and if its Intelligence and Charisma scores are higher than 5, they become 5. Each time the target finishes a long rest, it can repeat the saving throw, ending the effect on itself on a success. A greater restoration or remove curse spell cast on the target ends this effect early.

___Teleport___ The hydroloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.
