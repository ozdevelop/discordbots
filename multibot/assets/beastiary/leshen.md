"Leshen";;;_size_: Large plant
_alignment_: neutral
_challenge_: "10 (5,900 XP)"
_languages_: "Understands Druidic and Sylvan but can't speak"
_skills_: "Animal Handling +6, Perception +7, Survival +7"
_senses_: "darkvision 60 ft., passive Perception 17"
_saving_throws_: "Str +12, Con +10, Cha +6"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks from non-silvered weapons"
_condition_immunities_: "charmed, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "172 (15d10 + 90)"
_armor class_: "19 (natural armor)"
_stats_: | 26 (+8) | 9 (-1) | 22 (+6) | 15 (+2) | 18 (+4) | 14 (+2) |

___Magic Resistance.___ The leshen has advantage on all saving throws against magical effects and spells.

___Innate Spellcasting.___ The leshen's innate spellcasting ability is Wisdom (spell save DC 17, +9 to hit with spell attacks). The leshen can innately cast the following spells, requiring no material components:

* At will: _entangle_

* 3/day: _misty step_

* 1/day: _conjure animals _(wolves and/or dire wolves)

___Rough Bark.___ At the start of each of its turns, the leshen deals 5 (1d10) piercing damage to any creature grappling it.

**Actions**

___Multiattack.___ The leshen makes two claw attacks.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 28 (4d10 + 6) slashing damage.

___Blink.___ The leshen can teleport 20 ft. away to an open area it can see in a swirl of crows.  Creatures within 5 ft. of the leshen at the start and end of the teleport must make a DC 17 Dexterity saving throw, taking 21 (4d8+3) piercing damage on a failed save or half as much on a success.

___Command Roots.___ The leshen targets up to two creatures within 60 ft. from it. The targets must make a DC 17 Dexterity save or take 26 (4d8+8) bludgeoning damage and become grappled (Escape DC 17). The target is restrained as long as the grapple remains. If the target ends their turn in the roots, they take an additional 26 (4d8+8) bludgeoning damage. 
