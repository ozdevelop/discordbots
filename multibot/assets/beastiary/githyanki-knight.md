"Githyanki Knight";;;_page_number_: 160
_size_: Medium humanoid (gith)
_alignment_: lawful evil
_challenge_: "8 (3,900 XP)"
_languages_: "Gith"
_saving_throws_: "Con +5, Int +5, Wis +5"
_speed_: "30 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "18 (plate)"
_stats_: | 16 (+3) | 14 (+2) | 15 (+2) | 14 (+2) | 14 (+2) | 15 (+2) |

___Innate Spellcasting (Psionics).___ The githyanki's innate spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _jump, misty step, nondetection _(self only)_, tongues_

* 1/day each: _plane shift, telekinesis_

**Actions**

___Multiattack.___ The githyanki makes two silver greatsword attacks.

___Silver Greatsword.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage plus 10 (3d6) psychic damage. This is a magic weapon attack. On a critical hit against a target in an astral body (as with the astral projection spell), the githyanki can cut the silvery cord that tethers the target to its material body, instead of dealing damage.
