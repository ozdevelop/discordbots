"Hobgoblin Devastator";;;_size_: Medium humanoid (goblinoid)
_alignment_: lawful evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +5"
_speed_: "30 ft."
_hit points_: "45 (7d8+14)"
_armor class_: "13 (studded leather armor)"
_stats_: | 13 (+1) | 12 (+1) | 14 (+2) | 16 (+3) | 13 (+1) | 11 (0) |

___Source.___ Volo's Guide to Monsters, p. 161

___Arcane Advantage.___ Once per turn, the hobgoblin can deal an extra 7 (2d6) damage to a creature it hits with a damaging spell attack if that target is within 5 feet of an ally of the hobgoblin and that ally isn't incapacitated.

___Army Arcane.___ When the hobgoblin casts a spell that causes damage or that forces other creatures to make a saving throw, it can choose itself and any number of allies to be immune to the damage caused by the spell and to succeed on the required saving throw.

___Spellcasting.___ The hobgoblin is a 7th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). It has the following wizard spells prepared:

* Cantrips (at will): _acid splash, fire bolt, ray of frost, shocking grasp_

* 1st level (4 slots): _fog cloud, magic missile, thunderwave_

* 2nd level (3 slots): _gust of wind, Melf's acid arrow, scorching ray_

* 3rd level (3 slots): _fireball, fly, lightning bolt_

* 4th level (1 slot): _ice storm_

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +3 to hit, reach 5 ft, one target. Hit: 4 (1d6+1) bludgeoning damage, or 5 (1d8+1) bludgeoning damage if used with two hands.
