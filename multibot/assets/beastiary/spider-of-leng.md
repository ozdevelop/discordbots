"Spider Of Leng";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Void Speech"
_skills_: "Athletics +5, Perception +3, Stealth +6"
_senses_: "darkvision 240 ft., passive Perception 13"
_saving_throws_: "Dex +6, Con +6, Int +6"
_damage_resistances_: "poison"
_condition_immunities_: "charmed, poisoned, unconscious"
_speed_: "30 ft., climb 20 ft."
_hit points_: "144 (17d10 + 51)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 16 (+3) | 16 (+3) | 17 (+3) | 10 (+0) | 10 (+0) |

___Eldritch Understanding.___ A spider of Leng can read and use any scroll.

___Innate Spellcasting.___ The spider of Leng's innate spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: comprehend languages, detect magic, shocking grasp

3/day each: shield, silence

1/day each: arcane eye, confusion, hypnotic pattern, stoneskin

___Poisonous Blood.___ An attacker who hits a spider of Leng with a melee attack from within 5 feet must make a successful DC 15 Dexterity saving throw or take 7 (2d6) poison damage and be poisoned until the start of its next turn.

___Shocking Riposte.___ When a spider of Leng casts shield, it can also make a shocking grasp attack for 9 (2d8) lightning damage against one enemy within 5 feet as part of the same reaction.

**Actions**

___Multiattack.___ A spider of Leng makes two claw attacks, two staff attacks, or one of each.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 14 (2d10 + 3) slashing damage plus 4 (1d8) poison damage.

___Spit Venom.___ Ranged Weapon Attack: +6 to hit, range 60 ft., one target. Hit: 16 (3d8 + 3) poison damage, and the target must make a successful DC 14 Constitution saving throw or be poisoned and blinded until the end of its next turn.

___Staff of Leng.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage plus 13 (2d12) psychic damage, and the target must make a successful DC 15 Wisdom saving throw or be stunned until the start of the spider's next turn.

**Reactions**

___Ancient Hatred.___ When reduced to 0 hp, the spider of Leng makes one final spit venom attack before dying.

