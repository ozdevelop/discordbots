"Leprechaun";;;_size_: Small fey
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Common, Sylvan"
_skills_: "Deception +5, Perception +6, Persuasion +5, Sleight of Hand +5, Stealth +7"
_senses_: "passive Perception 16"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed"
_speed_: "40 ft."
_hit points_: "14 (4d6)"
_armor class_: "13"
_stats_: | 7 (-2) | 16 (+3) | 11 (+0) | 16 (+3) | 15 (+2) | 16 (+3) |

___Innate Spellcasting.___ The leprechaun’s innate spellcasting ability is
Intelligence (spell save DC 13, +5 to hit with spell attacks). It can innately
cast the following spells, requiring no material components:

* At will: _dancing lights, hideous laughter, invisibility _(self only)_, mage hand, magic mouth, major image, minor illusion_

* 1/day each: _hypnotic pattern, major image_

___Sneak Attack (1/turn).___ The leprechaun deals an extra 3 (1d6) damage
when it hits a target with a weapon attack and has advantage on the attack
roll, or when the target is within 5 feet of an ally of the leprechaun that
isn’t incapacitated and the leprechaun doesn’t have disadvantage on the
attack roll.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.
