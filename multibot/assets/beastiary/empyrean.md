"Empyrean";;;_size_: Huge celestial (titan)
_alignment_: chaotic good (75%) or neutral evil (25%)
_challenge_: "23 (50,000 XP)"
_languages_: "all"
_senses_: "truesight 120 ft."
_skills_: "Insight +13, Persuasion +15"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Str +17, Int +12, Wis +13, Cha +15"
_speed_: "50 ft., fly 50 ft., swim 50 ft."
_hit points_: "313 (19d12+190)"
_armor class_: "22 (natural armor)"
_stats_: | 30 (+10) | 21 (+5) | 30 (+10) | 21 (+5) | 22 (+6) | 27 (+8) |

___Innate Spellcasting.___ The empyrean's innate spellcasting ability is Charisma (spell save DC 23, +15 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _greater restoration, pass without trace, water breathing, water walk_

* 1/day each: _commune, dispel evil and good, earthquake, fire storm, plane shift _(self only)

___Legendary Resistance (3/Day).___ If the empyrean fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The empyrean has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The empyrean's weapon attacks are magical.

**Actions**

___Maul.___ Melee Weapon Attack: +17 to hit, reach 10 ft., one target. Hit: 31 (6d6 + 10) bludgeoning damage. If the target is a creature, it must succeed on a DC 15 Constitution saving throw or be stunned until the end of the empyrean's next turn.

___Bolt.___ Ranged Spell Attack: +15 to hit, range 600 ft., one target. Hit: 24 (7d6) damage of one of the following types (empyrean's choice): acid, cold, fire, force, lightning, radiant, or thunder.

**Legendary** Actions

The empyrean can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The empyrean regains spent legendary actions at the start of its turn.

___Attack.___ The empyrean makes one attack.

___Bolster.___ The empyrean bolsters all nonhostile creatures within 120 feet of it until the end of its next turn. Bolstered creatures can't be charmed or frightened, and they gain advantage on ability checks and saving throws until the end of the empyrean's next turn.

___Trembling Strike (Costs 2 Actions).___ The empyrean strikes the ground with its maul, triggering an earth tremor. All other creatures on the ground within 60 feet of the empyrean must succeed on a DC 25 Strength saving throw or be knocked prone.
