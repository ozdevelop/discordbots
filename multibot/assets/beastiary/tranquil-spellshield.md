"Tranquil Spellshield";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any three languages"
_skills_: "Arcana +7, Insight +5, Nature +7, Persuasion +4"
_senses_: "passive Perception 12"
_saving_throws_: "Int +7, Wis +5"
_speed_: "30 ft."
_hit points_: "34 (8d6 + 8)"
_armor class_: "12 (15 with <i>mage armor</i>)"
_stats_: | 8 (-1) | 14 (+2) | 12 (+1) | 18 (+4) | 14 (+2) | 12 (+1) |

___Arcane Recovery (1/Day).___ When the spellshield
finishes a short rest, it can regain up to 3 total
expended spell slots.

___Arcane Ward.___ When the spellshield casts its first
abjuration spell of 1st level or higher for the day, it
gains 16 temporary hit points. After these
temporary hit points have been reduced to 0, any
additional abjuration spells the spellshield casts
this day provide 5 temporary hit points.

___Spellcasting.___ The spellshield is a 6th-level
spellcaster. Its spellcasting ability is Intelligence
(spell save DC 15, +7 to hit with spell attacks). The
spellshield has the following wizard spells
prepared:

* Cantrips (at will): _blade ward, mending, ray of frost, true strike_

* 1st level (4 slots): _alarm, mage armor, shield_

* 2nd level (3 slots): _arcane lock, darkvision, scorching ray_

* 3rd level (3 slots): _counterspell, dispel magic, lightning bolt_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 4
(1d4 + 2) piercing damage.

**Reactions**

___Project Ward.___ When a creature the spellshield can
see within 30 feet takes damage, it can use its
reaction to transfer the temporary hit points
granted by its Arcane Ward to that creature.
