"Sparking Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_damage_immunities_: "lightning"
_speed_: "30 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "12"
_stats_: | 10 (+0) | 14 (+2) | 14 (+2) | 16 (+3) | 12 (+1) | 6 (-2) |

___Spellcasting.___ The alchemist is a 3rd-level spellcaster. His
spellcasting ability is Intelligence (spell save DC 13, +5
to hit with spell attacks). He has the following Wizard
spells prepared:

* Cantrips (at will): _mage hand, shocking grasp_

* 1st level (4 slots): _chromatic orb, magic missile, witch bolt_

* 2nd level (3 slots): _mirror image, shatter_

**Actions**

___Multiattack.___ The alchemist makes two attacks with its
sparking strike.

___Sparking Strike.___ Melee Weapon Attack: +3 to hit, reach
5ft., one target. Hit: 2 (1d4) bludgeoning damage plus
3 (1d6) lightning damage.
