"Baron Malgas";;;_size_: Medium aberration
_alignment_: chaotic neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Deep Speech, Primordial"
_skills_: "Athletics +6, Deception +3, Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Str +6, Con +5"
_damage_resistances_: "psychic"
_condition_immunities_: "charmed, petrified"
_speed_: "30 ft."
_hit points_: "71 (11d8 + 22)"
_armor class_: "20 (plate, shield)"
_stats_: | 16 (+3) | 13 (+1) | 14 (+2) | 10 (+0) | 14 (+2) | 10 (+0) |

___Everchanging Changers.___ The Court of All Flesh are
beings of pure chaos. Because their minds are pure
disorder, they cannot be driven mad or charmed
and any attempts to magically compel their behavior fails.

___Formless Shape.___ Baron Malgas is immune to any
spell or effect that would alter his form.

**Actions**

___Multiattack.___ Baron Malgas makes three attacks
with his flail. He can use Melt Flesh in place of one
flail attack.

___Flail.___ Melee Weapon Attack: +6 to hit, range 5 ft.,
one target. Hit: 7 (1d8 + 3) bludgeoning damage.

___Javelin.___ Melee or Ranged Weapon Attack: +6 to hit,
reach 5 ft. or range 30/120 ft., one target. Hit: 12
(2d8 + 3) piercing damage.

___Melt Flesh.___ Baron Malgas chooses a target he can
see within 30 feet. The target must succeed on a
DC 13 Wisdom saving throw or watch as their dominant hand melts into a runny, fleshy lump. It drops
whatever weapon it was carrying, cannot hold a
weapon in that hand, and cannot cast spells that
require somatic components. At the end of each
of its turns, the target repeats this saving throw,
ending the effect on itself on a success. Casting
_mending_ on the affected hand also ends the effect.
If the target succeeds on a saving throw, the creature becomes immune to Baron Malgas’s Melt Flesh
for the next 24 hours.
