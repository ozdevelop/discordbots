"Valkyrie";;;_size_: Medium celestial
_alignment_: neutral
_challenge_: "11 (7200 XP)"
_languages_: "Common, Dwarvish, Giant, and see Gift of Tongues"
_skills_: "Perception +8"
_senses_: "truesight 60 ft., passive Perception 18"
_saving_throws_: "Str +12, Dex +12, Con +11, Int +5, Wis +8, Cha +12"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "acid, cold, fire, lightning, thunder"
_condition_immunities_: "frightened"
_speed_: "30 ft., fly 30 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "16 (chain mail) or 18 (chain mail with shield)"
_stats_: | 18 (+4) | 18 (+4) | 16 (+3) | 12 (+1) | 19 (+4) | 18 (+4) |

___Asgardian Weapons.___ The valkyrie's weapon attacks are magical. When she hits with any weapon, it does an extra 11 (2d10) radiant damage (included in attacks listed below).

___Cloak of Doom.___ Any living creature that starts its turn within 60 feet of a valkyrie senses her unsettling presence and must succeed on a DC 16 Charisma saving throw or be frightened for 1d4 rounds. Those who succeed are immune to the effect for 24 hours. The valkyrie can suppress this aura at will.

___Gift of Tongues.___ Valkyries become fluent in any language they hear spoken for at least 1 minute, and they retain this knowledge forever.

___Innate Spellcasting.___ The valkyrie's innate spellcasting ability is Wisdom (spell save DC 16, +8 to hit with spell attacks). She can innately cast the following spells, requiring no material components:

At will: bane, bless, invisibility, sacred flame, spare the dying, speak with animals, thaumaturgy

5/day each: gentle repose, healing word, warding bond

3/day each: beacon of hope, mass healing word, revivify

1/day each: commune, death ward, freedom of movement, geas

**Actions**

___Longsword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage or 9 (1d10 + 4) slashing damage if used with two hands, plus 11 (2d10) radiant damage.

___Spear.___ Melee or Ranged Weapon Attack: +8 to hit, reach 10 ft. or range 20/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage or 8 (1d8 + 4) piercing damage if used with two hands to make a melee attack, plus 11 (2d10) radiant damage.

**Legendary** Actions

___A valkyrie can take 3 legendary actions, choosing from the options below.___ Only one option can be used at a time and only at the end of another creature's turn. A valkyrie regains spent legendary actions at the start of her turn.

___Cast a Cantrip.___ The valkyrie casts one spell from her at-will list.

___Spear or Longsword Attack.___ The valkyrie makes one longsword or spear attack.

___Harvest the Fallen (Costs 2 Actions).___ A valkyrie can take the soul of a newly dead body and bind it into a weapon or shield. Only one soul can be bound to any object. Individuals whose souls are bound can't be raised by any means short of a wish or comparable magic. A valkyrie can likewise release any soul that has been bound by another valkyrie, or transfer a bound soul from one object to another. Once bound, the soul grants the item a +1 bonus for every 4 character levels of the soul (maximum of +3), and this replaces any other magic on the item. At the DM's discretion, part of this bonus can become an appropriate special quality (a fire giant's soul might create a flaming weapon, for example).

