"Elemental Water Dragon";;;_size_: Huge elemental
_alignment_: neutral evil
_challenge_: "17 (18,000 XP)"
_languages_: "Aquan, Common"
_skills_: "Arcana +9, Nature +9, Perception +14, Stealth +13"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 24"
_saving_throws_: "Dex +13, Con +11, Wis +8, Cha +10"
_damage_immunities_: "poison"
_damage_resistances_: "acid; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "40 ft., fly 60 ft., swim 80 ft."
_hit points_: "310 (27d12 + 135)"
_armor class_: "17"
_stats_: | 25 (+7) | 24 (+7) | 21 (+5) | 16 (+3) | 14 (+2) | 19 (+4) |

___Freeze.___ If the elemental water dragon takes cold damage, it partially
freezes; its speed is reduced by 20 feet until the end of its next turn.

___Innate Spellcasting.___ The dragon’s innate spellcasting ability is
Charisma (spell save DC 18, +10 to hit with spell attacks). It can cast the
following spells, requiring no material components:

* At will: _create or destroy water_

* 3/day: _control water_

* 1/day each: _control weather, plane shift_

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it can
choose to succeed instead.

___Siege Monster.___ The dragon deals double damage to objects and
structures.

**Actions**

___Multiattack.___ The dragon can make three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 18
(2d10 + 7) piercing damage.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 14
(2d6 + 7) slashing damage.

___Tail.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 16
(2d8 + 7) bludgeoning damage.

___Steam Breath (Recharge 5–6).___ The dragon exhales a 60-foot cone of
superheated steam that spreads around corners. Creatures within the area
must make DC 19 Dexterity saving throws, taking 63 (18d6) fire damage
on a failed saving throw, or half as much on a successful one. Being
underwater does not provide resistance to this effect.

**Bonus** Actions

___Drench.___ The elemental water dragon can extinguish any one nonmagical
flame or one magical flame created by a 5th level or lower spell it can see
within 15 feet of it.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 19 Dexterity
saving throw or take 14 (2d6 + 7) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.
