"Adult Blue Dracolich";;;_size_: Huge undead
_alignment_: lawful evil
_challenge_: "17 (18,000 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 60 ft., darkvision 120 ft."
_skills_: "Perception +12, Stealth +5"
_damage_immunities_: "lightning, poison"
_saving_throws_: "Dex +5, Con +11, Wis +7, Cha +9"
_speed_: "40 ft., burrow 30 ft., fly 80 ft."
_hit points_: "225 (18d12+108)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_damage_resistances_: "necrotic"
_stats_: | 25 (+7) | 10 (0) | 23 (+6) | 16 (+3) | 15 (+2) | 19 (+4) |

___Legendary Resistance (3/Day).___ If the dracolich fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The dracolich has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The dracolich can use its Frightful Presence. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 18 (2d10 + 7) piercing damage plus 5 (1d10) lightning damage.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) slashing damage.

___Tail.___ Melee Weapon Attack: +12 to hit, reach 15 ft., one target. Hit: 16 (2d8 + 7) bludgeoning damage.

___Frightful Presence.___ Each creature of the dracolich's choice that is within 120 feet of the dracolich and aware of it must succeed on a DC 18 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dracolich's Frightful Presence for the next 24 hours.

___Lightning Breath (Recharge 5-6).___ The dracolich exhales lightning in a 90-foot line that is 5 feet wide. Each creature in that line must make a DC 20 Dexterity saving throw, taking 66 (12d10) lightning damage on a failed save, or half as much damage on a successful one.

**Legendary** Actions

The adult blue dracolich can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The adult blue dracolich regains spent legendary actions at the start of its turn.

___Detect.___ The dracolich makes a Wisdom (Perception) check.

___Tail Attack.___ The dracolich makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dracolich beats its tattered wings. Each creature within 10 ft. of the dracolich must succeed on a DC 21 Dexterity saving throw or take 14 (2d6 + 7) bludgeoning damage and be knocked prone. After beating its wings this way, the dracolich can fly up to half its flying speed.