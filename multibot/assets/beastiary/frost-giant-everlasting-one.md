"Frost Giant Everlasting One";;;_size_: Huge giant (frost giant)
_alignment_: chaotic evil
_challenge_: "12 (8,400 XP)"
_languages_: "Giant"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +11, Perception +4"
_damage_immunities_: "cold"
_saving_throws_: "Str +11, Con +11, Wis +4"
_speed_: "40 ft."
_hit points_: "189 (14d12+98)"
_armor class_: "15 (patchwork armor)"
_stats_: | 25 (+7) | 9 (-1) | 24 (+7) | 9 (-1) | 10 (0) | 12 (+1) |

___Extra Heads.___ The giant has a 25 percent chance of having more than one head. If it has more than one, it has advantage on Wisdom (Perception) checks and on saving throws against being blinded, charmed, deafened, frightened, stunned, or knocked unconscious.

___Regeneration.___ The giant regains 10 hit points at the start of its turn. If the giant takes acid or fire damage, this trait doesn't function at the start of its next turn. The giant dies only if it starts its turn with 0 hit points and doesn't regenerate.

___Vaprak's Rage (Recharges on a Short or Long Rest).___ As a bonus action, the giant can enter a rage at the start of its turn. The rage lasts for 1 minute or until the giant is incapacitated. While raging, the giant gains the following benefits:

- The giant has advantage on Strength checks and Strength saving throws

- When it makes a melee weapon attack, the giant gains a +4 bonus to the damage roll.

- The giant has resistance to bludgeoning, piercing, and slashing damage.

**Actions**

___Multiattack.___ The giant makes two attacks with its greataxe.

___Greataxe.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 26 (3d12+7) slashing damage, or 30 (3d12+11) slashing damage while raging.

___Rock.___ Ranged Weapon Attack: +11 to hit, range 60/240 ft., one target. Hit: 29 (4d10+7) bludgeoning damage.