"Roachling Skirmisher";;;_size_: Small humanoid
_alignment_: chaotic neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Common"
_skills_: "Acrobatics +4, Stealth +6"
_senses_: "darkvision 60 ft., tremorsense 10 ft., passive Perception 9"
_saving_throws_: "Dexterity +4, Constitution +2"
_speed_: "25 ft."
_hit points_: "7 (2d6)"
_armor class_: "13 (natural armor)"
_stats_: | 10 (+0) | 14 (+2) | 11 (+0) | 10 (+0) | 9 (-1) | 8 (-1) |

___Resistant.___ The roachling skirmisher has advantage on Constitution saving throws.

___Unlovely.___ The skirmisher has disadvantage on Performance and Persuasion checks in interactions with nonroachlings.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Dart.___ Ranged Weapon Attack: +4 to hit, range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

