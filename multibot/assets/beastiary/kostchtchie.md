"Kostchtchie, Demon Prince of Wrath";;;_size_: Large fiend (demon lord)
_alignment_: chaotic evil
_challenge_: "22 (41,000 XP)"
_languages_: "Abyssal, Celestial, Common, Draconic, Giant, Ignan, Infernal, Terran; telepathy 120 ft."
_skills_: "Acrobatics +11, Athletics +15, Nature +10, Perception +10, Survival +10"
_senses_: "truesight 120 ft., passive Perception 20"
_saving_throws_: "Str +15, Dex +11, Con +14, Wis +10"
_damage_immunities_: "cold, lightning, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "acid, fire"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "312 (25d10 + 175)"
_armor class_: "19 (natural armor)"
_stats_: | 26 (+8) | 18 (+4) | 25 (+7) | 17 (+3) | 16 (+3) | 20 (+5) |

___Special Equipment.___ Kostchtchie carries Gorynya, a powerful magic warhammer.

___Innate Spellcasting.___ Kostchtchie’s innate spellcasting ability is
Charisma (spell save DC 20, +12 to hit with spell attacks). It can cast the
following spells, requiring no material components.

* At will: _command, darkness, detect magic, ice storm, sleet storm_

* 3/day each: _cone of cold, disintegrate, fire shield _(cold damage only)_, wall of ice_

* 1/day: _control weather_

___Legendary Resistance (3/day).___ If Kostchtchie fails a saving throw, it
can choose to succeed instead.

___Magic Resistance.___ Kostchtchie has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ Kostchtchie’s weapon attacks are magical.

___Unholy Aura.___ An unholy aura surrounds Kostchtchie out to a radius of
40 feet. A creature who enters or begins their turn in the area must make
a DC 20 Wisdom saving throw. On a failed saving throw, the target is
frightened for 1 minute. While frightened, it is paralyzed. A frightened
target can repeat the saving throw at the end of each of its turns, ending
the effect on a success.

**Actions**

___Multiattack.___ Kostchtchie makes two attacks with Gorynya.

___Gorynya.___ Melee Weapon Attack: +18 to hit, reach 15 ft., one target. Hit: 29 (4d8 + 11) bludgeoning damage, or 33 (4d10 + 11) bludgeoning damage if used with two hands, plus 22 (4d10) force damage. If the target is a creature, it must make a DC 23 Strength saving throw or be knocked prone.

___Summon (1/day).___ Kostchtchie summons 1d12 dretches, 1d6 glabrezus,
1d6 hezrous, 1 marilith, or 1 balor. The summoned demon appears in an
unoccupied space within 60 feet of Kostchtchie, but can’t summon other
demons. It remains for 1 minute, until it or Kostchtchie is slain, or until
Kostchtchie takes an action to dismiss it.

**Legendary** Actions

Kostchtchie can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. Kostchtchie regains spent legendary
actions at the start of its turn.

___Cold Burst.___ Kostchtchie causes a burst of cold to coalesce around it.
Creatures within 10 feet of it must make a DC 20 Constitution saving
throw. On a failed saving throw, the targets take 13 (3d8) cold damage, or
half as much damage on a successful one.

___Sunder (Costs 2 Actions).___ Kostchtchie slams Gorynya on the ground.
The area within 15 feet becomes difficult terrain. Each creature that is
concentrating must make a DC 23 Constitution saving throw. On a failed
save, the creature’s concentration is broken. In addition, all creatures
must make a DC 23 Dexterity saving throw or fall prone. Kostchtchie is
immune to these effects.

___Freeze (Costs 3 Actions).___ Kostchtchie chooses one target it can see
within 120 feet to make a DC 20 Constitution saving throw. On a failed
saving throw, the creature takes 22 (4d10) cold damage and is restrained.
At the start of its next turn, the target must repeat the saving throw. On
a failed saving throw, it is fully encased in ice and is petrified. On a
successful saving throw, the creature suffers no other effect.

**Lair** Actions

On initiative count 20 (losing initiative ties), Kostchtchie can take a
lair action to cause one of the following effects; Kostchtchie can’t use the
same effect two rounds in a row:

___Rage.___ Kostchtchie chooses any number of creatures within its lair
to make a DC 20 Wisdom saving throw; Kostchtchie must be aware of
intruders within its lair, but he does not need to see the targets. Until
initiative 20 a creature who fails the saving throw is filled with blinding
rage and uses its actions and movement to attack the nearest creature that
it can see.

___Corpse Cleanup.___ Kostchtchie destroys any corpse within its lair; its
does not need to see it in order to destroy it. That creature is incapable of
being brought back from the dead absent a true resurrection or wish spell.

___Curse.___ Kostchtchie chooses one creature that it can see within 60 feet
to make a DC 20 Constitution saving throw. On a failed saving throw,
the creature is cursed; the curse ends early if removed with magic such
as remove curse. While cursed, the creature is vulnerable to cold damage.

**Regional** Effects

The region containing Kostchtchie’s lair is warped by his magic,
creating one or more of the following effects:

___Cold.___ The area within 6 miles of Kostchtchie’s lair is unnaturally cold,
even if that would not make sense; during the day the temperature hovers
around freezing, while at night the temperature plummets to below 0
degrees Fahrenheit.

___Ire.___ The various beasts and creatures that inhabit the area around the
lair are more inclined to be hostile, filled with wrath, even attacking other
creatures who would not normally be their prey.

If Kostchtchie dies, these effects fade over the course of 1d10 days.

# Gorynya

_Weapon (warhammer), artifact (requires attunement)_

Gorynya is an overly large warhammer, wrought of black iron,
covered in frost. It stands almost 4 feet tall from rounded pommel
to the head, which bears a spike at its top and two blunt, unfinished
faces. It is remarkably devoid of ornamentation but bears what
appears to be centuries’ worth of chips, nicks, and dried blood.

___Attunement.___ Gorynya cannot be attuned to as long as
Kostchtchie, the Demon Lord of Wrath, is on the same plane of
existence as Gorynya. For you to attune to the weapon, you must
first slay an ally of like alignment in a fit of blind rage and remain
unremorseful for doing so.

___Oversized.___ Gorynya is much larger than a normal warhammer.
You can only wield Gorynya two-handed if your Strength score is
18 or higher. To wield Gorynya one-handed, your Strength score
must be 22 or higher.

___Magic Weapon.___ You have a +3 bonus to attack and damage rolls
made with Gorynya. Gorynya also functions as a mace of terror
and a staff of frost.

___Overwhelming.___ You deal an additional 4d10 force damage when
you hit with Gorynya, and your target must succeed on a Strength
saving throw or be knocked prone. The DC for this saving throw is
8 + your Strength modifier + your proficiency bonus.

___Cold Bones.___ You have resistance to cold damage while you are
attuned to Gorynya, and rime and frost coat every visible surface
within 10 feet of you.

___Destroying Gorynya.___ In order to destroy Gorynya, you must first
beg forgiveness from all those you have slain with the weapon, and
they must forgive you for the acts you committed while attuned to
the weapon. If that is done, then Gorynya shatters into rubble forever.
