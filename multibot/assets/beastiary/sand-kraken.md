"Sand Kraken";;;_size_: Large aberration
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +6"
_senses_: "darkvision 60 ft., tremorsense 120 ft., passive Perception 14"
_speed_: "20 ft., burrow 30 ft."
_hit points_: "85 (9d10 + 36)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 18 (+4) | 5 (-3) | 13 (+1) | 6 (-2) |

___Buried Camouflage.___ A sand kraken has advantage on Dexterity
(Stealth) checks and it has total cover from attacks as long as it remains
buried.

___Keen Smell.___ The sand kraken has advantage on Wisdom (Perception)
checks that rely on smell.

___Tentacles.___ A sand kraken has 10 tentacles, each of which can grapple
one creature. Each tentacle has an AC of 20, 15 hit points, and immunity
to poison and psychic damage. Severing or destroying a tentacle deals
no damage to the sand kraken, and severed tentacles regrow at a rate of
1 per day.

**Actions**

___Multiattack.___ The sand kraken makes three tentacle attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 30
(4d12 + 4) piercing damage. If the target is a creature, it must succeed on
a DC 15 Constitution saving throw against disease or become poisoned
until the disease is cured. Every 24 hours that elapse, the target must
repeat the saving throw, reducing its hit point maximum by 5 (1d10) on
a failure. The disease is cured on a success. The target dies if the disease
reduces its hit point maximum to 0. This reduction to the target’s hit point
maximum lasts until the disease is cured.

___Tentacles.___ Melee Weapon Attack: +7 to hit, reach 30 ft., one target. Hit:
13 (2d8 + 4) bludgeoning damage. The target is grappled (escape DC 15)
and restrained. Until the grapple ends, a grappled target takes 13 (2d8 + 4)
bludgeoning damage at the beginning of each of the sand kraken’s turns.
