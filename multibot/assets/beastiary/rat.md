"Rat";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 30 ft."
_speed_: "20 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "10"
_stats_: | 2 (-4) | 11 (0) | 9 (-1) | 2 (-4) | 10 (0) | 4 (-3) |

___Keen Smell.___ The rat has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Bite.___ Melee Weapon Attack: +0 to hit, reach 5 ft., one target. Hit: 1 piercing damage.