"Langdedrosa Cyanwrath";;;_size_: Medium humanoid (half-dragon)
_alignment_: lawful evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Athletics +6, Intimidation +3, Perception +4"
_saving_throws_: "Str +6, Con +5"
_speed_: "30 ft."
_hit points_: "57 (6d12+18)"
_armor class_: "17 (splint)"
_damage_resistances_: "lightning"
_stats_: | 19 (+4) | 13 (+1) | 16 (+3) | 10 (0) | 14 (+2) | 12 (+1) |

___Action Surge (Recharges on a Short or Long Rest).___ On his turn, Langdedrosa can take one additional action.

___Improved Critical.___ Langdedrosa's weapon attacks score a critical hit on a roll of 19 or 20.

**Actions**

___Multiattack.___ Langdedrosa attacks twice, either with his greatsword or spear.

___Greatsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Spear.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or ranged 20 ft.,/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Lightning Breath (Recharge 5-6).___ Langdedrosa breathes lightning in a 30-foot line that is 5 feet wide. Each creature in the line must make a DC 13 Dexterity saving throw, taking 22 (4d10) lightning damage on a failed save, or half as much damage on a successful one.