"Firenewt Warlock of Imix";;;_size_: Medium humanoid (firenewt)
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Draconic, Ignan"
_senses_: "darkvision 120 ft. (penetrates magical darkness)"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "10 (13 with mage armor)"
_stats_: | 13 (+1) | 11 (0) | 12 (+1) | 9 (-1) | 11 (0) | 14 (+2) |

___Amphibious.___ The firenewt can breathe air and water.

___Innate Spellcasting.___ The firenewt's innate spellcasting ability is Charisma. It can innately cast mage armor (self only) at will, requiring no material components.

___Spellcasting.___ The firenewt is a 3rd-level spellcaster. Its spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _fire bolt, guidance, light, mage hand, prestidigitation_

* 1st-2nd level (2 2nd-level slots): _burning hands, flaming sphere, hellish rebuke, scorching ray_

___Imix's Blessing.___ When the firenewt reduces an enemy to 0 hit points, the firenewt gains 5 temporary hit points.

**Actions**

___Morningstar.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8+1) piercing damage.
