"Bouda";;;_size_: Medium fiend
_alignment_: neutral evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Celestial, Infernal, Nurian; telepathy 100 ft."
_skills_: "Athletics +7, Deception +5, Intimidation +5, Perception +4, Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 14"
_saving_throws_: "Dex+5, Con +7, Wis +4, Cha +5"
_damage_immunities_: "fire, poison"
_damage_resistances_: "acid, lightning; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "93 (11d8 + 44)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 18 (+4) | 10 (+0) | 12 (+1) | 15 (+2) |

___Shapechanger.___ The bouda can use its action to polymorph into a human, a hyena, or its true form, which is a hyena-humanoid hybrid. Its statistics, other than its Mephitic Claw attack, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if destroyed, before turning to dust.

___Defiling Smear (1/Day).___ The bouda can secrete a disgusting whitish-yellow substance with the viscosity of tar to mark food and territory. As a bonus action, the bouda marks a single adjacent 5-foot space, object, or helpless creature. Any living creature within 30 feet of the smear at the start of its turn must succeed on a DC 15 Constitution saving throw against poison or be poisoned for 1d6 rounds. A creature that makes a successful saving throw is immune to that particular bouda's defiling smear for 24 hours. The stench of a smear remains potent for one week.

___Innate Spellcasting.___ The bouda's innate spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It can cast the following spells, requiring no material components: Constant: detect evil and good, detect magic

At will: thaumaturgy

3/day: darkness, expeditious retreat

1/day: contagion

**Actions**

___Multiattack.___ The bouda makes one bite attack and one mephitic claw attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage plus 10 (3d6) poison damage.

___Mephitic Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage, and the target must make a successful DC 15 Constitution saving throw or become poisoned for 1 round by the visible cloud of vermin swarming around the bouda's forearms.

___Ravenous Gorge.___ The bouda consumes the organs of a corpse in a space it occupies. It gains temporary hit points equal to the dead creature's HD that last 1 hour. Organs consumed by this ability are gone, and the creature can't be restored to life through spells and magical effects that require a mostly intact corpse.

