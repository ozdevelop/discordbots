"Haste Devil";;;_size_: Medium fiend (devil)
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Infernal"
_skills_: "Acrobatics +9"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical, nonsilvered weapons"
_condition_immunities_: "grappled, restrained, poisoned, paralyzed"
_speed_: "90 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "16"
_stats_: | 14 (+2) | 22 (+6) | 14 (+2) | 10 (+0) | 10 (+0) | 9 (-1) |

___Devil’s Sight.___ Magical darkness doesn’t impede the
devil’s darkvision.

___Impossible Speed.___ The devil cannot have its speed
reduced by any effect and can move triple its speed
when it takes the dash action.

___Magic Resistance.___ The devil has advantage on saving
throws against spells and other magical effects.

___Numbing Strikes.___ Whenever the devil makes a melee
attack against a creature, it doesn’t provoke attacks
of opportunity from that creature for the rest of the
turn, whether it hits or not.

**Actions**

___Multiattack.___ The devil uses its Whirlwind ability. It
then makes three attacks with its unarmed strike.

___Unarmed Strike.___ Melee Weapon Attack: +9 to hit,
reach 5ft., one target. Hit: 8 (1d4 + 6) bludgeoning
damage.

___Whirlwind.___ The devil performs a quick spin that
generates a whirlwind in a 10 ft. radius sphere
centered on itself. All other creatures within this
whirlwind must succeed on a DC 13 Strength
saving throw or take 10 (3d6) bludgeoning damage
and be flung up to 20 feet away from the whirlwind
in a random direction and knocked prone. If a
thrown target strikes an object, such as a·wall or
floor, the target takes 3 (1d6) bludgeoning damage
for every 10 feet it was thrown. If the target is
thrown at another creature, that creature must
succeed on a DC 13 Dexterity saving throw or take
the same damage and be knocked prone.
