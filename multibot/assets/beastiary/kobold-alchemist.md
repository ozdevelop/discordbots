"Kobold Alchemist";;;_size_: Small humanoid
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "Common, Draconic"
_skills_: "Arcana +5, Medicine +3"
_senses_: "darkvision 60 ft., passive Perception 9"
_saving_throws_: "Dex +5"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "44 (8d6 + 16)"
_armor class_: "15 (studded leather)"
_stats_: | 7 (-2) | 16 (+3) | 15 (+2) | 16 (+3) | 9 (-1) | 8 (-1) |

___Pack Tactics.___ The kobold has advantage on an attack roll against a target if at least one of the kobold's allies is within 5 feet of the target and the ally isn't incapacitated.

___Sunlight Sensitivity.___ While in sunlight, the kobold has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The kobold makes two attacks.

___Dagger.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage plus 5 (2d4) poison damage.

___Dart.___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage plus 5 (2d4) poison damage.

___Alchemical Protection (Recharge after a Short or Long Rest).___ The kobold chooses up to six allied creatures within 10 feet. It releases alchemical vapors that grant those allies resistance to poison damage for 10 minutes. Instead of poison damage, the kobold can grant resistance to the damage type currently in effect for its Apothecary trait.

___Explosive Flask (Recharge 5-6).___ The kobold throws a flask of volatile substances at a point within 30 feet. The flask explodes in a 15-foot radius. Creatures in the area take 17 (5d6) poison damage and are poisoned for 1 minute, or take half damage and are not poisoned with a successful DC 13 Dexterity saving throw. A poisoned creature repeats the saving throw at the end of each of its turns, ending the poisoned condition on a success. Instead of poison damage, the kobold can deal the damage type currently in effect for its Apothecary trait.

**Bonus** Actions

___Apothecary.___ The kobold selects one of the following damage types: acid, cold, or fire. Until it uses this bonus action again, the kobold has resistance to the chosen damage type. Additionally, the kobold is proficient with a poisoner's kit.
