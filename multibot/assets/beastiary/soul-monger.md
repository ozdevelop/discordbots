"Soul Monger";;;_page_number_: 226
_size_: Medium humanoid (elf)
_alignment_: neutral
_challenge_: "11 (7200 XP)"
_languages_: "Common, Elvish"
_senses_: "darkvision 60 ft., passive Perception 17"
_skills_: "Perception +7"
_damage_immunities_: "necrotic, psychic"
_saving_throws_: "Dex +7, Wis +7, Cha +5"
_speed_: "30 ft."
_hit points_: "123  (19d8 + 38)"
_armor class_: "15 (studded leather)"
_condition_immunities_: "charmed, exhaustion, frightened"
_stats_: | 8 (-1) | 17 (+3) | 14 (+2) | 19 (+4) | 15 (+2) | 13 (+1) |

___Fey Ancestry.___ The soul monger has advantage on saving throws against being charmed, and magic can't put it to sleep.

___Innate Spellcasting.___ The soul monger's innate spellcasting ability is Intelligence (spell save DC 16, +8 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _chill touch _(3d8 damage)_, poison spray _(3d12 damage)

* 1/day each: _bestow curse, chain lightning, finger of death, gaseous form, phantasmal killer, seeming_

___Magic Resistance.___ The soul monger has advantage on saving throws against spells and other magical effects.

___Soul Thirst.___ When the soul monger reduces a creature to 0 hit points, the soul monger can gain temporary hit points equal to half the creature's hit point maximum. While the soul monger has temporary hit points from this ability, it has advantage on attack rolls.

___Weight of Ages.___ Any beast or humanoid, other than a shadar-kai, that starts its turn within 5 feet of the soul monger has its speed reduced by 20 feet until the start of that creature's next turn.

**Actions**

___Multiattack___ The soul monger makes two phantasmal dagger attacks.

___Phantasmal Dagger___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (4d4 + 3) piercing damage plus 19 (3d12) necrotic damage, and the target has disadvantage on saving throws until the start of the soul monger's next turn.

___Wave of Weariness (Recharge 4-6)___ The soul monger emits weariness in a 60-foot cube. Each creature in that area must make a DC 16 Constitution saving throw. On a failed save, a creature takes 45 (10d8) psychic damage and suffers 1 level of exhaustion. On a successful save, it takes 22 (5d8) psychic damage.
