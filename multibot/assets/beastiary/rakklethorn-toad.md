"Rakklethorn Toad";;;_size_: Small monstrosity
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "20 ft."
_hit points_: "13 (3d6 + 3)"
_armor class_: "13 (natural armor)"
_stats_: | 10 (+0) | 15 (+2) | 12 (+1) | 2 (-4) | 10 (+0) | 6 (-2) |

___Keen Smell.___ The rakklethorn toad has advantage on Wisdom
(Perception) checks that rely on smell.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 4 (1d4 + 2) piercing damage.

___Thorn Volley.___ Ranged Weapon Attack: +4 to hit, range 50 ft.,
one target. Hit: 5 (1d6 + 2) slashing damage, and the target must
succeed on a DC 11 Constitution saving throw or become poisoned
for 1 hour.
