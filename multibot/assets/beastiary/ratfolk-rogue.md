"Ratfolk Rogue";;;_size_: Small humanoid
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Thieves Cant"
_skills_: "Acrobatics +5, Perception +2, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "25 ft., swim 10 ft."
_hit points_: "18 (4d6 + 4)"
_armor class_: "15 (studded leather armor)"
_stats_: | 7 (-2) | 16 (+3) | 12 (+1) | 14 (+2) | 10 (+0) | 10 (+0) |

___Cunning Action.___ A ratfolk rogue can use a bonus action to Dash, Disengage, or Hide.

___Nimbleness.___ A ratfolk rogue can move through the space of any creature size Medium or larger.

___Pack Tactics.___ A ratfolk rogue has advantage on its attack roll against a creature if at least one of the ratfolk's allies is within 5 feet of the creature and the ally is capable of attacking.

___Sneak Attack (1/Turn).___ A ratfolk rogue deals an extra 3 (1d6) damage when it hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 feet of one of its allies that isn't incapacitated and the rogue doesn't have disadvantage on the attack roll.

**Actions**

___Dagger.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Light Crossbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Rat Dagger Flurry.___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., three targets. Hit: 7 (1d4 + 3) piercing damage.

