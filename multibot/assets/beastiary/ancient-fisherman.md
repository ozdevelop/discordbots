"Ancient Fisherman";;;_size_: Medium humanoid
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "any three languages"
_skills_: "Athletics +6, Nature +4, Perception +9, Survival +6"
_senses_: "passive Perception 19"
_saving_throws_: "Str +6, Wis +6"
_speed_: "30 ft., swim 30 ft."
_hit points_: "110 (13d10 + 39)"
_armor class_: "16 (mithril chainmail)"
_stats_: | 17 (+3) | 10 (+0) | 16 (+3) | 12 (+1) | 16 (+3) | 12 (+1) |

___Aquatic Expert.___ The fisherman can hold his breath
for up to 10 minutes and his abilities are
unimpeded when engaging in underwater combat.

___Innate Spellcasting.___ The fisherman’s innate
spellcasting ability is Wisdom (spell save DC 14).
The fisherman can innately cast the following
spells, requiring no material components:

* At will: _dancing lights, shape water_

* 1/day each: _control water, tidal wave, wall of water, watery sphere_

**Actions**

___Multiattack.___ The fisherman makes three attacks: one
with his harpoon and two with his hook.

___Giant Hook.___ Melee Weapon Attack: +6 to hit, reach
5ft., one target. Hit: 7 (1d8 + 3) piercing damage.

___Harpoon.___ Ranged Weapon Attack: +6 to hit, range
20/60 ft., one target. Hit: 6 (1d6 + 3) piercing
damage and the target is grappled (escape DC 14).

The fisherman may use his bonus action to pull the
target up to 30 feet towards him, dealing an
additional 9 (2d8) piercing damage. If the
fisherman uses this attack with a target already
grappled, the target is pulled up to an additional 30
feet and takes 9 (2d8) piercing damage. Whenever
the grappled creature attempts to break free of the
grapple, they immediately take 9 (2d8) piercing
damage at the harpoon tears at them.

___Catch and Release (Recharge 5-6).___ The fisherman
performs a vicious attack on a target grappled by
his harpoon within 5 feet. He makes an attack roll
with advantage using his giant hook. On a hit, he
pierces through the target with the hook, then rips
it from their body, dealing an additional 33 (6d10)
piercing damage with the attack.
