"Goliath Warrior";;;_size_: Medium humanoid (goliath)
_alignment_: lawful neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Giant"
_skills_: "Athletics +5, Survival +2"
_senses_: "passive Perception 10"
_speed_: "30 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "13 (hide armor)"
_stats_: | 16 (+3) | 13 (+1) | 17 (+3) | 10 (+0) | 11 (+0) | 10 (+0) |

**Actions**

___Multiattack.___ The goliath makes two attacks with
either its battleaxe or javelin.

___Battleaxe.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8
(1d10 + 3) slashing damage if used with two hands.

___Javelin.___ Melee or Ranged Weap on Attack: +5 to hit,
reach 5 ft. or range 30/120 feet, one target. Hit: 6
(1d6 + 3) piercing damage.

**Reactions**

___Stone’s Endurance.___ If the goliath takes damage and
remains above 0 hit points, it can make a
Constitution saving throw with a DC of 5 + the
damage taken. On a success, it gains 5 temporary
hit points.
