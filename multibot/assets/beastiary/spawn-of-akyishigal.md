"Spawn Of Akyishigal";;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Infernal, Spawn of Akyishigal"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Dex +4"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft., climb 15 ft."
_hit points_: "119 (14d8 + 56)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 13 (+1) | 19 (+4) | 10 (+0) | 11 (+0) | 12 (+1) |

___Command Vermin.___ Spawn of Akyishigal can command tiny beasts of Intelligence 2 or less within 30 feet that aren't under any magical control. The vermin obey the spawn's commands to the best of their ability, heedless of their own safety.

___Swarming Cough (recharge 5-6).___ The spawn can belch forth a swarm of insects. The swarm is completely under the spawn's control. It remains for 1 minute or until destroyed.

**Actions**

___Multiattack.___ The spawn makes one bite attack and two sting attacks.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) piercing damage.

___Sting.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 11 (2d8 + 2) piercing damage plus 5 (1d10) poison damage, and the target must succeed on a DC 15 Constitution saving throw or become poisoned for 1d6 rounds.

