"Chitine";;;_size_: Small monstrosity
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Undercommon"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +4, Stealth +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "18 (4d6+4)"
_armor class_: "14 (hide armor)"
_stats_: | 10 (0) | 14 (+2) | 12 (+1) | 10 (0) | 10 (0) | 7 (-2) |

___Fey Ancestry.___ The chitine has advantage on saving throws against being charmed, and magic can't put the chitine to sleep.

___Sunlight Sensitivity.___ While in sunlight, the chitine has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Web Sense.___ While in contact with a web, the chitine knows the exact location of any other creature in contact with the same web.

___Web Walker.___ The chitine ignores movement restrictions caused by webbing.

**Actions**

___Multiattack.___ The chitine makes three attacks with its daggers.

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4+2) piercing damage.