"Fire Snake";;;_size_: Medium elemental
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "understands Ignan but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "22 (5d8)"
_armor class_: "14 (natural armor)"
_damage_vulnerabilities_: "cold"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 12 (+1) | 14 (+2) | 11 (0) | 7 (-2) | 10 (0) | 8 (-1) |

___Heated Body.___ A creature that touches the snake or hits it with a melee attack while within 5 ft. of it takes 3 (1d6) fire damage.

**Actions**

___Multiattack.___ The snake makes two attacks: one with its bite and one with its tail.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage plus 3 (1d6) fire damage.

___Tail.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) bludgeoning damage plus 3 (1d6) fire damage.