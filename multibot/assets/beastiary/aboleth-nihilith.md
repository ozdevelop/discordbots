Aboleth, Nihilith;;;_size_: Large undead
_alignment_: chaotic evil
_challenge_: 12 (8400 XP)
_languages_: Void Speech, telepathy 120 ft.
_skills_: History +12, Perception +10
_senses_: darkvision 120 ft., passive Perception 20
_saving_throws_: Con +6, Int +8, Wis +6
_damage_immunities_: cold, necrotic, poison; bludgeoning, piercing and slashing from nonmagical weapons (only when in ethereal form)
_damage_resistances_: acid, fire, lightning, thunder (only when in ethereal form); bludgeoning, piercing and slashing from nonmagical weapons
_condition_immunities_: charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained
_speed_: 10 ft., swim 40 ft., fly 40 ft. (ethereal only, hover)
_hit points_: 135 (18d10 + 36)
_armor class_: 17 (natural armor)
_stats_: | 21 (+5) | 9 (-1) | 15 (+2) | 18 (+4) | 15 (+2) | 18 (+4) |

___Undead Fortitude.___ If damage reduces the nihileth to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is radiant or from a critical hit. On a success, the nihileth drops to 1 hit point instead.

___Dual State.___ A nihileth exists upon the Material Plane in one of two forms and can switch between them at will. In its material form, it has resistance to damage from nonmagical weapons. In its ethereal form, it is immune to damage from nonmagical weapons. The creature's ethereal form appears as a dark purple outline of its material form, with a blackish-purple haze within. A nihileth in ethereal form can move through air as though it were water, with a fly speed of 40 feet.

___Void Aura.___ The undead nihileth is surrounded by a chilling cloud. A living creature that starts its turn within 5 feet of a nihileth must make a successful DC 14 Constitution saving throw or be slowed until the start of its next turn. In addition, any creature that has been diseased by a nihileth or a nihilethic zombie takes 7 (2d6) cold damage every time it starts its turn within the aura.

___Infecting Telepathy.___ If a creature communicates telepathically with the nihileth, or uses a psychic attack against it, the nihileth can spread its disease to the creature. The creature must succeed on a DC 14 Wisdom save or become infected with the same disease caused by the nihileth's tentacle attack.

___Nihileth 's Lair.___ On initiative count 20 (losing initiative ties), the nihileth can take a lair action to create one of the magical effects as per an aboleth, or the void absorbance action listed below. The nihileth cannot use the same effect two rounds in a row.

- Void Absorbance: A nihileth can pull the life force from those it has converted to nihilethic zombies to replenish its own life. This takes 18 (6d6) hit points from zombies within 30 feet of the nihileth, spread evenly between the zombies, and healing the nihileth. If a zombie reaches 0 hit points from this action, it perishes with no Undead Fortitude saving throw.

___Regional Effects.___ The regional effects of a nihileth's lair are the same as that of an aboleth, except as following.

- Water sources within 1 mile of a nihileth's lair are not only supernaturally fouled but can spread the disease of the nihileth. A creature who drinks from such water must make a successful DC 14 Constitution check or become infected.

**Actions**

___Multiattack.___ The nihileth makes three tentacle attacks or three withering touches, depending on what form it is in.

___Tentacle (Material Form Only).___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage. If the target creature is hit, it must make a successful DC 14 Constitution saving throw or become diseased. The disease has no effect for 1 minute; during that time, it can be removed by lesser restoration or comparable magic. After 1 minute, the diseased creature's skin becomes translucent and slimy. The creature cannot regain hit points unless it is entirely underwater, and the disease can only be removed by heal or comparable magic. Unless the creature is fully submerged or frequently doused with water, it takes 6 (1d12) acid damage every 10 minutes. If a creature dies while diseased, it rises in 1d6 rounds as a nihilethic zombie. This zombie is permanently dominated by the nihileth.

___Withering Touch (Ethereal Form Only).___ Melee Weapon Attack: +8 to hit, reach 10 ft., one creature. Hit: 14 (3d6+4) necrotic damage.

___Form Swap.___ As a bonus action, the nihileth can alter between its material and ethereal forms at will.

___Tail (Material Form Only).___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 15 (3d6 + 5) bludgeoning damage.

___Enslave (3/day).___ The nihileth targets one creature it can see within 30 ft. of it. The target must succeed on a DC 14 Wisdom saving throw or be magically charmed by the nihileth until the nihileth dies or until it is on a different plane of existence from the target. The charmed target is under the nihileth's control and can't take reactions, and the nihileth and the target can communicate telepathically with each other over any distance. Whenever the charmed target takes damage, the target can repeat the saving throw. On a success, the effect ends. No more than once every 24 hours, the target can also repeat the saving throw when it is at least 1 mile away from the nihileth.

**Reactions**

___Void Body.___ The nihileth can reduce the damage it takes from a single source to 0. Radiant damage can only be reduced by half.

**Legendary** Actions

___A nihileth can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The nihileth regains spent legendary actions at the start of its turn.

___Detect.___ The aboleth makes a Wisdom (Perception) check.

___Tail Swipe.___ The aboleth makes one tail attack.

___Psychic Drain (Costs 2 Actions).___ One creature charmed by the aboleth takes 10 (3d6) psychic damage, and the aboleth regains hit points equal to the damage the creature takes.

___.___ The nihileth cannot use the same effect two rounds in a row.

___.___ A nihileth can pull the life force from those it has converted to nihilethic zombies to replenish its own life. This takes 18 (6d6) hit points from zombies within 30 feet of the nihileth, spread evenly between the zombies, and healing the nihileth. If a zombie reaches 0 hit points from this action, it perishes with no Undead Fortitude saving throw.

___The regional effects of a nihileth's lair are the same as that of an aboleth, except as following.___ 

___Water sources within 1 mile of a nihileth's lair are not only supernaturally fouled but can spread the disease of the nihileth.___ A creature who drinks from such water must make a successful DC 14 Constitution check or become infected.

