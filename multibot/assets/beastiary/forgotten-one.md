"Forgotten One";;;_size_: Tiny fey
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Elvish, Sylvan"
_senses_: "darkvision 60 ft., tremorsense 90 ft., passive Perception 13"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "20 ft."
_hit points_: "7 (3d4)"
_armor class_: "16 (natural armor)"
_stats_: | 3 (-4) | 19 (+4) | 11 (+0) | 14 (+2) | 17 (+3) | 20 (+5) |

___Innate Spellcasting.___ The forgotten one’s spellcasting ability is
Charisma (spell save DC 15, +7 to hit with spell attacks). The forgotten
one can innately cast the following, requiring no material components.

* At will: _blur_

___Environmental Awareness.___ The very earth speaks to a forgotten one. It
cannot be surprised.

___Poison Use.___ Forgotten Ones are skilled in the use of poison.

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 7 (2d6) poison damage.
