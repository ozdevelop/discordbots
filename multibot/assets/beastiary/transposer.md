"Transposer";;;_size_: Medium aberration
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Deep Speech"
_skills_: "Deception +3, Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "30 ft."
_hit points_: "49 (9d8 + 9)"
_armor class_: "12 (natural armor)"
_stats_: | 15 (+2) | 13 (+1) | 12 (+1) | 8 (-1) | 12 (+1) | 12 (+1) |

___Innate Spellcasting.___ The transposer’s
innate spellcasting ability is Charisma
(spell save DC 11, +3 to hit with spell
attacks). It can innately cast _alter self_ at
will requiring no material components.

___Transposition.___ An opponent hit by a transposer’s slam attack must make
a DC 13 Constitution saving throw or become linked to the transposer.
A transposer can have no more than 4 linked creatures at a time. While
linked, the transposer knows the exact location of the target as long as
it is on the same plane of existence.

While the transposer remains linked to a target, any damage the
transposer takes is halved, and each linked target takes an equal
amount. If a transposer’s linked target regain hit points, the
amount received by the target is halved, and the transposer
regains an equal amount of hit points.

A _remove curse_ or _dispel magic_ cast on the linked target can
end the link. If _dispel magic_ is cast on the transposer, one random link
is severed. Otherwise the link lasts until the targert dies or the transposer
chooses to end the link.

**Actions**

___Multiattack.___ The transposer makes two attacks with its slam.

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9
(2d6 + 2) bludgeoning damage.
