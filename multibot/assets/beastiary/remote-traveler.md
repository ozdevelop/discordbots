"Remote Traveler";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "Common"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 12"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "37 (6d8 + 10)"
_armor class_: "18 (natural armor)"
_stats_: | 14 (+2) | 11 (+0) | 13 (+1) | 16 (+3) | 14 (+2) | 10 (+0) |

___Antimagic Susceptibility.___ The traveler is incapacitated
while in the area of an antimagic field. If targeted by
dispel magic, the armor must succeed on a Constitution
saving throw against the caster's spell save DC or fall
unconscious for 1 minute.

___Homunculus Cage.___ The flail on the traveler's polearm flail
is actually a small metal cage containing an angry
homunculus. While the traveler is grappled, restrained,
or incapacitated, a creature can use its action to make a
DC 14 Dexterity (Sleight of Hand) check to open the
cage, releasing the homunculus. Once freed, the
homunculus doesn't attack when the traveler attacks
with the cage, and the homunculus becomes hostile to
the traveler.

___Remote Bond.___ The traveler follows its pilot's telepathic
commands as long as they are both on the same plane
of existence. In the absence of any such commands, the
traveler defends itself (and its pilot, if present). The
traveler does not need a command to use its reaction,
for example to make an attack of opportunity.
The traveler adds its pilot's proficiency bonus to its
damage rolls, and its proficiency bonus times its level to
its hit point maximum (included in its statistics).

**Actions**

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 5 (1d6 + 4) bludgeoning damage.

___Homunculus Cage.___ Melee Weapon Attack: +4 to hit,
reach 10 ft., one target. Hit: 7 (1d6 + 4) bludgeoning
damage, and if the target is a creature, the homunculus
makes a bite attack against it.

The homunculus' bite attack has a +4 to hit. On hit,
the attack deals 1 piercing damage, and the target must
succeed on a DC 10 Constitution saving throw or be
poisoned for 1 minute. If the saving throw fails by 5 or
more, the target is instead poisoned for 5 (1d10)
minutes and unconscious while poisoned in this way.

___Mend (3/Day).___ The traveler's pilot remotely casts
mending on the traveler, causing it to regain 6 (1d6 + 3)
hit points.
