"Chuul";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "understands Deep Speech but can't speak"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4"
_damage_immunities_: "poison"
_speed_: "30 ft., swim 30 ft."
_hit points_: "93 (11d10+33)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 19 (+4) | 10 (0) | 16 (+3) | 5 (-3) | 11 (0) | 5 (-3) |

___Amphibious.___ The chuul can breathe air and water.

___Sense Magic.___ The chuul senses magic within 120 feet of it at will. This trait otherwise works like the detect magic spell but isn't itself magical.

**Actions**

___Multiattack.___ The chuul makes two pincer attacks. If the chuul is grappling a creature, the chuul can also use its tentacles once.

___Pincer.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage. The target is grappled (escape DC 14) if it is a Large or smaller creature and the chuul doesn't have two other creatures grappled.

___Tentacles.___ One creature grappled by the chuul must succeed on a DC 13 Constitution saving throw or be poisoned for 1 minute. Until this poison ends, the target is paralyzed. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.