"Tosculi Warrior";;;_size_: Small monstrosity
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Tosculi"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "20 ft., fly 60 ft."
_hit points_: "58 (9d6 + 27)"
_armor class_: "15"
_stats_: | 12 (+1) | 20 (+5) | 16 (+3) | 10 (+0) | 12 (+1) | 12 (+1) |

___Skittering.___ Up to two tosculi can share the same space at one time. The tosculi has advantage on attack rolls while sharing its space with another tosculi that isn't incapacitated.

**Actions**

___Multiattack.___ The tosculi warrior makes one bite attack, one claws attack, and one stinger attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 7 (1d4 + 5) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 10 (2d4 + 5) slashing damage.

___Stinger.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 7 (1d4 + 5) piercing damage, and the target must succeed on a DC 13 Constitution saving throw against poison or be paralyzed for 1 minute. A paralyzed target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Prepare Host.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one paralyzed creature. Hit: 10 (2d4 + 5) piercing damage, and the target is paralyzed for 8 hours. The paralysis can be ended with a successful DC 20 Wisdom (Medicine) check or by a spell or magical effect that cures disease. (Because only paralyzed creatures can be targeted, a hit by this attack is automatically a critical hit; bonus damage is included in the damage listing.)

