"Chernomoi";;;_size_: Tiny fey
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic, Sylvan"
_skills_: "Acrobatics +6, Perception +2, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks not made with silver weapons"
_speed_: "20 ft., fly 20 ft."
_hit points_: "32 (5d4 + 20)"
_armor class_: "13"
_stats_: | 9 (-1) | 18 (+4) | 18 (+4) | 12 (+1) | 11 (+0) | 16 (+3) |

___Innate Spellcasting.___ The chernomoi's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no material components:

* At will: _detect magic, invisibility, mage hand, mending, message, prestidigitation_

* 1/day each: _detect poison and disease, dimension door_

**Actions**

___Scimitar.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

___Shriek (Recharge 5-6).___ The chernomoi emits a loud shriek. All creatures within 60 feet who can hear take 10 (3d6) thunder damage, or half damage with a successful DC 13 Constitution saving throw.

