"Giant Lizard";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "darkvision 30 ft."
_speed_: "30 ft., climb 30 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "12 (natural armor)"
_stats_: | 15 (+2) | 12 (+1) | 13 (+1) | 2 (-4) | 10 (0) | 5 (-3) |

___Variant: Hold Breath.___ The lizard can hold its breath for 15 minutes. (A lizard that has this trait also has a swimming speed of 30 feet.)

___Variant: Spider Climb.___ The lizard can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.