"Deathwisp";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "7 (2900 XP)"
_languages_: "the languages it knew in life"
_skills_: "Perception +6, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +8, Con +6, Wis +6"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "charmed, exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained"
_speed_: "0 ft., fly 60 ft. (hover)"
_hit points_: "82 (11d8 + 33)"
_armor class_: "15"
_stats_: | 6 (-2) | 20 (+5) | 16 (+3) | 18 (+4) | 16 (+3) | 20 (+5) |

___Flicker.___ The deathwisp flickers in and out of sight, and ranged weapon attacks against it are made with disadvantage.

___Incorporeal Movement.___ The deathwisp can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside a solid object.

___Shadow Jump.___ A deathwisp can travel between shadows as if by means of dimension door. This magical transport must begin and end in an area with at least some shadow. A shadow fey can jump up to a total of 40 feet per day; this may be a single jump of 40 feet, four jumps of 10 feet each, etc. This ability must be used in 10-foot increments.

___Sunlight Sensitivity.___ While in sunlight, the deathwisp has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Unnatural Aura.___ Animals do not willingly approach within 30 feet of a deathwisp, unless a master makes a successful DC 15 Wisdom (Animal Handling) check.

**Actions**

___Life Drain.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 36 (7d8 + 5) necrotic damage. The target must succeed on a DC 15 Constitution saving throw, or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

___Create Deathwisp.___ The deathwisp targets a humanoid within 10 feet of it that died violently less than 1 minute ago. The target's spirit rises as a wraith in the space of its corpse or in the nearest unoccupied space. This wraith is under the deathwisp's control. The deathwisp can keep no more than five wraiths under its control at one time.

