"Hobgoblin Captain";;;_size_: Medium humanoid (goblinoid)
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "39 (6d8+12)"
_armor class_: "17 (half plate)"
_stats_: | 15 (+2) | 14 (+2) | 14 (+2) | 12 (+1) | 10 (0) | 13 (+1) |

___Martial Advantage.___ Once per turn, the hobgoblin can deal an extra 10 (3d6) damage to a creature it hits with a weapon attack if that creature is within 5 ft. of an ally of the hobgoblin that isn't incapacitated.

**Actions**

___Multiattack.___ The hobgoblin makes two greatsword attacks.

___Greatsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) piercing damage.

___Javelin.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Leadership (Recharges after a Short or Long Rest).___ For 1 minute, the hobgoblin can utter a special command or warning whenever a nonhostile creature that it can see within 30 ft. of it makes an attack roll or a saving throw. The creature can add a d4 to its roll provided it can hear and understand the hobgoblin. A creature can benefit from only one Leadership die at a time. This effect ends if the hobgoblin is incapacitated.