"Githzerai Monk";;;_size_: Medium humanoid (gith)
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "Gith"
_skills_: "Insight +4, Perception +4"
_saving_throws_: "Str +3, Dex +4, Int +3, Wis +4"
_speed_: "30 ft."
_hit points_: "38 (7d8+7)"
_armor class_: "14"
_stats_: | 12 (+1) | 15 (+2) | 12 (+1) | 13 (+1) | 14 (+2) | 10 (0) |

___Innate Spellcasting (Psionics).___ The githzerai's innate spellcasting ability is Wisdom. It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _feather fall, jump, see invisibility, shield_

___Psychic Defense.___ While the githzerai is wearing no armor and wielding no shield, its AC includes its Wisdom modifier.

**Actions**

___Multiattack.___ The githzerai makes two unarmed strikes.

___Unarmed Strike.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) bludgeoning damage plus 9 (2d8) psychic damage. This is a magic weapon attack.
