"Red Bulette";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "12 (8,400 XP)"
_languages_: "--"
_skills_: "Perception +4"
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +5, Con +11, Wis +4"
_damage_immunities_: "fire"
_speed_: "40 ft., burrow 80 ft."
_hit points_: "135 (10d12 + 70)"
_armor class_: "18 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 24 (+7) | 3 (-4) | 10 (+0) | 5 (-3) |

___Metalsense.___ The red bulette is aware of all metals within 60 feet of it.

___Mineral Rich.___ If a red bulette is killed, its planar connection is
immediately severed. Over the course of 4 to 6 hours, its body will cool
to the ambient temperature of the environment. While the vast majority of
its body will cool into a basalt-like stone, its digestive track will solidify
into roughly 1,000 lbs. of an alloy admixture of every mineral substance
it has consumed. Depending upon its recent feeding habits, this
substance is the equivalent of metal-rich ore of many types in
combination, and can be smelted back down into standard and
precious metals by experts knowledgeable in such methods.
Additionally, a few diamonds will be found in what was the
red bulette’s gizzard.

___Planar Connection.___ Because their arcane natures
link red bulettes to the Elemental Plane of Fire, they also
simultaneously exist there as insubstantial and barely visible
shadows of themselves.

___Tunneler.___ The bulette can burrow through solid rock at its
burrow speed leaving a 10-foot-diameter tunnel in its wake.

___Vanishing Act.___ Red bulettes avoid overland movement,
preferring to swim through earth and bedrock. If confronted
above ground, they will reflexively increase their body temperatures
enough to simply melt their way down into the ground and vanish. If
threatened by a much larger creature, they simply dive underground and
“swim” away. However, if they are threatened by any creature that wears
or is carrying refined metals, they will instinctively return and attempt to
ambush with a bite/swallow attack from underground.

**Actions**

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 31
(4d12 + 5) piercing damage plus 7 (2d6) fire damage, and the target is
grappled (escape DC 15). Until this grapple ends, the target is restrained,
and the bulette can’t bite another target.

___Swallow.___ The bulette makes one bite attack against a Medium or smaller
target it is grappling. If the attack hits, the target is also swallowed, and the
grapple ends. While swallowed, the target is blinded and restrained, it has
total cover against attacks and other effects outside the bulette, and it takes
21 (6d6) fire damage at the start of each of the bulette’s turns. A bulette
can have three Medium or smaller creatures swallowed at the same time.

___Death From Below (Recharge 5–6).___ If the bulette burrows at least 20
feet as part of its movement, it can then use this action to surface from
underground in a space that contains one or more creatures. Each of
those creatures must succeed on a DC 16 Strength or Dexterity saving
throw (target’s choice) or be knocked prone and splashed with globules
of molten rock, taking 18 (2d12 + 5) bludgeoning damage plus 14 (4d6)
fire damage. On a successful save, the creature takes only half damage,
isn’t knocked prone, and is pushed 5 feet out of the bulette’s space into
an unoccupied space of the creature’s choice. If no unoccupied space is
within range, the creature falls prone in the bulette’s space.
