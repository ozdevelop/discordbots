"Kobold Chieftain";;;_size_: Small humanoid
_alignment_: lawful evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Draconic"
_skills_: "Intimidation +6, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +5, Cha +4"
_condition_immunities_: "charmed, frightened"
_speed_: "30 ft."
_hit points_: "82 (15d6 + 30)"
_armor class_: "17 (studded leather and shield)"
_stats_: | 10 (+0) | 17 (+3) | 14 (+2) | 11 (+0) | 13 (+1) | 14 (+2) |

___Pack Tactics.___ The kobold chieftain has advantage on an attack roll against a target if at least one of the chieftain's allies is within 5 feet of the target and the ally isn't incapacitated.

___Sunlight Sensitivity.___ While in sunlight, the kobold chieftain has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The kobold makes 2 attacks.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 10 (3d6) poison damage and the target must make a successful DC 12 Constitution saving throw or be poisoned for 1 minute. A poisoned creature repeats the saving throw at the end of each of its turns, ending the effect on a success.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 10 (3d6) poison damage and the target must make a successful DC 12 Constitution saving throw or be poisoned for 1 minute. A poisoned creature repeats the saving throw at the end of each of its turns, ending the effect on a success.

___Inspiring Presence (Recharge after Short or Long Rest).___ The chieftain chooses up to six allied kobolds it can see within 30 feet. For the next minute, the kobolds gain immunity to the charmed and frightened conditions, and add the chieftain's Charisma bonus to attack rolls.

**Reactions**

___Springspike Shield (5/rest).___ When the kobold chieftain is hit by a melee attack within 5 feet, the kobold chieftain can fire one of its shield spikes at the attacker. The attacker takes 3 (1d6) piercing damage plus 3 (1d6) poison damage.

