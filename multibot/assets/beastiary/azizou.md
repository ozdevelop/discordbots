"Azizou (Pain Demon)";;;_size_: Small fiend (demon)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Abyssal"
_skills_: "Deception +4, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 50 ft."
_hit points_: "17 (5d6)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 15 (+2) | 11 (+0) | 8 (-1) | 10 (+0) | 10 (+0) |

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Innate Spellcasting.___ The demon’s spellcasting ability is Charisma
(spell save DC 10, +2 to hit with spell attacks). The demon can innately
cast the following spells at will, requiring no material components: _detect
evil and good, detect thoughts, invisibility._

**Actions**

___Multiattack.___ The demon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4
(1d4 + 2) slashing damage.
