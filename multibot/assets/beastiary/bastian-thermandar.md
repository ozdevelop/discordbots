"Bastian Thermandar";;;_size_: Medium humanoid (fire genasi)
_alignment_: neutral evil
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Ignan"
_skills_: "Arcana +3, Deception +7"
_senses_: "darkvision 60 ft."
_damage_resistances_: "fire"
_speed_: "30 ft."
_hit points_: "78 (12d8+24)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 12 (+1) | 14 (+2) | 15 (+2) | 11 (0) | 9 (-1) | 18 (+4) |

___Innate Spellcasting.___ Bastian's innate spellcasting ability is Constitution (spell save DC 13, +5 to hit with spell attacks). He can innately cast the following spells:

* At will: _produce flame_

* 1/day: _burning hands_

___Spellcasting.___ Bastian is a 9th-level spellcaster. His spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). Bastian knows the following sorcerer spells:

* Cantrips (at will): _fire bolt, mage hand, message, prestidigitation, shocking grasp_

* 1st level (4 slots): _mage armor, magic missile, shield_

* 2nd level (3 slots): _misty step, scorching ray_

* 3rd level (3 slots): _counterspell, fireball_

* 4th level (3 slots): _dimension door, wall of fire_

* 5th level (1 slot): _hold monster_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
