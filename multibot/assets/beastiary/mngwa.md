"Mngwa";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Sylvan, can speak with felines"
_skills_: "Perception +5, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "40 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "16"
_stats_: | 19 (+4) | 17 (+3) | 15 (+2) | 11 (+0) | 17 (+3) | 17 (+3) |

___Keen Smell.___ The mngwa has advantage on Wisdom (Perception) checks that rely on smell.

___Pack Tactics.___ The mngwa has advantage on attack rolls against a creature if at least one of the mngwa's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Running Leap.___ With a 10-foot running start, the mngwa can long jump up to 25 feet.

___Feline Empathy.___ The mngwa has advantage on Wisdom (Animal Handling) checks to deal with felines.

___Ethereal Coat.___ The armor class of the mngwa includes its Charisma modifier. All attack rolls against the mngwa have disadvantage. If the mngwa is adjacent to an area of smoke or mist, it can take a Hide action even if under direct observation.

**Actions**

___Multiattack.___ The mngwa makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

