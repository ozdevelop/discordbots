"Armor Plated Beaver";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Survival +2"
_senses_: "passive Perception 10"
_speed_: "20 ft., swim 30 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "17 (natural armor)"
_stats_: | 14 (+2) | 11 (+0) | 13 (+1) | 5 (-3) | 11 (+0) | 4 (-3) |

___Hold Breath.___ The armor-plated beaver can hold its breath for up to 20
minutes.

___Keen Smell.___ The armor-plated beaver has advantage on Wisdom
(Perception) checks based on scent.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8
+ 2) piercing damage.
