"Iron Cobra";;;_page_number_: 125
_size_: Medium construct
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "understands one language of its creator but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_skills_: "Stealth +7"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_speed_: "30 ft."
_hit points_: "45  (7d8 + 14)"
_armor class_: "13"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 3 (-3) | 10 (0) | 1 (-4) |

___Magic Resistance.___ The iron cobra has advantage on saving throws against spells and other magical effects.

**Actions**

___Bite___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage. If the target is a creature, it must succeed on a DC 13 Constitution saving throw or suffer one random poison effect:

1. _Poison Damage:_ The target takes 13 (3d8) poison damage.

2. _Confusion:_ On its next turn, the target must use its action to make one weapon attack against a random creature it can see within 30 feet of it, using whatever weapon it has in hand and moving beforehand if necessary to get in range. If it's holding no weapon, it makes an unarmed strike. If no creature is visible within 30 feet, it takes the Dash action, moving toward the nearest creature.

3. _Paralysis:_ The target is paralyzed until the end of its next turn.
