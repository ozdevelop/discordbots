"Thursir Giant";;;_size_: Large giant
_alignment_: neutral evil (50 percent) lawful evil (50 percent)
_challenge_: "3 (700 XP)"
_languages_: "Common, Dwarven, Giant"
_skills_: "Athletics +6, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Con +6"
_speed_: "40 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "13 (chain shirt)"
_stats_: | 19 (+4) | 10 (+0) | 18 (+4) | 13 (+1) | 15 (+2) | 11 (+0) |

___Cast Iron Stomach.___ The giant can consume half of its weight in food without ill effect, and it has advantage against anything that would give it the poisoned condition. Poisonous and spoiled foodstuffs are common in a thursir lair.

**Actions**

___Multiattack.___ The giant makes two warhammer attacks.

___Warhammer.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +6 to hit, range 40/160 ft., one target. Hit: 15 (2d10 + 4) bludgeoning damage.

___Runic Blood (3/day).___ Thursir giants can inscribe the thurs rune on a weapon. A creature hit by the weapon takes an additional 1d8 lightning damage, and the target can't take reactions until the start of its next turn. The thurs rune lasts for one hour, or for three hits, whichever comes first.

