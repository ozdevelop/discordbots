"Small Fire Elemental";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Ignan"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "poison, fire"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "40 ft."
_hit points_: "19 (3d8 + 6)"
_armor class_: "13 (natural armor)"
_stats_: | 8 (-1) | 14 (+2) | 14 (+2) | 5 (-3) | 12 (+1) | 6 (-2) |

___Fire Form.___ The elemental can move through a space
as narrow as 1 inch wide without squeezing. A
creature that touches the elemental or hits it with a
melee attack while within 5 feet of it takes 1 fire
damage.

___Illumination.___ The elemental sheds bright light in a
10-foot radius and dim light in an additional 10
feet.

___Water Susceptibility.___ For every 5 feet the elemental
moves in water, or for every gallon of water
splashed on it, it takes 1 cold damage.

**Actions**

___Touch.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 5 (1d6 + 2) fire damage. If the
target is a creature or a flammable object, it ignites.
Until a creature takes an action to douse the fire,
the target takes 2 (1d4) fire damage at the start of
each of its turns.

___Blaze.___ Ranged Spell Attack: +3 to hit, range 30 ft.,
one target. Hit: 5 (1d10) fire damage.
