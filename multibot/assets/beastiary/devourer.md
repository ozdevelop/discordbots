"Devourer";;;_size_: Large fiend
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "178 (17d10+85)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 20 (+5) | 12 (+1) | 20 (+5) | 13 (+1) | 10 (0) | 16 (+3) |

**Actions**

___Multiattack.___ The devourer makes two claw attacks and can use either Imprison Soul or Soul Rend.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 12 (2d6+5) slashing damage plus 21 (6d6) necrotic damage.

___Imprison Soul.___ The devourer chooses a living humanoid with 0 hit points that it can see within 30 feet of it. That creature is teleported inside the devourer's ribcage and imprisoned there. A creature imprisoned in this manner has disadvantage on death saving throws. If it dies while imprisoned, the devourer regains 25 hit points, immediately recharges Soul Rend, and gains an additional action on its next turn. Additionally, at the start of its next turn, the devourer regurgitates the slain creature as a bonus action, and the creature becomes an undead. If the victim had 2 or fewer Hit Dice, it becomes a zombie. if it had 3 to 5 Hit Dice, it becomes a ghoul. Otherwise, it becomes a wight. A devourer can imprison only one creature at a time.

___Soul Rend (Recharge 6).___ The devourer creates a vortex of life-draining energy in a 20-foot radius centered on itself. Each humanoid in that area must make a DC 18 Constitution saving throw, taking 44 (8d10) necrotic damage on a failed save, or half as much damage on a successful one. Increase the damage by 10 for each living humanoid with 0 hit points in that area.