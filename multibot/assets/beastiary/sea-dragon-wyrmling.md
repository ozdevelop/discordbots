"Sea Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Draconic, Primordial"
_skills_: "Perception +4, Stealth +2"
_senses_: "blindsight 10 ft. darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +2, Con +4, Wis +2, Cha +4"
_damage_immunities_: "cold"
_speed_: "30 ft., fly 60 ft., swim 40 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "17 (natural armor)"
_stats_: | 17 (+3) | 10 (+0) | 15 (+2) | 13 (+1) | 11 (+0) | 15 (+2) |

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage plus 3 (1d6) cold damage.

___Tidal Breath (Recharge 5-6).___ The dragon exhales a crushing wave of frigid seawater in a 15-foot cone. Each creature in that area must make a DC 12 Dexterity saving throw. On a failure, the target takes 11 (2d10) bludgeoning damage and 11 (2d10) cold damage, and is pushed 15 feet away from the dragon and knocked prone. On a successful save the creature takes half as much damage and isn't pushed or knocked prone.

