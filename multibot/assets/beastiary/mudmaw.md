"Mudmaw";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_skills_: "Stealth +5"
_senses_: "passive Perception 10"
_speed_: "30 ft., swim 30 ft."
_hit points_: "133 (14d12 + 42)"
_armor class_: "15 (natural armor)"
_stats_: | 21 (+5) | 9 (-1) | 17 (+3) | 2 (-4) | 10 (+0) | 7 (-2) |

___False Appearance.___ While the mudmaw remains
motionless, it is indistinguishable from a floating log.

___Hold Breath.___ The mudmaw can hold its breath for 15 minutes.

___Soften Earth.___ While on land, the ground in a 15 feet
radius around the mudmaw is difficult terrain.

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one creature. Hit: 21 (3d10 + 5) piercing damage.

___Tentacles.___ Melee Weapon Attack: +8 to hit, reach
20 ft., one creature. Hit: The target is grappled
(escape DC 16). Until this grapple ends, the target is
restrained and has disadvantage on Strength checks
and Strength saving throws, and the mudmaw can’t
use its tentacles on another target.
