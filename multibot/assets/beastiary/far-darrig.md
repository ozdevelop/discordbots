"Far Darrig";;;_size_: Small fey
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Nature +4, Animal Handling +6, Medicine +6, Perception +6, Survival +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +5, Con +7, Cha +7"
_speed_: "20 ft."
_hit points_: "104 (16d6 + 48)"
_armor class_: "14 (hide armor)"
_stats_: | 15 (+2) | 16 (+3) | 17 (+3) | 11 (+0) | 15 (+2) | 17 (+3) |

___Innate Spellcasting.___ The far darrig's innate spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _calm emotions, charm animal _(as _charm person_ but affects beasts only)_, cure wounds, detect poison and disease, water breathing, water walk, magic weapon _(antler glaive only)_, speak with animals_

* 3/day each: _barkskin, conjure woodland beings, hold animal _(as _hold person_ but affects beasts only)_, jump, longstrider_

* 1/day each: _commune with nature, freedom of movement, nondetection, tree stride_

**Actions**

___Multiattack.___ The far darrig makes four antler glaive attacks.

___Antler Glaive.___ Melee Weapon Attack: +4 to hit, reach 5 ft. or 10 ft., one target. Hit: 7 (1d10 + 2) slashing damage and the target must make a successful DC 13 Strength saving throw or either be disarmed or fall prone; the attacking far darrig chooses which effect occurs.

**Bonus** Actions

___Enchanted Glaive Maneuvers.___ A far darrig can magically extend or shrink its antler glaive to give it either a 10-foot or 5-foot reach.
