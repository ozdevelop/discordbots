"Frilled Deathspitter Lizard";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "passive Perception 13"
_speed_: "40 ft."
_hit points_: "18 (4d6 + 4)"
_armor class_: "13"
_stats_: | 12 (+1) | 16 (+3) | 13 (+1) | 4 (-3) | 12 (+1) | 6 (-2) |

**Actions**

___Multiattack.___ The deathspitter makes three attacks: one with its bite
and two with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6
(1d6 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6
(1d6 + 3) slashing damage.

___Spit Poison.___ Ranged Weapon Attack: +5 to hit, range 15/30 ft., one
target. Hit: The target must make a DC 13 Constitution saving throw,
taking 18 (4d8) poison damage on a failed save, or half as much
damage on a successful one. In addition, a creature that fails its
saving throw is blinded until the end of the deathspitter’s next turn.
