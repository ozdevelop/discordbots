"Xvart";;;_size_: Small humanoid (xvart)
_alignment_: chaotic evil
_challenge_: "1/8 (25 XP)"
_languages_: "Abyssal"
_senses_: "darkvision 30 ft."
_skills_: "Stealth +4"
_speed_: "30 ft."
_hit points_: "7 (2d6)"
_armor class_: "13 (leather armor)"
_stats_: | 8 (-1) | 14 (+2) | 10 (0) | 8 (-1) | 7 (-2) | 7 (-2) |

___Low Cunning.___ The xvart can take the Disengage action as a bonus action on each of its turns.

___Overbearing Pack.___ The xvart has advantage on Strength (Athletics) checks to shove a creature if at least one of the xvart's allies is within 5 feet of the target and the ally isn't incapacitated.

___Raxivort's Tongue.___ The xvart can communicate with ordinary bats and rats, as well as giant bats and giant rats.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) piercing damage.

___Sling.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 4 (1d4+2) bludgeoning damage.