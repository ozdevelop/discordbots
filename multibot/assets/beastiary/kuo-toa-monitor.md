"Kuo-Toa Monitor";;;_size_: Medium humanoid (kuo-toa)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +6, Religion +4"
_speed_: "33 ft., swim 30 ft."
_hit points_: "65 (10d8+20)"
_armor class_: "13 (natural armor, unarmored defense)"
_stats_: | 14 (+2) | 10 (0) | 14 (+2) | 12 (+1) | 14 (+2) | 11 (0) |

___Amphibious.___ The kuo-toa can breathe air and water.

___Otherwordly Perception.___ The kuo-toa can sense the presence of any creature within 30 feet of it that is invisible or on the Ethereal Plane. It can pinpoint such a creature that is moving.

___Slippery.___ The kuo-toa has advantage on ability checks and saving throws made to escape a grapple.

___Sunlight Sensitivity.___ While in sunlight, the kuo-toa has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Unarmored Defense.___ The kuo-toa adds its Wisdom modifier to its armor class.

**Actions**

___Multiattack.___ The kuo-toa makes one bite attack and two unarmed strikes.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Unarmed Strike.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage plus 3 (1d6) lightning damage, and the target can't take reactions until the end of the kuo-toa's next turn.