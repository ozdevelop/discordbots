"Princip";;;_size_: Medium celestial
_alignment_: any good alignment
_challenge_: "5 (1,800 XP)"
_languages_: "all, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 19"
_saving_throws_: "Wis +6, Cha +6"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: " charmed, exhaustion, frightened, poisoned"
_speed_: "40 ft."
_hit points_: "82 (11d8 + 33)"
_armor class_: "15 (natural armor)"
_stats_: | 16 (+3) | 15 (+2) | 16 (+3) | 16 (+3) | 16 (+3) | 16 (+3) |

___Angelic Weapons.___ The Princip’s weapon attacks are
magical. When the Princip hits with any weapon,
the weapon deals an extra 9 (2d8) radiant damage
(included in the attack).

___Magic Resistance.___ The Princip has advantage on
saving throws against spells and other magical
effects from evil characters and sources.

___Aura of Protection Against Evil.___ Evil creatures have
disadvantage on attack rolls against all allies within
5 feet of the Princip. Allies in this area can’t be
charmed, frightened, or possessed by evil creatures.
If an ally is already charmed, frightened, or possessed
by evil magic, the ally has advantage on any new
saving throw against the relevant effect.

___Innate Spellcasting.___ The Princip’s spellcasting ability
is Wisdom (spell save DC 14). It can innately cast the
following spells, requiring only verbal components:

* At will: _aid, enhance ability, lesser restoration, shield of faith_

* 3/day each: _haste, remove curse, stoneskin_

**Actions**

___Multiattack.___ The Princip makes three attacks with the
Scepter of Celestial Will.

___Scepter of Celestial Will.___ Melee Weapon Attack: +6 to
hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) bludgeoning damage and 9 (2d8) radiant damage.

___Crown of Glory.___ The Princip selects an ally. A golden
crown appears over the ally’s head. While the crown
remains, the ally cannot be compelled to act in a
manner contrary to their will and may choose one
ability score to raise to 24. Each time the ally takes
damage while the crown is on them, they must make
a Constitution saving throw or lose the crown. The
DC equals 10 or half the damage taken, whichever
number is higher.
