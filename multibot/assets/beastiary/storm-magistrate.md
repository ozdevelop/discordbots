"Storm Magistrate";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "9 (5,000 XP)"
_languages_: "Auran, Common, Ignan"
_senses_: "darkvision 60 ft., passive Perception 19"
_saving_throws_: "Int +6, Wis +9, Cha +7"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "157 (15d10 + 75)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 21 (+5) | 14 (+2) | 20 (+5) | 16 (+3) |

___Innate Spellcasting.___ The Magistrate is a 13th-level
spellcaster. Its spellcasting ability is Wisdom
(spell save DC 17, +9 to hit with spell attacks). It
can innately cast the following spells, requiring
no components:

* At will: _enhance ability_

___Spellcasting.___ The Magistrate has the following
cleric spells prepared:

* Cantrips (at will): _light, guidance, sacred flame, spare the dying_

* 1st level (4 slots): _bane, bless, healing word, shield of faith_

* 2nd level (3 slots): _aid, lesser restoration, magic weapon, prayer of healing, hold person_

* 3rd level (3 slots): _beacon of hope, magic circle, dispel magic, revivify, spirit guardians, mass healing word_

* 4th level (3 slots): _banishment, freedom of movement, guardian of faith, stoneskin_

* 5th level (1 slot): _flame strike, mass cure wounds, hold monster_

**Actions**

___Multiattack.___ The Magistrate makes two attacks
with its morningstar.

___Morningstar.___ Melee Weapon Attack: +7 to hit, reach
10 ft., one target. Hit: 12 (2d8 + 3) piercing damage.

___Storm Strike (Recharge 6).___ Lightning arcs from the
Magistrate toward up to three targets it can see
within 120 feet. Three bolts then leap from those
targets to as many as three other targets, each of
which must be within 10 feet of the first target. A
creature can be targeted by only one bolt.
A target must make a DC 17 Dexterity saving
throw. A creature takes 14 (4d6) lightning damage
and 14 (4d6) radiant damage on a failed save, or
half as much damage on a successful one.
