"Decapus";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Deep Speech, Sylvan"
_skills_: "Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "10 ft., climb 30 ft."
_hit points_: "32 (5d8 + 10)"
_armor class_: "13 (natural armor)"
_stats_: | 14 (+2) | 13 (+1) | 15 (+2) | 10 (+0) | 10 (+0) | 8 (-1) |

___Brachiation.___ A decapus can move through trees at its base climb speed
by using its tentacles to swing from tree to tree. Trees used by the decapus
in this manner can be no further than 10 feet apart.

___Mimicry.___ The decapus can mimic any sounds it has heard, including
voices. A creature that hears the sounds can tell they are imitations with a
successful DC 13 Wisdom (Insight) check.

**Actions**

___Multiattack.___ The decapus makes up to four tentacle attacks.

___Tentacle.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage. If the target is Medium or smaller, it
is grappled (escape DC 12) and restrained until the grapple ends. The
decapus has ten tentacles but can only grapple two targets at any given
time.
