"Nkosi Pridelord";;;_size_: Medium humanoid
_alignment_: nkosi), lawful neutral
_challenge_: "4 (1100 XP)"
_languages_: "Common"
_skills_: "Survival +2"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "93 (17d8 + 17)"
_armor class_: "16 (studded leather)"
_stats_: | 18 (+4) | 18 (+4) | 12 (+1) | 10 (+0) | 10 (+0) | 14 (+2) |

___Shapechanger.___ The nkosi can use its action to polymorph into a Medium lion or back into its true form. While in lion form, the nkosi can't speak and its walking speed is 50 feet. Other than its speed, its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Keen Smell.___ The nkosi has advantage on Wisdom (Perception) checks that rely on smell.

___Brute.___ A melee weapon deals one extra die of damage when the pridelord hits with it (included in the attack).

___Hunter's Maw.___ If the nkosi moves at least 20 feet straight toward a creature and then hits it with a scimitar attack on the same turn, that target must succeed on a DC 15 Strength saving throw or be knocked prone. If the target is prone, the nkosi can make one bite attack against it as a bonus action.

**Actions**

___Multiattack.___ The pridelord makes two attacks with its scimitar or with its mambele throwing knife.

___Scimitar (Nkosi Form Only).___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Mambele Throwing Knife (Nkosi Form Only).___ Ranged Weapon Attack: +6 to hit, range 20/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage.

___Pridelord's Roar (Recharges after a Short or Long Rest).___ Each nkosi of the pridelord's choice that is within 30 feet of it and can hear it can immediately use its reaction to make a bite attack. The pridelord can then make one bite attack as a bonus action.

