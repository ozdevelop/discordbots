"Chort Devil";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "12 (8400 XP)"
_languages_: "Celestial, Common, Draconic, Infernal, Primordial; telepathy (120 ft.)"
_skills_: "Athletics +11, Deception +9, Insight +9, Perception +9"
_senses_: "darkvision 120 ft., passive Perception 19"
_saving_throws_: "Str +11, Dex +9, Con +12, Int +8, Cha +9"
_damage_immunities_: "cold, fire, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "187 (15d8 + 120)"
_armor class_: "18 (natural armor)"
_stats_: | 24 (+7) | 20 (+5) | 26 (+8) | 18 (+4) | 20 (+5) | 20 (+5) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The chort devil's spellcasting ability is Charisma (spell save DC 17, +9 to hit with spell attacks). The chort devil can innately cast the following spells, requiring no material components:

At will: blur, magic circle, teleportation

3/day: scorching ray (5 rays)

1/day each: dispel magic, dominate person, flame strike, haste

**Actions**

___Multiattack.___ The chort devil makes three melee attacks with its flaming ranseur, or three melee attacks with its claws.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 12 (2d4 + 7) slashing damage plus 2 (1d4) Charisma damage.

___Flaming Ranseur.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 12 (1d10 + 7) piercing damage plus 10 (3d6) fire damage.

___Devilish Weapons.___ Any weapons wielded by a chort devil do 10 (3d6) fire damage in addition to their normal weapon damage.

