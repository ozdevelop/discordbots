"Lemurfolk";;;_size_: Small humanoid
_alignment_: neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Lemurfolk"
_skills_: "Acrobatics +4, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "20 ft., climb 10 ft., fly 40 ft."
_hit points_: "14 (4d6)"
_armor class_: "13"
_stats_: | 10 (+0) | 15 (+2) | 11 (+0) | 12 (+1) | 10 (+0) | 8 (-1) |

___Silent Glide.___ The lemurfolk can glide for 1 minute, making almost no sound. It gains a fly speed of 40 feet, and it must move at least 20 feet on its turn to keep flying. A gliding lemurfolk has advantage on Dexterity (Stealth) checks.

___Sneak Attack (1/Turn).___ The lemurfolk deals an extra 3 (1d6) damage when it hits with a weapon attack and it has advantage, or when the target is within 5 feet of an ally of the lemurfolk that isn't incapacitated and the lemurfolk doesn't have disadvantage on the attack roll.

**Actions**

___Kukri Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft., 20/60 range, one target. Hit: 4 (1d4 + 2) piercing damage.

___Blowgun.___ Ranged Weapon Attack: +4 to hit, range 25/100 ft., one creature. Hit: 4 (1d4 + 2) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned and unconscious for 1d4 hours. Another creature can use an action to shake the target awake and end its unconsciousness but not the poisoning.

