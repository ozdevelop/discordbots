"Shadow Fey";;;_size_: Medium humanoid
_alignment_: lawful evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Elvish, Umbral"
_skills_: "Arcana +2, Perception +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "30 ft."
_hit points_: "31 (7d8)"
_armor class_: "15 (chain shirt)"
_stats_: | 10 (+0) | 14 (+2) | 10 (+0) | 11 (+0) | 11 (+0) | 13 (+1) |

___Fey Ancestry.___ The shadow fey has advantage on saving throws against being charmed, and magic can't put it to sleep.

___Innate Spellcasting.___ The shadow fey's innate spellcasting ability is Charisma. It can cast the following spells innately, requiring no material components.

* 1/day: _misty step_ (when in shadows, dim light, or darkness only)

___Sunlight Sensitivity.___ While in sunlight, the shadow fey has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

___Traveler in Darkness.___ The shadow fey has advantage on Intelligence (Arcana) checks made to know about shadow roads and shadow magic spells or items.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

