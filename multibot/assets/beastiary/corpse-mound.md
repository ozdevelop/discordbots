"Corpse Mound";;;_size_: Huge undead
_alignment_: neutral evil
_challenge_: "11 (7200 XP)"
_languages_: "Understands Common but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Con +9, Int +3, Wis +4"
_damage_immunities_: "poison"
_damage_resistances_: "necrotic"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "207 (18d12 + 90)"
_armor class_: "16 (natural armor)"
_stats_: | 24 (+7) | 11 (+0) | 21 (+5) | 8 (-1) | 10 (+0) | 8 (-1) |

___Absorb the Dead.___ Whenever a Small or larger non.undead creature dies within 20 feet of the corpse mound, that creature's remains join its form and the corpse mound regains 10 hit points.

___Noxious Aura.___ Creatures that are within 20 feet of the corpse mound at the end of its turn must succeed on a DC 17 Constitution saving throw or become poisoned until the end of their next turn. On a successful saving throw, the creature is immune to the Noxious Aura for 24 hours.

___Zombie Drop.___ At the start of the corpse mound's turn during combat, one corpse falls from the mound onto the ground and immediately rises as a zombie under its control. Up to 10 such zombies can be active at one time. Zombies take their turns immediately after the corpse mound's turn.

**Actions**

___Multiattack.___ The corpse mound makes two weapon attacks or uses envelop once.

___Slam.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 18 (2d10 + 7) bludgeoning damage plus 10 (3d6) necrotic damage and the target is grappled (escape DC 17). Until this grapple ends, the target is restrained.

___Bone Shard.___ Ranged Weapon Attack: +11 to hit, range 30/120 ft., one target. Hit: 14 (2d6 + 7) piercing damage and 10 (3d6) necrotic damage. When hit, the target must make a successful DC 17 Strength check or be knocked prone, pinned to the ground by the shard, and restrained. To end this restraint, the target or a creature adjacent to it must use an action to make a successful DC 17 Strength (Athletics) check to remove the shard.

___Envelop.___ The corpse mound makes a slam attack against a restrained creature. If the attack hits, the target takes damage as normal, is pulled 5 feet into the corpse mound's space, and is enveloped, which ends any grappled or prone condition. While enveloped, the creature is blinded and restrained, it has total cover against attacks and other effects outside the corpse mound, and it takes 21 (6d6) necrotic damage at the start of each of the corpse mound's turns. An enveloped creature can escape by using its action to make a successful DC 17 Strength saving throw. If the corpse mound takes 30 or more damage on a single turn from the enveloped creature, it must succeed on a DC 17 Constitution saving throw at the end of that turn or expel the creature, which falls prone in a space within 10 feet of the corpse mound. If the corpse mound dies, an enveloped creature is no longer restrained by it and can escape by using 10 feet of movement, exiting prone. A corpse mound can envelop up to 4 creatures at once.

