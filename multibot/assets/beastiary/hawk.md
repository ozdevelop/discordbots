"Hawk";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_skills_: "Perception +4"
_speed_: "10 ft., fly 60 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "13"
_stats_: | 5 (-3) | 16 (+3) | 8 (-1) | 2 (-4) | 14 (+2) | 6 (-2) |

___Keen Sight.___ The hawk has advantage on Wisdom (Perception) checks that rely on sight.

**Actions**

___Talons.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 slashing damage.