"Dust Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Auran, Terran"
_senses_: "darkvision 60 ft."
_skills_: "Perception +2, Stealth +4"
_damage_immunities_: "poison"
_speed_: "30 ft., fly 30 ft."
_hit points_: "17 (5d6)"
_armor class_: "12"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "poisoned"
_stats_: | 5 (-3) | 14 (+2) | 10 (0) | 9 (-1) | 11 (0) | 10 (0) |

___Death Burst.___ When the mephit dies, it explodes in a burst of dust. Each creature within 5 ft. of it must then succeed on a DC 10 Constitution saving throw or be blinded for 1 minute. A blinded creature can repeat the saving throw on each of its turns, ending the effect on itself on a success.

___Innate Spellcasting (1/Day).___ The mephit can innately cast sleep, requiring no material components. Its innate spellcasting ability is Charisma.

**Actions**

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 4 (1d4 + 2) slashing damage.

___Blinding Breath (Recharge 6).___ The mephit exhales a 15-foot cone of blinding dust. Each creature in that area must succeed on a DC 10 Dexterity saving throw or be blinded for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Variant: Summon Mephits (1/Day).___ The mephit has a 25 percent chance of summoning 1d4 mephits of its kind. A summoned mephit appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other mephits. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.