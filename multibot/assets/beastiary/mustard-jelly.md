"Mustard Jelly";;;_size_: Large ooze
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 16"
_skills_: "Perception +6, Stealth +6"
_damage_immunities_: "force, lightning, poison"
_damage_resistances_: "cold"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "136 (13d10 + 65)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 10 (+0) | 21 (+5) | 10 (+0) | 10 (+0) | 10 (+0) |

___Amorphous.___ The jelly can move through a space as narrow as 1 inch
wide without squeezing.

___Energy Absorption.___ A mustard jelly is immune to force and lightning
damage. If the jelly would have taken force or lightning damage, it is
instead healed for the same amount it would have taken in damage.

___Magic Weapons.___ The jelly’s attacks are magical.

___Spider Climb.___ The jelly can climb difficult surfaces, including upside
down on ceilings, without needing to make an ability check.

___Poison Aura.___ At the start of each of the jelly’s turns, each creature
within 10 feet of it takes 10 (3d6) poison damage. A creature that touches
the jelly or hits it with a melee attack while within 5 feet of it takes 10
(3d6) poison damage.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit:
12 (3d6 + 2) bludgeoning damage and 10 (3d6) acid damage.
