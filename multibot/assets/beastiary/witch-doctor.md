"Witch Doctor";;;_size_: Medium humanoid
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Primordial"
_skills_: "Animal Handling +6, Insight +6, Sleight of Hand +5"
_senses_: "passive Perception 13"
_saving_throws_: "Con +5, Wis +6"
_speed_: "30 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "13 (leather armor)"
_stats_: | 8 (-1) | 15 (+2) | 14 (+2) | 10 (+0) | 17 (+3) | 12 (+1) |

___Innate Spellcasting.___ The witch doctor’s innate
spellcasting ability is Wisdom (spell save DC 14).
The witch doctor can innately cast the following
spells, requiring no material components:

* 3/day each: _darkness, hex, hellish rebuke_

* 1/day each: _counterspell, fear, magic circle_

___Totemic Ritualist.___ Whenever the witch doctor uses
its action to cast a spell, it may use its bonus action
to conjure a totem if able.

**Actions**

___Multiattack.___ The witch doctor conjures a totem is
able. It then makes two attacks with its claws or
two with its blowgun.

___Dagger.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 4 (1d4 + 2) piercing damage.

___Blowgun.___ Ranged Weapon Attack: +5 to hit, range
20/60 ft., one target. Hit: 5 (1d6 + 2) piercing
damage plus 14 (4d6) poison damage.

___Conjure Totem (Recharge 4-6).___ The Witch Doctor
creates a powerful, magical totem in an unoccupied
space within 30 feet. This totem is a tiny object
with 15 hit points and AC 12. The totem provides
bonuses to the witch doctor and its allies until
destroyed. The witch doctor may only have one
totem of each type active at a time. When the witch
doctor summons a totem it chooses from the
following options:
* ___Invigorating Totem.___ On initiative count 20
(losing ties), evil creatures within 30 feet of the
totem regain 5 (1d10) hit points.
* ___Empowering Totem.___ Evil creatures within 30
feet of the totem deal an additional 5 (1d10)
force damage on all weapon attacks.
* ___Impeding Totem.___ The area within 30 feet of the
totem is considered difficult terrain for non-evil
creatures.
