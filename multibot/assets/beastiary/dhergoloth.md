"Dhergoloth";;;_page_number_: 248
_size_: Medium fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "7 (2900 XP)"
_languages_: "Abyssal, Infernal, telepathy 60 ft."
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 10"
_damage_immunities_: "acid, poison"
_saving_throws_: "Str +6"
_speed_: "30 ft."
_hit points_: "119  (14d8 +56)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 17 (+3) | 10 (0) | 19 (+4) | 7 (-1) | 10 (0) | 9 (0) |

___Innate Spellcasting.___ The dhergoloth's innate spellcasting ability is Charisma (spell save DC 10). It can innately cast the following spells, requiring no material components:

* At will: _darkness, fear_

* 3/day: _sleep_

___Magic Resistance.___ The dhergoloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The dhergoloth's weapon attacks are magical.

**Actions**

___Multiattack___ The dhergoloth makes two claw attacks.

___Claw___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage.

___Flailing Claws (Recharge 5-6)___ The dhergoloth moves up to its walking speed in a straight line and targets each creature within 5 feet of it during its movement. Each target must succeed on a DC 14 Dexterity saving throw or take 22 (3d12 + 3) slashing damage.

___Teleport___ The dhergoloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.
