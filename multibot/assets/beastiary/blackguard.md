"Blackguard";;;_size_: Medium humanoid (any race)
_alignment_: any non-good alignment
_challenge_: "8 (3,900 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Athletics +7, Deception +5, Intimidation +5"
_saving_throws_: "Wis +5, Cha +5"
_speed_: "30 ft."
_hit points_: "153 (18d8+72)"
_armor class_: "18 (plate)"
_stats_: | 18 (+4) | 11 (0) | 18 (+4) | 11 (0) | 14 (+2) | 15 (+2) |

___Spellcasting.___ The blackguard is a 10th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It has the following paladin spells prepared:

* 1st level (4 slots): _command, protection from evil and good, thunderous smite_

* 2nd level (3 slots): _branding smite, find steed_

* 3rd level (2 slots): _blinding smite, dispel magic_

**Actions**

___Multiattack.___ The blackguard makes three attacks with its glaive or its shortbow.

___Glaive.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 9 (1d10+4) slashing damage.

___Shortbow.___ Ranged Weapon Attack: +3 to hit, range 80/320 ft., one target. Hit: 5 (1d6+2) piercing damage.

___Dreadful Aspect (Recharges after a Short or Long Rest).___ The blackguard exudes magical menace. Each enemy within 30 feet of the blackguard must succeed on a DC 13 Wisdom saving throw or be frightened for 1 minute. If a frightened target ends its turn more than 30 feet away from the blackguard, the target can repeat the saving throw, ending the effect on itself on a success.
