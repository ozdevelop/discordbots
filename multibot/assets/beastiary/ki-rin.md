"Ki-rin";;;_size_: Huge celestial
_alignment_: lawful good
_challenge_: "12 (8,400 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Insight +9, Perception +9, Religion +8"
_damage_immunities_: "poison"
_speed_: "60 ft., fly 120 ft. (hover)"
_hit points_: "152 (16d12+48)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 21 (+5) | 16 (+3) | 16 (+3) | 19 (+4) | 20 (+5) | 20 (+5) |

___Innate Spellcasting.___ The ki-rin's innate spellcasting ability is Charisma (spell save DC 17). The ki-rin can innately cast the following spells, requiring no material components:

* At will: _gaseous form, major image _(6th-level version)_, wind walk_

* 1/day: _create food and water_

___Legendary Resistance (3/Day).___ If the ki-rin fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The ki-rin has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The ki-rin's weapon attacks are magical.

___Spellcasting.___ The ki-rin is a 18th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 17, +9 to hit with spell attacks). It has the following cleric spells prepared:

* Cantrips (at will): _light, mending, sacred flame, spare the dying, thaumaturgy_

* 1st level (4 slots): _command, cure wounds, detect evil and good, protection from evil and good, sanctuary_

* 2nd level (3 slots): _calm emotions, lesser restoration, silence_

* 3rd level (3 slots): _dispel magic, remove curse, sending_

* 4th level (3 slots): _banishment, freedom of movement, guardian of faith_

* 5th level (3 slots): _greater restoration, mass cure wounds, scrying_

* 6th level (1 slot): _heroes' feast, true seeing_

* 7th level (1 slot): _etherealness, plane shift_

* 8th level (1 slot): _control weather_

* 9th level (1 slot): _true resurrection_

**Actions**

___Multiattack.___ The ki-rin makes three attacks: two with its hooves and one with its horn.

___Hoof.___ Melee Weapon Attack: +9 to hit, reach 15 ft, one target. Hit: 10 (2d4+5) bludgeoning damage.

___Horn.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 14 (2d8+5) piercing damage.

**Legendary** Actions

The ki-rin can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The ki-rin regains spent legendary actions at the start of its turn.

___Detect.___ The ki-rin makes a Wisdom (Perception) check or a Wisdom (Insight) check.

___Smite.___ The ki-rin makes a hoof attack or casts sacred flame.

___Move.___ The ki-rin moves up to its half its speed without provoking opportunity attacks.
