"Yellow Musk Zombie";;;_size_: Medium undead
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 30 ft., passive Perception 8"
_condition_immunities_: "charmed, exhaustion"
_speed_: "20 ft."
_hit points_: "33 (6d8 +6)"
_armor class_: "9"
_stats_: | 13 (+1) | 9 (-1) | 12 (+1) | 1 (-5) | 6 (-2) | 3 (-4) |

___Undead fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is fire or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Slam.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (ld8 + 1) bludgeoning damage.