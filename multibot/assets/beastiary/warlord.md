"Warlord";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "12 (8,400 XP)"
_languages_: "any two languages"
_skills_: "Athletics +9, Intimidation +8, Perception +5, Persuasion +8"
_saving_throws_: "Str +9, Dex +7, Con +8"
_speed_: "30 ft."
_hit points_: "229 (27d8+108)"
_armor class_: "18 (plate)"
_stats_: | 20 (+5) | 16 (+3) | 18 (+4) | 12 (+1) | 12 (+1) | 18 (+4) |

___Indomitable (3/Day).___ The warlord can reroll a saving throw it fails. It must use the new roll.

___Survivor.___ The warlord regains 10 hit points at the start of its turn if it has at least 1 hit point but fewer hit points than half its hit point maximum.

**Actions**

___Multiattack.___ The warlord makes two weapon attacks.

___Greatsword.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6+5) slashing damage.

___Shortbow.___ Ranged Weapon Attack: +7 to hit, range 80/320 it, one target. Hit: 6 (1d6+3) piercing damage.

**Legendary** Actions

The warlord can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The warlord regains spent legendary actions at the start of its turn.

___Weapon Attack.___ The warlord makes a weapon attack.

___Command Ally.___ The warlord targets one ally it can see within 30 feet of it. if the target can see and hear the warlord, the target can make one weapon attack as a reaction and gains advantage on the attack roll.

___Frighten Foe (Costs 2 Actions).___ The warlord targets one enemy it can see within 30 feet of it. If the target can see and hear it, the target must succeed on a DC 16 Wisdom saving throw or be frightened until the end of warlord's next turn.