"Scylla";;;_size_: Huge monstrosity
_alignment_: neutral
_challenge_: "9 (5,000 XP)"
_languages_: "Aquan, Common"
_skills_: "Perception +10, Stealth +6, Survival +6"
_senses_: "darkvision 60 ft., passive Perception 20"
_damage_immunities_: "fire"
_speed_: "10 ft., swim 50 ft."
_hit points_: "94 (9d12 + 36)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 19 (+4) | 10 (+0) | 14 (+2) | 10 (+0) |

___Heat.___ A creature who touches a scylla takes 7 (2d6) fire damage.

___Multiple Heads.___ The scylla has six heads. It has advantage on saving
throws against being blinded, charmed, deafened, frightened, stunned, or
knocked unconscious.

___Water Breathing.___ The scylla can only breathe underwater.

___Water Dependency.___ A scylla can survive on land for 6 hours before
suffocating.

**Actions**

___Multiattack.___ The scylla makes three bite attacks.

___Bites.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 23
(4d8 + 5) piercing damage.

___Superheat Water (Recharge 5–6).___ The scylla uses one of the following
options.

* _Scalding Blast._ The scylla releases a line of scalding water that is 60 feet
long and 5 feet wide. Creatures in the area must make a DC 16 Dexterity
saving throw, taking 35 (10d6) fire damage on a failed saving throw, or
half as much damage on a successful saving throw.

* _Boil Water._ The water surrounding a scylla in a radius of 10 feet rises
to a boil. All creatures within the area must make a DC 16 Constitution
saving throw, taking 24 (7d6) fire damage on a failed saving throw, or
half as much damage on a successful one. The area remains at a boil for 1
minute, even if the scylla moves to another space.
