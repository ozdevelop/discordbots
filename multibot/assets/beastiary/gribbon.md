"Gribbon";;;_size_: Small monstrosity
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 10"
_skills_: "Stealth +4"
_speed_: "30 ft., fly 30 ft."
_hit points_: "13 (3d6 + 3)"
_armor class_: "12"
_stats_: | 12 (+1) | 15 (+2) | 13 (+1) | 10 (+0) | 10 (+0) | 11 (+0) |

___Pack Tactics.___ The gribbon has advantage on an attack roll against a
creature if at least one of the gribbon’s allies is within 5 feet of the creature
and the ally isn’t incapacitated.

**Actions**

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Dart.___ Ranged Weapon Attack: +4 to hit, range 26/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
