"Deathcap Myconid";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "4 (1100 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "20 ft."
_hit points_: "90 (12d8 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 10 (+0) | 16 (+3) | 10 (+0) | 11 (+0) | 9 (-1) |

___Distress Spores.___ When a deathcap myconid takes damage, all other myconids within 240 feet of it sense its pain.

___Sun Sickness.___ While in sunlight, the myconid has disadvantage on ability checks, attack rolls, and saving throws. The myconid dies if it spends more than 1 hour in direct sunlight.

**Actions**

___Multiattack.___ The myconid uses either its Deathcap Spores or its Slumber Spores, then makes a fist attack.

___Fist.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 11 (4d4 + 1) bludgeoning damage plus 10 (4d4) poison damage.

___Deathcap Spores (3/day).___ The myconid ejects spores at one creature it can see within 5 feet of it. The target must succeed on a DC 13 Constitution saving throw or be poisoned for 3 rounds. While poisoned this way, the target also takes 10 (4d4) poison damage at the start of each of its turns. The target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Slumber Spores (3/day).___ The myconid ejects spores at one creature it can see within 5 feet of it. The target must succeed on a DC 13 Constitution saving throw or be poisoned and unconscious for 1 minute. A creature wakes up if it takes damage, or if another creature uses its action to shake it awake.

