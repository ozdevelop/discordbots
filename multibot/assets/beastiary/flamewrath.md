"Flamewrath";;;_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Ignan"
_skills_: "Arcana +3, Religion +3"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "105 (14d8+42)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 10 (0) | 14 (+2) | 16 (+3) | 11 (0) | 10 (0) | 16 (+3) |

___Spellcasting.___ The flamewrath is a 7th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _control flames, fire bolt, friends, light, minor illusion_

* 1st level (4 slots): _burning hands, color spray, mage armor_

* 2nd level (3 slots): _scorching ray, suggestion_

* 3rd level (3 slots): _fireball, hypnotic pattern_

* 4th level (1 slot): _fire shield _(see Wreathed in Flame)

___Wreathed in Flame.___ For the flamewrath, the warm version of the fire shield spell has a duration of "until dispelled." The fire shield burns for 10 minutes after the flamewrath dies, consuming its body.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
