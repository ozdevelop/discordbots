"Duergar Despot";;;_page_number_: 188
_size_: Medium humanoid (dwarf)
_alignment_: lawful evil
_challenge_: "12 (8400 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 12"
_damage_immunities_: "poison"
_saving_throws_: "Con +8, Wis +6"
_speed_: "25 ft."
_hit points_: "119  (14d8 + 56)"
_armor class_: "21 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_stats_: | 20 (+5) | 5 (-2) | 19 (+4) | 15 (+2) | 14 (+2) | 13 (+1) |

___Innate Spellcasting (Psionics).___ The duergar despot's innate spellcasting ability is Intelligence (spell save DC 12). It can cast the following spells, requiring no components:

* At will: _mage hand, minor illusion_

* 1/day each: _counterspell, misty step, stinking cloud_

___Magic Resistance.___ The duergar has advantage on saving throws against spells and other magical effects.

___Psychic Engine.___ When the duergar despot suffers a critical hit or is reduced to 0 hit points, psychic energy erupts from its frame to deal 14 (4d6) psychic damage to each creature within 5 feet of it.

___Sunlight Sensitivity.___ While in sunlight, the duergar despot has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The despot makes two iron fist attacks and two stomping foot attacks. It can replace up to four of these attacks with uses of its Flame Jet.

___Iron Fist___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage. If the target is a Large or smaller creature, it must make a successful DC 17 Strength saving throw or be thrown up to 30 feet away in a straight line. The target is knocked prone and then takes 10 (3d6) bludgeoning damage.

___Stomping Foot___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) bludgeoning damage, or 18 (3d8 + 5) bludgeoning damage to a prone target.

___Flame Jet___ The duergar spews flames in a line 100 feet long and 5 feet wide. Each creature in the line must make a DC 16 Dexterity saving throw, taking 18 (4d8) fire damage on a failed save, or half as much damage on a successful one.
