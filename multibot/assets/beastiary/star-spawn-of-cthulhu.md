"Star Spawn Of Cthulhu";;;_size_: Large fiend
_alignment_: chaotic evil
_challenge_: "15 (13000 XP)"
_languages_: "Common, Infernal, Void Speech"
_skills_: "Arcana +15, Perception +14"
_senses_: "darkvision 300 ft., passive Perception 24"
_saving_throws_: "Str +12, Con +12, Int +15, Wis +9"
_damage_immunities_: "cold, fire, lightning, poison, psychic"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., swim 30 ft., fly 50 ft."
_hit points_: "187 (15d10 + 105)"
_armor class_: "17 (natural armor)"
_stats_: | 25 (+7) | 15 (+2) | 24 (+7) | 30 (+10) | 18 (+4) | 23 (+6) |

___Interdimensional Movement.___ A star spawn of Cthulhu can use misty step as a bonus action once per round.

___Psychic Tower.___ When an attack that causes psychic damage is directed against the spawn, the attack rebounds against the attacker. Resolve the attack as if the attacker were the original target and using the star spawn's ability modifiers and proficiency bonus rather than the original attacker's.

___Void Traveler.___ The star spawn of Cthulhu requires no air, warmth, ambient pressure, food, or water, enabling it to travel safely through interstellar space and similar voids.

**Actions**

___Multiattack.___ The star spawn can use disintegrating gaze if it's available, and also make one claws attack and one dimensional stomp attack.

___Crushing Claws.___ Melee Weapon Attack. +12 to hit, reach 10 ft., one target. Hit: 20 (2d12 + 7) bludgeoning damage plus 13 (3d8) necrotic damage.

___Disintegrating Gaze (Recharge 5-6).___ Ranged Spell Attack: +15 to hit, range 60 ft., one target in line of sight. Hit: 32 (5d12) necrotic damage, and the target must make a successful DC 20 Constitution saving throw or dissipate into vapor as if affected by a gaseous form spell. An affected creature repeats the saving throw at the end of each of its turns; on a success, the effect ends on that creature, but on a failure, the creature takes another 32 (5d12) necrotic damage and remains gaseous. A creature reduced to 0 hit points by this necrotic damage is permanently disintegrated and can be restored only by a wish or comparable magic that doesn't require some portion of a corpse to work.

___Dimensional Stomp.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 18 (2d20 + 7) bludgeoning damage, and the target must make a successful DC 15 Dexterity saving throw or be teleported to a new location as if affected by the dimension door spell. The destination is chosen by the star spawn, but it cannot be in the same space as another object or creature.

