"Centaur Marauder";;;_size_: Large monstrosity
_alignment_: neutral good
_challenge_: "3 (700 XP)"
_languages_: "Elvish, Sylvan"
_skills_: "Athletics +7, Perception +2"
_senses_: "passive Perception 12"
_speed_: "50 ft."
_hit points_: "68 (8d10 + 24)"
_armor class_: "16 (scale mail)"
_stats_: | 21 (+6) | 14 (+2) | 16 (+3) | 9 (-1) | 11 (+0) | 7 (-2) |

___Specialized Combatant.___ The centaur is considered
mounted for the purposes of using a lance in
combat.

___Impaling Strike.___ If the centaur moves at least 30 feet
straight toward a target and then hits it with a lance
attack on the same turn, the target takes an extra
10 (3d6) piercing damage and must succeed on a
DC 12 Dexterity saving throw or become impaled
by the centaur’s lance. While impaled, the creature
is considered grappled (escape DC 12) and takes an
additional 10 (3d6) piercing damage at the end of
each of its turns. While a creature is impaled, the
centaur may not attack with its lance or longbow
unless it chooses to end this effect.

**Actions**

___Multiattack.___ The centaur makes two attacks: one
with its lance and one with its hooves or two with
its greatbow.

___Lance.___ Melee Weapon Attack: +7 to hit, reach 10 ft.,
one target. Hit: 11 (1d12 + 5) piercing damage.

___Hooves.___ Melee Weapon Attack: +7 to hit, reach 5ft.,
one target. Hit: 14 (2d8 + 5) bludgeoning damage.

___Greatbow.___ Ranged Weapon Attack: +4 to hit, range
150/600 ft., one target. Hit: 11 (2d8 + 2) piercing
damage.
