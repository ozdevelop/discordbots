"Arcanaloth";;;_size_: Medium fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "12 (8,400 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft."
_skills_: "Arcana +13, Deception +9, Insight +9, Perception +7"
_damage_immunities_: "acid, poison"
_saving_throws_: "Dex +5, Int +9, Wis +7, Cha +7"
_speed_: "30 ft., fly 30 ft."
_hit points_: "104 (16d8+32)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 17 (+3) | 12 (+1) | 14 (+2) | 20 (+5) | 16 (+3) | 17 (+3) |

___Innate Spellcasting.___ The arcanaloth's innate spellcasting ability is Charisma (spell save DC 15). The arcanaloth can innately cast the following spells, requiring no material components:

* At will: _alter self, darkness, heat metal, invisibility _(self only)_ _, magic missile_

___Magic Resistance.___ The arcanaloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The arcanaloth's weapon attacks are magical.

___Spellcasting.___ The arcanaloth is a 16th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 17, +9 to hit with spell attacks). The arcanaloth has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, mage hand, minor illusion, prestidigitation_

* 1st level (4 slots): _detect magic, identify, shield, Tenser's floating disk_

* 2nd level (3 slots): _detect thoughts, mirror image, phantasmal force, suggestion_

* 3rd level (3 slots): _counterspell, fear, fireball_

* 4th level (3 slots): _banishment, dimension door_

* 5th level (2 slots): _contact other plane, hold monster_

* 6th level (1 slot): _chain lightning_

* 7th level (1 slot): _finger of death_

* 8th level (1 slot): _mind blank_

**Actions**

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) slashing damage. The target must make a DC 14 Constitution saving throw, taking 10 (3d6) poison damage on a failed save, or half as much damage on a successful one.

___Teleport.___ The arcanaloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.

___Variant: Summon Yugoloth (1/Day).___ The yugoloth attempts a magical summoning.

An arcanaloth has a 40 percent chance of summoning one arcanaloth.

A summoned yugoloth appears in an unoccupied space within 60 feet of its summoner, does as it pleases, and can't summon other yugoloths. The summoned yugoloth remains for 1 minute, until it or its summoner dies, or until its summoner takes a bonus action to dismiss it.
