"Female Steeder (A)";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "-"
_senses_: "darkvision 120 ft."
_skills_: "Stealth +7"
_speed_: "30 ft., climb 30 ft."
_hit points_: "30 (4d10+8)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 16 (+3) | 14 (+2) | 2 (-4) | 10 (0) | 3 (-4) |

___Spider Climb.___ The steeder can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Leap.___ The steeder can expend all its movement on its turn to jump up to 90 feet vertically or horizontally, provided that is speed is at least 30 feet.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 7 (1d8 + 3) piercing damage, and the target must make a DC 12 Constitution saving throw, taking 9 (2d8) acid damage on a failed save, or half as much damage on a successful one.

___Sticky Leg (Recharges when the Steeder Has No Creatures Grappled.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one Medium or smaller creature. Hit: The target is stuck ot the steeder's leg and grappled until it escapes (escape DC 12)