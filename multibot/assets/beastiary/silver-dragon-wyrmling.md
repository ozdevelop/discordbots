"Silver Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: lawful good
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +2"
_damage_immunities_: "cold"
_saving_throws_: "Dex +2, Con +5, Wis +2, Cha +4"
_speed_: "30 ft., fly 60 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 10 (0) | 17 (+3) | 12 (+1) | 11 (0) | 15 (+2) |

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) piercing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Cold Breath. The dragon exhales an icy blast in a 15-foot cone. Each creature in that area must make a DC 13 Constitution saving throw, taking 18 (4d8) cold damage on a failed save, or half as much damage on a successful one.

Paralyzing Breath. The dragon exhales paralyzing gas in a 15-foot cone. Each creature in that area must succeed on a DC 13 Constitution saving throw or be paralyzed for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.