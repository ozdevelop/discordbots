"Paper Drake";;;_size_: Small dragon
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Common, Draconic, Dwarvish, Elvish"
_senses_: "darkvision 60 ft., passive Perception 11"
_condition_immunities_: "paralysis, unconscious"
_speed_: "40 ft., fly 100 ft."
_hit points_: "78 (12d6 + 36)"
_armor class_: "13"
_stats_: | 7 (-2) | 17 (+3) | 16 (+3) | 10 (+0) | 12 (+1) | 13 (+1) |

___Shelve.___ A paper drake can fold itself into a small, almost flat form, perfect for hiding on bookshelves. The drake can still be recognized as something other than a book by someone who handles it (doesn't just glance at it on the shelf) and makes a successful DC 11 Intelligence (Nature or Investigation) check. The drake can hop or fly (clumsily, by flapping its pages) 5 feet per turn in this form.

___Refold (Recharge 5-6).___ A paper drake can fold its body into different sizes and shapes. The drake can adjust its size by one step in either direction, but can't be smaller than Tiny or larger than Medium size. Changes in size or shape don't affect the paper drake's stats.

**Actions**

___Multiattack.___ The drake makes one bite attack, one claw attack, and one tail attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 13 (3d6 + 3) slashing damage.

___Tail (Recharge 5-6).___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 16 (5d6 + 3) slashing damage, and the target must succeed on a DC 13 Constitution saving throw or be incapacitated for 1 round.

