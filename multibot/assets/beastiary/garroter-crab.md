"Garroter Crab";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "psychic"
_condition_immunities_: "charmed, frightened"
_speed_: "30 ft., swim 20 ft."
_hit points_: "18 (4d4 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 7 (-2) | 14 (+2) | 14 (+2) | 1 (-5) | 10 (+0) | 2 (-4) |

___Amphibious.___ The crab can breathe air and water.

**Actions**

___Whip-claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage, and the target is grappled (escape DC 8). While grappled, the target cannot speak or cast spells with verbal components.

