"Troglodyte Champion of Laogzed";;;_size_: Medium humanoid (troglodyte)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Troglodyte"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +6, Intimidation +3, Stealth +3"
_speed_: "30 ft."
_hit points_: "59 (7d8+28)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 18 (+4) | 8 (-1) | 23 (+6) | 12 (+1) |

___Chameleon Skin.___ The troglodyte has aadvantage on Dexterity (Stealth) checks made to hide.

___Stench.___ Any creature other than a ttroglodyte that starts its turn within 5 feet of the troglodyte must succeed on a DC 14 Constitution saving throw or be poisoned until the start of the creature's next turn. On a successful saving throw, the creature is immune to the stench of all ttroglodytes for 1 hour.

___Sunlight Sensitivity.___ While in sunlight, the troglodyte has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The troglodyte makes three attacks: one with its bite and two with either its claws or its greatclub.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d4 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d4 + 4) slashing damage.

___Greatclub.___ Melee Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage.

___Acid Spray (Recharge 6).___ The troglodyte spits acid in a line 15 feet long and 5 feet wide. Each creature in that line must make a DC 14 Dexterity saving throw, taking 10 (3d6) acid damage on a failed save, or half as much damage on a successful one.