"Necromancer";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "9 (5,000 XP)"
_languages_: "any four languages"
_skills_: "Arcana +7, History +7"
_saving_throws_: "Int +7, Wis +5"
_speed_: "30 ft."
_hit points_: "66 (12d8+12)"
_armor class_: "12 (15 with mage armor)"
_damage_resistances_: "necrotic"
_stats_: | 9 (-1) | 14 (+2) | 12 (+1) | 17 (+3) | 12 (+1) | 11 (0) |

___Spellcasting.___ The necromancer is a 12th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). The necromancer has the following wizard spells prepared:

* Cantrips (at will): _chill touch, dancing lights, mage hand, mending_

* 1st level (4 slots): _false life\*, mage armor, ray of sickness* _

* 2nd level (3 slots): _blindness/deafness\*, ray of enfeeblement\*, web_

* 3rd level (3 slots): _animate dead\*, bestow curse\*, vampiric touch* _

* 4th level (3 slots): _blight\*, dimension door, stoneskin_

* 5th level (2 slots): _Bigby's hand, cloudkill_

* 6th level (1 slot): _circle of death* _

*Necromancy spell of 1st level or higher

___Grim Harvest (1/Turn).___ When necromancer kills a creature that is neither a construct nor undead with a spell of 1st level or higher, the necromancer regains hit points equal to twice the spell's level, or three times if it is a necromancy spell.

**Actions**

___Withering Touch.___ Melee Spell Attack: +7 to hit, reach 5 ft., one creature. Hit: 5 (2d4) necrotic damage.
