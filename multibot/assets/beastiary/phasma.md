"Phasma";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "15 (13,000 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing and slashing from nonmagical weapons that aren’t silvered"
_condition_immunities_: "charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "117 (18d8 + 36)"
_armor class_: "15"
_stats_: | 6 (-2) | 20 (+5) | 15 (+2) | 15 (+2) | 17 (+3) | 20 (+5) |

___Innate Spellcasting.___ The phasma’s spellcasting ability is Charisma
(spell save DC 18, +10 to hit with spell attacks). It can innately cast the
following spells, requiring no material components:

* At will: _detect evil and good_

* 3/day each: _dispel magic, protection from evil and good_

* 1/day each: _banishment, (un)holy aura_

___Flyby.___ The phasma doesn’t provoke
opportunity attacks when it flies out of an
enemy’s reach.

___Incorporeal Movement.___ The phasma can
move through other creatures and objects as
if they were difficult terrain. It takes 5 (1d10)
force damage if it ends its turn inside an object.

___Sunlight Sensitivity.___ While in sunlight, the
phasma has disadvantage on attack rolls, as well
as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The phasma makes two phantom
touch attacks.

___Phantom Touch.___ Melee Weapon Attack: +10 to
hit, reach 5 ft., one target. Hit: 41 (8d8 + 5) necrotic
damage, and the target’s Wisdom score is
lowered by 1d6 and the phasma gains
5 temporary hit points. The target
dies if this reduces its Wisdom to
0. Otherwise, the reduction lasts
until the target finishes a long rest.

In addition, the target must make a DC
18 Wisdom saving throw. On a failure, the target can’t take reactions and
must roll a d10 at the start of each of its turns to determine its behavior
for that turn.
* ___1.___ The creature uses all its movement to move in a
random direction. To determine the direction, roll
a d8 and assign a direction to each die face. The
creature doesn’t take an action this turn.
* ___2-6.___ The creature doesn’t move or take actions this
turn.
* ___7-8.___ The creature uses its action to make a melee
attack against a randomly determined creature
within its reach. If there is no creature within its
reach, the creature does nothing this turn.
* ___9-10.___ The creature can act and move normally.

At the end of each of its turns, the creature can repeat the saving throw,
ending the effect on itself on a success.

___Phantasmagoria (Recharge 6).___ The phasma taps into the nightmares
of each creature it can see that is within 30 feet of it, creating an illusory
manifestation of each target’s deepest fears, visible only to that creature.
Each creature must make a DC 18 Wisdom saving throw, taking 33 (6d10)
psychic damage on a failed save, or half as much damage on a successful
one. A creature that succeeds on the saving throw is immune to the
phasma’s Phantasmagoria for the next 24 hours.
