"Giant Strider";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_damage_immunities_: "fire"
_speed_: "50 ft."
_hit points_: "22 (3d10+6)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 14 (+2) | 4 (-3) | 12 (+1) | 6 (-2) |

___Fire Absorption.___ Whenever the giant strider is subjected to fire damage, it takes no damage and regains a number of hit points equal to half the fire damage dealt.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8+4) piercing damage.

___Fire Burst (Recharges 5-6).___ The giant strider hurls a gout of flame at a point it can see within 60 feet of it. Each creature in a 10-foot-radius sphere centered on that point must make a DC 12 Dexterity saving throw, taking 14 (4d6) fire damage on a failed save, or half as much damage on a successful one. The fire spreads around corners, and it ignites flammable objects in that area that aren't being worn or carried.