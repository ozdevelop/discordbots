"Stone Golem";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "10 (5,900 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 120 ft."
_damage_immunities_: "poison, psychic, bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_speed_: "30 ft."
_hit points_: "178 (17d10+85)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 22 (+6) | 9 (-1) | 20 (+5) | 3 (-4) | 11 (0) | 1 (-5) |

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 19 (3d8 + 6) bludgeoning damage.

___Slow (Recharge 5-6).___ The golem targets one or more creatures it can see within 10 ft. of it. Each target must make a DC 17 Wisdom saving throw against this magic. On a failed save, a target can't use reactions, its speed is halved, and it can't make more than one attack on its turn. In addition, the target can take either an action or a bonus action on its turn, not both. These effects last for 1 minute. A target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.