"Pombero";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "Sylvan"
_skills_: "Athletics +5, Sleight of Hand +5, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "90 (12d8 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 16 (+3) | 16 (+3) | 8 (-1) | 10 (+0) | 14 (+2) |

___Beast's Voice.___ The pombero can magically speak with any beast and can perfectly mimic beast sounds.

___Twisted Limbs.___ The pombero can twist and squeeze itself through a space small enough for a Tiny bird to pass through as if it were difficult terrain.

___Sneak Attack (1/turn).___ The pombero does an extra 7 (2d6) damage with a weapon attack when it has advantage on the attack roll, or when the target is within 5 feet of an ally of the pombero that isn't incapacitated and the pombero doesn't have disadvantage on the roll.

___Soft Step.___ The pombero has advantage on Dexterity (Stealth) checks in forest terrain.

**Actions**

___Multiattack.___ The pombero uses Charming Touch if able, and makes two fist attacks.

___Fist.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage and the target is grappled (escape DC 13).

___Charming Touch (recharge 5-6).___ The pombero chooses a creature it can see within 5 feet. The creature must make a successful DC 12 Wisdom saving throw or be charmed for 10 minutes. The effect ends if the charmed creature takes damage. The pombero can have only one creature at a time charmed with this ability. If it charms a new creature, the previous charm effect ends immediately.

___Invisibility.___ The pombero becomes invisible until it chooses to end the effect as a bonus action, or when it attacks.

