"Urgala Meltimer";;;_size_: Medium humanoid (turami human)
_alignment_: lawful good
_challenge_: "0 (10 XP)"
_languages_: "Common, Giant"
_skills_: "Athletics +5, Intimidation +3"
_speed_: "30 ft."
_hit points_: "58 (9d8 + 18)"
_armor class_: "12 (leather)"
_senses_: "passive Perception 12"
_stats_: | 16 (+3) | 13 (+1) | 14 (+2) | 12 (+1) | 14 (+2) | 13 (+1) |

**Giant Slayer.** Any weapon attack that Urgala makes against a giant deals an extra 7 (2d6) damage on a hit.

**Actions**

**Multiattack.** Urgala makes two attacks with her morningstar or her shortbow.

**Morningstar.** Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

**Shortbow.** Ranged Weapon Attack: +3 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 1) piercing damage. Urgala carries a quiver of twenty arrows

**Roleplaying** Information

A retired adventurer, Urgala owns a respectable inn, the Northshield House, and she doesn't want to see it or her neighbours' homes destroyed. She has no tolerance for monsters or bullies.

**Ideal:** We live in a violent world, and sometimes violence is necessary for survival.

**Bond:** My home is my life. Threaten it, and I'll hurt you.

**Flaw:** I know how treacherous and greedy adventurers can be. I don't trust them - any of them.