"Swwarm of Zoblins";;;_size_: Large swarm of small creatures (goblinoids)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "poisoned, charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "30 ft."
_hit points_: "66 (12d6 + 24)"
_armor class_: "9"
_stats_: | 18 (+4) | 6 (-2) | 14 (+2) | 5 (-3) | 8 (-1) | 4 (-3) |

___Undead Swarm Fortitude.___ The swarm fights at full
strength until it is completely destroyed.

___Swarm.___ The swarm can occupy another creature's
space and vice versa, and the swarm can move
through any opening large enough for a Small
goblin. The swarm can't regain hit points or gain
temporary hit points.

___Plagued.___ When the swarm hits with an attack, it
deals an extra 10 (3d6) necrotic damage (included
in the attack).

**Actions**

___Multiattack.___ The swarm uses its pull under ability if
able, then uses its gnaw and claw attack.

___Gnaw and Claw.___ Melee Weapon Attack: +6 to hit,
reach 0 ft., one target in the swarm’s space. Hit: 11
(2d6 + 4) piercing damage plus 10 (3d6) necrotic
damage, and the target must succeed a DC 12
Constitution saving throw or become poisoned
until the end of their next turn.

___Pull Under.___ The swarm attempts to envelope a
creature within its space. The target must succeed
on a DC 12 Strength saving throw or be knocked
prone and considered grappled (escape DC 12) as
they are pinned to the ground under a sea of
zombies. This ability can only be used if the swarm
is above half of its maximum hit points.
