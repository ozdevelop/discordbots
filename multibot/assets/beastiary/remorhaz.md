"Remorhaz";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "11 (7,200 XP)"
_senses_: "darkvision 60 ft., tremorsense 60 ft."
_damage_immunities_: "cold, fire"
_speed_: "30 ft., burrow 20 ft."
_hit points_: "195 (17d12+85)"
_armor class_: "17 (natural armor)"
_stats_: | 24 (+7) | 13 (+1) | 21 (+5) | 4 (-3) | 10 (0) | 5 (-3) |

___Heated Body.___ A creature that touches the remorhaz or hits it with a melee attack while within 5 feet of it takes 10 (3d6) fire damage.

**Actions**

___Bite.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 40 (6d10 + 7) piercing damage plus 10 (3d6) fire damage. If the target is a creature, it is grappled (escape DC 17). Until this grapple ends, the target is restrained, and the remorhaz can't bite another target.

___Swallow.___ The remorhaz makes one bite attack against a Medium or smaller creature it is grappling. If the attack hits, that creature takes the bite's damage and is swallowed, and the grapple ends. While swallowed, the creature is blinded and restrained, it has total cover against attacks and other effects outside the remorhaz, and it takes 21 (6d6) acid damage at the start of each of the remorhaz's turns.

If the remorhaz takes 30 damage or more on a single turn from a creature inside it, the remorhaz must succeed on a DC 15 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet oft he remorhaz. If the remorhaz dies, a swallowed creature is no longer restrained by it and can escape from the corpse using 15 feet of movement, exiting prone.