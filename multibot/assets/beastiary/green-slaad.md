"Green Slaad";;;_size_: Large aberration (shapechanger)
_alignment_: chaotic neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Slaad, telepathy 60 ft."
_senses_: "blindsight 30 ft., darkvision 60 ft."
_skills_: "Arcana +3, Perception +2"
_speed_: "30 ft."
_hit points_: "127 (15d10+45)"
_armor class_: "16 (natural armor)"
_damage_resistances_: "acid, cold, fire, lightning, thunder"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 11 (0) | 8 (-1) | 12 (+1) |

___Shapechanger.___ The slaad can use its action to polymorph into a Small or Medium humanoid, or back into its true form. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Innate Spellcasting.___ The slaad's innate spellcasting ability is Charisma (spell save DC 12). The slaad can innately cast the following spells, requiring no material components:

* At will: _detect magic, detect thoughts, mage hand_

* 2/day each: _fear, invisibility _(self only)

* 1/day: _fireball_

___Magic Resistance.___ The slaad has advantage on saving throws against spells and other magical effects

___Regeneration.___ The slaad regains 10 hit points at the start of its turn if it has at least 1 hit point.

___Variant: Control Gem.___ Implanted in the slaad's brain is a magic control gem. The slaad must obey whoever possesses the gem and is immune to being charmed while so controlled.

Certain spells can be used to acquire the gem. If the slaad fails its saving throw against imprisonment, the spell can transfer the gem to the spellcaster's open hand, instead of imprisoning the slaad. A wish spell, if cast in the slaad's presence, can be worded to acquire the gem.

A greater restoration spell cast on the slaad destroys the gem without harming the slaad.

Someone who is proficient in Wisdom (Medicine) can remove the gem from an incapacitated slaad. Each try requires 1 minute of uninterrupted work and a successful DC 20 Wisdom (Medicine) check. Each failed attempt deals 22 (4d10) psychic damage to the slaad.

**Actions**

___Multiattack.___ The slaad makes three attacks: one with its bite and two with its claws or staff. Alternatively, it uses its Hurl Flame twice.

___Bite (Slaad Form Only).___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage.

___Claw (Slaad Form Only).___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

___Staff.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.

___Hurl Flame.___ Ranged Spell Attack: +4 to hit, range 60 ft., one target. Hit: 10 (3d6) fire damage. The fire ignites flammable objects that aren 't being worn or carried.
