"Mindwitness";;;_size_: Large aberration
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Deep Speech, Undercommon, telepathy 600 ft."
_senses_: "darkvision 120 ft."
_skills_: "Perception +8"
_saving_throws_: "Int +5, Wis +5"
_speed_: "0 ft., fly 20 ft. (hover)"
_hit points_: "75 (10d10+20)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "prone"
_stats_: | 10 (0) | 14 (+2) | 14 (+2) | 15 (+2) | 15 (+2) | 10 (0) |

___Telepathic Hub.___ When the mindwitness receives a telepathic message, it can telepathically share that message with up to seven other creatures within 600 feet of it that it can see.

**Actions**

___Multiattack.___ The mindwitness makes two attacks: one with its tentacles and one with its bite.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 16 (4d6+2) piercing damage.

___Tentacles.___ Melee Weapon Attack: +5 to hit, reach 5 it, one creature. Hit: 20 (4d8+2) psychic damage. if the target is Large or smaller, it is grappled (escape DC 13) and must succeed on a DC 13 Intelligence saving throw or be stunned until this grapple ends.

___Eye Rays.___ Eye Rays. The mindwitness shoots three of the following magical eye rays at random (reroll duplicates), choosing one to three targets it can see within 120 feet of it:

1. Aversion Ray: The targeted creature must make a DC 13 Charisma saving throw. On a failed save, the target has disadvantage on attack rolls for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

2. Fear Ray: The targeted creature must succeed on a DC 13 Wisdom saving throw or be frightened for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

3. Psychic Ray: The target must succeed on a DC 13 Intelligence saving throw or take 27 (6d8) psychic damage.

4. Slowing Ray: The targeted creature must make a DC 13 Dexterity saving throw. On a failed save, the target's speed is halved for 1 minute. In addition, the creature can't take reactions, and it can take either an action or a bonus action on its turn but not both. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

5. Stunning Ray: The targeted creature must succeed on a DC 13 Constitution saving throw or be stunned for 1 minute. The target can repeat the saving throw at the start of each of its turns, ending the effect on itself on a success.

6. Telekinetic Ray: If the target is a creature, it must make a DC 13 Strength saving throw. On a failed save, the mindwitness moves it up to 30 feet in any direction, and it is restrained by the ray's telekinetic grip until the start of the mindwitness's next turn or until the mindwitness is incapacitated.

If the target is an object weighing 300 pounds or less that isn't being worn or carried, it is telekinetically moved up to 30 feet in any direction. The mindwitness can also exert fine control on objects with this ray, such as manipulating a simple tool or opening a door or a container.
