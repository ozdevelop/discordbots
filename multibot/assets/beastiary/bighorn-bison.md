"Bighorn Bison";;;_size_: Large beast
_alignment_: neutral
_challenge_: "4 (1,100 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_speed_: "40 ft."
_hit points_: "57 (6d10 + 24)"
_armor class_: "14 (natural armor)"
_stats_: | 22 (+6) | 12 (+1) | 19 (+4) | 2 (-4) | 12 (+1) | 5 (-3) |

___Trample.___ If the bison moves at least 20 feet straight toward a creature
and then hits it with a gore attack on the same turn, that target must succeed
on a DC 16 Strength saving throw or be knocked prone. If the target is
prone, the bison can make one stomp attack against it as a bonus action.

**Actions**

___Multiattack.___ The bighorn bison can make one gore attack and one kick
attack.

___Gore.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 19
(3d8 + 6) piercing damage.

___Stomp.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 22
(3d10 + 6) bludgeoning damage.

___Kick.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target directly
behind it. Hit: 15 (2d8 + 6) bludgeoning damage.
