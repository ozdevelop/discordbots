"Duodrone";;;_size_: Medium construct
_alignment_: construct
_challenge_: "1/4 (50 XP)"
_languages_: "Modron"
_senses_: "truesight 120 ft."
_speed_: "30 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "15 (natural armor)"
_stats_: | 11 (0) | 13 (+1) | 12 (+1) | 6 (-2) | 10 (0) | 7 (-2) |

___Axiomatic Mind.___ The duodrone can't be compelled to act in a manner contrary to its nature or its instructions.

___Disintegration.___ If the duodrone dies, its body disintegrates into dust, leaving behind its weapons and anything else it was carrying.

**Actions**

___Multiattack.___ The duodrone makes two fist attacks or two javelin attacks.

___Fist.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d4) bludgeoning damage.

___Javelin.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 4 (1d6 + 1) piercing damage.