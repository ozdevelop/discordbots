"Deep Drake";;;_size_: Large dragon
_alignment_: chaotic evil
_challenge_: "9 (5000 XP)"
_languages_: "Common, Darakhul, Draconic, Undercommon"
_skills_: "Athletics +9, Insight +6, Perception +6"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 16"
_saving_throws_: "Dex +8, Con +6"
_damage_immunities_: "necrotic"
_condition_immunities_: "paralyzed, unconscious"
_speed_: "50 ft., climb 30 ft., fly 100 ft."
_hit points_: "150 (20d10 + 40)"
_armor class_: "17 (natural armor)"
_stats_: | 21 (+5) | 19 (+4) | 14 (+2) | 11 (+0) | 14 (+2) | 12 (+1) |

___Magic Resistance.___ The drake has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The drake makes one bite attack, two claw attacks, and one stinger attack.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft, one target. Hit: 12 (2d6 + 5) slashing damage.

___Stinger.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) piercing damage plus 7 (2d6) poison damage, and the target must succeed on a DC 16 Constitution saving throw or become poisoned for 4 rounds. While poisoned this way, the target must repeat the save at the start of its turn, ending the condition on a success. On a failure, it takes 10 (3d6) poison damage. When animate dead is cast on creatures killed by this poison, the caster requires no material components.

___Breath Weapon (Recharge 5-6).___ A deep drake blasts forth a crackling 80-foot line of purple-black energy that wracks its victims with pain. This attack deals 35 (10d6) necrotic damage, or half damage with a successful DC 16 Dexterity saving throw. Targets that fail this saving throw must also succeed on a DC 16 Constitution saving throw or become stunned for 1d4 rounds.

