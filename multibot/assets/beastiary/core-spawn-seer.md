"Core Spawn Seer";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Deep Speech, Undercommon, telepathy 120 ft."
_skills_: "Perception +9"
_saving_throws_: "Dex +6, Int +11, Wis +9, Cha +8"
_senses_: "blindsight 60 ft., tremorsense 60 ft., passive Perception 19"
_damage_immunities_: "psychic"
_condition_immunities_: "charmed, frightened"
_speed_: "30 ft."
_hit points_: "153 (18d8 + 72)"
_armor class_: "17 (natural armor)"
_stats_: | 14 (+2) | 12 (+1) | 18 (+4) | 22 (+6) | 19 (+4) | 16 (+3) |

___Earth Glide.___ The seer can traverse through nonmagical, unworked earth and stone. While doing so, the seer doesn’t disturb the material it moves through.

___Magic Resistance.___ The seer has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The seer uses Fission Staff twice, Psychedelic Orb twice, or each one once.

___Fission Staff.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 9 (1d6 + 6) bludgeoning damage plus 18 (4d8) radiant damage, and the target is knocked prone.

___Psychedelic Orb.___ The seer hurls a glimmering orb at one creature it can see within 120 of it. The target must succeed on a DC 19 Wisdom saving throw or take 27 (5d10) psychic damage and suffer a random condition until the start of the seer’s next turn. Roll a d6 for the condition:
* (1–2) blinded
* (3–4) frightened
* (5–6) stunned.

**Reactions**

___Fuse Damage.___ When the seer is hit by an attack, it takes only half of the triggering damage. The first time the seer hits with a melee attack on its next turn, the target takes an extra 1d6 radiant damage.
