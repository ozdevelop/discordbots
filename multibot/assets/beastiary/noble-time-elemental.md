"Noble Time Elemental";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "13 (10,000 XP)"
_languages_: "telepathy 120 ft."
_skills_: "Insight +9, Perception +9, Stealth +10"
_senses_: "darkvision 60 ft., passive Perception 19"
_saving_throws_: "Con +9, Wis +9, Cha +7"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "50 ft., fly 50 ft. (hover)"
_hit points_: "123 (13d10 + 52)"
_armor class_: "18 (natural armor)"
_stats_: | 18 (+4) | 20 (+5) | 18 (+4) | 18 (+4) | 18 (+4) | 15 (+2) |

___Cell Death.___ Damage dealt by the elemental can only be healed
magically. In addition, a creature that is slain by a time elemental can only
be restored to life by a _true resurrection_ or _wish_ spell.

___Foresight.___ A time elemental can see a few seconds into the future. This
ability prevents it from being surprised.

___Immunity to Temporal Magic.___ Time elementals are immune to all
time-related spells and effects that are not cast by other time elementals.

**Actions**

___Multiattack.___ The time elemental makes two slam attacks.

___Slam.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 18
(3d8 + 5) bludgeoning damage.

___Alter Age (1/day).___ The elemental can attempt to age a target creature
within 5 feet of it. The target must make a DC 17 Constitution saving
throw or be aged 1d4 x 10 years. The aging effect can be reversed with a
_greater restoration_ spell, but only within 24 hours of it occurring.

___Multi-Manifestation (Recharge 5–6).___ The time elemental summons 1d4
duplicate manifestations of itself from alternate dimensions. Each of these
manifestations have the same statistics of the time elemental but can only
use melee attacks. Each manifestation can move and attack immediately
after it is summoned. Attacks that deal damage to one manifestation deal
the same damage to the elemental and the other manifestations.
The elemental can have no more than four manifestations under its control at
any time. The manifestations disappear at the start of the elemental’s next turn.

___Temporal Displacement (1/day).___ The time elemental can remove a target
creature from the current timeline. The target must succeed on a DC 18
Constitution saving throw, or disappear in a flash of white energy. For a number
of minutes equal to the time elemental’s Wisdom modifier, it is as if the displaced
creature never existed. The creature is completely undetectable while in this state.
A displaced creature can use its action to attempt to end the displacement.
When it does so, it makes a DC 18 Intelligence check. If it succeeds, it
escapes, and the effect ends.

When the effect ends, the creature reappears in the same space it was in
before being displaced. If the space is occupied when the creature returns,
it appears in the nearest open space and takes no damage.

___Time Jaunt.___ A time elemental can slip through the time stream and
appear anywhere on the same plane of existence as if by _teleport_. This
ability transports the time elemental and up to four other creatures of the
elemental’s choice that are within 30 feet of it. Unwilling creatures must
succeed on a DC 15 Wisdom saving throw to avoid being carried away.
