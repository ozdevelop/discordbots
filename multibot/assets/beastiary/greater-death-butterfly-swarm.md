"Greater Death Butterfly Swarm";;;_size_: Huge swarm
_alignment_: chaotic evil
_challenge_: "6 (2300 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_vulnerabilities_: "cold, fire"
_damage_resistances_: "bludgeoning, piercing, and slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, petrified"
_speed_: "5 ft., fly 40 ft. (hover)"
_hit points_: "84 (13d12)"
_armor class_: "15 (natural armor)"
_stats_: | 1 (-5) | 16 (+3) | 10 (+0) | 1 (-5) | 15 (+2) | 12 (+1) |

___Potent Poison.___ The death butterfly swarm's poison affects corporeal undead who are otherwise immune to poison.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny insect. The swarm can't regain hit points or gain temporary hit point.

___Weight of Wings.___ As death butterfly swarm but with DC 16 Dexterity saving throw

**Actions**

___Multiattack.___ The swarm makes a Bite attack against every target in its spaces.

___Bites.___ Melee Weapon Attack: +6 to hit, reach 0 ft., every target in the swarm's space. Hit: 24 (6d6 +3) piercing damage, or 13 (3d6 + 3) piercing damage if the swarm has half of its hit points or fewer. The target also takes 17 (5d6) poison damage and becomes poisoned for 1d4 rounds; a successful DC 15 Constitution saving throw reduces poison damage by half and prevents the poisoned condition.

