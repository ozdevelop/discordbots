"Sharwyn Hucrele";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Draconic, Goblin"
_skills_: "Arcana +5, Insight +4, Persuasion + 1"
_speed_: "30 ft."
_hit points_: "13 (2d8+4)"
_armor class_: "16 (Barkskin trait)"
_stats_: | 11 (0) | 13 (+1) | 14 (+2) | 16 (+3) | 14 (+2) | 9 (-1) |

___Source.___ tales from the yawning portal,  page 242

___Barkskin.___ Sharwyn's AC can't be lower than 16.

___Special Equipment.___ Sharwyn has a spell book that contains the spells listed in her Spellcasting trait, plus detect magic and silent image.

___Spellcasting.___ Sharwyn is a 1st-level spellcaster. Her spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). She has the following wizard spells prepared:

* Cantrips (at will): _light, prestidigitation, ray of frost_

* 1st level (2 slots): _color spray, magic missile, shield, sleep_

___Tree Thrall.___ If the Gulthias Tree dies, Sharwyn dies 24 hours later.

**Actions**

___Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
