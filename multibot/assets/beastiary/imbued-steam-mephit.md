"Imbued Steam Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Aquan, Terran"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 30 ft."
_hit points_: "28 (8d6)"
_armor class_: "12"
_stats_: | 6 (-2) | 14 (+2) | 10 (+0) | 12 (+1) | 10 (+0) | 12 (+1) |

___Death Burst.___ When the mephit dies, it explodes in a
cloud of steam. Each creature within 5 feet of it
must make a DC 11 Dexterity saving throw, taking
9 (2d8) fire damage on a failed save.

___Innate Spellcasting (1/Day).___ The mephit can innately
cast _mirror image_, requiring no material
components. Its innate spellcasting ability is
Charisma.

**Actions**

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 7 (2d4 + 2) piercing damage plus 2
(1d4) fire damage.

___Steam Discharge (Recharge 6).___ The mephit causes
steam to erupt from its body. Each creature in a 15-
foot cube originating on the mephit must make a
DC 11 Dexterity saving throw, taking 9 (2d8) fire
damage on a failed save, or half as much damage on
a successful one.
