"Mongrelfolk";;;_page_number_: 234
_size_: Medium humanoid (mongrelfolk)
_alignment_: any alignment
_challenge_: "1/4 (50 XP)"
_languages_: "Common"
_skills_: "Deception +2, Perception +2, Stealth +3"
_speed_: "20 ft."
_hit points_: "26 (4d8+8)"
_armor class_: "11 (natural armor)"
_stats_: | 12 (+1) | 9 (-1) | 15 (+2) | 9 (-1) | 10 (0) | 6 (-2) |

___Extraordinary Feature.___ The mongrelfolk has one of the following extraordinary features, determined randomly by rolling a d20 or chosen by the DM:

* ___1-3___ - Amphibious: The mongrelfolk can breathe air and water.

* ___4-9___ - Darkvision: The mongrelfolk has darkvision out to a range of 60 feet.

* ___10___ - Flight: The mongrelfolk has leathery wings and a flying speed of 40 feet.

* ___11-15___ - Keen Hearing and Smell: The mongrelfolk has advantage on Wisdom (Perception) checks that rely on hearing or smell.

* ___16-17___ - Spider Climb: The mongrelfolk can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

* ___18-19___ - Standing Leap: The mongrelfolk's long jump is up to 20 feet and its high jump up to 10 feet, with or without a running start.

* ___20___ - Two-Headed: The mongrelfolk has advantage on Wisdom (Perception) checks and on saving throws against being blinded, charmed, deafened, frightened, stunned, or knocked unconscious.

___Mimicry.___ The mongrelfolk can mimic any sounds it has beard, including voices. A creature that hears the sounds can tell they are imitations with a successful DC 12 Wisdom (Insight) check.

**Actions**

___Multiattack.___ The mongrelfolk makes two attacks: one with its bite and one with its claw or dagger.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4+1) piercing damage.

___Claw.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4+1) slashing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 3 (1d4+1) piercing damage.
