"Animated Armor";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_damage_immunities_: "poison, psychic"
_speed_: "25 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 14 (+2) | 11 (0) | 13 (+1) | 1 (-5) | 3 (-4) | 1 (-5) |

___Antimagic Susceptibility.___ The armor is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the armor must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute.

___False Appearance.___ While the armor remains motionless, it is indistinguishable from a normal suit of armor.

**Actions**

___Multiattack.___ The armor makes two melee attacks.

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage.