Abyssal Wretch (A);;;_page_number_: 136
_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: 1/4 (50 XP)
_languages_: Understands Abyssal but can’t speak
_senses_: Darkvision 120 ft., passive Perception 9
_damage_immunities_: Poison
_speed_: 20 ft.
_hit points_: 18 (4d8)
_armor class_: 11
_condition_immunities_: Charmed, frightened, poisoned
_stats_: | 9 (-1) | 12 (+1) | 11 (+0) | 5 (-3) | 8 (-1) | 5 (-3) |

**Damage Resistance** Cold, fire, lightning


**Actions**

___Bite___ Melee Weapon: +3 to hit, reach 5 ft., One target. Hit: (1d8+1) Slashing damage.