"Poisonous Snake";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_senses_: "blindsight 10 ft."
_speed_: "30 ft., swim 30 ft."
_hit points_: "2 (1d4)"
_armor class_: "13"
_stats_: | 2 (-4) | 16 (+3) | 11 (0) | 1 (-5) | 10 (0) | 3 (-4) |

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 piercing damage, and the target must make a DC 10 Constitution saving throw, taking 5 (2d4) poison damage on a failed save, or half as much damage on a successful one.