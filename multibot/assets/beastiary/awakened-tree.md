"Awakened Tree";;;_size_: Huge plant
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "one language known by its creator"
_speed_: "20 ft."
_hit points_: "59 (7d12+14)"
_armor class_: "13 (natural armor)"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "bludgeoning, piercing"
_stats_: | 19 (+4) | 6 (-2) | 15 (+2) | 10 (0) | 10 (0) | 7 (-2) |

___False Appearance.___ While the tree remains motionless, it is indistinguishable from a normal tree.

**Actions**

___Slam.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 14 (3d6 + 4) bludgeoning damage.