"Wolf Trainer";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages"
_skills_: "Acrobatics +6, Animal Handling +6, Perception +6, Survival +6"
_senses_: "passive Perception 16"
_saving_throws_: "Str +5, Dex +6"
_speed_: "30 ft."
_hit points_: "58 (9d10 + 9)"
_armor class_: "16 (breastplate)"
_stats_: | 14 (+2) | 16 (+3) | 13 (+1) | 12 (+1) | 17 (+3) | 12 (+1) |

___Faithful Pet.___ The trainer has a faithful wolf (CR 1/4)
companion that always stays by its side and obeys
the trainer's commands to the best of its ability.
When the trainer makes the first attack on a turn
against a target within 5 feet of its wolf, the trainer
gains advantage on that attack.

___Two-Weapon Fighting Style.___ The trainer adds its
ability modifier to the damage of its off-hand
weapon attacks.

___Spellcasting.___ The trainer is a 6th-level spellcaster. Its
spellcasting ability is Wisdom (spell save DC 14, +6
to hit with spell attacks). The trainer has the
following ranger spells prepared:

* 1st level (4 slots): _animal friendship, beast bond, hunter's mark_

* 2nd level (2 slots): _locate animals and plants_

**Actions**

___Multiattack.___ The trainer makes three attacks with its
shortsword or two attacks with its longbow.

___Shortsword.___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Longbow.___ Ranged Weapon Attack: +6 to hit, range
150/600 ft., one target. Hit: 7 (1d8 + 3) piercing
damage.
