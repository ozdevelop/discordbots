"Dragon Turtle";;;_size_: Gargantuan dragon
_alignment_: neutral
_challenge_: "17 (18,000 XP)"
_languages_: "Aquan, Draconic"
_senses_: "darkvision 120 ft."
_saving_throws_: "Dex +6, Con +11, Wis +7"
_speed_: "20 ft., swim 40 ft."
_hit points_: "341 (22d20+110)"
_armor class_: "20 (natural armor)"
_damage_resistances_: "fire"
_stats_: | 25 (+7) | 10 (0) | 20 (+5) | 10 (0) | 12 (+1) | 12 (+1) |

___Amphibious.___ The dragon turtle can breathe air and water.

**Actions**

___Multiattack.___ The dragon turtle makes three attacks: one with its bite and two with its claws. It can make one tail attack in place of its two claw attacks.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 26 (3d12 + 7) piercing damage.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 16 (2d8 + 7) slashing damage.

___Tail.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 26 (3d12 + 7) bludgeoning damage. If the target is a creature, it must succeed on a DC 20 Strength saving throw or be pushed up to 10 feet away from the dragon turtle and knocked prone.

___Steam Breath (Recharge 5-6).___ The dragon turtle exhales scalding steam in a 60-foot cone. Each creature in that area must make a DC 18 Constitution saving throw, taking 52 (15d6) fire damage on a failed save, or half as much damage on a successful one. Being underwater doesn't grant resistance against this damage.