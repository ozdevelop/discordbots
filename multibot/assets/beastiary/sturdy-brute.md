"Sturdy Brute";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages"
_skills_: "Animal Handling +4, Athletics +7, Survival +4"
_senses_: "passive Perception 11"
_saving_throws_: "Str +5, Con +5"
_speed_: "40 ft."
_hit points_: "66 (7d12 + 21)"
_armor class_: "15 (unarmored defense)"
_stats_: | 18 (+4) | 14 (+2) | 16 (+3) | 8 (-1) | 12 (+1) | 10 (+0) |

___Danger Sense.___ The brute has advantage on
Dexterity saving throws.

___Legendary Resistance (1/Day).___ If the brute fails a
saving throw, it can choose to succeed instead.

___Rage (4/Day).___ As a bonus action, the brute can
enter a rage for 1 minute. While raging, the
brute gains the following benefits:
* Advantage on Strength checks and Strength
saving throws
* Melee weapon attacks deal an additional 2
damage.
* The brute gains resistance to bludgeoning,
piercing, and slashing damage.

___Totem of the Bear.___ While raging, the brute has
resistance to all damage psychic damage.
Additionally, the brute's carrying capacity is
doubled and it has advantage on Strength checks
made to push, pull, lift, or break objects.

___Unarmored Defense.___ While not wearing armor, the
brute's AC includes its Constitution modifier.

**Actions**

___Multiattack.___ The brute makes two attacks with its
greataxe.

___Greataxe.___ Melee Weapon Attack: +7 to hit, reach 5
ft., one target. Hit: 10 (1d12 + 4) slashing damage,
or 12 (1d12 + 6) slashing damage if raging.
