"Cranium Rat";;;_size_: Tiny beast
_alignment_: lawful evil
_challenge_: "0 (10 XP)"
_languages_: "telepathy 30 ft."
_senses_: "darkvision 30 ft."
_speed_: "30 ft."
_hit points_: "2 (1d4)"
_armor class_: "12"
_stats_: | 2 (-4) | 14 (+2) | 10 (0) | 4 (-3) | 11 (0) | 8 (-1) |

___Illumination.___ As a bonus action, the cranium rat can shed dim light from its brain in a 5-foot radius or extinguish the light.

___Telepathic Shroud.___ The cranium rat is immune to any effect that would sense its emotions or read its thoughts, as well as to all divination spells.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 1 piercing damage.