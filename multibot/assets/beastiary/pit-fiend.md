"Pit Fiend";;;_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "20 (25,000 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "truesight 120 ft."
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +8, Con +13, Wis +10"
_speed_: "30 ft., fly 60 ft."
_hit points_: "300 (24d10+168)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 26 (+8) | 14 (+2) | 24 (+7) | 22 (+6) | 18 (+4) | 24 (+7) |

___Fear Aura.___ Any creature hostile to the pit fiend that starts its turn within 20 feet of the pit fiend must make a DC 21 Wisdom saving throw, unless the pit fiend is incapacitated. On a failed save, the creature is frightened until the start of its next turn. If a creature's saving throw is successful, the creature is immune to the pit fiend's Fear Aura for the next 24 hours.

___Magic Resistance.___ The pit fiend has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The pit fiend's weapon attacks are magical.

___Innate Spellcasting.___ The pit fiend's spellcasting ability is Charisma (spell save DC 21). The pit fiend can innately cast the following spells, requiring no material components:

* At will: _detect magic, fireball_

* 3/day each: _hold monster, wall of fire_

**Actions**

___Multiattack.___ The pit fiend makes four attacks: one with its bite, one with its claw, one with its mace, and one with its tail.

___Bite.___ Melee Weapon Attack: +14 to hit, reach 5 ft., one target. Hit: 22 (4d6 + 8) piercing damage. The target must succeed on a DC 21 Constitution saving throw or become poisoned. While poisoned in this way, the target can't regain hit points, and it takes 21 (6d6) poison damage at the start of each of its turns. The poisoned target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Claw.___ Melee Weapon Attack: +14 to hit, reach 10 ft. , one target. Hit: 17 (2d8 + 8) slashing damage.

___Mace.___ Melee Weapon Attack: +14 to hit, reach 10ft., one target. Hit: 15 (2d6 + 8) bludgeoning damage plus 21 (6d6) fire damage.

___Tail.___ Melee Weapon Attack: +14 to hit, reach 10ft., one target. Hit: 24 (3d10 + 8) bludgeoning damage.
