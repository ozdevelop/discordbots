"Giant Eagle";;;_size_: Large beast
_alignment_: neutral good
_challenge_: "1 (200 XP)"
_languages_: "Giant Eagle, understands Common and Auran but can't speak"
_skills_: "Perception +4"
_speed_: "10 ft., fly 80 ft."
_hit points_: "26 (4d10+4)"
_armor class_: "13"
_stats_: | 16 (+3) | 17 (+3) | 13 (+1) | 8 (-1) | 14 (+2) | 10 (0) |

___Keen Sight.___ The eagle has advantage on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The eagle makes two attacks: one with its beak and one with its talons.

___Beak.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Talons.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage.