"Dwarf Guard";;;_page_number_: 999
_size_: Medium humanoid (dwarf)
_alignment_: any alignment
_challenge_: "1/8 (25 XP)"
_languages_: "dwarf"
_senses_: "Darkvision, passive Perception 12"
_skills_: "Perception +2"
_speed_: "30 ft."
_hit points_: "11  (2d8 + 2)"
_armor class_: "16 (chain shirt, shield)"
_damage_resistances_: "poison"
_stats_: | 13 (+1) | 12 (+1) | 12 (+1) | 10 (0) | 11 (0) | 10 (0) |

___Dwarven Resilience.___ The dwarf has advantage on saving throws against poison and has resistance to poison damage

**Actions**

___Spear___ Melee Attack: +3 to hit, reach 5 ft. one target. Hit: 4 (1d6 + 1) piercing damage or 5 (1d8 + 1) piercing damage if used with two hands. Or Ranged Weapon Attack: +3 to hit, range 20/60 ft., one target. Hit: 4 (1d6 + 1) piercing damage
