"Captain Erlich";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Common"
_senses_: "passive Perception 10"
_speed_: "30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "20 (plate, shield)"
_stats_: | 16 (+3) | 11 (+0) | 14 (+2) | 11 (+0) | 11 (+0) | 15 (+2) |

___Special Equipment.___ Captain Erlich has two _potions of healing_.

___Brave.___ Captain Erlich has advantage on saving throws against being frightened.

**Actions**

___Multiattack.___ Captain Erlich makes two melee attacks.

___Longsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage.

___Poisoned Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage and the target must succeed on a DC 15 saving throw or become poisoned for 1d4 hours.

___Battle Cry (1/Day).___ Each creature of Captain Erlich’s choice that is within 30 feet of it, can hear it, and not already affected by Battle Cry gain advantage on attack rolls until the start of Captain Erlich’s next turn. Captain Erlich can then make one attack as a bonus action.
