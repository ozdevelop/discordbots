"Mouth of Grolantor";;;_size_: Huge giant (hill giant)
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Giant"
_skills_: "Perception +1"
_speed_: "50 ft."
_hit points_: "105 (10d12+40)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "frightened"
_stats_: | 21 (+5) | 10 (0) | 18 (+4) | 5 (-3) | 7 (-2) | 5 (-3) |

___Mouth of Madness.___ The giant is immune to confusion spells and similar magic.

On each of its turns, the giant uses all its movement to move toward the nearest creature or whatever else it might perceive as food. Roll a d10 at the start of each of the giant's turns to determine its action for that turn:

* ___1-3.___ The giant makes three attacks with its fists against one random target within its reach. If no other creatures are within its reach, the giant flies into a rage and gains advantage on all attack rolls until the end of its next turn.

* ___4-5.___ The giant makes one attack with its fist against every creature within its reach. If no other creatures are within its reach, the giant makes one fist attack against itself.

* ___6-7.___ The giant makes one attack with its bite against one random target within its reach. If no other creatures are within its reach, its eyes glaze over and it becomes stunned until the start of its next turn.

* ___8-10.___ The giant makes three attacks against one random target within its reach: one attack with its bite and two with its fists. If no other creatures are within its reach, the giant flies into a rage and gains advantage on all attack rolls until the end of its next turn.

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 15 (3d6+5) piercing damage, and the giant magically regains hit points equal to the damage dealt.

___Fist.___ Melee Weapon Attack: +8 to hit, reach 10 ft, one target. Hit: 18 (3d8+5) bludgeoning damage.
