"Fire Giant Howdah";;;_size_: Huge giant
_alignment_: lawful evil
_challenge_: "9 (5,000 XP)"
_languages_: "Giant"
_senses_: "Passive Perception 16"
_skills_: "Athletics +11, Perception +6"
_saving_throws_: "Dex +3, Con +10, Cha +5"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "162 (13d12 + 78)"
_armor class_: "18 (plate)"
_stats_: | 25 (+7) | 9 (-1) | 23 (+6) | 10 (+0) | 14 (+2) | 13 (+2) |

___Howdah.___ The fire giant carries a compact fort on
its back. Up to four Medium creatures can ride in
the fort without squeezing. Creatures in the fort
are 15 feet from the ground, out of range of melee
weapons. Creatures in the fort have three-quarters
cover against attacks and effects from outside it. If
the fire giant dies, creatures in the fort are placed
in unoccupied spaces within 5 feet of the fire giant.

**Actions**

___Multiattack.___ The giant makes two greatsword
attacks.

___Greatsword.___ Melee Weapon Attack: +11 to hit,
reach 10 ft., one target. Hit: 28 (6d6 + 7) slashing
damage.

___Rock.___ Ranged Weapon Attack: +11 to hit, range
60/240 ft., one target. Hit: 29 (4d10 + 7) bludgeoning
damage.
