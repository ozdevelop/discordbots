"Moloch";;;_page_number_: 177
_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "21 (33,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 21"
_skills_: "Deception +13, Intimidation +13, Perception +11"
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +11, Con +13, Wis +11, Cha +13"
_speed_: "30 ft."
_hit points_: "253  (22d10 + 132)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 26 (+8) | 19 (+4) | 22 (+6) | 21 (+5) | 18 (+4) | 23 (+6) |

___Innate Spellcasting.___ Moloch's innate spellcasting ability is Charisma (spell save DC 21). He can innately cast the following spells, requiring no material components:

* At will: _alter self _(can become Medium when changing his appearance)_, animate dead, burning hands _(as a 7th-level spell)_, confusion, detect magic, fly, geas, major image, stinking cloud, suggestion, wall of fire_

* 1/day each: _flame strike, symbol _(stunning only)

___Legendary Resistance (3/Day).___ If Moloch fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Moloch has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Moloch's weapon attacks are magical.

___Regeneration.___ Moloch regains 20 hit points at the start of his turn. If he takes radiant damage, this trait doesn't function at the start of his next turn. Moloch dies only if he starts his turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack___ Moloch makes three attacks: one with his bite, one with his claw, and one with his whip.

___Bite___ Melee Weapon Attack: +15 to hit, reach 5 ft., one target. Hit: 26 (4d8 + 8) piercing damage.

___Claw___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target. Hit: 17 (2d8 + 8) slashing damage.

___Many-Tailed Whip___ Melee Weapon Attack: +15 to hit, reach 30 ft., one target. Hit: 13 (2d4 + 8) slashing damage plus 11 (2d10) lightning damage. If the target is a creature, it must succeed on a DC 24 Strength saving throw or be pulled up to 30 feet in a straight line toward Moloch.

___Breath of Despair (Recharge 5-6)___ Moloch exhales in a 30-foot cube. Each creature in that area must succeed on a DC 21 Wisdom saving throw or take 27 (5d10) psychic damage, drop whatever it is holding, and become frightened for 1 minute. While frightened in this way, a creature must take the Dash action and move away from Moloch by the safest available route on each of its turns, unless there is nowhere to move, in which case it needn't take the Dash action. If the creature ends its turn in a location where it doesn't have line of sight to Moloch, the creature can repeat the saving throw. On a success, the effect ends.

___Teleport___ Moloch magically teleports, along with any equipment he is wearing and carrying, up to 120 feet to an unoccupied space he can see.

**Legendary** Actions

Moloch can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Moloch regains spent legendary actions at the start of his turn.

___Stinking Cloud___ Moloch casts stinking cloud.

___Teleport___ Moloch uses his Teleport action.

___Whip___ Moloch makes one attack with his whip.
