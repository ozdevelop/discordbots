"Iron-Infused Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_damage_resistances_: "piercing, bludgeoning, and slashing damage from nonmagical weapons"
_speed_: "25 ft."
_hit points_: "27 (5d6 + 10)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 10 (+0) | 14 (+2) | 12 (+1) | 10 (+0) | 8 (-1) |

**Actions**

___Pummeling Strike.___ Melee Weapon Attack: +4 to hit,
reach 5ft., one target. Hit: 6 (1d8 + 2) bludgeoning
damage and the target must succeed on a DC 11
Strength saving throw or be knocked prone.

___Iron Shard.___ Ranged Weapon Attack: +4 to hit, range
20/60 ft., one target. Hit: 6 (1d8 + 2) piercing damage.
