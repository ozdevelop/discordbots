"Tabaxi Minstrel";;;_size_: Medium humanoid (tabaxi)
_alignment_: chaotic good
_challenge_: "1/4 (50 XP)"
_Languages_: "Common plus any two languages"
_senses_: "darkvision 60 ft., passive Perception 13"
_skills_: "Perception +3, Performance +7, Persuasion +5, Stealth +4"
_speed_: "30 ft., climb 20 ft."
_hit points_: "22 (5d8)"
_armor class_: "12"
_stats_: | 10 (0) | 15 (+2) | 11 (0) | 14 (+2) | 12 (+1) | 16 (+3) |

___Feline agility.___ When the tabaxi moves on its turn in combat, it can double its speed until the end of the turn. Once it uses this ability, the tabaxi can't use it again until it moves 0 feet on one of its turns.

___Inspire (1/Day).___ While taking a short rest, the tabaxi can spend 1 minute singing, playing an instrument, telling a story, or reciting a poem to soothe and inspire creatures other than itself. Up to five creatures of the tabaxi's choice that can see and hear its performance gain 8 temporary hit points at the end of the tabaxi's short rest.

**Actions**

___Multiattack.___ The tabaxi makes two claws attacks or two dart attacks.

___Claws.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (ld4) slashing damage.

___Dart.___ Ranged Weapon Attack: +4 to hit, range 20/ 60 ft., one target. Hit: 4 (ld4 +2) piercing damage.
