Abyssal Wretch (B);;;_page_number_: 136
_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: 1/4 (50 XP)
_languages_: understands Abyssal but can't speak
_senses_: darkvision 120 ft., passive Perception 9
_damage_immunities_: poison
_speed_: 20 ft.
_hit points_: 18  (4d8)
_armor class_: 11
_condition_immunities_: charmed, frightened, poisoned
_damage_resistances_: cold, fire, lightning
_stats_: | 9 (0) | 12 (+1) | 11 (0) | 5 (-2) | 8 (-1) | 5 (-2) |

**Actions**

___Bite___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) slashing damage.