"Small Ice Elemental";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Aquan"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "thunder, fire"
_damage_immunities_: "poison, cold"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "30 ft."
_hit points_: "22 (3d10 + 6)"
_armor class_: "12 (natural armor)"
_stats_: | 14 (+2) | 10 (+0) | 14 (+2) | 5 (-3) | 10 (+0) | 8 (-1) |

___Frozen Form.___ The ground within 10 ft. of the
elemental is considered difficult terrain as it
becomes lightly frozen. When a creature enters this
area for the first time, they must succeed on a DC
10 Dexterity saving throw or fall prone.

**Actions**

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 6 (1d8 + 2) bludgeoning damage.

___Ice Shard.___ Ranged Weapon Attack: +4 to hit, range
30/120 ft., one target. Hit: 4 (1d4 + 2) piercing
damage plus 1 cold damage.
