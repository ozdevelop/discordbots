"Duergar Warlord";;;_page_number_: 192
_size_: Medium humanoid (dwarf)
_alignment_: lawful evil
_challenge_: "6 (2300 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 11"
_speed_: "25 ft."
_hit points_: "75  (10d8 + 30)"
_armor class_: "20 (plate mail, shield)"
_damage_resistances_: "poison"
_stats_: | 18 (+4) | 11 (0) | 17 (+3) | 12 (+1) | 12 (+1) | 14 (+2) |

___Duergar Resilience.___ The duergar has advantage on saving throws against poison, spells, and illusions, as well as to resist being charmed or paralyzed.

___Sunlight Sensitivity.___ While in sunlight, the duergar has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The duergar makes three hammer or javelin attacks and uses Call to Attack, or Enlarge if it is available.

___Psychic-Attuned Hammer___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) bludgeoning damage plus 5 (1d10) psychic damage, or 15 (2d10 + 4) bludgeoning damage plus 5 (1d10) psychic damage while enlarged,

___Javelin___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 7 (1d6 + 4) piercing damage, or 11 (2d6 + 4) piercing damage while enlarged.

___Call to Attack___ Up to three allied duergar within 120 feet of this duergar that can hear it can each use their reaction to make one weapon attack.

___Enlarge (Recharges after a Short or Long Rest)___ For 1 minute, the duergar magically increases in size, along with anything it is wearing or carrying. While enlarged, the duergar is Large, doubles its damage dice on Strength-based weapon attacks (included in the attacks), and makes Strength checks and Strength saving throws with advantage. If the duergar lacks the room to become Large, it attains the maximum size possible in the space available.

___Invisibility (Recharge 4-6)___ The duergar magically turns invisible for up to 1 hour or until it attacks, it casts a spell, it uses its Enlarge, or its concentration is broken (as if concentrating on a spell). Any equipment the duergar wears or carries is invisible with it.