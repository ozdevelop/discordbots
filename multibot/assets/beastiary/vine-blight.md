"Vine Blight";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_skills_: "Stealth +1"
_speed_: "10 ft."
_hit points_: "26 (4d8+8)"
_armor class_: "12 (natural armor)"
_condition_immunities_: "blinded, deafened"
_stats_: | 15 (+2) | 8 (-1) | 14 (+2) | 5 (-3) | 10 (0) | 3 (-4) |

___False Appearance.___ While the blight remains motionless, it is indistinguishable from a tangle of vines.

**Actions**

___Constrict.___ Melee Weapon Attack: +4 to hit, reach 10 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage, and a Large or smaller target is grappled (escape DC 12) . Until this grapple ends, the target is restrained, and the blight can't constrict another target.

___Entangling Plants (Recharge 5-6).___ Grasping roots and vines sprout in a 15-foot radius centered on the blight, withering away after 1 minute. For the duration, that area is difficult terrain for nonplant creatures. In addition, each creature of the blight's choice in that area when the plants appear must succeed on a DC 12 Strength saving throw or become restrained. A creature can use its action to make a DC 12 Strength check, freeing it self or another entangled creature within reach on a success.