"Rusalka";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "6 (2300 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "necrotic, poison; piercing and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, frightened, paralyzed, poisoned"
_speed_: "30 ft., swim 40 ft."
_hit points_: "88 (16d8 + 16)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 13 (+1) | 12 (+1) | 11 (+0) | 15 (+2) | 18 (+4) |

___Withered Tresses.___ If a rusalka is kept out of water for 24 consecutive hours, its hair and body dry into desiccated swamp weeds and the creature is utterly destroyed.

___Innate Spellcasting.___ The rusalka's innate casting ability is Charisma (spell save DC 15). She can innately cast the following spells, requiring no material components:

At will: control water, suggestion, tongues, water walk (can be ended freely at will)

1/day: dominate person

___Watery Camouflage.___ In dim light or darkness, a rusalka that's underwater is invisible.

**Actions**

___Breathless Kiss.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: the target is grappled (escape DC 13) and the rusalka draws all the air from the target's lungs with a kiss. If the rusalka has movement remaining, she drags the grappled creature into deep water, where it begins suffocating.

___Drowning Hair (1/Day).___ The rusalka's long hair tangles around a creature the rusalka has grappled. The creature takes 33 (6d10) necrotic damage, or half damage with a successful DC 15 Constitution saving throw. In addition, until it escapes the rusalka's grapple, it is restrained and has disadvantage on Strength checks to break free of the grapple.

