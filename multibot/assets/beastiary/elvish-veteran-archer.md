"Elvish Veteran Archer";;;_size_: Medium Humanoid
_alignment_: chaotic good or chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Elvish"
_skills_: "Nature +2, Perception +5, Stealth +5, Survival +3"
_senses_: "passive Perception 15"
_speed_: "30 ft."
_hit points_: "77 (14d8+14)"
_armor class_: "15 (studded leather)"
_stats_: | 11 (+0) | 16 (+3) | 12 (+1) | 11 (+0) | 13 (+1) | 11 (+0) |

___Beast Hunter.___ The elvish veteran archer has advantage on Wisdom (Survival) checks to track beasts and on Intelligence (Nature) checks to recall information about beasts.

___Fey Ancestry.___ The elvish veteran archer has advantage on saving throws against being charmed, and magic can't put the elvish archer to sleep.

___Keen Hearing and Sight.___ The elvish veteran archer has advantage on Wisdom (Perception) checks that rely on hearing or sight.

___Magic Weapons.___ The elvish veteran archer's weapon attacks are magical.

___Stealthy Traveler.___ The elvish veteran archer can use Stealth while traveling at a normal pace.

___Surprise Attack.___ If the elvish veteran archer surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 7 (2d6) damage from the attack.

**Actions**

___Multiattack.___ The elvish veteran archer makes two melee attacks or three ranged attacks.

___Shortsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft, one target. Hit: 7 (1d6 + 4) piercing damage.

___Longbow.___ Ranged Weapon Attack: +6 to hit, range 150/600 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Volley (Recharge 6).___ The elvish archer makes one ranged attack against every enemy within 10 feet of a point it can see.

