"Duergar Spy";;;_size_: Medium humanoid (dwarf)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Deception +5, Insight +2, Investigation +5, Perception +4, Persuasion +3, Sleight of Hand +5, Stealth +7"
_speed_: "25 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "15 (studded leather)"
_damage_resistances_: "poison"
_stats_: | 10 (0) | 16 (+3) | 12 (+1) | 12 (+1) | 10 (0) | 13 (+1) |

___Source.___ tales from the yawning portal,  page 234

___Cunning Action.___ On each of its turns, the spy can use a bonus action to take the Dash, Disengage, or Hide action.

___Duergar Resilience.___ The spy has advantage on saving throws against poison, spells, and illusions, as well as to resist being charmed or paralyzed.

___Sneak Attack.___ Once per turn, the spy can deal an extra 7 (2d6) damage when it hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 feet of an ally of the spy that isn't incapacitated and the spy doesn't have disadvantage on the attack roll.

___Sunlight Sensitivity.___ While in sunlight, the spy has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The spy makes two shortsword attacks.

___Eniarge (Recharges after a Short or Long Rest).___ For 1 minute, the spy magically increases in size, along with anything it is wearing or carrying. While enlarged, the spy is Large, doubles her damage dice on Strength-based weapon attacks (included in the attacks), and makes Strength checks and Strength saving throws with advantage. If the spy lacks the room to become Large, it attains the maximum size possible in the space available.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage, or 10 (2d6 + 3) piercing damage while enlarged.

___Hand Crossbow.___ Ranged Weapon Attack: +5 to hit, range 30/120 ft ., one target. Hit: 6 (1d6 + 3) piercing damage.

___Invisibility (Recharges after a Short or Long Rest).___ The spy magically turns invisible until it attacks, deals damage, casts a spell, or uses its Enlarge, or until its concentration is broken, up to 1 hour (as if concentrating on a spell). Any equipment the spy wears or carries is invisible with it.