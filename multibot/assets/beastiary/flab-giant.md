"Flab Giant";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "Dwarvish, Giant"
_skills_: "Perception +3"
_senses_: ", passive Perception 13"
_saving_throws_: "Con +5"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "20 ft."
_hit points_: "110 (13d10 + 39)"
_armor class_: "14 (natural armor)"
_stats_: | 20 (+5) | 6 (-2) | 16 (+3) | 9 (-1) | 13 (+1) | 8 (-1) |

___Massive.___ A flab giant can't dash. Attacks that push, trip, or grapple are made with disadvantage against a flab giant.

**Actions**

___Multiattack.___ The giant makes two slam attacks. If both hit, the target is grappled (escape DC 15), and the flab giant uses its squatting pin against the target as a bonus action.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage.

___Squatting Pin.___ The flab giant squats atop the target, pinning it to the ground, where it is grappled and restrained (escape DC 17). The flab giant is free to attack another target, but the restrained creatures are released if it moves from its current space. As long as the giant does not move from the spot, it can maintain the squatting pin on up to two Medium-sized or smaller creatures. A creature suffers 9 (1d8 + 5) bludgeoning damage every time it starts its turn restrained by a squatting pin.

