"Fire Mote";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Sylvan"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +6"
_damage_immunities_: "fire"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "49 (11d6 + 11)"
_armor class_: "13 (natural armor)"
_stats_: | 17 (+3) | 16 (+3) | 12 (+1) | 13 (+1) | 11 (+0) | 12 (+1) |

___Fire Form.___ The mote can enter a hostile creature’s
space and stop there.

___Annoying.___ An enemy in the same space as a
mote must make a DC 14 Dexterity (Acrobatics)
save when it moves. On a failed save, the mote
moves with it. While in the same space as a mote,
enemies have disadvantage on Constitution
checks to maintain concentration.

**Actions**

___Multiattack.___ The mote makes three touch attacks.

___Touch.___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 7 (1d8 + 3) fire damage.
