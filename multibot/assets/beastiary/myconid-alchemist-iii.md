"Myconid Alchemist III";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Undercommon"
_skills_: "Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 13"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 12 (+1) | 10 (+0) | 8 (-1) |

___Stalk-still.___ A myconid that remains perfectly still has advantage on
Dexterity (Stealth) checks made to hide.

**Actions**

___Stone Knuckles.___ Melee weapon attack: +6 to hit. Hit: 6 (1d6 + 3)
slashing damage.

___Spore Bomb.___ Ranged weapon attack: +4 to hit, range 30/60 ft. Hit: 10
(4d4) poison damage and the target must succeed on a DC 14 Constitution
saving throw or fall asleep for 1 minute. A sleeping creature is awakened
if it takes damage or if another creature uses an action to awaken it.
