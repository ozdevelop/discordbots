"Frost Man";;;_size_: Medium elemental
_alignment_: lawful evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Survival +2"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "cold"
_damage_vulnerabilities_: "fire"
_speed_: "30 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "13 (studded leather)"
_stats_: | 10 (+0) | 12 (+1) | 12 (+1) | 10 (+0) | 11 (+0) | 11 (+0) |

**Actions**

___Morningstar.___ Melee Weapon Attack: +2 to hit, reach 5
ft., one target. Hit: 4 (1d8) piercing damage.

___Longbow.___ Ranged Weapon Attack: +3 to hit, range 150/600
ft., one target. Hit: 5 (1d8 + 1) piercing damage.

___Ice Blast (3/day).___ As a bonus action, the frost man
can use its action to remove his eye patch, blasting
everything in a 30-foot cone with a freezing mist. All
creatures in the area of the cone must make a DC 13
Dexterity saving throw, taking 14 (4d6) cold damage on a
failed save, or half as much on a successful save.
