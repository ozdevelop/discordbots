"Marut";;;_page_number_: 213
_size_: Large construct (inevitable)
_alignment_: lawful neutral
_challenge_: "25 (75,000 XP)"
_languages_: "all but rarely speaks"
_senses_: "darkvision 60 ft., passive Perception 20"
_skills_: "Insight +10, Intimidation +12, Perception +10"
_damage_immunities_: "poison"
_saving_throws_: "Int +12, Wis +10, Cha +12"
_speed_: "40 ft., fly 30 ft. (hover)"
_hit points_: "432  (32d10 + 256)"
_armor class_: "22 (natural armor)"
_condition_immunities_: "charmed, frightened, paralyzed, poisoned, unconscious"
_damage_resistances_: "thunder; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 28 (+9) | 12 (+1) | 26 (+8) | 19 (+4) | 15 (+2) | 18 (+4) |

___Immutable Form.___ The marut is immune to any spell or effect that would alter its form.

___Innate Spellcasting.___ The marut's innate spellcasting ability is Intelligence (spell save DC 20). The marut can innately cast the following spell, requiring no material components.

* At will: _plane shift _(self only)

___Legendary Resistance (3/Day).___ If the marut fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The marut has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The marut makes two slam attacks.

___Unerring Slam___ Melee Weapon Attack: automatic hit, reach 5 ft., one target. Hit: 60 force damage, and the target is pushed up to 5 feet away from the marut if it is Huge or smaller.

___Blazing Edict (Recharge 5-6)___ Arcane energy emanates from the marut's chest in a 60-foot cube. Every creature in that area takes 45 radiant damage. Each creature that takes any of this damage must succeed on a DC 20 Wisdom saving throw or be stunned until the end of the marut's next turn.

___Justify___ The marut targets up to two creatures it can see within 60 feet of it. Each target must succeed on a DC 20 Charisma saving throw or be teleported to a teleportation circle in the Hall of Concordance in Sigil. A target fails automatically if it is incapacitated. If either target is teleported in this way, the marut teleports with it to the circle.
After teleporting in this way, the marut can't use this action again until it finishes a short or long rest.
