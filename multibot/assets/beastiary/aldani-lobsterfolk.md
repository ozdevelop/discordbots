"Aldani (Lobsterfolk)";;;_size_: Medium monstrosity
_alignment_: lawful neutral
_challenge_: "1 (200 XP)"
_languages_: "Common"
_senses_: "darkvision 60ft., passive Perception 14"
_skills_: "Perception +4, Survival +4"
_speed_: "20 ft., swim 30 ft."
_hit points_: "49 (9d8 +9)"
_armor class_: "14 (natural armor)"
_stats_: | 13 (+1) | 8 (-1) | 12 (+1) | 10 (0) | 14 (+2) | 10 (0) |

___Amphibious.___ The aldani can breathe air and water.

**Actions**

___Multiattack.___ The aldani makes two attacks with its claws.

___Claw.___ Mele Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 +1) slashing damage, and the target is grappled (escape DC 11). The aldani has two claws, each of which can grapple only one target.