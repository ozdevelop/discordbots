"Tallow Golem";;;_size_: Medium construct
_alignment_: neutral
_challenge_: "6 (2,300 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "cold, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft."
_hit points_: "76 (9d8 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 9 (-1) | 18 (+4) | 5 (-3) | 11 (+0) | 4 (-3) |

___Immutable Form.___ The golem is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against
spells and other magical effects.

___Magic Weapon.___ The golem’s weapon attacks are magical.

**Actions**

___Multiattack.___ The tallow golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit:
13 (2d8 + 4) bludgeoning damage plus 7 (2d6) poison damage. If the
tallow golem rolls a natural 20, the target also suffers from discolored
skin across its body and has disadvantage on Charisma skill checks. This
further effect can be restored with magic such as lesser restoration.
