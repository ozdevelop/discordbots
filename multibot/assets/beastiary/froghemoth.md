"Froghemoth";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "10 (5,900 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +9, Stealth +5"
_saving_throws_: "Con +9, Wis +5"
_speed_: "50 ft., swim 30 ft."
_hit points_: "184 (16d12+80)"
_armor class_: "14 (natural armor)"
_damage_resistances_: "fire, lightning"
_stats_: | 23 (+6) | 13 (+1) | 20 (+5) | 2 (-4) | 12 (+1) | 5 (-3) |

___Amphibious.___ The froghemoth can breathe air and water.

___Shock Suscptibility.___ If the froghemoth takes lightning damage, it suffers several effects until the end of its next turn: its speed is halved, it takes a -2 penalty to AC and Dexterity saving throws, it can't use reactions or Multiattack, and on its turn, it can use either an action or a bonus action, not both.

**Actions**

___Multiattack.___ The froghemoth makes two attacks with its tentacles. It can also use its tongue or bite.

___Tentacle.___ Melee Weapon Attack: +10 to hit, reach 20 ft., one target. Hit: 19 (3d8+6) bludgeoning damage, and the target is grappled (escape DC 16) if it is a Huge or smaller creature. Until the grapple ends, the froghemoth can't use this tentacle on another target. The froghemoth has four tentacles.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft, one target. Hit: 22 (3d10+6) piercing damage, and the target is swallowed if it is a Medium or smaller creature. A swallowed creature is blinded and restrained, has total cover against attacks and other effects outside the froghemoth, and takes 10 (3d6) acid damage at the start of each of the froghemoth's turns.

The froghemoth's gullet can hold up to two creatures at a time. If the Froghemoth takes 20 damage or more on a single turn from a creature inside it, the Froghemoth must succeed on a DC 20 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, each of which falls prone in a space within 10 feet of the froghemoth. If the froghemoth dies, a swallowed creature is no longer restrained by it and can escape from the corpse using 10 feet of movement, exiting prone.

___Tongue.___ The Froghemoth targets one Medium or smaller creature that it can see within 20 feet of it. The target must make a DC 18 Strength saving throw. On a failed save, the target is pulled into an unoccupied space within 5 feet of the froghemoth, and the froghemoth can make a bite attack against it as a bonus action.
