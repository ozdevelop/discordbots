"Furious Zealot";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any two languages"
_skills_: "Athletics +8, Insight +7, Persuasion +6, Religion +4"
_senses_: "passive Perception 13"
_saving_throws_: "Wis +7, Cha +6"
_speed_: "30 ft."
_hit points_: "81 (7d8 + 3d12 + 30)"
_armor class_: "15 (unarmored defense, shield)"
_stats_: | 18 (+4) | 10 (+0) | 16 (+3) | 10 (+0) | 16 (+3) | 14 (+2) |

___Combat Caster.___ The zealot can perform the somatic
components of spells even when it has weapons or a
shield in one or both hands.

___Danger Sense.___ The zealot has advantage on Dexterity
saving throws.

___Rage (2/Day).___ As a bonus action, the zealot can enter a
rage for 1 minute. While raging, the zealot gains the
following benefits:
* Advantage on Strength checks and Strength saving
throws
* Melee weapon attacks deal an addition 2 damage.
* The zealot gains resistance to bludgeoning, piercing,
and slashing damage.

___Spellcasting.___ The zealot is a 6th-level spellcaster. Its
spellcasting ability is wisdom (spell save DC 15, +7 to
hit with spell attacks). The zealot has the following
cleric spells prepared:
Cantrips (at will): guidance, sacred flame

* 1st level (4 slots): _fog cloud, thunderwave_

* 2nd level (3 slots): _gust of wind, shatter_

* 3rd level (3 slots): _call lightning, sleet storm_

___Thunderbolt Strike.___ When the zealot deals damage to a
Large or smaller creature, it can also push it up to 10
feet away from it.

**Actions**

___Multiattack.___ The zealot makes three attacks with its
warhammer.

___Warhammer.___ Melee Weapon Attack: +8 to hit, reach 5
ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage, or
10 (1d8 + 6) bludgeoning damage while raging.

**Reactions**

___Storm's Wrath (3/Day).___ When a creature within 5 feet of
the zealot hits it with an attack, it can use its reaction
to cause that creature to make a DC 15 Dexterity
saving throw. The creature takes 9 (2d8) lightning
damage on a failed saving throw, or half as much
damage on a successful one.
