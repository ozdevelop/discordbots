"Gorson, the Blood Duke";;;_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "21 (33,000 XP)"
_languages_: "Abyssal, Celestial, Common, Draconic, Giant, Goblin, Infernal; telepathy 100 ft."
_saving_throws_: "Int +10, Wis +11, Cha +12"
_skills_: "Acrobatics +9, Arcana +10, Athletics +13, Perception +18, Stealth +9"
_senses_: "truesight 120 ft., passive Perception 28"
_damage_immunities_: "fire, poison; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t silvered"
_damage_resistances_: "acid, cold"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "50 ft."
_hit points_: "218 (19d10 + 114)"
_armor class_: "19 (natural armor)"
_stats_: | 22 (+6) | 15 (+2) | 23 (+6) | 16 (+3) | 18 (+4) | 20 (+5) |

___Innate Spellcasting.___ Gorson’s innate spellcasting ability is Charisma
(spell save DC 20, +12 to hit with spell attacks). It can cast the following
spells, requiring no material components.

* At will: _detect magic, charm person_

* 3/day each: _dispel magic, lightning bolt, suggestion, wall of fire_

___Keen Smell.___ Gorson has advantage on Wisdom (Perception) checks that
rely on smell.

___Legendary Resistance (3/day).___ If Gorson fails a saving throw, it can
choose to succeed instead.

___Magic Resistance.___ The devil has advantage on saving throws against
spells and other magical effects.

___Magic Weapon.___ The devil’s weapon attacks are magical.

___Pounce.___ If Gorson moves at least 20 feet straight toward a creature and then
hits it with a claw attack on the same turn, the target must succeed on a DC 21
Strength saving throw or be knocked prone. If the target is prone, Gorson can
make one claw attack or one battleaxe attack as a bonus action against it.

___Running Leap.___ With a 10-foot running start, Gorson can long jump up
to 50 feet.

**Actions**

___Multiattack.___ Gorson makes two attacks with its claw, and then two
battleaxe attacks or two slam attacks. If it uses the battleaxe two-handed,
it can’t also make claw attacks that turn.

___Battleaxe.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target.
Hit: 24 (4d8 + 6) slashing damage, or 28 (4d10 + 6) slashing damage if
used with two hands to make a melee attack.

___Slam.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 20
(4d6 + 6) bludgeoning damage.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 20
(4d6 + 6) slashing damage.

___Summon (1/day).___ Gorson summons 2d6 bearded devils, 2d4 barbed
devils, 1 bone devil, or 1 erinyes. The summoned devil appears in an
unoccupied space within 60 feet of Gorson, but can’t summon other
devils. It remains for 1 minute, until it or Gorson is slain, or until Gorson
takes an action to dismiss it.

**Legendary** Actions

Gorson can take 3 legendary actions, choosing from the options below.
Only one legendary action option can be used at a time and only at the end
of another creature’s turn. Gorson regains spent legendary actions at the
start of its turn.

___Jump.___ Gorson leaps up to 25 feet in any direction without provoking
attacks of opportunity.

___Rake (Costs 2 Actions).___ Gorson rakes its forepaws at one prone
creature within 5 feet, attacking twice with its claw attack.

___Ferocious Roar (Costs 3 Actions).___ Gorson releases a thunderous roar,
audible out to 100 feet. Allies of Gorson who can hear it gain 19 (4d6 + 5)
temporary hit points. Hostile creatures must make a DC 20 Wisdom saving
throw. On a failed saving throw, the target takes 19 (4d6 + 5) psychic
damage and is frightened for 1 minute. On a successful saving throw, the
target takes half damage and is not frightened. While frightened, the target
must drop whatever it is holding and use its action to Dash away from
Gorson. A creature who succeeds on the saving throw, or for whom the
effect ends, is immune to Gorson’s Ferocious Roar for 24 hours.
