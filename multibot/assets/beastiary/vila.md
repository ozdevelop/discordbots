"Vila";;;_size_: Medium fey
_alignment_: lawful neutral
_challenge_: "5 (1800 XP)"
_languages_: "Common, Sylvan, telepathy 60 ft. (beasts only)"
_skills_: "Animal Handling +8, Insight +5, Intimidation +6, Perception +8, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Dex +8, Con +4, Wis +5, Cha +6"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft."
_hit points_: "77 (14d8 + 14)"
_armor class_: "15"
_stats_: | 12 (+1) | 20 (+5) | 13 (+1) | 11 (+0) | 14 (+2) | 16 (+3) |

___Dance of the Luckless (1/Day).___ Vila who dance for one hour create a fairy ring of small gray mushrooms. The ring lasts seven days and has a 50-foot diameter per dancing vila. Non.vila who fall asleep (including magical sleep) inside the ring have disadvantage on skill checks for 24 hours from the time they awaken.

___Forest Quickness.___ While in forest surroundings, a vila receives a +4 bonus on initiative checks.

___Forest Meld.___ A vila can meld into any tree in her forest for as long as she wishes, similar to the meld into stone spell.

___Innate Spellcasting.___ The vila's innate spellcasting ability is Charisma (spell save DC 14). She can innately cast the following spells, requiring no material components:

3/day: sleep

1/week: control weather

**Actions**

___Multiattack.___ A vila makes two shortsword attacks or two shortbow attacks.

___+1 Shortsword.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 9 (1d6 + 6) piercing damage.

___+1 Shortbow.___ Ranged Weapon Attack: +9 to hit, range 80/320 ft., one target. Hit: 9 (1d6 + 6) piercing damage.

___Fascinate (1/Day).___ When the vila sings, all those within 60 feet of her and who can hear her must make a successful DC 14 Charisma saving throw or be stunned for 1d4 rounds. Those who succeed on the saving throw are immune to that vila's singing for 24 hours.

___Forest Song (1/Day).___ The vila magically calls 2d6 wolves or 2 wampus cats. The called creatures arrive in 1d4 rounds, acting as allies of the vila and obeying its spoken commands. The beasts remain for 1 hour, until the vila dies, or until the vila dismisses them as a bonus action.

