"Gearforged Templar";;;_size_: Medium humanoid
_alignment_: lawful neutral
_challenge_: "6 (2300 XP)"
_languages_: "Common"
_senses_: ", passive Perception 13"
_saving_throws_: "Dex +2, Con +5"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, frightened, exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "71 (11d8 + 22)"
_armor class_: "18 (plate armor)"
_stats_: | 20 (+5) | 9 (-1) | 15 (+2) | 12 (+1) | 16 (+3) | 10 (+0) |

___Defensive Zone.___ The gearforged templar can make an opportunity attack when a creature enters its reach.

___Onslaught.___ As a bonus action, the gearforged can make a Shove attack.

**Actions**

___Multiattack.___ The gearforged templar makes three attacks with its glaive.

___Glaive.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 10 (1d10 + 5) slashing damage.

___Javelin.___ Ranged Weapon Attack: +8 to hit, range 30/120 ft., one target. Hit: 8 (1d6 + 5) piercing damage.

___Whirlwind (recharge 5-6).___ The gearforged templar whirls its glaive in a great arc. Every creature within 10 feet of the gearforged takes 16 (3d10) slashing damage, or half damage with a successful DC 16 Dexterity saving throw.

**Reactions**

___Parry.___ The gearforged templar raises its AC by 3 against one melee attack that would hit it. To do so, the gearforged must be able to see the attacker and must be wielding a melee weapon.

