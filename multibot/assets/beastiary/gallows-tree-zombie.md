"Gallows Tree Zombie";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "Common, but cannot speak, telepathy 100 ft. with gallows tree"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "fire"
_condition_immunities_: "poisoned, frightened, prone, stunned, unconscious"
_speed_: "20 ft."
_hit points_: "52 (7d8 + 21)"
_armor class_: "11 (natural armor)"
_stats_: | 19 (+4) | 10 (+0) | 16 (+3) | 4 (-3) | 10 (+0) | 1 (-5) |

___Regeneration.___ The gallows tree zombie regenerates 5 hit points at the
start of its turn if it has at least 1 hit point.

___Tether-Vine.___ A gallows tree zombie is connected to the gallows tree that
created it by a long, sinewy vine. This vine can be lengthened to allow the
zombie to move up to 100 feet away from the tree. The vine is AC 12 and
has 10 hit points. Harming the vine deals no damage to the gallows tree
zombie or the gallows tree, but if severed, does prevent the zombie from
regenerating any health at the start of its turn.

**Actions**

___Multiattack.___ The gallows tree zombie makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) bludgeoning damage.

___Spore Cloud (Recharge 6).___ A gallows tree zombie can breathe a cloud
of poisonous, greenish spores at the space directly in front of it in a 5-foot
cube. A creature caught in the cloud must succeed on a DC 13 Constitution
saving throw or be poisoned for 1 minute. Until this poison ends, the target
is slowed (as the slow spell). The target can repeat the saving throw at the
end of each of its turns, ending the effect on itself on a success.
