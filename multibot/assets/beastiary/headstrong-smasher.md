"Headstrong Smasher";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Athletics +5, Survival +3"
_senses_: "passive Perception 11"
_saving_throws_: "Str +5, Con +5"
_speed_: "30 ft."
_hit points_: "26 (3d12 + 9)"
_armor class_: "13 (unarmored defense)"
_stats_: | 16 (+3) | 12 (+1) | 16 (+3) | 8 (-1) | 12 (+1) | 10 (+0) |

___Danger Sense.___ The smasher has advantage on
Dexterity saving throws.

___Rage (2/Day).___ As a bonus action, the smasher can
enter a rage for 1 minute. While raging, the
smasher gains the following benefits:
* Advantage on Strength checks and Strength
saving throws
* Melee weapon attacks deal an addition 2
damage.
* The smasher gains resistance to bludgeoning,
piercing, and slashing damage.

___Unarmored Defense.___ While not wearing armor, the
smasher's AC includes its Constitution modifier.

**Actions**

___Maul.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 10 (2d6 + 3) bludgeoning damage,
or 12 (2d6 + 5) bludgeoning damage if raging.
