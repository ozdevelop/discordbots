"Gnarljak";;;_size_: Small construct
_alignment_: unaligned
_challenge_: "6 (2300 XP)"
_languages_: "--"
_skills_: "Perception +5, Stealth +9"
_senses_: "blindsight 30 ft., passive Perception 15"
_saving_throws_: "Dex +9"
_damage_immunities_: "necrotic, poison, psychic"
_damage_resistances_: "cold, acid, fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "63 (14d6 + 14)"
_armor class_: "16"
_stats_: | 13 (+1) | 22 (+6) | 11 (+0) | 2 (-4) | 14 (+2) | 1 (-5) |

**Actions**

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 15 (2d8 + 6) piercing damage, and the target must succeed on a DC 16 Dexterity saving throw or fall prone.

___Gnaw.___ When a gnarljak knocks a Medium or smaller target prone, it immediately makes three additional bite attacks against the same target and can move 5 feet, all as a bonus action.

