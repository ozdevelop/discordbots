"Giant Wolf Spider";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +3, Stealth +7"
_speed_: "40 ft., climb 40 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "13"
_stats_: | 12 (+1) | 16 (+3) | 13 (+1) | 3 (-4) | 12 (+1) | 4 (-3) |

___Spider Climb.___ The spider can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Web Sense.___ While in contact with a web, the spider knows the exact location of any other creature in contact with the same web.

___Web Walker.___ The spider ignores movement restrictions caused by webbing.

**Actions**

___Bite.___ Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 4 (1d6 + 1) piercing damage, and the target must make a DC 11 Constitution saving throw, taking 7 (2d6) poison damage on a failed save, or half as much damage on a successful one. If the poison damage reduces the target to 0 hit points, the target is stable but poisoned for 1 hour, even after regaining hit points, and is paralyzed while poisoned in this way.