"Orthon";;;_page_number_: 169
_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "10 (5900 XP)"
_languages_: "Common, Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft., truesight 30 ft., passive Perception 20"
_skills_: "Perception +10, Stealth +11, Survival +10"
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +7, Con +9, Wis +6"
_speed_: "30 ft., climb 30 ft."
_hit points_: "105  (10d10 + 50)"
_armor class_: "17 (half plate)"
_condition_immunities_: "charmed, exhaustion, poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 22 (+6) | 16 (+3) | 21 (+5) | 15 (+2) | 15 (+2) | 16 (+3) |

___Invisibility Field.___ The orthon can use a bonus action to become invisible. Any equipment the orthon wears or carries is also invisible as long as the equipment is on its person. This invisibility ends immediately after the orthon makes an attack roll or is hit by an attack.

___Magic Resistance.___ The orthon has advantage on saving throws against spells and other magical effects.

**Actions**

___Infernal Dagger___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 11 (2d4 + 6) slashing damage, and the target must make a DC 17 Constitution saving throw, taking 22 (4d10) poison damage on a failed save, or half as much damage on a successful one. On a failure, the target is poisoned for 1 minute. The poisoned target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Brass Crossbow___ Ranged Weapon Attack: +7 to hit, range 100/400 ft., one target. Hit: 14 (2d10 + 3) piercing damage, plus one of the following effects:

1. ___Acid___ The target must make a DC 17 Constitution saving throw, taking an additional 17 (5d6) acid damage on a failed save, or half as much damage on a successful one.

2. ___Blindness (1/Day)___ The target takes 5 (1d10) radiant damage. In addition, the target and all other creatures within 20 feet of it must each make a successful DC 17 Dexterity saving throw or be blinded until the end of the orthon's next turn.

3. ___Concussion___ The target and each creature within 20 feet of it must make a DC 17 Constitution saving throw, taking 13 (2d12) thunder damage on a failed save, or half as much damage on a successful one.

4. ___Entanglement___ The target must make a successful DC 17 Dexterity saving throw or be restrained for 1 hour by strands of sticky webbing. A restrained creature can escape by using an action to make a successful DC 17 Dexterity or Strength check. Any creature other than an orthon that touches the restrained creature must make a successful DC 17 Dexterity saving throw or become similarly restrained.

5. ___Paralysis (1/Day)___ The target takes 22 (4d10) lightning damage and must make a successful DC 17 Constitution saving throw or be paralyzed for 1 minute. The paralyzed target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

6. ___Tracking___ For the next 24 hours, the orthon knows the direction and distance to the target, as long as it's on the same plane of existence. If the target is on a different plane, the orthon knows which one, but not the exact location there.
