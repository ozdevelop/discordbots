"Frost Dwarf";;;_size_: Medium humanoid (dwarf)
_alignment_: chaotic neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Dwarven"
_skills_: "Perception +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_resistances_: "cold"
_speed_: "25 ft."
_hit points_: "13 (2d8 + 4)"
_armor class_: "12 (leather)"
_stats_: | 13 (+1) | 12 (+1) | 14 (+2) | 10 (+0) | 11 (+0) | 10 (+0) |

**Actions**

___Battleaxe.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) slashing damage.

___Frost Breath.___ The frost dwarf exhales a 15-foot cone of freezing air.
Each creature in the area must make a DC 12 Constitution saving throw,
taking 7 (2d6) cold damage on a failed saving throw, or half as much
damage on a successful one.
