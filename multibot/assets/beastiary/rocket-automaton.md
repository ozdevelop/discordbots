"Rocket Automaton";;;_size_: Small construct
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "understands one language known by its creator but can't speak"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "10 ft., fly 60 ft."
_hit points_: "9 (2d6 + 2)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (+0) | 16 (+3) | 13 (+1) | 3 (-4) | 14 (+2) | 1 (-5) |

___Animated Explosive.___ When it takes fire damage, or as
an action on its turn, the automaton's fuse lights. At
the start of each of the automaton's turns, roll a d4.
On a result of 1, the automaton explodes in a 15-
foot radius fireball at the start of its next turn,
forcing each creature in the area to make a DC 12
Dexterity saving throw, taking 4 (1d8) fire damage
plus 4 (1d8) bludgeoning damage on a failed save, or
half as much on a successful one.

If the automaton is killed, the fuse remains lit,
potentially setting off the explosion after its death. If
another creature hits the automaton with an
unarmed strike attack, the creature can choose to
snuff out the fuse instead of dealing damage.

___Flyby.___ The automaton doesn't provoke opportunity
attacks when it flies out of an enemy's reach.

___Immutable Form.___ The automaton is immune to any spell or
effect that would alter its form.

___Siege Monster.___ The automaton deals double damage to
objects and structures.

___Rocket Flight.___ The automaton can only use its fly
speed once the rocket on its back is ignited. Once it
lifts off, the automaton must use at least half of its
flying speed on each of its turns, unless something
prevents it from doing so.

1 minute after its rocket has been ignited, roll a
d10 at the end of each of the automaton's turns. On
a result of 1, the rocket's fuel is spent, and the
automaton immediately begins to fall.

**Actions**

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 5 (1d4 + 3) bludgeoning damage.
