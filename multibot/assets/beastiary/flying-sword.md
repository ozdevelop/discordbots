"Flying Sword";;;_size_: Small construct
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_damage_immunities_: "poison, psychic"
_saving_throws_: "Dex +4"
_speed_: "0 ft., fly 50 ft. It can hover."
_hit points_: "17 (5d6)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, frightened, paralyzed, petrified, poisoned"
_stats_: | 12 (+1) | 15 (+2) | 11 (0) | 1 (-5) | 5 (-3) | 1 (-5) |

___Antimagic Susceptibility.___ The sword is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the sword must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute.

___False Appearance.___ While the sword remains motionless and isn't flying, it is indistinguishable from a normal sword.

**Actions**

___Longsword.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) slashing damage.