"Plesiosaurus";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_skills_: "Perception +3, Stealth +4"
_speed_: "20 ft., swim 40 ft."
_hit points_: "68 (8d10+24)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 2 (-4) | 12 (+1) | 5 (-3) |

___Hold Breath.___ The plesiosaurus can hold its breath for 1 hour.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 14 (3d6 + 4) piercing damage.