"Skulleton";;;_size_: Tiny undead
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_skills_: "Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_immunities_: "poison"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "fly 10 ft."
_hit points_: "21 (6d4 + 6)"
_armor class_: "12 (natural armor)"
_stats_: | 6 (-2) | 10 (+0) | 13 (+1) | 10 (+0) | 12 (+1) | 14 (+2) |

**Actions**

___Bite.___ Melee Weapon Attack: +2 to hit, reach 0 ft., one target.
Hit: 3 (1d6) piercing damage. If the target is a creature, it
must succeed on a DC 11 Constitution saving throw against
disease or be poisoned until the disease is cured. For every
24 hours that elapse, the target must repeat the saving
throw, reducing its hit point maximum by 5 (1d10) on
a failure. The disease is cured on a success. The target
dies if the disease reduces its hit point maximum to 0. This
reduction to the target’s hit point maximum lasts until the
disease is cured.

___Dust (2/day).___ The skulleton can use its crumbled
remains to attack any creature that comes within 10
feet of it. The skulleton billows forth a cloud of dust that surrounds it
in a 10-foot radius. All creatures in this area must succeed on a DC 12
Constitution saving throw or be poisoned for 1 minute. The target can
repeat the saving throw at the end of each of its turns, ending the effect on
itself on a success.
