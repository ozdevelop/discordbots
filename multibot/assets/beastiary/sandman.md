"Sandman";;;_size_: Medium celestial
_alignment_: chaotic neutral
_challenge_: "5 (1800 XP)"
_languages_: "Common, Celestial, Giant, Infernal, Umbral"
_senses_: "truesight 60 ft., passive Perception 12"
_saving_throws_: "Dex +7, Cha +7"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned, unconscious"
_speed_: "40 ft."
_hit points_: "82 (11d8 + 33)"
_armor class_: "14"
_stats_: | 11 (+0) | 19 (+4) | 16 (+3) | 13 (+1) | 14 (+2) | 19 (+4) |

___Eye-Closer's Curse.___ If a sandman obtains a critical hit or successful surprise attack against an opponent, its talons scratch a rune onto the target's eyeballs that snaps their eyelids shut, leaving them blinded. This effect can be ended with greater restoration, remove curse, or comparable magic.

___Innate Spellcasting.___ The sandman's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

At will: darkness, minor illusion, plane shift (at night only), phantom steed, prestidigitation, sleep (11d8)

3/day each: hypnotic pattern, major image

1/day each: dream, phantasmal killer (5d10)

___Stuff of Dreams.___ Made partially from dreams and imagination, a sandman takes only half damage from critical hits and from sneak attacks. All of the attack's damage is halved, not just bonus damage.

___Surprise Attack.___ If the sandman hits a surprised creature during the first round of combat, the target takes 14 (4d6) extra damage from the attack.

**Actions**

___Multiattack.___ The sandman makes two claw attacks.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

