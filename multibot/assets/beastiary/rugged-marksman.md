"Rugged Marksman";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Animal Handling +4, Investigation +3, Nature +3, Survival +4"
_senses_: "passive Perception 12"
_saving_throws_: "Str +4, Dex +5"
_speed_: "30 ft."
_hit points_: "26 (4d10 + 4)"
_armor class_: "15 (chain shirt)"
_stats_: | 14 (+2) | 16 (+3) | 13 (+1) | 12 (+1) | 15 (+2) | 8 (-1) |

___Archery Fighting Style.___ The marksman gains a +2
bonus to attack rolls it makes with ranged weapons.

___Colossus Slayer.___ Once per turn, when the marksman
hits a creature with an attack, the creature takes an
additional 1d8 damage if it's below its hit points
maximum (included in the attack).

___Spellcasting.___ The marksman is a 3th-level
spellcaster. Its spellcasting ability is Wisdom (spell
save DC 12, +4 to hit with spell attacks). The
marksman has the following ranger spells prepared:

* 1st level (3 slots): _alarm, hail of thorns, hunter's mark_

**Actions**

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 6 (1d6 + 3) piercing damage
plus 4 (1d8) piercing damage if the target is a
creature below its hit points maximum.

___Longbow.___ Ranged Weapon Attack: +7 to hit, range
150/600 ft., one target. Hit: 7 (1d8 + 3) piercing
damage plus 4 (1d8) piercing damage if the target
is a creature below its hit points maximum.
