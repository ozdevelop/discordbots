"Kobold Dragon Shaman";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft., passive Perception 8"
_speed_: "30 ft."
_hit points_: "17 (5d6)"
_armor class_: "11"
_stats_: | 8 (-1) | 12 (+1) | 10 (+0) | 8 (-1) | 7 (-2) | 12 (+1) |

___Sunlight Sensitivity.___ While in sunlight, the kobold
has disadvantage on attack rolls, as well as on
Wisdom (Perception) checks that rely on sight.

___Pack Tactics.___ The kobold has advantage on an attack
roll against a creature if at least one of the kobold’s
allies is within 5 feet of the creature and the ally
isn’t incapacitated.

___Innate Spellcasting.___ The kobold’s innate spellcasting
ability is Charisma (spell save DC 11, +3 to hit with
spell attacks). It can innately cast a number of
spells, requiring no material components. The type
of dragon the kobold worships determines the
spells it has available, as shown below.
* ___Black,___ 1/day each: _acid splash, fog cloud_
* ___Blue,___ 1/day each: _shocking grasp, witch bolt_
* ___Green,___ 1/day each: _poison spray, ray of sickness_
* ___Red,___ 1/day each: _firebolt, burning hands_
* ___White,___ 1/day each: _ray of frost, ice knife_

**Actions**

___Dagger.___ Melee Weapon Attack: +3 to hit, reach 5ft.,
one target. Hit: 3 (1d4 + 1) slashing damage.

___Sling.___ Ranged Weapon Attack: +3 to hit, range
30/120 ft., one target. Hit: 3 (1d4 + 1)
bludgeoning damage.
