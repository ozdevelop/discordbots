"Piercer";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 30 ft., darkvision 60 ft."
_skills_: "Stealth +5"
_speed_: "5 ft., climb 5 ft."
_hit points_: "22 (3d8+9)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (0) | 13 (+1) | 16 (+3) | 1 (-5) | 7 (-2) | 3 (-4) |

___False Appearance.___ While the piercer remains motionless on the ceiling, it is indistinguishable from a normal stalactite.

___Spider Climb.___ The piercer can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Drop.___ Melee Weapon Attack: +3 to hit, one creature directly underneath the piercer. Hit: 3 (1d6) piercing damage per 10 feet fallen, up to 21 (6d6). Miss: The piercer takes half the normal falling damage for the distance fallen.