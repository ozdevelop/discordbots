"Empty Cloak";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "understands Elvish and Umbral but can't speak"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Dex +4, Con +2"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "0 ft., fly 40 ft."
_hit points_: "45 (10d8)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 10 (+0) | 10 (+0) | 10 (+0) | 1 (-5) |

___Diligent Sentinel.___ Empty cloaks are designed to watch for intruders. They gain advantage on Wisdom (Perception) checks.

___Shadow Construction.___ Empty cloaks are designed with a delicate shadow construction. They burst into pieces, then dissipate into shadow, on a critical hit.

___Wrapping Embrace.___ Empty cloaks can share the same space as one Medium or smaller creature. The empty cloak has advantage on attack rolls against any creature in the same space with it.

**Actions**

___Razor Cloak.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage.

___Shadow Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft, one target. Hit: 6 (1d4 + 4) bludgeoning damage.

___Shadow Snare.___ Ranged Weapon Attack: +4 to hit, range 20/60 ft., one target. Hit: Large or smaller creatures are restrained. To escape, the restrained creature or an adjacent ally must use an action to make a successful DC 14 Strength check. The shadow snare has 15 hit points and AC 12.

