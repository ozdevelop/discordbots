"Trapper";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "blindsight 30 ft., darkvision 60 ft."
_skills_: "Stealth +2"
_speed_: "10 ft., climb 10 ft."
_hit points_: "85 (10d10+30)"
_armor class_: "13 (natural armor)"
_stats_: | 17 (+3) | 10 (0) | 17 (+3) | 2 (-4) | 13 (+1) | 4 (-3) |

___Fake Appearance.___ While the trapper is attached to a ceiling, floor, or wall and remains motionless, it is almost indistinguishable from an ordinary section of ceiling, floor, or wall. A creature that can see it and succeeds on a DC 20 Intelligence (investigation) or Intelligence (Nature) check can discern its presence.

___Spider Climb.___ The trapper can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Smother.___ One Large or smaller creature within 5 feet of the trapper must succeed on a DC 14 Dexterity saving throw or be grappled (escape DC 14). Until the grapple ends, the target takes 17 (4d6+3) bludgeoning damage plus 3 (1d6) acid damage at the start of each of its turns. While grappled in this way, the target is restrained, blinded, and at risk of suffocating. The trapper can smother only one creature at a time.