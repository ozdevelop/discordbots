"Grung";;;_size_: Small humanoid (grung)
_alignment_: lawful evil
_challenge_: "1/4 (50 XP)"
_languages_: "Grung"
_skills_: "Athletics +2, Perception +2, Stealth +4, Survival +2"
_damage_immunities_: "poison"
_saving_throws_: "Dex +4"
_speed_: "25 ft., climb 25 ft."
_hit points_: "11 (2d6+4)"
_armor class_: "12"
_condition_immunities_: "poisoned"
_stats_: | 7 (-2) | 14 (+2) | 15 (+2) | 10 (0) | 11 (0) | 10 (0) |

___Amphibious.___ The grung can breathe air and water.

___Poisonous Skin.___ Any creature that grapples the grung or otherwise comes into direct contact with the grung's skin must succeed on a DC 12 Constitution saving throw or become poisoned for 1 minute. A poisoned creature no longer in direct contact with the grung can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Standing Leap.___ The grung's long jump is up to 25 feet and its high jump is up to 15 feet, with or without a running start.

___Variant: Grung Poison.___ Grung poison loses its potency 1 minute after being removed from a grung. A similar breakdown occurs if the grung dies.

>A creature poisoned by a grung can suffer an additional effect that varies depending on the grung's skin color. This effect lasts until the creature is no longer poisoned by the grung.

Green. The poisoned creature can't move except to climb or make standing jumps. If the creature is flying, it can't take any actions or reactions unless it lands.

Blue. The poisoned creature must shout loudly or otherwise make a loud noise at the start and end of its turn.

Purple. The poisoned creature feels a desperate need to soak itself in liquid or mud. It can't take actions or move except to do so or to reach a body of liquid or mud.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft, one target. Hit: 4 (1d4+2) piercing damage, and the target must succeed on a DC 12 Constitution saving throw or take 5 (2d4) poison damage.