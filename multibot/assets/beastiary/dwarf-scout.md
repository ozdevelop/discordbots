"Dwarf Scout";;;_page_number_: 999
_size_: Medium humanoid (dwarf)
_alignment_: any alignment
_challenge_: "1/2 (100 XP)"
_languages_: "any one language (usually Common)"
_senses_: "darkvision, passive Perception 15"
_skills_: "Nature +4, Perception +5, Stealth +6, Survival +5"
_speed_: "30 ft."
_hit points_: "16  (3d8 + 3)"
_armor class_: "13 (leather armor)"
_damage_resistances_: "poison"
_stats_: | 11 (0) | 14 (+2) | 12 (+1) | 11 (0) | 13 (+1) | 11 (0) |

___Dwarven Resilience.___ The dwarf has advantage on saving throws against poison and has resistance to poison damage

___Keen Hearing and Sight.___ The scout has advantage on Wisdom (Perception) checks that rely on hearing or sight.

**Actions**

___Multiattack___ The scout makes two melee attacks or two ranged attacks.

___Shortsword___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Longbow___ Ranged Weapon Attack: +4 to hit, ranged 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.
