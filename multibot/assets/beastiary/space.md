"Space";;;_size_: Medium construct
_alignment_: lawful neutral
_challenge_: "5 (1,800 XP)"
_languages_: "all those of the creature who summoned it"
_senses_: "truesight 60 ft., passive Perception 16"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 13 (+1) | 16 (+3) | 14 (+2) |

___The Law of Motion.___ No enemies within 60 feet
can gain the benefit of magical movement.

___Chaos Vulnerability.___ The Inexorables have disadvantage
on all saving throws against spells.

___Inexorable.___ The Inexorables are immune to any
effects that would slow them or deny them
actions or movement.

**Actions**

___Multiattack.___ Space makes three slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 12 (2d8 + 3) bludgeoning damage.
