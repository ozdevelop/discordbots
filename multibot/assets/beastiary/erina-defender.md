"Erina Defender";;;_size_: Small humanoid
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Erina"
_skills_: "Athletics +4, Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_resistances_: "poison"
_speed_: "20 ft., burrow 20 ft."
_hit points_: "44 (8d6 + 16)"
_armor class_: "15 (chain shirt)"
_stats_: | 11 (+0) | 14 (+2) | 14 (+2) | 13 (+1) | 12 (+1) | 11 (+0) |

___Keen Smell.___ The erina has advantage on Wisdom (Perception) checks that rely on smell.

___Hardy.___ The erina has advantage on saving throws against poison.

___Spines.___ An enemy who hits the erina with a melee attack while within 5 feet of it takes 5 (2d4) piercing damage.

**Actions**

___Multiattack.___ The erina defender makes two attacks.

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2)

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

**Reactions**

___Protect.___ The erina imposes disadvantage on an attack roll made against an ally within 5 feet of the erina defender.

