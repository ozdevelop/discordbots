"Elusa Hound";;;_size_: Medium monstrosity
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "understands Common but can't speak"
_skills_: "Athletics +4, Perception +3, Survival +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "50 ft."
_hit points_: "26 (4d8 + 8)"
_armor class_: "12"
_stats_: | 15 (+2) | 15 (+2) | 15 (+2) | 6 (-2) | 12 (+1) | 8 (-1) |

___Arcane Sight.___ Elusa hounds can perceive magical auras from active
spells, magical effects, and magic items within a radius of 120 feet. Once
an elusa hound has seen one of the above, it has advantage on Wisdom
(Survival) checks to track the origin of the effect for 24 hours.

___Keen Hearing and Smell.___ The elusa hound has advantage on Wisdom
(Perception) checks that rely on hearing or smell.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage. If the target is a creature, it must succeed on a DC
11 Strength saving throw or be knocked prone and grappled. The elusa
hound can only grapple one creature at a time.
