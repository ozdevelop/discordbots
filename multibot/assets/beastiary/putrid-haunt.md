"Putrid Haunt";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning and piercing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "44 (8d8 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 17 (+3) | 8 (-1) | 13 (+1) | 6 (-2) | 11 (+0) | 6 (-2) |

___Dead Still.___ Treat a putrid haunt as invisible while it's buried in swamp muck.

___Swamp Shamble.___ Putrid haunts suffer no movement penalties in marshy terrain.

**Actions**

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) bludgeoning damage.

___Vomit Leeches (Recharge 6).___ A putrid haunt can vomit forth the contents of its stomach onto a target within 5 feet. Along with the bile and mud from its stomach, this includes 2d6 undead leeches that attach to the target. A creature takes 1 necrotic damage per leech on it at the start of the creature's turn, and the putrid haunt gains the same number of temporary hit points. As an action, a creature can remove or destroy 1d3 leeches from itself or an adjacent ally.

