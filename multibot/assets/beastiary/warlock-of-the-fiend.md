"Warlock of the Fiend";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "7 (2,900 XP)"
_languages_: "any two languages (usually Abyssal or Infernal)"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +4, Deception +7, Persuasion +7, Religion +4"
_saving_throws_: "Wis +4, Cha +7"
_speed_: "30 ft."
_hit points_: "78 (12d8+24)"
_armor class_: "12 (15 with mage armor)"
_damage_resistances_: "slashing damage from nonmagical attacks not made with silvered weapons"
_stats_: | 10 (0) | 14 (+2) | 15 (+2) | 12 (+1) | 12 (+1) | 18 (+4) |

___Innate Spellcasting.___ The warlock's innate spellcasting ability is Charisma. It can innately cast the following spells (spell save DC 15), requiring no material components:

* At will: _alter self, false life, levitate _(self only)_, mage armor _(self only)_, silent image_

* 1/day each: _feeblemind, finger of death, plane shift_

___Spellcasting.___ The warlock is a l7th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _eldritch blast, fire bolt, friends, mage hand, minor illusion, prestidigitation, shocking grasp_

* 1st-5th level (4 5th-level slots): _banishment, burning hands, flame strike, hellish rebuke, magic circle, scorching ray, scrying, stinking cloud, suggestion, wall of fire_

___Dark One's Own Luck (Recharges after a Short or Long Rest).___ When the warlock makes an ability check or saving throw, it can add a d10 to the roll. It can do this after the roll is made but before any of the roll's effects occur.

**Actions**

___Mace.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d6) bludgeoning damage plus 10 (3d6) fire damage.
