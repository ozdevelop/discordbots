"Medium Fire Crab";;;_size_: Medium elemental
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "understands Ignan but can't speak"
_skills_: "Athletics +7"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 12"
_damage_immunities_: "fire"
_damage_vulnerabilities_: "cold"
_condition_immunities_: "charmed"
_speed_: "30 ft., swim 30 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 16 (+3) | 2 (-4) | 14 (+2) | 2 (-4) |

___Heated Body.___ A creature that touches the fire crab or hits it with a melee
attack while within 5 feet of it takes 7 (2d6) fire damage.

**Actions**

___Multiattack.___ The medium fire crab
makes two claw attacks

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage plus 7 (2d6) fire damage. If both claw attacks hit in the same turn and the target is Large size or smaller, the target is grappled (escape DC 15). While grappled, the target is restrained and takes 7 (2d6) fire damage at the start of its turn. The fire crab can only grapple one target at a time and cannot perform a claw attack while grappling a target.

___Constrict.___ If a target is grappled, the medium fire crab squeezes it and the target
takes 8 (1d8 + 4) bludgeoning damage and 7 (2d6) fire damage.
