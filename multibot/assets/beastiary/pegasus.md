"Pegasus";;;_size_: Large celestial
_alignment_: chaotic good
_challenge_: "2 (450 XP)"
_languages_: "understands Celestial, Common, Elvish, and Sylvan but can't speak"
_skills_: "Perception +6"
_saving_throws_: "Dex +4, Wis +4, Cha +3"
_speed_: "60 ft., fly 90 ft."
_hit points_: "59 (7d10+21)"
_armor class_: "12"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 10 (0) | 15 (+2) | 13 (+1) |

**Actions**

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.