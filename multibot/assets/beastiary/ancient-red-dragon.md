"Ancient Red Dragon";;;_size_: Gargantuan dragon
_alignment_: chaotic evil
_challenge_: "24 (62,000 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 60 ft., darkvision 120 ft."
_skills_: "Perception +16, Stealth +7"
_damage_immunities_: "fire"
_saving_throws_: "Dex +7, Con +16, Wis +9, Cha +13"
_speed_: "40 ft., climb 40 ft., fly 80 ft."
_hit points_: "546 (28d20+252)"
_armor class_: "22 (natural armor)"
_stats_: | 30 (+10) | 10 (0) | 29 (+9) | 18 (+4) | 15 (+2) | 23 (+6) |

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +17 to hit, reach 15 ft., one target. Hit: 21 (2d10 + 10) piercing damage plus 14 (4d6) fire damage.

___Claw.___ Melee Weapon Attack: +17 to hit, reach 10 ft., one target. Hit: 17 (2d6 + 10) slashing damage.

___Tail.___ Melee Weapon Attack: +17 to hit, reach 20 ft., one target. Hit: 19 (2d8 + 10) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 21 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours.

___Fire Breath (Recharge 5-6).___ The dragon exhales fire in a 90-foot cone. Each creature in that area must make a DC 24 Dexterity saving throw, taking 91 (26d6) fire damage on a failed save, or half as much damage on a successful one.

**Legendary** Actions

The ancient red dragon can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The ancient red dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 25 Dexterity saving throw or take 17 (2d6 + 10) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.