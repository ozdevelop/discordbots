"Dark Cadejo";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common"
_skills_: "Deception +4, Perception +8, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 18"
_speed_: "30 ft."
_hit points_: "99 (18d8 + 18)"
_armor class_: "18 (Natural Armor)"
_stats_: | 10 (+0) | 18 (+4) | 12 (+1) | 10 (+0) | 18 (+4) | 10 (+0) |

___Nullification.___ While it is within 120 feet of a light
cadejo, a dark cadejo makes ability checks, attacks,
and saving throws at disadvantage. Additionally, the
dark cadejo’s Paralysis and Stench features do not function.
However, any attacks, ability checks, and saving throws resulting from
direct combat with a light cadejo are made without disadvantage.

___Paralysis.___ When a creature that can see the dark cadejo’s eyes starts its
turn within 30 feet of the dark cadejo, the dark cadejo can force it to make
a DC 14 Constitution saving throw if the dark cadejo isn’t incapacitated
and can see the creature. On a failure, the creature is paralyzed for as long
as the dark cadejo maintains eye contact. A paralysed creature can repeat
the saving at the end of each of its turns, ending the effect on a success.
This ability does not function for the dark cadejo while it is within 120
feet of a light cadejo.

Unless surprised, a creature can avert its eyes to avoid the saving
throw at the start of its turn. If the creature does so, it can’t see the dark
cadejo until the start of its next turn when it can avert its eyes again. If
the creature looks at the dark cadejo in the meantime, it must immediately
make the save.

___Stench.___ The dark cadejo emits a sickening stench, affecting all creatures
near it. Any creature that begins its turn within 10 feet of the dark cadejo
must make a DC 14 Constitution save or be poisoned until the start of its
next turn.

This ability does not function for the dark cadejo while it is within 120
feet of a light cadejo.

**Actions**

___Multiattack.___ The dark cadejo makes three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage.
