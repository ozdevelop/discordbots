"Decaying Zoblin";;;_size_: Small undead (goblinoid)
_alignment_: neutral evil
_challenge_: "1/8 (25 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "25 ft."
_hit points_: "10 (3d6)"
_armor class_: "8 (leather armor)"
_stats_: | 10 (+0) | 5 (-3) | 10 (+0) | 5 (-3) | 7 (-2) | 4 (-3) |

___Undead Fortitude.___ If damage reduces the zoblin to 0
hit points, it must make a Constitution saving
throw with a DC of 10 + the damage taken, unless
the damage is radiant or from a critical hit. On a
success, the zoblin drops to 1 hit point instead.

___Putrid.___ A creature that touches the zoblin or hits it
with a melee attack while within 5 feet of it takes 1
necrotic damage.

**Actions**

___Claw.___ Melee Weapon Attack: +2 to hit, reach 5ft.,
one target. Hit: 2 (1d4) slashing damage.
