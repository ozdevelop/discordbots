"Roc";;;_size_: Gargantuan monstrosity
_alignment_: unaligned
_challenge_: "11 (7,200 XP)"
_skills_: "Perception +4"
_saving_throws_: "Dex +4, Con +9, Wis +4, Cha +3"
_speed_: "20 ft., fly 120 ft."
_hit points_: "248 (16d20+80)"
_armor class_: "15 (natural armor)"
_stats_: | 28 (+9) | 10 (0) | 20 (+5) | 3 (-4) | 10 (0) | 9 (-1) |

___Keen Sight.___ The roc has advantage on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The roc makes two attacks: one with its beak and one with its talons.

___Beak.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 27 (4d8 + 9) piercing damage.

___Talons.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 23 (4d6 + 9) slashing damage, and the target is grappled (escape DC 19). Until this grapple ends, the target is restrained, and the roc can't use its talons on another target.