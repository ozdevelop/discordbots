"Wyrmling Wind Dragon";;;_size_: Medium dragon
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Draconic, Primordial"
_skills_: "Perception +4, Stealth +6"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +6, Con +4, Wis +2, Cha +4"
_damage_immunities_: "lightning"
_condition_immunities_: "charmed, exhausted, paralyzed"
_speed_: "40 ft., fly 80 ft."
_hit points_: "45 (7d8 + 14)"
_armor class_: "14"
_stats_: | 16 (+3) | 19 (+4) | 14 (+2) | 12 (+1) | 11 (+0) | 14 (+2) |

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) piercing damage.

___Breath of Gales (Recharge 5-6).___ The dragon exhales a blast of wind in a 15-foot cone. Each creature in that cone must make a successful DC 12 Strength saving throw or be pushed 15 feet away from the dragon and knocked prone. Unprotected flames in the cone are extinguished, and sheltered flames (such as those in lanterns) have a 50 percent chance of being extinguished.

