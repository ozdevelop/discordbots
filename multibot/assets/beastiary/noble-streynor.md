"Noble Streynor";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Perception +7"
_senses_: "darkvision 60 ft., passive Perception 17"
_speed_: "50 ft."
_hit points_: "19 (3d10 + 3)"
_armor class_: "14"
_stats_: | 18 (+4) | 14 (+2) | 13 (+1) | 2 (-4) | 17 (+3) | 7 (-2) |

___Keen Hearing and Smell.___ The noble streynor has advantage on Wisdom
(Perception) checks that rely on hearing or smell up to 300 yards away.

**Actions**

___Bite.___ Melee Weapon Attack +6 to hit, reach 5 ft., one target. Hit: 11
(3d4 + 4) piercing damage.
