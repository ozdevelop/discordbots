"The Beast";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Giant"
_senses_: "Darkvision 60 ft., passive Perception 14"
_skills_: "Perception +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "84 (8d10 + 40)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 20 (+5) | 12 (+1) | 10 (+0) | 7 (-2) |

___Keen Smell.___ The Beast has advantage on Wisdom
(Perception) checks that rely on smell.

___Regeneration.___ The Beast regains 10 hit points at
the start of its turn. If the Beast takes acid or fire
damage, this trait doesn’t function at the start of the
Beast’s next turn. The Beast dies only if it starts its
turn with 0 hit points and doesn’t regenerate.

___Two Heads.___ The Beast has advantage on Wisdom
(Perception) checks and on saving throws against
being blinded, charmed, deafened, frightened,
stunned, and knocked unconscious.

___Wakeful.___ When one of the Beast’s heads is asleep,
its other head is awake.

**Actions**

___Multiattack.___ The Beast makes three attacks: one
with its bite and two with its claws, or two with its
bite and one with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 7 (1d6 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 11 (2d6 + 4) slashing damage.
