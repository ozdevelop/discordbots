"Shadow Beast";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Elvish, Umbral, Void Speech"
_skills_: "Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 12"
_saving_throws_: "Dex +7, Con +6"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "135 (18d8 + 54)"
_armor class_: "14"
_stats_: | 20 (+5) | 18 (+4) | 17 (+3) | 14 (+2) | 14 (+2) | 19 (+4) |

___Amorphous.___ The shadow beast can move through a space as narrow as 1 inch wide without squeezing.

___Incorporeal Movement.___ The shadow beast can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Innate Spellcasting.___ The shadow beast's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

3/day each: fear, telekinesis

___Magic Resistance.___ The beast has advantage on saving throws against spells and other magical effects.

___Sunlight Sensitivity.___ While in sunlight, the shadow beast has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The shadow beast makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 16 (2d10 + 5) slashing damage.

___Shadow Push (Recharge 5-6).___ The shadow beast buffets opponents with a gale of animated shadows in a 15-foot cone. Any creatures in the area of effect must succeed on a DC 15 Strength saving throw or be pushed back 10 feet and knocked prone.

