"Stegosaurus";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_speed_: "40 ft."
_hit points_: "76 (8d12+24)"
_armor class_: "13 (natural armor)"
_stats_: | 20 (+5) | 9 (-1) | 17 (+3) | 2 (-4) | 11 (0) | 5 (-3) |

**Actions**

___Thagomizer.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 26 (6d6+5) piercing damage.
