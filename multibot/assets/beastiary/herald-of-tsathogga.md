"Herald of Tsathogga";;;_size_: Huge aberration
_alignment_: chaotic evil
_challenge_: "18 (20,000 XP)"
_languages_: "--"
_skills_: "Perception +9"
_senses_: "darkvision 60 ft., passive Perception 19"
_saving_throws_: "Dex +9, Con +11, Wis +9, Cha +7"
_damage_resistances_: "thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "10 ft., fly 60 ft."
_hit points_: "299 (26d12 + 130)"
_armor class_: "19 (natural armor)"
_stats_: | 23 (+6) | 16 (+3) | 20 (+5) | 7 (-2) | 16 (+3) | 12 (+1) |

___Bloated.___ If the Herald of Tsathogga attacks with more than four
tentacles during a single turn, its movement speed is reduced to 0 and it
falls prone. The herald can use half its movement on its next turn to rise
from the prone position.

___Regeneration.___ The Herald of Tsathogga regains 20 hit points at the start
of its turn. If the herald takes fire or lightning damage, this trait doesn’t
function at the start of the herald’s next turn. The herald dies only if it
starts its turn with 0 hit points and doesn’t regenerate.

**Actions**

___Multiattack.___ The Herald of Tsathogga makes up to twelve attacks: one
with its bite or swallow, one with its tongue, and it can make one attack
with each of its ten tentacles.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 15
(2d8 + 6) piercing damage, and the target is grappled (escape DC 16).
Until this grapple ends, the target is restrained and the herald can’t bite
another target.

___Tongue.___ Melee Weapon Attack: +12 to hit, reach 30 ft., one target. Hit:
12 (1d12 + 6) slashing damage plus 9 (2d8) acid damage, and the target
must succeed on a DC 16 Strength saving throw or be pulled up to 25 feet
toward the Herald.

___Tentacle.___ Melee Weapon Attack: +12 to hit, reach 20 ft., one target. Hit:
10 (1d8 + 6) bludgeoning damage. If the target is a creature other than
an undead, it must succeed on a DC 19 Constitution saving throw or be
paralyzed for 1 minute. The target can repeat the saving throw at the end
of each of its turns, ending the effect on itself on a success.

___Swallow.___ The Herald of Tsathogga makes one bite attack against a Large
or smaller target it is grappling. If the attack hits, the target is swallowed
and the grapple ends. The swallowed target is blinded and restrained, has
total cover against attacks and other effects outside of the herald, and it
takes 21 (6d6) acid damage at the start of each of the herald’s turns. The
herald can have only one target swallowed at a time.

If the herald takes 50 damage or more on a single turn from the
swallowed creature, the herald must succeed on a DC 15 Constitution
saving throw at the end of that turn or forcefully disgorge the creature,
which falls prone in a space within 20 feet of the herald. If the herald dies,
a swallowed creature is no longer restrained by it and can escape from the
corpse using 15 feet of movement, exiting prone.

___Bellow (Recharge 5–6).___ The Herald of Tsathogga emits a deafening,
trilling croak. All creatures within 30 feet of the herald that can hear it
must make a DC 19 Constitution saving throw, taking 28 (8d6) thunder
damage on a failure, or half as much damage on a successful save. On a
failed save, the creature is also deafened for 1 minute. A deafened creature
can repeat the saving throw at the end of each of its turns, ending the effect
on itself on a success.
