"Young Bronze Dragon";;;_size_: Large dragon
_alignment_: lawful good
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Insight +4, Perception +7, Stealth +3"
_damage_immunities_: "lightning"
_saving_throws_: "Dex +3, Con +7, Wis +4, Cha +6"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "142 (15d10+60)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 10 (0) | 19 (+4) | 14 (+2) | 13 (+1) | 17 (+3) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Lightning Breath. The dragon exhales lightning in a 60-foot line that is 5 feet wide. Each creature in that line must make a DC 15 Dexterity saving throw, taking 55 (10d10) lightning damage on a failed save, or half as much damage on a successful one.

Repulsion Breath. The dragon exhales repulsion energy in a 30-foot cone. Each creature in that area must succeed on a DC 15 Strength saving throw. On a failed save, the creature is pushed 40 feet away from the dragon.