"Howling Hatred Priest";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Auran, Common"
_skills_: "Acrobatics +5, Intimidation +4, Religion +4"
_speed_: "30 ft."
_hit points_: "45 (10d8)"
_armor class_: "15 (studded leather)"
_stats_: | 12 (+1) | 16 (+3) | 10 (0) | 14 (+2) | 10 (0) | 14 (+2) |

___Hold Breath.___ The priest can hold its breath for 30 minutes.

___Spellcasting.___ The priest is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _blade ward, gust, light, prestidigitation, shocking grasp_

* 1st level (4 slots): _featherfall, shield, witch bolt_

* 2nd level (3 slots): _dust devil, gust of wind_

* 3rd level (2 slots): _gaseous form_

**Actions**

___Multiattack.___ The priest makes two melee attacks or two ranged attacks.

___Scimitar.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.
