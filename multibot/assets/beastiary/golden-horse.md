"Golden Horse";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "10 (5,900 XP)"
_languages_: "Understands the languages of its creator but can’t speak"
_senses_: "darkvision 120 ft., Passive Perception 10"
_damage_immunities_: "Poison, Psychic; Bludgeoning, Piercing, and Slashing from Nonmagical Attacks that aren’t Adamantine"
_condition_immunities_: "Charmed, Exhaustion, Frightened, Paralyzed, Petrified, Poisoned"
_speed_: "40 ft."
_hit points_: "178 (17d10 + 85)"
_armor class_: "17 (natural armor)"
_stats_: | 22 (+6) | 9 (-1) | 20 (+5) | 3 (-4) | 11 (+0) | 1 (-5) |

___Immutable Form.___ The golden horse is immune to any
spell or effect that would alter its form.

___Magic Resistance.___ The golden horse has advantage
on saving throws against spells and other magical
effects.

___Magic Weapons.___ The golden horse’s weapon attacks
are magical.

___Trampling Charge.___ If the golden horse moves at
least 20 feet straight toward a creature and then
hits it with a hooves attack on the same turn, that
target must succeed on a DC 16 Strength saving
throw or be knocked prone. If the target is prone,
the golden horse can make another attack with its
hooves against it as a bonus action.

**Actions**

___Multiattack.___ The golden horse makes two hoof
attacks.

___Hooves.___ Melee Weapon Attack: +10 to hit, reach 5 ft.,
one target. Hit: 19 (3d8 + 6) bludgeoning damage.

___Slow (Recharge 5–6).___ The golden horse targets one
or more creatures it can see within 10 feet of it. Each
target must make a DC 16 Wisdom saving throw
against this magic. On a failed save, a target can’t
use reactions, its speed is halved, and it can’t make
more than one attack on its turn. In addition, the
target can take either an action or a bonus action on
its turn, not both. These effects last for 1 minute. A
target can repeat the saving throw at the end of each
of its turns, ending the effect on itself on a success.
