"Glass Wyrm";;;_size_: Large dragon
_alignment_: neutral
_challenge_: "11 (7,200 XP)"
_languages_: "Draconic, Undercommon"
_skills_: "Arcana +6, Insight +4, Perception +8, Persuasion +8, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Dex +4, Con +9, Wis +4, Cha +8"
_damage_immunities_: "poison"
_damage_vulnerabilities_: "thunder"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "paralyzed, poisoned, unconscious"
_speed_: "30 ft., fly 60 ft."
_hit points_: "105 (10d10 + 50)"
_armor class_: "17 (natural armor)"
_stats_: | 23 (+6) | 10 (+0) | 21 (+5) | 14 (+2) | 11 (+0) | 19 (+4) |

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it can
choose to succeed instead.

___Spell Reflection.___ Any time the glass wyrm is targeted by a magic
missile spell, a line spell, or a spell that requires a ranged attack roll, roll a
d6. On a 1 to 5, the dragon is unaffected. On a 6, the dragon is unaffected,
and the effect is reflected back at the caster as though it originated from
the dragon, turning the caster into the target.

**Actions**

___Multiattack.___ The glass wyrm uses its blinding reflection ability, and
makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 15 (2d8 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 17 (2d10 + 6) slashing damage.

___Blinding Reflection.___ The glass wyrm reflects available light in a burst of radiance out to a
30-foot radius. Creatures that can see the glass wyrm in the area must succeed on a DC 17 Dexterity saving throw or be blinded for 1 minute. A blinded creature can attempt a DC 17 Constitution saving throw at the end of each of its turns, ending the blinded condition on a success.

The glass wyrm can’t use this ability if it does not begin its turn within
the dim light or bright light radius of a light source.

___Glass Shards (Recharge 5–6).___ The glass wyrm breaths a 40-foot cone
of glass razor-like glass shards. Creatures in the area must succeed on a
DC 17 Dexterity saving throw, taking 54 (12d8) piercing damage on a
failed saving throw, or half as much damage on a successful one.
