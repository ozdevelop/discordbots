"Goblin Boss";;;_size_: Small humanoid (goblinoid)
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +6"
_speed_: "30 ft."
_hit points_: "21 (6d6)"
_armor class_: "17 (chain shirt, shield)"
_stats_: | 10 (0) | 14 (+2) | 10 (0) | 10 (0) | 8 (-1) | 10 (0) |

___Nimble Escape.___ The goblin can take the Disengage or Hide action as a bonus action on each of its turns.

**Actions**

___Multiattack.___ The goblin makes two attacks with its scimitar. The second attack has disadvantage.

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage.

___Javelin.___ Melee or Ranged Weapon Attack: +2 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 5 (1d6) piercing damage.

**Reactions**

___Redirect Attack.___ When a creature the goblin can see targets it with an attack, the goblin chooses another goblin within 5 feet of it. The two goblins swap places, and the chosen goblin becomes the target instead.