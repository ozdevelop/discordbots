"Ambush Drake";;;_size_: Medium dragon
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "understands Draconic but can't speak it"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4, Stealth +4"
_speed_: "30 ft."
_hit points_: "22 (4d6+8)"
_armor class_: "13 (natural armor)"
_damage_resistances_: "poison"
_stats_: | 13 (+1) | 15 (+2) | 14 (+2) | 4 (-3) | 11 (0) | 6 (-2) |

___Pack Tactics.___ The drake has advantage on an attack roll against a creature if at least one of the drake's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Surprise Attack.___ If the drake surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 7 (2d6) damage from the attack.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) piercing damage.