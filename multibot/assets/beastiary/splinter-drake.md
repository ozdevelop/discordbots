"Splinter Drake";;;_size_: Large plant
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Sylvan"
_skills_: "Perception +5, Stealth +6 (+10 in forest), Survival +5"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 15"
_damage_resistances_: "cold, fire"
_condition_immunities_: "blinded, charmed, deafened, exhaustion"
_speed_: "40 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "16 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 18 (+4) | 10 (+0) | 12 (+1) | 10 (+0) |

___Land’s Stride.___ Moving through nonmagical difficult terrain costs the
splinter drake no extra movement, and it can pass through nonmagical
plants without being slowed by them and without taking damage from them.

**Actions**

___Multiattack.___ The splinter drake makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 12
(2d8 + 3) piercing damage plus 7 (2d6) poison damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) slashing damage.

___Thorn Volley (Recharge 5–6).___ The splinter drake emits a spray of
thorns in a 40-foot cone. Creatures within this area must make a DC 16
Dexterity saving throw, taking 21 (6d6) piercing damage and 28 (8d6)
poison damage on a failed saving throw, or half as much damage on a
successful saving throw.
