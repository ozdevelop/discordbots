"Bear King";;;_size_: Medium fey
_alignment_: lawful neutral
_challenge_: "12 (8400 XP)"
_languages_: "Common, Elvish, Giant, Sylvan"
_skills_: "Athletics +9, Intimidate +7, Perception +7"
_senses_: "darkvision 60 ft., passive Perception 17"
_saving_throws_: "Str +9, Dex +4, Wis +7"
_damage_immunities_: "poison; bludgeoning, slashing, and piercing from nonmagical weapons that aren't made of cold iron"
_damage_resistances_: "cold"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "133 (14d8 + 70)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 10 (+0) | 20 (+5) | 12 (+1) | 17 (+3) | 16 (+3) |

___Alternate Form.___ As a bonus action, the Bear King can assume the form of a shaggy grizzly bear, or a hybrid of his humanoid and bear forms. In alternate form the Bear King's size increases to Large, and he can make bite and claw attacks. He remains in this form until he returns to human form as a bonus action or he falls unconscious or dies.

___Keen Smell.___ The Bear King has advantage on Perception (Wisdom) checks that rely on smell.

___Legendary Resistance (3/day).___ If the Bear King fails a saving throw, he can choose to succeed instead.

___Regeneration (Alternate Form only).___ The Bear King regains 10 hit points at the start of his turn if he has at least 1 hit point.

___Bear King's Lair.___ On initiative count 20 (losing initiative ties), the Bear King takes a lair action to cause one of the following effects; the Bear King can't use the same effect two rounds in a row:

- The Bear King magically conjures up a swarm of eight giant bees (use giant wasp statistics) to defend his lair. The bees act immediately, and on initiative count 20 in subsequent rounds. The bees remain until they're killed or until the Bear King dismisses them as an action. The Bear King can use this action again, but no more than eight giant bees can be present at a time. 

- The Bear King targets a creature within the lair that he can see. The creature must succeed on a DC 15 Constitution saving throw or be magically transformed into a brown bear as if by a polymorph spell. At the beginning of its turn, a transformed creature repeats the saving throw. If it fails, the creature must use its action to attack one of the Bear King's foes. If it succeeds, the effect ends and the creature returns to its normal form. The effect lasts until the creature succeeds on the saving throw or until the Bear King uses this lair action again.

- The Bear King causes the ground in a 20-foot radius to tremble and shake. Any creatures in the affected area must succeed on a DC 15 Strength saving throw or suffer 7 (2d6) bludgeoning damage and fall prone. The ground continues to tremble until initiative count 20 on the following round, during which time the area is difficult terrain.

___Regional Effects.___ The region surrounding Gloaming Crag is warped by the Bear King's magic, which creates one or more of the following effects:

- Within 10 miles of the Bear King's lair, creatures have disadvantage on saving throws made to avoid contracting lycanthropy from a werebear.

- Bees within 10 miles of the Bear King's lair are easily agitated quick to attack. Insect swarms (bees or hornets) are common in the area, but they tend to ignore locals.

- Emotions within 5 miles of the Bear King's lair run high. Arguments quickly descend into physical scuffles and enjoyable get-togethers are likely to become raucous carousing or even brawls.

If the Bear King dies, conditions in the area surrounding the lair return to normal over the course of 1d6 days.

**Actions**

___Multiattack.___ The bear king makes two ranged attacks or two melee attacks. In hybrid or grizzly bear form he can also make one additional bite attack.

___Bite (Grizzly or Hybrid Form Only).___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 10 (1d10 + 5) piercing damage. A target creature other than a construct or undead must make a successful DC 17 Constitution saving throw at the start of each of its turns or lose 10 (3d6) hit points from blood loss. Each time the Bear King hits the wounded creature with this attack, the hit point loss increases by 10 (3d6). A creature can take an action to staunch the bleeding on itself or an adjacent ally with a successful DC 12 Wisdom (Medicine) check. The bleeding also stops if the creature receives any magical healing.

___Claws (Grizzly or Hybrid Form Only).___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

___Maul (Human or Hybrid Form Only).___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage. A creature hit by two maul attacks in the same turn must succeed on a DC 17 Strength saving throw or fall prone.

___Javelin (Human Form Only).___ Ranged Weapon Attack: +9 to hit, range 30/60 ft., one target. Hit: 8 (1d6 + 5) piercing damage.

**Legendary** Actions

___The Bear King can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The Bear King regains spent legendary actions at the start of his turn.

___Melee Attack.___ The Bear King makes a claw or maul attack.

___Honey Toss.___ The Bear King reaches into the jar he carries at his side and hurls a glob of honey at a target within 30 feet as a ranged weapon attack (+9 to hit). If the attack hits, the creature is restrained (escape DC 17).

___Frightful Roar (2 actions).___ The Bear King lets out a bloodcurdling roar. All creatures within 90 feet who can hear the Bear King must succeed on a DC 15 Wisdom saving throw or be frightened for 1 minute. A creature that makes its save, or who the effect ends for, is immune to the Frightful Roar for 24 hours. The bear king's allies are unaffected.

