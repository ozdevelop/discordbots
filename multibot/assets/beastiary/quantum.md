"Quantum";;;_size_: Huge aberration
_alignment_: neutral
_challenge_: "20 (25,000 XP)"
_languages_: "Common, Quantum"
_skills_: "Acrobatics +16, Insight +14, Perception +14, Religion +12"
_senses_: "darkvision 60 ft., passive Perception 24"
_saving_throws_: "Wis +8, Cha +9"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "0 ft., fly 40 ft."
_hit points_: "448 (39d12 + 195)"
_armor class_: "20 (natural armor)"
_stats_: | 30 (+10) | 18 (+4) | 20 (+5) | 11 (+0) | 14 (+2) | 17 (+3) |

___Displacement.___ The quantum projects a magical illusion that makes it
appear to be standing near its actual location, causing attack rolls against
it to have disadvantage. If it is hit by an attack, this trait is disrupted until
the end of its next turn. This trait is also disrupted while the quantum is
incapacitated or has a speed of 0.

___Innate Spellcasting.___ The quantum’s spellcasting ability is Charisma
(spell save DC 17, +9 to hit with spell attacks). It can innately cast
_dimension door_ once per day, requiring no material components.

___Quantum Form (3/day).___ A quantum can move in such a way as to
appear in two places at once, at a distance no greater than 30 feet. This is
a bonus action that provokes an attack of opportunity and lasts only one
round. While occupying two spaces simultaneously, each representation of
the quantum can perform one action normally. Using this ability, the same
quantum could attack two different opponents, or attack one opponent
while opening a door to escape, and so on. At the end of the round, both
instances of the quantum return to the space it originally occupied before
activating this ability.

**Actions**

___Multiattack.___ The quantum makes two attacks with its tentacles.

___Tentacles.___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target.
Hit: 31 (6d6 + 10) bludgeoning damage.

___Disintegration (3/day).___ Melee Weapon Attack: +16 to hit, reach 15
ft., one target. Hit: the target must make a DC 18 Constitution saving
throw, taking 140 (40d6) force damage on a failed save, or 42 (12d6)
force damage on a successful one. If the damage reduces the target to 0 hit
points, it is disintegrated.
