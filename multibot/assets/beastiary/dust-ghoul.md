"Dust Ghoul";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 18"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "40 ft., fly 40 ft., burrow 20 ft."
_hit points_: "104 (16d8 + 32)"
_armor class_: "16 (natural armor)"
_stats_: | 21 (+5) | 16 (+3) | 15 (+2) | 14 (+2) | 14 (+2) | 16 (+3) |

___Improved Critical.___ The ghoul’s attacks score a critical hit
on a roll of 19 or 20.

**Actions**

___Multiattack.___ The dust ghoul makes three attacks: one with
its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) piercing damage.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 15 (3d6 + 5) slashing damage.

___Blinding Dust (1/day).___ Blinding dust and sand swirls magically around
the ghoul. Each creature within 5 feet of the ghoul must succeed on a DC
15 Constitution saving throw or be blinded until the end of the creature’s
next turn.

___Paralyzing Shriek (Recharge 5–6).___ The dust ghoul unleashes a hellish
shriek. Each creature that is within 60 feet of the ghoul and can hear it
must succeed on a DC 13 Constitution saving throw or be paralyzed for
1 minute. The target can repeat the saving throw at the end of each of its
turns, ending the effect on itself on a success.
