"Wolf Spirit Swarm";;;_size_: Large swarm
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "understands Common"
_skills_: "Perception +3, Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 13"
_saving_throws_: "Str +5, Dex +6"
_damage_immunities_: "cold"
_damage_resistances_: "necrotic; bludgeoning, piercing, slashing"
_condition_immunities_: "exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "50 ft., fly 50 ft. (hover)"
_hit points_: "97 (15d10 + 15)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 16 (+3) | 12 (+1) | 4 (-3) | 10 (+0) | 12 (+1) |

___Speed Over Snow.___ A swarm of wolf spirits is not affected by difficult terrain caused by snowy or icy conditions.

**Actions**

___Multiattack.___ A wolf spirit swarm uses icy doom, if it's available, and makes 3 bite attacks.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) piercing damage plus 3 (1d6) cold damage. The target is also knocked prone if the attack scored a critical hit.

___Icy Doom (Recharge 5-6).___ All creatures within 5 feet of the wolf spirit swarm take 22 (4d10) cold damage, or half damage with a successful DC 14 Constitution saving throw. Those that fail the saving throw also gain one level of exhaustion and become frightened until the start of the swarm's next turn.

___Chilling Howl.___ As a bonus action on its first turn of combat, the wolf spirit swarm howls, emitting an unnatural and eerie cacophony that chills the blood. All creatures within 300 feet that hear the howl must make a successful DC 12 Charisma saving throw or be frightened until the start of the swarm's next turn.

