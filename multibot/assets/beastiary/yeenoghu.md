"Yeenoghu";;;_page_number_: 155
_size_: Huge fiend (demon)
_alignment_: chaotic evil
_challenge_: "24 (62,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 24"
_skills_: "Intimidation +9, Perception +14"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_saving_throws_: "Dex +10, Con +15, Wis +14"
_speed_: "50 ft."
_hit points_: "333  (23d12 + 184)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 29 (+9) | 16 (+3) | 23 (+6) | 15 (+2) | 24 (+7) | 15 (+2) |

___Innate Spellcasting.___ Yeenoghu's spellcasting ability is Charisma (spell save DC 17, +9 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

* At will: _detect magic_

* 3/day each: _dispel magic, fear, invisibility_

* 1/day: _teleport_

___Legendary Resistance (3/Day).___ If Yeenoghu fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Yeenoghu has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Yeenoghu's weapon attacks are magical.

___Rampage.___ When Yeenoghu reduces a creature to 0 hit points with a melee attack on his turn, Yeenoghu can take a bonus action to move up to half his speed and make a bite attack.

**Actions**

___Multiattack___ Yeenoghu makes three flail attacks. If an attack hits, he can cause it to create an additional effect of his choice or at random (each effect can be used only once per Multiattack):
1. The attack deals an extra 13 (2d12) bludgeoning damage.
2. The target must succeed on a DC 17 Constitution saving throw or be paralyzed until the start of Yeenoghu's next turn.
3. The target must succeed on a DC 17 Wisdom saving throw or be affected by the confusion spell until the start of Yeenoghu's next turn.

___Flail___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 15 (1d12 + 9) bludgeoning damage.

___Bite___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 14 (1d10 + 9) piercing damage.

**Legendary** Actions

Yeenoghu can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Yeenoghu regains spent legendary actions at the start of his turn.

___Charge___ Yeenoghu moves up to his speed.

___Swat Away___ Yeenoghu makes a flail attack. If the attack hits, the target must succeed on a DC 24 Strength saving throw or be pushed 15 feet in a straight line away from Yeenoghu. If the saving throw fails by 5 or more, the target falls prone.

___Savage (Costs 2 Actions)___ Yeenoghu makes a bite attack against each creature within 10 feet of him.