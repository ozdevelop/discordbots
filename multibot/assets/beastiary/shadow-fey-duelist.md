"Shadow Fey Duelist";;;_size_: Medium humanoid
_alignment_: lawful evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Elvish, Umbral"
_skills_: "Arcana +4, Deception +6, Perception +4, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +8, Con + 5, Wis + 4, Cha +6"
_speed_: "30 ft."
_hit points_: "117 (18d8 + 36)"
_armor class_: "17 (studded leather)"
_stats_: | 13 (+1) | 20 (+5) | 14 (+2) | 13 (+1) | 12 (+1) | 16 (+3) |

___Fey Ancestry.___ The shadow fey has advantage on saving throws against being charmed, and magic can't put it to sleep.

___Innate Spellcasting.___ The shadow fey's innate spellcasting ability is Charisma. It can cast the following spells innately, requiring no material components.

3/day: misty step (when in shadows, dim light, or darkness only)

___Sunlight Sensitivity.___ While in sunlight, the shadow fey has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

___Traveler in Darkness.___ The shadow fey has advantage on Intelligence (Arcana) checks made to know about shadow roads and shadow magic spells or items.

**Actions**

___Multiattack.___ The shadow fey makes two rapier attacks. If it has a dagger drawn, it can also make one dagger attack.

___Dagger.___ Melee or Ranged Weapon Attack: +8 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d4 + 5) piercing damage, and a target creature must succeed on a DC 15 Constitution saving throw or become poisoned for 1 minute. A poisoned creature repeats the save at the end of each of its turns, ending the effect on a success.

___Rapier.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) piercing damage plus 7 (2d6) poison damage.

**Reactions**

___Parry.___ The shadow fey duelist adds 3 to its AC against one melee attack that would hit it. To do so, the duelist must see the attacker and be wielding a melee weapon.

