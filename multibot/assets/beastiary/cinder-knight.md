"Cinder Knight";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Ignan"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "paralyzed, poisoned, stunned, unconscious"
_speed_: "30 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "18 (plate)"
_stats_: | 18 (+4) | 17 (+3) | 16 (+3) | 6 (-2) | 10 (+0) | 7 (-2) |

___Heat Aura.___ At the start of each of the cinder knight’s
turns, each creature within 5 feet of it takes 7 (2d6) fire
damage. A creature that touches the cinder knight or
hits it with a melee attack while within 5 feet of it
takes 7 (2d6) fire damage.

___Illumination.___ The cinder knight sheds dim light
in a 15-foot radius.

___Water Susceptibility.___ For every 5 feet the cinder
knight moves in water, or for every gallon of
water splashed on it, it takes 1 cold damage.

**Actions**

___Multiattack.___ The cinder knight makes two
melee attacks.

___Greatsword.___ Melee Weapon Attack: +8 to hit,
reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing
damage plus 14 (4d6) fire damage. If the target is
a creature or a flammable object, it ignites. Until a
creature takes an action to douse the fire, the target
takes 5 (1d10) fire damage at the start of each
of its turns.

___Slam.___ Melee Weapon Attack: +8 to hit, reach
5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning
damage plus 14 (4d6) fire damage. If the
target is a creature or a flammable object,
it ignites. Until a creature takes an action
to douse the fire, the target takes 5 (1d10)
fire damage at the start of each of its turns.
