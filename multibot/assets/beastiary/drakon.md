"Drakon";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "5 (1800 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_resistances_: "acid"
_condition_immunities_: "paralyzed"
_speed_: "30 ft., fly 60 ft., swim 40 ft."
_hit points_: "105 (14d10 + 28)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 19 (+4) | 15 (+2) | 2 (-4) | 12 (+1) | 10 (+0) |

___Dissolving Gaze.___ When a creature that can see the drakon's eyes starts its turn within 30 feet of the drakon, the drakon can force it to make a DC 13 Constitution saving throw if the drakon isn't incapacitated and can see the creature. On a failed saving throw, the creature takes 3 (1d6) acid damage, its hit point maximum is reduced by an amount equal to the acid damage it takes (which ends after a long rest), and it's paralyzed until the start of its next turn. Unless surprised, a creature can avert its eyes at the start of its turn to avoid the saving throw. If the creature does so, it can't see the drakon until the start of its next turn, when it chooses again whether to avert its eyes. If the creature looks at the drakon before then, it must immediately make the saving throw.

**Actions**

___Multiattack.___ The drakon makes one bite attack and one tail attack.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage plus 10 (4d4) acid damage.

___Tail.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage.

___Acid Breath (Recharge 5-6).___ The drakon exhales acidic vapors in a 15-foot cone. Each creature in that area takes 28 (8d6) acid damage, or half damage with a successful DC 13 Constitution saving throw.

