"Naxene Drathkala";;;_size_: Medium humanoid (turami human)
_alignment_: neutral good
_challenge_: "0 (10 XP)"
_languages_: "Common, Draconic, Dwarvish, Elvish"
_skills_: "Arcana +5, History +5"
_speed_: "30 ft."
_hit points_: "27 (6d8)"
_armor class_: "10 (13 with mage armor)"
_senses_: " passive Perception 11"
_stats_: | 8 (-1) | 11 (0) | 11 (0) | 17 (+3) | 12 (+1) | 11 (0) |

___Spellcasting.___ Naxene is a 6th level spellcaster. Her spellcasting ability is Intelligence (spell save DC 13; +5 to hit with spell attacks). She has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, light, mage hand, prestidigitation_

* 1st level (4 slots): _mage armor, magic missile, shield_

* 2nd level (3 slots): _misty step, suggestion_

* 3rd level (3 slots): _fly, lightning bolt_

**Actions**

___Staff.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one creature. Hit: 2 (1d6-1) bludgeoning damage, or 3 (1d8-1) bludgeoning damage if used with two hands.

**Roleplaying** Information

Goldenfields' crops are vital Waterdeep's survival, which is why the Watchful Order of Magists and Protectors sent Naxene to make sure the temple-farm is adeuately defended. At first she regarded the task as a punishment, but now she appreciates the peace and quiet.

**Ideal:** "There's no problem that can't be solved with magic."

**Bond:** "I have great respect for Lady Laeral Silverhand of Waterdeep. She and the Lords' Alliance are going to bring some much-needed order to this lawless land."

**Flaw:** "I'm too smart to be wrong about anything."
