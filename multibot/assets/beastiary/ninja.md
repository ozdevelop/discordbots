"Ninja";;;_size_: Medium humanoid
_alignment_: lawful neutral
_challenge_: "1 (200 XP)"
_languages_: "Common"
_skills_: "Stealth +7, Sleight of Hand +5, Perception +3"
_senses_: "blindsight 10 ft., passive Perception 13"
_damage_resistances_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft., climb 20 ft."
_hit points_: "22 (4d8 + 4)"
_armor class_: "14 (leather armor)"
_stats_: | 10 (+0) | 16 (+3) | 12 (+1) | 10 (+0) | 12 (+1) | 10 (+0) |

___Sneak Attack.___ Once per turn, whenever the ninja
attacks, it can deal an extra 7 (2d6) damage to one
creature hit with an attack if it had advantage on the
attack roll or if it has an ally within 5 feet of its
target that isn’t incapacitated.

___Rigorous Training.___ The ninja can hold its breath for
up to five minutes and cannot be poisoned.

___Grappling Hook.___ The ninja is skilled in the use of a
grappling hook, allowing it to scale structures with
ease. The ninja gains a 20 ft. climb speed.

**Actions**

___Dagger.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 5 (1d4 + 3) piercing damage.

___Throwing Star.___ Ranged Weapon Attack: +5 to hit,
range 20/60 ft., one target. Hit: 5 (1d4 + 3)
piercing damage.

___Smoke Bomb.___ The ninja throws a smoke bomb at its
feet. This creates a cloud of smoke in a 10-foot
radius area that renders all creatures within it blind
and heavily obscures the area to creatures outside
of it. This cloud persists until the beginning of the
ninja’s next turn.
