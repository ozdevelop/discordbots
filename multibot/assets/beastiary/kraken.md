"Kraken";;;_size_: Gargantuan monstrosity (titan)
_alignment_: chaotic evil
_challenge_: "23 (50,000 XP)"
_languages_: "understands Abyssal, Celestial, Infernal, and Primordial but can't speak, telepathy 120 ft."
_senses_: "truesight 120 ft."
_damage_immunities_: "lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Str +17, Dex +7, Con +14, Int +13, Wis +11"
_speed_: "20 ft., swim 60 ft."
_hit points_: "472 (27d20+189)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "frightened, paralyzed"
_stats_: | 30 (+10) | 11 (0) | 25 (+7) | 22 (+6) | 18 (+4) | 20 (+5) |

___Amphibious.___ The kraken can breathe air and water.

___Freedom of Movement.___ The kraken ignores difficult terrain, and magical effects can't reduce its speed or cause it to be restrained. It can spend 5 feet of movement to escape from nonmagical restraints or being grappled.

___Siege Monster.___ The kraken deals double damage to objects and structures.

**Actions**

___Multiattack.___ The kraken makes three tentacle attacks, each of which it can replace with one use of Fling.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 23 (3d8 + 10) piercing damage. If the target is a Large or smaller creature grappled by the kraken, that creature is swallowed, and the grapple ends. While swallowed, the creature is blinded and restrained, it has total cover against attacks and other effects outside the kraken, and it takes 42 (12d6) acid damage at the start of each of the kraken's turns. If the kraken takes 50 damage or more on a single turn from a creature inside it, the kraken must succeed on a DC 25 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet of the kraken. If the kraken dies, a swallowed creature is no longer restrained by it and can escape from the corpse using 15 feet of movement, exiting prone.

___Tentacle.___ Melee Weapon Attack: +7 to hit, reach 30 ft., one target. Hit: 20 (3d6 + 10) bludgeoning damage, and the target is grappled (escape DC 18). Until this grapple ends, the target is restrained. The kraken has ten tentacles, each of which can grapple one target.

___Fling.___ One Large or smaller object held or creature grappled by the kraken is thrown up to 60 feet in a random direction and knocked prone. If a thrown target strikes a solid surface, the target takes 3 (1d6) bludgeoning damage for every 10 feet it was thrown. If the target is thrown at another creature, that creature must succeed on a DC 18 Dexterity saving throw or take the same damage and be knocked prone.

___Lightning Storm.___ The kraken magically creates three bolts of lightning, each of which can strike a target the kraken can see within 120 feet of it. A target must make a DC 23 Dexterity saving throw, taking 22 (4d10) lightning damage on a failed save, or half as much damage on a successful one.

**Legendary** Actions

The kraken can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The kraken regains spent legendary actions at the start of its turn.

___Tentacle Attack or Fling.___ The kraken makes one tentacle attack or uses its Fling.

___Lightning Storm (Costs 2 Actions).___ The kraken uses Lightning Storm.

___Ink Cloud (Costs 3 Actions).___ While underwater, the kraken expels an ink cloud in a 60-foot radius. The cloud spreads around corners, and that area is heavily obscured to creatures other than the kraken. Each creature other than the kraken that ends its turn there must succeed on a DC 23 Constitution saving throw, taking 16 (3d10) poison damage on a failed save, or half as much damage on a successful one. A strong current disperses the cloud, which otherwise disappears at the end of the kraken's next turn.