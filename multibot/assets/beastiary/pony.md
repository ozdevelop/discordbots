"Pony";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_speed_: "40 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "10"
_stats_: | 15 (+2) | 10 (0) | 13 (+1) | 2 (-4) | 11 (0) | 7 (-2) |

**Actions**

___Hooves.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) bludgeoning damage.