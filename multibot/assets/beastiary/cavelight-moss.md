"Cavelight Moss";;;_size_: Large plant
_alignment_: neutral
_challenge_: "4 (1100 XP)"
_languages_: "--"
_senses_: "tremorsense 60 ft., passive Perception 11"
_damage_resistances_: "acid, cold, fire; slashing from nonmagical weapons"
_condition_immunities_: "charmed, deafened, frightened, paralyzed, prone, stunned, unconscious"
_speed_: "5 ft., climb 5 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "15 (natural armor)"
_stats_: | 24 (+7) | 10 (+0) | 18 (+4) | 1 (-5) | 13 (+1) | 5 (-3) |

___Luminescence.___ The chemicals within cavelight moss make the entire creature shed light as a torch. A cavelight moss cannot suppress this effect. It can, however, diminish the light produced to shed illumination as a candle.

___Strength Drain.___ Living creatures hit by the cavelight moss's tendril attack or caught up in its grapple must make a successful DC 14 Constitution saving throw or suffer 1 level of exhaustion. Creatures that succeed are immune to that particular cavelight moss's Strength Drain ability for 24 hours. For every level of exhaustion drained, the cavelight moss gains 5 temporary hit points.

**Actions**

___Tendrils.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 33 (4d12 + 7) bludgeoning damage. If the target is a creature, it is grappled (escape DC 17). Until this grapple ends, the target is restrained, and the cavelight moss can't use its tendrils against another target.
