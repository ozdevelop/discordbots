"Zuggtmoy";;;_page_number_: 157
_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "23 (50,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 21"
_skills_: "Perception +11"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_saving_throws_: "Dex +9, Con +11, Wis +11"
_speed_: "30 ft."
_hit points_: "304  (32d10 + 128)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 22 (+6) | 15 (+2) | 18 (+4) | 20 (+5) | 19 (+4) | 24 (+7) |

___Innate Spellcasting.___ Zuggtmoy's spellcasting ability is Charisma (spell save DC 22). She can innately cast the following spells, requiring no material components:

* At will: _detect magic, locate animals or plants, ray of sickness_

* 3/day each: _dispel magic, ensnaring strike, entangle, plant growth_

* 1/day each: _etherealness, teleport_

___Legendary Resistance (3/Day).___ If Zuggtmoy fails a saving throw, she can choose to succeed instead.

___Magic Resistance.___ Zuggtmoy has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Zuggtmoy's weapon attacks are magical.

**Actions**

___Multiattack___ Zuggtmoy makes three pseudopod attacks.

___Pseudopod___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) bludgeoning damage plus 9 (2d8) poison damage.

___Infestation Spores (3/Day)___ Zuggtmoy releases spores that burst out in a cloud that fills a 20-foot-radius sphere centered on her, and it lingers for 1 minute. Any flesh-and-blood creature in the cloud when it appears, or that enters it later, must make a DC 19 Constitution saving throw. On a successful save, the creature can't be infected by these spores for 24 hours. On a failed save, the creature is infected with a disease called the spores of Zuggtmoy and also gains a random form of madness (determined by rolling on the Madness of Zuggtmoy table) that lasts until the creature is cured of the disease or dies. While infected in this way, the creature can't be reinfected, and it must repeat the saving throw at the end of every 24 hours, ending the infection on a success. On a failure, the infected creature's body is slowly taken over by fungal growth, and after three such failed saves, the creature dies and is reanimated as a spore servant if it's a type of creature that can be (see the "Myconids" entry in the Monster Manual).

___Mind Control Spores (Recharge 5-6)___ Zuggtmoy releases spores that burst out in a cloud that fills a 20-foot-radius sphere centered on her, and it lingers for 1 minute. Humanoids and beasts in the cloud when it appears, or that enter it later, must make a DC 19 Wisdom saving throw. On a successful save, the creature can't be infected by these spores for 24 hours. On a failed save, the creature is infected with a disease called the influence of Zuggtmoy for 24 hours. While infected in this way, the creature is charmed by her and can't be reinfected by these spores.

**Legendary** Actions

Zuggtmoy can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Zuggtmoy regains spent legendary actions at the start of her turn.

___Attack___ Zuggtmoy makes one pseudopod attack.

___Exert Will___ One creature charmed by Zuggtmoy that she can see must use its reaction to move up to its speed as she directs or to make a weapon attack against a target that she designates.