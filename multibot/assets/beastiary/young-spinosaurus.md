"Young Spinosaurus";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "5 (1800 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: ", passive Perception 13"
_speed_: "50 ft., swim 30 ft."
_hit points_: "105 (10d12 + 40)"
_armor class_: "14 (natural armor)"
_stats_: | 23 (+6) | 11 (+0) | 19 (+4) | 2 (-4) | 11 (+0) | 8 (-1) |

___Tamed.___ The spinosaurus never willingly attacks any reptilian humanoid, and if forced or magically compelled to do so it suffers disadvantage on attack rolls. Up to three Medium or one Large creatures can ride the spinosaurus. This trait disappears if the spinosaurus spends a month away from any reptilian humanoid.

**Actions**

___Multiattack.___ The spinosaurus makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10ft., one target. Hit: 25 (3d12 + 6) piercing damage. If the target is a Medium or smaller creature, it is grappled (escape DC 16). Until this grapple ends, the target is restrained and the spinosaurus can't bite another target.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

