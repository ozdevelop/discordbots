"Goristro";;;_size_: Huge fiend (demon)
_alignment_: chaotic evil
_challenge_: "17 (18,000 XP)"
_languages_: "Abyssal"
_senses_: "darkvision 120 ft."
_skills_: "Perception +7"
_damage_immunities_: "poison"
_saving_throws_: "Str +13, Dex +6, Con +13, Wis +7"
_speed_: "40 ft."
_hit points_: "310 (23d12+161)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 25 (+7) | 11 (0) | 25 (+7) | 6 (-2) | 13 (+1) | 14 (+2) |

___Charge.___ If the goristro moves at least 15 feet straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 38 (7d10) piercing damage. If the target is a creature, it must succeed on a DC 21 Strength saving throw or be pushed up to 20 feet away and knocked prone.

___Labyrinthine Recall.___ The goristro can perfectly recall any path it has traveled.

___Magic Resistance.___ The goristro has advantage on saving throws against spells and other magical effects.

___Siege Monster.___ The goristro deals double damage to objects and structures.

**Actions**

___Multiattack.___ The goristro makes three attacks: two with its fists and one with its hoof.

___Fist.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 20 (3d8 + 7) bludgeoning damage.

___Hoof.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 23 (3d10 + 7) bludgeoning damage. If the target is a creature, it must succeed on a DC 21 Strength saving throw or be knocked prone.

___Gore.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 45 (7d10 + 7) piercing damage.