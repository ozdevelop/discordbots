"Purple Worm Zombie";;;_size_: Gargantuan undead
_alignment_: neutral evil
_challenge_: "15 (13,000 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft., tremorsense 60 ft., passive Perception 9"
_saving_throws_: "Wis +4"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "50 ft., burrow 30 ft."
_hit points_: "247 (15d20 + 90)"
_armor class_: "18 (natural armor)"
_stats_: | 28 (+9) | 7 (-2) | 22 (+6) | 1 (-5) | 8 (-1) | 4 (-3) |

___Tunneler.___ The worm can burrow through solid rock as half its burrow
speed leaving a 10-foot-diameter tunnel in its wake.

___Undead Fortitude.___ If damage reduces the zombie purple worm to 0 hit
points, it makes a Constitution saving throw with a DC of 5 + the damage
taken, unless the damage is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.

**Actions**

___Multiattack.___ The worm makes two attacks: one with its bite and one
with its stinger.

___Bite.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target. Hit: 22
(3d8 + 9) piercing damage. If the target is a Large or smaller creature, it
must succeed on a DC 19 Dexterity saving throw or be swallowed by the
worm. A swallowed creature is blinded and restrained, it has total cover
against attacks and other effects outside the worm, and it takes 21 (6d6)
necrotic damage at the start of each of the worm’s turns.

If the worm dies, a swallowed creature is no longer restrained by it and
can escape from the corpse by using 20 feet of movement, exiting prone.

___Tail Stinger.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one
creature. Hit: 19 (3d6 + 9) piercing damage, and the target must make a
DC 19 Constitution saving throw, taking 42 (12d6) poison damage on a
failed save, or half as much damage on a successful one.
