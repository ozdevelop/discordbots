"Bat";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "blindsight 60 ft."
_speed_: "5 ft., fly 30 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "12"
_stats_: | 2 (-4) | 15 (+2) | 8 (-1) | 2 (-4) | 12 (+1) | 4 (-3) |

___Echolocation.___ The bat can't use its blindsight while deafened.

___Keen Hearing.___ The bat has advantage on Wisdom (Perception) checks that rely on hearing.

**Actions**

___Bite.___ Melee Weapon Attack: +0 to hit, reach 5 ft., one creature. Hit: 1 piercing damage.