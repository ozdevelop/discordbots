"Vine Lord's Tendril Puppet";;;_size_: Medium plant
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft. (blind beyond this radius), passive Perception 8"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "blinded, deafened"
_speed_: "30 ft."
_hit points_: "34 (4d8 + 16)"
_armor class_: "13 (studded leather armor)"
_stats_: | 16 (+3) | 12 (+1) | 18 (+4) | 6 (-2) | 6 (-2) | 8 (-1) |

___Poor Vision.___ Tendril puppets see almost nothing beyond 30 feet away.

___Regeneration.___ The tendril puppet regains 5 hit points at the start of its turn if it has at least 1 hit point and is in jungle terrain.

___Root Mind.___ Within a vine lord's forest or jungle, the tendril puppet's blindsight extends to 60 feet, it succeeds on all Wisdom (Perception) checks, and it can't be surprised.

___Green Strider.___ The tendril puppet ignores movement restrictions and damage caused by natural undergrowth.

___Magic Resistance.___ The tendril puppet has advantage on saving throws against spells and other magical effects.

**Actions**

___Assegai.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d6 + 3) piercing damage, or 7 (1d8 + 3) piercing damage if used with two hands to make a melee attack.

___Hurl Thorns.___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., one target. Hit: 12 (2d8 + 3) piercing damage, and the thorn explodes in a 10-foot-radius sphere centered on the target. Every creature in the affected area other than the original target takes 4 (1d8) piercing damage, or half damage with a successful DC 13 Dexterity saving throw.

