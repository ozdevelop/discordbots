"Mavka";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "12 (8400 XP)"
_languages_: "Common, Infernal, Sylvan"
_skills_: "Athletics +9, Nature +5, Perception +5"
_senses_: "darkvision 90 ft., passive Perception 15"
_saving_throws_: "Str +9, Dex +6, Con +8, Cha +8"
_damage_immunities_: "cold, lightning"
_damage_resistances_: "acid, fire, necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft."
_hit points_: "170 (20d8 + 80)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 15 (+2) | 18 (+4) | 13 (+1) | 13 (+1) | 18 (+4) |

___Innate Spellcasting.___ The mavka's innate spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). She can innately cast the following spells, requiring no material components:

Constant: protection from evil and good

At will: create or destroy water, dancing lights, ray of frost, resistance, witch bolt

3/day each: darkness, hold person, inflict wounds, invisibility, silence

1/day each: animate dead, bestow curse, blindness/deafness, contagion, dispel magic, vampiric touch

___Nightmare Mount.___ A mavka is bonded to a nightmare when it is created. Mavkas are encountered with their mounts 95___Sunlight Hypersensitivity.___ The mavka takes 20 radiant damage when she starts her turn in sunlight. While in sunlight, she has disadvantage on attack rolls and ability checks.

**Actions**

___Multiattack.___ The mavka makes two slam attacks.

___Slam.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 23 (4d8 + 5) bludgeoning damage plus 11 (2d10) necrotic damage.

