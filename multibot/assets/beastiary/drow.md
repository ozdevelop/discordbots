"Drow";;;_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +2, Stealth +4"
_speed_: "30 ft."
_hit points_: "13 (3d8)"
_armor class_: "15 (chain shirt)"
_stats_: | 10 (0) | 14 (+2) | 10 (0) | 11 (0) | 11 (0) | 12 (+1) |

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's spellcasting ability is Charisma (spell save DC 11). It can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire_

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Hand Crossbow.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour. If the saving throw fails by 5 or more, the target is also unconscious while poisoned in this way. The target wakes up if it takes damage or if another creature takes an action to shake it awake.
