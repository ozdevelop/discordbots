"Grim Puppeteer";;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Infernal, telepathy 120 ft."
_skills_: "Deception +10, Performance +10, Persuasion +7, Sleight of Hand +6"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Dex +7, Cha +6"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical, nonsilvered weapons"
_speed_: "30 ft."
_hit points_: "98 (15d8 + 30)"
_armor class_: "16 (natural armor)"
_stats_: | 15 (+2) | 17 (+3) | 14 (+2) | 14 (+2) | 10 (+0) | 18 (+4) |

___Legion of Puppets.___ The puppeteer crafts and distributes
Grinning Marionettes. The puppeteer may use its
action to see through the eyes of any of its
marionettes. While seeing through the eyes of a
marionette, the puppeteer is blind to everything
happening around its body. The puppeteer is in
constant telepathic communication with all of its
puppets and they follow its will.

___Innate Spellcasting.___ The puppeteer’s innate spellcasting
ability is Charisma (spell save DC 15). It can innately
cast the following spells, requiring no components:

* At will: _alter self, charm person, crown of madness_

* 3/Day: _dominate person, hold person_

___Master Ventriloquist.___ The puppeteer can cast spells
without any somatic or verbal components and can
perfectly mimic any voice it has ever heard.

**Actions**

___Multiattack.___ The puppeteer makes three attacks with its
razor strings.

___Razor Strings.___ Melee Weapon Attack: +6 to hit, reach
15ft., one target. Hit: 10 (2d6 + 3) slashing damage.

___Strings Attached (Recharge 5-6).___ The puppeteer
teleports to an unoccupied space within 120 feet and
makes three attacks with its razor strings against a
single target, then casts dominate person on them. If at
least two of puppeteer’s attacks hit, the target has
disadvantage on their saving throw against the spell.
