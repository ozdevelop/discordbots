"Bucca";;;_size_: Tiny fey
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Darakhul, Dwarvish"
_skills_: "Perception +1, Sleight of Hand +7, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "20 ft., fly 30 ft."
_hit points_: "27 (5d4 + 15)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 16 (+3) | 17 (+3) | 13 (+1) | 9 (-1) | 16 (+3) |

___Flyby.___ The bucca doesn't provoke an opportunity attack when it flies out of an enemy's reach.

___Vulnerability to Sunlight.___ A bucca takes 1 point of radiant damage for every minute it is exposed to sunlight.

___Innate Spellcasting.___ The bucca's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no material components:

* At will: _invisibility_

* 3/day each: _darkness, ensnaring strike, locate object_

**Actions**

___Dagger.___ Melee Weapon Damage: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage, and the target must succeed on a DC 13 Constitution saving throw against poison or take 1d2 Strength damage. The target must repeat the saving throw at the end of each of its turns, and it loses another 1d2 Strength for each failed saving throw. The effect ends when one of the saving throws succeeds or automatically after 4 rounds. All lost Strength returns after a long rest.

