"Giant Weasel";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +5"
_speed_: "40 ft."
_hit points_: "9 (2d8)"
_armor class_: "13"
_stats_: | 11 (0) | 16 (+3) | 10 (0) | 4 (-3) | 12 (+1) | 5 (-3) |

___Keen Hearing and Smell.___ The weasel has advantage on Wisdom (Perception) checks that rely on hearing or smell.

**Actions**

___Bite.___ Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.