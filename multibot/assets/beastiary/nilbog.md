"Nilbog";;;_size_: Small humanoid (goblinoid)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +6"
_speed_: "30 ft."
_hit points_: "7 (2d6)"
_armor class_: "13 (leather armor)"
_stats_: | 8 (-1) | 14 (+2) | 10 (0) | 10 (0) | 8 (-1) | 15 (+2) |

___Innate Spellcasting.___ The nilbog's innate spellcasting ability is Charisma (spell save DC 12). It can innately cast the following spells, requiring no material components:

At will: mage hand, Tasha's hideous laughter, vicious mockery 1/day: confusion

___Nilbogism.___ Any creature that attempts to damage the nilbog must first succeed on a DC 12 Charisma saving throw or be charmed until the end of the creature's next turn. A creature charmed in this way must use its action praising the nilbog. The nilbog can't regain hit points, including through magical healing, except through its Reversal of Fortune reaction.

___Nimble Escape.___ The nilbog can take the Disengage or Hide action as a bonus action on each of its turns.

**Actions**

___Fool's Scepter.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) bludgeoning damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6+2) piercing damage.

**Reactions**

___Reversal of Fortune.___ In response to another creature dealing damage to the nilbog, the nilbog reduces the damage to 0 and regains 1d6 hit points.