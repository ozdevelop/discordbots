"Four-Armed Gargoyle";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons not made of adamantine"
_condition_immunities_: "exhaustion, petrified, poisoned"
_speed_: "30 ft., fly 60 ft."
_hit points_: "55 (10d8 + 10)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 12 (+1) | 6 (-2) | 11 (+0) | 7 (-2) |

___False Appearance.___ While the gargoyle remains motionless, it is
indistinguishable from an inanimate statue.

**Actions**

___Multiattack.___ The gargoyle makes four attacks: one with its bite, two with its claws, and one gore.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage.

___Gore.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.
