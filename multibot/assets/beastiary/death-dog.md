"Death Dog";;;_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_senses_: "darkvision 120 ft."
_skills_: "Perception +5, Stealth +4"
_speed_: "40 ft."
_hit points_: "39 (6d8+12)"
_armor class_: "12"
_stats_: | 15 (+2) | 14 (+2) | 14 (+2) | 3 (-4) | 13 (+1) | 6 (-2) |

___Two-Headed.___ The dog has advantage on Wisdom (Perception) checks and on saving throws against being blinded, charmed, deafened, frightened, stunned, or knocked unconscious.

**Actions**

___Multiattack.___ The dog makes two bite attacks.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage. If the target is a creature, it must succeed on a DC 12 Constitution saving throw against disease or become poisoned until the disease is cured. Every 24 hours that elapse, the creature must repeat the saving throw, reducing its hit point maximum by 5 (1d10) on a failure. This reduction lasts until the disease is cured. The creature dies if the disease reduces its hit point maximum to 0.