"The Wretched";;;_page_number_: 233
_size_: Small monstrosity
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "-"
_senses_: "darkvision 60 ft., passive Perception 8"
_speed_: "40 ft."
_hit points_: "10  (4d6 - 4)"
_armor class_: "15 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing while in dim light or darkness"
_stats_: | 7 (-1) | 12 (+1) | 9 (0) | 5 (-2) | 6 (-2) | 5 (-2) |

___Wretched Pack Tactics.___ The Wretched has advantage on an attack roll against a creature if at least one of the Wretched's allies is within 5 feet of the creature and the ally isn't incapacitated. The Wretched otherwise has disadvantage on attack rolls.

**Actions**

___Bite___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (1d10 + 1) piercing damage, and the Wretched attaches to the target. While attached, the Wretched can't attack, and at the start of each of the Wretched's turns, the target takes 6 (1d10 + 1) necrotic damage.
The attached Wretched moves with the target whenever the target moves, requiring none of the Wretched's movement. The Wretched can detach itself by spending 5 feet of its movement on its turn. A creature, including the target, can use its action to detach a Wretched.