"Summer Eladrin";;;_page_number_: 196
_size_: Medium fey (elf)
_alignment_: chaotic neutral
_challenge_: "10 (5900 XP)"
_languages_: "Common, Elvish, Sylvan"
_senses_: "darkvision 60 ft., passive Perception 9"
_skills_: "Athletics +8, Intimidation +8"
_speed_: "50 ft."
_hit points_: "127  (17d8 + 51)"
_armor class_: "19 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 19 (+4) | 21 (+5) | 16 (+3) | 14 (+2) | 12 (+1) | 18 (+4) |

___Fearsome Presence.___ Any non-eladrin creature that starts its turn within 60 feet of the eladrin must make a DC 16 Wisdom saving throw. On a failed save, the creature becomes frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to any eladrin's Fearsome Presence for the next 24 hours.

___Fey Step (Recharge 4-6).___ As a bonus action, the eladrin can teleport up to 30 feet to an unoccupied space it can see.

___Magic Resistance.___ The eladrin has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The eladrin makes two weapon attacks.

___Longsword___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage plus 4 (1d8) fire damage, or 15 (2d10 + 4) slashing damage plus 4 (1d8) fire damage if used with two hands.

___Longbow___ Ranged Weapon Attack: +9 to hit, range 150/600 ft., one target. Hit: 14 (2d8 + 5) piercing damage plus 4 (1d8) fire damage.

**Reactions**

___Parry___ The eladrin adds 3 to its AC against one melee attack that would hit it. To do so, the eladrin must see the attacker and be wielding a melee weapon.
