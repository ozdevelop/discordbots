"Kamasuhn";;;_size_: Large celestial
_alignment_: lawful good
_challenge_: "12 (8,400 XP)"
_languages_: "Celestial, Common"
_skills_: "Acrobatics +12, Insight +10, Intimidation +11, Perception +10, Persuasion +11, Religion +9"
_senses_: "darkvision 60 ft., passive Perception 20"
_saving_throws_: "Str +6, Wis +6"
_damage_resistances_: "necrotic; bludgeoning, piercing and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened"
_speed_: "30 ft., fly 30 ft."
_hit points_: "238 (28d10 + 84)"
_armor class_: "17 (natural armor)"
_stats_: | 15 (+2) | 18 (+4) | 16 (+3) | 12 (+1) | 14 (+2) | 16 (+3) |

**Actions**

___Multiattack.___ The Kamasuhn makes two attacks with its glaive.

___Glaive.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 13
(2d10 + 2) slashing damage.

___Radiant Strike (Recharge 6).___ On a successful strike with its glaive,
the kamasuhn unleashes a burst of radiant energy in a 60-foot radius. All
creatures in this area must make a DC 13 Dexterity saving throw, taking
27 (6d8) radiant damage on a failed save, or half as much damage on a
successful one.
