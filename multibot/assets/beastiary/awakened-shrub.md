"Awakened Shrub";;;_size_: Small plant
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_languages_: "one language known by its creator"
_speed_: "20 ft."
_hit points_: "10 (3d6)"
_armor class_: "9"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "piercing"
_stats_: | 3 (-4) | 8 (-1) | 11 (0) | 10 (0) | 10 (0) | 6 (-2) |

___False Appearance.___ While the shrub remains motionless, it is indistinguishable from a normal shrub.

**Actions**

___Rake.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 1 (1d4 - 1) slashing damage.