"Void Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: chaotic neutral
_challenge_: "2 (450 XP)"
_languages_: "Common, Draconic, Void Speech"
_skills_: "Perception +3, Stealth +2"
_senses_: ", passive Perception 13"
_saving_throws_: "Dex +4, Con +5, Wis +1, Cha +5"
_damage_immunities_: "cold"
_speed_: "30 ft., fly 60 ft. (hover)"
_hit points_: "45 (6d8 + 18)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 17 (+3) | 12 (+1) | 9 (-1) | 17 (+3) |

___Chill of the Void.___ Cold damage dealt by the void dragon ignores resistance to cold damage, but not cold immunity.

___Void Dweller.___ As an ancient void dragon.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage plus 3 (1d6) cold damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons:

* ___Gravitic Breath.___ The dragon exhales a 15-foot cube of powerful localized gravity, originating from the dragon. Falling damage in the area increases to 1d10 per 10 feet fallen. When a creature starts its turn within the area or enters it for the first time in a turn, including when the dragon creates the field, must make a DC 13 Dexterity saving throw. On a failure the creature is restrained. On a success the creature's speed is halved as long as it remains in the field. A restrained creature repeats the saving throw at the end of its turn. The field persists until the dragon's breath recharges, and it can't use gravitic breath twice consecutively.

* ___Stellar Flare Breath.___ The dragon exhales star fire in a 15-foot cone. Each creature in that area must make a DC 13 Dexterity saving throw, taking 10 (3d6) fire damage and 10 (3d6) radiant damage on a failed save, or half as much damage on a successful one.

**Reactions**

___Void Twist.___ When the dragon is hit by a ranged attack it can create a small rift in space to increase its AC by 2 against that attack. If the attack misses because of this increase the dragon can choose a creature within 30 feet to become the new target for that attack. Use the original attack roll to determine if the attack hits the new target.

