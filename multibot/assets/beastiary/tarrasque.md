"Tarrasque";;;_size_: Gargantuan monstrosity (titan)
_alignment_: unaligned
_challenge_: "30 (155,000 XP)"
_senses_: "blindsight 120 ft."
_damage_immunities_: "fire, poison, bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Int +5, Wis +9, Cha +9"
_speed_: "40 ft."
_hit points_: "676 (33d20+330)"
_armor class_: "25 (natural armor)"
_condition_immunities_: "charmed, frightened, paralyzed, poisoned"
_stats_: | 30 (+10) | 11 (0) | 30 (+10) | 3 (-4) | 11 (0) | 11 (0) |

___Legendary Resistance (3/Day).___ If the tarrasque fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The tarrasque has advantage on saving throws against spells and other magical effects.

___Reflective Carapace.___ Any time the tarrasque is targeted by a magic missile spell, a line spell, or a spell that requires a ranged attack roll, roll a d6. On a 1 to 5, the tarrasque is unaffected. On a 6, the tarrasque is unaffected, and the effect is reflected back at the caster as though it originated from the tarrasque, turning the caster into the target.

___Siege Monster.___ The tarrasque deals double damage to objects and structures.

**Actions**

___Multiattack.___ The tarrasque can use its Frightful Presence. It then makes five attacks: one with its bite, two with its claws, one with its horns, and one with its tai l. It can use its Swallow instead of its bite.

___Bite.___ Melee Weapon Attack: +19 to hit, reach 10 ft., one target. Hit: 36 (4d12 + 10) piercing damage. If the target is a creature, it is grappled (escape DC 20). Until this grapple ends, the target is restrained, and the tarrasque can't bite another target.

___Claw.___ Melee Weapon Attack: +19 to hit, reach 15 ft., one target. Hit: 28 (4d8 + 10) slashing damage.

___Horns.___ Melee Weapon Attack: +19 to hit, reach 10 ft., one target. Hit: 32 (4d10 + 10) piercing damage.

___Tail.___ Melee Weapon Attack: +19 to hit, reach 20 ft., one target. Hit: 24 (4d6 + 10) bludgeoning damage. If the target is a creature, it must succeed on a DC 20 Strength saving throw or be knocked prone.

___Frightful Presence.___ Each creature of the tarrasque's choice within 120 feet of it and aware of it must succeed on a DC 17 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, with disadvantage if the tarrasque is within line of sight, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the tarrasque's Frightful Presence for the next 24 hours.

___Swallow.___ The tarrasque makes one bite attack against a Large or smaller creature it is grappling. If the attack hits, the target takes the bite's damage, the target is swallowed, and the grapple ends. While swallowed, the creature is blinded and restrained, it has total cover against attacks and other effects outside the tarrasque, and it takes 56 (16d6) acid damage at the start of each of the tarrasque's turns.

If the tarrasque takes 60 damage or more on a single turn from a creature inside it, the tarrasque must succeed on a DC 20 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet of the tarrasque. If the tarrasque dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 30 feet of movement, exiting prone.

**Legendary** Actions

The tarrasque can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The tarrasque regains spent legendary actions at the start of its turn.

___Attack.___ The tarrasque makes one claw attack or tail attack.

___Move.___ The tarrasque moves up to half its speed.

___Chomp (Costs 2 Actions).___ The tarrasque makes one bite attack or uses its Swallow.