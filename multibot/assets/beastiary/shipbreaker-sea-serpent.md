"Shipbreaker Sea Serpent";;;_size_: Gargantuan dragon
_alignment_: chaotic neutral
_challenge_: "30 (155,000 XP)"
_languages_: "Deep Speech, Draconic"
_skills_: "Perception +22, Stealth +18"
_senses_: "blindsight 120 ft., darkvision 120 ft., passive Perception 22"
_saving_throws_: "Str +19 Dex +9, Con +18, Wis +12"
_damage_immunities_: "cold, poison; bludgeoning, piercing, and slashing from nonmagical weapons."
_condition_immunities_: "frightened, paralyzed, poisoned, restrained"
_speed_: "20 ft., swim 60 ft."
_hit points_: "663 (34d20 + 306)"
_armor class_: "19 (natural armor)"
_stats_: | 30 (+10) | 10 (+0) | 29 (+9) | 11 (+0) | 17 (+3) | 11 (+0) |

___Amphibious.___ The sea serpent can breathe air and water.

___Keen Scent.___ This sea serpent can notice creatures by scent in a 180-foot
radius underwater and can detect blood in the water at a range of up to a
mile.

___Legendary Resistance (3/day).___ If the sea serpent fails a saving throw, it
can choose to succeed instead.

___Siege Monster.___ The sea serpent deals double damage to objects and
structures

**Actions**

___Multiattack.___ The sea serpent can use its frightful presence and make
two attacks: one with its bite and one with its tail.

___Bite.___ Melee Weapon Attack: +19 to hit, reach 20 ft., one target. Hit: 45
(10d6 + 10) piercing damage and 28 (8d6) poison damage.

___Ram.___ Melee Weapon Attack: +19 to hit, reach 10 ft., one target. Hit: 65
(10d10 + 10) bludgeoning damage.

___Tail.___ Melee Weapon Attack: +19 to hit, reach 30 ft., one target. Hit: 37
(6d8 + 10) bludgeoning damage.

___Frightful Presence.___ Each creature of the sea serpent’s choice that is
within 120 feet of the sea serpent and aware of it must succeed on a DC 23
Wisdom saving throw or become frightened for 1 minute. A creature can
repeat the saving throw at the end of each of its turns, ending the effect on
itself on a success. If a creature’s saving throw is successful or the effect
ends for it, the creature is immune to the sea serpent’s Frightful Presence
for the next 24 hours.

___Capsize.___ If the sea serpent moves at least 30 feet straight toward
a watercraft (boat, ship, ferry, etc.) and then hits it with a ram attack on
the same turn, there is a chance that it will capsize. All occupants of the
watercraft must make Dexterity saving throw when the craft is rammed (see
below). To determine if the watercraft capsizes, consult the following table:
* ___Small___ The craft is destroyed, all on board must
make a successful DC 23 Dexterity saving
throw to avoid being stunned. On a failed
save, passengers each take 65 (10d10 + 10)
bludgeoning damage, are trapped in the
wreckage, and are stunned for 1 minute, or
half as much on a successful save and are
thrown clear of the wreckage.
* ___Medium___ The craft must make a generic DC 20
saving throw to avoid being destroyed.
If the craft fails the save by 5 or more, it
capsizes and all on board must make a DC
18 Dexterity saving throw to avoid being
pulled under with the ship.
If the save is successful, the ship takes
120 (20d10 + 10) damage and those
on board must make a successful DC
17 Dexterity saving throw. On a failed
save, the passenger takes 48 (7d10 + 10)
bludgeoning damage and is knocked
prone (or overboard if they are near the
ship’s edge), or half as much without being
knocked prone on a success.
* ___Large___ The craft must make a generic DC 18
saving throw to avoid being capsized. If it
fails the save by 5 or more, the ship capsizes
and all on board must make a DC 16
Dexterity saving throw to avoid being pulled
under with the ship.
If the save is successful, the ship takes 103
(17d10 + 10) damage and those on board
must make a DC 14 Dexterity saving throw.
On a failed save, the passenger takes 37
(5d10 + 10) bludgeoning damage and is
knocked prone (or overboard if they are
near the ship’s edge), or half as much
without being knocked prone on a success.
* ___Huge___ The craft must make a generic DC 16
saving throw to avoid being capsized. If it
fails the save by 5 or more, the ship capsizes
and all on board must make a DC 15
Dexterity saving throw to avoid being pulled
under with the ship.
If the save is successful, the ship takes 81
(13d10 + 10) damage and those on board
must make a DC 12 Dexterity saving throw.
On a failed save, the passenger takes 26
(3d10 + 10) bludgeoning damage and is
knocked prone (or overboard if they are
near the ship’s edge), or half as much
without being knocked prone on a success.
* ___Gargantuan___ The craft must make a generic DC 14
saving throw to avoid being capsized. If it
fails the save by 5 or more, the ship capsizes
and all on board must make a DC 13
Dexterity saving throw to avoid being pulled
under with the ship.
If the save is successful, the ship takes 65
(10d10 + 10) damage and those on board
must make a DC 10 Dexterity saving throw.
On a failed save, the passenger takes 15
(1d10 + 10) bludgeoning damage and is
knocked prone (or overboard if they are
near the ship’s edge), or half as much
without being knocked prone on a success.

**Legendary** Actions

The sea serpent can take 3 legendary actions, choosing from the options
below. Only one legendary action can be used at a time and only at the end
of another creature’s turn. The sea serpent regains spent legendary actions
at the start of its turn.

___Detect.___ The sea serpent makes a Wisdom (Perception) check.

___Tail Attack.___ The sea serpent makes a tail attack.

___Breach (Costs 3 Actions).___ The sea serpent dives deep below the surface
of the water then swims rapidly upward, propelling itself out of the water,
often clearing the surface, before violently slamming into the surface with
great force. Each creature within 60 feet of the sea serpent must succeed
on a DC 23 Dexterity saving or take 27 (5d10) bludgeoning damage and
be knocked prone.
