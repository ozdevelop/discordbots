"Allosaurus";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_skills_: "Perception +5"
_speed_: "60 ft."
_hit points_: "51 (6d10+18)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 13 (+1) | 17 (+3) | 2 (-4) | 12 (+1) | 5 (-3) |

___Pounce.___ If the allosaurus moves at least 30 ft. straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is prone, the allosaurus can make one bite attack against it as a bonus action.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage.