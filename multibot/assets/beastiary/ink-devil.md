"Ink Devil";;;_size_: Small fiend
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Celestial, Common, Draconic, Infernal; telepathy (120 ft.)"
_skills_: "Arcana +9, Deception +8, History +9, Stealth +8"
_senses_: "darkvision 120 ft., passive Perception 9"
_saving_throws_: "Dex +6"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "54 (12d6 + 12)"
_armor class_: "14"
_stats_: | 12 (+1) | 18 (+4) | 12 (+1) | 20 (+5) | 8 (-1) | 18 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The ink devil's spellcasting ability is Charisma (spell save DC 14). The ink devil can cast the following spells, requiring no material components:

* At will: _detect magic, illusory script, invisibility, teleportation _(self plus 50 lb of objects only)

* 1/day each: _glyph of warding, planar ally _(1d4 + 1 lemures 40 percent, or 1 ink devil 25 percent)

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., single target. Hit: 11 (2d6 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., single target. Hit: 14 (3d6 + 4) slashing damage.

___Corrupt Scroll.___ An ink devil can corrupt the magic within any scroll by touch. Any such corrupted scroll requires a DC 13 Intelligence saving throw to use successfully. If the check fails, the scroll's spell affects the caster if it is an offensive spell, or it affects the nearest devil if it is a beneficial spell.

___Devil's Mark.___ Ink devils can flick ink from their fingertips at a single target within 15 feet of the devil. The target must succeed on a Dexterity saving throw (DC 13), or the affected creature gains a devil's mark: a black, red, or purple tattoo in the shape of an archduke's personal seal (most often Mammon or Totivillus but sometimes Arbeyach, Asmodeus, Beelzebub, Dispater, or others). All devils have advantage on spell attacks made against the devil-marked creature, and the creature has disadvantage on saving throws made against spells and abilities used by devils. The mark can be removed only by a remove curse spell or comparable magic. In addition, the mark detects as faintly evil and often shifts its position on the body. Paladins, witchfinders, and some clerics may consider such a mark proof that a creature has made a pact with a devil.

**Bonus** Actions

___Disrupt Concentration.___ Their sharp, shrill tongues and sharper claws make ink devils more distracting than their own combat prowess might indicate. An ink devil can force a single foe within 30 feet of the ink devil to make a DC 13 Wisdom saving throw or lose concentration until the beginning of the target's next turn.
