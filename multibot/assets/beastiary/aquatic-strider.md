"Aquatic Strider";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "understands Common and Aquan but can’t speak."
_senses_: "passive Perception 10"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft., swim 30 ft."
_hit points_: "51 (6d10 + 18)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 8 (-1) | 16 (+3) | 6 (-2) | 10 (+0) | 6 (-2) |

___Shapeshifter.___ The strider can use its bonus action to
transform its body into a tall, swift, four-legged
construct held together by streams of water, or into
a tough, slow-moving, cannon construct. While in
the cannon form, the strider has its AC increased to
16 and its movement speed reduced to 20 ft. The
strider falls to pieces when it dies, regardless of its
current form.

**Actions**

___Multiattack.___ The strider makes two attacks with its
impale ability.

___Impale (Strider Form Only).___ Melee Weapon Attack: +5 to hit, reach 5ft., one target. Hit: 7 (1d8 + 3)
piercing damage.

___Water Orb (Cannon Form Only).___ Ranged Weapon
Attack: +5 to hit, range 30/90 ft., one target. Hit:
18 (4d8) cold damage and the target’s speed is
reduced by 10 feet on their next turn.

___Aqua Cannon (Cannon Form Only) (Recharge 6).___ The
strider unleashes a torrent of powerful water from
its cannon in a 60-foot line. Any creature in this
area must make a DC 12 Dexterity saving throw,
taking 7 (2d6) bludgeoning damage and 7 (2d6)
cold damage and be pushed 15 feet on a failed
save, or half as much damage and not pushed on a
successful one. Any creatures behind the initial
target hit by this ability have advantage on this
saving throw.
