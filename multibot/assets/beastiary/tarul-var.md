"Tarul Var";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "13 (10,000 XP)"
_languages_: "Abyssal, Common, Infernal, Primordial, Thayan"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +9, History +9, Insight +7, Perception +7"
_damage_immunities_: "poison"
_saving_throws_: "Con +8, Int +9, Wis +7"
_speed_: "30 ft."
_hit points_: "105 (14d8+42)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_damage_resistances_: "cold, lightning, necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 11 (0) | 16 (+3) | 16 (+3) | 19 (+4) | 14 (+2) | 16 (+3) |

___Source.___ tales from the yawning portal,  page 244

___Undead Nature.___ A lich doesn't require air, food, drink, or sleep.

___Focused Conjuration.___ While Var is concentrating on a conjuration spell, his concentration can't be broken as a result of taking damage.

___Legendary Resistance (3/Day).___ If Var fails a saving throw, he can choose to succeed instead .

___Rejuvenation.___ If Var is destroyed but his phylactery remains intact, Var gains a new body in ldlO days, regaining all his hit points and becoming active again. The new body appears within 5 feet of the phylactery.

___Spellcasting.___ Var is a 12th-level spellcaster. His spellcasting ability is Intelligence (spell save DC 17, +9 to hit with spell attacks). He has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, mage hand, minor illusion, prestidigitation, ray of frost_

* 1st level (4 slots): _detect magic, magic missile, shield, unseen servant\* _

* 2nd level (3 slots): _detect thoughts, flaming sphere\*, mirror image, scorching ray_

* 3rd level (3 slots): _counterspell, dispel magic, fireball_

* 4th level (3 slots): _dimension door\*, Evard's black tentacles\* _

* 5th level (3 slots): _cloudkill\*, scrying_

* 6th level (1 slot): _circle of death_

*Conjuration spell of 1st level or higher.

___Turn Resistance.___ Var has advantage on saving throws against any effect that turns undead .

**Actions**

___Paralyzing Touch.___ Melee Spell Attack: +9 to hit, reach 5 ft., one creature. Hit: 10 (3d6) cold damage. The target must succeed on a DC 17 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Benign Transposition.___ Var teleports up to 30 feet to an unoccupied space he can see. Alternatively, he can choose a space within range that is occupied by a Small or Medium creature. If that creature is willing, both creatures teleport, swapping places. Var can use this feature again only after he finishes a long rest or casts a conjuration spell of 1st level or higher.

**Legendary** Actions

Var can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Var regains spent legendary actions at the start of his turn.

___Cantrip.___ Var casts a cantrip.

___Paralyzing Touch (Costs 2 Actions).___ Var uses Paralyzing Touch .

___Frightening Gaze (Costs 2 Actions).___ Var fixes his gaze on one creature he can see within 10 feet of him . The target must succeed on a DC 17 Wisdom saving throw against this magic or become frightened for 1 minute. The frightened target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a target's saving throw is successful or the effect ends for it, the target is immune to Var's gaze for the next 24 hours.
