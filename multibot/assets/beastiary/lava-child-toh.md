"Lava Child";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Ignan, Lava Child"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft. passive Perception 14"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire; bludgeoning, piercing, and slashing from metal weapons"
_damage_resistances_: "force"
_speed_: "30 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "13 (natural armor)"
_stats_: | 13 (+1) | 11 (+0) | 13 (+1) | 10 (+0) | 11 (+0) | 11 (+0) |

___Heated Body.___ A creature that touches the lava child or hits it with a
melee attack while within 5 ft. of it takes 7 (2d6) fire damage.

___Metal Immunity.___ Lava children are unaffected by metal. They can walk
through solid metal doors as if the door wasn’t there. Metal weapons, even
magical, have no effect on lava children. Lava children make all attacks
with advantage against foes wearing metal armor.

___Water Vulnerability.___ For every 1 gallon of water splashed on the lava
child, it takes 3 cold damage.

**Actions**

___Multiattack.___ The lava child makes one bite attack and one attack with
its claws.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) piercing damage.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5
(1d8 + 1) slashing damage.
