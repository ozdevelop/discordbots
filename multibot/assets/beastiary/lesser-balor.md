"Lesser Balor";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "10 (5,900 XP)"
_languages_: "Abyssal, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 13"
_saving_throws_: "Str +9, Con +8, Wis +6, Cha +8"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 80 ft."
_hit points_: "123 (13d10 + 52)"
_armor class_: "18 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 18 (+4) | 16 (+3) | 15 (+2) | 18 (+4) |

___Fiery End.___ When the lesser balor dies, it
explodes. Each creature within 20 feet of it must
make a DC 16 Dexterity saving throw. On a failed
save, a creature takes 35 (10d6) fire damage, or
half as much damage on a successful one. The
explosion destroys the lesser balor’s weapons.

___Singeing Aura.___ At the start of each of the lesser
balor’s turns, each creature within 5 feet of
it takes 7 (2d6) fire damage. A creature that
touches the lesser balor or hits it with a melee
attack while within 5 feet of it takes 7 (2d6)
fire damage.

___Magic Resistance.___ The lesser balor has advantage on saving throws against spells and other
magical effects.

___Magic Weapons.___ The lesser balor’s weapon
attacks are magical.

**Actions**

___Multiattack.___ The lesser balor makes one attack
with its longsword and one attack with its whip.

___Longsword.___ Melee Weapon Attack: +9 to hit,
reach 10 ft., one target. Hit: 14 (2d8 + 5) slashing
damage plus 9 (2d8) lightning damage. If the
lesser balor scores a critical hit, it rolls damage
dice three times, instead of twice.

___Whip.___ Melee Weapon Attack: +9 to hit, reach 30
ft., one target. Hit: 12 (2d6 + 5) slashing damage
plus 7 (2d6) fire damage, and the target must
succeed on a DC 16 Strength saving throw or be
pulled up to 15 feet toward the lesser balor.
