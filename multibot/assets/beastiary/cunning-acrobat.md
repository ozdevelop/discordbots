"Cunning Acrobat";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages and Thieves' Cant"
_skills_: "Acrobatics +7, Deception +3, Perception +3, Sleight of Hand +7, Stealth +5"
_senses_: "passive Perception 13"
_saving_throws_: "Dex +5, Int +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "22 (4d8 + 4)"
_armor class_: "15 (studded leather)"
_stats_: | 10 (+0) | 16 (+3) | 12 (+1) | 15 (+2) | 12 (+1) | 12 (+1) |

___Cunning Action.___ As a bonus action, the acrobat can
take the Dash, Disengage, or Hide action.

___Naturally Acrobatic.___ The acrobat has a climb speed
of 30 ft. In addition, whenever it makes a running
jump, the distance it can jump is increased by 3
feet.

___Sneak Attack (1/Turn).___ The acrobat deals an extra 7
(2d6) damage when it hits a target with a weapon
attack and has advantage on the attack roll, or when
the target is within 5 feet of an ally of the acrobat
that isn't incapacitated and the acrobat doesn't
have disadvantage on the attack roll.

**Actions**

___Multiattack.___ The acrobat makes two melee attacks:
one with its shortsword and one with its dagger.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 2
(1d4) piercing damage.
