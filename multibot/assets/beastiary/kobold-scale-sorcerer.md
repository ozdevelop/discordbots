"Kobold Scale Sorcerer";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +2, Medicine +1"
_speed_: "30 ft."
_hit points_: "27 (5d6+10)"
_armor class_: "15 (natural armor)"
_stats_: | 7 (-2) | 15 (+2) | 14 (+2) | 10 (0) | 9 (-1) | 14 (+2) |

___Spellcasting.___ The kobold is a 3rd-level spellcaster. Its spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). It has the following sorcerer spells prepared:

* Cantrips (at will): _fire bolt, mage hand, mending, poison spray_

* 1st level (4 slots): _charm person, chromatic orb, expeditious retreat_

* 2nd level (2 slots): _scorching ray_

___Sorcery Points.___ Sorcery Points. The kobold has 3 sorcery points. It can spend 1 or more sorcery points as a bonus action to gain one of the following benefits:

* _Heightened Spell:_ When it casts a spell that forces a creature to a saving throw to resist the spell's effects, the kobold can spend 3 sorcery points to give one target of the spell disadvantage on its first saving throw against the spell.

* _Subtle Spell:_ When the kobold casts a spell, it can spend 1 sorcery point to cast the spell without any somatic or verbal components.

___Pack Tactics.___ The kobold has advantage on an attack roll against a creature it at least one of the kobold's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Sunlight Sensitivity.___ While in sunlight, the kobold has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 it, one target. Hit: 4 (1d4+2) piercing damage.
