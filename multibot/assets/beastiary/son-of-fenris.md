"Son Of Fenris";;;_size_: Huge monstrosity
_alignment_: chaotic evil
_challenge_: "12 (8400 XP)"
_languages_: "Common, Celestial, Draconic, Elvish, Dwarvish, Giant, Infernal, telepathy 60 ft."
_skills_: "Arcana +7, Intimidation +6, Religion +12"
_senses_: "truesight 60 ft., tremorsense 100 ft., passive Perception 14"
_saving_throws_: "Dex +7, Con +10, Wis +8"
_damage_immunities_: "cold, lightning, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "psychic, radiant"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "60 ft., burrow 15 ft. (30 ft. in ice or snow)"
_hit points_: "175 (14d12 + 84)"
_armor class_: "17 (natural armor)"
_stats_: | 26 (+8) | 16 (+3) | 23 (+6) | 16 (+3) | 18 (+4) | 14 (+2) |

___Keen Hearing and Smell.___ The son of Fenris has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Magic Weapons.___ The son of Fenris' weapon attacks are magical.

___Spellcasting.___ The son of Fenris is a 15th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 16, +8 to hit with spell attacks). It requires no material components to cast its spells. It has the following cleric spells prepared:

Cantrips (at will): guidance, light, sacred flame, spare the dying, thaumaturgy

1st level (4 slots): bane, command, guiding bolt, sanctuary

2nd level (3 slots): blindness/deafness, hold person, silence

3rd level (3 slots): animate dead, bestow curse, dispel magic

4th level (3 slots): banishment, death ward, locate creature

5th level (2 slots): contagion, scrying

6th level (1 slot): harm

7th level (1 slot): plane shift

8th level (1 slot): earthquake

___Trampling Charge.___ If the son of Fenris moves at least 20 feet straight toward a creature and hits it with a slam attack on that turn, that target must succeed on a DC 18 Strength saving throw or be knocked prone. If it is knocked prone, the son of Fenris can make another slam attack against it as a bonus action.

**Actions**

___Multiattack.___ The son of Fenris makes one bite attack and one slam attack.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 19 (2d10 + 8) piercing damage plus 5 (1d10) poison damage, and the target is grappled (escape DC 18). If the target was already grappled, it is swallowed instead. While swallowed, the target is blinded and restrained, it has total cover against attacks and other effects from outside the son of Fenris, and it takes 28 (8d6) acid damage at the start of each of the son of Fenris's turns. It can swallow only one creature at a time. If it takes 45 damage or more on a single turn from the swallowed creature, it must succeed on a DC 17 Constitution saving throw at the end of that turn or regurgitate the creature, which falls prone in a space within 10 feet of the son of Fenris. If the son of Fenris dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 15 feet of movement, exiting prone.

___Slam.___ Melee Weapon Attack: +12 to hit, reach 15 ft., one target. Hit: 19 (2d10 + 8) bludgeoning damage.

___Acid Breath (Recharge 5-6).___ The son of Fenris exhales acid in a 60-foot line that is 10 feet wide. Each creature in the line takes 45 (10d8) acid damage, or half damage with a successful DC 18 Dexterity saving throw.

