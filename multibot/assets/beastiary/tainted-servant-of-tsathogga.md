"Tainted Servant of Tsathogga";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 30 ft., passive Perception 14"
_damage_vulnerabilities_: "radiant"
_damage_resistances_: "bludgeoning, cold"
_speed_: "30 ft., swim 30 ft."
_hit points_: "45 (10d10)"
_armor class_: "17 (natural armor)"
_stats_: | 17 (+3) | 12 (+1) | 11 (+0) | 9 (-1) | 12 (+1) | 8 (-1) |

___Amphibious.___ A tainted servant of Tsathogga can breathe air or water.

___Slimy.___ Any attempt to grapple a tainted servant of Tsathogga is made
with disadvantage. Any contested grapple checks made by a tainted
servant of Tsathogga have advantage. All touch attack spells against a
tainted servant of Tsathogga are made with disadvantage.

**Actions**

___Frog’s Claw.___ Melee weapon attack: +6 to hit, range 5 ft., one target.
Hit: 7 (1d8+3) slashing damage.

___Chaotic Croak (Recharge 5-6).___ A tainted servant of Tsathogga can
croak, forcing all enemies within 50 ft. to attempt a DC 14 Wisdom
saving throw. Any who fail use their action on their next turn to make
an attack against the nearest creature if possible. A creature that fails
its saving throw and cannot attack the nearest creature does not
use its action on its turn.

___Tsathogga’s Spit (2/day).___ tainted servant of Tsathogga can
spit a viscous glob at a target. On a successful hit, the target
becomes restrained (Escape DC 14).

___Tsathogga’s Gift (1/day).___ A tainted servant of Tsathogga
releases a 20 ft. sphere of poisonous gas through its skin. Any
other creature that starts its turn within the range must make a
successful DC 12 Constitution saving throw or take 7 (2d6) poison
damage. This sphere follows the tainted servant and dissipates after 1
minute.
