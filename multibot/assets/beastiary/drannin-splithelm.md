"Drannin Splithelm";;;_size_: Medium humanoid (shield dwarf)
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Dwarvish"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +7, Intimidation +4"
_speed_: "25 ft."
_hit points_: "93 (11d8+44)"
_armor class_: "18 (plate)"
_damage_resistances_: "cold, poison"
_stats_: | 19 (+4) | 10 (0) | 18 (+4) | 11 (0) | 8 (-1) | 12 (+1) |

___Action Surge (Recharges after a Short or Long Rest).___ Drannin takes an additional action on his turn.

___Brute.___ A melee weapon deals one extra die of its damage when Drannin hits with it (included in the attack).

___Dwarven Resilience.___ Drannin has advantage on saving throws against poison.

___Indomitable (Recharges after a Short or Long Rest).___ Drannin can reroll a saving throw that he fails. He must use the new roll.

___Second  Wind (Recharges after a Short or Long Rest).___ Drannin can use a bonus action to regain 16 (1d10 + 11) hit points.

___Special Equipment.___ Drannin wears a control amulet for his shield guardian (see the Monster Manual) and a ring of cold resistance. He also carries a potion of frost giant strength.

**Actions**

___Multiattack.___ Drannin makes three attacks with his greataxe.

___Greataxe.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 17 (2d12 + 4) slashing damage.