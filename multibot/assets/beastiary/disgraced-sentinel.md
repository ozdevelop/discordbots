"Disgraced Sentinel";;;_size_: Medium celestial
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Insight +6, Perception +7"
_senses_: "darkvision 120ft., passive Perception 17"
_saving_throws_: "Dex +7, Wis +7, Cha +6"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhausted, frightened"
_speed_: "30 ft., fly 90 ft."
_hit points_: "111 (13d8 + 52)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 19 (+4) | 18 (+4) | 17 (+3) | 18 (+4) | 16 (+3) |

___Corrupted Angelic Weapons.___ The sentinel's weapons
are magical. When the sentinel hits with any
weapon, the weapon deals an extra 3d8 necrotic
damage (included in the attack).

___Magic Resistance.___ The sentinel has advantage on
saving throws against spells and other magical
effects.

**Actions**

___Multiattack.___ The sentinel makes two longbow
attacks.

___Longbow.___ Ranged Weapon Attack: +7 to hit, range
150/600 ft., one target. Hit: 8 (1d8 + 4) piercing
damage plus 13 (3d8) necrotic damage.

___Retreating Shot (Recharge 4-6).___ The sentinel leaps
back 50 feet without provoking attacks of
opportunity, then makes two shots against a single
target with its longbow. The first of these attacks is
made with advantage.

___Decaying Touch (3/Day).___ Melee Spell Attack: +7 to
hit, reach 5 ft., one target. Hit: 30 (6d8 + 3)
necrotic damage.

___Crash of Arrows (1/Day).___ The sentinel creates a 30
foot diameter runic circle centered on a location it
can see within 120 feet. Whenever a creature
enters this area for the first time on a turn or ends
its turn in this area, conjured arrows rain down
upon them. That creature must make a DC 15
Dexterity saving throw, taking 32 (7d8) piercing
damage on a failed save or half as much damage on
a successful one.
