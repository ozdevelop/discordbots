"Dark Witness Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "Darkvision 120 ft., Passive Perception 9"
_condition_immunities_: "Prone"
_speed_: "30 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 8 (-1) | 16 (+3) | 3 (-4) | 8 (-1) | 5 (-3) |

___Undead Fortitude.___ If damage reduces a dark witness
zombie to 0 hit points, it must make a Constitution
saving throw with a DC equal to 5 + the
damage taken, unless the damage is radiant or a
critical hit. On a success, the dark witness zombie
drops to 1 hit point instead.

**Actions**

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft.,
Hit: 4 (1d8) piercing damage.

___Aberrant Gaze.___ The dark witness zombie targets a
creature it can see within 120 ft. An eye at the end
of one of its tentacle arms uses one of the following
effects on the target.
* **Fear Gaze.** The target creature must succeed on
a DC 13 Wisdom saving throw or be frightened of
the dark witness for 1 minute. The target can repeat
the saving throw at the end of each of its turns,
ending the effect on a success.
* **Shocking Gaze.** The target creature must succeed
on a DC 13 Dexterity saving throw or take 28
(8d6) lightning damage on a failed save, or half as
much on a successful one.
