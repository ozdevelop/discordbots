"Large Spider";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "Blindsight 10 ft., darkvision 30 ft., Passive Perception 10"
_skills_: "Stealth +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "4 (1d6 + 1)"
_armor class_: "13"
_stats_: | 5 (-3) | 14 (+2) | 12 (+1) | 1 (-5) | 10 (+0) | 3 (-4) |

___Spider Climb.___ The large spider can climb difficult
surfaces, including upside down on ceilings, without
needing to make an ability check.

___Web Sense.___ While in contact with a web, the spider
knows the exact location of any creature in contact
with the same web.

___Web Walker.___ The spider ignores movement restrictions
caused by webbing.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one creature. Hit: 4 (1d4+2) piercing damage, and
the target must succeed on a DC 11 Constitution
saving throw or take 7 (2d6) poison damage. If
the target is reduced to 0 hit points, the target is
stable but poisoned for 1 hour, even after regaining
hit points, and is paralyzed while poisoned this
way.
