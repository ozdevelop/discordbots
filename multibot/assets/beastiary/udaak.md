"Udaak";;;_size_: Gargantuan fiend
_alignment_: neutral evil
_challenge_: "16 (15,000 XP)"
_languages_: "--"
_senses_: "darkvision 120 ft., passive Perception 10"
_saving_throws_: "Str +13, Con +11"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_damage_vulnerabilities_: "thunder"
_condition_immunities_: "frightened, grappled, poisoned, restrained"
_speed_: "50 ft."
_hit points_: "165 (10d20 + 60)"
_armor class_: "18 (natural armor)"
_stats_: | 26 (+8) | 14 (+2) | 22 (+6) | 3 (-4) | 11 (+0) | 10 (+0) |

___Charge.___ If the udaak moves at least 20 feet straight toward a target and then hits it with a slam attack on the same turn, the target takes an extra 27 (6d8) bludgeoning damage. If the target is a creature, it must succeed on a DC 21 Strength saving throw or be pushed up to 20 feet away from the udaak and knocked prone.

___Siege Monster.___ The udaak deals double damage to objects and structures.

**Actions**

___Multiattack.___ The udaak makes three attacks: one with its bite and two with its slam.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one creature. Hit: 21 (2d12 + 8) piercing damage, and the target is grappled (escape DC 21). Until this grapple ends, the target is restrained, and the udaak can’t bite another target.

___Slam.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 21 (3d8 + 8) bludgeoning damage.

___Swallow.___ The udaak makes one bite attack against a Large or smaller target it is grappling. If the attack hits, the target is also swallowed, and the grapple ends. A swallowed creature is blinded and restrained, it has total cover against attacks and other effects outside the udaak, and it takes 21 (6d6) acid damage at the start of each of the udaak’s turns.

If the udaak takes 30 damage or more on a single turn from a creature inside it, the udaak must succeed on a DC 21 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet of the udaak. If the udaak dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 20 feet of movement, exiting prone.
