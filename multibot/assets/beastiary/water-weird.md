"Water Weird";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "understands Aquan but doesn't speak"
_senses_: "blindsight 30 ft."
_damage_immunities_: "poison"
_speed_: "0 ft., swim 60 ft."
_hit points_: "58 (9d10+9)"
_armor class_: "13"
_condition_immunities_: "exhaustion, grappled, paralyzed, poisoned, restrained, prone, unconscious"
_damage_resistances_: "fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 17 (+3) | 16 (+3) | 13 (+1) | 11 (0) | 10 (0) | 10 (0) |

___Invisible in Water.___ The water weird is invisible while fully immersed in water.

___Water Bound.___ The water weird dies if it leaves the water to which it is bound or if that water is destroyed.

**Actions**

___Constrict.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one creature. Hit: 13 (3d6 + 3) bludgeoning damage. If the target is Medium or smaller, it is grappled (escape DC 13) and pulled 5 ft. toward the water weird. Until this grapple ends, the target is restrained, the water weird tries to drown it, and the water weird can't constrict another target.