"Greenskin Orc";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Orc"
_skills_: "Intimidation +0, Perception +2, Stealth +4 (+6 in forests), Survival +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "35 ft."
_hit points_: "11 (2d8 + 2)"
_armor class_: "13 (leather)"
_stats_: | 15 (+2) | 15 (+2) | 12 (+1) | 7 (-2) | 10 (+0) | 6 (-2) |

___Arboreal Hunter.___ The greenskin orc has advantage on
Dexterity (Stealth) checks when in temperate or warm forests.

**Actions**

___Longsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target.
Hit: 6 (1d8 + 2) slashing damage or 7 (1d10 + 2) slashing damage when
used with two hands.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one
target. Hit: 6 (1d8 + 2) piercing damage.
