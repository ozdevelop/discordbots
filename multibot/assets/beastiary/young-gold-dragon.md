"Young Gold Dragon";;;_size_: Large dragon
_alignment_: lawful good
_challenge_: "10 (5,900 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Insight +5, Perception +9, Persuasion +9, Stealth +6"
_damage_immunities_: "fire"
_saving_throws_: "Dex +6, Con +9, Wis +5, Cha +9"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "178 (17d10+85)"
_armor class_: "18 (natural armor)"
_stats_: | 23 (+6) | 14 (+2) | 21 (+5) | 16 (+3) | 13 (+1) | 20 (+5) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 17 (2d10 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Fire Breath. The dragon exhales fire in a 30-foot cone. Each creature in that area must make a DC 17 Dexterity saving throw, taking 55 (10d10) fire damage on a failed save, or half as much damage on a successful one.

Weakening Breath. The dragon exhales gas in a 30-foot cone. Each creature in that area must succeed on a DC 17 Strength saving throw or have disadvantage on Strength-based attack rolls, Strength checks, and Strength saving throws for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.