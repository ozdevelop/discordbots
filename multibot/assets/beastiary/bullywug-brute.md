"Bullywug Brute";;;_size_: Medium humanoid
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Bullywug"
_skills_: "Athletics +5"
_senses_: "passive Perception 10"
_speed_: "20 ft., swim 40 ft."
_hit points_: "30 (4d8 + 12)"
_armor class_: "12 (hide armor)"
_stats_: | 16 (+3) | 10 (+0) | 16 (+3) | 7 (-2) | 10 (+0) | 6 (-2) |

___Amphibious.___ The bullywug can breathe air and
water.

___Speak with Frogs and Toads.___ The bullywug can
communicate simple concepts to frogs and toads
when it speaks in Bullywug.

___Swamp Camouflage.___ The bullywug has advantage on
Dexterity (Stealth) checks made to hide in swampy
terrain.

___Standing Leap.___ The bullywug's long jump is up to 20
feet and its high jump is up to 10 feet, with or
without a running start.

**Actions**

___Multiattack.___ The bullywug makes three attacks: one
with its tongue and two with its punch.

___Tongue.___ Melee Weapon Attack: +5 to hit, reach
15ft., one target. Hit: 4 (1d4 + 2) bludgeoning
damage and the target is grappled (escape DC 13)
if it is a medium or smaller creature and the
bullywug doesn’t have a creature grappled. The
target must then succeed on a DC 13 Strength
saving throw or be pulled to a space within 5 feet
of the bullywug.

___Punch.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 4 bludgeoning damage.
