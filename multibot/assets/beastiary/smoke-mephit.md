"Smoke Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Auran, Ignan"
_senses_: "darkvision 60 ft."
_skills_: "Perception +2, Stealth +4"
_damage_immunities_: "fire, poison"
_speed_: "30 ft., fly 30 ft."
_hit points_: "22 (5d6+5)"
_armor class_: "12"
_condition_immunities_: "poisoned"
_stats_: | 6 (-2) | 14 (+2) | 12 (+1) | 10 (0) | 10 (0) | 11 (0) |

___Death Burst.___ When the mephit dies, it leaves behind a cloud of smoke that fills a 5-foot-radius sphere centered on its space. The sphere is heavily obscured. Wind disperses the cloud, which otherwise lasts for 1 minute.

___Innate Spellcasting (1/Day).___ The mephit can innately cast dancing lights, requiring no material components. Its innate spellcasting ability is Charisma.

**Actions**

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 4 (1d4 + 2) slashing damage.

___Cinder Breath (Recharge 6).___ The mephit exhales a 15-foot cone of smoldering ash. Each creature in that area must succeed on a DC 10 Dexterity saving throw or be blinded until the end of the mephit's next turn .

___Variant: Summon Mephits (1/Day).___ The mephit has a 25 percent chance of summoning 1d4 mephits of its kind. A summoned mephit appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other mephits. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.