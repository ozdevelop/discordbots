"Ancient Sapphire Dragon";;;_size_: Gargantuan dragon
_alignment_: neutral
_challenge_: "24 (62,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +15, Insight +11, Perception +11, Religion +15"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 28"
_saving_throws_: "Dex +15, Int +15, Wis +11, Cha +14"
_damage_vulnerabilities_: "psychic"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, slashing, and piercing from nonmagical attacks"
_condition_immunities_: "stunned"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "350 (28d20 + 56)"
_armor class_: "23 (natural armor)"
_stats_: | 24 (+7) | 27 (+8) | 14 (+2) | 26 (+8) | 19 (+4) | 24 (+7) |

___Legendary Resistance (3/Day).___ If the dragon fails
a saving throw, it can choose to succeed instead.

___Awe Aura.___ All creatures within 30 feet must
make a DC 22 Charisma saving throw in order to
attack this dragon. On a failed save, the attacking
creature’s turn ends immediately. On a success,
that creature is immune to the Awe Aura of all
gemstone dragons for 1 week.

**Psionics**

___Charges:___ 28 | ___Recharge:___ 1d10 | ___Fracture:___ 35

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +14 to hit, reach 15 ft.,
one target. Hit: 18 (2d10 + 7) piercing damage.

___Claw.___ Melee Weapon Attack: +14 to hit, reach 10 ft.,
one target. Hit: 14 (2d6 + 7) slashing damage.

___Tail.___ Melee Weapon Attack: +14 to hit; reach 20 ft.,
one target. Hit: 16 (2d8 + 7) bludgeoning damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The dragon regains spent
legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) Check.

___Psionics.___ The dragon uses a psionic ability.

___Psionic Shift (Costs 2 Actions).___ The dragon
releases a wave of telekinetic energy from its mind.
Every creature within 15 feet must make a DC 24
Intelligence saving throw or take 15 (2d6 + 8) force
damage and be knocked prone. The dragon then
can move up to half its movement speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the
dragon takes a lair action to cause one of the
following effects. The dragon can’t use the same
effect two rounds in a row.

* The dragon manifests _flay_ in a 120-foot cone,
doing 1d6 damage per charge spent.

* The dragon manifests _reflection_ at no cost. It
cannot use this action again for a week.

* The dragon casts _plane shift_ with DC 22.

**Regional** Effects

Intelligent creatures who sleep within 12 miles of a
sapphire dragon’s lair dream of the past and future.
They know the things they see in these dreams are
real events that have, or will, occur.
