"Noxious Savageclaw";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 14 (+2) | 2 (-4) | 12 (+1) | 1 (-5) |

___Poison Trail.___ The savageclaw’s body exudes poison
in a 5 foot cube around it. Whenever the
savageclaw moves, this poison follows along and
fills every space the savageclaw touched during its
movement. This poison dissipates after 1 round.
Any creature that moves through one of these
poison clouds must succeed on a DC 12
Constitution saving throw or take 9 (2d8) poison
damage. A creature that holds their breath while
moving through one of these spaces has advantage
on the saving throw.

**Actions**

___Multiattack.___ The savageclaw makes two attacks with
its bite.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 7 (1d8 + 3) piercing damage.

___Poison Tail.___ Melee Weapon Attack: +5 to hit, reach
5ft., one target. Hit: 6 (1d6 + 3) piercing damage
plus 7 (2d6) poison damage and the target must
succeed on a DC 12 Constitution saving throw or
become poisoned for 1 minute. Creatures poisoned
in this way cannot use reactions. A creature can
repeat the saving throw at the end of each of its
turns, ending the effect on itself on a success.

___Noxious Discharge (1/Day).___ The savageclaw lets out
a powerful blast of poison gas in a 15 foot cube
originating on itself. Any creature in this area must
make a DC 12 Constitution saving throw, taking 13
(3d8) poison damage on a failed save, or half as
much damage on a successful one.
