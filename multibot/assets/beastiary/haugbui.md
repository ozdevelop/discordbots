"Haugbui";;;_size_: Medium undead
_alignment_: lawful neutral
_challenge_: "13 (10000 XP)"
_languages_: "the languages it spoke in life; telepathy 120 ft."
_skills_: "Arcana +7, History +7, Intimidation +8, Perception +10, Religion +12"
_senses_: "truesight 60 ft., passive Perception 20"
_saving_throws_: "Dex +8, Con +9, Wis +10"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "cold, lightning, necrotic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "136 (16d8 + 64)"
_armor class_: "18 (natural armor)"
_stats_: | 18 (+4) | 17 (+3) | 18 (+4) | 15 (+2) | 20 (+5) | 16 (+3) |

___Incorporeal Movement.___ The haugbui can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Innate Spellcasting.___ The haugbui's innate spellcasting ability is Wisdom (spell save DC 18). It can innately cast the following spells, requiring no material components:

Constant: detect thoughts, invisibility, mage hand, scrying

At will: dancing lights, druidcraft, mending, spare the dying

7/day each: bane, create or destroy water, fog cloud, purify food and drink

5/day each: blindness/deafness, gust of wind, locate object, moonbeam, shatter

3/day each: bestow curse, dispel magic, plant growth, remove curse, telekinesis

1/day each: blight, contagion, dream

1/week each: geas, hallow

___Legendary Resistance (3/Day).___ If the haugbui fails a saving throw it can choose to succeed instead.

___Sepulchral Scrying (1/Day).___ An invisible magical eye is created under the haugbui's control, allowing it to watch its territory without leaving the burial mound. The eye travels at the speed of thought and can be up to 5 miles from the haugbui's location. The haugbui can see and hear as if it were standing at the eye's location, and it can use its innate spellcasting abilities as if it were at the eye's location. The eye can be noticed with a successful DC 18 Wisdom (Perception) check and can be dispelled as if it were 3rd-level spell. Spells that block other scrying spells work against Sepulchral Scrying as well. Unless dismissed by its creator or dispelled, lasts for up to 12 hours after its creation; only one can be created per 24-hour period.

___Sunlight Sensitivity.___ While in sunlight, the haugbui has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Turn Resistance.___ The haugbui has advantage on saving throws against any effect that turns undead.

**Actions**

___Multiattack.___ The haugbui makes two psychic claw attacks.

___Psychic Claw.___ Ranged Magical Attack: +10 to hit, range 40 ft., one target. Hit: 32 (6d8 + 5) psychic damage.

