"Copper Mouse Dragon";;;_size_: Tiny dragon
_alignment_: chaotic good
_challenge_: "1 (200 XP)"
_languages_: "understands Common and Draconic but can’t speak"
_skills_: "Arcana +1, Nature +1, Perception +5, Stealth +6"
_senses_: "darkvision 30 ft., passive Perception 15"
_saving_throws_: "Dex +4, Con +4, Wis +3, Cha +3"
_damage_immunities_: "acid"
_speed_: "20 ft. climb 20 ft., burrow 10 ft."
_hit points_: "18 (4d4 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 2 (-4) | 15 (+2) | 14 (+2) | 9 (-1) | 12 (+1) | 13 (+1) |

___Pack Tactics.___ The mouse dragon has advantage on an attack roll against
a creature if at least one of the dragon’s allies is within 5 feet of the creature
and the ally isn’t incapacitated.

___Treasure Sense.___ The mouse dragon can pinpoint, by scent, the location
of precious metals and stones, such as coins and gems, within 60 feet of it.

___Underfoot.___ The mouse dragon can attempt to hide even when it is
obscured only by a creature that is at least one size larger than it.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4
(1d4 + 2) slashing damage.

___Acid Breath (Recharge 5–6).___ The dragon exhales acid in a 15-foot line
that is 1-foot wide. Each creature in that line must make a DC 12 Dexterity
saving throw, taking 14 (4d6) acid damage on a failed saving throw, or
half as much damage on a successful one.
