"Leviathan";;;_page_number_: 198
_size_: Gargantuan elemental
_alignment_: neutral
_challenge_: "20 (25,000 XP)"
_languages_: "-"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "acid, poison"
_saving_throws_: "Wis +10, Cha +9"
_speed_: "40 ft., swim 120 ft."
_hit points_: "328  (16d20 + 160)"
_armor class_: "17"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 30 (+10) | 24 (+7) | 30 (+10) | 2 (-4) | 18 (+4) | 17 (+3) |

___Legendary Resistance (3/Day).___ If the leviathan fails a saving throw, it can choose to succeed instead.

___Partial Freeze.___ If the leviathan takes 50 cold damage or more during a single turn, the leviathan partially freezes; until the end of its next turn, its speeds are reduced to 20 feet, and it makes attack rolls with disadvantage.

___Siege Monster.___ The leviathan deals double damage to objects and structures (included in Tidal Wave).

___Water Form.___ The leviathan can enter a hostile creature's space and stop there. It can move through a space as narrow as 1 inch wide without squeezing.

**Actions**

___Multiattack___ The leviathan makes two attacks: one with its slam and one with its tail.

___Slam___ Melee Weapon Attack: +16 to hit, reach 20 ft., one target. Hit: 15 (1d10 + 10) bludgeoning damage plus 5 (1d10) acid damage.

___Tail___ Melee Weapon Attack: +16 to hit, reach 20 ft., one target. Hit: 16 (1d12 + 10) bludgeoning damage plus 6 (1d12) acid damage.

___Tidal Wave (Recharge 6)___ While submerged, the leviathan magically creates a wall of water centered on itself. The wall is up 250 feet long, up to 250 feet high, and up to 50 feet thick. When the wall appears, all other creatures within its area must each make a DC 24 Strength saving throw. A creature takes 33 (6d10) bludgeoning damage on failed save, or half as much damage on a successful one.
At the start of each of the leviathan's turns after the wall appears, the wall, along with any other creatures in it, moves 50 feet away from the leviathan. Any Huge or smaller creature inside the wall or whose space the wall enters when it moves must succeed on a DC 24 Strength saving throw or take 27 (5d10) bludgeoning damage. A creature takes this damage no more than once on a turn. At the end of each turn the wall moves, the wall's height is reduced by 50 feet, and the damage creatures take from the wall on subsequent rounds is reduced by 1d10. When the wall reaches 0 feet in height, the effect ends.
A creature caught in the wall can move by swimming. Because of the force of the wave, though, the creature must make a successful DC 24 Strength (Athletics) check to swim at all during that turn.

**Legendary** Actions

The leviathan can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The leviathan regains spent legendary actions at the start of its turn.

___Slam (Costs 2 Actions)___ The leviathan makes one slam attack.

___Move___ The leviathan moves up to its speed.