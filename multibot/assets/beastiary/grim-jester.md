"Grim Jester";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "11 (7200 XP)"
_languages_: "Abyssal, Celestial, Common, Gnomish, telepathy 60 ft."
_skills_: "Acrobatics +10, Deception +9, Perception +7, Performance +9, Sleight of Hand +10, Stealth +10"
_senses_: "darkvision 60 ft., passive Perception 17"
_saving_throws_: "Dex +10, Con +8, Cha +9"
_damage_immunities_: "necrotic, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "cold"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "30 ft."
_hit points_: "136 (16d8 + 64)"
_armor class_: "18 (natural armor)"
_stats_: | 14 (+2) | 22 (+6) | 18 (+4) | 16 (+3) | 16 (+3) | 20 (+5) |

___Innate Spellcasting.___ The jester's spellcasting ability is Charisma (spell save DC 17, +9 to hit with spell attacks). It can innately cast the following spells requiring no components:

At will: disguise self, grease, inflict wounds, magic mouth, misty step

3/day each: contagion, mirror image

1/day each: delayed blast fireball, finger of death, mislead, seeming

___Last Laugh.___ Unless it is destroyed in a manner amusing to the god of death that created it, the grim jester is brought back after 1d20 days in a place of the god's choosing.

___Mock the Dying.___ Death saving throws made within 60 feet of the jester have disadvantage.

___Turn Resistance.___ The jester has advantage on saving throws against any effect that turns undead.

**Actions**

___Joker's Shuffle (recharge 6).___ The jester forces one Medium or Small humanoid within 60 feet to make a DC 17 Charisma saving throw. If the saving throw fails, the jester and the target exchange locations via teleportation and an illusion causes them to swap appearance: the jester looks and sounds like the target, and the target looks and sounds like the jester. The illusion lasts for 1 hour unless it is dismissed earlier by the jester as a bonus action, or dispelled (DC 17).

___Killing Joke (recharge 6).___ The jester performs an ancient, nihilistic joke of necromantic power. This joke has no effect on undead or constructs. All other creatures within 60 feet of the jester must make a DC 17 Wisdom saving throw. Those that fail fall prone in a fit of deadly laughter. The laughter lasts 1d4 rounds, during which time the victim is incapacitated and unable to stand up from prone. At the end of its turn each round, an incapacitated victim must make a successful DC 17 Constitution saving throw or be reduced to 0 hit points. The laughter can be ended early by rendering the victim unconscious or with greater restoration or comparable magic.

**Reactions**

___Ridicule Hope (recharge 4-6).___ When a spell that restores hit points is cast within 60 feet of the jester, the jester can cause that spell to inflict damage instead of curing it. The damage equals the hit points the spell would have cured.

