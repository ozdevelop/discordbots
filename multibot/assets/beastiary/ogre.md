"Ogre";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft."
_speed_: "40 ft."
_hit points_: "59 (7d10+21)"
_armor class_: "11 (hide armor)"
_stats_: | 19 (+4) | 8 (-1) | 16 (+3) | 5 (-3) | 7 (-2) | 7 (-2) |

**Actions**

___Greatclub.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.

___Javelin.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 11 (2d6 + 4) piercing damage.