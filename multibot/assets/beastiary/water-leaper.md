"Water Leaper";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "5 ft., fly 50 ft., swim 40 ft."
_hit points_: "97 (13d10 + 26)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 14 (+2) | 15 (+2) | 4 (-3) | 12 (+1) | 5 (-3) |

___Amphibious.___ The water leaper can breathe both air and water.

___Camouflage.___ The water leaper has advantage on Dexterity (Stealth) checks when underwater.

**Actions**

___Multiattack.___ The water leaper uses its shriek and makes one bite attack and one stinger attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage and the target is grappled (escape DC 13). Until this grapple ends, the target is restrained and the water leaper can't bite another target.

___Shriek.___ The water leaper lets out a blood-curdling shriek. Every creature within 40 feet that can hear the water leaper must make a successful DC 12 Constitution saving throw or be frightened until the start of the water leaper's next turn. A creature that successfully saves against the shriek is immune to the effect for 24 hours.

___Stinger.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage and the target must make a successful DC 12 Constitution saving throw or be poisoned for 1 minute. While poisoned in this way, the creature takes 7 (2d6) poison damage at the start of its turn. A poisoned creature repeats the saving throw at the end of its turn, ending the effect on a success.

___Swallow.___ The water leaper makes a bite attack against a medium or smaller creature it is grappling. If the attack hits, the target is swallowed and the grapple ends. The swallowed target is blinded and restrained, and has total cover against attacks and other effects outside the water leaper. A swallowed target takes 10 (3d6) acid damage at the start of the water leaper's turn. The water leaper can have one creature swallowed at a time. If the water leaper dies, the swallowed creature is no longer restrained and can use 5 feet of movement to crawl, prone, out of the corpse.

