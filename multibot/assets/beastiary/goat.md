"Goat";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_speed_: "40 ft."
_hit points_: "4 (1d8)"
_armor class_: "10"
_stats_: | 12 (+1) | 10 (0) | 11 (0) | 2 (-4) | 10 (0) | 5 (-3) |

___Charge.___ If the goat moves at least 20 ft. straight toward a target and then hits it with a ram attack on the same turn, the target takes an extra 2 (1d4) bludgeoning damage. If the target is a creature, it must succeed on a DC 10 Strength saving throw or be knocked prone.

___Sure-Footed.___ The goat has advantage on Strength and Dexterity saving throws made against effects that would knock it prone.

**Actions**

___Ram.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) bludgeoning damage.