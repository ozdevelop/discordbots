"Red Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +2"
_damage_immunities_: "fire"
_saving_throws_: "Dex +2, Con +5, Wis +2, Cha +4"
_speed_: "30 ft., climb 30 ft., fly 60 ft."
_hit points_: "75 (10d8+30)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 10 (0) | 17 (+3) | 12 (+1) | 11 (0) | 15 (+2) |

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (1d10 + 4) piercing damage plus 3 (1d6) fire damage.

___Fire Breath (Recharge 5-6).___ The dragon exhales fire in a 15-foot cone. Each creature in that area must make a DC l3 Dexterity saving throw, taking 24 (7d6) fire damage on a failed save, or half as much damage on a successful one.