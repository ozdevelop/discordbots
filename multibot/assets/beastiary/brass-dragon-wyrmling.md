"Brass Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: chaotic good
_challenge_: "1 (200 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +2"
_damage_immunities_: "fire"
_saving_throws_: "Dex +2, Con +3, Wis +2, Cha +3"
_speed_: "30 ft., burrow 15 ft., fly 60 ft."
_hit points_: "16 (3d8+3)"
_armor class_: "16 (natural armor)"
_stats_: | 15 (+2) | 10 (0) | 13 (+1) | 10 (0) | 11 (0) | 13 (+1) |

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) piercing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Fire Breath. The dragon exhales fire in an 20-foot line that is 5 feet wide. Each creature in that line must make a DC 11 Dexterity saving throw, taking 14 (4d6) fire damage on a failed save, or half as much damage on a successful one.

Sleep Breath. The dragon exhales sleep gas in a 15-foot cone. Each creature in that area must succeed on a DC 11 Constitution saving throw or fall unconscious for 1 minute. This effect ends for a creature if the creature takes damage or someone uses an action to wake it.