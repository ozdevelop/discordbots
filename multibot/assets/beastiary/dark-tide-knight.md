"Dark Tide Knight";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Common"
_skills_: "Athletics +7, Stealth +7"
_speed_: "30 ft."
_hit points_: "58 (9d8+18)"
_armor class_: "13"
_stats_: | 17 (+3) | 16 (+3) | 14 (+2) | 10 (0) | 11 (0) | 11 (0) |

___Bonded Mount.___ The knight is magically bound to a beast with an innate swimming speed trained to serve as its mount. While mounted on this beast, the knight gains the beast's senses and ability to breathe underwater. The bonded mount obeys the knight's commands. If its mount dies, the knight can train a new beast to serve as its bonded mount, a process requiring a month.

___Sneak Attack.___ The knight deals an extra 7 (2d6) damage when it hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 feet of an ally of the knight that isn't incapacitated and the knight doesn't have disadvantage on the attack roll.

**Actions**

___Multiattack.___ The knight makes two shortsword attacks.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Lance.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 9 (1d12 + 3) piercing damage.

**Reactions**

___Uncanny Dodge.___ When an attacker the knight can see hits it with an attack, the knight can halve the damage against it.