"Winter Eladrin";;;_page_number_: 197
_size_: Medium fey (elf)
_alignment_: chaotic neutral
_challenge_: "10 (5900 XP)"
_languages_: "Common, Elvish, Sylvan"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "30 ft."
_hit points_: "127  (17d8 + 51)"
_armor class_: "19 (natural armor)"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 11 (0) | 10 (0) | 16 (+3) | 18 (+4) | 17 (+3) | 13 (+1) |

___Fey Step (Recharge 4-6).___ As a bonus action, the eladrin can teleport up to 30 feet to an unoccupied space it can see.

___Innate Spellcasting.___ The eladrin's innate spellcasting ability is Intelligence (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _fog cloud, gust of wind_

* 1/day each: _cone of cold, ice storm_

___Magic Resistance.___ The eladrin has advantage on saving throws against spells and other magical effects.

___Sorrowful Presence.___ Any non-eladrin creature that starts its turn within 60 feet of the eladrin must make a DC 13 Wisdom saving throw. On a failed save, the creature is charmed for 1 minute. While charmed in this way, the creature has disadvantage on ability checks and saving throws. The charmed creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to any eladrin's Sorrowful Presence for the next 24 hours.
Whenever the eladrin deals damage to the charmed creature, it can repeat the saving throw, ending the effect on itself on a success.

**Actions**

___Longsword___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d8) slashing damage, or 5 (1d10) slashing damage if used with two hands.

___Longbow___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one target. Hit: 4 (1d8) piercing damage.

**Reactions**

___Frigid Rebuke___ When the eladrin takes damage from a creature the eladrin can see within 60 feet of it, the eladrin can force that creature to succesed on a DC 16 Constitution saving throw or take 11 (2d10) cold damage.
