"Orc War Chief";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +5"
_saving_throws_: "Str +6, Con +6, Wis +2"
_speed_: "30 ft."
_hit points_: "93 (11d8+44)"
_armor class_: "16 (chain mail)"
_stats_: | 18 (+4) | 12 (+1) | 18 (+4) | 11 (0) | 11 (0) | 16 (+3) |

___Aggressive.___ As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.

___Gruumsh's Fury.___ The orc deals an extra 4 (1d8) damage when it hits with a weapon attack (included in the attacks).

**Actions**

___Multiattack.___ The orc makes two attacks with its greataxe or its spear.

___Greataxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 15 (1d12 + 4 plus 1d8) slashing damage.

___Spear.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 12 (1d6 + 4 plus 1d8) piercing damage, or 13 (2d8 + 4) piercing damage if used with two hands to make a melee attack.

___Battle Cry (1/Day).___ Each creature of the war chief's choice that is within 30 feet of it, can hear it, and not already affected by Battle Cry gain advantage on attack rolls until the start of the war chief's next turn. The war chief can then make one attack as a bonus action.