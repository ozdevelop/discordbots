"Adult Dungeon Dragon";;;_size_: Huge dragon
_alignment_: neutral
_challenge_: "16 (15,000 XP)"
_languages_: "all"
_skills_: "Arcana +8, History +8, Insight +8, Investigation +13, Perception +13, Survival +8"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 23"
_saving_throws_: "Dex +5, Con +12, Wis +8, Cha +10"
_speed_: "40 ft., fly 80 ft."
_hit points_: "364 (27d12 + 189)"
_armor class_: "18 (natural armor)"
_stats_: | 27 (+8) | 10 (+0) | 25 (+7) | 16 (+3) | 17 (+3) | 21 (+5) |

___Special Equipment.___ The dragon is attuned to a crystal ball, which is
hidden somewhere within its labyrinthine lair. If a dungeon dragon loses
its crystal ball, it can create a new one over the course of 10 days.

___Innate Spellcasting.___ The dungeon dragon’s innate spellcasting ability
is Charisma (spell save DC 18, +10 to hit with spell attacks). It can cast
the following spells, requiring no material components.

* At will: _alarm, arcane lock, detect magic, detect thoughts, find traps, glyph of warding, identify, knock, scrying, stone shape, wall of stone_

* 3/day each: _dispel magic, hallucinatory terrain, locate creature, locate object, project image_

___Labyrinthine Recall.___ The dungeon dragon can perfectly recall any path
it has traveled.

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it can
choose to succeed instead.

___Trap Master.___ The dungeon dragon has advantage on saving throws
against the effects of traps.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 19
(2d10 + 8) piercing damage.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 15
(2d6 + 8) slashing damage.

___Tail.___ Melee Weapon Attack: +13 to hit, reach 20 ft., one target. Hit: 17
(2d8 + 8) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon’s choice that is within
120 feet of the dragon and aware of it must succeed on a DC 18 Wisdom
saving throw or become frightened for 1 minute. A creature can repeat the
saving throw at the end of each of its turns, ending the effect on itself on
a success. If a creature’s saving throw is successful or the effect ends for
it, the creature is immune to the dragon’s Frightful Presence for the next
24 hours.

___Confusion Breath (Recharge 5–6).___ The dragon exhales gas in a 60-foot
cone that spreads around corners. Each creature in that area must succeed
on a DC 20 Constitution saving throw. On a failed save, the creature is
confused for 1 minute. While confused, the creature uses its action to
Dash in a random direction, even if that movement takes the creature into
dangerous areas. A confused creature can repeat the saving throw at the
end of each of its turns, ending the effect on a success.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 21 Dexterity
saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.
