"Ogre Chieftain, Corrupted";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft., passive Perception 8"
_saving_throws_: "Str +8, Con +6, Cha +2"
_speed_: "40 ft."
_hit points_: "127 (15d10 + 45)"
_armor class_: "17 (splint)"
_stats_: | 20 (+5) | 8 (-1) | 16 (+3) | 5 (-3) | 7 (-2) | 8 (-1) |

___Aggressive.___ As a bonus action, the corrupted ogre chieftain can move up to its speed toward a hostile creature that it can see. Mutation. The corrupted ogre chieftain has one mutation from the list below:

___Mutation.___ 1 - Armored Hide: The corrupted ogre chieftain's skin is covered in dull, melted scales that give it resistance to bludgeoning, piercing, and slashing damage from nonmagical weapons.

2 - Extra Arm: The corrupted ogre chieftain has a third arm growing from its chest. The corrupted ogre chieftain can make three melee attacks or two ranged attacks.

3 - Savant: The corrupted ogre chieftain's head is grossly enlarged. Increase its Charisma to 16. The corrupted ogre chieftain gains Innate Spellcasting (Psionics), and its innate spellcasting ability is Charisma (spell save DC 14). It can innately cast the following spells, requiring no components: At will: misty step, shield; 1/day each: dominate monster, levitate.

4 - Terrifying: The corrupted ogre chieftain's body is covered in horns, eyes, and fanged maws. Each creature of the corrupted ogre chieftain's choice that is within 60 feet of it and is aware of it must succeed on a DC 15 Wisdom saving throw or become frightened for 1 minute. A frightened creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to this ogre chieftain's Frightful Presence for the next 24 hours.

**Actions**

___Multiattack.___ The corrupted ogre chieftain makes two melee attacks.

___Greatclub.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage.

___Javelin.___ Melee or Ranged Weapon Attack: +8 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 12 (2d6 + 5) piercing damage.

