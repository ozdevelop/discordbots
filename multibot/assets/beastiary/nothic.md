"Nothic";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Undercommon"
_senses_: "truesight 120 ft."
_skills_: "Arcana +3, Insight +4, Perception +2, Stealth +5"
_speed_: "30 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 16 (+3) | 16 (+3) | 13 (+1) | 10 (0) | 8 (-1) |

___Keen Sight.___ The nothic has advantage on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The nothic makes two claw attacks.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Rotting Gaze.___ The nothic targets one creature it can see within 30 ft. of it. The target must succeed on a DC 12 Constitution saving throw against this magic or take 10 (3d6) necrotic damage.

___Weird Insight.___ The nothic targets one creature it can see within 30 ft. of it. The target must contest its Charisma (Deception) check against the nothic's Wisdom (Insight) check. If the nothic wins, it magically learns one fact or secret about the target. The target automatically wins if it is immune to being charmed.