"Otyugh";;;_size_: Large aberration
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Otyugh"
_senses_: "darkvision 120 ft."
_saving_throws_: "Con +7"
_speed_: "30 ft."
_hit points_: "114 (12d10+48)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 11 (0) | 19 (+4) | 6 (-2) | 13 (+1) | 6 (-2) |

___Limited Telepathy.___ The otyugh can magically transmit simple messages and images to any creature within 120 ft. of it that can understand a language. This form of telepathy doesn't allow the receiving creature to telepathically respond.

**Actions**

___Multiattack.___ The otyugh makes three attacks: one with its bite and two with its tentacles.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) piercing damage. If the target is a creature, it must succeed on a DC 15 Constitution saving throw against disease or become poisoned until the disease is cured. Every 24 hours that elapse, the target must repeat the saving throw, reducing its hit point maximum by 5 (1d10) on a failure. The disease is cured on a success. The target dies if the disease reduces its hit point maximum to 0. This reduction to the target's hit point maximum lasts until the disease is cured.

___Tentacle.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 7 (1d8 + 3) bludgeoning damage plus 4 (1d8) piercing damage. If the target is Medium or smaller, it is grappled (escape DC 13) and restrained until the grapple ends. The otyugh has two tentacles, each of which can grapple one target.

___Tentacle Slam.___ The otyugh slams creatures grappled by it into each other or a solid surface. Each creature must succeed on a DC 14 Constitution saving throw or take 10 (2d6 + 3) bludgeoning damage and be stunned until the end of the otyugh's next turn. On a successful save, the target takes half the bludgeoning damage and isn't stunned.