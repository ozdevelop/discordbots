"Stormwarden";;;_size_: Medium humanoid
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic, Giant"
_skills_: "Survival +3"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_resistances_: "lightning, thunder"
_speed_: "30 ft."
_hit points_: "45 (7d8 + 14)"
_armor class_: "12 (hide armor)"
_stats_: | 14 (+2) | 10 (+0) | 14 (+2) | 16 (+3) | 13 (+1) | 10 (+0) |

___Innate Spellcasting.___ The stormwarden’s spellcasting ability is
Intelligence (spell save DC 13, + 5 to hit with spell attacks). The
stormwarden can innately cast the following spells, requiring only verbal
components:

* At will: _thaumaturgy_

* 1/day: _fog cloud_

**Actions**

___Longsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target.
Hit: 6 (1d8 + 2) slashing damage, or 7 (1d10 + 2) slashing damage if used
with two hands.

___Longbow.___ Ranged Weapon Attack: +2 to hit, range 150/600 ft., one
target. Hit: 4 (1d8) piercing damage.

___Stormscream (1/day).___ The stormwarden unleashes a deafening scream
filled with the fury of the storm in a 15-foot cone. Each creature in that
area must make a DC 12 Constitution saving throw. On a failed save, a
creature takes 9 (2d8) thunder damage and is pushed 10 feet away from
the stormwarden. On a successful save, the creature takes half as much
damage and isn’t pushed.
