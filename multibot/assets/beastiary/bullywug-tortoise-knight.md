"Bullywug Tortoise Knight";;;_size_: Medium humanoid
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Bullywug"
_skills_: "Athletics +4"
_senses_: "passive Perception 10"
_speed_: "20 ft., swim 40 ft."
_hit points_: "19 (3d8 + 6)"
_armor class_: "13 (hide armor)"
_stats_: | 14 (+2) | 12 (+1) | 14 (+2) | 7 (-2) | 10 (+0) | 6 (-2) |

___Amphibious.___ The bullywug can breathe air and
water.

___Mounted Combatant.___ The bullywug typically fights
from the back of a tortoise. The tortoise and the
bullywug share an initiative in combat. If the
bullywug is hit for 10 points of damage or more
from a single attack, it must succeed on an athletics
check with DC equal to the damage of the attack or
fall from its mount.

___Speak with Frogs and Toads.___ The bullywug can
communicate simple concepts to frogs and toads
when it speaks in Bullywug.

___Swamp Camouflage.___ The bullywug has advantage on
Dexterity (Stealth) checks made to hide in swampy
terrain.

___Standing Leap.___ The bullywug's long jump is up to 20
feet and its high jump is up to 10 feet, with or
without a running start.

**Actions**

___Lance.___ Melee Weapon Attack: +4 to hit, reach 10 ft.,
one target. Hit: 8 (1d12 + 2) piercing damage. If
the bullywug is not riding a tortoise, this attack is
made with disadvantage.
