"Ulitharid";;;_size_: Large aberration
_alignment_: lawful evil
_challenge_: "9 (5,000 XP)"
_languages_: "Deep Speech, Undercommon, telepathy 2 miles"
_senses_: "darkvision 120 ft."
_skills_: "Arcana +9, Insight +8, Perception +8, Stealth +5"
_saving_throws_: "Int +9, Wis +8, Cha +9"
_speed_: "30 ft."
_hit points_: "127 (17d10+34)"
_armor class_: "15 (breastplate)"
_stats_: | 15 (+2) | 12 (+1) | 15 (+2) | 21 (+5) | 19 (+4) | 21 (+5) |

___Creature Sense.___ The ulitharid is aware of the presence of creatures within 2 miles of it that have an Intelligence score of 4 or higher. It knows the distance and direction to each creature, as well as each creature's intelligence score, but can't sense anything else about it. A creature protected by a mind blank spell, a nondetection spell, or similar magic can't be perceived in this manner.

___Magic Resistance.___ The ulitharid has advantage on saving throws against spells and other magical effects.

___Psionic Hub.___ If an elder brain establishes a psychic link with the ulitharid, the elder brain can form a psychic link with any other creature the ulitharid can detect using its Creature Sense. Any such link ends if the creature falls outside the telepathy ranges of both the ulitharid and the elder brain. The ulitharid can maintain its psychic link with the elder brain regardless of the distance between them, so long as they are both on the same plane of existence. lithe ulitharid is more than 5 miles away from the elder brain, it can end the psychic link at any time (no action required).

___Innate Spellcasting (Psionics).___ The ulitharid's innate spellcasting ability is Intelligence (spell save DC 17). It can innately cast the following spells, requiring no components:

* At will: _detect thoughts, levitate_

* 1/day each: _confusion, dominate monster, eyebite, feeblemind, mass suggestion, plane shift _(self only)_, project image, scrying, telekinesis_

**Actions**

___Tentacles.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one creature. Hit: 27 (4d10+5) psychic damage. If the target is Large or smaller, it is grappled (escape DC 14) and must succeed on a DC 17 Intelligence saving throw or be stunned until this grapple ends.

___Extract Brain.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one incapacitated humanoid grappled by the ulitharid. Hit: 55 (10d10) piercing damage. If this damage reduces the target to 0 hit points, the ulitharid kills the target by extracting and devouring its brain.

___Mind Blast (Recharge 5-6).___ The ulitharid magically emits psychic energy in a 60-foot cone. Each creature in that area must succeed on a DC 17 Intelligence saving throw or take 31 (4d12+5) psychic damage and be stunned for 1 minute. A target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
