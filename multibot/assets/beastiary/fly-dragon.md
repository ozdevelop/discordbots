"Fly Dragon";;;_size_: Tiny dragon
_alignment_: any alignment
_challenge_: "18 (20,000 XP)"
_languages_: "Common, Draconic"
_skills_: "Arcana +10, Deception +10, History +10, Intimidation +10, Perception +9, Stealth +16"
_senses_: "truesight 120 ft., passive Perception 19"
_saving_throws_: "Dex +16, Con +13, Wis +9"
_speed_: "5 ft., fly 60 ft."
_hit points_: "275 (29d4 + 203)"
_armor class_: "20"
_stats_: | 13 (+1) | 30 (+10) | 24 (+7) | 19 (+4) | 17 (+3) | 19 (+4) |

___Legendary Resistance (3/day).___ If the fly dragon fails a saving throw, it
can choose to succeed instead.

___Magic Resistance.___ The fly dragon has advantage on saving throws
against spells and other magical effects.

___Spellcasting.___ The fly dragon is a 20th level spellcaster. Its spellcasting
ability is Charisma (spell save DC 18, +10 to hit with spell attacks). The
fly dragon’s spells require no material components, and individual fly
dragons know different sets of spells. A typical list is provided below:

* Cantrips (at will): _fire bolt, mage hand, message, minor illusion, prestidigitation, shocking grasp_

* 1st level (4/day): _detect magic_

* 2nd level (3/day): _darkness, suggestion_

* 3rd level (3/day): _haste_

* 4th level (3/day): _confusion, greater invisibility, wall of fire_

* 5th level (3/day): _hold monster, telekinesis_

* 6th level (2/day): _chain lightning, globe of invulnerability_

* 7th level (2/day): _plane shift, teleport_

* 8th level (1/day): _dominate monster_

* 9th level (1/day): _time stop_

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +16 to hit, reach 5 ft., one target. Hit: 12
(1d4 + 10) piercing damage plus 7 (2d6) fire damage.

___Claw.___ Melee Weapon Attack: +16 to hit, reach 5 ft., one target. Hit: 13
(1d6 + 10) slashing damage.

___Frightful Presence.___ Each creature of the dragon’s choice that is within
60 feet of the fly dragon and aware of it must succeed on a DC 19 Wisdom
saving throw or become Frightened for 1 minute. A creature can repeat
the saving throw at the end of each of its turns, ending the effect on itself
on a success. If a creature’s saving throw is successful or the effect ends
for it, the creature is immune to the fly dragon’s Frightful Presence for the
next 24 hours.

___Fire Breath (Recharge 5–6).___ The dragon exhales fire in a 40-foot cone.
Creatures in the area must make a DC 19 Dexterity saving throw, taking
63 (18d6) fire damage, or half as much damage on a successful one. Being
underwater doesn’t grant resistance to this damage.

**Legendary** Actions

The fly dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only at
the end of another creature’s turn. The fly dragon regains spent legendary
actions at the start of its turn.

___Move.___ The fly dragon moves up to its speed without provoking
opportunity attacks.

___Cantrip.___ The fly dragon casts a cantrip.

___Detect.___ The fly dragon makes a Wisdom (Perception) check.

___Cast a Spell (Costs 3 Actions).___ The fly dragon casts a spell from its list
of prepared spells, using a spell slot as normal.

**Lair** Actions

At an initiative of 20, losing to ties, a fly dragon defending its lair can
take a lair action to cause one of the following effects. The fly dragon can’t
use the same effect two rounds in a row.

___Searing Wind.___ Super-heated air erupts from one or more passages the
fly dragon can see, filling one or more entire chambers of no more than
a 20-foot radius total with a searing wind. Each creature in the area must
make a DC 16 Constitution save, taking 21 (6d6) fire damage on a failed
save, or half as much damage on a successful one.

___Unstable Footing.___ A tremor shakes the lair in a 40-foot radius around
the fly dragon. Each creature other than the fly dragon in contact with the
ground, walls, or ceilings in that area must succeed on a DC 16 Dexterity
saving throw or be knocked prone.

___Noxious Fumes.___ Volcanic gases form a cloud in a 20-foot radius sphere
centered on a point the fly dragon can see within 120 feet of it. The sphere
spreads around corners, and its area is lightly obscured. It lasts until the
initiative count 20 on the next round. Each creature that starts its turn
in the cloud must succeed on a DC 16 Constitution saving throw or be
poisoned until the end of its turn. While poisoned in this way, a creature
is incapacitated.
