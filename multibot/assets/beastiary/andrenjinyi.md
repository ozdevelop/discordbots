"Andrenjinyi";;;_size_: Gargantuan celestial
_alignment_: neutral
_challenge_: "15 (13000 XP)"
_languages_: "Common, Celestial, Giant, Sylvan"
_skills_: "Arcana +5, Perception +9, Religion +5"
_senses_: "darkvision 60 ft., tremorsense 120 ft., passive Perception 19"
_saving_throws_: "Con +12, Wis +9, Cha +11"
_damage_immunities_: "psychic"
_damage_resistances_: "acid, cold, fire, lightning"
_speed_: "60 ft., burrow 20 ft., climb 20 ft., swim 60 ft."
_hit points_: "228 (13d20 + 91)"
_armor class_: "18 (natural armor)"
_stats_: | 30 (+10) | 17 (+3) | 25 (+7) | 10 (+0) | 18 (+4) | 23 (+6) |

___Amphibious.___ The andrenjinyi can breathe air and water.

___Innate Spellcasting.___ The andrenjinyi's spellcasting ability is Charisma (spell save DC 19, +11 to hit with spell attacks). It can innately cast the following spells, requiring only verbal components:

At will: create water, speak with animals, stoneshape

3/day each: control weather, dispel magic, reincarnate

1/day each: blight, commune with nature, contagion, flesh to stone, plant growth

___Magic Resistance.___ The andrenjinyi has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The andrenjinyi's weapon attacks are magical.

**Actions**

___Multiattack.___ The andrenjinyi makes two attacks, one with its bite and one with its constriction. If both attacks hit the same target, then the target is Swallowed Whole.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 20 ft., one target. Hit: 36 (4d12 + 10) piercing damage.

___Constrict.___ Melee Weapon Attack: +15 to hit, reach 5 ft., one target. Hit: 36 (4d12 + 10) bludgeoning damage, and the target is grappled (escape DC 20). Until this grapple ends the target is restrained, and the andrenjinyi can't constrict another target.

___Rainbow Arch.___ The andrenjinyi can instantaneously teleport between sources of fresh water within 1 mile as an action. It can't move normally or take any other action on the turn when it uses this power. When this power is activated, a rainbow manifests between the origin and destination, lasting for 1 minute.

___Swallow Whole.___ If the bite and constrict attacks hit the same target in one turn, the creature is swallowed whole. The target is blinded and restrained, and has total cover against attacks and other effects outside the andrenjinyi. The target takes no damage inside the andrenjinyi. The andrenjinyi can have three Medium-sized creatures or four Small-sized creatures swallowed at a time. If the andrenjinyi takes 20 damage or more in a single turn from a swallowed creature, the andrenjinyi must succeed on a DC 18 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 5 feet of the andrenjinyi. If the andrenjinyi is slain, a swallowed creature is no longer restrained by it and can escape from the andrenjinyi by using 15 feet of movement, exiting prone. The andrenjinyi can regurgitate swallowed creatures as a free action.

___Transmuting Gullet.___ When a creature is swallowed by an andrenjinyi, it must make a successful DC 19 Wisdom saving throw each round at the end of its turn or be affected by true polymorph into a new form chosen by the andrenjinyi. The effect is permanent until dispelled or ended with a wish or comparable magic.

