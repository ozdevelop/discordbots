"Illithilich";;;_size_: Medium undead
_alignment_: any evil alignment
_challenge_: "22 (41,000 XP)"
_languages_: "Deep Speech, Undercommon, telepathy 120 ft."
_senses_: "truesight 120 ft."
_skills_: "Arcana +18, History +12, Insight +9, Perception +9"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Con +10, Int +12, Wis +9"
_speed_: "30 ft."
_hit points_: "135 (18d8+54)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_damage_resistances_: "cold, lightning, necrotic"
_stats_: | 11 (0) | 16 (+3) | 16 (+3) | 20 (+5) | 14 (+2) | 16 (+3) |

___Legendary Resistance (3/Day).___ If the illithilich fails a saving throw, it can choose to succeed instead.

___Rejuvenation.___ If it has a phylactery, a destroyed illithilich gains a new body in 1d10 days, regaining all its hit points and becoming active again. The new body appears within 5 feet of the phylactery.

___Spellcasting.___ The illithilich is an 18th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 20, +12 to hit with spell attacks). The lich has the following wizard spells prepared:

* Cantrips (at will): _mage hand, prestidigitation, ray of frost_

* 1st level (4 slots): _detect magic, magic missile, shield, thunderwave_

* 2nd level (3 slots): _detect thoughts, invisibility, Melf's acid arrow, mirror image_

* 3rd level (3 slots): _animate dead, counterspell, dispel magic, fireball_

* 4th level (3 slots): _blight, dimension door_

* 5th level (3 slots): _cloudkill, scrying_

* 6th level (1 slot): _disintegrate, globe of invulnerability_

* 7th level (1 slot): _finger of death, plane shift_

* 8th level (1 slot): _dominate monster, power word stun_

* 9th level (1 slot): _power word kill_

___Turn Resistance.___ The illithilich has advantage on saving throws against any effect that turns undead.

___Magic Resistance.___ The illithilich has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting (Psionics).___ The illithilich's innate spellcasting ability is Intelligence (spell save DC 20). It can innately cast the following spells, requiring no components.

At will: detect thoughts, levitate

1/day each: dominate monster, plane shift (self only)

**Actions**

___Paralyzing Touch.___ Melee Spell Attack: +12 to hit, reach 5 ft., one creature. Hit: 10 (3d6) cold damage. The target must succeed on a DC 18 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.



___Tentacles.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one creature. Hit: 21 (3d10+5) psychic damage. If the target is Large or smaller, it is grappled (escape DC 15) and must succeed on a DC 20 Intelligence saving throw or be stunned until this grapple ends.

___Extract Brain.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one incapacitated humanoid grappled by the lich. Hit: 55 (10d10) piercing damage. If this damage reduces the target to 0 hit points, the lich kills the target by extracting and devouring its brain.

___Mind Blast (Recharge 5-6).___ The illithilich magically emits psychic energy in a 60-foot cone. Each creature in that area must succeed on a DC 18 Intelligence saving throw or take 27 (5d8+5) psychic damage and be stunned for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

**Legendary** Actions

The illithilich can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The illithilich regains spent legendary actions at the start of its turn.

___Tentacles.___ The illithilich makes one attack with its tentacles.

___Extract Brain (Costs 2 Actions).___ The illithilich uses Extract Brain.

___Mind Blast (Costs 3 Actions).___ The illithilich recharges its Mind Blast and uses it.

___Cast Spell (Costs 1-3 Actions).___ The illithilich uses a spell slot to cast a 1st-, 2nd-, or 3rd-level spell that it has prepared. Doing so costs 1 legendary action per level of the spell.
