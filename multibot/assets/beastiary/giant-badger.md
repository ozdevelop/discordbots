"Giant Badger";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "darkvision 30 ft."
_speed_: "30 ft., burrow 10 ft."
_hit points_: "13 (2d8+4)"
_armor class_: "10"
_stats_: | 13 (+1) | 10 (0) | 15 (+2) | 2 (-4) | 12 (+1) | 5 (-3) |

___Keen Smell.___ The badger has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Multiattack.___ The badger makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) piercing damage.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) slashing damage.