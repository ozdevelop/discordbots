"Einherjar";;;_size_: Medium humanoid
_alignment_: chaotic neutral
_challenge_: "7 (2900 XP)"
_languages_: "Celestial, Common"
_skills_: "Animal Handling +5, Intimidation +6, Perception +5"
_senses_: "darkvision 60 ft., truesight 60 ft., passive Perception 15"
_damage_resistances_: "piercing weapons that are nonmagical"
_speed_: "30 ft."
_hit points_: "119 (14d8 + 56)"
_armor class_: "18 (chain mail and shield)"
_stats_: | 19 (+4) | 16 (+3) | 19 (+4) | 10 (+0) | 14 (+2) | 11 (+0) |

___Asgardian Battleaxes.___ Made in Valhalla and kept keen with runic magic, Asgardian axes have a +2 enchantment and add a second die of weapon damage. Their magic must be renewed each week by a valkyrie or Odin's own hand.

___Battle Loving.___ Einherjars relish combat and never turn down a challenge to single combat or shirk a fight, even if the odds are hopeless. After all, Valhalla awaits them.

___Battle Frenzy.___ Once reduced to 30 hp or less, einherjar make all attacks with advantage.

___Fearsome Gaze.___ The stare of an einherjar is especially piercing and intimidating. They make Intimidation checks with advantage.

___Innate Spellcasting.___ The einherjar's innate spellcasting ability is Wisdom (spell save DC 13). It can innately cast the following spells, requiring no material components:

___At will.___ less, spare the dying

___1/day each.___ eath ward, spirit guardians

**Actions**

___Multiattack.___ An einherjar makes three attacks with its Asgardian battleaxe or one with its handaxe.

___Asgardian Battleaxe.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 15 (2d8 + 6) slashing damage when used one handed or 17 (2d10 + 6) when used two-handed.

___Handaxe.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

