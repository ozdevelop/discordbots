"Bonebreaker Dorokor";;;_size_: Medium humanoid (orc)
_alignment_: neutral evil
_challenge_: "6 (2,300 XP)"
_languages_: "Orc, Common"
_skills_: "Athletics +7, Intimidation +6, Persuasion +6"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Str +7, Dex +5, Wis +6"
_damage_resistances_: "fire"
_speed_: "30 ft."
_hit points_: "82 (11d8 + 33)"
_armor class_: "16 (chainmail)"
_stats_: | 19 (+4) | 15 (+2) | 18 (+4) | 12 (+1) | 17 (+3) | 17 (+3) |

___Aggressive.___ As a bonus action, the orc can
move up to its speed toward a hostile creature it
can see.

___Wielder of Wound.___ Bonebreaker Dorokor wields
the magical greataxe _Wound._


**Actions**

___Multiattack.___ Bonebreaker Dorokor makes three
weapon attacks, or she makes two attacks and
issues a War Cry if it is available.

___Wound.___ Melee Weapon Attack: +8 to hit, reach 5
ft., one target. Hit: 11(1d12 + 5) slashing damage
plus 7 (2d6) necrotic damage. The target’s
maximum hit points are decreased in equal
amount to the necrotic damage dealt. The target’s
hit point maximum does not return to normal until
it finishes a long rest or its grievous wounds are
soothed by a greater restoration spell.

___Longbow.___ Ranged Weapon Attack: +5 to hit, range
150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___War Cry (Recharge 4–6).___ Dorokor screams an
orcish war phrase, spurring her warriors on toward
victory. Choose one of the following effects:
* ___Rally:___ All of Dorokor’s minions within 30 feet
that can hear her gain 22 (4d10) temporary
hit points.
* ___Focus:___ All of Dorokor’s minions within 30 feet
that can hear her will have advantage on the
next attack roll they make before the end of
their next turn.
* ___Charge:___ All of Dorokor’s minions within 30 feet
that can hear her can move up to their speed as
a reaction.

**Reactions**

___Villain Ability: Warlord.___ As a reaction, when
a minion dies, Dorokor can issue a command to her other
minions. Those who can hear her gain a reaction
they can use to immediately take another movement.
