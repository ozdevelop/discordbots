"White Tusk Juggernaut";;;_size_: Large humanoid (orc)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Orc"
_skills_: "Athletics +7, Intimidation +3"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Str +6, Wis +3 "
_speed_: "25 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "20 (plate, shield)"
_stats_: | 19 (+4) | 8 (-1) | 19 (+4) | 7 (-2) | 12 (+1) | 8 (-1) |

___Aggressive.___ As a bonus action, the orc can
move up to its speed toward a hostile creature it
can see.

___Minion: Savage Horde.___ After moving at least 20
feet in a straight line toward a creature, the next
attack the orc makes against that creature scores
a critical hit on a roll of 18–20.


**Actions**

___Multiattack.___ The White Tusk juggernaut makes
one tusk attack and one longsword attack.

___Tusk.___ Melee Weapon Attack: +6 to hit, reach 10 ft.,
one target. Hit: 11 (2d6 + 4) piercing damage. If
the target is a creature, it must succeed on a DC
14 Strength saving throw or be knocked prone.

___Longsword.___ Melee Weapon Attack: +6 to hit,
reach 10 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

**Reactions**

___Horde Protector.___ When a creature within 5 feet
of the orc is attacked by a creature the orc can
see and the orc is wielding a shield, the orc can
impose disadvantage on that attack roll. 
