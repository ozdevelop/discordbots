"Water Elemental Myrmidon (B)";;;_page_number_: 203
_size_: Medium elemental
_alignment_: neutral
_challenge_: "7 (2900 XP)"
_languages_: "Aquan, one language of its creator's choice"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_speed_: "40 ft., swim 40 ft."
_hit points_: "127  (17d8 + 51)"
_armor class_: "18 (plate)"
_condition_immunities_: "paralyzed, petrified, poisoned, prone"
_damage_resistances_: "acid; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 8 (-1) | 10 (0) | 10 (0) |

___Magic Weapons.___ The myrmidon's weapon attacks are magical.

**Actions**

___Multiattack___ The myrmidon makes three trident attacks.

___Trident___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage, or 8 (1d8 + 4) piercing damage if used with two hands to make a melee attack.

___Freezing Strikes (Recharge 6)___ The myrmidon uses Multiattack. Each attack that hits deals an extra 5 (1d10) cold damage. A target that is hit by one or more of these attacks has its speed reduced by 10 feet until the end of the myrmidon's next turn.