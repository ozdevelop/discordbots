"Jotun Giant";;;_size_: Gargantuan giant
_alignment_: chaotic neutral
_challenge_: "22 (41000 XP)"
_languages_: "Common, Giant"
_skills_: "Arcana +10, History +10, Nature +10, Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 15"
_saving_throws_: "Con +14, Wis +11, Cha +8"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "60 ft."
_hit points_: "407 (22d20 + 176)"
_armor class_: "20 (natural armor)"
_stats_: | 30 (+10) | 8 (-1) | 26 (+8) | 18 (+4) | 20 (+5) | 14 (+2) |

___Immortality.___ Jotuns suffer no ill effects from age, and are immune to effects that reduce ability scores and hit point maximum.

___Innate Spellcasting.___ The Jotun giant's innate spellcasting ability is Wisdom (spell save DC 19). It can innately cast the following spells, requiring no material components:

At will: earthquake, shapechange, speak with animals

3/day: bestow curse, gust of wind

1/day: divination

___Magic Resistance.___ The giant has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The giant's weapon attacks are magical.

___Too Big to Notice.___ The sheer size of the Jotun giant often causes those near it to confuse one at rest for part of the landscape. The jotun has advantage on Stealth checks when not moving.

**Actions**

___Multiattack.___ The giant makes two greatclub attacks and a frightful presence attack, or one rock throwing attack.

___Greatclub.___ Melee Weapon Attack: +16 to hit, reach 30 ft., one target. Hit: 55 (10d8 + 10) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +16 to hit, range 90/240 ft., one target. Hit: 49 (6d12 + 10) bludgeoning damage.

___Frightful Presence.___ Each creature of the giant's choice that is within 120 feet of the giant and aware of it must succeed on a DC 19 Wisdom saving throw or become frightened for 1 minute. A creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. Once a creature's saving throw is successful, it is immune to the giant's Frightful Presence for the next 24 hours.

**Reactions**

___Rock Catching.___ If a rock or similar object is hurled at the giant, the giant can, with a successful DC 10 Dexterity saving throw, catch the missile and take no bludgeoning damage from it.

**Legendary** Actions

___The Jotun giant can take 1 legendary action, and only at the end of another creature's turn.___ The giant regains the spent legendary action at the start of its turn.

___Detect.___ The Jotun giant makes a Wisdom (Perception) check.

___Planar Return.___ If banished, a Jotun giant can return to the plane it departed 2/day. If banished a third time, it cannot return.

___Sweeping Blow.___ The Jotun giant can sweep its greatclub in an arc around itself. The sweep affects a semicircular path 30 feet wide around the giant. All targets in that area take 46 (8d8 + 10) bludgeoning damage, or no damage with a successful DC 19 Dexterity saving throw.

