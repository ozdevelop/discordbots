"Domovoi";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "4 (1100 XP)"
_languages_: "Common, Dwarvish, Elvish"
_skills_: "Intimidation +5, Perception +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "acid, lightning"
_speed_: "30 ft."
_hit points_: "93 (11d8 + 44)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 13 (+1) | 18 (+4) | 6 (-2) | 10 (+0) | 16 (+3) |

___Innate Spellcasting.___ The domovoi's innate spellcasting ability is Charisma (spell save DC 15). He can innately cast the following spells, requiring no material components:

At will: alter self, invisibility

3/day each: darkness, dimension door, haste

**Actions**

___Multiattack.___ The domovoi makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.

