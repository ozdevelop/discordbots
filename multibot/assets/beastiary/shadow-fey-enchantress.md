"Shadow Fey Enchantress";;;_size_: Medium humanoid
_alignment_: lawful evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Elvish, Umbral"
_skills_: "Arcana +4, Deception +7, Perception +6, Persuasion +7, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +5, Wis +6, Cha +7"
_speed_: "30 ft."
_hit points_: "123 (19d8 + 38)"
_armor class_: "16 (breastplate)"
_stats_: | 10 (+0) | 15 (+2) | 14 (+2) | 12 (+1) | 17 (+3) | 18 (+4) |

___Fey Ancestry.___ The shadow fey has advantage on saving throws against being charmed, and magic can't put her to sleep.

___Innate Spellcasting.___ The shadow fey's innate spellcasting ability is Charisma. She can cast the following spells innately, requiring no material components.

4/day: misty step (when in shadows, dim light, or darkness only)

___Spellcasting.___ The shadow fey is a 10th-level spellcaster. Her spellcasting ability is Charisma (save DC 15, +7 to hit with spell attacks). She knows the following bard spells.

Cantrips (at will): blade ward, friends, message, vicious mockery

1st level (4 slots): bane, charm person, faerie fire

2nd level (3 slots): enthrall, hold person

3rd level (3 slots): conjure fey, fear, hypnotic pattern

4th level (3 slots): confusion, greater invisibility, phantasmal killer

5th level (2 slots): animate objects, dominate person, hold monster

___Sunlight Sensitivity.___ While in sunlight, the shadow fey has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

___Traveler in Darkness.___ The shadow fey has advantage on Intelligence (Arcana) checks made to know about shadow roads and shadow magic spells or items.

**Actions**

___Multiattack.___ The shadow fey makes two rapier attacks.

___Rapier.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage plus 17 (5d6) psychic damage.

___Beguiling Whispers (recharge 5-6).___ The shadow fey speaks sweet words to a creature she can see within 60 feet, that can hear the enchantress. The creature must succeed on a DC 15 Charisma saving throw or be charmed for 1 minute. While charmed in this way, the creature has disadvantage on Wisdom and Charisma saving throws made to resist spells cast by the enchantress.

___Leadership (recharges after a Short or Long Rest).___ The enchantress can utter a special command or warning to a creature she can see within 30 feet of her. The creature must not be hostile to the enchantress and it must be able to hear (the command is inaudible to all but the target creature). For 1 minute, the creature adds a d4 to its attack rolls and saving throws. A creature can benefit from only one enchantress's Leadership at a time. This effect ends if the enchantress is incapacitated.

