Abjurer;;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: 9 (5,000 XP)
_languages_: any four languages
_skills_: Arcana +8, History +8
_saving_throws_: Int +8, Wis +5
_speed_: 30 ft.
_hit points_: 84 (13d8+26)
_armor class_: 12 (15 with mage armor)
_stats_: | 9 (-1) | 14 (+2) | 14 (+2) | 18 (+4) | 12 (+1) | 11 (0) |

___Spellcasting.___ The abjurer is a 13th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 16, +8 to hit with spell attacks). The abjurer has the following wizard spells prepared:

* Cantrips (at will): _blade ward. dancing lights, mending, message, ray of frost_

* 1st level (4 slots): _alarm* , mage armor, magic missile, shield* _

* 2nd level (3 slots): _arcane lock* , invisibility_

* 3rd level (3 slots): _counterspell* , dispel magic* , fireball_

* 4th level (3 slots): _banishment* , stoneskin* _

* 5th level (2 slots): _cone of cold, wall of force_

* 6th level (1 slot): _flesh to stone, globe of invulnerability* _

* 7th level (1 slot): _symbol* , teleport_

*Abjuration spell of 1st level or higher

___Arcane Ward.___ The abjurer has a magical ward that has 30 hit points. Whenever the abjurer takes damage, the ward takes the damage instead. If the ward is reduced to 0 hit points, the abjurer takes any remaining damage. When the abjurer casts an abjuration spell of 1st level or higher, the ward regains a number of hit points equal to twice the level of the spell.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 2 (1d6-l) bludgeoning damage, or 3 (1d8-l) bludgeoning damage if used with two hands.
