"Runebound Dire Wolf";;;_size_: Large beast
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +5, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_condition_immunities_: "charmed"
_speed_: "50 ft."
_hit points_: "60 (8d10 + 16)"
_armor class_: "14 (natural armor)"
_stats_: | 20 (+5) | 15 (+2) | 15 (+2) | 6 (-2) | 12 (+1) | 6 (-2) |

___Keen Hearing and Smell.___ The wolf has advantage on
Wisdom (Perception) checks that rely on hearing or
smell.

___Pack Tactics.___ The wolf has advantage on an attack
roll against a creature if at least one of the wolf’s
allies is within 5 feet of the creature and the ally
isn’t incapacitated.

___Runebound.___ The wolf's body is coated in magical
runes granted by a runespeaker that provide it with
additional strength. The wolf gains one of the
following runic bonuses:
* Rune of Fortitude – The wolf's AC is increased to
16. In addition, as long as the wolf has at least 1
hit point, it regenerates 5 hit points at the start
of its turn.
* Rune of Strength – The wolf's bite attack deal an
additional 3 piercing damage and it has
advantage on strength checks.
* Rune of Swiftness – The wolf has advantage on
Dexterity saving throws and its speed is
increased by 20 feet.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 12 (2d6 + 5) piercing damage. If
the target is a creature, it must succeed on a DC 15
Strength saving throw or be knocked prone.
