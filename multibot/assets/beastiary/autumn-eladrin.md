"Autumn Eladrin";;;_page_number_: 195
_size_: Medium fey (elf)
_alignment_: chaotic neutral
_challenge_: "10 (5900 XP)"
_languages_: "Common, Elvish, Sylvan"
_senses_: "darkvision 60 ft., passive Perception 13"
_skills_: "Insight +7, Medicine +7"
_speed_: "30 ft."
_hit points_: "127  (17d8 + 51)"
_armor class_: "19 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 12 (+1) | 16 (+3) | 16 (+3) | 14 (+2) | 17 (+3) | 18 (+4) |

___Enchanting Presence.___ Any non-eladrin creature that starts its turn within 60 feet of the eladrin must make a DC 16 Wisdom saving throw. On a failed save, the creature is charmed by the eladrin for 1 minute. On a successful save, the creature becomes immune to any eladrin's Enchanting Presence for 24 hours.
Whenever the eladrin deals damage to the charmed creature, the creature can repeat the saving throw, ending the effect on itself on a success.

___Fey Step (Recharge 4-6).___ As a bonus action, the eladrin can teleport up to 30 feet to an unoccupied space it can see.

___Innate Spellcasting.___ The eladrin's innate spellcasting ability is Charisma (spell sace DC 16). It can innately cast the following spells, requiring no material components:

* At will: _calm emotions, sleep_

* 3/day each: _cure wounds _(as a 5th-level spell)_, lesser restoration_

* 1/day each: _greater restoration, heal, raise dead_

___Magic Resistance.___ The eladrin has advantage on saving throws against spells and other magical effects.

**Actions**

___Longsword___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) slashing damage plus 18 (4d8) psychic damage, or 6 (1d10 + 1) slashing damage plus 18 (4d8) psychic damage if used with two hands.

___Longbow___ Ranged Weapon Attack: +7 to hit, range 150/600 ft., one target. Hit: 7 (1d8 + 3) piercing damage plus 18 (4d8) psychic damage.

**Reactions**

___Foster Peace___ If a creature charmed by the eladrin hits with an attack roll while whitin 60 feet of the eladrin, the eladrin magically causes the attack to miss, provided the eladrin can see the attacker.
