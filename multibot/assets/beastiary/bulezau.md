"Bulezau";;;_page_number_: 131
_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Abyssal, telepathy 60 ft."
_senses_: "darkvision 120 ft., passive Perception 9"
_damage_immunities_: "poison"
_speed_: "40 ft."
_hit points_: "52  (7d8 + 21)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 15 (+2) | 14 (+2) | 17 (+3) | 8 (-1) | 9 (0) | 6 (-2) |

___Rotting Presence.___ When any creature that isn't a demon starts its turn within 30 feet one or more bulezaus, that creature must succeed on a DC 13 Constitution saving throw or take 1d6 necrotic damage plus 1 necrotic damage for each bulezau within 30 feet of it.

___Standing Leap.___ The bulezau's long jump is up to 20 feet and its high jump is up to 10 feet, with or without a running start.

___Sure-Footed.___ The bulezau has advantage on Strength and Dexterity saving throws made against effects that would knock it prone.

**Actions**

___Barbed Tail___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 8 (1d12 + 2) piercing damage. If the target is a creature, it must succeed on a DC 13 Constitution saving throw against disease or become poisoned until the disease ends. While poisoned in this way, the target sports festering boils, coughs up flies, and sheds rotting skin, and the target must repeat the saving throw after every 24 hours that elapse. On a successful save, the disease ends. On a failed save, the target's hit point maximum is reduced by 4 (1d8). The target dies if its hit point maximum is reduced to 0.