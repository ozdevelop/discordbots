"Ravenfolk Doom Croaker";;;_size_: Medium humanoid
_alignment_: neutral
_challenge_: "5 (1800 XP)"
_languages_: "Common, Feather Speech, Huginn"
_skills_: "Intimidate +5, Perception +10"
_senses_: "darkvision 120 ft., passive Perception 20"
_saving_throws_: "Str +3, Dex+5, Wis +7"
_speed_: "30 ft."
_hit points_: "88 (16d8 + 16)"
_armor class_: "14 (studded leather armor)"
_stats_: | 10 (+0) | 14 (+2) | 12 (+1) | 12 (+1) | 18 (+4) | 14 (+2) |

___Mimicry.___ Ravenfolk doom croakers can mimic the voices of others with uncanny accuracy. They have advantage on Charisma (Deception) checks involving audible mimicry.

___Magic Resistance.___ The doom croaker has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The doom croaker's innate spellcasting ability is Wisdom (spell save DC 15). It can innately cast the following spells, requiring no material components:

At will: comprehend languages

3/day each: counterspell, fear, phantom steed

1/day each: blight, call lightning, clairvoyance, insect plague

1/week: legend lore

**Actions**

___Ghost Wings.___ The ravenfolk doom croaker furiously "beats" a set of phantasmal wings. Every creature within 5 feet of the ravenfolk must make a successful DC 13 Dexterity saving throw or be blinded until the start of the ravenfolk's next turn.

___Radiant Runestaff.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d8) bludgeoning damage plus 4 (1d8) radiant damage.

