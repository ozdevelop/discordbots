"Undead Swordsman";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "understands the languages it knew in its life but can’t speak"
_skills_: "Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "cold, necrotic, poison"
_condition_immunities_: "exhaustion, paralysis, poison"
_speed_: "30 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "16 (chainmail)"
_stats_: | 17 (+3) | 13 (+1) | 13 (+1) | 9 (-1) | 10 (+0) | 12 (+1) |

**Actions**

___Greatsword.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 10 (2d6 + 3) slashing damage.
