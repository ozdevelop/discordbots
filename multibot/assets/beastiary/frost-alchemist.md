"Frost Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_damage_immunities_: "cold"
_speed_: "30 ft."
_hit points_: "45 (6d10 + 12)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 14 (+2) | 16 (+3) | 12 (+1) | 8 (-1) |

___Frozen Flesh.___ A creature that touches the alchemist
or hits it with a melee attack while within 5 feet of
it takes 4 (1d8) cold damage.

**Actions**

___Multiattack.___ The alchemist makes two freezing
strike attacks.

___Freezing Strike.___ Melee Weapon Attack: +5 to hit,
reach 5ft., one target. Hit: 5 (1d4 + 3) bludgeoning
damage plus 5 (2d4) cold damage and the target's
movement speed is reduced by 10 on its next turn.
If the alchemist hits the same target with both of
its freezing strike attacks in a single round, the
target is restrained until the end of their next turn
instead.
