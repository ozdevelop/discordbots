"Baba Yaga's Hut";;;_size_: Gargantuan construct
_alignment_: chaotic neutral
_challenge_: "12 (8,400 XP)"
_languages_: "Understands Common and Sylvan but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Str +10, Con +6"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhausted, frightened, paralyzed, petrified, poisoned, prone"
_speed_: "30 ft."
_hit points_: "154 (11d20 + 33)"
_armor class_: "17"
_stats_: | 23 (+6) | 9 (-1) | 17 (+3) | 7 (-2) | 10 (+0) | 16 (+3) |

___False Appearance.___ While the hut remains motionless, it
is indistinguishable from an ordinary (albeit hideously
decorated) hut.

___Immutable Form.___ Baba Yaga's Hut is immune to any
spell or effect that would alter its form.

___Siege Monster.___ Baba Yaga's Hut deals double damage to
objects and structures.

___A Witch's Best Friend.___ While within 60 feet of Baba
Yaga, the hut gains access to a limited spell list, as
detailed under Shared Spellcasting.

___Shared Spellcasting.___ While Baba Yaga and her hut are
within 60 feet of one another, they can each cast the
following spells but must share the spell slots among
themselves. For casting these spells, the hut is a 7thlevel
spellcaster. Its spellcasting ability is Charisma
(spell save DC is 15, +7 to hit with spell attacks). The
hut knows the following spells, requiring no material
components:

* Cantrips (at will): _druid craft, minor illusion, thorn whip_

* 1st level (4 slots): _entangle, ray of sickness_

* 2nd level (3 slots): _flaming sphere, moonbeam_

* 3rd level (3 slots): _call lightning, plant growth_

* 4th level (1 slot): _blight_

**Actions**

___Multiattack.___ Baba Yaga's Hut makes two attacks. One
with its kick and one with its Gaping Maw.

___Kick.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one
target. Hit 39 (6d10+6) bludgeoning damage. On a hit,
targets Large or smaller must make a DC 15 Strength
saving throw or be knocked prone.

___Gaping Maw.___ Melee Weapon Attack: +9 to hit, reach 5
ft., one target. Hit 32 (6d12+6) piercing damage. On a
hit, targets Large or smaller are grappled as the hut
attempts to eat the target.

___Swallow.___ The hut makes one gaping maw attack against
a Large or smaller target it is grappling. If the attack
hits, the target is swallowed and the grapple ends. The
swallowed target has total cover against attacks and
other effects outside the hut as they float aimlessly in
an extradimensional space. As an action at the end of
their turn, a swallowed creature can make a DC 17
Intelligence Saving Throw to find a means of escape
from the hut. On a success, the creature is spit out in
an unoccupied space within 5 feet of the hut. If the hut
dies, it spits up all swallowed creatures and objects.

___Mega Hop (Recharge 5-6).___ Baba Yaga's Hut hops high
into the air, landing in an unoccupied space within 120
feet of its original location. All creatures within a 30-
foot radius must make a Strength saving throw DC 18
or take 51 (10d8+6) thunder damage and be knocked
prone. On a successful save, a creature takes half
damage and is not knocked prone.
