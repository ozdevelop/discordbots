"Caterprism";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 15"
_condition_immunities_: "prone"
_speed_: "30 ft., burrow 20 ft."
_hit points_: "76 (8d10 + 32)"
_armor class_: "15 (Natural Armor)"
_stats_: | 20 (+5) | 12 (+1) | 18 (+4) | 4 (-3) | 13 (+1) | 11 (+0) |

___Crystalline Mandibles.___ The caterprism’s mandibles ignore resistance to
slashing damage. In addition, when the caterprism attacks a creature with
at least one head with its bite attack and rolls a natural 20 on the attack
roll, it cuts off one of the creature’s heads. The creature dies if it cannot
survive without the lost head. A creature is immune to this ability if it is
immune to slashing damage, doesn’t have or need a head, has legendary
actions, or the GM decides that the creature is too big for the head to be
cut off with this attack. Such a creature instead takes an extra 27 (6d8)
slashing damage from the hit.

___Tunneler.___ Caterprism can burrow through solid rock at 5 feet per round
leaving a 5 foot-wide, 8-foot-high tunnel in its wake.

**Actions**

___Multiattack.___ The caterprism makes one bite and two claw attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 +5) slashing damage. If the caterprism scores a critical hit, it rolls
damage dice four times, instead of twice.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target Hit: 16 (2d10 + 5) slashing damage.

___Crystal Breath Recharge (5–6).___ The caterprism spews forth a
crystalline silk-like substance in a 30-foot cone that instantly hardens into
razor-sharp crystalline spears. Each creature in that area must make a DC
15 Dexterity saving throw, taking 28 (8d6) piercing damage on a failed
save, or half as much damage on a successful one.
