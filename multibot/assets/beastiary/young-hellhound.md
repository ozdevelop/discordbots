"Young Hellhound";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Understands Infernal and Ignan, but cannot speak"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "fire"
_speed_: "40 ft."
_hit points_: "26 (4d8 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 12 (+1) | 14 (+2) | 6 (-2) | 12 (+1) | 5 (-3) |

___Keen Hearing and Smell.___ The hound has advantage
on Wisdom (Perception) checks that rely on
hearing or smell.

___Pack Tactics.___ The hound has advantage on an attack
roll against a creature if at least one of the hound’s
allies is within 5 feet of the creature and the ally
isn’t incapacitated.

___Pounce.___ If the hound moves at least 10 feet straight
toward a target and then hits it with a bite attack on
the same turn, the target takes an extra 2 (1d4)
piercing damage and must succeed on a DC 11
Strength saving throw or be knocked prone.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 5 (1d6 + 2) piercing damage plus 3
(1d6) fire damage.
