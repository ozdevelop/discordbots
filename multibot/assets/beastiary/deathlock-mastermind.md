"Deathlock Mastermind";;;_page_number_: 129
_size_: Medium undead
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 120 ft. (including magical darkness), passive Perception 14"
_skills_: "Arcana +5, History +5, Perception +4"
_damage_immunities_: "poison"
_saving_throws_: "Int +5, Cha +6"
_speed_: "30 ft."
_hit points_: "110  (20d8 + 20)"
_armor class_: "13 (16 with mage armor)"
_condition_immunities_: "exhaustion, poisoned"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 11 (0) | 16 (+3) | 12 (+1) | 15 (+2) | 12 (+1) | 17 (+3) |

___Innate Spellcasting.___ The deathlock's innate spellcasting ability is Charisma (spell save DC 14). It can innately cast the following spells, requiring no material components:

* At will: _detect magic, disguise self, mage armor_

___Spellcasting.___ The deathlock is a 10th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _chill touch, mage hand, minor illusion, poison spray_

* 5th level (2 slots): _arms of Hadar, blight, counterspell, crown of madness, darkness, dimension door, dispel magic, fly, hold monster, invisibility_

___Turn Resistance.___ The deathlock has advantage on saving throws against any effect that turns undead.

**Actions**

___Deathly Claw___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (3d6 + 3 necrotic damage).

___Grave Bolts___ Ranged Spell Attack: +6 to hit, range 120 ft., one or two targets. Hit: 18 (4d8) necrotic damage. If the target is Large or smaller, it must succeed on a DC 16 Strength saving throw or become restrained as shadowy tendrils wrap around it for 1 minute. A restrained target can use its action to repeat the saving throw, ending the effect on itself on a success.
