"Archer";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Acrobatics +6, Perception +5"
_speed_: "30 ft."
_hit points_: "75 (10d8+30)"
_armor class_: "16 (studded leather armor)"
_stats_: | 11 (0) | 18 (+4) | 16 (+3) | 11 (0) | 13 (+1) | 10 (0) |

___Archer's Eye.___ As a bonus action, the archer can add 1d10 to its next attack or damage roll with a longbow or shortbow.

**Actions**

___Multiattack .___ The archer makes two attacks with its longbow.

___Shortsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft. one target. Hit: 7 (1d6+4) piercing damage.

___Longbow.___ Ranged Weapon Attack: +6 to hit, range 150/600 ft, one target. Hit: 8 (1d8+4) piercing damage.