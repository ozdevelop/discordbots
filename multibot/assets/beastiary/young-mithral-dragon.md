"Young Mithral Dragon";;;_size_: Medium dragon
_alignment_: neutral
_challenge_: "6 (2300 XP)"
_languages_: "Celestial, Common, Draconic, Primordial"
_skills_: "Acrobatics +6, Insight +5, Perception +5, Persuasion +5"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 15"
_saving_throws_: "Dex +9, Con +4, Wis +5, Cha +5"
_damage_immunities_: "acid, thunder"
_damage_resistances_: "ludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed"
_speed_: "50 ft., fly 60 ft."
_hit points_: "92 (16d8 + 20)"
_armor class_: "16 (natural armor)"
_stats_: | 13 (+1) | 22 (+6) | 13 (+1) | 14 (+2) | 15 (+2) | 14 (+2) |

___Innate Spellcasting.___ The dragon's innate spellcasting ability is Intelligence. It can innately cast the following spells, requiring no material components:

At will: tongues

3/day: enhance ability

**Actions**

___Multiattack.___ The dragon makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 14 (2d10 + 3) slashing damage, and the target loses 3 hit point from bleeding at the start of each of its turns for six rounds unless it receives magical healing. Bleeding damage is cumulative; the target loses 3 hp per round for each bleeding wound it has taken from a mithral dragon's claws. 

___Breath Weapon (Recharge 5-6).___ A mithral dragon can spit a 50-foot-long, 5-foot-wide line of metallic shards. Targets in its path take 21 (6d6) magical slashing damage and lose another 5 hit points from bleeding at the start of their turns for 6 rounds; slashing damage is halved by a successful DC 12 Dexterity saving throw, but bleeding damage is not affected. Only magical healing stops the bleeding before 6 rounds. The shards dissolve into wisps of smoke 1 round after the breath weapon's use

