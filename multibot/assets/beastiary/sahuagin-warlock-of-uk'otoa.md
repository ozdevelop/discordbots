"Sahuagin Warlock of Uk'otoa";;;_size_: Medium humanoid (sahuagin)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Sahuagin"
_skills_: "Arcana +1, Persuasion +5"
_senses_: "darkvision 120 ft., passive Perception 9"
_speed_: "30 ft., swim 40 ft."
_hit points_: "22 (5d8)"
_armor class_: "14 (natural armor)"
_stats_: | 14 (+2) | 10 (+0) | 11 (+0) | 8 (-1) | 8 (-1) | 16 (+3) |

___Blood Frenzy.___ The warlock has advantage on melee attack rolls against any creature that doesn’t have all its hit points.

___Innate Spellcasting.___ The warlock’s innate spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _eldritch blast, minor illusion_

* 1/day each: _armor of Agathys, arms of Hadar, counterspell, crown of madness, invisibility, hunger of Hadar._

___Limited Amphibiousness.___ The warlock can breathe air and water, but it needs to be submerged at least once every 4 hours to avoid suffocating.

___Shark Telepathy.___ The warlock can magically command any shark within 120 feet of it, using a limited telepathy.

**Actions**

___Multiattack.___ The warlock makes two attacks: one with its bite and one with its Sword of Fathoms.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 4 (1d4 + 2) piercing damage.

___Sword of Fathoms.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) slashing damage, and if the target is a creature, it must succeed on a DC 13 Constitution saving throw or begin choking. The choking creature is incapacitated until the end of its next turn, when the effect ends on it.

___Eldritch Blast (Cantrip).___ Ranged Spell Attack: +5 to hit, range 120 ft., one creature. Hit: 5 (1d10) force damage.
