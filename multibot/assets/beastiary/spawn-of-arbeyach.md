"Spawn Of Arbeyach";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "5 (1800 XP)"
_languages_: "Infernal"
_skills_: "Perception +4, Stealth +5"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 14"
_saving_throws_: "Wis +4"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft., climb 20 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 15 (+2) | 15 (+2) | 10 (+0) | 13 (+1) | 12 (+1) |

___Hive Mind.___ Spawn of Arbeyach share a bond with other members of their hive that enhances their hive mates' perception. As long as a spawn is within 60 feet of at least one hive mate, it has advantage on initiative rolls and Wisdom (Perception) checks. If one spawn is aware of a particular danger, all others in the hive are, too. No spawn in a hive mind is surprised at the beginning of an encounter unless all of them are.

___Innate Spellcasting.___ The spawn of Arbeyach's spellcasting ability is Charisma. The Spawn of Arbeyach can innately cast the following spells, requiring no material components: 1/day: conjure animals (only swarms of insects) Scent Communication. Spawn of Arbeyach can communicate with each other and all swarms of insects within 60 feet via pheromone transmission. In a hive, this range extends to cover the entire hive. This is a silent and instantaneous mode of communication that only Arbeyach, spawn of Arbeyach, and swarms of insects can understand. As a bonus action, the spawn of Arbeyach can use this trait to control and give orders to one swarm of insects within 60 feet.

**Actions**

___Multiattack.___ A Spawn of Arbeyach makes one bite attack and two stinger attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage plus 4 (1d8) poison damage.

___Stinger.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 9 (2d8) poison damage. If the target is a creature, it must succeed on a DC 13 Constitution saving throw or be poisoned for 1 minute. A poisoned creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

