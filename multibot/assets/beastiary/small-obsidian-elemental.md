"Small Obsidian Elemental";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "20 ft."
_hit points_: "45 (6d6 + 24)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 8 (-1) | 18 (+4) | 4 (-3) | 11 (+0) | 11 (+0) |

___Brute.___ A melee weapon attack deals one extra die of its damage when
the obsidian elemental hits with it (included in the attack).

___Death Throes.___ When the obsidian elemental dies, it explodes, and each
creature within 30 feet of it must make a DC 14 Dexterity saving throw,
taking 10 (3d6) slashing damage and 10 (3d6) fire damage on a failed
saving throw, or half as much damage on a successful one.

___Molten Glass.___ A creature that hits the obsidian elemental with a melee
attack while within 5 feet of it takes 7 (2d6) fire damage.

**Actions**

___Multiattack.___ The obsidian elemental makes two claw attacks.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) slashing damage plus 3 (1d6) fire damage.
