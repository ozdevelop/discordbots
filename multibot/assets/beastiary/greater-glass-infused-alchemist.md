"Greater Glass-Infused Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_saving_throws_: "Dex +6, Int +4"
_damage_vulnerabilities_: "bludgeoning, thunder"
_speed_: "40 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "14"
_stats_: | 8 (-1) | 18 (+4) | 14 (+2) | 14 (+2) | 10 (+0) | 8 (-1) |

___Shattering Blast.___ When the alchemist dies, it
explodes in a burst of sharp glass. Each creature
within 5 feet of it must make a make a DC 13
Dexterity saving throw, taking 21 (6d6) slashing
damage on a failed save, or half as much damage on
a successful one.

___Reflective Skin.___ The alchemist has advantage on
saving throws against spells and other magical
effects.

___Vicious Cuts.___ If the alchemist hits the same target
with both slashing strikes on the same round of
combat, the target takes an additional 11 (2d10)
piercing damage as glass shards cut deeply into
flesh.

**Actions**

___Multiattack.___ The alchemist makes two attacks with
its slashing strike

___Slashing Strike.___ Melee Weapon Attack: +6 to hit,
reach 5ft., one target. Hit: 7 (1d6 + 4) slashing
damage.

___Rain of Glass (Recharge 5-6).___ The alchemist holds
out its hands and unleashes a blast of glass shards
in a 15 foot cone. Each creature in that area must
make a DC 13 Dexterity saving throw, taking 22
(4d10) slashing damage on a failed save, or have as
much damage on a successful one.
