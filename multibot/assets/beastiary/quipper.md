"Quipper";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 60 ft."
_speed_: "swim 40 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "13"
_stats_: | 2 (-4) | 16 (+3) | 9 (-1) | 1 (-5) | 7 (-2) | 2 (-4) |

___Blood Frenzy.___ The quipper has advantage on melee attack rolls against any creature that doesn't have all its hit points.

___Water Breathing.___ The quipper can breathe only underwater.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 piercing damage.