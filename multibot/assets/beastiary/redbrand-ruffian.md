"Redbrand Ruffian";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Intimidation +2"
_speed_: "30 ft."
_hit points_: "16 (3d8+3)"
_armor class_: "14 (studded leather armor)"
_stats_: | 11 (0) | 14 (+2) | 12 (+1) | 9 (-1) | 9 (-1) | 11 (0) |

**Actions**

___Multiattack.___ The ruffian makes two melee attacks.

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.