"Water Goblin";;;_size_: Small humanoid
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Goblin, Sylvan"
_skills_: "Acrobatics +4, Athletics +3"
_senses_: "darkvision 120 ft., passive Perception 11"
_saving_throws_: "Dex +4, Cha +5"
_damage_vulnerabilities_: "lightning"
_damage_immunities_: "cold"
_damage_resistances_: "acid; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "20 ft., swim 20 ft."
_hit points_: "13 (3d6 + 3)"
_armor class_: "13 (natural armor), 16 with mage armor"
_stats_: | 13 (+1) | 14 (+2) | 13 (+1) | 10 (+0) | 12 (+1) | 16 (+3) |

___Amphibious.___ Water goblins can breathe air and water.

___Innate Spellcasting.___ The water goblin’s spellcasting ability is Charisma
(spell save DC 13, +5 to hit with spell attacks). The water goblin can
innately cast the following spells, requiring no material components:

* At will: _mending, minor illusion, prestidigitation, ray of frost_

* 3/day each: _create or destroy water, fog cloud, grease, mage armor, purify food and drink, water breathing_

* 1/day: _water walk_

**Actions**

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 5 (1d6 + 2) slashing damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one
target. Hit: 5 (1d6 + 2) piercing damage.

**Reactions**

___Uncanny Dodge.___ When an attacker the water goblin can see hits it with
an attack, it can use its reaction to halve the attack’s damage against it.
