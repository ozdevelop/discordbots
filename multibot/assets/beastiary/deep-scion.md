"Deep Scion";;;_size_: Medium humanoid (shapechanger)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Aquan, Common, thieves' cant"
_senses_: "darkvision 120 ft."
_skills_: "Deception +6, Insight +3, Sleight of Hand +3, Stealth +3"
_saving_throws_: "Wis +3, Cha +4"
_speed_: "30 ft. (20 ft. and swim 40 ft. in hybrid form)"
_hit points_: "67 (9d8+27)"
_armor class_: "11"
_stats_: | 18 (+4) | 13 (+1) | 16 (+3) | 10 (0) | 12 (+1) | 14 (+2) |

___Shapechanger.___ The deep scion can use its action to polymorph into a humanoid-piscine hybrid form, or back into its true form. Its statistics, other than its speed, are the same in each form. Any equipment it is wearing or carrying isn't transformed. The deep scion reverts to its true form if it dies.

___Amphibious (Hybrid Form Only).___ The deep scion can breathe air and water.

**Actions**

___Multiattack.___ In humanoid form, the deep scion makes two melee attacks. In hybrid form, the deep scion makes three attacks: one with its bite and two with its claws.

___Battleaxe (Humanoid Form Only).___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8+4) slashing damage, or 9 (1d10+4) slashing damage if used with two hands.

___Bite (Hybrid Form Only).___ Melee Weapon Attack: +6 to hit, reach 5 ft, one creature. Hit: 6 (1d4+4) piercing damage.

___Claw (Hybrid Form Only).___ Melee Weapon Attack: +6 to hit, reach 5 ft, one target. Hit: 7 (1d6+4) slashing damage.

___Psychic Screech (Hybrid Form Only; Recharges after a Short or Long Rest).___ The deep scion emits a terrible scream audible within 300 feet. Creatures within 30 feet of the deep scion must succeed on a DC 13 Wisdom saving throw or be stunned until the end of the deep scion's next turn. In water, the psychic screech also telepathically transmits the deep scion's memories of the last 24 hours to its master, regardless of distance, so long as it and its master are in the same body of water.