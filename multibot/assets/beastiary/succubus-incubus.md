"Succubus/Incubus";;;_size_: Medium fiend (shapechanger)
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Common, Infernal, telepathy 60 ft."
_senses_: "darkvision 60 ft."
_skills_: "Deception +9, Insight +5, Perception +5, Persuasion +9, Stealth +7"
_speed_: "30 ft., fly 60 ft."
_hit points_: "66 (12d8+12)"
_armor class_: "15 (natural armor)"
_damage_resistances_: "cold, fire, lightning, poison, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 8 (-1) | 17 (+3) | 13 (+1) | 15 (+2) | 12 (+1) | 20 (+5) |

___Telepathic Bond.___ The fiend ignores the range restriction on its telepathy when communicating with a creature it has charmed. The two don't even need to be on the same plane of existence.

___Shapechanger.___ The fiend can use its action to polymorph into a Small or Medium humanoid, or back into its true form. Without wings, the fiend loses its flying speed. Other than its size and speed, its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

**Actions**

___Claw (Fiend Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Charm.___ One humanoid the fiend can see within 30 feet of it must succeed on a DC 15 Wisdom saving throw or be magically charmed for 1 day. The charmed target obeys the fiend's verbal or telepathic commands. If the target suffers any harm or receives a suicidal command, it can repeat the saving throw, ending the effect on a success. If the target successfully saves against the effect, or if the effect on it ends, the target is immune to this fiend's Charm for the next 24 hours.

The fiend can have only one target charmed at a time. If it charms another, the effect on the previous target ends.

___Draining Kiss.___ The fiend kisses a creature charmed by it or a willing creature. The target must make a DC 15 Constitution saving throw against this magic, taking 32 (5d10 + 5) psychic damage on a failed save, or half as much damage on a successful one. The target's hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

___Etherealness.___ The fiend magically enters the Ethereal Plane from the Material Plane, or vice versa.