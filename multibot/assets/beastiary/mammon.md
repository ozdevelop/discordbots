"Mammon";;;_size_: Huge fiend
_alignment_: lawful evil
_challenge_: "25 (75000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Deception +16, Insight +13, Perception +13, Persuasion +16"
_senses_: "truesight 120 ft., passive Perception 23"
_saving_throws_: "Dex +9, Int +14, Wis +13, Cha +16"
_damage_immunities_: "fire, poison; bludgeoning, piercing, and slashing from weapons that aren't silvered"
_damage_resistances_: "cold"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "50 ft."
_hit points_: "378 (28d12 + 196)"
_armor class_: "20 (natural armor)"
_stats_: | 22 (+6) | 13 (+1) | 24 (+7) | 23 (+6) | 21 (+5) | 26 (+8) |

___Innate Spellcasting.___ Mammon's innate spellcasting ability is Charisma (spell save DC 24, +16 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

At will: charm person, detect magic, dispel magic, fabricate (Mammon can create valuable objects), heat metal, magic aura

3/day each: animate objects, counterspell, creation, instant summons, legend lore, teleport

1/day each: imprisonment (minimus containment only, inside gems), sunburst

___Legendary Resistance (3/day).___ If Mammon fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Mammon has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Mammon's weapon attacks are magical.

___Mammon's Lair.___ On initiative count 20 (losing initiative ties), Mammon takes a lair action to cause one of the following effects; Mammon can't use the same effect two rounds in a row:

- Mammon infuses a pile of treasure in his lair with life. It becomes an earth elemental made of precious metals and gems. The elemental acts immediately and lasts until destroyed or until Mammon uses this action again.

- Stacked piles of treasure shift and slide, collapsing onto a creature Mammon can see. The creature is restrained until initiative count 20 on the following round, or until it or an adjacent ally uses an action to make a successful DC 18 Strength check to free it.

- Mammon magically teleports from one area of treasure to another within 150 feet.

___Regional Effects.___ The region containing Mammon's lair is warped by the archdevil's magic, which creates one or more of the following effects:

- Treasure in the possession of creatures other than Mammon turns to worthless materials such as lead, wood, or gravel, after spending 24 hours within 1 mile of the lair. The resulting junk resumes its valuable form when Mammon claims it, or by means of a wish spell or comparable magic.

- Creatures that spend more than 1 hour within 1 mile of Mammon's lair become obsessed with gaining the most generous payment or portion of wealth in any dealing unless they succeed on a DC 18 Wisdom saving throw. A creature that saves successfully is immune to this effect for 24 hours. The effect can be removed by a greater restoration spell or comparable magic.

- Any naturally occurring treasure in Mammon's home plane reverts to worthless junk if removed from the plane. Similarly, any mundane item left behind transforms into something fantastically valuable, as long as it remains on the plane.

 If Mammon dies, conditions in the area surrounding the lair return to normal over the course of 1d10 days.

**Actions**

___Multiattack.___ Mammon makes three attacks.

___Purse.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target. Hit: 19 (3d8 + 6) bludgeoning damage plus 18 (4d8) radiant damage

___Molten Coins.___ Ranged Weapon Attack: +14 to hit, range 40/120 ft., one target. Hit: 16 (3d6 + 6) bludgeoning damage plus 18 (4d8) fire damage.

___Your Weight In Gold (Recharge 5-6).___ Mammon can use this ability as a bonus action immediately after hitting a creature with his purse attack. The creature must make a DC 24 Constitution saving throw. If the saving throw fails by 5 or more, the creature is instantly petrified by being turned to solid gold. Otherwise, a creature that fails the saving throw is restrained. A restrained creature repeats the saving throw at the end of its next turn, becoming petrified on a failure or ending the effect on a success. The petrification lasts until the creature receives a greater restoration spell or comparable magic.

**Legendary** Actions

___Mammon can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. Mammon regains spent legendary actions at the start of his turn.

___Attack.___ Mammon makes one purse or molten coins attack.

___Make It Rain!.___ Mammon casts gold and jewels into a 5-foot radius within 60 feet. One creature within 60 feet of the treasure that can see it must make a DC 24 Wisdom saving throw. On a failure, the creature must use its reaction to move its speed toward the trinkets, which vanish at the end of the turn.

___Deep Pockets (3 actions).___ Mammon recharges his Your Weight In Gold ability.

