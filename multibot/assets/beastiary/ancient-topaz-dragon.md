"Ancient Topaz Dragon";;;_size_: Gargantuan dragon
_alignment_: neutral good
_challenge_: "20 (25,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +12, Insight +9, Perception +9, Religion +12"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 25"
_saving_throws_: "Dex +13, Int +12, Wis +9, Cha +11"
_damage_vulnerabilities_: "psychic"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, slashing, and piercing from nonmagical attacks"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "231 (22d20)"
_armor class_: "22 (natural armor)"
_stats_: | 20 (+5) | 24 (+7) | 10 (+0) | 22 (+6) | 16 (+3) | 20 (+5) |

___Legendary Resistance (3/Day).___ If the dragon fails
a saving throw, it can choose to succeed instead.

___Uplift Aura.___ All allies within 30 feet gain +6 on
Intelligence checks and saving throws.

**Psionics**

___Charges:___ 22 | ___Recharge:___ 1d10 | ___Fracture:___ 23

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 15 ft.,
one target. Hit: 16 (2d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 10 ft.,
one target. Hit: 12 (2d6 + 5) slashing damage.

___Tail.___ Melee Weapon Attack: +11 to hit; reach 20 ft.,
one target. Hit: 14 (2d8 + 5) bludgeoning damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The dragon regains spent
legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) Check.

___Psionics.___ The dragon uses a psionic ability.

___Psionic Shift (Costs 2 Actions).___ The dragon
releases a wave of telekinetic energy from its mind.
Every creature within 15 feet must make a DC 24
Intelligence saving throw or take 13 (2d6 + 6) force
damage and be knocked prone. The dragon then
can move up to half its movement speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the
dragon takes a lair action to cause one of the
following effects. The dragon can’t use the same
effect two rounds in a row.

* The dragon manifests _another world_ at no
cost. It affects up to four creatures.

* The dragon remembers a 5th-level spell it
once researched, and casts it. Its spellcasting
ability is Intelligence, and the save DC to resist
its spells is 21.

* The dragon remembers _counterspell_ and can
cast it as a reaction within the next minute.
The dragon’s spellcasting ability is Intelligence.

**Regional** Effects

Intelligent creatures who sleep within 12 miles of
a topaz dragon’s lair dream of ancient lore they
never studied, and in these dreams they hold
conversations with sages speaking languages the
dreamer does not know, but in the dream can
speak and understand.
