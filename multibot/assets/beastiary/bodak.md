"Bodak";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Abyssal, the languages it knew in life"
_senses_: "darkvision 120 ft."
_skills_: "Perception +4, Stealth +6"
_damage_immunities_: "lightning, poison"
_speed_: "30 ft."
_hit points_: "58 (9d8+18)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "cold, fire, necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 15 (+2) | 16 (+3) | 15 (+2) | 7 (-2) | 12 (+1) | 12 (+1) |

___Aura of Annihilation.___ The bodak can activate or deactivate this feature as a bonus action. While active, the aura deals 5 necrotic damage to any creature that ends its turn within 30 feet of the bodak. Undead and fiends ignore this effect.

___Death Gaze.___ When a creature that can see the bodak's eyes starts its turn within 30 feet of the bodak, the bodak can force it to make a DC 13 Constitution saving throw if the bodak isn't incapacitated and can see the creature. If the saving throw fails by 5 or more, the creature is reduced to 0 hit points, unless it is immune to the frightened condition. Otherwise, a creature takes 16 (3d10) psychic damage on a failed save.

Unless surprised, a creature can avert its eyes to avoid the saving throw at the start of its turn. If the creature does so, it has disadvantage on attack rolls against the bodak until the start of its next turn. If the creature looks at the bodak in the meantime, it must immediately make the saving throw.

___Sunlight Hypersensitivity.___ The bodak takes 5 radiant damage when it starts its turn in sunlight. While in sunlight, it has disadvantage on attack rolls and ability checks.

**Actions**

___Fist.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 4 (1d4+2) bludgeoning damage plus 9 (2d8) necrotic damage.

___Withering Gaze.___ One creature that the bodak can see within 60 feet of it must make a DC 13 Constitution saving throw, taking 22 (4d10) necrotic damage on a failed save, or half as much damage on a successful one.
