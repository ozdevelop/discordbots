"Greater Iron-Infused Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_saving_throws_: "Con +4, Str +6"
_damage_resistances_: "piercing, bludgeoning, and slashing damage from nonmagical weapons"
_speed_: "25 ft."
_hit points_: "60 (8d10 + 16)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 14 (+2) | 12 (+1) | 12 (+1) | 8 (-1) |

___Tough as Nails.___ The alchemist cannot be critically
hit.

**Actions**

___Multiattack.___ The alchemist makes two attacks with
its pummeling strike or two with its iron shard
attack.

___Pummeling Strike.___ Melee Weapon Attack: +6 to hit,
reach 5ft., one target. Hit: 9 (1d10 + 4)
bludgeoning damage and the target must succeed
on a DC 13 Strength saving throw or be knocked
prone.

___Iron Shard.___ Ranged Weapon Attack: +6 to hit, range
20/60 ft., one target. Hit: 10 (1d12 + 4) piercing
damage.
