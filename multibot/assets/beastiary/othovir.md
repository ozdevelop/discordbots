"Othovir";;;_size_: Medium humanoid (illuskan human)
_alignment_: lawful neutral
_challenge_: "0 (10 XP)"
_languages_: "Common, Elvish"
_skills_: "Deception +5, Insight +4, Persuasion +5"
_speed_: "30 ft."
_hit points_: "16 (3d8 + 3)"
_armor class_: "10 (13 with mage armor)"
_senses_: "passive Perception 12"
_stats_: | 11 (0) | 10 (0) | 13 (+1) | 12 (+1) | 14 (+2) | 16 (+3) |

Spellcasting. Othovir is a 2nd-level spellcaster. His spellcasting ability is Charisma (spell save DC 13; +5 to hit with spell attacks). He has the following sorcerer spells prepared:

* Cantrips (at will): _blade ward, fire bolt, mending, prestidigitation_

* 1st level (3 slots): _mage armor, thunderwave, witch bolt_

**Actions**

**Rapier.** Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 4 (1d8) piercing damage.

**Reactions**

**Parry.** Othovir adds 2 to his AC against one melee attack that would hit him. To do so, Othovir must see the attacker and be wielding a melee weapon.

**Roleplaying** Information

Othovir is a gifted harness-maker who doesn't talk about his family or where he came from. He cares about his business, his clients, and his good name.

**Ideal:** Find what you do well, and do it to the best of your ability.

**Bond:** I won't allow my name to be tarnished.

**Flaw:** I get angry when others pry into my private life.
