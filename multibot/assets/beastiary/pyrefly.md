"Pyrefly";;;_size_: Tiny elemental
_alignment_: chaotic neutral
_challenge_: "1/4 (50 XP)"
_languages_: "understands Ignan but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_resistances_: "fire"
_speed_: "5 ft., fly 40 ft."
_hit points_: "14 (4d6)"
_armor class_: "12"
_stats_: | 6 (-2) | 14 (+2) | 10 (+0) | 10 (+0) | 14 (+2) | 4 (-3) |

___Attraction to Fire.___ Pyreflies can detect any nearby
fires and will flock to them, dancing and making
beautiful colors. They grow aggressive when these
fires are put out.

___Illumination.___ The pyrefly sheds bright light in a 5-
foot radius and dim light an additional 5 feet.

**Actions**

___Spark.___ Melee Spell Attack: +4 to hit, reach 5ft., one
target. Hit: 5 (1d6 + 2) fire damage.

___Flare.___ The pyrefly gives off a brilliant flash of light.
All non-pyrefly creatures within 10 feet of the
pyrefly must succeed on a DC 12 Constitution
saving throw or be blinded until the end of their
next turn.

___Ignite (1/Day).___ The pyrefly sets fire to a flammable
object not being worn or carried.
