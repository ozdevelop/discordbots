"Deinonychus";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_skills_: "Perception +3"
_speed_: "40 ft."
_hit points_: "26 (4d8+8)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 15 (+2) | 14 (+2) | 4 (-3) | 12 (+1) | 6 (-2) |

___Pounce.___ If the deinonychus moves at least 20 feet straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 12 Strength saving throw or be knocked prone. If the target is prone, the deinonychus can make one bite attack against it as a bonus action.

**Actions**

___Multiattack.___ The deinonychus makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8+2) piercing damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8+2) slashing damage.