"Worg";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Goblin, Worg"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4"
_speed_: "50 ft."
_hit points_: "26 (4d10+4)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 13 (+1) | 13 (+1) | 7 (-2) | 11 (0) | 8 (-1) |

___Keen Hearing and Smell.___ The worg has advantage on Wisdom (Perception) checks that rely on hearing or smell.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage. If the target is a creature, it must succeed on a DC 13 Strength saving throw or be knocked prone.