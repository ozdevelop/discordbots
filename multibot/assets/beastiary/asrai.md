"Asrai";;;_size_: Tiny fey
_alignment_: chaotic neutral
_challenge_: "2 (450 XP)"
_languages_: "Common, Sylvan"
_skills_: "Perception +3, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "cold"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "20 ft., swim 50 ft."
_hit points_: "7 (2d4 + 2)"
_armor class_: "16 (natural armor)"
_stats_: | 5 (-3) | 18 (+4) | 13 (+1) | 10 (+0) | 13 (+1) | 14 (+2) |

___Limited Amphibious.___ The asrai can breathe both air and water, but it
needs to be submerged at least once every 2 hours to avoid suffocating.

___Innate Spellcasting.___ The asrai’s spellcasting ability is Charisma (spell
save DC 12, +4 to hit with spell attacks). It can innately cast the following
spells, requiring no material components:

* 3/day: _fog cloud_

* 1/day: _control water_

___Spellcasting.___ The asrai is a 5th level spellcaster. Its spellcasting ability
is Charisma (spell save DC 12, +4 to hit with spell attacks.) The asrai can
cast the following spells:

* Cantrips (at will): _dancing lights, ray of frost, resistance, thaumaturgy_

* 1st level (4 slots): _charm person, create or destroy water, detect magic, thunderwave_

* 2nd level (3 slots): _hideous laughter, misty step_

* 3rd level (2 slots): _hypnotic pattern_

**Actions**

___Cold Touch.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target.
Hit: 7 (1d6 + 4) cold damage.
