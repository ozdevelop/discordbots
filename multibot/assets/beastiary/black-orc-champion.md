"Black Orc Champion";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Abyssal, Common, Orc"
_skills_: "Intimidation +7, Perception +4, Religion +2"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Wis +4, Cha +7"
_speed_: "30 ft."
_hit points_: "97 (15d8 + 30)"
_armor class_: "18 (plate)"
_stats_: | 20 (+5) | 14 (+2) | 14 (+2) | 9 (-1) | 12 (+1) | 18 (+4) |

___Blessing of Orcus.___ Black orcs have advantage on saving throws against
the spells and effects of undead creatures.

___Spellcasting.___ The black orc champion is a 7th level spellcaster. Its
spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell
attacks). It has the following paladin spells prepared:

* 1st level (4 slots): _command, detect evil and good, false life, protection
from evil and good, shield of faith_

* 2nd level (3 slots): _magic weapon, silence, protection from poison_

___Unholy Strike.___ Once on each of the black orc champion’s turns when it
hits a creature with a melee weapon attack, the champion can cause the attack to
deal an extra 13 (3d8) necrotic damage to the target.

**Actions**

___Multiattack.___ The black orc champion makes two melee attacks.

___Greatsword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target.
Hit: 12 (2d6 + 5) slashing damage.

___Light Crossbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft.,
one target. Hit: 6 (1d8 + 2) piercing damage.

___Dreadful Glare (Recharges on a Short or Long Rest).___ Each enemy
within 30 feet of the champion must succeed on a DC 15 Wisdom saving
throw or drop whatever it is holding and become frightened for 1 minute.
A frightened creature can repeat the saving throw on the end of each of its
turns, ending the effect on a success.

**Reactions**

___Parry.___ The black orc champion adds 3 to its AC against one melee
attack that would hit it. To do so, the champion must see the attacker and
be wielding a melee weapon.
