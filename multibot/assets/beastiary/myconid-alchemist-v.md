"Myconid Alchemist V";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "10 (5,900 XP)"
_languages_: "Undercommon"
_skills_: "Stealth +7"
_senses_: "darkvision 120 ft., passive Perception 14"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "104 (16d8 + 32)"
_armor class_: "17 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 12 (+1) | 10 (+0) | 8 (-1) |

___Stalk-still.___ A myconid that remains perfectly still has advantage on
Dexterity (Stealth) checks made to hide.

**Actions**

___Stone Knuckles.___ Melee weapon attack: +7 to hit. Hit: 10 (2d6 + 3)
slashing damage.

___Spore Bomb.___ Ranged weapon attack: +8 to hit, range 30/60 ft. Hit: 15
(6d4) poison damage and the target must succeed on a DC 15 Constitution
saving throw or drop to 0 hit points.
