"Snow Queen";;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: "16 (15000 XP)"
_languages_: "Common, Elvish, Giant, Sylvan"
_skills_: "Perception +10, Stealth +9"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 20"
_saving_throws_: "Dex +9, Con +7, Cha +9"
_damage_immunities_: "cold"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't cold iron"
_condition_immunities_: "charmed, frightened, exhaustion"
_speed_: "40 ft."
_hit points_: "123 (19d8 + 38)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 18 (+4) | 14 (+2) | 18 (+4) | 20 (+5) | 18 (+4) |

___Innate Spellcasting.___ The Snow Queen's innate spellcasting ability score is Charisma (save DC 17, +9 to hit with spell attacks). The Snow Queen can innately cast the following spells, requiring no material components.

At will: fog cloud, magic missile, ray of frost

3/day each: chill metal (as heat metal, but does cold damage), freezing sphere

1/day: cone of cold

___Legendary Resistance (3/day).___ If the Snow Queen fails a saving throw, she can choose to succeed instead.

___Magic Weapons.___ The Snow Queen's weapon attacks are magical and do an extra 7 (2d6) cold damage (included below).

___Winter Step.___ The Snow Queen ignores difficult terrain caused by ice and snow. She can walk on vertical and horizontal surfaces that are covered by ice or snow.

___The Snow Queen's Lair.___ On initiative count 20 (losing initiative ties), the Snow Queen takes a lair action to cause one of the following effects; the Snow Queen can't use the same effect two rounds in a row:

- A wall of dense snow springs into existence within 120 feet of the Snow Queen. The wall is up to 60 feet long, 10 feet high, and 5 feet thick, and it blocks line of sight. When the wall appears, each creature in the wall's area must make a DC 15 Dexterity saving throw. A creature that fails the saving throw takes 18 (4d8) cold damage and is pushed 5 feet out of the wall's space, on whichever side of the wall it chooses. A creature that touches the wall at any time takes the same damage. Each 10-foot section of the wall has AC 5, 15 hit points, vulnerability to fire damage, resistance to bludgeoning and piercing damage, and immunity to cold, poison, and psychic damage. The wall lasts until the Snow Queen uses this action again, or she dies.

- Icy wind and stinging snow swirls around the Snow Queen. All creatures that aren't immune to cold damage have disadvantage when making saving throws against cold damage or cold-based effects while within 60 feet of the Snow Queen. This effect lasts until initiative count 20 on the following round.

- The snow and ice of the lair shimmer and gleam, catching the light and flashing it into the eyes of the Snow Queen's foes. Attack rolls against the Snow Queen and her allies within the lair have disadvantage until initiative count 20 on the following round.

___Regional Effects.___ The region containing the Snow Queen's lair is warped by the fey lady's magic, which creates one or more of the following effects:

- Within 10 miles of the lair, snow and ice resist melting. Snow and ice can be melted only with prolonged contact with fire.

- The sky is overcast most of the time within 10 miles of the lair, and snowfall is common. The area is difficult terrain for Tiny, Small, and Medium creatures because of deep snow.

- Light snowfall or swirling powder blown by the wind lightly obscures the area within 5 miles of the lair.

If the Snow Queen dies, conditions in the area surrounding the lair return to normal over the course of 1d10 days.

**Actions**

___Multiattack.___ The Snow Queen makes two attacks in any combination of her claws and her ice crown.

___Claws.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 7 (2d6) cold damage.

___Ice Crown.___ Ranged Weapon Attack: +9 to hit, range 80/320ft., one target. Hit: 13 (2d8 + 4) piercing damage plus 7 (2d6) cold damage. The target's speed is reduced by 10 feet until the end of its next turn.

___Cold Snap (Recharge 5-6).___ The Snow Queen causes the temperature around her to drop dramatically. Every creature within 30 feet of the Snow Queen must make a successful DC 17 Constitution saving throw or gain 1 level of exhaustion. Immunity to cold grants immunity to Cold Snap.

**Reactions**

___Frozen Shards.___ When the Snow Queen is hit by a melee attack, she can strike her attacker with shards from her icy crown. The attacker takes 11 (2d10) piercing damage and 11 (2d10) cold damage, or no damage with a successful DC 17 Dexterity saving throw.

**Legendary** Actions

___The Snow Queen can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The Snow Queen regains spent legendary actions at the start of her turn.

___Ice Crown.___ The Snow Queen makes an ice crown attack.

___Snowblind.___ One target that the Snow Queen can see within 100 feet must succeed on a DC 17 Constitution saving throw or be blinded by swirling snow until the end of its next turn.

___Snowfall Mantle (2 Actions).___ The Snow Queen throws her mantle of snow at a point she can see within 60 feet. An area within 30 feet of that point instantly becomes covered in deep snow. Creatures in the area must succeed on a DC 17 Strength saving throw or be restrained. The area becomes difficult terrain, costing 2 feet of movement for every 1 foot moved. The difficult terrain lasts until the end of the Snow Queen's next turn, at which time her mantle returns to her shoulders.

