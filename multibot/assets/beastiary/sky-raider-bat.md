"Sky Raider Bat";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "Blindsight 60 ft., Passive Perception 11"
_speed_: "10 ft., fly 60 ft."
_hit points_: "18 (4d8)"
_armor class_: "13"
_stats_: | 15 (+2) | 16 (+3) | 11 (+0) | 2 (-4) | 12 (+1) | 6 (-2) |

___Echolocation.___ The bat can’t use its blindsight while
deafened.

___Flyby.___ The bat doesn’t provoke opportunity attacks
when it flies out of an enemies reach.

___Keen Hearing.___ The bat has advantage on Wisdom
(Perception) checks that rely on hearing.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one creature. Hit: 5 (1d6 +2) piercing damage.

**Bonus** Actions

___Goblin Boomer (2/Day).___ As a bonus action, the
sky raider bat drops a ceramic pot of alchemist’s
fire on a random target. The target must succeed
on a DC 14 Dexterity saving throw or take 2 (1d4)
fire damage, and 2 (1d4) fire damage at the start of
each of its turns. A creature can end this damage by
using its action to make a DC 10 Dexterity check to
extinguish the flames.
