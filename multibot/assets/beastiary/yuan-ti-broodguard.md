"Yuan-ti Broodguard";;;_size_: Medium humanoid (yuan-ti)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Abyssal, Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Perception +2"
_damage_immunities_: "poison"
_saving_throws_: "Str +4, Dex +4, Wis +2"
_speed_: "30 ft."
_hit points_: "45 (7d8+14)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 15 (+2) | 14 (+2) | 14 (+2) | 6 (-2) | 11 (0) | 4 (-3) |

___Mental Resistance.___ The broodguard has advantage on saving throws against being charmed, and magic can't paralyze it.

___Reckless.___ At the start of its turn, the broodguard can gain advantage on all melee weapon attack rolls it makes during that turn, but attack rolls against it have advantage until the start of its next turn.

___Variant: Chameleon Skin.___ The yuan-ti has advantage on Dexterity (Stealth) checks made to hide.

___Variant: Shed Skin (1/Day).___ The yuan-ti can shed its skin as a bonus action to free itself from a grapple, shackles, or other restraints. If the yuan-ti spends 1 minute eating its shed skin, it regains hit points equal to half its hit point maximum.

**Actions**

___Multiattack.___ The broodguard makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8+2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) slashing damage.