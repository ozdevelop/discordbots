"White Maw";;;_size_: Gargantuan ooze
_alignment_: chaotic neutral
_challenge_: "10 (5,900 XP)"
_languages_: "telepathy 50 ft."
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_damage_immunities_: "poison"
_speed_: "10 ft."
_hit points_: "217 (14d20+70)"
_armor class_: "5"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, poisoned, prone"
_damage_resistances_: "acid, cold, fire"
_stats_: | 18 (+4) | 1 (-5) | 20 (+5) | 12 (+1) | 10 (0) | 3 (-4) |

___Ooze Nature.___ White Maw doesn't require sleep.

___Amorphous Form.___ White Maw can occupy another creature's space and vice versa.

___Corrode Metal.___ Any nonmagical weapon made of metal that hits White Maw corrodes. After dealing damage, the weapon takes a permanent and cumulative -1 penalty to damage rolls. If its penalty drops to -5, the weapon is destroyed. Nonmagical ammunition made of metal that hits White Maw is destroyed after dealing damage.

White Maw can eat through 2-inch-thick, nonmagical metal in 1 round.

___False Appearance.___ While White Maw remains motionless, it is indistinguishable from white stone.

___Killer Response.___ Any creature that starts its turn in White Maw's space is targeted by a pseudopod attack if White Maw isn't incapacitated.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 22 (4d8 + 4) bludgeoning damage plus 9 (2d8) acid damage. If the target is wearing nonmagical metal armor, its armor is partly corroded and takes a permanent and cumulative -1 penalty to the AC it offers. The armor is destroyed if the penalty reduces its AC to 10.
