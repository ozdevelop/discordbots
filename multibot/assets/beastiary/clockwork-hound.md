"Clockwork Hound";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "understands Common"
_skills_: "Athletics +7, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +4, Con +4"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "50 ft."
_hit points_: "71 (11d8 + 22)"
_armor class_: "12 (natural armor)"
_stats_: | 16 (+3) | 15 (+2) | 14 (+2) | 1 (-5) | 10 (+0) | 1 (-5) |

___Immutable Form.___ The clockwork hound is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The clockwork hound has advantage on saving throws against spells and other magical effects.

___Diligent Tracker.___ Clockwork hounds are designed to guard areas and track prey. They have advantage on all Wisdom (Perception) and Wisdom (Survival) checks when tracking.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 16 (2d10 + 5) piercing damage.

___Tripping Tongue.___ Melee Weapon Attack: +5 to hit, reach 15 ft., one target. Hit: 9 (1d8 + 5) slashing damage, and the target must succeed on a DC 13 Strength saving throw or be knocked prone.

___Explosive Core.___ The mechanism that powers the hound explodes when the construct is destroyed. All creatures within 5 feet of the hound take 7 (2d6) fire damage, or half damage with a successful DC 12 Dexterity saving throw.

