"Siren";;;_size_: Medium fey
_alignment_: chaotic good
_challenge_: "3 (700 XP)"
_languages_: "Common, Elvish, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Medicine +4, Nature +3, Stealth +6, Survival +4"
_speed_: "30 ft., swim 30 ft."
_hit points_: "38 (7d8+7)"
_armor class_: "14"
_stats_: | 10 (0) | 18 (+4) | 12 (+1) | 13 (+1) | 14 (+2) | 16 (+3) |

___Source.___ tales from the yawning portal,  page 243

___Amphibious.___ Siren can breathe air and water.

___Innate Spellcasting.___ Siren's innate spellcasting ability is Charisma (spell save DC 13). She can innately cast the following spells, requiring no material components:

* 1/day each: _charm person, fog cloud, greater invisibility, polymorph _(self only).

___Magic Resistance.___ Siren has advantage on saving throws against spells and other magical effects.

**Actions**

___Shortsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Stupefying Touch.___ Siren touches one creature she can see within 5 feet of her. The creature must succeed on a DC 13 Intelligence saving throw or take 13 (3d6 + 3) psychic damage and be stunned until the start of Siren's next turn.
