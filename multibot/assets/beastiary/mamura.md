"Mamura";;;_size_: Small aberration
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Elvish, Goblin, Sylvan, Void Speech"
_skills_: "Acrobatics +7, Perception +6, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +7, Con +7, Cha +6"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "20 ft., fly 30 ft."
_hit points_: "97 (13d6 + 52)"
_armor class_: "16 (natural armor)"
_stats_: | 8 (-1) | 18 (+4) | 19 (+4) | 17 (+3) | 11 (+0) | 16 (+3) |

___All-Around Vision.___ Attackers never gain advantage on attacks or bonus damage against a mamura from the presence of nearby allies.

___Magic Resistance.___ The mamura has advantage on saving throws against spells and other magical effects.

___Friend to Darkness.___ In darkness or dim light, the mamura has advantage on Stealth checks. It can attempt to hide as a bonus action at the end of its turn if it's in dim light or darkness.

___Distraction.___ Because of the mamura's alien and forbidding aura, any spellcaster within 20 feet of the mamura must make a successful DC 14 spellcasting check before casting a spell; if the check fails, they lose their action but not the spell slot. They must also make a successful DC 14 spellcasting check to maintain concentration if they spend any part of their turn inside the aura.

___Flyby.___ The mamura doesn't provoke an opportunity attack when it flies out of an enemy's reach.

**Actions**

___Multiattack.___ The mamura makes three claw attacks and one whiptail sting attack.

___Claw.___ Melee Weapon Attack: +7 to hit, range 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, and the target must succeed on a DC 15 Constitution saving throw or be poisoned for 1 round. The poison duration is cumulative with multiple failed saving throws.

___Whiptail Stinger.___ Melee Weapon Attack: +7 to hit, range 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage, and the target must succeed on a DC 15 Constitution saving throw or take 1d6 poison damage. If the target is also poisoned by the mamura's claws, it takes another 1d6 poison damage at the start of each of its turns while the poisoning remains in effect.

