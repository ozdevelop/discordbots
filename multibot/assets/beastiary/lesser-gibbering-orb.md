"Lesser Gibbering Orb";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "14 (11,500 XP)"
_languages_: "all"
_skills_: "Perception +12"
_senses_: "darkvision 60 ft., passive Perception 20"
_condition_immunities_: "prone"
_speed_: "5 ft., fly 30 ft."
_hit points_: "136 (16d10 + 48)"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 17 (+3) | 17 (+3) | 20 (+5) | 14 (+2) | 21 (+5) |

___Hyper-Awareness.___ A lesser gibbering orb can see in all directions at
once and cannot be surprised.

___Flyby.___ The lesser gibbering orb doesn’t provoke an opportunity attack
when it flies out of an enemy’s reach.

___Gibbering.___ The gibbering orb babbles incoherently while it can see any
creature and isn’t incapacitated. Each creature that starts its turn within
20 feet of the orb that can hear the gibbering must succeed on a DC 16
Wisdom saving throw. On a failure, the creature can’t take reactions until
the start of its next turn and rolls a d8 to determine what it does during
its turn. On a 1 to 4, the creature does nothing. On a 5 or 6, the creature
takes no action or bonus action and uses all its movement to move in a
randomly determined direction. On a 7 or 8, the creature makes a melee
attack against a randomly determined creature within its reach or does
nothing if it can’t make such an attack.

___Esoteric Theft.___ When a creature dies by being swallowed whole (or
when a creature killed by the lesser gibbering orb in some other fashion is
eaten by it), the lesser gibbering orb absorbs the creature’s known spells,
prepared spells, and innate magic abilities. The orb can use one of the
absorbed abilities per turn as a bonus action. Each originates from an eye
that is not producing an eye ray that round. Stolen spells and innate magic
abilities are lost after 24 hours.

**Actions**

___Multiattack.___ The lesser gibbering orb makes three bite attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 21
(4d8 + 3) piercing damage. The target is grappled (escape DC 13) if the
lesser gibbering orb isn’t already grappling a creature, and the target is
restrained until the grapple ends.

___Swallow.___ The lesser gibbering orb makes one bite attack against a
Medium or smaller target it is grappling. If the attack hits, the target is
swallowed, and the grapple ends. The swallowed target is blinded and
restrained, it has total cover against attacks and other effects outside the
gibbering orb, and it takes 10 (3d6) acid damage at the start of each of the
lesser gibbering orb’s turns. The gibbering orb can have only one target
swallowed at a time.

If the gibbering orb takes 30 damage or more on a single turn from
the swallowed creature, the gibbering orb must succeed on a DC
13 Constitution saving throw at the end of that turn or regurgitate the
creature, which falls prone in a space within 10 feet of the orb. If the lesser
gibbering orb dies, a swallowed creature is no longer restrained by it and
can escape from the corpse using 5 feet of movement, exiting prone.

___Eye Rays.___ The orb casts three of the following spells as eye rays at
random (reroll duplicates), choosing one to three targets it can see within
150 ft. of it. The spells have a save DC of 18 and a +10 to hit.  Roll a d20 to determine the effects:

1. _acid arrow_
2. _blindness/deafness_
3. _chill touch_
4. _color spray_
5. _enthrall_
6. _dispel magic_
7. _flaming sphere_
8. _grease_
9. _hypnotic pattern_
10. _inflict wounds_
11. _bestow curse_
12. _magic missile_
13. _ray of enfeeblement_
14. _ray of frost_
15. _shatter_
16. _sleep_
17. _slow_
18. _scorching ray_
19. _lightning bolt_
20. _hideous laughter_
