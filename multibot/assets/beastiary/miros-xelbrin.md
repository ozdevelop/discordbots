"Miros Xelbrin";;;_size_: Medium humanoid (damaran human)
_alignment_: neutral good
_challenge_: "0 (10 XP)"
_languages_: "Common"
_skills_: "Intimidation +4, Perception +3"
_speed_: "30 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "10"
_senses_: " passive Perception 13"
_stats_: | 16 (+3) | 10 (0) | 15 (+2) | 11 (0) | 12 (+1) | 14 (+2) |

**Actions**

___Bear Hug.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 5 (1d4+3) bludgeoning damage, and the target grappled (escape DC 13) and takes 5 (1d4 + 3) bludgeoning damage at the start of each of Miros's turns until the grapple ends. Miros cannot make attacks while grappling a creature.

___Club.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4+1) bludgeoning damage

___Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage. Miros carries ten crossbow bolts.

**Roleplaying** Information

Innkeeper Miros is a retired carnival attraction, dubbed "the Yeti" because of his barrel-shaped body and the thick, white hair covering his arms, chest, back, and head. When Goldenfields suffers, so does his business, so he takes strides to protect the compound.

**Ideal:** "As does the Emerald Enclave, I believe that civilization and the wilderness need to learn to coexist."

**Bond:** "Make fun of me all you like, but don't speak ill of my inn or my employees."

**Flaw:** "When something upsets me, I have a tendency to fly into a rage."