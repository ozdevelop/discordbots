"Phlogiston Bush";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft., passive Perception 11"
_damage_resistances_: "fire"
_condition_immunities_: "blinded, deafened"
_speed_: "0 ft."
_hit points_: "32 (5d8 + 10)"
_armor class_: "13 (natural armor)"
_stats_: | 17 (+3) | 13 (+1) | 14 (+2) | 2 (-4) | 12 (+1) | 2 (-4) |

___Death Throes.___ If a phlogiston bush is reduced to 0 or fewer hit points,
it explodes in a concussive blast of fire in a 10-foot radius. All creatures
within the area must make a DC 13 Dexterity saving throw, taking 17 (5d6)
fire damage on a failed save, or half as much damage on a successful one.

**Actions**

___Multiattack.___ The phlogiston bush makes two attacks with its tendrils.

___Tendrils.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 17
(4d6 + 3) slashing damage.

___Fire Bolt.___ Ranged Weapon Attack: +5 to hit, range 40 ft., one target.
Hit: 10 (3d6) fire damage.
