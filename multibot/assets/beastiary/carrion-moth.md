"Carrion Moth";;;_size_: Large aberration
_alignment_: Neutral
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "30 ft., climb 15 ft., fly 60 ft."
_hit points_: "104 (11d10 + 44)"
_armor class_: "14 (Natural Armor)"
_stats_: | 16 (+3) | 18 (+4) | 18 (+4) | 6 (-2) | 15 (+2) | 6 (-2) |

___Death Throes.___ When the carrion moth reaches 0 hit points, it splits
open and a gas spews forth in a 10-foot radius. All creatures in the area
except for other carrion moths must succeed on a DC 15 Constitution
saving throw or be poisoned for 1 minute. A poisoned creature can
repeat the saving throw at the end of each of its turns, ending the effect
on a success.

___Drone.___ A creature who begins its turn within 60 feet of the carrion moth
must succeed on a DC 15 Wisdom saving throw or be incapacitated until
the beginning of its next turn. If a creature succeeds on the saving throw,
or the condition ends on it, it is immune to the moth’s drone for 24 hours.

**Actions**

___Multiattack.___ The carrion moth makes one bite attack and two tentacles
attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage.

___Tentacles.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit:
14 (2d10 + 3) bludgeoning damage, and the target must succeed on a DC
15 Constitution saving throw or be poisoned for 1 minute. While poisoned,
the target is paralyzed. A poisoned creature can repeat the saving throw at
the end of each of its turns, ending the effect on a success.
