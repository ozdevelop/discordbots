"Gloom Weaver";;;_page_number_: 224
_size_: Medium humanoid (elf)
_alignment_: neutral
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Elvish"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "necrotic"
_saving_throws_: "Dex +8, Con +6"
_speed_: "30 ft."
_hit points_: "104  (16d8 + 32)"
_armor class_: "14 (17 with mage armor)"
_condition_immunities_: "charmed, exhaustion"
_stats_: | 11 (0) | 18 (+4) | 14 (+2) | 15 (+2) | 12 (+1) | 18 (+4) |

___Burden of Time.___ Beasts and humanoids, other than shadar-kai, have disadvantage on saving throws while within 10 feet of the gloom weaver.

___Fey Ancestry.___ The gloom weaver has advantage on saving throws against being charmed, and magic can't put it to sleep.

___Innate Spellcasting.___ The gloom weaver's innate spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _arcane eye, mage armor, speak with dead_

* 1/day each: _arcane gate, bane, compulsion, confusion, true seeing_

___Spellcasting.___ The gloom weaver is a 12th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _minor illusion, prestidigitation, chill touch _(3d8 damage)_, eldritch blast _(3 beams +4 bonus to each damage roll)

* 5th level (3 slots): _armor of Agathys, blight, darkness, dream, invisibility, fear, hypnotic pattern, major image, contact other plane, vampiric touch, witch bolt_


**Actions**

___Multiattack___ The gloom weaver makes two spear attacks and casts one spell that takes 1 action to cast.

___Shadow Spear___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 26 (4d12) necrotic damage, or 8 (1d8 + 4) piercing damage plus 26 (4d12) necrotic damage if used with two hands.
