"Swarm of Daggers";;;_size_: Medium swarm of tiny constructs
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: "Blindsight 60 ft. (blind beyond this radius), Passive Perception 8"
_damage_immunities_: "Poison, Psychic"
_condition_immunities_: "Blinded, Charmed, Deafened, Frightened, Paralyzed, Petrified, Poisoned"
_speed_: "0 ft., fly 50 ft."
_hit points_: "22 (5d8)"
_armor class_: "17 (natural armor)"
_stats_: | 3 (-4) | 13 (+1) | 10 (+0) | 1 (-5) | 7 (-2) | 1 (-5) |

___Swarm.___ The swarm can occupy another creature’s
space and vice versa, and the swarm can move through
any opening large enough for a Tiny insect. The swarm
can’t regain hit points or gain temporary hit points.

**Actions**

___Dagger.___ Melee Weapon Attack: +3 to hit, reach 0
ft., one target in the swarm’s space. Hit: 10 (4d4)
piercing damage, or 5 (2d4) piercing damage if the
swarm has half of its hit points or fewer.
