"Berbalang";;;_page_number_: 120
_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "all, but rarely speaks"
_senses_: "truesight 120 ft., passive Perception 12"
_skills_: "Arcana +5, History +5, Insight +2, Perception +2, Religion +5"
_saving_throws_: "Dex +5, Int +5"
_speed_: "30 ft., fly 40 ft."
_hit points_: "38  (11d8 - 11)"
_armor class_: "14 (natural armor)"
_stats_: | 9 (0) | 16 (+3) | 9 (0) | 17 (+3) | 11 (0) | 10 (0) |

___Spectral Duplicate (Recharges after a Short or Long Rest).___ As a bonus action, the berbalang creates one spectral duplicate of itself in an unoccupied space it can see within 60 feet of it. While the duplicate exists, the berbalang is unconscious. A berbalang can have only one duplicate at a time. The duplicate disappears when it or the berbalang drops to 0 hit points or when the berbalang dismisses it (no action required).

The duplicate has the same statistics and knowledge as the berbalang, and everything experienced by the duplicate is known by the berbalang. All damage dealt by the duplicate's attacks is psychic damage.

___Innate Spellcasting.___ The berbalang's innate spellcasting ability is Intelligence (spell save DC 13). The berbalang can innately cast the following spells, requiring no material components:

* At will: _speak with dead_

* 1/day: _plane shift _(self only)

**Actions**

___Multiattack___ The berbalang makes two attacks: one with its bite and one with its claws.

___Bite___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage.

___Claws___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) slashing damage.
