"Woolly Mammoth";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_damage_resistances_: "cold"
_speed_: "40 ft."
_hit points_: "73 (7d12 + 28)"
_armor class_: "13 (natural armor)"
_stats_: | 22 (+6) | 9 (-1) | 18 (+4) | 3 (-4) | 13 (+1) | 6 (-2) |

___Keen Smell.___ The mammoth has advantage on Wisdom (Perception)
checks that rely on smell.

___Charge.___ If the mammoth moves at least 20 feet straight toward a
creature and then hits it with a gore attack on the same turn, that target
must succeed on a DC 17 Strength saving throw or be knocked prone. If
the target is prone, the mammoth can make one stomp attack against it as
a bonus action.

**Actions**

___Multiattack.___ The mammoth makes two attacks: one with its gore and
one with its slam.

___Gore.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16
(3d6 + 6) piercing damage.

___Slam.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 15
(2d8 + 6) bludgeoning damage.

___Stomp.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16
(3d6 + 6) bludgeoning damage.
