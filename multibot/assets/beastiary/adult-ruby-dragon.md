"Adult Ruby Dragon";;;_size_: Huge dragon
_alignment_: lawful neutral
_challenge_: "16 (50,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +10, Insight +8, Perception +8, Religion +10"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 23"
_saving_throws_: "Dex +11, Int +10, Wis +8, Cha +10"
_damage_vulnerabilities_: "psychic"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, slashing, and piercing from nonmagical attacks"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "135 (18d12 + 18)"
_armor class_: "20 (natural armor)"
_stats_: | 20 (+5) | 22 (+6) | 12 (+1) | 20 (+5) | 16 (+3) | 20 (+5) |

___Legendary Resistance (3/Day).___ If the dragon fails
a saving throw, it can choose to succeed instead.

___Amplification Aura.___ Allies’ spells cast within 30
feet have their saving throw DC increased by 3.

**Psionics**

___Charges:___ 18 | ___Recharge:___ 1d8 | ___Fracture:___ 22

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft.,
one target. Hit: 16 (2d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft.,
one target. Hit: 12 (2d6 + 5) slashing damage.

___Tail.___ Melee Weapon Attack: +10 to hit; reach 15 ft.,
one target. Hit: 14 (2d8 + 5) bludgeoning damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The dragon regains spent
legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) Check.

___Psionics.___ The dragon uses a psionic ability.

___Psionic Shift (Costs 2 Actions).___ The dragon
releases a wave of telekinetic energy from its mind.
Every creature within 15 feet must make a DC 24
Intelligence saving throw or take 12 (2d6 + 5) force
damage and be knocked prone. The dragon then
can move up to half its movement speed.
