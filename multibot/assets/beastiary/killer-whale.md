"Killer Whale";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "blindsight 120 ft."
_skills_: "Perception +3"
_speed_: "swim 60 ft."
_hit points_: "90 (12d12+12)"
_armor class_: "12 (natural armor)"
_stats_: | 19 (+4) | 10 (0) | 13 (+1) | 3 (-4) | 12 (+1) | 7 (-2) |

___Echolocation.___ The whale can't use its blindsight while deafened.

___Hold Breath.___ The whale can hold its breath for 30 minutes

___Keen Hearing.___ The whale has advantage on Wisdom (Perception) checks that rely on hearing.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 21 (5d6 + 4) piercing damage.