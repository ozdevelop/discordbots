"Clockwork Beetle Swarm";;;_size_: Large swarm
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "fire, poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing"
_condition_immunities_: " charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "30 ft., fly 50 ft."
_hit points_: "52 (8d10 + 8)"
_armor class_: "14 (natural armor)"
_stats_: | 8 (-1) | 16 (+3) | 12 (+1) | 4 (-3) | 12 (+1) | 7 (-2) |

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny construct. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Bites.___ Melee Weapon Attack: +5 to hit, reach 0 ft., up to 4 creatures in the swarm's space. Hit: 17 (5d6) piercing damage plus 3 (1d6) poison damage.

