"Burrowshark";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common"
_skills_: "Animal Handling +2, Athletics +6, Intimidation +3, Perception +2"
_speed_: "30 ft."
_hit points_: "82 (11d8+33)"
_armor class_: "18 (plate)"
_stats_: | 18 (+4) | 12 (+1) | 16 (+3) | 10 (0) | 11 (0) | 13 (+1) |

___Bond of the Black Earth.___ The burrowshark is magically bound to a bulette trained to serve as its mount. While mounted on its bulette, the burrowshark shares the bulette's senses and can ride the bulette while it burrows. The bonded bulette obeys the burrowshark's commands. If its mount dies, the burrowshark can train a new bulette to serve as its bonded mount, a process requiring a month.

**Actions**

___Multiattack.___ The burrowshark makes three melee attacks.

___Spear.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage, or 8 (1d8 + 4) piercing damage if used with two hands to make a melee attack.

**Reactions**

___Unyielding.___ When the burrowshark is subjected to an effect that would move it, knock it prone, or both, it can use its reaction to be neither moved nor knocked prone.