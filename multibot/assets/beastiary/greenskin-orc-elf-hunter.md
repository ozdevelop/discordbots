"Greenskin Orc Elf Hunter";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Elvish, Orc, Sylvan"
_skills_: "Acrobatics +8, Intimidation +2, Nature +2, Perception +5, Stealth +8 (+11 in forests), Survival +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Str +6, Dex +8"
_speed_: "35 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "17 (studded leather)"
_stats_: | 17 (+3) | 20 (+5) | 15 (+2) | 9 (-1) | 14 (+2) | 8 (-1) |

___Arboreal Hunter.___ The greenskin orc has advantage on Dexterity
(Stealth) checks when in temperate or warm forests.

___Favored Enemy.___ The elfhunter has advantage on Wisdom (Perception)
and Wisdom (Survival) checks to track or notice any variety of elf or fey
creature.

___Forest Hunter.___ When the elfhunter hits a creature with a weapon attack,
the creature takes an extra 3 (1d6) damage (included in the weapon damage).

___Land Stride.___ The elfhunter can move through nonmagical difficult
terrain without using extra movement and can pass through nonmagical
plants without being slowed by them and without taking damage from
them even if they have thorns, spines, or a similar hazard.

___Spellcasting.___ The elfhunter is an 8th level spellcaster. Its spellcasting
ability is Wisdom (spell save DC 13, +5 to hit with spell attacks). It has
the following ranger spells prepared:

* 1st level (4 slots): _cure wounds, detect magic, jump, longstrider_

* 2nd level (3 slots): _find traps, pass without trace, silence_

**Actions**

___Multiattack.___ The green orc elfhunter makes two attacks with its
longsword or two with its longbow.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit:
11 (1d8 + 1d6 + 3) slashing damage or 12 (1d10 + 1d6 + 3) slashing damage when used
with two hands.

___Longbow.___ Ranged Weapon Attack: +8 to hit, range 150/600 ft., one
target. Hit: 13 (1d8 +1d6 + 5) piercing damage.
