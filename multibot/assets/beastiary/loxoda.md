"Loxoda";;;_size_: Huge monstrosity
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "Loxodan"
_skills_: "Survival +5"
_senses_: ", passive Perception 12"
_speed_: "40 ft."
_hit points_: "147 (14d12 + 56)"
_armor class_: "13 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 19 (+4) | 12 (+1) | 14 (+2) | 10 (+0) |

___Trampling Charge.___ If the loxoda moves at least 20 feet straight toward a Large or smaller creature it then attacks with a stomp, the stomp attack is made with advantage. If the stomp attack hits, the target must also succeed on a DC 15 Strength saving throw or be knocked prone.

**Actions**

___Multiattack.___ The loxoda makes two attacks, but no more than one of them can be a maul or javelin attack.

___Maul.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 25 (6d6 + 4) bludgeoning damage.

___Stomp.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 20 (3d10 + 4) bludgeoning damage.

___Javelin.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 14 (3d6 + 4) piercing damage.

