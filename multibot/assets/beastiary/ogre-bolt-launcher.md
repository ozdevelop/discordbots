"Ogre Bolt Launcher";;;_page_number_: 220
_size_: Large giant
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft., passive Perception 8"
_speed_: "40 ft."
_hit points_: "59  (7d10 + 21)"
_armor class_: "13 (hide armor)"
_stats_: | 19 (+4) | 12 (+1) | 16 (+3) | 5 (-2) | 7 (-1) | 7 (-1) |

**Actions**

___Fist___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d4 + 4) bludgeoning damage.

___Bolt Launcher___ Ranged Weapon Attack: +3 to hit, range 120/480 ft., one target. Hit: 17 (3d10 + 1) piercing damage.