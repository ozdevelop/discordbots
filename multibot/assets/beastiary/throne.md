"Throne";;;_size_: Large celestial
_alignment_: any good alignment
_challenge_: "9 (5,000 XP)"
_languages_: "all, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 22"
_saving_throws_: "Str +7, Con +7"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: " charmed, exhaustion, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "127 (15d10 + 45)"
_armor class_: "15 (natural armor)"
_stats_: | 16 (+3) | 16 (+3) | 16 (+3) | 14 (+2) | 18 (+4) | 18 (+4) |

___Magic Resistance.___ The Throne has advantage on
saving throws against spells and other magical
effects from evil characters and sources.

___Aura of Protection Against Evil.___ Evil creatures
have disadvantage on attack rolls against all
allies within 5 feet of the Throne. Allies in this
area can’t be charmed, frightened, or possessed
by evil creatures. If an ally is already charmed,
frightened, or possessed by evil magic, the ally
has advantage on any new saving throw against
the relevant effect.

**Actions**

___The Burning Wheel.___ Ranged Spell Attack: +8
to hit, range 120 ft., one target. Hit: 10 (3d6) fire
damage plus 9 (2d8) radiant damage. If the
target is Medium or smaller, it must succeed on
a DC 16 Dexterity saving throw or be teleported
into the Throne’s burning rings. The target is
grappled (escape DC 17). Until the grapple ends,
the target is restrained, and the Throne uses
Radiant Burn as its first action on each of its subsequent turns. Only one creature can be imprisoned at a time.

___Radiant Burn.___ The wheel spins around the creature grappled by it, dealing 13 (3d8) fire damage
plus 9 (2d8) radiant damage.
