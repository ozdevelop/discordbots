"Runebound Orc";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Orc"
_senses_: "darkvision 60 ft., passive Perception 9"
_condition_immunities_: "charmed"
_speed_: "30 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "14 (hide armor)"
_stats_: | 17 (+3) | 14 (+2) | 16 (+3) | 8 (-1) | 8 (-1) | 6 (-2) |

___Aggressive.___ As a bonus action, the orc can move up
to its speed toward a hostile creature that it can
see.

___Runebound.___ The orc's body is coated in magical
runes granted by a runespeaker that provide it with
additional strength. The orc gains one of the
following runic bonuses:
* Rune of Brutality – When the orc hits a creature
with less than its maximum hit points, the attack
deals an additional 5 (1d10) slashing damage.
* Rune of Fortitude – The orc's AC is increased to
16. In addition, as long as the orc has at least 1
hit point, it regenerates 5 hit points at the start
of its turn.
* Rune of Strength – The orc's greataxe attacks
deal an additional 3 slashing damage and it has
advantage on Strength-based checks and saving
throws.

___Runic Weapon.___ The orc's greataxe is imbued with
powerful arcane runes, causing attacks with that
weapon to deal an additional 4 (1d8) force damage
(included in the attack).

**Actions**

___Runic Greataxe.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one target. Hit: 9 (1d12 + 3) slashing
damage plus 4 (1d8) force damage.
