"Spitting Sea Serpent";;;_size_: Large dragon
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "understands Aquan but seldom speaks"
_skills_: "Athletics +8, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "acid, poison"
_condition_immunities_: "poisoned, prone"
_speed_: "10 ft., swim 40 ft."
_hit points_: "97 (13d10 + 26)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 13 (+1) | 14 (+2) | 8 (-1) | 13 (+1) | 6 (-2) |

___Amphibious.___ The spitting sea serpent can breathe air and water.

**Actions**

___Multiattack.___ A spitting sea serpent spits once and bits once, or it can
make a constrict attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 14
(2d8 + 5) piercing damage plus 10 (3d6) poison damage.

___Constrict.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature.
Hit: 23 (4d8 + 5) bludgeoning damage and the target is grappled if it is
Medium-sized or smaller (escape DC 16). At the beginning of each of
the serpent’s turn, the grappled creature takes 23 (4d8 + 5) bludgeoning
damage.

___Spit.___ Ranged Weapon Attack: +8 to hit, range 60 ft., one target. Hit: 15
(3d6 + 5) acid damage. This attack can be used underwater.
