"Hutijin";;;_page_number_: 175
_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "21 (33,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 21"
_skills_: "Intimidation +14, Perception +11"
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +9, Con +14, Wis +11"
_speed_: "30 ft., fly 60 ft."
_hit points_: "200  (16d10 + 112)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 27 (+8) | 15 (+2) | 25 (+7) | 23 (+6) | 19 (+4) | 25 (+7) |

___Infernal Despair.___ Each creature within 15 feet of Hutijin that isn't a devil makes saving throws with disadvantage.

___Innate Spellcasting.___ Hutijin's innate spellcasting ability is Charisma (spell save DC 22). He can innately cast the following spells, requiring no material components:

* At will: _alter self _(can become Medium when changing his appearance)_, animate dead, detect magic, hold monster, invisibility _(self only)_, lightning bolt, suggestion, wall of fire_

* 3/day: _dispel magic_

* 1/day each: _heal, symbol _(hopelessness only)

___Legendary Resistance (3/Day).___ If Hutijin fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Hutijin has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Hutijin's weapon attacks are magical.

___Regeneration.___ Hutijin regains 20 hit points at the start of his turn. If he takes radiant damage, this trait doesn't function at the start of his next turn. Hutijin dies only if he starts his turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack___ Hutijin makes four attacks: one with his bite, one with his claw, one with his mace, and one with his tail.

___Bite___ Melee Weapon Attack: +15 to hit, reach 5 ft., one target. Hit: 15 (2d6 + 8) piercing damage. The target must succeed on a DC 22 Constitution saving throw or become poisoned. While poisoned in this way, the target can't regain hit points, and it takes 10 (3d6) poison damage at the start of each of its turns. The poisoned target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Claw___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target. Hit: 17 (2d8 + 8) slashing damage.

___Mace___ Melee Weapon Attack: +15 to hit, reach 5 ft., one target. Hit: 15 (2d6 + 8) bludgeoning damage.

___Tail___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target. Hit: 19 (2d10 + 8) bludgeoning damage.

___Teleport___ Hutijin magically teleports, along with any equipment he is wearing and carrying, up to 120 feet to an unoccupied space he can see.

**Legendary** Actions

Hutijin can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Hutijin regains spent legendary actions at the start of his turn.

___Attack___ Hutijin attacks once with his mace.

___Lightning Storm (Costs 2 Actions)___ Hutijin releases lightning in a 20-foot radius. All other creatures in that area must each make a DC 22 Dexterity saving throw, taking 18 (4d8) lightning damage on a failed save, or half as much damage on a successful one.

___Teleport___ Hutijin uses his Teleport action.
