"Demonblood Orc Hunter";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Orc"
_skills_: "Animal Handling +4, Survival +4, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "30 ft."
_hit points_: "60 (8d10 + 16)"
_armor class_: "15 (studded leather)"
_stats_: | 13 (+1) | 16 (+3) | 14 (+2) | 8 (-1) | 14 (+2) | 8 (-1) |

___Aggressive.___ As a bonus action, the orc can move up
to its speed toward a hostile creature that it can
see.

___Demon’s Aim.___ As a bonus action, the orc can
choose to tap into its demonic blood to make its
next shot. The orc’s next ranged attack takes a -3
penalty to the attack roll. If the attack hits, add +6
to the attack’s damage.

**Actions**

___Multiattack.___ The orc makes two attacks with its
longbow or with its shortswords.

___Longbow.___ Melee Weapon Attack: +7 to hit, range
150/600 ft., one target. Hit: 7 (1d8 + 3) piercing
damage plus 2 (1d4) necrotic damage.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Volley (Recharge 5-6).___ The orc makes a ranged
attack against any number of creatures within 10
feet of a point it can see within its weapon’s range.
It makes a separate attack roll for each target.
