"Core Spawn Emissary";;;_size_: Medium aberration
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: " understands Deep Speech but can't speak, telepathy 120 ft."
_skills_: "Perception +4"
_saving_throws_: "Dex +5, Wis +4, Cha +2"
_senses_: "blindsight 30 ft., tremorsense 60 ft., passive Perception 14"
_damage_immunities_: "psychic"
_condition_immunities_: "blinded"
_speed_: "40 ft., fly 60 ft. (hover)"
_hit points_: "102 (12d8 + 48)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 18 (+4) | 8 (-1) | 13 (+1) | 8 (-1) |

___Magic Resistance.___ The emissary has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The emissary makes three talons attacks.

___Talons.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 14 (2d10 + 3) slashing damage.

___Alluring Thrum (Recharges 5—6).___ The emissary emits a dreadful yet alluring hum. Each creature within 20 feet of the emissary that can hear it and that isn’t an aberration must succeed on a DC 14 Constitution saving throw or be charmed for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Crystal Spores (Recharge 6).___ A 15-foot-radius cloud of toxic crystalline spores extends out from the emissary. The spores spread around corners. Each creature in the area must succeed on a DC 14 Constitution saving throw or become poisoned. While poisoned in this way, a creature takes 11 (2d10) poison damage at the start of each of its turns. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
