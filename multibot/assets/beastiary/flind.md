"Flind";;;_size_: Medium humanoid (gnoll)
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "Abyssal, Gnoll"
_senses_: "darkvision 60 ft."
_skills_: "Intimidation +5, Perception +5"
_saving_throws_: "Con +8, Wis +5"
_speed_: "30 ft."
_hit points_: "127 (15d8+60)"
_armor class_: "16 (chain mail)"
_stats_: | 20 (+5) | 10 (0) | 19 (+4) | 11 (0) | 13 (+1) | 12 (+1) |

___Aura of Blood Thirst.___ If the flind isn't incapacitated, any creature with the Rampage trait can make a bite attack as a bonus action while within 10 feet of the flind.

**Actions**

___Multiattack.___ The flind makes three attacks: one with each of its different flail attacks or three with its longbow.

___Flail of Madness.___ Melee Weapon Attack: +9 to hit, reach 5 ft, one target. Hit: 10 (1d10+5) bludgeoning damage, and the target must make a DC 16 Wisdom saving throw. On a failed save, the target must make a melee attack against a random target within its reach on its next turn. If it has no targets within its reach even after moving, it loses its action on that turn.

___Flail of Pain.___ Melee Weapon Attack: +9 to hit, reach 5 it, one target. Hit: 10 (1d10+5) bludgeoning damage plus 22 (4d10) psychic damage.

___Flail of Paralysis.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 10 (1d10+5) bludgeoning damage, and the target must succeed on a DC 16 Constitution saving throw or be paralyzed until the end of its next turn.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one target. Hit: 4 (1d8) piercing damage.