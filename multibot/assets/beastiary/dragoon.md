"Dragoon";;;_size_: Medium humanoid
_alignment_: lawful good
_challenge_: "3 (700 XP)"
_languages_: "Common and the languages of the race of the dragoon"
_skills_: "Athletics +5, Acrobatics +4, Performance +2"
_senses_: "passive Perception 10"
_speed_: "30 ft."
_hit points_: "60 (8d10 + 16)"
_armor class_: "15 (chain shirt)"
_stats_: | 16 (+3) | 14 (+2) | 14 (+2) | 12 (+1) | 10 (+0) | 10 (+0) |

**Actions**

___Multiattack.___ The dragoon makes two attacks with its
halberd, or uses its skystrike and attacks once with
its halberd.

___Halberd.___ Melee Weapon Attack: +5 to hit, reach 10
ft., one target. Hit: 8 (1d10 + 3) slashing damage.

___Skystrike (3/Day).___ As the dragoon returns to the
ground after using its Unnatural Athletics, it
performs a special weapon attack using the pointed
end of its halberd against an enemy within 10 ft. of
the space it landed. On a hit, the target takes takes
15 (1d10 + 2d6 + 3) piercing damage and must
succeed on a DC 13 Strength saving throw or be
knocked prone.

**Bonus** Actions

___Unnatural Athletics.___ The Dragoon
expends half of its total movement to leap 20 feet
vertically and move up to 10 feet horizontally
before returning to the ground in an unoccupied
space. If the dragoon moves away from an enemy
when using this feature, attacks of opportunity
against it are made with disadvantage.
