"Sand Giant";;;_size_: Huge giant
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Giant"
_skills_: "Perception +5"
_senses_: "passive Perception 15"
_saving_throws_: "Con +10, Wis +5"
_speed_: "40 ft."
_hit points_: "262 (21d12 + 126)"
_armor class_: "16 (natural armor)"
_stats_: | 28 (+9) | 13 (+1) | 22 (+6) | 12 (+1) | 12 (+1) | 14 (+2) |

___Innate Spellcasting.___ The giant’s innate spellcasting ability is Charisma
(spell save DC 14, +6 to hit with spell attacks). It can innately cast the
following spells, requiring no material components:

* 2/day: _move earth_

* 1/day: _earthquake_

**Actions**

___Multiattack.___ The sand giant makes two attacks with its greatsword.

___Greataxe.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 26 (5d6 + 9) slashing damage.

___Fist of Sand (1/day).___ The giant slams its fist violently into the ground.
All creatures within 10 feet of the giant must make a DC 16 Dexterity
saving throw or be knocked prone and take 14 (4d6) bludgeoning damage
on a failure, or half as much damage without being knocked prone on a
success.
