"Tigerpede";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Intimidation +0, Perception +3"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 13"
_speed_: "30 ft., climb 20 ft."
_hit points_: "75 (8d10 + 8)"
_armor class_: "12 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 15 (+2) | 3 (-4) | 12 (+1) | 6 (-2) |

___Aggressive Charge.___ On its first turn in combat, before
moving or taking any actions, the tigerpede can
spend its entire movement to move up to twice its
speed toward a hostile creature it can see.

___Keen Sight and Smell.___ The tigerpede has advantage on
Wisdom (Perception) checks that rely on sight or
smell.

___Sure-Footed.___ The tigerpede has advantage on
Strength and Dexterity saving throws made against
effects that would knock it prone.

___Tackle.___ If the tigerpede tiger moves at least 20 feet
straight toward a target and then hits it with a gore
attack on the same turn, the target takes an extra 5
(2d4) slashing damage. If the target is prone, the
tigerpede can make a bite attack against it as a bonus
action.

**Actions**

___Multiattack.___ The tigerpede makes two attacks: one
with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one
target. Hit: 11 (2d6 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 9 (2d4 + 4) slashing damage.

___Gore.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 9 (2d4 + 4) slashing damage, and if
the target is a creature, it must succeed on a DC 14
Strength saving throw or be knocked prone.
