"Spined Devil";;;_size_: Small fiend (devil)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_damage_immunities_: "fire, poison"
_speed_: "20 ft., fly 40 ft."
_hit points_: "22 (5d6+5)"
_armor class_: "13 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 10 (0) | 15 (+2) | 12 (+1) | 11 (0) | 14 (+2) | 8 (-1) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Flyby.___ The devil doesn't provoke an opportunity attack when it flies out of an enemy's reach.

___Limited Spines.___ The devil has twelve tail spines. Used spines regrow by the time the devil finishes a long rest.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The devil makes two attacks: one with its bite and one with its fork or two with its tail spines.

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 5 (2d4) slashing damage.

___Fork.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 3 (1d6) piercing damage.

___Tail Spine.___ Ranged Weapon Attack: +4 to hit, range 20/80 ft ., one target. Hit: 4 (1d4 + 2) piercing damage plus 3 (1d6) fire damage.