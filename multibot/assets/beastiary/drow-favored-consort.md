"Drow Favored Consort";;;_page_number_: 183
_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "18 (20,000 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 18"
_skills_: "Acrobatics +11, Athletics +8, Perception +8, Stealth +11"
_saving_throws_: "Dex +11, Con +9, Cha +10"
_speed_: "30 ft."
_hit points_: "225  (30d8 + 90)"
_armor class_: "15 (18 with mage armor)"
_stats_: | 15 (+2) | 20 (+5) | 16 (+3) | 18 (+4) | 15 (+2) | 18 (+4) |

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's innate spellcasting ability is Charisma (spell save DC 18). It can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire, levitate _(self only)

___Spellcasting.___ The drow is a 11th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 18, +10 to hit with spell attacks). It has the following wizard spells prepared:

* Cantrips (at will): _mage hand, message, poison spray, shocking grasp, ray of frost_

* 1st level (4 slots): _burning hands, mage armor, magic missile, shield_

* 2nd level (3 slots): _gust of wind, invisibility, misty step, shatter_

* 3rd level (3 slots): _counterspell, fireball, haste_

* 4th level (3 slots): _dimension door, Otiluke's resilient sphere_

* 5th level (2 slots): _cone of cold_

* 6th level (1 slot): _chain lightning_

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___War Magic.___ When the drow uses its action to cast a spell, it can make one weapon attack as a bonus action.

**Actions**

___Multiattack___ The drow makes three scimitar attacks.

___Scimitar___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) slashing damage plus 18 (4d8) poison damage. In addition, the target has disadvantage on the next saving throw it makes against a spell the drow casts before the end of the drow's next turn.

___Hand Crossbow___ Ranged Weapon Attack: +11 to hit, range 30/120 ft., one target. Hit: 8 (1d6 + 5) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour. If the saving throw fails by 5 or more, the target is also unconscious while poisoned in this way. The target regains consciousness if it takes damage or if another creature takes an action to shake it.
