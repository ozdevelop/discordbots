"Millitaur";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Common"
_skills_: "Acrobatics +4"
_senses_: "darkvision 60 ft., tremorsense 30 ft., passive Perception 11"
_damage_resistances_: "poison; bludgeoning and slashing from nonmagical weapons"
_condition_immunities_: "prone"
_speed_: "40 ft., burrow 20 ft., climb 30 ft."
_hit points_: "85 (10d10 + 30)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 16 (+3) | 8 (-1) | 12 (+1) | 10 (+0) |

**Actions**

___Multiattack.___ The millitaur makes two handaxe attacks.

___Handaxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 2 (1d4) poison damage.

