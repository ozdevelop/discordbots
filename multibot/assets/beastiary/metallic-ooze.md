"Metallic Ooze";;;_size_: Large ooze
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_skills_: "Stealth +3"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 8"
_damage_immunities_: "acid"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_speed_: "20 ft., climb 10 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "9 (natural armor)"
_stats_: | 17 (+3) | 5 (-3) | 19 (+4) | 2 (-4) | 6 (-2) | 1 (-5) |

___False Appearance.___ The metallic ooze is indistinguishable from a pile of
coins as long as it remains motionless.

___Irritating Fumes.___ If a metallic ooze takes fire damage, it emanates a
cloud of semi-transparent vapor that irritates the eyes and respiratory
system of living creatures within 10 feet of it. All creatures other
than undead or constructs within the area must succeed on a DC
15 Constitution saving throw or take 10 (3d6) acid damage and be
poisoned for 1 minute. A poisoned creature can repeat the saving
throw at the end of each of its turns, ending the effect on a success.
The cloud disperses in 1 minute, or can be dispersed by a moderate
or greater wind.

___Spider Climb.___ The metallic ooze can climb difficult surfaces, including
upside down on ceilings, without needing to make an ability check.

**Actions**

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12
(2d8 + 3) bludgeoning damage plus 18 (4d8) acid damage, and the target
must succeed on a DC 15 Constitution saving throw or be poisoned for
1 hour.
