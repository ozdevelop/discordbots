"Cloaker";;;_size_: Large aberration
_alignment_: chaotic neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Deep Speech, Undercommon"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +5"
_speed_: "10 ft., fly 40 ft."
_hit points_: "78 (12d10+12)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 12 (+1) | 13 (+1) | 12 (+1) | 14 (+2) |

___Damage Transfer.___ While attached to a creature, the cloaker takes only half the damage dealt to it (rounded down). and that creature takes the other half.

___False Appearance.___ While the cloaker remains motionless without its underside exposed, it is indistinguishable from a dark leather cloak.

___Light Sensitivity.___ While in bright light, the cloaker has disadvantage on attack rolls and Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The cloaker makes two attacks: one with its bite and one with its tail.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 10 (2d6 + 3) piercing damage, and if the target is Large or smaller, the cloaker attaches to it. If the cloaker has advantage against the target, the cloaker attaches to the target's head, and the target is blinded and unable to breathe while the cloaker is attached. While attached, the cloaker can make this attack only against the target and has advantage on the attack roll. The cloaker can detach itself by spending 5 feet of its movement. A creature, including the target, can take its action to detach the cloaker by succeeding on a DC 16 Strength check.

___Tail.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one creature. Hit: 7 (1d8 + 3) slashing damage.

___Moan.___ Each creature within 60 feet of the cloaker that can hear its moan and that isn't an aberration must succeed on a DC 13 Wisdom saving throw or become frightened until the end of the cloaker's next turn. If a creature's saving throw is successful, the creature is immune to the cloaker's moan for the next 24 hours.

___Phantasms (Recharges after a Short or Long Rest).___ The cloaker magically creates three illusory duplicates of itself if it isn't in bright light. The duplicates move with it and mimic its actions, shifting position so as to make it impossible to track which cloaker is the real one. If the cloaker is ever in an area of bright light, the duplicates disappear.

Whenever any creature targets the cloaker with an attack or a harmful spell while a duplicate remains, that creature rolls randomly to determine whether it targets the cloaker or one of the duplicates. A creature is unaffected by this magical effect if it can't see or if it relies on senses other than sight.

A duplicate has the cloaker's AC and uses its saving throws. If an attack hits a duplicate, or if a duplicate fails a saving throw against an effect that deals damage, the duplicate disappears.