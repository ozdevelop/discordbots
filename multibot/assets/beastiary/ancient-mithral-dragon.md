"Ancient Mithral Dragon";;;_size_: Gargantuan dragon
_alignment_: neutral
_challenge_: "18 (20000 XP)"
_languages_: "Celestial, Common, Draconic, Primordial"
_skills_: "Athletics +15, History +13, Insight +13, Intimidation +13, Perception +13, Persuasion +13"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 23"
_saving_throws_: "Dex +9, Con +13, Int +13, Wis +13, Cha +13"
_damage_immunities_: "acid, thunder"
_damage_resistances_: "bludgeoning, piercing, and slashing from"
_condition_immunities_: "charmed"
_speed_: "40 ft., fly 80 ft."
_hit points_: "297 (17d20 + 119)"
_armor class_: "20 (natural armor)"
_stats_: | 29 (+9) | 16 (+3) | 25 (+7) | 24 (+7) | 25 (+7) | 24 (+7) |

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

___Innate Spellcasting.___ The dragon's innate spellcasting ability is Intelligence (spell save DC 21). It can innately cast the following spells, requiring no material components:

At will: tongues

5/day each: counterspell, dispel magic, enhance ability

___Mithral Shards.___ Ancient mithral dragons can choose to retain the mithral shards of their breath weapon as a hazardous zone of spikes. Treat as a spike growth zone that does 2d8 magical slashing damage for every 5 feet travelled.

___Spellcasting.___ The dragon is a 15th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 21, +13 to hit with spell attacks). It requires no material components to cast its spells. The dragon has the following wizard spells prepared:

Cantrips (at will): acid splash, light, mage hand, minor illusion, prestidigitation

1st level (4 slots): charm person, expeditious retreat, magic missile, unseen servant

2nd level (3 slots): blur, hold person, see invisibility

3rd level (3 slots): haste, lightning bolt, protection from energy

4th level (3 slots): dimension door, stoneskin, wall of fire

5th level (2 slots): polymorph, teleportation circle

6th level (1 slot): guards and wards

7th level (1 slot): forcecage

8th level (1 slot): antimagic field

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one target. Hit: 22 (2d12 + 9) piercing damage.

___Claw.___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target. Hit: 18 (2d8 +9) slashing damage, and the target loses 5 hit point from bleeding at the start of each of its turns for six rounds unless it receives magical healing. Bleeding damage is cumulative; the target loses 5 hp per round for each bleeding wound it's taken from a mithral dragon's claws.

___Tail.___ Melee Weapon Attack: +15 to hit, reach 20 ft., one target. Hit: 20 (2d10 + 9) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 21 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of its turn, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours.

___Breath Weapon (Recharge 5-6).___ A mithral dragon can spit a 60-foot-long, 5-foot-wide line of metallic shards. Targets in its path take 59 (17d6) magical slashing damage and lose another 10 hit points from bleeding at the start of their turns for 6 rounds; slashing and bleed damage is halved by a successful DC 21 Dexterity saving throw. Only magical healing stops the bleeding before 6 rounds. The shards dissolve into wisps of smoke 1 round after the breath weapon's use.

**Legendary** Actions

___The dragon can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each creature within 10 feet of the dragon must succeed on a DC 23 Dexterity saving throw or take 18 (2d8 + 9) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.

