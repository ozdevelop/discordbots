"Wind Drake";;;_size_: Medium dragon
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "20 ft., fly 40 ft."
_hit points_: "45 (6d10 + 12)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 15 (+2) | 11 (+0) | 12 (+1) | 7 (-2) |

___Wind Barrier.___ The drake has half-cover against all
ranged attacks due to the powerful winds that
constantly flow around it.

**Actions**

___Multiattack.___ The drake makes two attacks: one with
its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5ft.,
one target. Hit: 11 (2d6 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5ft.,
one target. Hit: 7 (1d6 + 4) piercing damage.

___Imprisoning Winds (Recharge 6).___ The drake conjures
up a storm of powerful winds around a creature
within 60 ft. The target must succeed on a DC 11
Strength saving throw or takes 10 (3d6)
bludgeoning damage become grappled (escape DC
11) for 1 minute by these winds. Pass or fail, any
open flames being held by the target are
immediately extinguished.
