"Temple Dog";;;_size_: Medium celestial
_alignment_: good
_challenge_: "5 (1800 XP)"
_languages_: "understands Celestial and Common but can't speak"
_skills_: "Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Str +7, Con +5, Int +2, Wis +5"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft."
_hit points_: "97 (15d8 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 8 (-1) | 14 (+2) | 10 (+0) |

___Magic Resistance.___ The temple dog has advantage on saving throws against spells and other magical effects.

___Protector's Initiative.___ If the temple dog is entering combat against a clear threat to its temple, it has advantage on its initiative roll.

___Rushing Slam.___ If the temple dog moves at least 10 feet straight toward a target and then makes a slam attack against that target, it can make an additional slam attack against a second creature within 5 feet of the first target as a bonus action.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 17 (3d8 + 4) piercing damage plus 9 (2d4 + 4) bludgeoning damage, and the target is grappled (escape DC 14). The target must also make a successful DC 15 Constitution saving throw or be stunned until the end of its next turn.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (3d6 + 4) bludgeoning damage, and the target must succeed on a DC 15 Strength saving throw or be knocked prone and pushed 5 feet. The temple dog can immediately enter the position the target was pushed out of, if it chooses to.

