"Greater Aspect of Sorrow";;;_size_: Large construct
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 120ft., passive Perception 10"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 18 (+4) | 6 (-2) | 10 (+0) | 1 (-5) |

___Aura of Silence.___ Living creatures within 30 feet of
the aspect are under the effects of the silence spell.
This effect ends when the aspect is reduced to 0
hit points.

___Immutable Form.___ The aspect is immune to any spell
or effect that would alter its form.

___Magic Resistance.___ The aspect has advantage on
saving throws against spell and other magical
effects.

___Magic Weapons.___ The aspect's weapon attacks are
magical.

**Actions**

___Multiattack.___ The aspect makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5ft.,
one target. Hit: 10 (2d6 + 3) bludgeoning damage.
If the target is a creature, it must succeed on a DC
14 Wisdom saving throw or become frightened
until the end of its next turn.

___Wave of Dread (1/Day).___ Each hostile creature within
30 feet of the aspect must succeed on a DC 14
Wisdom saving throw or become frightened for
one minute. A creature can repeat the saving throw
at the end of each of its turns, ending the effect on
itself on a success.
