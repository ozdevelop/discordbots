"Warlock of the Great Old One";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "6 (2,300 XP)"
_languages_: "any two languages, telepathy 30 ft."
_senses_: "darkvision 60 ft."
_skills_: "Arcana +4, History +4"
_saving_throws_: "Wis +4, Cha +7"
_speed_: "30 ft."
_hit points_: "91 (14d8+28)"
_armor class_: "12 (15 with mage armor)"
_damage_resistances_: "psychic"
_stats_: | 9 (-1) | 14 (+2) | 15 (+2) | 12 (+1) | 12 (+1) | 18 (+4) |

___Innate Spellcasting.___ The warlock's innate spellcasting ability is Charisma. It can innately cast the following spells (spell save DC 15), requiring no material components:

* At will: _detect magic, jump, levitate, mage armor _(self only)_, speak with dead_

* 1/day each: _arcane gate, true seeing_

___Spellcasting.___ The warlock is a 14th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _chill touch, eldritch blast, guidance, mage hand, minor illusion, prestidigitation, shocking grasp_

* 1st-5th level (3 5th-level slots): _armor of Agathys, arms of Hadar, crown of madness, clairvoyance, contact other plane, detect thoughts, dimension door, dissonant whispers, dominate beast, telekinesis, vampiric touch_

___Whispering Aura.___ At the start of each of the warlock's turns, each creature of its choice within 5 feet of it must succeed on a DC 15 Wisdom saving throw or take 10 (3d6) psychic damage, provided that the warlock isn't incapacitated.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4+2) piercing damage.
