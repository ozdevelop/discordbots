"Dragonbait";;;_size_: Medium humanoid (saurial)
_alignment_: lawful good
_challenge_: "5 (1,800 XP)"
_Languages_: "understands Common but can't speak"
_senses_: "passive Perception 13"
_skills_: "Athletics +5, Medicine +6"
_Saving_throws_: "Wis +6, Cha +7"
_speed_: "30 ft."
_hit points_: "120 (16d8 +48)"
_armor class_: "17 (breastplate, shield)"
_stats_: | 15 (+2) | 13 (+1) | 17 (+3) | 14 (+2) | 16 (+3) | 18 (+4) |

___Divine Health.___ Dragonbait is immune to disease.

___Magic Resistan ce Aura.___ While holding his holy avenger,
Dragonbait creates an aura in a 10-foot radius around him. While this aura is active , Dragonbait and all creatures friendly to him in the aura have advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ Dragonbait makes two melee weapon attacks.

___Holy Avenger (+3 Longsword).___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 9 (ld8 +5) slashing damage, or 10 (ldlO +5) slashing damage when used with two hands. If the target is a fiend or an undead, it takes an extra 11 (2d10) radiant damage.

___Sense Alignment.___ Dragonbait chooses one creature he can see within 60 feet of him and determines its alignment, as long as the creature isn't hidden from divination magic by a spell or ot her magical effect.
