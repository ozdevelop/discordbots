"Arcane Wyrm";;;_size_: Tiny elemental
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "understands Primordial but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_condition_immunities_: "prone"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "14 (4d6)"
_armor class_: "12"
_stats_: | 5 (-3) | 14 (+2) | 10 (+0) | 15 (+2) | 10 (+0) | 7 (-2) |

___Arcane Flicker.___ Instead of flying, the wyrm can
teleport between locations by expending that
much movement.

___Translucent Body.___ The wyrm has advantage on
Stealth checks.

**Actions**

___Arcane Volley.___ The wyrm unleashes 2 jolts of arcane
energy at one or two targets within 120 feet. These
jolts automatically hit and deal 3 (1d4 + 1) force
damage.
