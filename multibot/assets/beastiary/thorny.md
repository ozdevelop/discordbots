"Thorny";;;_size_: Medium plant
_alignment_: neutral
_challenge_: "1 (200 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4, Stealth +3"
_speed_: "30 ft."
_hit points_: "27 (5d8+5)"
_armor class_: "14 (natural armor)"
_damage_resistances_: "lightning, piercing"
_stats_: | 13 (+1) | 12 (+1) | 13 (+1) | 2 (-4) | 10 (0) | 6 (-2) |

___Plant Camouflage .___ The thorny has advantage on Dexterity (Stealth) checks it makes in any terrain with ample obscuring plant life.

___Regeneration.___ The thorny regains 5 hit points at the start of its turn. If it takes cold, fire, or necrotic damage, this trait doesn't function at the start of the thorny's next turn. The thorny dies only if it starts its turn with 0 hit points and doesn't regenerate.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 8 (2d6+1) piercing damage.