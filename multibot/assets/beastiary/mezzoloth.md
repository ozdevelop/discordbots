"Mezzoloth";;;_size_: Medium fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal, Infernal, telepathy 60 ft."
_senses_: "blindsight 60 ft., darkvision 60 ft."
_skills_: "Perception +3"
_damage_immunities_: "acid, poison"
_speed_: "40 ft."
_hit points_: "75 (10d8+30)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 11 (0) | 16 (+3) | 7 (-2) | 10 (0) | 11 (0) |

___Innate Spellcasting.___ The mezzoloth's innate spellcasting ability is Charisma (spell save DC 11). The mezzoloth can innately cast the following spells, requiring no material components:

* 2/day each: _darkness, dispel magic_

* 1/day: _cloudkill_

___Magic Resistance.___ The mezzoloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The mezzoloth's weapon attacks are magical.

**Actions**

___Multiattack.___ The mezzoloth makes two attacks: one with its claws and one with its trident.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 9 (2d4 + 4) slashing damage.

___Trident.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage, or 8 (1d8 + 4) piercing damage when held with two claws and used to make a melee attack.

___Teleport.___ The mezzoloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.

___Variant: Summon Yugoloth (1/Day).___ The yugoloth attempts a magical summoning.

A mezzoloth has a 30 percent chance of summoning one mezzoloth.

A summoned yugoloth appears in an unoccupied space within 60 feet of its summoner, does as it pleases, and can't summon other yugoloths. The summoned yugoloth remains for 1 minute, until it or its summoner dies, or until its summoner takes a bonus action to dismiss it.
