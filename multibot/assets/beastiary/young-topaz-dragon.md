"Young Topaz Dragon";;;_size_: Large dragon
_alignment_: neutral good
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +5, Insight +4, Perception +4, Religion +5"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 17"
_saving_throws_: "Dex +5, Int +5, Wis +4, Cha +5"
_damage_vulnerabilites_: "psychic"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "71 (13d10)"
_armor class_: "17 (natural armor)"
_stats_: | 15 (+2) | 15 (+2) | 11 (+0) | 15 (+2) | 12 (+1) | 15 (+2) |

___Uplift Aura.___ All allies within 30 feet gain +2 on
Intelligence checks and saving throws.

**Psionics**

___Charges:___ 13 | ___Recharge:___ 1d6 | ___Fracture:___ 10

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 10 ft.,
one target. Hit: 13 (2d10 + 2) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 9 (2d6 + 2) slashing damage.
