"Gilded Sea Serpent";;;_size_: Medium dragon
_alignment_: neutral
_challenge_: "4 (1,100 XP)"
_languages_: "Draconic"
_skills_: "Athletics +5, Perception +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned, prone"
_speed_: "0 ft., swim 30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 14 (+2) | 5 (-3) | 11 (+0) | 7 (-2) |

___Amphibious.___ The gilded serpent can breathe air and water.

___Extract Poison.___ The poison of the gilded serpent can be milked or harvested as well,
and then made into a powerful narcotic drug called golden bliss:
* ___Golden Bliss (Inhaled).___ A creature subjected to this poison must
succeed on a DC 13 Constitution saving throw or be poisoned for 1
minute. While poisoned, the target is stunned and has advantage on
Intelligence, Wisdom, and Charisma saving throws against spells
and magical effects. The poisoned target can repeat the saving
throw at the end of each of its turns, ending the effect on a success.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 14
(2d10 + 3) piercing damage and if the target is a creature, it must succeed
at a DC 14 Constitution saving throw. On a failed saving throw, the target
is poisoned for 1 minute. While poisoned, the target is also stunned. A
poisoned creature can repeat the saving throw at the end of each of its
turns, ending the effect on a success.

___Constrict.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit:
12 (2d8 + 3) bludgeoning damage and the target is grappled if it is Mediumsized
or smaller (escape DC 13). At the beginning of each of the serpent’s
turn, the grappled creature takes 12 (2d8 + 3) bludgeoning damage.
