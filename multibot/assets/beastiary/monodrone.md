"Monodrone";;;_size_: Medium construct
_alignment_: construct
_challenge_: "1/8 (25 XP)"
_languages_: "Modron"
_senses_: "truesight 120 ft."
_speed_: "30 ft., fly 30 ft."
_hit points_: "5 (1d8+1)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (0) | 13 (+1) | 12 (+1) | 4 (-3) | 10 (0) | 5 (-3) |

___Axiomatic Mind.___ The monodrone can't be compelled to act in a manner contrary to its nature or its instructions.

___Disintegration.___ If the monodrone dies, its body disintegrates into dust, leaving behind its weapons and anything else it was carrying.

**Actions**

___Dagger.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage.

___Javelin.___ Melee or Ranged Weapon Attack: +2 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 4 (1d6) piercing damage.