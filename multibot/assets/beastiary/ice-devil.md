"Ice Devil";;;_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "14 (11,500 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "blindsight 60 ft., darkvision 120 ft."
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +7, Con +9, Wis +7, Cha +9"
_speed_: "40 ft."
_hit points_: "180 (19d10+76)"
_armor class_: "18 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 21 (+5) | 14 (+2) | 18 (+4) | 18 (+4) | 15 (+2) | 18 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The devil makes three attacks: one with its bite, one with its claws, and one with its tail.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) piercing damage plus 10 (3d6) cold damage.

___Claws.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 10 (2d4 + 5) slashing damage plus 10 (3d6) cold damage.

___Tail.___ Melee Weapon Attack:+10 to hit, reach 10 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage plus 10 (3d6) cold damage.

___Wall of Ice.___ The devil magically forms an opaque wall of ice on a solid surface it can see within 60 feet of it. The wall is 1 foot thick and up to 30 feet long and 10 feet high, or it's a hemispherical dome up to 20 feet in diameter.

When the wall appears, each creature in its space is pushed out of it by the shortest route. The creature chooses which side of the wall to end up on, unless the creature is incapacitated. The creature then makes a DC 17 Dexterity saving throw, taking 35 (10d6) cold damage on a failed save, or half as much damage on a successful one.

The wall lasts for 1 minute or until the devil is incapacitated or dies. The wall can be damaged and breached; each 10-foot section has AC 5, 30 hit points, vulnerability to fire damage, and immunity to acid, cold, necrotic, poison, and psychic damage. If a section is destroyed, it leaves behind a sheet of frigid air in the space the wall occupied. Whenever a creature finishes moving through the frigid air on a turn, willingly or otherwise, the creature must make a DC 17 Constitution saving throw, taking 17 (5d6) cold damage on a failed save, or half as much damage on a successful one. The frigid air dissipates when the rest of the wall vanishes.