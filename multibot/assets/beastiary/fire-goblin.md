"Fire Goblin";;;_size_: Small humanoid
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common, Goblin, Sylvan"
_skills_: "Deception +4, Perception +3, Stealth +7"
_senses_: "darkvision 120 ft., passive Perception 13"
_saving_throws_: "Dex +7, Con +4"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire"
_damage_resistances_: "lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "20 ft."
_hit points_: "45 (10d6 + 10)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 18 (+4) | 13 (+1) | 16 (+3) | 10 (+0) | 12 (+1) |

___Flaming Arrows.___ Fire goblins are adept at manufacturing flaming
arrows, and coupled with their produce flame cantrip, a fire goblin with
prepared arrows (wrapped and treated to be easily flammable) may set
up to 2 arrows on fire as a seamless part of each turn’s attack, without
requiring a separate action. Preparing an arrow for such a use still takes
the normal amount of time but can be done in advance and saved for a
later battle.

___Innate Spellcasting.___ The fire goblin’s spellcasting ability is Intelligence
(spell save DC 14, +6 to hit with spell attacks). The fire goblin can innately
cast the following spells, requiring no material components:

* At will: _fire bolt, produce flame_

* 3/day each: _burning hands, flame blade, flaming sphere, heat metal_

* 1/day: _fireball, wall of fire_

**Actions**

___Multiattack.___ The fire goblin can make two attacks with its flaming
arrows using its shortbow.

___Battleaxe.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage, or 7 (1d10 + 2) slashing damage if used with two hands.

___Shortbow.___ Ranged Weapon Attack: +7 to hit, range 80/320 ft., one
target. Hit: 7 (1d6 + 4) piercing damage plus 7 (2d6) fire damage.

___Fire Breath (Recharge 5–6).___ The goblin exhales fire in a 20-foot cone.
Each creature in that area must make a DC 15 Dexterity saving throw,
taking 45 (10d8) fire damage on a failed save, or half as much damage on
a successful one.
