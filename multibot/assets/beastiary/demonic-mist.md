"Demonic Mist";;;_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_saving_throws_: "Dex +6, Con +6"
_skills_: "Perception +3, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_damage_resistances_: "acid, cold, fire; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "fly 50 ft."
_hit points_: "85 (10d8 + 40)"
_armor class_: "16 (natural armor)"
_stats_: | 6 (-2) | 18 (+4) | 18 (+4) | 8 (-1) | 13 (+1) | 16 (+3) |

___Incorporeal Movement.___ The demonic mist can move through other
creatures and objects as if they were difficult terrain. It takes 5 (1d10)
force damage if it ends its turn inside an object.


___Innate Spellcasting.___ A demonic mist’s spellcasting ability is Charisma
(spell save DC 13, +5 to hit with spell attacks), and requires no material
components for the following spells:

* At will: _detect magic_

* 3/day each: _ray of enfeeblement, vampiric touch_

* 1/day each: _confusion, fear_

___Vulnerability to Wind.___ The demonic mist has
disadvantage on saving throws against wind and wind-like
effects (gust of wind, etc.).

**Actions**

___Demonic Touch.___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 25 (6d6 + 4) necrotic damage.

___Psychic Crush (Recharge 5–6).___ The demonic mist
attempts to crush the mind of a single creature it can see
within 30 feet. The target must make a successful DC 15
Wisdom saving throw or take 14 (4d6) psychic damage and
be frightened for 1 minute.
