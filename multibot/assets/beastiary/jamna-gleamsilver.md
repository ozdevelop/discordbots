"Jamna Gleamsilver";;;_size_: Small humanoid (gnome)
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Gnomish, Goblin, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Acrobatics +5, Deception +3, Insight +2, Perception +4, Persuasion +3, Stealth +7"
_saving_throws_: "Dex +5, Int +4"
_speed_: "25 ft."
_hit points_: "22 (4d6+8)"
_armor class_: "15 (leather armor)"
_stats_: | 8 (-1) | 17 (+3) | 14 (+2) | 15 (+2) | 10 (0) | 12 (+1) |

___Cunning Action.___ Jamna can take a bonus action to take the Dash, Disengage, or Hide action.

___Gnome Cunning.___ Jamna has advantage on Intelligence, Wisdom and Charisma saving throws against magic.

___Spellcasting.___ Jamna is a 4th-level spellcaster that uses Intelligence as her spellcasting ability (spell save DC 12, +4 to hit with spell attacks). Jamna has the following spells prepared from the wizard spell list.

* Cantrips (at will): _mage hand, minor illusion, prestidigitation, ray of frost_

* 1st level (3 slots): _charm person, color spray, disguise self, longstrider_

**Actions**

___Multiattack.___ Jamna attacks twice with her shortswords.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage, or 9 (1d6 + 3) plus (1d6) piercing damage if the target is Medium or larger.
