"Magnesium Golem";;;_size_: Medium construct
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "68 (8d8 + 32)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 9 (-1) | 18 (+4) | 6 (-2) | 10 (+0) | 5 (-3) |

___Aura of Sickness.___ Creatures who begin their turn within 10 feet of
the golem must make a DC 15 Constitution saving throw. On a failed
saving throw, the creature is poisoned for 1 minute. If a creature ends its
turn outside the 10-foot area, it can repeat the saving throw, ending the
effect on a success. If the creature succeeds or if the effect ends for it, it is
immune to the golem’s aura of sickness for 24 hours.

___Fire Absorption.___ Whenever the golem is subjected to fire damage, it
takes no damage and instead regains a number of hit points equal to the
fire damage dealt.

___Immutable Form.___ The golem is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against
spells and other magical effects.

___Magic Weapon.___ The golem’s weapon attacks are magical.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) bludgeoning damage.
