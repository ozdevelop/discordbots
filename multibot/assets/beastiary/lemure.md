"Lemure";;;_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "0 (10 XP)"
_languages_: "understands infernal but can't speak"
_senses_: "darkvision 120 ft."
_damage_immunities_: "fire, poison"
_speed_: "15 ft."
_hit points_: "13 (3d8)"
_armor class_: "7"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "cold"
_stats_: | 10 (0) | 5 (-3) | 11 (0) | 1 (-5) | 11 (0) | 3 (-4) |

___Devil's Sight.___ Magical darkness doesn't impede the lemure's darkvision.

___Hellish Rejuvenation.___ A lemure that dies in the Nine Hells comes back to life with all its hit points in 1d10 days unless it is killed by a good-aligned creature with a bless spell cast on that creature or its remains are sprinkled with holy water.

**Actions**

___Fist.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 2 (1d4) bludgeoning damage