"Fear Smith";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "10 (5900 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Intimidate +6, Stealth +7"
_senses_: "blindsight 30 ft., passive Perception 12"
_saving_throws_: "Wis +6"
_damage_resistances_: "bludgeoning, piercing, and slashing from weapons that aren't made of cold iron"
_condition_immunities_: "charmed, frightened"
_speed_: "40 ft., climb 15 ft."
_hit points_: "123 (19d8 + 38)"
_armor class_: "17 (natural armor)"
_stats_: | 11 (+0) | 17 (+3) | 14 (+2) | 11 (+0) | 15 (+2) | 18 (+4) |

___Distortion Gaze.___ Those who meet the gaze of the fear smith experience the world seeming to twist at unnatural angles beneath their feet. When a creature that can see the fear smith's eye starts its turn within 30 feet of the fear smith, the creature must make a successful DC 16 Wisdom saving throw or become disoriented. While disoriented, the creature falls prone each time it tries to move or take the Dash or Disengage action. To recover from disorientation, a creature must start its turn outside the fear smith's gaze and make a successful DC 16 Wisdom saving throw. To use this ability, the fear smith can't be incapacitated and must see the affected creature. A creature that isn't surprised can avert its eyes at the start of its turn to avoid the effect. In that case, no saving throw is necessary but the creature treats the fear smith as invisible until the start of the creature's next turn. If during its turn the creature chooses to look at the fear smith, it must immediately make the saving throw.

___Hidden Eye.___ The fear smith has advantage on saving throws against the blinded condition.

___Innate Spellcasting.___ The fear smith's innate spellcasting ability is Charisma (spell save DC 16). The fear smith can innately cast the following spells, requiring no verbal or material components:

At will: detect thoughts, fear

2/day each: charm person, command, confusion

___Magic Resistance.___ The fear smith has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The fear smith makes three claw attacks.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., 1 creature. Hit: 16 (2d12 + 3) slashing damage. If the target is disoriented by Distortion Gaze, this attack does an additional 13 (3d8) psychic damage and heals the fear smith by an equal amount.

___Heartstopping Stare.___ The fear smith terrifies a creature within 30 feet with a look. The target must succeed on a DC 16 Wisdom saving throw or be stunned for 1 round and take 13 (3d8) psychic damage and heal the fear smith by an equal amount.

