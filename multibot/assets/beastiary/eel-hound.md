"Eel Hound";;;_size_: Medium fey
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Sylvan"
_skills_: "Perception +3, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "30 ft., swim 40 ft."
_hit points_: "77 (14d8 + 14)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 16 (+3) | 13 (+1) | 6 (-2) | 13 (+1) | 16 (+3) |

___Amphibious.___ The eel hound can breathe air and water.

___Pack Tactics.___ The eel hound has advantage on an attack roll against a creature if at least one of the hound's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Slick Spittle.___ By spending 2 rounds dribbling spittle on an area, an eel hound can cover a 5-foot square with its slippery saliva. This area is treated as if under the effects of a grease spell, but it lasts for 1 hour.

___Slithering Bite.___ A creature an eel hound attacks can't make opportunity attacks against it until the start of the creature's next turn.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage, and the target is grappled (escape DC 14).

