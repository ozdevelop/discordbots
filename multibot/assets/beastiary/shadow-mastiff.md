"Shadow Mastiff";;;_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +6"
_speed_: "40 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "12"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks while in dim light or darkness"
_stats_: | 16 (+3) | 14 (+2) | 13 (+1) | 5 (-3) | 12 (+1) | 5 (-3) |

___Ethereal Awareness.___ The shadow mastiff can see ethereal creatures and objects.

___Keen Hearing and Smell.___ The shadow mastiff has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Shadow Blend.___ While in dim light or darkness, the shadow mastiff can use a bonus action to become invisible, along with anything it is wearing or carrying. The invisibility lasts until the shadow mastiff uses a bonus action to end it or until the shadow mastiff attacks, is in bright light, or is incapacitated.

___Sunlight Weakness.___ While in bright light created by sunlight, the shadow mastiff has disadvantage on attack rolls, ability checks, and saving throws.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6+3) piercing damage. If the target is a creature, it must succeed on a DC 13 Strength saving throw or be knocked prone.