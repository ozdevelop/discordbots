"Elder Conflux Elemental";;;_size_: Gargantuan elemental
_alignment_: unaligned
_challenge_: "20 (25,000 XP)"
_languages_: "Primordial, Aquan, Auran, Ignan, Terran"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +9, Con +12, Wis +9"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "50 ft., fly 50 ft. (hover)"
_hit points_: "313 (19d20 + 114)"
_armor class_: "19 (natural armor)"
_stats_: | 24 (+7) | 16 (+3) | 23 (+6) | 10 (+0) | 16 (+3) | 8 (-1) |

___Illumination.___ The elemental sheds bright light in a 50-
foot radius and dim light for an additional 50 feet.

___Legendary Resistance (3/Day).___ If the elemental fails a
saving throw, it can choose to succeed instead.

___Mutable Form.___ The elemental can move through a space
as narrow as 1 inch wide without squeezing.

___Tempest Barrier.___ A creature that touches the elemental
or hits it with a melee or ranged attack while within 15
feet of it takes 11 (2d10) lightning damage.

**Actions**

___Multiattack.___ The elemental makes two touch attacks.

___Touch.___ Melee Weapon Attack: +13 to hit, reach 10 ft.,
one target. Hit: 14 (2d6 + 7) bludgeoning damage plus
3 (1d6) cold damage, 3 (1d6) fire damage, and 3 (1d6)
lightning damage.

___Boulder Barrage (Recharge 5-6).___ The elemental launches
a barrage of boulders at a point it can see within 60
feet. Each creature within 15 feet of that point must
make a DC 19 Dexterity saving throw, taking 38
(7d10) bludgeoning damage on a failed save, or half as
much damage on a successful one. A creature that fails
this save by 10 or more is also knocked prone.

___Icy Pulse (Recharge 5-6).___ The elemental sends out a
pulse of ice around its body. Each creature within 30
feet of the elemental must make a DC 19 Constitution
saving throw, taking 40 (9d8) cold damage and having
its speed halved on its next turn on a failed save, or half
as much damage and not slowed on a successful one.
A creature that fails this save by 10 or more has a
speed of 0 on its next turn instead.

___Flame Blades (Recharge 5-6).___ The elemental unleashes 3
blades of fire in separate 5-foot wide, 90-foot long
lines. Each creature within one of these lines must
make a DC 19 Dexterity saving throw, taking 28 (8d6)
fire damage on a failed save, or half as much damage
on a successful one.

___Summon Elemental Obelisks (1/Day).___ The elemental
summons 3 immobile obelisks in unoccupied spaces
within 60 feet. These obelisks are medium objects
with AC 17 and 40 hit points. Each of these have a
small orb floating above them charged with elemental
power – one cold, one fire, and one lightning. Until
they are destroyed or the elemental killed, on initiative
count 20 (losing initiative ties), these orbs perform a
ranged spell attack against a creature within 60 feet
with a +10 bonus to hit. On a hit, that creature takes
17 (5d6) damage of the corresponding element type.

**Legendary** Actions

The elemental can take 3 legendary actions, choosing
from the options below. Only one legendary action can
be used at a time and only at the end of another
creature’s turn. The elemental regains spent legendary
actions at the start of its turn.

___Touch.___ The elemental makes a touch attack.

___Elemental Onslaught (2 actions).___ The elemental uses its
boulder barrage, icy pulse, or flame blades.
