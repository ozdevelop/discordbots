"Yuan-ti Anathema";;;_size_: Huge monstrosity (shapechanger yuan-ti)
_challenge_: "12 (8,400 XP)"
_languages_: "Abyssal, Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Perception +7, Stealth +5"
_damage_immunities_: "poison"
_speed_: "40 ft., climb 30 ft., swim 30 ft."
_hit points_: "189 (18d12+72)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "acid, fire, lightning"
_stats_: | 23 (+6) | 13 (+1) | 19 (+4) | 19 (+4) | 17 (+3) | 20 (+5) |

___Innate Spellcasting (Anathema Form Only).___ The anathema's innate spellcasting ability is Charisma (spell save DC 17). It can innately cast the following spells, requiring no material components:

* At will: _animal friendship _(snakes only)

* 3/day each: _darkness, entangle, fear, haste, suggestion, polymorph_

* 1/day: _divine word_

___Magic Resistance.___ The anathema has advantage on saving throws against spells and other magical effects.

___Ophidiophobia Aura.___ Any creature of the anathema's choice, other than a snake or a yuan-ti, that starts its turn within 30 feet of the anathema and can see or hear it must succeed on a DC 17 Wisdom saving throw or become frightened of snakes and yuan-ti. A frightened target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a target's saving throw is successful or the effect ends for it, the target is immune to this aura for the next 24 hours.

___Shapechanger.___ The anathema can use its action to polymorph into a Huge giant constrictor snake, or back into its true form. its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed.

___Six Heads.___ The anathema has advantage on Wisdom (Perception) checks and on saving throws against being blinded. charmed, deafened, frightened, stunned, or knocked unconscious.

___Variant: Acid Slime.___ As a bonus action, the yuan-ti can coat its body in a slimy acid that lasts for 1 minute. A creature that touches the yuan-ti, hits it with a melee attack while within 5 feet of it, or is hit by its constrict attack takes 5 (1d10) acid damage.

___Variant: Chameleon Skin.___ The yuan-ti has advantage on Dexterity (Stealth) checks made to hide.

___Variant: Shed Skin (1/Day).___ The yuan-ti can shed its skin as a bonus action to free itself from a grapple, shackles, or other restraints. If the yuan-ti spends 1 minute eating its shed skin, it regains hit points equal to half its hit point maximum.

**Actions**

___Multiattack (Anathema Form Only).___ The anathema makes two claw attacks, one constrict attack, and one Flurry of Bites attack.

___Claw (Anathema Form Only).___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 13 (2d6+6) slashing damage.

___Constrict.___ Melee Weapon Attack: +10 to hit, reach 15 ft., one Large or smaller creature. Hit: 16 (3d6+6) bludgeoning damage plus 7 (2d6) acid damage, and the target is grappled (escape DC 16). Until this grapple ends, the target is restrained and takes 16 (3d6+6) bludgeoning damage plus 7 (2d6) acid damage at the start of each of its turns, and the anathema can't constrict another target.

___Flurry of Bites.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one creature. Hit: 27 (6d6+6) piercing damage plus 14 (4d6) poison damage.
