"Gnoll Fang of Yeenoghu";;;_size_: Medium humanoid (gnoll)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Gnoll"
_senses_: "darkvision 60 ft."
_saving_throws_: "Con +4, Wis +2, Cha +3"
_speed_: "30 ft."
_hit points_: "65 (10d8+20)"
_armor class_: "14 (hide armor)"
_stats_: | 17 (+3) | 15 (+2) | 15 (+2) | 10 (0) | 11 (0) | 13 (+1) |

___Rampage.___ When the gnoll reduces a creature to 0 hit points with a melee attack on its turn, the gnoll can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Multiattack.___ The gnoll makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 6 (1d6 + 3) piercing damage, and the target must succeed on a DC 12 Constitution saving throw or take 7 (2d6) poison damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage.