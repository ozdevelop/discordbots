"Red Abishai";;;_page_number_: 160
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "19 (22,000 XP)"
_languages_: "Draconic, Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 18"
_skills_: "Intimidation +10, Perception +8"
_damage_immunities_: "fire, poison"
_saving_throws_: "Str +12, Con +10, Wis +8"
_speed_: "30 ft., fly 50 ft."
_hit points_: "255  (30d8 + 120)"
_armor class_: "22 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 23 (+6) | 16 (+3) | 19 (+4) | 14 (+2) | 15 (+2) | 19 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the abishai's darkvision.

___Magic Resistance.___ The abishai has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The abishai's weapon attacks are magical.

**Actions**

___Multiattack___ The abishai can use its Frightful Presence. It also makes three attacks: one with its morningstar, one with its claw, and one with its bite.

___Morningstar___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 10 (1d8 + 6) piercing damage.

___Claw___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 17 (2d10 + 6) slashing damage.

___Bite___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 22 (5d10 + 6) piercing damage plus 38 (7d10) fire damage.

___Frightful Presence___ Each creature of the abishai's choice that is within 120 feet and aware of it must succeed on a DC 18 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the abishai's Frightful Presence for the next 24 hours.

___Incite Fanaticism___ The abishai chooses up to four of its allies within 60 feet of it that can see it. For 1 minute, each of those allies makes attack rolls with advantage and can't be frightened.

___Power of the Dragon Queen___ The abishai targets one dragon it can see within 120 feet of it. The dragon must make a DC 18 Charisma saving throw. A chromatic dragon makes this save with disadvantage. On a successful save, the target is immune to the abishai's Power of the Dragon Queen for 1 hour. On a failed save, the target is charmed by the abishai for 1 hour. While charmed in this way, the target regards the abishai as a trusted friend to be heeded and protected. This effect ends if the abishai or its companions deal damage to the target.