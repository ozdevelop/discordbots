"Sand Hag";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Dwarvish, Giant, Gnomish"
_skills_: "Deception +6, Perception +5, Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 15"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 16 (+3) | 14 (+2) | 16 (+3) |

___Magic Resistance.___ The sand hag has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The sand hag's innate spellcasting ability is Charisma (spell save DC 14). It can innately cast the following spells, requiring no material components:

At will: invisibility

2/day each: hallucinatory terrain, major image

___Mimicry.___ The sand hag can mimic animal sounds and humanoid voices. A creature that hears the sounds can tell they are imitations only with a successful DC 14 Wisdom (Insight) check.

___Scorpion Step.___ The sand hag walks lightly across sandy surfaces, never sinking into soft sand or leaving tracks. When in sand terrain, the sand hag ignores difficult terrain, doesn't leave tracks, and gains tremorsense 30 ft.

**Actions**

___Multiattack.___ The sand hag makes two claw attacks.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage. If the target is a creature, it must make a successful DC 12 Constitution saving throw or gain one level of exhaustion.

___Scouring Sirocco (Recharge 5-6).___ The sand hag generates a blast of hot wind in a 30-foot line or a 15-foot cone. Creatures inside it take 14 (4d6) slashing damage plus 7 (2d6) fire damage and are blinded for 1 minute; a successful DC 14 Constitution saving throw halves the damage and negates the blindness. A blinded creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. The affected area (line or cone) is heavily obscured until the end of the sand hag's next turn.

___Change Shape.___ The hag polymorphs into a Small or Medium female humanoid, or back into her true form. Her statistics are the same in each form. Any equipment she is wearing or carrying isn't transformed. She reverts to her true form if she dies.

