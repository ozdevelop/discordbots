"Frulam Mondath";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Draconic, Infernal"
_skills_: "Deception +4, History +2, Religion +2"
_saving_throws_: "Wis +6, Cha +4"
_speed_: "30 ft."
_hit points_: "44 (8d8+8)"
_armor class_: "16 (chain mail)"
_stats_: | 14 (+2) | 10 (0) | 13 (+1) | 11 (0) | 18 (+4) | 15 (+2) |

___Spellcasting.___ Frulam is a 5th-level spellcaster that uses Wisdom as her spellcasting ability (spell save DC 14, +6 to hit with spell attacks). Frulam has the following spells prepared from the cleric spell list:

* Cantrips (at will): _light, sacred flame, thaumaturgy_

* 1st level (4 slots): _command, cure wounds, healing word, sanctuary_

* 2nd level (3 slots): _calm emotions, hold person, spiritual weapon_

* 3rd level (2 slots): _mass healing word, spirit guardians_

**Actions**

___Multiattack.___ Frulam attacks twice with her halberd.

___Halberd.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 7 (1d10 + 2) bludgeoning damage.
