"Tribal Soldier Veteran";;;_size_: Medium humanoid (any race)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "any two languages"
_skills_: "Acrobatics +4, Athletics +4"
_senses_: "passive Perception 11"
_speed_: "30 ft."
_hit points_: "58 (9d8 + 18)"
_armor class_: "15 (studded leather)"
_stats_: | 14 (+2) | 15 (+2) | 15 (+2) | 10 (+0) | 12 (+1) | 8 (-1) |

___Natural Resilience.___ A life away from the pleasures of
society has made the soldier tougher than an
ordinary individual. It has advantage on Constitution
and Strength saving throws.

___Inspirational Presence.___ Allies within 60 feet of the elder that
can see it gain a +1 bonus to all attack and damage rolls. (This
bonus cannot exceed +1, even if multiple veterans are
present.)

**Actions**

___Multiattack.___ The soldier makes two glaive attacks.

___Glaive.___ Melee Weapon Attack: +4 to hit, reach 10
ft., one target. Hit: 7 (1d10 + 2) slashing damage.

___Javelin.___ Ranged Weapon Attack: +4 to hit, range
30/120 ft., one target. Hit: 5 (1d6 + 2) piercing
damage.

**Reactions**

___Readied Strike.___ Whenever a creature enters the
reach of the soldier's glaive, the soldier can
immediately make a glaive attack against that
creature at disadvantage.
