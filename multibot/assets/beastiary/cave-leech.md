"Cave Leech";;;_size_: Medium monstrosity
_alignment_: neutral
_challenge_: "8 (3,900 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +3"
_senses_: "darkvision 60 ft., tremorsense 60 ft. passive Perception 14"
_speed_: "30 ft., swim 30 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 11 (+0) | 17 (+3) | 3 (-4) | 12 (+1) | 6 (-2) |

**Actions**

___Multiattack.___ The leech makes up to four tentacle attacks and can use
blood drain on a grappled creature.

___Tentacles.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit:
11 (2d8 + 2) bludgeoning damage. The target is grappled (escape DC 12)
if the leech isn’t already grappling a creature, and the target is restrained
until the grapple ends. While grappling a creature, the leech can’t use that
tentacle. The leech has four tentacles

___Blood Drain.___ Melee Weapon Attack: +5 to hit, reach 5 ft. one creature.
Hit: 9 (2d6+2) piercing damage, and the leech attaches to the target. While
attached, the leech doesn’t attack. Instead, at the start of the leech’s turns,
the target loses 9 (2d6 + 2) hit points due to blood loss.

The leech can detach itself by spending 5 feet of its movement. It does
so after it drains 25 hit points of blood from the target or the target dies. A
creature, including the target, can use its action to make a DC 12 Strength
check to rip the leech off and make it detach.
