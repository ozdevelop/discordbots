"Jaculus";;;_size_: Small dragon
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Draconic"
_skills_: "Acrobatics +6, Perception +3, Stealth +6"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 13"
_saving_throws_: "Str +4, Dex +6, Con +5, Wis +3, Cha +3"
_damage_resistances_: "acid, lightning"
_speed_: "20 ft., climb 20 ft., fly 10 ft."
_hit points_: "65 (10d6 + 30)"
_armor class_: "18 (natural armor)"
_stats_: | 14 (+2) | 18 (+4) | 17 (+3) | 13 (+1) | 13 (+1) | 13 (+1) |

___Spearhead.___ If the jaculus moves at least 10 feet straight toward a target and hits that target with a jaws attack on the same turn, the jaws attack does an extra 4 (1d8) piercing damage.

**Actions**

___Multiattack.___ The jaculus makes one jaws attack and one claws attack.

___Jaws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d4 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 2) slashing damage.

