"Woodwraith";;;_size_: Medium undead (plant)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "bludgeoning, piercing"
_speed_: "30 ft."
_hit points_: "85 (10d8 + 40)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 10 (+0) | 18 (+4) | 6 (-2) | 10 (+0) | 5 (-3) |

___Locus of Spirits.___ Whenever a non-evil humanoid dies
within 60 feet of the wraith, their body rises as an
Overgrowth Ghoul under the wraith’s control 1d4
rounds later.

**Actions**

___Multiattack.___ The wraith uses its Convert Flesh ability.
It then makes two vine slash attacks..

___Vine Slash.___ Melee Weapon Attack: +4 to hit, reach
10 ft., one target. Hit: 8 (1d8 + 2) slashing damage.

___Convert Flesh.___ Target creature within 60 feet makes
a DC 13 Constitution saving throw against being
magically petrified. On a failed save, the creature
begins to turn to wood and their movement speed
is reduced to 0 ft. It must repeat the saving throw
at the end of its next turn. On a success, the effect
ends. On a failure, the creature turns to wood and is
considered petrified for 24 hours.

___Rain of Splinters (Recharge 5-6).___ The wraith
unleashes an explosion of sharp splinters in a 15
foot cone. Each creature in that area must make a
DC 13 Dexterity saving throw, taking 27 (6d8)
piercing damage on a failed save, or half as much
on a successful one.
