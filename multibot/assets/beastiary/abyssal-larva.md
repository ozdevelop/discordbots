Abyssal Larva;;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: 1/2 (100 XP)
_languages_: Abyssal
_skills_: Perception +2, Stealth +2
_damage_immunities_: psychic
_damage_resistances_: acid, cold, fire
_condition_immunities_: charmed, frightened
_speed_: 20 ft.
_hit points_: 39 (6d8 + 12)
_armor class_: 12 (natural armor)
_stats_: | 10 (+0) | 13 (+1) | 14 (+2) | 3 (-4) | 10 (+0) | 7 (-2) |

___Tortured Mind.___ The mind of an Abyssal larva cannot be read. If a
creature attempts to read an Abyssal larva’s mind, it takes 7 (2d6) psychic
damage and must succeed on a DC 12 Wisdom saving throw. On a failed
saving throw, the creature is poisoned for 1 minute. While poisoned, the
creature cannot take reactions and uses their action to Dash in a random
direction, even if that leads them into dangerous areas.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) piercing damage plus 5 (2d4) acid damage.

___Maggot Spray.___ Ranged Spell Attack: +3 to hit, range 10 ft., one target.
Hit: 1 poison damage and the target must succeed a DC 12 Dexterity
saving throw or be poisoned for 1 minute. A poisoned creature can make
a DC 12 Constitution saving throw at the end of each of its turns, ending
the poisoned effect on a success.
