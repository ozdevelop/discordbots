"Constrictor Snake";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 10 ft."
_speed_: "30 ft., swim 30 ft."
_hit points_: "13 (2d10+2)"
_armor class_: "12"
_stats_: | 15 (+2) | 14 (+2) | 12 (+1) | 1 (-5) | 10 (0) | 3 (-4) |

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 5 (1d6 + 2) piercing damage.

___Constrict.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 6 (1d8 + 2) bludgeoning damage, and the target is grappled (escape DC 14). Until this grapple ends, the creature is restrained, and the snake can't constrict another target.