"Androsphinx";;;_size_: Large monstrosity
_alignment_: lawful neutral
_challenge_: "17 (18,000 XP)"
_languages_: "Common, Sphinx"
_senses_: "truesight 120 ft."
_skills_: "Arcana +9, Perception +10, Religion +15"
_damage_immunities_: "psychic, bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Dex +6, Con +11, Int +9, Wis +10"
_speed_: "40 ft., fly 60 ft."
_hit points_: "199 (19d10+95)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, frightened"
_stats_: | 22 (+6) | 10 (0) | 20 (+5) | 16 (+3) | 18 (+4) | 23 (+6) |

___Inscrutable.___ The sphinx is immune to any effect that would sense its emotions or read its thoughts, as well as any divination spell that it refuses. Wisdom (Insight) checks made to ascertain the sphinx's intentions or sincerity have disadvantage.

___Magic Weapons.___ The sphinx's weapon attacks are magical.

___Spellcasting.___ The sphinx is a 12th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 18, +10 to hit with spell attacks). It requires no material components to cast its spells. The sphinx has the following cleric spells prepared:

* Cantrips (at will): _sacred flame, spare the dying, thaumaturgy_

* 1st level (4 slots): _command, detect evil and good, detect magic_

* 2nd level (3 slots): _lesser restoration, zone of truth_

* 3rd level (3 slots): _dispel magic, tongues_

* 4th level (3 slots): _banishment, freedom of movement_

* 5th level (2 slots): _flame strike, greater restoration_

* 6th level (1 slot): _heroes' feast_

**Actions**

___Multiattack.___ The sphinx makes two claw attacks.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 17 (2d10 + 6) slashing damage.

___Roar (3/Day).___ The sphinx emits a magical roar. Each time it roars before finishing a long rest, the roar is louder and the effect is different, as detailed below. Each creature within 500 feet of the sphinx and able to hear the roar must make a saving throw.

First Roar. Each creature that fails a DC 18 Wisdom saving throw is frightened for 1 minute. A frightened creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

Second Roar. Each creature that fails a DC 18 Wisdom saving throw is deafened and frightened for 1 minute. A frightened creature is paralyzed and can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

Third Roar. Each creature makes a DC 18 Constitution saving throw. On a failed save, a creature takes 44 (8d10) thunder damage and is knocked prone. On a successful save, the creature takes half as much damage and isn't knocked prone.

**Legendary** Actions

The androsphinx can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The androsphinx regains spent legendary actions at the start of its turn.

___Claw Attack.___ The sphinx makes one claw attack.

___Teleport (Costs 2 Actions).___ The sphinx magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.

___Cast a Spell (Costs 3 Actions).___ The sphinx casts a spell from its list of prepared spells, using a spell slot as normal.
