"Ancient White Dragon";;;_size_: Gargantuan dragon
_alignment_: chaotic evil
_challenge_: "20 (25,000 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 60 ft., darkvision 120 ft."
_skills_: "Perception +13, Stealth +6"
_damage_immunities_: "cold"
_saving_throws_: "Dex +6, Con +14, Wis +7, Cha +8"
_speed_: "40 ft., burrow 40 ft., fly 80 ft., swim 40 ft."
_hit points_: "333 (18d20+144)"
_armor class_: "20 (natural armor)"
_stats_: | 26 (+8) | 10 (0) | 26 (+8) | 10 (0) | 13 (+1) | 14 (+2) |

___Ice Walk.___ The dragon can move across and climb icy surfaces without needing to make an ability check. Additionally, difficult terrain composed of ice or snow doesn't cost it extra moment.

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +14 to hit, reach 15 ft., one target. Hit: 19 (2d10 + 8) piercing damage plus 9 (2d8) cold damage.

___Claw.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target. Hit: 15 (2d6 + 8) slashing damage.

___Tail.___ Melee Weapon Attack: +14 to hit, reach 20 ft., one target. Hit: 17 (2d8 + 8) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours .

___Cold Breath (Recharge 5-6).___ The dragon exhales an icy blast in a 90-foot cone. Each creature in that area must make a DC 22 Constitution saving throw, taking 72 (l6d8) cold damage on a failed save, or half as much damage on a successful one.