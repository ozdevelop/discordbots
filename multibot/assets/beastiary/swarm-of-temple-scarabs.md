"Swarm of Temple Scarabs";;;_size_: Medium swarm
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "darkvision 60ft., passive Perception 10"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "25 ft., climb 25 ft."
_hit points_: "36 (8d8)"
_armor class_: "13"
_stats_: | 8 (-1) | 16 (+3) | 10 (+0) | 2 (-4) | 10 (+0) | 4 (-3) |

___Swarm.___ The swarm can occupy another creature's
space and vice versa, and the swarm can move
through any opening large enough for a Tiny temple
scarab. The swarm can't regain hit points or gain
temporary hit points.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 10 (4d4) piercing damage and the
target must succeed on a DC 12 Constitution
saving throw or 1d4 scarabs burrows under the
skin of the target. A creature may use one of its
attack actions to attempt to cut out a scarab. That
creature makes a DC 10 sleight of hand check,
cutting it out of the victim on a success and
dealing 3 damage to the swarm. Success or fail, the
creature in which the scarab is burrowed takes 1
point of piercing damage from this maneuver.

If a scarab is burrowed, at the end of each of its
turns it moves closer to the creature’s brain,
dealing an additional 1 piercing damage. If the
scarab is not removed after 3 turns of being
burrowed, it latches on to the brain of the creature
and takes control of its body until removed. A
creature that has a scarab removed from its brain
falls unconscious for 1d4 days.
