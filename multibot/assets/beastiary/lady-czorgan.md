"Lady Czorgan";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "12 (8,400 XP)"
_languages_: "Abyssal, Common"
_senses_: "darkvision 120 ft., passive Perception 11"
_saving_throws_: "Dex +5, Wis +6, Cha +7"
_damage_immunities_: "necrotic, poison"
_condition_immunities_: "exhaustion, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "97 (13d8 + 39)"
_armor class_: "18 (plate)"
_stats_: | 18 (+4) | 10 (+0) | 16 (+3) | 10 (+0) | 13 (+1) | 16 (+3) |

___Magic Resistance.___ Lady Czorgan has advantage
on saving throws against spells and other
magical effects.

___Unholy Aura.___ Unless Lady Czorgan is incapacitated,
she and undead creatures of her choice
within 30 feet have advantage on saving throws
against features that turn undead.

___Spellcasting.___ Lady Czorgan is a 13th-level spellcaster.
Her spellcasting ability is Charisma (spell
save DC 15, +8 to hit with spell attacks). She has
the following spells prepared:

* 1st level (4 slots): _command, hunter's mark, inflict wounds_

* 2nd level (3 slots): _hold person, magic weapon_

* 3rd level (3 slots): _dispel magic, vampiric touch_

* 4th level (1 slot): _banishment, blight_

**Actions**

___Multiattack.___ Lady Czorgan makes two longsword
attacks.

___Longsword.___ Melee Weapon Attack: +8 to hit,
reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing
damage, or 10 (1d10 + 4) slashing damage if used
with two hands, plus 13 (3d8) necrotic damage.

___Decaying Ground (1/Day).___ Lady Czorgan points
at a location she can see within 60 feet of her and corruption explodes
from the ground at that point. Each
creature in a 20-foot-radius sphere centered on
that point must make a DC 15 Constitution saving
throw. The corrupted ground spreads around
corners. A creature takes 52 (15d6) necrotic
damage on a failed save, or half as much damage
on a successful one.
