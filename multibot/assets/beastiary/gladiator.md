"Gladiator";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Athletics +10, Intimidation +5"
_saving_throws_: "Str +7, Dex +5, Con +6"
_speed_: "30 ft."
_hit points_: "112 (15d8+45)"
_armor class_: "16 (studded leather, shield)"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 10 (0) | 12 (+1) | 15 (+2) |

___Brave.___ The gladiator has advantage on saving throws against being frightened.

___Brute.___ A melee weapon deals one extra die of its damage when the gladiator hits with it (included in the attack).

**Actions**

___Multiattack.___ The gladiator makes three melee attacks or two ranged attacks.

___Spear.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. and range 20/60 ft., one target. Hit: 11 (2d6 + 4) piercing damage, or 13 (2d8 + 4) piercing damage if used with two hands to make a melee attack.

___Shield Bash.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 9 (2d4 + 4) bludgeoning damage. If the target is a Medium or smaller creature, it must succeed on a DC 15 Strength saving throw or be knocked prone.

**Reactions**

___Parry.___ The gladiator adds 3 to its AC against one melee attack that would hit it. To do so, the gladiator must see the attacker and be wielding a melee weapon.