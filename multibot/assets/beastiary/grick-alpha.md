"Grick Alpha";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_senses_: "darkvision 60 ft."
_speed_: "30 ft., climb 30 ft."
_hit points_: "75 (10d10+20)"
_armor class_: "18 (natural armor)"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_stats_: | 18 (+4) | 16 (+3) | 15 (+2) | 4 (-3) | 14 (+2) | 9 (-1) |

___Stone Camouflage.___ The grick has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.

**Actions**

___Multiattack.___ The grick makes two attacks: one with its tail and one with its tentacles. If it hits with its tentacles, the grick can make one beak attack against the same target.

___Tail.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.

___Tentacles.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 22 (4d8 + 4) slashing damage.

___Beak.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) piercing damage.