"Gem Dog";;;_size_: Small monstrosity
_alignment_: unaligned
_challenge_: "9 (5,000 XP)"
_languages_: "--"
_skills_: "Perception +8, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 18"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons not made of adamantine"
_condition_immunities_: "poisoned"
_speed_: "50 ft."
_hit points_: "112 (15d6 + 60)"
_armor class_: "16 (natural armor)"
_stats_: | 22 (+6) | 16 (+3) | 18 (+4) | 12 (+1) | 18 (+4) | 13 (+1) |

___Innate Spellcasting.___ The gem dog’s spellcasting ability is Wisdom
(spell save DC 16, +8 to hit with spell attacks). The gem dog can innately
cast the following spells, requiring no material, verbal, or somatic components:

* At will: _color spray, dancing lights, faerie fire_

* 3/day each: _blur, heat metal, hypnotic pattern, mirror image, misty step, moonbeam_

* 1/day: _hallucinatory terrain_

___Magic Resistance.___ The gem dog has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The gem dog attacks once with its bite and twice with its
claws, or it can make two Gem Spine Spray attacks.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 28
(4d10 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 15
(2d8 + 6) slashing damage.

___Gem Spine Spray.___ Ranged Weapon Attack: +7 to hit, range 20/60 ft.,
one target. Hit: 30 (5d10 + 3) piercing.

___Blinding Gleam.___ If the gem dog is in bright light, it can force one
creature that it can see within 60 feet of it to make a DC 16 Dexterity
saving throw if the gem dog isn’t incapacitated. On a failed save, the
creature is blinded for 1 minute as the gem dog reflects light into its eyes.

A creature that isn’t surprised can avert its eyes to avoid the saving
throw at the start of its turn. If it does so, it can’t see the gem dog until the
start of its next turn, when it can avert its eyes again. If it looks at the gem
dog in the meantime, it must immediately make the save.
