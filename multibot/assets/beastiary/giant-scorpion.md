"Giant Scorpion";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_senses_: "blindsight 60 ft."
_speed_: "40 ft."
_hit points_: "52 (7d10+14)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 13 (+1) | 15 (+2) | 1 (-5) | 9 (-1) | 3 (-4) |

**Actions**

___Multiattack.___ The scorpion makes three attacks: two with its claws and one with its sting.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) bludgeoning damage, and the target is grappled (escape DC 12). The scorpion has two claws, each of which can grapple only one target.

___Sting.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 7 (1d10 + 2) piercing damage, and the target must make a DC 12 Constitution saving throw, taking 22 (4d10) poison damage on a failed save, or half as much damage on a successful one.
