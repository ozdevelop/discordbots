"Green Guardian Gargoyle";;;_size_: Medium elemental
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Terran"
_skills_: "Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons not made of adamantine"
_condition_immunities_: "exhaustion, petrified, poisoned"
_speed_: "40 ft., fly 60 ft."
_hit points_: "85 (10d8 + 40)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 18 (+4) | 6 (-2) | 11 (+0) | 7 (-2) |

___False Appearance.___ While the gargoyle remains motionless, it is
indistinguishable from a weathered, inanimate statue that is covered in
fungus, lichen, and moss.

___Magic Weapons.___ The gargoyle’s attacks are magical.

___Reanimation.___ The eyes of a green guardian gargoyle are made of two
pieces of jet (500 gp each) that detect as both magic (faint conjuration)
and evil. After being destroyed, a green guardian automatically reanimates
in 1d8+2 days unless the eye gems are crushed and
disenchanted with both dispel magic and remove curse.

___Swoop.___ If the green guardian flies at least 20 feet
straight towards a target and then hits it with its gore
attack on the same turn, the target takes an additional 9
(2d8) piercing damage. If the target is a creature, it must
make a DC 13 Strength saving throw or be knocked prone.

**Actions**

___Multiattack.___ The green guardian makes four attacks: one with
its bite, two with its claws, and one with its gore.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 11
(2d8 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +45 to hit, reach 5 ft., one target. Hit: 9
(2d6 + 2) slashing damage. If both claws attacks hit the same target, then
it is grappled (escape DC 12) and restrained. At the start of the gargoyle’s
next turn, it will attempt to fly off with the target.

___Gore.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 13
(2d10 + 2) piercing damage.
