"Giant Hamster";;;_size_: Huge monstrosity
_alignment_: chaotic evil
_challenge_: "15 (13,000 XP)"
_languages_: "--"
_skills_: "Survival +7"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened"
_speed_: "40 ft., burrow 20 ft."
_hit points_: "189 (18d12 + 72)"
_armor class_: "16 (natural armor)"
_stats_: | 22 (+6) | 12 (+1) | 19 (+4) | 11 (+0) | 14 (+2) | 8 (-1) |

___Magic Resistance.___ The giant hamster has advantage on saving
throws against spells and other magical effects.

___Radioactive Aura.___ Creatures who enter or begin their turn within 10
feet of the giant hamster take 7 (2d6) necrotic damage and must make a
successful DC 15 Constitution saving throw or be poisoned for 1 minute.
A poisoned creature can repeat the saving throw at the end of each of its
turns, ending the effect on a success. If the saving throw is successful, or
the effect ends for it, the creature is immune to being poisoned by the giant
hamster’s aura for 24 hours, but not to further necrotic damage.
Siege Monster. The giant hamster deals double damage to objects and
structures.

**Actions**

___Multiattack.___ The giant hamster makes one bite attack and two claw
attacks.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 15
(2d8 + 6) piercing damage and the target is grappled (escape DC 17).

Until this grapple ends, the target is restrained and the hamster can’t bite
another target.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 16
(3d6 + 6) slashing damage.

___Energy Ray (Recharge 5–6).___ The giant hamster fires a ray of energy
from its eyes in a 60-foot line that is 5 feet wide. Creatures in the area must
succeed on a DC 15 Constitution saving throw or take 54 (12d8) necrotic
damage and be sickened for 1 minute. While sickened, a creature reduces
its maximum hit points by 5 at the start of each of its turns. Magic such as
lesser restoration can cure the sickened effect early.

___Swallow.___ One grappled creature must make a DC 17 Strength saving
throw or it is swallowed and the grapple ends. While swallowed, the
creature is blinded and restrained, it has total cover against attacks and
other effects outside the hamster, and it takes 14 (4d6) acid damage and
14 (4d6) necrotic damage at the start of each of the hamster’s turns. If the
hamster takes 25 damage or more on a single turn from a creature inside
it, the hamster must succeed on a DC 25 Constitution saving throw at the
end of that turn or regurgitate all swallowed creatures, which fall prone
in a space within 10 feet of it. If the hamster dies, a swallowed creature is
no longer restrained by it and can escape from the corpse using 15 feet of
movement, exiting prone.
