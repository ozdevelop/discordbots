"Gearkeeper Construct";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "10 (5,900 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "blindsight 120 ft., passive Perception 10"
_damage_immunities_: "fire, poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "60 ft."
_hit points_: "161 (17d10 + 68)"
_armor class_: "18 (natural armor)"
_stats_: | 20 (+5) | 16 (+3) | 18 (+4) | 3 (-4) | 11 (+0) | 1 (-5) |

___Immutable Form.___ The gearkeeper is immune to any spell or effect that would alter its form.

___Rapid Shifting.___ Opportunity attacks made against the gearkeeper have disadvantage.

___Whirling Blades.___ Any creature that starts its turn within 5 feet of the gearkeeper takes 4 (1d8) slashing damage.

**Actions**

___Multiattack.___ The gearkeeper makes two Arm Blade attacks, or one Arm Blade attack and one Spear Launcher attack.

___Arm Blade.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 18 (3d8 + 5) slashing damage.

___Spear Launcher.___ Ranged Weapon Attack: +9 to hit, range 90 ft., one target. Hit: 12 (2d6 + 5) piercing damage, and the target is knocked prone.

___Shrapnel Blast (Recharge 6).___ The gearkeeper jettisons a spray of jagged metal in a 30-foot cone. Each creature in the area must make a DC 15 Dexterity saving throw, taking 21 (6d6) piercing damage on a failed save, or half as much damage on a successful one.

