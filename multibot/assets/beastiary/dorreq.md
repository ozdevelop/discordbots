"Dorreq";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "4 (1100 XP)"
_languages_: "Void Speech"
_skills_: "Intimidate +2, Perception +3, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +6"
_damage_resistances_: "acid, cold, lightning"
_speed_: "20 ft., climb 15 ft."
_hit points_: "93 (17d8 + 17)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 19 (+4) | 13 (+1) | 11 (+0) | 8 (-1) | 6 (-2) |

___Innate Spellcasting.___ The dorreq's innate spellcasting ability is Intelligence (spell save DC 10). It can innately cast the following spells, requiring no material components: 3/day each: blink, dimension door, haste, shatter Wasteland Stride. This ability works like tree stride, but the dorreq can use it to sink into and appear out of any sandy or rocky ground, and the range is only 30 ft. Using this ability replaces the dorreq's usual movement.

**Actions**

___Multiattack.___ The dorreq makes two tentacle attacks and one bite attack. If both tentacle attacks hit, the target is grappled (escape DC 14).

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 22 (4d8 + 4) piercing damage.

___Tentacle.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage. If both tentacles hit the same target in a single turn, the target is grappled (escape DC 14) and pulled within reach of the bite attack, if it was farther than 5 feet away. The target must be size Large or smaller to be pulled this way. The dorreq can maintain a grapple on one Large, two Medium, or two Small creatures at one time.

___Entanglement.___ Any creature that starts its turn within 10 feet of a dorreq must make a successful DC 14 Dexterity saving throw each round or be restrained by the dorreq's tentacles until the start of its next turn. On its turn, the dorreq can ignore or freely release a creature in the affected area.

