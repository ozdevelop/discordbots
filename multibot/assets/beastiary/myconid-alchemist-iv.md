"Myconid Alchemist IV";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "Undercommon"
_skills_: "Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 13"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 12 (+1) | 10 (+0) | 8 (-1) |

___Stalk-still.___ A myconid that remains perfectly still has advantage on
Dexterity (Stealth) checks made to hide.

**Actions**

___Stone Knuckles.___ Melee weapon attack: +6 to hit. Hit: 10 (2d6 + 3)
slashing damage.

___Spore Bomb.___ Ranged weapon attack: +4 to hit, range 30/60 ft. Hit: 15
(6d4) poison damage and the target must succeed on a DC 15 Constitution
saving throw or be dominated for 1 minute. A dominated creature obeys
all commands telepathically delivered by the myconid’s spores. Each
time the target takes damage it is allowed another saving throw.
