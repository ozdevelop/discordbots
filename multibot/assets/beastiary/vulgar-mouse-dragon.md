"Vulgar Mouse Dragon";;;_size_: Tiny dragon
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "understands Common and Draconic, but can’t speak"
_skills_: "Perception +4, Stealth +5"
_senses_: "darkvision 30 ft., passive Perception 14"
_saving_throws_: "Dex +3, Con +2, Wis +2, Cha +3"
_speed_: "20 ft. climb 20 ft., burrow 10 ft."
_hit points_: "15 (6d4)"
_armor class_: "13 (natural armor)"
_stats_: | 4 (-3) | 13 (+1) | 10 (+0) | 8 (-1) | 11 (+0) | 12 (+1) |

___Pack Tactics.___ The mouse dragon has advantage on an attack roll against
a creature if at least one of the dragon’s allies is within 5 feet of the creature
and the ally isn’t incapacitated.

___Treasure Sense.___ The mouse dragon can pinpoint, by scent, the location
of precious metals and stones, such as coins and gems, within 60 feet of it.

___Underfoot.___ The mouse dragon can attempt to hide even when it is
obscured only by a creature that is at least one size larger than it.

**Actions**

___Multiattack.___ The vulgar mouse dragon makes one bite attack and one
claw attack.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3
(1d4 + 1) slashing damage.
