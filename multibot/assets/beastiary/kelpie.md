"Kelpie";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Sylvan"
_senses_: "blindsight 60 ft."
_skills_: "Perception +3, Stealth +4"
_speed_: "10 ft., swim 30 ft."
_hit points_: "67 (9d8+27)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "blinded, deafened, exhaustion"
_damage_resistances_: "bludgeoning, fire, piercing"
_stats_: | 14 (+2) | 14 (+2) | 16 (+3) | 7 (-2) | 12 (+1) | 10 (0) |

___Source.___ tales from the yawning portal,  page 238

___Amphibious.___ The kelpie can breathe air and water.

___Seaweed Shape.___ The kelpie can use its action to reshape its body into the form of a humanoid or beast that is Small, Medium, or Large. Its statistics are otherwise unchanged. The disguise is convincing, unless the kelpie is in bright light or the viewer is within 30 feet of it, in which case the seams between the seaweed strands are visible. The kelpie returns to its true form if takes a bonus action to do so or if it dies.

___False Appearance.___ While the kelpie remains motionless in its true form, it is indistinguishable from normal seaweed.

**Actions**

___Multiattack.___ The kelpie makes two slam attacks.

___Slam.___ Melee Weapon Attack: +4 to hit, reach 10 ft., one target. Hit: 11 (2d8 + 2) piercing damage. If the target is a Medium or smaller creature, it is grappled (escape DC 12).

___Drowning Hypnosis.___ The kelpie chooses one humanoid it can see within 150 feet of it. If the target can see the kelpie, the target must succeed on a DC 11 Wisdom saving throw or be magically charmed while the kelpie maintains concentration, up to 10 minutes (as if concentrating on a spell).

>The charmed target is incapacitated, and instead of holding its breath underwater, it tries to breathe normally and immediately runs out of breath, unless it can breathe water.

>If the charmed target is more than 5 feet away from the kelpie, the target must move on its turn toward the kelpie by the most direct route, trying to get within 5 feet. It doesn't avoid opportunity attacks.

>Before moving into damaging terrain, such as lava or a pit, and whenever it takes damage from a source other than the kelpie or drowning, the target can repeat the saving throw. A charmed target can also repeat the saving throw at the end of each of its turns. If the saving throw is successful, the effect ends on it.

>A target that successfully saves is immune to this kelpie's hypnosis for the next 24 hours.