"Beggar Ghoul";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Undercommon"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "13 (3d8)"
_armor class_: "12"
_stats_: | 10 (+0) | 15 (+2) | 10 (+0) | 12 (+1) | 11 (+0) | 14 (+2) |

___Pack Tactics.___ The beggar ghoul has advantage on an attack roll against a creature if at least one of the beggar ghoul's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Savage Hunger.___ A beggar ghoul that hits with its bite attack against a creature that hasn't acted yet in this combat scores a critical hit.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) slashing damage. If the target is a creature other than an elf or undead, it must succeed on a DC 10 Constitution saving throw or be paralyzed for 1 minute. A paralyzed target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

