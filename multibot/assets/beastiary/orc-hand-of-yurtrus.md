"Orc Hand of Yurtrus";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "understands Common and Orc but can't speak"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +2, Intimidation +1, Medicine +4, Religion +2"
_speed_: "30 ft."
_hit points_: "30 (4d8+12)"
_armor class_: "12 (hide armor)"
_stats_: | 12 (+1) | 11 (0) | 16 (+3) | 11 (0) | 14 (+2) | 9 (-1) |

___Source.___ Volo's Guide to Monsters, p. 184

___Aggressive.___ As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.

___Spellcasting.___ The orc is a 4th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). It requires no verbal components to cast its spells. The orc has the following cleric spells prepared:

* Cantrips (at-will): _guidance, mending, resistance, thaumaturgy_

* 1st level (4 slots): _bane, detect magic, inflict wounds, protection from evil and good_

* 2nd level (3 slots): _blindness/deafness, silence_

**Actions**

___Touch of the White Hand.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 9 (2d8) necrotic damage.
