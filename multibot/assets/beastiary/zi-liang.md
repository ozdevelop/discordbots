"Zi Liang";;;_size_: Medium humanoid (shou human)
_alignment_: chaotic good
_challenge_: "0 (10 XP)"
_languages_: "Common, Elvish, Goblin"
_skills_: "Acrobatics +4, Athletics +3, Perception +5, Stealth +4"
_speed_: "30 ft."
_hit points_: "22 (5d8)"
_armor class_: "15"
_senses_: "passive Perception 15"
_stats_: | 12 (+1) | 15 (+2) | 11 (0) | 14 (+2) | 16 (+3) | 11 (0) |

___Unarmored Defense.___ While Zi is wearing no armor and wielding no shield, her AC includes her Wisdom modifier.

**Actions**

___Multiattack.___ Zi makes two melee attacks.

___Quarterstaff.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6+1) bludgeoning damage, or 5 (1d8+1) bludgeoning damage if used with both hands.

___Sling.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 4 (1d4+2) bludgeoning damage. Zi carries twenty sling stones.

**Roleplaying** Information

Zi Liang is a devout worshiper of Chauntea, the Earth Mother. She has considerably less faith in Goldenfields' defenders, so she patrols the temple-farm during her off-duty hours.

**Ideal:** "If we faithfully tend to our gardens and our fields, Chauntea will smile upon us."

**Bond:** "Goldenfields is the breadbasket of the North. People depend on its safety and prosperity, and I'll do what must be done to protect it."

**Flaw:** "I don't trust authority. I do what my heart says is right."