"Livestone";;;_size_: Large ooze
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 5"
_damage_immunities_: "acid, cold, fire, poison"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, poisoned, prone"
_speed_: "20 ft."
_hit points_: "168 (16d10 + 80)"
_armor class_: "10"
_stats_: | 20 (+5) | 10 (+0) | 20 (+5) | 2 (-4) | 1 (-5) | 1 (-5) |

___False Appearance.___ While the livestone is solidified and remains
motionless, it is indistinguishable from a typical stone.

___Stone Camouflage.___ The livestone has advantage on Dexterity (Stealth)
checks made to hide in rocky terrain.

**Actions**

___Multiattack.___ The livestone makes two attacks with its pseudopod.

___Pseudopod.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target.
Hit: 14 (2d8 + 5) bludgeoning damage. If the target is Medium or smaller,
it is grappled (escape DC 15) and restrained until the grapple ends. The
livestone can grapple two targets.

___Engulf.___ The livestone engulfs a Medium or smaller creature grappled
by it. The engulfed target is blinded, restrained, and unable to breathe, and
it must succeed on a DC 15 Constitution saving throw at the start of each
of the livestone’s turns or take 14 (2d8 + 5) bludgeoning damage. If the
livestone moves, the engulfed target moves with it. The livestone can have
only one creature engulfed at a time.

**Reactions**

___Solidify.___ As a reaction, the livestone can solidify all or part of itself into
material with the same consistency of solid rock. The livestone adds 4 to
its AC against one melee attack that would hit it. The livestone does not
have to see the attack to use this ability. A livestone cannot take attack or
move actions if its entire form is solidified. A livestone must use an action
to desolidify itself.
