"Baba Lysaga";;;_page_number_: 228
_size_: Medium humanoid (human
_alignment_: shapechanger)
_challenge_: "11 (7,200 XP)"
_languages_: "Abyssal, Common, Draconic, Dwarvish, Giant"
_skills_: "Arcana +13, Religion +13"
_saving_throws_: "Wis +7"
_speed_: "30 ft."
_hit points_: "120 (16d8+48)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 10 (0) | 16 (+3) | 20 (+5) | 17 (+3) | 13 (+1) |

___Shapechanger.___ Baba Lysaga can use an action to polymorph into a swarm of insects (flies) or back into her true form. While in swarm form, she has a walking speed of 5 feet and a flying speed of 30 feet. Anything she is wearing transforms with her, but nothing she is carrying does.

___Blessing of Mother Night.___ Baba Lysaga is shielded against divination magic, as though protected by a nondetection spell.

___Spellcasting.___ Baba Lysaga is a 16th-level spellcaster. Her spellcasting ability is Intelligence (spell save DC 17, +9 to hit with spell attacks). Baba Lysaga has the following wizard spells prepared:

* Cantrips (at will): _acid splash, fire bolt, light, mage hand, prestidigitation_

* 1st level (4 slots): _detect magic, magic missile, sleep, witch bolt_

* 2nd level (3 slots): _crown of madness, enlarge/reduce, misty step_

* 3rd level (3 slots): _dispel magic, fireball, lightning bolt_

* 4th level (3 slots): _blight, Everard's black tentacles, polymorph_

* 5th level (2 slots): _cloudkill, geas, scrying_

* 6th level (1 slot): _programmed illusion, true seeing_

* 7th level (1 slot): _finger of death, mirage arcane_

* 8th level (1 slot): _power word stun_

**Actions**

___Multiattack.___ Baba Lysaga makes three attacks with her quarterstaff

___Quarterstaff.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d6+4) bludegoning damage, or 8 (1d8+4) bludgeoning damage if wielded with two hands.

___Summon Swarms of Insects (Recharges after a Short or Long Rest).___ Baba Lysaga summons 1d4 swarms of insects. A summoned swarm appears in an unoccupied space within 60 feet of Baba Lysaga and acts as her ally. It remains until it dies or until Baba Lysaga dismisses it as an action.
