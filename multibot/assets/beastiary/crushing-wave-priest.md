"Crushing Wave Priest";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Aquan, Common"
_skills_: "Deception +5, Religion +2, Stealth +2"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "13 (chain shirt)"
_stats_: | 15 (+2) | 11 (0) | 14 (+2) | 10 (0) | 11 (0) | 16 (+3) |

___Spellcasting.___ The priest is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _chill touch, mage hand, minor illusion, prestidigitation, ray of frost_

* 1st level (4 slots): _expeditious retreat, ice knife, magic missile, shield_

* 2nd level (3 slots): _blur, hold person_

* 3rd level (2 slots): _sleet storm_

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) bludgeoning damage.
