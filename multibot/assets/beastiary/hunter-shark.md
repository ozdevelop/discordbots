"Hunter Shark";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "darkvision 30 ft."
_skills_: "Perception +2"
_speed_: "swim 40 ft."
_hit points_: "45 (6d10+12)"
_armor class_: "12 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 15 (+2) | 1 (-5) | 10 (0) | 4 (-3) |

___Blood Frenzy.___ The shark has advantage on melee attack rolls against any creature that doesn't have all its hit points.

___Water Breathing.___ The shark can breathe only underwater.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage.