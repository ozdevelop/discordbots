"Zealoraptor Zombie";;;_size_: Large undead
_alignment_: unaligned
_challenge_: "2 (200 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft."
_hit points_: "52 (7d10 + 14)"
_armor class_: "11 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 15 (+2) | 3 (-4) | 8 (-1) | 5 (-3) |

___Pounce.___ If the zombie moves at least 20 feet
straight toward a creature and then hits it with a
claw attack on the same turn, that target must
succeed on a DC 13 Strength saving throw or be
knocked prone. If the target is prone, the zombie
can make one bite attack against it as a bonus
action.

___Undead Fortitude.___ If damage reduces the zombie
to 0 hit points, it must make a Constitution saving
throw with a DC of 5 + the damage taken, unless
the damage is radiant or from a critical hit. On a
success, the zombie drops to 1 hit point instead.

**Actions**

___Multiattack.___ The zombie makes two attacks: one
with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 8 (1d10 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 8 (1d10 + 3) slashing damage, and if
the target is a creature it must succeed on a DC 12
Constitution saving throw or contract Sewer Plague
(see "Diseases" in chapter 8 of the Dungeon
Master’s Guide).
