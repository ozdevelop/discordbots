"Thayan Warrior";;;_size_: Medium humanoid (human)
_alignment_: any non-good alignment
_challenge_: "2 (450 XP)"
_languages_: "Common, Thayan"
_skills_: "Perception +2"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "16 (chain shirt, shield)"
_stats_: | 16 (+3) | 13 (+1) | 14 (+2) | 10 (0) | 11 (0) | 11 (0) |

___Source.___ tales from the yawning portal,  page 246

___Doomvault Devotion.___ Within the Doomvault, the warrior has advantage on saving throws against being charmed or frightened .

___Pack Tactics.___ The warrior has advantage on an attack roll against a creature if at least one of the warrior's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The warrior makes two melee attacks.

___Longsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1dlO + 3) slashing damage if used with two hands.

___Javelin.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 6 (1d6 + 3) piercing damage.