"Mordnaissant";;;_size_: Tiny undead
_alignment_: neutral evil
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 15"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned, unconscious"
_speed_: "5 ft., fly 50 ft."
_hit points_: "67 (15d4 + 30)"
_armor class_: "15"
_stats_: | 3 (-4) | 14 (+2) | 15 (+2) | 7 (-2) | 14 (+2) | 16 (+3) |

___Death Curse.___ If a mordnaissant dies, it releases a death curse on all
creatures within 30 feet of it. Those creatures must make a DC 15 Wisdom
saving throw. On a failed saving throw, the creature cannot regain hit
points from spending hit dice and it has disadvantage on all saving throws
for 24 hours. A remove curse or greater magic is necessary to end this
curse early.

___Shield of Agony.___ The mordnaissant’s Armor Class includes its Charisma
modifier.

**Actions**

___Lash of Fury.___ Ranged Spell Attack: +5 to hit, range 30 ft., one target.
Hit: 24 (5d8 + 2) necrotic damage and the target must make a DC 14
Constitution saving throw or lose 1d4 points of Intelligence. The target
becomes incapacitated if this reduces its Intelligence to 0. The
reduction lasts until the target finishes a long rest.

___Pain Wail.___ The mordnaissant releases a wail. Creatures within 20 feet
that can hear the mordnaissant must make a DC 14 Wisdom saving throw.
On a failed saving throw, the target is stunned until the end of its next turn.
