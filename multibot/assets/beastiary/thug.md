"Thug";;;_size_: Medium humanoid (any race)
_alignment_: any non-good alignment
_challenge_: "1/2 (100 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Intimidation +2"
_speed_: "30 ft."
_hit points_: "32 (5d8+10)"
_armor class_: "11 (leather armor)"
_stats_: | 15 (+2) | 11 (0) | 14 (+2) | 10 (0) | 10 (0) | 11 (0) |

___Pack Tactics.___ The thug has advantage on an attack roll against a creature if at least one of the thug's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack.___ The thug makes two melee attacks.

___Mace.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 5 (1d6 + 2) bludgeoning damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage.