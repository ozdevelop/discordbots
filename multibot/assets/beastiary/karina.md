"Karina";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Elvish, Sylvan"
_skills_: "Animal Handling +7, Insight +9, Perception +9, Persuasion +9, Stealth +8, Survival +9"
_senses_: "darkvision 60 ft., passive Perception 19"
_saving_throws_: "Con +3, Wis +7"
_speed_: "30 ft., fly 60 ft. (in owl form)"
_hit points_: "88 (16d8 + 16)"
_armor class_: "17"
_stats_: | 10 (+0) | 18 (+4) | 13 (+1) | 14 (+2) | 20 (+5) | 20 (+5) |

___Flyby.___ When in owl form, the karina does not provoke opportunity
attacks when it flies out of an enemy’s reach.

___Innate Spellcasting.___ The karina’s spellcasting ability is Wisdom (spell
save DC 15, +7 to hit with spell attacks). The karina can innately cast the
following spells, requiring no material components:

* At will: _chill touch, dancing lights_

* 3/day each: _faerie fire, fog cloud_

* 1/day: _moonbeam_

___Keen Hearing and Sight.___ Karina have advantage on Wisdom
(Perception) checks based on sight or hearing.

___Owl Form.___ As a bonus action when under moonlight, a Karina can
transform either into or from owl form. In owl form, the karina gains fly
60 ft. and natural attacks (see below).

**Actions**

___Multiattack.___ A karina makes two dagger attacks or one bite and one
talon attack.

___Bite (Owl Form Only).___ Melee Weapon Attack: +6 to hit, reach 5 ft., one
target. Hit: 17 (3d8 + 4) piercing damage.

___Dagger (Human Form Only).___ Melee Weapon Attack: +6 to hit, reach 5
ft., one target. Hit: 6 (1d4 + 4) piercing or slashing damage.

___Talons (Owl Form Only).___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 13 (2d8 + 4) slashing damage.
