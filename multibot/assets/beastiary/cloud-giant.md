"Cloud Giant";;;_size_: Huge giant
_alignment_: neutral good (50%) or neutral evil (50%)
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Giant"
_skills_: "Insight +7, Perception +7"
_saving_throws_: "Con +10, Wis +7, Cha +7"
_speed_: "40 ft."
_hit points_: "200 (16d12+96)"
_armor class_: "14 (natural armor)"
_stats_: | 27 (+8) | 10 (0) | 22 (+6) | 12 (+1) | 16 (+3) | 16 (+3) |

___Keen Smell.___ The giant has advantage on Wisdom (Perception) checks that rely on smell.

___Innate Spellcasting.___ The giant's innate spellcasting ability is Charisma. It can innately cast the following spells, requiring no material components:

* At will: _detect magic, fog cloud, light_

* 3/day each: _feather fall, fly, misty step, telekinesis_

* 1/day each: _control weather, gaseous form_

**Actions**

___Fling.___ The giant tries to throw a Small or Medium creature within 10 feet of it. The target must succeed on a DC 20 Dexterity saving throw or be hurled up to 60 feet horizontally in a direction of the giant's choice and land prone, taking 1d8 bludgeoning damage for every 10 feet it was thrown.

___Wind Aura.___ A magical aura of wind surrounds the giant. The aura is a 10-foot-radius sphere that lasts as long as the giant maintains concentration on it (as if concentrating on a spell). While the aura is in effect, the giant gains a +2 bonus to its AC against ranged weapon attacks, and all open flames within the aura are extinguished unless they are magical.

___Multiattack.___ The giant makes two morningstar attacks.

___Morningstar.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 21 (3d8 + 8) piercing damage.

___Rock.___ Ranged Weapon Attack: +12 to hit, range 60/240 ft., one target. Hit: 30 (4d10 + 8) bludgeoning damage.
