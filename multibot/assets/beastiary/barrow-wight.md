"Barrow Wight";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "the languages it knew in life"
_skills_: "Perception +3, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_damage_resistances_: "necrotic; bludgeoning, piercing and slashing from nonmagical weapons that are not silvered"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "14 (studded leather)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 10 (+0) | 13 (+1) | 15 (+2) |

___Gaze of Insanity.___ If a creature starts its turn within 30 feet of the barrow
wight and the two of them can see each other, the barrow wight can force
the creature to make a DC 13 Wisdom saving throw if the barrow wight is
not incapacitated. On a failed save, the creature is affected by a short-term
madness effect for 1 minute. Roll percentile dice and determine the effect from the table below.
* _1-20_ The target retreats into its mind and becomes
paralyzed. The effect ends if the creature takes
any damage.
* _21-30_ The creature is incapacitated, and can only
scream, laugh, or weep hysterically.
* _31-40_ The creature is frightened and must use its actions
to flee from the source of the fear.
* _41-50_ The creature babbles incoherently and cannot
speak normally or cast spells.
* _51-60_ The creature must use its action to attack the
nearest creature.
* _61-70_ The creature hallucinates vividly, incurring
disadvantage on all ability checks.
* _71-75_ The creature does whatever anyone tells it to do
that isn’t obviously self-destructive.
* _76-80_ The creature experiences an overpowering urge
to eat something strange, such as dirt, offal, or
slime.
* _81-90_ The creature is stunned.
* _91-100_ The creature falls unconscious.

The target can repeat the saving throw at the end of each of its turns. A
successful save ends the effect and renders the target immune to the same
barrow wight’s insanity gaze for 24 hours.
A creature that isn’t surprised can avert its eyes to avoid the saving
throw at the start of its turn. If the creature does so, it can’t see the barrow
wight until the start of its next turn, when it can avert its eyes again. If the
creature looks at the barrow wight in the meantime, it must immediately
make the save.

___Sunlight Sensitivity.___ While in sunlight, the wight has disadvantage on
attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Slam.___ Melee weapon attack: +5 to hit, reach 5 ft., one creature. Hit:
6 (1d6+3) bludgeoning damage plus 6 (1d6 + 3) necrotic damage. The
target must succeed on a DC 13 Constitution saving throw or its hit point
maximum is reduced by an amount equal to the necrotic damage taken.

This reduction lasts until the target takes a long rest. The target dies if this
effect reduces their hit point maximum to zero.

A humanoid slain by this attack rises 1d4 rounds later as a barrow wight
under the control of the wight that killed it unless the humanoid is restored
to life or its body is destroyed. The wight can have no more than three
barrow wights under its control at one time.
