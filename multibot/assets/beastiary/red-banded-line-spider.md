"Red-Banded Line Spider";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_skills_: "Perception +2, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "psychic"
_condition_immunities_: "charmed, frightened"
_speed_: "30 ft., climb 30 ft."
_hit points_: "2 (1d4)"
_armor class_: "13"
_stats_: | 4 (-3) | 16 (+3) | 10 (+0) | 1 (-5) | 10 (+0) | 2 (-4) |

___Spider Climb.___ The spider can climb difficult surfaces, including upside down and on ceilings, without needing to make an ability check.

___Web Walker.___ The spider ignores movement restrictions caused by webbing.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 piercing damage, and the target must succeed on a DC 10 Constitution saving throw or take 3 (1d6) poison damage and be poisoned until the start of the spider's next turn. The target fails the saving throw automatically and takes an extra 1d6 poison damage if it is bitten by another red-banded line spider while poisoned this way.

___Swingline.___ Ranged Weapon Attack: +5 to hit, range 60 ft., one target. Hit: the spider immediately moves the full length of the webbing (up to 60 feet) to the target and delivers a bite with advantage. This attack can be used only if the spider is higher than its target and at least 10 feet away.

