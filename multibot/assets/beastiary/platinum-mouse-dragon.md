"Platinum Mouse Dragon";;;_size_: Tiny dragon
_alignment_: chaotic good
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Draconic"
_skills_: "Arcana +4, Nature +4, Perception +8, Stealth +9"
_senses_: "darkvision 30 ft., passive Perception 18"
_saving_throws_: "Dex +6, Con +6, Wis +5, Cha +5"
_damage_immunities_: "acid, fire, lightning"
_speed_: "20 ft. climb 20 ft., burrow 10 ft."
_hit points_: "44 (8d4 + 24)"
_armor class_: "15 (natural armor)"
_stats_: | 7 (-2) | 16 (+3) | 16 (+3) | 12 (+1) | 15 (+2) | 15 (+2) |

___Innate Spellcasting.___ The platinum mouse dragon’s innate spellcasting
ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It can
cast the following spells, requiring no material components.

* At will: _mage hand, minor illusion, silent image_

* 1/day each: _enlarge/reduce, invisibility, major image, see invisibility_

___Pack Tactics.___ The mouse dragon has advantage on an attack roll against
a creature if at least one of the dragon’s allies is within 5 feet of the creature
and the ally isn’t incapacitated.

___Treasure Sense.___ The mouse dragon can pinpoint, by scent, the location
of precious metals and stones, such as coins and gems, within 60 feet of it.

___Underfoot.___ The mouse dragon can attempt to hide even when it is
obscured only by a creature that is at least one size larger than it.

**Actions**

___Multiattack.___ The platinum mouse dragon makes one bite attack and
two claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7
(1d8 + 3) slashing damage.

___Radiant Breath (Recharge 5–6).___ The dragon exhales radiant light in a
20-foot line that is 1 foot wide. Each creature in that line must make a DC
13 Dexterity saving throw, taking 38 (7d10) radiant damage on a failed
saving throw, or half as much damage on a successful one.
