"Oleander Dragon";;;_size_: Large dragon
_alignment_: chaotic neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Draconic, Elven, Sylvan"
_skills_: "Arcana +6, Insight +8, Nature +6, Perception +8, Stealth +6"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 23"
_saving_throws_: "Dex +6, Int +6, Wis +8, Cha +6"
_damage_resistances_: "poison, psychic"
_speed_: "40 ft., fly 40 ft."
_hit points_: "119 (14d10 + 42)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 16 (+3) | 16 (+3) | 20 (+5) | 16 (+3) |

___Shapechanger.___ As an action, the dragon can polymorph
into a Small or Medium humanoid or back
into its true form. Its statistics, other than its size,
are the same in each form. Any worn or carried
equipment isn’t transformed. It reverts to its true
form if it dies.

___Magic Resistance.___ The dragon has advantage
on saving throws against spells and other
magical effects.

___False Appearance.___ While the dragon remains
motionless, it is indistinguishable from a shrub or
small tree of blooming oleander.

**Actions**

___Implant Nightmare.___ The dragon reaches into the
mind of a target it can see within 60 feet and manifests
the target’s deepest fear. The target must
make a DC 16 Wisdom saving throw. On a failed
save, the target becomes frightened for 1 minute.
At the start of each of the target’s turns, the target
must repeat the saving throw or take 22 (4d10)
psychic damage. On a successful save, the nightmare
ends.

___Pollen Breath (Recharge 5–6).___ The dragon exhales
a swirling cloud of pollen and red and purple petals
in a 60-foot cone. Each creature in the area must
make a DC 16 Wisdom saving throw. On a failed
save, the creature becomes charmed until the
end of their next round. Creatures who successfully
save cannot be affected by Pollen Breath for
24 hours.

___Song of Cestilani (1/Day).___ The oleander dragon
sings a magical song. Each enemy that can hear
it must succeed on a DC 16 Intelligence saving
throw or take 22 (4d8 + 4) psychic damage and
be stunned for 1 minute. A creature can repeat this
saving throw at the end of each of its turns, ending
the effect on itself on a success.
