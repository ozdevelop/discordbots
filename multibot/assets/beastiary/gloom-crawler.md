"Gloom Crawler";;;_size_: Huge monstrosity
_alignment_: neutral
_challenge_: "10 (5,900 XP)"
_languages_: "--"
_skills_: "Perception +9, Stealth +11"
_senses_: "darkvision 60 ft., passive Perception 19"
_speed_: "20 ft., climb 20 ft."
_hit points_: "136 (13d12 + 52)"
_armor class_: "17"
_stats_: | 20 (+5) | 24 (+7) | 18 (+4) | 4 (-3) | 12 (+1) | 2 (-4) |

___Hyper-Awareness.___ The gloom crawler has advantage on Wisdom
(Perception) checks and on saving throws against being blinded.

**Actions**

___Multiattack.___ The gloom crawler makes three tentacles attacks and one
bite attack.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 16 (2d8 + 7) piercing damage.

___Tentacles.___ Melee Weapon Attack: +11 to hit, reach 15 ft., one target.
Hit: 23 (3d10 + 7) bludgeoning damage, and the target is grappled (escape
DC 19). At the beginning of the gloom crawler’s turns, it can choose to
pull a grappled creature 15 feet to its mouth or constrict its tentacles to
deal 10 (1d10 + 5) bludgeoning damage to the grappled target. The gloom
crawler can grapple up to three different targets.
