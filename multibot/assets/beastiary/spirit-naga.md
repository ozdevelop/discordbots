"Spirit Naga";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "Abyssal, Common"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Dex +6, Con +5, Wis +5, Cha +6"
_speed_: "40 ft."
_hit points_: "75 (10d10+20)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "charmed, poisoned"
_stats_: | 18 (+4) | 17 (+3) | 14 (+2) | 16 (+3) | 15 (+2) | 16 (+3) |

___Rejuvenation.___ If it dies, the naga returns to life in 1d6 days and regains all its hit points. Only a wish spell can prevent this trait from functioning.

___Spellcasting.___ The naga is a 10th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks), and it needs only verbal components to cast its spells. It has the following wizard spells prepared:

* Cantrips (at will): _mage hand, minor illusion, ray of frost_

* 1st level (4 slots): _charm person, detect magic, sleep_

* 2nd level (3 slots): _detect thoughts, hold person_

* 3rd level (3 slots): _lightning bolt, water breathing_

* 4th level (3 slots): _blight, dimension door_

* 5th level (2 slots): _dominate person_

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one creature. Hit: 7 (1d6 + 4) piercing damage, and the target must make a DC 13 Constitution saving throw, taking 31 (7d8) poison damage on a failed save, or half as much damage on a successful one.
