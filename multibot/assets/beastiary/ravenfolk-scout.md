"Ravenfolk Scout";;;_size_: Medium humanoid
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Feather Speech, Huginn"
_skills_: "Deception +3, Perception +6, Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 16"
_saving_throws_: "Dex+4, Con +1, Wis +4, Cha +3"
_speed_: "30 ft."
_hit points_: "21 (6d8 . 6)"
_armor class_: "14 (studded leather armor)"
_stats_: | 10 (+0) | 14 (+2) | 8 (-1) | 10 (+0) | 15 (+2) | 12 (+1) |

___Mimicry.___ Ravenfolk scouts can mimic the voices of others with uncanny accuracy. They have advantage on Charisma (Deception) checks involving audible mimicry.

**Actions**

___Multiattack.___ The ravenfolk scout makes one peck attack and one other melee or ranged attack.

___Ghost Wings.___ The ravenfolk scout furiously "beats" a set of phantasmal wings. Every creature within 5 feet of the ravenfolk must make a successful DC 12 Dexterity saving throw or be blinded until the start of the ravenfolk's next turn.

___Longbow.___ Ranged Weapon Attack. +4 to hit, range 150/600 ft., one target. Hit: 6 (1d8 +2) piercing damage.

___Peck.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Rapier.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8+ 2) piercing damage.

