"Crimson Drake";;;_size_: Tiny dragon
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic, telepathy 60 ft."
_skills_: "Acrobatics +4, Perception +1"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +4"
_damage_immunities_: "fire"
_condition_immunities_: "paralyzed, unconscious"
_speed_: "15 ft., fly 80 ft."
_hit points_: "54 (12d4 + 24)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 14 (+2) | 14 (+2) | 8 (-1) | 9 (-1) | 14 (+2) |

___Magic Resistance.___ The drake has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The crimson drake makes one bite attack and one stinger attack.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage plus 4 (1d8) fire damage.

___Stinger.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or become poisoned for 1 hour. If the saving throw fails by 5 or more, the target takes 2 (1d4) poison damage at the start of each of its turns for 3 rounds. The target may repeat the saving throw at the end of its turn to end the effect early.

___Breath Weapon (Recharge 6).___ The drake exhales fire in a 15-ft. cone. Each target in that cone takes 18 (4d8) fire damage, or half damage with a successful DC 12 Dexterity saving throw.

