"Homunculus";;;_size_: Tiny construct
_alignment_: neutral
_challenge_: "0 (10 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "20 ft., fly 40 ft."
_hit points_: "5 (2d4)"
_armor class_: "13 (natural armor)"
_condition_immunities_: "charmed, poisoned"
_stats_: | 4 (-3) | 15 (+2) | 11 (0) | 10 (0) | 10 (0) | 7 (-2) |

___Telepathic Bond.___ While the homunculus is on the same plane of existence as its master, it can magically convey what it senses to its master, and the two can communicate telepathically.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 1 piercing damage, and the target must succeed on a DC 10 Constitution saving throw or be poisoned for 1 minute. If the saving throw fails by 5 or more, the target is instead poisoned for 5 (1d10) minutes and unconscious while poisoned in this way.