"Firegeist";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Primordial"
_skills_: "Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_immunities_: "fire, poison"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "40 ft."
_hit points_: "87 (25d6)"
_armor class_: "14"
_stats_: | 7 (-2) | 18 (+4) | 10 (+0) | 4 (-3) | 16 (+3) | 6 (-2) |

___Hide By Firelight.___ In an area lit only by nonmagical flame, a Firegeist gains a +2 bonus on Stealth checks. This becomes +4 when hiding within the fire.

___Illumination.___ The firegeist sheds dim light in a 30-foot radius.

___Magical Light Sensitivity.___ While in magical light, the firegeist has disadvantage on attack rolls and ability checks.

___Water Susceptibility.___ For every 5 feet the firegeist moves in water, or for every gallon of water splashed on it, it takes 3 cold damage.

**Actions**

___Multiattack.___ The firegeist makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage.

___Combustion Touch (Recharge 5-6).___ The firegeist may ignite a target when making a slam attack. The target must immediately succeed at a DC 13 Dexterity saving throw or catch fire, taking an additional 5 (1d10) fire damage at the beginning of its next turn. Until someone takes an action to douse the fire, the creature takes 5 (1d10) fire damage at the start of each of its turns.

