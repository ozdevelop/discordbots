"Barbed Devil";;;_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_skills_: "Deception +5, Insight +5, Perception +8"
_damage_immunities_: "fire, poison"
_saving_throws_: "Str +6, Con +7, Wis +5, Cha +5"
_speed_: "30 ft."
_hit points_: "110 (13d8+52)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 16 (+3) | 17 (+3) | 18 (+4) | 12 (+1) | 14 (+2) | 14 (+2) |

___Barbed Hide.___ At the start of each of its turns, the barbed devil deals 5 (1d10) piercing damage to any creature grappling it.

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The devil makes three melee attacks: one with its tail and two with its claws. Alternatively, it can use Hurl Flame twice.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft ., one target. Hit: 6 (1d6 + 3) piercing damage.

___Tail.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage.

___Hurl Flame.___ Ranged Spell Attack: +5 to hit, range 150 ft., one target. Hit: 10 (3d6) fire damage. If the target is a flammable object that isn't being worn or carried, it also catches fire.