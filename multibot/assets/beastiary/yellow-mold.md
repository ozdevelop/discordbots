"Yellow Mold";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, frightened, poisoned, prone, stunned"
_speed_: "0 ft."
_hit points_: "5 (1d6 + 2)"
_armor class_: "5"
_stats_: | 1 (-5) | 1 (-5) | 15 (+2) | 1 (-5) | 10 (+0) | 1 (-5) |

___Light Sensitivity.___ Sunlight or any amount of fire damage instantly destroys one patch of yellow mold.

___Poison Spores.___ If touched, the yellow mold ejects a cloud of spores that fills a 10-foot cube originating from the mold.  
Each creature in the area must succeed on a DC 15 Constitution saving throw or take 11 (2d10) poison damage and become poisoned for 1 minute.  
While poisoned in this way, the creature takes 5 (1d10) poison damage at the start of each of its turns.  The creature can repeat the saving throw at the end of each of its turns, 
ending the effect on itself on a successful save.
