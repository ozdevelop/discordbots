"Hulking Whelp";;;_size_: Small fey
_alignment_: chaotic neutral
_challenge_: "5 (1800 XP)"
_languages_: "--"
_senses_: "impaired sight 30 ft., passive Perception 12"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "40 ft."
_hit points_: "94 (9d12 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 21 (+5) | 10 (+0) | 18 (+4) | 7 (-2) | 14 (+2) | 9 (-1) |

___Calm State.___ When a hulking whelp is calm and unafraid, it uses the following statistics instead of those listed above: Size Small; HP 9 (6d6 . 12); Speed 20 ft.; STR 8 (-1); CON 6 (-2); Languages Common, Sylvan

___Poor Senses.___ A hulking whelp has poor hearing and is nearsighted. It can see in normal or dim light up to 30 feet and hear sounds from up to 60 feet away.

___Unleashed Emotion.___ When a hulking whelp feels threatened - it's touched, intimidated, cornered, attacked, or even just if a stranger moves adjacent to the whelp - it immediately grows from size Small to Huge as a reaction. If the whelp was attacked, this reaction occurs after the attack is made but before damage is done. Nearby creatures and objects are pushed to the nearest available space and must make a successful DC 15 Strength saving throw or fall prone. Weapons, armor, and other objects worn or carried by the hulking whelp grow (and shrink again) proportionally when it changes size. Overcome by raw emotion, it sets about destroying anything and everything it can see (which isn't much) and reach (which is quite a lot). The transformation lasts until the hulking whelp is unaware of any nearby creatures for 1 round, it drops to 0 hit points, it has 5 levels of exhaustion, or it's affected by a calm emotions spell or comparable magic. The transformation isn't entirely uncontrollable; people or creatures the whelp knows and trusts can be near it without triggering the reaction. Under the wrong conditions, such as in a populated area, a hulking whelp's Unleashed Emotion can last for days.

**Actions**

___Multiattack.___ The hulking whelp makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 15 ft., one target. Hit: 18 (3d8 + 5) bludgeoning damage.

**Reactions**

___Quick Step.___ A hulking whelp can move 20 feet as a reaction when it is attacked. No opportunity attacks are triggered by this move.

