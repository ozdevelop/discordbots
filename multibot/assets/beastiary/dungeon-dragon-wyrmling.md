"Dungeon Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral
_challenge_: "4 (1,100 XP)"
_languages_: "all"
_skills_: "Investigation +6, Perception +6, Survival +4"
_senses_: "blindsight 10 ft., darkvision 30 ft., passive Perception 16"
_saving_throws_: "Dex +2, Con +5, Wis +4, Cha +4"
_speed_: "30 ft., fly 60 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 10 (+0) | 17 (+3) | 14 (+2) | 15 (+2) | 15 (+2) |

___Innate Spellcasting.___ The dungeon dragon’s innate spellcasting ability
is Charisma (spell save DC 12, +4 to hit with spell attacks). It can cast
the following spells, requiring no material components.

* At will: _alarm, arcane lock, detect magic, identify, knock_

* 1/day each: _find traps, scrying, stone shape, wall of stone_

___Labyrinthine Recall.___ The dungeon dragon can perfectly recall any path
it has traveled.

___Trap Master.___ The dungeon dragon has advantage on saving throws
against the effects of traps.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9
(1d10 + 4) piercing damage.

___Confusion Breath (Recharge 5–6).___ The dragon exhales gas in a
15-foot cone that spreads around corners. Each creature in that area
must succeed on a DC 13 Constitution saving throw. On a failed save,
the creature is confused for 1 minute. While confused, the creature
uses its action to Dash in a random direction, even if that movement
takes the creature into dangerous areas. A confused creature can
repeat the saving throw at the end of each of its turns, ending the
effect on a success.
