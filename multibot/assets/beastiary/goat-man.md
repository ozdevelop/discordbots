"Goat-Man";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Giant, Trollkin, but cannot speak"
_skills_: "Acrobatics +4, Athletics +6, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +4"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "charmed, frightened"
_speed_: "40 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 14 (+2) | 10 (+0) | 13 (+1) | 8 (-1) |

___Headbutt.___ If the goat-man moves at least 10 feet straight toward a creature and then hits it with a slam attack on the same turn, the target must succeed on a DC 14 Strength saving throw or be knocked prone and stunned for 1 round. If the target is prone, the goat-man can make one bite attack against it immediately as a bonus action.

**Actions**

___Multiattack.___ The goat-man makes one bite attack and one slam attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.

