"Pillar of Water";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Aquan"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Str +7, Con +9"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "149 (13d10 + 78)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 5 (-3) | 22 (+6) | 11 (+0) | 15 (+2) | 11 (+0) |

___Constant Flood.___ Enemies beginning their turn
within 15 feet of the Pillar must make a DC 15
Strength check or be pushed 10 feet away from
the Pillar. The ground within 15 feet of the Pillar
is difficult terrain for all enemies attempting to
move toward the Pillar.

___Geyser.___ When summoned, the Pillar of Water
appears in a 10-by-10-foot space. All creatures in
that area must make Dexterity saving throws. On
a failure, a creature is suspended in the column
of water. On a success, a creature is pushed to an
empty space adjacent to the Pillar.

Up to four Medium creatures, or one Large
creature, can be contained in the Pillar. Creatures
suspended in the Pillar can’t breathe unless they
can breathe water, are restrained, and take 21
(6d6) bludgeoning damage at the start of each
of the Pillar’s turns.

As an action, a restrained creature can make a
DC 15 Strength (Athletics) check. On a success, it
escapes from the column of water.

**Actions**

___Reform.___ As a bonus action, the Pillar disappears,
depositing any suspended creatures on the
ground, prone. On its summoner’s next turn, it
appears again anywhere within 60 feet of the
summoner.
