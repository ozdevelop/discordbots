"Mammoth";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_speed_: "40 ft."
_hit points_: "126 (11d12+55)"
_armor class_: "13 (natural armor)"
_stats_: | 24 (+7) | 9 (-1) | 21 (+5) | 3 (-4) | 11 (0) | 6 (-2) |

___Trampling Charge.___ If the mammoth moves at least 20 ft. straight toward a creature and then hits it with a gore attack on the same turn, that target must succeed on a DC 18 Strength saving throw or be knocked prone. If the target is prone, the mammoth can make one stomp attack against it as a bonus action.

**Actions**

___Gore.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 25 (4d8 + 7) piercing damage.

___Stomp.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one prone creature. Hit: 29 (4d10 + 7) bludgeoning damage.