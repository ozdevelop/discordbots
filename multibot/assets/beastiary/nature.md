"Nature";;;_size_: Large construct
_alignment_: lawful neutral
_challenge_: "10 (5,900 XP)"
_languages_: "all those of the creature who summoned it"
_senses_: "truesight 60 ft., passive Perception 14"
_saving_throws_: "Str +9, Dex +4, Con +8, Wis +8, Cha +9"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "190 (20d10 + 80)"
_armor class_: "20 (natural armor)"
_stats_: | 20 (+5) | 11 (+0) | 18 (+4) | 15 (+2) | 18 (+4) | 20 (+5) |

___The Law of Nature.___ Allies within 20 feet have
advantage on all saving throws.

___Inexorable.___ The Inexorables are immune to any
effects that would slow them or deny them
actions or movement.

___Innate Spellcasting.___ Nature’s innate spellcasting
ability is Charisma (spell save DC 17). It can
innately cast the following spells, requiring no
material components:

* 3/day: _banishment, counterspell_

___By Will Alone.___ As a reaction to missing an attack,
Nature makes a DC 15 Wisdom saving throw. On
a successful save, the original attack is successful.

**Actions**

___Multiattack.___ Nature makes two slam attacks or
makes one slam attack and casts banishment.

___Slam.___ Melee Weapon Attack: +9 to hit, reach 10 ft.,
one target. Hit: 12 (2d6 + 5) bludgeoning damage
plus 18 (4d8) force damage.
