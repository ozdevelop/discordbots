"Leaf Sorcerer";;;_size_: Tiny fey
_alignment_: chaotic good
_challenge_: "1/4 (50 XP)"
_languages_: "Sylvan"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "10 ft., fly 30 ft."
_hit points_: "10 (4d6 - 4)"
_armor class_: "15"
_stats_: | 4 (-3) | 20 (+5) | 8 (-1) | 10 (+0) | 10 (+0) | 12 (+1) |

___Leaf Camouflage.___ While unmoving, the sorcerer is
indistinguishable from an ordinary cluster of leaves.

___Innate Spellcasting.___ The sorcerer’s innate
spellcasting ability is Charisma (spell save DC 11).
It can innately cast the following spells, requiring no
components:

* 1/day each: _animal friendship, color spray, fog cloud_

**Actions**

___Razor Leaf.___ Ranged Spell Attack: +3 to hit, range
20/60 ft., one target. Hit: 4 (1d6 + 1) slashing
damage.
