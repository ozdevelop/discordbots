"Nightwalker";;;_page_number_: 216
_size_: Huge undead
_alignment_: chaotic evil
_challenge_: "20 (25,000 XP)"
_languages_: "-"
_senses_: "darkvision 120 ft., passive Perception 9"
_damage_immunities_: "necrotic, poison"
_saving_throws_: "Con +13"
_speed_: "40 ft., fly 40 ft."
_hit points_: "297  (22d12 + 154)"
_armor class_: "14"
_condition_immunities_: "exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 22 (+6) | 19 (+4) | 24 (+7) | 6 (-2) | 9 (0) | 8 (-1) |

___Annihilating Aura.___ Any creature that starts its turn within 30 feet of the nightwalker must succeed on a DC 21 Constitution saving throw or take 14 (4d6) necrotic damage and grant the nightwalker advantage on attack rolls against it until the start of the creature's next turn. Undead are immune to this aura.

___Life Eater.___ A creature reduced to 0 hit points from damage dealt by the nightwalker dies and can't be revived by any means short of a wish spell.

**Actions**

___Multiattack___ The nightwalker uses Enervating Focus twice, or it uses Enervating Focus and Finger of Doom, if available.

___Enervating Focus___ Melee Weapon Attack: +12 to hit, reach 15 ft., one target. Hit: 28 (5d8 + 6) necrotic damage. The target must succeed on a DC 21 Constitution saving throw or its hit point maximum is reduced by an amount equal to the necrotic damage taken. This reduction lasts until the target finishes a long rest.

___Finger of Doom (Recharge 6)___ The nightwalker points at one creature it can see within 300 feet of it. The target must succeed on a DC 21 Wisdom saving throw or take 26 (4d12) necrotic damage and become frightened until the end of the nightwalker's next turn. While frightened in this way, the creature is also paralyzed. If a target's saving throw is successful, the target is immune to the nightwalker's Finger of Doom for the next 24 hours.