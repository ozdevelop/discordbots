"Sarcophagus Slime";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "5 (1800 XP)"
_languages_: "understands the languages of its creator but can't speak"
_skills_: "Stealth +4"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 11"
_saving_throws_: "Wis +4, Cha +4"
_damage_immunities_: "poison"
_damage_resistances_: "acid, necrotic"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, poisoned, prone"
_speed_: "20 ft."
_hit points_: "105 (14d8 + 42)"
_armor class_: "11"
_stats_: | 14 (+2) | 12 (+1) | 18 (+4) | 3 (-4) | 12 (+1) | 12 (+1) |

___Amorphous.___ The sarcophagus slime can move through a space as narrow as 1 inch wide without squeezing.

**Actions**

___Multiattack.___ The sarcophagus slime uses its Frightful Presence, uses its Corrupting Gaze, and makes two slam attacks.

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage plus 14 (4d6) acid damage.

___Frightful Presence.___ Each creature of the sarcophagus slime's choice that is within 60 feet of the sarcophagus slime and aware of it must succeed on a DC 15 Wisdom saving throw or become frightened for 1 minute. A frightened creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the sarcophagus slime's Frightful Presence for the next 24 hours.

___Corrupting Gaze.___ The sarcophagus slime targets one creature it can see within 30 feet of it. If the target can see the sarcophagus slime, the target must succeed on a DC 15 Constitution saving throw or take 14 (4d6) necrotic damage and its hit point maximum is reduced by an amount equal to the necrotic damage taken. If this effect reduces a creature's hit point maximum to 0, the creature dies and its corpse becomes a sarcophagus slime within 24 hours. This reduction lasts until the creature finishes a long rest or until it is affected by greater restoration or comparable magic.

