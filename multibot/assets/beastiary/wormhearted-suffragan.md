"Wormhearted Suffragan";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "the languages it knew in life"
_skills_: "Medicine +6, Religion +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_vulnerabilities_: "radiant"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: " 30 ft."
_hit points_: " 97 (13d8 + 39)"
_armor class_: " 12"
_stats_: | 10 (+0) | 14 (+2) | 16 (+3) | 11 (+0) | 16 (+3) | 8 (-1) |

___Innate Spellcasting.___ The wormhearted suffragan's innate spellcasting ability is Wisdom (spell save DC 14, +6 to hit with spell attacks). It can cast the following spells, requiring no material components:

At will: command, detect evil and good

4/day: inflict wounds

2/day each: blindness-deafness, hold person

1/day each: animate dead, speak with dead

**Actions**

___Multiattack.___ The wormhearted suffragan can make two helminth infestation attacks, or it can cast one spell and makeone helminth infestation attack.

___Helminth Infestation.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (2d6) bludgeoning damage plus 10 (3d6) necrotic damage. If the target is a creature, it must succeed on a DC 14 Constitution saving throw or be afflicted with a helminth infestation (parasitic worms). An afflicted creature can't regain hit points and its hit point maximum decreases by 10 (3d6) for every 24 hours that elapse. If the affliction reduces the target's hit point maximum to 0, the victim dies. The affliction lasts until removed by any magic that cures disease.

