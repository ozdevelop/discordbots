"Demogorgon";;;_page_number_: 144
_size_: Huge fiend (demon)
_alignment_: chaotic evil
_challenge_: "26 (90,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 29"
_skills_: "Insight +11, Perception +19"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_saving_throws_: "Dex +10, Con +16, Wis +11, Cha +15"
_speed_: "50 ft., swim 50 ft."
_hit points_: "406  (28d12 +224)"
_armor class_: "22 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 29 (+9) | 14 (+2) | 26 (+8) | 20 (+5) | 17 (+3) | 25 (+7) |

___Innate Spellcasting.___ Demogorgon's spellcasting ability is Charisma (spell save DC 23). Demogorgon can innately cast the following spells, requiring no material components:

* At will: _detect magic, major image_

* 3/day each: _dispel magic, fear, telekinesis_

* 1/day each: _feeblemind, project image_

___Legendary Resistance (3/Day).___ If Demogorgon fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Demogorgon has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Demogorgon's weapon attacks are magical.

___Two Heads.___ Demogorgon has advantage on saving throws against being blinded, deafened, stunned, or knocked unconscious.

**Actions**

___Multiattack___ Demogorgon makes two tentacle attacks.

___Tentacle___ Melee Weapon Attack: +17 to hit, reach 10 ft., one target. Hit: 28 (3d12 + 9) bludgeoning damage. If the target is a creature, it must succeed on a DC 23 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if its hit point maximum is reduced to 0.

___Gaze___ Demogorgon turns his magical gaze toward one creature that he can see within 120 feet of him. That target must make a DC 23 Wisdom saving throw. Unless the target is incapacitated, it can avert its eyes to avoid the gaze and to automatically succeed on the save. If the target does so, it can't see Demogorgon until the start of his next turn. If the target looks at him in the meantime, it must immediately make the save.
If the target fails the save, the target suffers one of the following effects of Demogorgon's choice or at random:

1. Beguiling Gaze. The target is stunned until the start of Demogorgon's next turn or until Demogorgon is no longer within line of sight.

2. Hypnotic Gaze. The target is charmed by Demogorgon until the start of Demogorgon's next turn. Demogorgon chooses how the charmed target uses its actions, reactions, and movement. Because this gaze requires Demogorgon to focus both heads on the target, he can't use his Maddening Gaze legendary action until the start of his next turn.

3. Insanity Gaze. The target suffers the effect of the confusion spell without making a saving throw. The effect lasts until the start of Demogorgon's next turn. Demogorgon doesn't need to concentrate on the spell.

**Legendary** Actions

Demogorgon can take 2 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Demogorgon regains spent legendary actions at the start of his turn.

___Tail___ Melee Weapon Attack: +17 to hit, reach 15 ft., one target. Hit: 20 (2d10 + 9) bludgeoning damage plus 11 (2d10) necrotic damage.

___Maddening Gaze___ Demogorgon uses his Gaze action, and must choose either the Beguiling Gaze or the Insanity Gaze effect.
