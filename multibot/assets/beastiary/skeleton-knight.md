"Skeleton Knight";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "understand its master’s language in life but cannot speak"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_vulnerabilities_: "bludgeoning"
_damage_immunities_: "poison"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "37 (5d8 + 15)"
_armor class_: "15 (armor scraps)"
_stats_: | 13 (+1) | 15 (+2) | 17 (+3) | 7 (-2) | 8 (-1) | 6 (-2) |

**Actions**

___Melee Weapon.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target.
Hit: varies, damage is determined by weapon. Battleaxe or longsword: 5
(1d8 + 1) slashing damage, or 6 (1d10 + 1) slashing damage if used with
two hands. Greatsword: 9 (2d6 + 1) slashing damage.
