"Sir Pelliton";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common"
_skills_: "Animal Handling +6, Arcana +5, Intimidation +8"
_senses_: "passive Perception 12"
_saving_throws_: "Dex +3, Int +5"
_damage_resistances_: "cold, fire, lightning, psychic"
_condition_immunities_: "blinded, deafened"
_speed_: "30 ft."
_hit points_: "76 (9d10 + 27)"
_armor class_: "20 (plate, shield)"
_stats_: | 18 (+4) | 8 (-1) | 17 (+3) | 13 (+1) | 15 (+2) | 18 (+4) |

___Mounted Combatant.___ While mounted, Sir Pelliton
has advantage on melee attack rolls against
unmounted creatures that are smaller than
his mount.

___The Star Knight.___ Sir Pelliton has resistance to
cold, fire, lightning, and psychic damage as a sign
of his pact, and cannot be blinded or deafened.
His hellish rebuke does 20 damage (no roll) to
enemies who fail their save.

___Innate Spellcasting.___ Sir Pelliton has gained the
power to cast dark magic. All spells he casts
with this feature are cast as if using a 5th-level
spell slot, and he casts cantrips as if he were
an 11th-level warlock. His spellcasting ability is
Charisma (DC 16, +8 to hit with spell attacks).

* At will: _acid splash, eldritch blast (3 beams), minor illusion, hellish rebuke_

* 5/day: _branding smite, black tentacles, counterspell, dimension door, hold person_

* 1/day each: _cone of cold, eyebite, mass suggestion_


**Actions**

___Multiattack.___ Sir Pelliton makes three melee
attacks or makes two melee attacks and casts
eldritch blast.

___Frost Brand Longsword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4)
slashing damage plus 3 (1d6) cold damage.

___Lance.___ Melee Weapon Attack: +8 to hit, reach 10
ft., one target. Hit: 10 (1d12 + 4) piercing damage. 

**Reactions**

___Villain Ability: Warlord.___ As a reaction, when
a minion dies, Sir Pelliton can issue a command to his other
minions. Those who can hear him gain a reaction
they can use to immediately take another
movement.
