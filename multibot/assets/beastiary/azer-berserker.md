"Azer Berserker";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "3 (700 XP)"
_languages_: "Ignan"
_skills_: "Perception +3, Athletics +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "68 (8d10 + 24)"
_armor class_: "15 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 16 (+3) | 10 (+0) | 12 (+1) | 10 (+0) |

___Heated Body.___ A creature that touches the azer or
hits it with a melee attack while within 5 feet of it
takes 5 (1d10) fire damage.

___Heated Weapons.___ When the azer hits with a metal
melee weapon, it deals an extra 4 (1d8) fire damage
(included in the attack).

___Illumination.___ The azer sheds bright light in a 10-foot
radius and dim light for an additional 10 feet.

**Actions**

___Multiattack.___ The azer makes two attacks with its
maul.

___Maul.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 10 (2d6 + 3) bludgeoning damage
plus 4 (1d8) fire damage.

___Rampage (1/Day).___ The damage from heated
weapons increases to 9 (2d8) this round and the
azer immediately makes an attack against each
enemy creature within 5 feet, making a separate
attack roll for each target.
