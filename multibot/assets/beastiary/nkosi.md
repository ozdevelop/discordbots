"Nkosi";;;_size_: Medium humanoid
_alignment_: nkosi), lawful neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Survival +2"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "11 (2d8 + 2)"
_armor class_: "15 (studded leather)"
_stats_: | 16 (+3) | 16 (+3) | 12 (+1) | 10 (+0) | 10 (+0) | 8 (-1) |

___Shapechanger.___ The nkosi can use its action to polymorph into a Medium lion or back into its true form. While in lion form, the nkosi can't speak, and its speed is 50 feet. Other than its speed, its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Keen Smell.___ The nkosi has advantage on Wisdom (Perception) checks that rely on smell.

___Hunter's Maw.___ If the nkosi moves at least 20 feet straight toward a creature and then hits it with a scimitar attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is knocked prone, the nkosi can immediately make one bite attack against it as a bonus action.

**Actions**

___Scimitar (Nkosi Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Mambele Throwing Knife (Nkosi Form Only).___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

