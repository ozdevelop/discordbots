"Cadaver Collector";;;_page_number_: 122
_size_: Large construct
_alignment_: lawful evil
_challenge_: "14 (11500 XP)"
_languages_: "understands all languages but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "necrotic, poison, psychic; bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_speed_: "30 ft."
_hit points_: "189  (18d10 + 90)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 21 (+5) | 14 (+2) | 20 (+5) | 5 (-2) | 11 (0) | 8 (-1) |

___Magic Resistance.___ The cadaver collector has advantage on saving throws against spells and other magical effects.

___Summon Specters (Recharges after a Short or Long Rest).___ As a bonus action, the cadaver collector calls up the enslaved spirits of those it has slain; 1d6 specters (without Sunlight Sensitivity) arise in unoccupied spaces within 15 feet of the cadaver collector. The specters act right after the cadaver collector on the same initiative count and fight until they're destroyed. They disappear when the cadaver collector is destroyed.

**Actions**

___Multiattack___ The cadaver collector makes two slam attacks.

___Slam___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 18 (3d8 + 5) bludgeoning damage plus 16 (3d10) necrotic damage.

___Paralyzing Breath (Recharge 5-6)___ The cadaver collector releases paralyzing gas in a 30-foot cone. Each creature in that area must make a successful DC 18 Constitution saving throw or be paralyzed for 1 minute. A paralyzed creature repeats the saving throw at the end of each of its turns, ending the effect on itself with a success.