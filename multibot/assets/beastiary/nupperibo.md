"Nupperibo";;;_page_number_: 168
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "1/2 (100 XP)"
_languages_: "understands Infernal but can't speak"
_senses_: "blindsight 10 ft. (blind beyond this radius), passive Perception 11"
_skills_: "Perception +1"
_damage_immunities_: "fire, poison"
_speed_: "20 ft."
_hit points_: "11  (2d8 + 2)"
_armor class_: "13 (natural armor)"
_condition_immunities_: "blinded, charmed, frightened, poisoned"
_damage_resistances_: "acid, cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 16 (+3) | 11 (0) | 13 (+1) | 3 (-3) | 8 (-1) | 1 (-4) |

___Cloud of Vermin.___ Any creature, other than a devil, that starts its turn within 20 feet of the nupperibo must make a DC 11 Constitution saving throw. A creature within the areas of two or more nupperibos makes the saving throw with disadvantage. On a failure, the creature takes 2 (1d4) piercing damage.

___Hunger-Driven.___ In the Nine Hells, the nupperibos can flawlessly track any creature that has taken damage from any nupperibo's Cloud of Vermin within the previous 24 hours.

**Actions**

___Bite___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.