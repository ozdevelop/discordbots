"Dragonfang";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Draconic, Infernal"
_skills_: "Deception +4, Stealth +5"
_saving_throws_: "Wis +3"
_speed_: "30 ft."
_hit points_: "78 (12d8+24)"
_armor class_: "15 (studded leather)"
_damage_resistances_: "one of the following: acid, cold, fire, lightning or poison"
_stats_: | 11 (0) | 16 (+3) | 14 (+2) | 12 (+1) | 12 (+1) | 14 (+2) |

___Dragon Fanatic.___ The dragonfang has advantage on saving throws against being charmed or frightened. While the dragonfang can see a dragon or higher-ranking Cult of the Dragon cultist friendly to it, the dragonfang ignores the effects of being charmed or frightened.

___Fanatic Advantage.___ Once per turn, if the dragonfang makes a weapon attack with advantage on the attack roll and hits, the target takes an extra 10 (3d6) damage.

___Limited Flight.___ The dragonfang can use a bonus action to gain a flying speed of 30 feet until the end of its turn.

**Actions**

___Multiattack.___ The Dragonfang attacks twice with its shortsword.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 7 (2d6) damage of the type to which the dragonfang has resistance.

___Orb of Dragon's Breath (2/Day).___ Ranged Spell Attack: +5 to hit, range 50 ft., one target. Hit: 22 (5d8) damage of the type to which the dragonfang has damage resistance.