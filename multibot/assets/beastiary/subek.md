"Subek";;;_size_: Large humanoid
_alignment_: lawful neutral
_challenge_: "5 (1800 XP)"
_languages_: "Common"
_skills_: "History +5, Investigation +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "30 ft., swim 20 ft."
_hit points_: "76 (8d10 + 32)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 10 (+0) | 18 (+4) | 14 (+2) | 13 (+1) | 13 (+1) |

___Hold Breath.___ The subek can hold its breath for 15 minutes.

___Flood Fever.___ During flood season, the subek is overcome with bloodthirsty malice. Its alignment shifts to chaotic evil, it gains the Blood Frenzy trait, and it loses the capacity to speak Common and its bonuses to History and Investigation.

___Blood Frenzy.___ The subek has advantage on melee attack rolls against any creature that doesn't have all its hit points.

**Actions**

___Multiattack.___ The subek makes one bite attack and one claws attack. If both attacks hit the same target, the subek can make a thrash attack as a bonus action against that target.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 11 (2d6 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 22 (4d8 + 4) slashing damage.

___Thrash.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d10) slashing damage.

