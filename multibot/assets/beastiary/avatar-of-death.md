"Avatar of Death";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "00"
_languages_: "all languages known to its summoner"
_senses_: "darkvision 60 ft., truesight 60 ft."
_damage_immunities_: "necrotic, poison"
_speed_: "60 ft., fly 60 ft. (hover)"
_hit points_: "0"
_armor class_: "20"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned , unconscious"
_stats_: | 16 (+3) | 16 (+3) | 16 (+3) | 16 (+3) | 16 (+3) | 16 (+3) |

___Incorporeal Movement.___ The avatar can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Turning Immunity.___ The avatar is immune to features that turn undead.

___Hit Points.___ The Avatar of Death appears with hit points equal to half its summoner's maximum hit points.

**Actions**

___Reaping Scythe.___ The avatar sweeps its spectral scythe through a creature within 5 feet of it, dealing 7 (1d8 + 3) slashing damage plus 4 (1d8) necrotic damage.