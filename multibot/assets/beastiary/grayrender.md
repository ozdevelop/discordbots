"Gray Render";;;_page_number_: 209
_size_: Large monstrosity
_alignment_: chaotic neutral
_challenge_: "12 (8400 XP)"
_languages_: "-"
_senses_: "darkvision 60 ft., passive Perception 12"
_skills_: "Perception +2"
_saving_throws_: "Str +8, Con +9"
_speed_: "30 ft."
_hit points_: "189  (18d10 + 90)"
_armor class_: "19 (natural armor)"
_stats_: | 19 (+4) | 13 (+1) | 20 (+5) | 3 (-3) | 6 (-2) | 8 (-1) |

**Actions**

___Multiattack___ The gray render makes three attacks: one with its bite and two with its claws.

___Bite___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 17 (2d12 + 4) piercing damage. If the target is Medium or smaller, the target must succeed on a DC 16 Strength saving throw or be knocked prone.

___Claws___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) slashing damage. If the target is prone an additional 7 (2d6) bludgeoning damage is dealt to the target.