"Orc Berserker";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Orc"
_skills_: "Athletics +5, Intimidation +2"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "30 (4d8 + 12)"
_armor class_: "14"
_stats_: | 16 (+3) | 12 (+1) | 17 (+3) | 7 (-2) | 11 (+0) | 10 (+0) |

___Aggressive.___ As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.

___Reckless.___ At the start of its turn, the orc can gain advantage on all melee weapon attack rolls during that turn, but attack rolls against it have advantage until the start of its next turn.

___Unarmored Defense.___ While the orc is wearing no armor, its AC includes its Constitution modifier.

**Actions**

___Greataxe.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 9 (1d12 + 3) slashing damage.
