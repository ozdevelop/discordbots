"Frost Salamander";;;_page_number_: 223
_size_: Huge elemental
_alignment_: unaligned
_challenge_: "9 (5,000 XP)"
_languages_: "Primordial"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 14"
_skills_: "Perception +4"
_damage_immunities_: "cold"
_saving_throws_: "Con +8, Wis +4"
_speed_: "60 ft., burrow 40 ft., climb 40 ft."
_hit points_: "168  (16d12 + 64)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 18 (+4) | 7 (-1) | 11 (0) | 7 (-1) |

___Burning Fury.___ When the salamander takes fire damage, its Freezing Breath automatically recharges.

**Actions**

___Multiattack___ The salamander makes five attacks: four with its claws and one with its bite.

___Claws___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 8 (1d6 + 5) piercing damage.

___Bite___ Melee Weapon Attack: +9 to hit, reach 15 ft., one target. Hit: 9 (1d8 + 5) piercing damage and 5 (1d10) cold damage.

___Freezing Breath (Recharge 6)___ The salamander exhales chill wind in a 60-foot cone. Each creature in that area must make a DC 17 Constitution saving throw, taking 44 (8d10) cold damage on a failed save, or half as much damage on a successful one.