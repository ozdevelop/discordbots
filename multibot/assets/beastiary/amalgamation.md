"Amalgamation";;;_size_: Gargantuan construct
_alignment_: neutral
_challenge_: "14 (11,500 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_skills_: "Perception +3, Stealth +3"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "10 ft., fly 50 ft."
_hit points_: "435 (30d20 + 120)"
_armor class_: "16"
_stats_: | 23 (+6) | 10 (+0) | 18 (+4) | 2 (-4) | 15 (+2) | 1 (-5) |

___Immutable Form.___ The amalgamation is immune to any spell or effect
that would alter its form.

___Item Use.___ The amalgamation can use any of the items contained within
its bulk. Items with limited uses, such as potions, scrolls, or wands, are
expended normally.

___Magic Resistance.___ The amalgamation has advantage on saving
throws against spells and other magical effects.

___Magic Weapons.___ The amalgamation’s weapon attacks are magical.

___Swarm Attack.___ The amalgamation can occupy another creature’s space
and vice versa, damaging the creature with the flying weapons and objects
composing its bulk. Any creature that starts their turn in the same space
as the amalgamation must succeed on a DC 18 Dexterity saving throw or
take 21 (6d6) slashing damage.

**Actions**

___Multiattack.___ The amalgamation makes four melee attacks.

___Sword.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit:
17 (2d10 + 6) slashing damage.

___Mace.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 15
(2d8 + 6) bludgeoning damage.

___Slam.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 19
(3d8 + 6) bludgeoning damage.
