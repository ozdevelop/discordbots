"Vampiric Mist (B)";;;_page_number_: 246
_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "-"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "poison"
_saving_throws_: "Wis +3"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "30  (4d8 + 12)"
_armor class_: "13"
_condition_immunities_: "charmed, exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, cold, lightning, necrotic, thunder; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 6 (-2) | 16 (+3) | 16 (+3) | 6 (-2) | 12 (+1) | 7 (-1) |

___Life Sense.___ The mist can sense the location of any creature within 60 feet of it, unless that creature's type is construct or undead.

___Forbiddance.___ The mist can't enter a residence without an invitation from one of the occupants.

___Misty Form.___ The mist can occupy another creature's space and vice versa. In addition, if air can pass through a space, the mist can pass through it without squeezing. Each foot of movement in water costs it 2 extra feet, rather than 1 extra foot. The mist can't manipulate objects in any way that requires fingers or manual dexterity.

___Sunlight Hypersensitivity.___ The mist takes 10 radiant damage whenever it starts its turn in sunlight. While in sunlight, the mist has disadvantage on attack rolls and ability checks.

**Actions**

___Life Drain___ The mist touches one creature in its space. The target must succeed on a DC 13 Constitution saving throw (undead and constructs automatically succeed), or it takes 10 (2d6 + 3) necrotic damage, the mist regains 10 hit points, and the target's hit point maximum is reduced by an amount equal to the necrotic damage taken. This reduction lasts until the target finishes a long rest. The target dies if its hit point maximum is reduced to 0.