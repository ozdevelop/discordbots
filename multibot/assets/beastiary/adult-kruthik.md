"Adult Kruthik";;;_page_number_: 212
_size_: Medium monstrosity
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "Kruthik"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 11"
_speed_: "40 ft., burrow 20 ft., climb 40 ft."
_hit points_: "39  (6d8 + 12)"
_armor class_: "18 (natural armor)"
_stats_: | 15 (+2) | 16 (+3) | 15 (+2) | 7 (-1) | 12 (+1) | 8 (-1) |

___Keen Smell.___ The kruthik has advantage on Wisdom (Perception) checks that rely on smell.

___Pack Tactics.___ The kruthik has advantage on an attack roll against a creature if at least one of the kruthik's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Tunneler.___ The kruthik can burrow through solid rock at half its burrowing speed and leaves a 5-foot-diameter tunnel in its wake.

**Actions**

___Multiattack___ The kruthik makes two stab attacks or two spike attacks.

___Stab___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Spike___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.