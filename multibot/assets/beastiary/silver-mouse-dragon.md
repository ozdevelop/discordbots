"Silver Mouse Dragon";;;_size_: Tiny dragon
_alignment_: chaotic good
_challenge_: "2 (450 XP)"
_languages_: "understands Common and Draconic but can’t speak"
_skills_: "Arcana +2, Nature +2, Perception +4, Stealth +6"
_senses_: "darkvision 30 ft., passive Perception 14"
_saving_throws_: "Dex +4, Con +4, Wis +2, Cha +3"
_damage_immunities_: "cold"
_speed_: "20 ft. climb 20 ft., burrow 10 ft."
_hit points_: "27 (6d4 + 12)"
_armor class_: "13 (natural armor)"
_stats_: | 5 (-3) | 14 (+2) | 14 (+2) | 10 (+0) | 11 (+0) | 11 (+1) |

___Pack Tactics.___ The mouse dragon has advantage on an attack roll against
a creature if at least one of the dragon’s allies is within 5 feet of the creature
and the ally isn’t incapacitated.

___Treasure Sense.___ The mouse dragon can pinpoint, by scent, the location
of precious metals and stones, such as coins and gems, within 60 feet of it.

___Underfoot.___ The mouse dragon can attempt to hide even when it is
obscured only by a creature that is at least one size larger than it.

**Actions**

___Multiattack.** The gold mouse dragon makes one bite attack and two
claw attacks.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 2) piercing damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6
(1d6 + 2) slashing damage.

___Cold Breath (Recharge 5–6).___ The dragon exhales an icy blast in a 10-
foot cone. Each creature in that area must make a DC 12 Constitution
saving throw, taking 22 (5d8) cold damage on a failed saving throw, or
half as much damage on a successful one.
