"Swarm of Centipedes";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 10 ft."
_speed_: "20 ft., climb 20 ft."
_hit points_: "22 (5d8)"
_armor class_: "12 (natural armor)"
_condition_immunities_: "charmed, frightened, grappled, paralyzed, petrified, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, slashing"
_stats_: | 3 (-4) | 13 (+1) | 10 (0) | 1 (-5) | 7 (-2) | 1 (-5) |

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny insect. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Bites.___ Melee Weapon Attack: +3 to hit, reach 0 ft., one target in the swarm's space. Hit: 10 (4d4) piercing damage, or 5 (2d4) piercing damage if the swarm has half of its hit points or fewer.

A creature reduced to 0 hit points by a swarm of centipedes is stable but poisoned for 1 hour, even after regaining hit points, and paralyzed while poisoned in this way.