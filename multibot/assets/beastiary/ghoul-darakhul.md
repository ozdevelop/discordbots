"Ghoul, Darakhul";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Darakhul"
_skills_: "Deception +3, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "poison"
_damage_resistances_: "necrotic"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "16 (scale mail; 18 with shield)"
_stats_: | 16 (+3) | 17 (+3) | 14 (+2) | 14 (+2) | 12 (+1) | 12 (+1) |

___Master of Disguise.___ A darakhul in a prepared disguise has advantage on Charisma (Deception) checks made to pass as a living creature. While using this ability, the darakhul loses its stench.

___Stench.___ Any creature that starts its turn within 5 feet of the darakhul must make a successful DC 12 Constitution saving throw or be poisoned until the start of its next turn. A successful saving throw makes the creature immune to the darakhul's stench for 24 hours. A darakhul using this ability can't also benefit from Master of Disguise.

___Sunlight Sensitivity.___ The darakhul has disadvantage on Wisdom (Perception) checks that rely on sight and on attack rolls while it, the object it is trying to see or attack in direct sunlight.

___Turning Defiance.___ The darakhul and any ghouls within 30 feet of it have advantage on saving throws against effects that turn undead.

___Darakhul Fever.___ Spread mainly through bite wounds, this rare disease makes itself known within 24 hours by swiftly debilitating the infected. A creature so afflicted must make a DC 17 Constitution saving throw after every long rest. On a failed save the victim takes 14 (4d6) necrotic damage, and its hit point maximum is reduced by an amount equal to the damage taken. This reduction can't be removed until the victim recovers from darakhul fever, and even then only by _greater restoration_ or similar magic. The victim recovers from the disease by making successful saving throws on two consecutive days. _Greater restoration_ cures the disease; _lesser restoration_ allows the victim to make the daily Constitution check with advantage. Primarily spread among humanoids, the disease can affect ogres, and therefore other giants may be susceptible. If the infected creature dies while infected with darakhul fever, roll 1d20, add the character's current Constitution modifier, and find the result below to determine what undead form the victim's body rises in:
* 1-9: None; victim is simply dead
* 10-16: Ghoul
* 17-20: Ghast
* 21+: Darakhul

**Actions**

___Multiattack.___ The darakhul bites once, claws once, and makes one war pick attack. Using a shield limits the darakhul to making either its claw or war pick attack, but not both.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) piercing damage, and if the target creature is humanoid it must succeed on a DC 11 Constitution saving throw or contract darakhul fever.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage. If the target is a creature other than an undead, it must make a successful DC 12 Constitution saving throw or be paralyzed for 1 minute. A paralyzed target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If a humanoid creature is paralyzed for 2 or more rounds (the victim fails at least 2 saving throws), consecutive or nonconsecutive, the creature contracts darakhul fever.

___War Pick.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

