"Scaladar";;;_page_number_: 315
_size_: Huge construct
_alignment_: unaligned
_challenge_: "8 (8,900 XP)"
_languages_: "—"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "force, lightning, poison"
_speed_: "30 ft., clmb 20 ft."
_hit points_: "94 (7d12 + 49)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, paralyzed, poisoned"
_damage_resistances_: "fire; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 19 (+4) | 10 (0) | 25 (+7) | 1 (-5) | 12 (+1) | 1 (-5) |

___Lightning Absorption.___ Whenever the scaladar is subjected to lightning damage, it takes no damage, and its sting deals an extra 11 (2d10) lightning damage until the end of its next turn.

___Scaladar Link.___ The scaladar knows the location of other scaladar within 100 feet of it, and it can sense when any of them take damage.

**Actions**

___Multiattack.___ The scaladar makes three attacks: two with its claws and one with its sting.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 10 (1d12 + 4) bludgeoning damage, and the target is grappled (escape DC 15). The scaladar has two claws, each of which can grapple one target.

___Sting.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 9 (1d10 + 4) piercing damage plus 11 (2d10) lightning damage.