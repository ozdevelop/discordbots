"Phoenix";;;_page_number_: 199
_size_: Gargantuan elemental
_alignment_: neutral
_challenge_: "16 (15,000 XP)"
_languages_: "-"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_immunities_: "fire, poison"
_saving_throws_: "Wis +10, Cha +9"
_speed_: "20 ft., fly 120 ft."
_hit points_: "175  (10d20 + 70)"
_armor class_: "18"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 19 (+4) | 26 (+8) | 25 (+7) | 2 (-4) | 21 (+5) | 18 (+4) |

___Fiery Death and Rebirth.___ When the phoenix dies, it explodes. Each creature within 60-feet of it must make a DC 20 Dexterity saving throw, taking 22 (4d10) fire damage on a failed save, or half as much damage on a successful one. The fire ignites flammable objects in the area that aren't worn or carried.
The explosion destroys the phoenix's body and leaves behind an egg-shaped cinder that weighs 5 pounds. The cinder is blazing hot, dealing 21 (6d6) fire damage to any creature that touches it, though no more than once per round. The cinder is immune to all damage, and after 1d6 days, it hatches a new phoenix.

___Fire Form.___ The phoenix can move through a space as narrow as 1 inch wide without squeezing. Any creature that touches the phoenix or hits it with a melee attack while within 5 feet of it takes 5 (1d10) fire damage. In addition, the phoenix can enter a hostile creature's space and stop there. The first time it enters a creature's space on a turn, that creature takes 5 (1d10) fire damage. With a touch, the phoenix can also ignite flammable objects that aren't worn or carried (no action required).

___Flyby.___ The phoenix doesn't provoke opportunity attacks when it flies out of an enemy's reach.

___Illumination.___ The phoenix sheds bright light in a 60-foot radius and dim light for an additional 30 feet.

___Legendary Resistance (3/Day).___ If the phoenix fails a saving throw, it can choose to succeed instead.

___Siege Monster.___ The phoenix deals double damage to objects and structures.

**Actions**

___Multiattack___ The phoenix makes two attacks: one with its beak and one with its fiery talons.

___Beak___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 15 (2d6 + 8) fire damage. If the target is a creature or a flammable object, it ignites. Until a creature takes an action to douse the fire, the target takes 5 (1d10) fire damage at the start of each of its turns.

___Fiery Talons___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 17 (2d8 + 8) fire damage.

**Legendary** Actions

The phoenix can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The phoenix regains spent legendary actions at the start of its turn.

___Peck___ The phoenix makes one beak attack.

___Move___ The phoenix moves up to its speed.

___Swoop (Costs 2 Actions)___ The phoenix moves up to its speed and attacks with its fiery talons.