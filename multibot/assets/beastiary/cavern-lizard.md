"Cavern Lizard";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Athletics +5, Stealth +4 (+6 in areas of natural stone or rock)"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "30 ft., climb 20 ft."
_hit points_: "93 (11d10 + 33)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 14 (+2) | 17 (+3) | 3 (-4) | 12 (+1) | 2 (-4) |

___Surprise Attack.___ If the cavern lizard surprises a creature and hits it with
an attack during the first round of combat, the target takes an extra 14
(4d6) damage from the attack.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 14
(2d10 + 3) piercing damage plus the target is grappled (escape DC 13),
and the cavern lizard can’t grapple another target.
