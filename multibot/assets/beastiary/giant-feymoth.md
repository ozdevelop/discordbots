"Giant Feymoth";;;_size_: Large fey
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_languages_: "Sylvan, Celestial"
_skills_: "Nature +4, Perception +6, Survival +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +5, Wis +6"
_speed_: "20 ft., fly 40 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "16 (natural armor)"
_stats_: | 13 (+1) | 14 (+2) | 16 (+3) | 12 (+1) | 16 (+3) | 8 (-1) |

___Magic Resistance.___ The feymoth has advantage on
saving throws against spells and other magical
effects.

___Enchanting Pattern.___ When a creature starts its turn
within 30 feet of the feymoth and is able to see the
feymoth’s wings, the feymoth can magically force it
to make a DC 14 Charisma saving throw, unless the
feymoth is incapacitated.

On a failed saving throw, the creature makes all
attacks rolls this turn with disadvantage as they are
dazzled and confused by the vibrant display of the
feymoth’s wings.

Unless surprised, a creature can avert its eyes to
avoid the saving throw at the start of its turn. If the
creature does so, it can’t see the feymoth until the
start of its next turn, when it can avert its eyes
again. If the creature looks at the feymoth in the
meantime, it must immediately make the save.

___Innate Spellcasting.___ The feymoth’s innate
spellcasting ability is Wisdom (spell save DC 14).
The feymoth can innately cast the following spells,
requiring no material components:

* At will: _dancing lights, druidcraft_

* 3/day each: _confusion, gust of wind, plant growth, sleep_

* 1/day each: _awaken, conjure woodland beings, hypnotic pattern, insect plague, mass cure wounds_

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 6 (1d8 + 2) piercing damage, and
the target must make a DC 14 Intelligence saving
throw or take 27 (5d10) psychic damage on a
failed save, or half as much on a successful one.
