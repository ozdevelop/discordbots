"Erinyes";;;_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "12 (8,400 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "truesight 120 ft."
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +7, Con +8, Wis +6, Cha +8"
_speed_: "30 ft., fly 60 ft."
_hit points_: "153 (18d8+72)"
_armor class_: "18 (plate)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 18 (+4) | 16 (+3) | 18 (+4) | 14 (+2) | 14 (+2) | 18 (+4) |

___Hellish Weapons.___ The erinyes's weapon attacks are magical and deal an extra 13 (3d8) poison damage on a hit (included in the attacks).

___Magic Resistance.___ The erinyes has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The erinyes makes three attacks

___Longsword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, or 9 (1d10 + 4) slashing damage if used with two hands, plus 13 (3d8) poison damage.

___Longbow.___ Ranged Weapon Attack: +7 to hit, range 150/600 ft., one target. Hit: 7 (1d8 + 3) piercing damage plus 13 (3d8) poison damage, and the target must succeed on a DC 14 Constitution saving throw or be poisoned. The poison lasts until it is removed by the lesser restoration spell or similar magic.

___Variant: Rope of Entanglement.___ Some erinyes carry a rope of entanglement (detailed in the Dungeon Master's Guide). When such an erinyes uses its Multiattack, the erinyes can use the rope in place of two of the attacks.

**Reactions**

___Parry.___ The erinyes adds 4 to its AC against one melee attack that would hit it. To do so, the erinyes must see the attacker and be wielding a melee weapon.