"Steam Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Aquan, Ignan"
_senses_: "darkvision 60 ft."
_damage_immunities_: "fire, poison"
_speed_: "30 ft., fly 30 ft."
_hit points_: "21 (6d6)"
_armor class_: "10"
_condition_immunities_: "poisoned"
_stats_: | 5 (-3) | 11 (0) | 10 (0) | 11 (0) | 10 (0) | 12 (+1) |

___Death Burst.___ When the mephit dies, it explodes in a cloud of steam. Each creature within 5 ft. of the mephit must succeed on a DC 10 Dexterity saving throw or take 4 (1d8) fire damage.

___Innate Spellcasting (1/Day).___ The mephit can innately cast blur, requiring no material components. Its innate spellcasting ability is Charisma.

**Actions**

___Claws.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one creature. Hit: 2 (1d4) slashing damage plus 2 (1d4) fire damage.

___Steam Breath (Recharge 6).___ The mephit exhales a 15-foot cone of scalding steam. Each creature in that area must succeed on a DC 10 Dexterity saving throw, taking 4 (1d8) fire damage on a failed save, or half as much damage on a successful one.

___Variant: Summon Mephits (1/Day).___ The mephit has a 25 percent chance of summoning 1d4 mephits of its kind. A summoned mephit appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other mephits. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.