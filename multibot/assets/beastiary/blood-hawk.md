"Blood Hawk";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_skills_: "Perception +4"
_speed_: "10 ft., fly 60 ft."
_hit points_: "7 (2d6)"
_armor class_: "12"
_stats_: | 6 (-2) | 14 (+2) | 10 (0) | 3 (-4) | 14 (+2) | 5 (-3) |

___Keen Sight.___ The hawk has advantage on Wisdom (Perception) checks that rely on sight.

___Pack Tactics.___ The hawk has advantage on an attack roll against a creature if at least one of the hawk's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Beak.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.