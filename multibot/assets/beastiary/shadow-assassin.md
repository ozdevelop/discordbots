"Shadow Assassin";;;_page_number_: 316
_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 19"
_skills_: "Perception +9, Stealth +12"
_damage_immunities_: "necrotic, poison"
_saving_throws_: "Dex +8, Int +5"
_speed_: "40 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "14"
_damage_vulnerabilities_: "radiant"
_condition_immunities_: "exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 6 (-2) | 19 (+4) | 14 (+2) | 13 (+1) | 12 (+1) | 14 (+2) |

___Amorphous.___ The assassin can move through a space as narrow as 1 inch wide without squeezing.

___Shadow Stealth.___ While in dim light or darkness, the assassin can take the Hide action as a bonus action.

___Sunlight Weakness.___ While in sunlight, the assassin has disadvantage on attack rolls, ability checks, and saving throws.

**Actions**

___Multiattack.___ The assassin makes two Shadow Blade attacks.

**Shadow Blade.** Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 10 (3d6) necrotic damage. Unless the target is immune to necrotic damage, the target’s Strength score is reduced by 1d4 each time it is hit by this attack. The target dies if its Strength is reduced to 0. The reduction lasts until the target finishes a short or long rest.

If a non-evil humanoid dies from this attack, a shadow (see the *Monster Manual*) rises from the corpse 1d4 hours later.