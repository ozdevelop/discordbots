"Glacial Raptor";;;_size_: Medium undead (beast)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Acrobatics +5, Perception +2"
_senses_: "passive Perception 12"
_damage_vulnerabilities_: "bludgeoning"
_damage_resistances_: "cold, piercing, slashing"
_speed_: "40 ft."
_hit points_: "51 (7d10 + 14)"
_armor class_: "13"
_stats_: | 14 (+2) | 16 (+3) | 14 (+2) | 4 (-3) | 10 (+0) | 2 (-4) |

___Frozen Ferocity.___ When the raptor hits with a melee
attack, it deals an extra 2 (1d4) cold damage
(included in the attack).

___Pack Tactics.___ The raptor has advantage on an attack
roll against a creature if at least one of the raptor’s
allies is within 5 feet of the creature and the ally
isn’t incapacitated.

**Actions**

___Multiattack.** The raptor makes one attack with its
bite and one with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 7 (1d8 + 3) piercing damage plus 2
(1d4) cold damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 6 (1d6 + 3) slashing damage plus 2
(1d4) cold damage.

___Frozen Gaze (Recharge 6).___ The raptor locks eyes
with a creature and attempts to freeze it from the
inside out. The target must make a DC 12
Constitution saving throw, taking 14 (4d6) cold
damage and gaining one level of exhaustion on a
failed save, or half as much damage and not
exhausted on a successful one.
