"Gilded Devil";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "7 (2900 XP)"
_languages_: "Celestial, Common, Draconic, Infernal; telepathy (120 ft.)"
_skills_: "Deception +9, History +5, Insight +10, Persuasion +9, Sleight of Hand +8"
_senses_: ", passive Perception 14"
_saving_throws_: "Str +6, Con +6, Wis +7, Cha +6"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "16 (coin mail)"
_stats_: | 17 (+3) | 15 (+2) | 17 (+3) | 15 (+2) | 18 (+4) | 17 (+3) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Liar's Largesse.___ A gilded devil has influence over the recipient of a gift for as long as that creature retains the gift. The recipient receives a -2 penalty on saving throws against the gilded devil's abilities and a further -10 penalty against scrying attempts made by the gilded devil. A remove curse spell removes this effect.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The gilded devil's weapon attacks are magical.

___Innate Spellcasting.___ The gilded devil's spellcasting ability is Wisdom (spell save DC 15, +7 to hit with spell attacks). The gilded devil can innately cast the following spells, requiring no material components:

At will: detect thoughts, major image, suggestion

3/day each: dominate person, polymorph, scorching ray (4 rays), scrying

1/day: teleport (self plus 50 lb of objects only)

**Actions**

___Multiattack.___ The gilded devil makes two heavy flail attacks.

___Heavy Flail (Scourge of Avarice).___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 10 (1d10 + 5) bludgeoning damage.

___Betrayal of Riches (Recharge 5-6).___ As a bonus action, a gilded devil can turn rings, necklaces, and other jewelry momentarily against their wearer. The devil can affect any visible item of jewelry on up to two creatures within 60 feet, twisting them into cruel barbs and spikes. Each target must succeed on a DC 15 Wisdom saving throw to halve the damage from this effect. If the saving throw fails, the victim takes 13 (3d8) piercing damage and an additional effect based on the item location targeted.

An item is treated as jewelry if it is made of a precious material (such as silver, gold, ivory, or adamantine), adorned with gems, or both, and is worth at least 100 gp.

Arms: STR Save or Melee Damage halved until a short rest

Hand: STR Save or Drop any held item

Eyes: DEX Save or Permanetly blinded

Head: DEX Save Disadvantage on INT checks until long rest

Feet: DEX Save or Speed halved for 24 hours

Neck: CON Save or Stunned, unable to breathe for 1 round

Other: No additional effects

___Scorn Base Metals.___ A gilded devil's attacks ignore any protection provided by nonmagical armor made of bronze, iron, steel, or similar metals. Protection provided by valuable metals such as adamantine, mithral, and gold apply, as do bonuses provided by non-metallic objects.

___Scourge of Avarice.___ As a bonus action, a gilded devil wearing jewelry worth at least 1,000 gp can reshape it into a +2 heavy flail. A creature struck by this jeweled flail suffers disadvantage on all Wisdom saving throws until his or her next short rest, in addition to normal weapon damage. The flail reverts to its base components 1 minute after it leaves the devil's grasp, or upon the gilded devil's death.

___Voracious Greed.___ As an action, a gilded devil can consume nonmagical jewelry or coinage worth up to 1,000 gp. For each 200 gp consumed, it heals 5 hp of damage. A gilded devil can use this ability against the worn items of a grappled foe. The target must succeed on a DC 13 Dexterity saving throw to keep an item from being consumed.

