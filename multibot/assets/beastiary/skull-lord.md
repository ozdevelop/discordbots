"Skull Lord";;;_page_number_: 230
_size_: Medium undead
_alignment_: lawful evil
_challenge_: "15 (13,000 XP)"
_languages_: "all the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 22"
_skills_: "Athletics +7, History +8, Perception +12, Stealth +8"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "105  (14d8 + 42)"
_armor class_: "18 (plate)"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, poisoned, stunned, unconscious"
_damage_resistances_: "cold, necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 14 (+2) | 16 (+3) | 17 (+3) | 16 (+3) | 15 (+2) | 21 (+5) |

___Legendary Resistance (3/Day).___ If the skull lord fails a saving throw, it can choose to succeed instead.

___Master of the Grave.___ While within 30 feet of the skull lord, any undead ally of the skull lord makes saving throws with advantage, and that ally regains 1d6 hit points whenever it starts its turn there.

___Evasion.___ If the skull lord is subjected to an effect that allows it to make a Dexterity saving throw to take only half the damage, the skull lord instead takes no damage if it succeeds on the saving throw, and only half damage if it fails.

___Spellcasting.___ The skull lord is a 13th-level spellcaster. Its spellcasting ability is Charisma (spell save DC18, +10 to hit with spell attacks). The skull lord knows the following sorcerer spells:

* Cantrips (at will): _chill touch, fire bolt, mage hand, poison spray, ray of frost, shocking grasp_

* 1st level (4 slots): _magic missile, expeditious retreat, thunderwave_

* 2nd level (3 slots): _mirror image, scorching ray_

* 3rd level (3 slots): _fear, haste_

* 4th level (3 slots): _dimension door, ice storm_

* 5th level (2 slots): _cloudkill, cone of cold_

* 6th level (1 slot): _eyebite_

* 7th level (1 slot): _finger of death_

**Actions**

___Multiattack___ The skull lord makes three bone staff attacks.

___Bone Staff___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) bludgeoning damage plus 14 (4d6) necrotic damage.

**Legendary** Actions

The skull lord can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The skull lord regains spent legendary actions at the start of its turn.

___Bone Staff (Costs 2 Actions)___ The skull lord makes a bone staff attack.

___Cantrip___ The skull lord casts a cantrip.

___Move___ The skull lord moves up to its speed without provoking opportunity attacks.

___Summon Undead (Costs 3 Actions)___ Up to five skeletons or zombies appear in unoccupied spaces within 30 feet of the skull lord and remain until destroyed. Undead summoned in this way roll initiative and act in the next available turn. The skull lord can have up to five undead summoned by this ability at a time.
