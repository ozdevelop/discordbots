"Ancient Ruby Dragon";;;_size_: Gargantuan dragon
_alignment_: lawful neutral
_challenge_: "23 (50,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +15, Insight +11, Perception +11, Religion +15"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 28"
_saving_throws_: "Dex +15, Int +15, Wis +11, Cha +14"
_damage_vulnerabilities_: "psychic"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, slashing, and piercing from nonmagical attacks"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "299 (26d20 + 26)"
_armor class_: "23 (natural armor)"
_stats_: | 23 (+6) | 26 (+8) | 12 (+1) | 26 (+8) | 18 (+4) | 24 (+7) |

___Legendary Resistance (3/Day).___ If the dragon fails
a saving throw, it can choose to succeed instead.

___Amplification Aura.___ Allies’ spells cast within 30
feet have their saving throw DC increased by 4.

**Psionics**

___Charges:___ 26 | ___Recharge:___ 1d10 | ___Fracture:___ 32

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 15 ft.,
one target. Hit: 17 (2d10 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +13 to hit, reach 10 ft.,
one target. Hit: 13 (2d6 + 6) slashing damage.

___Tail.___ Melee Weapon Attack: +13 to hit; reach 20 ft.,
one target. Hit: 15 (2d8 + 6) bludgeoning damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The dragon regains spent
legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) Check.

___Psionics.___ The dragon uses a psionic ability.

___Psionic Shift (Costs 2 Actions).___ The dragon
releases a wave of telekinetic energy from its mind.
Every creature within 15 feet must make a DC 24
Intelligence saving throw or take 15 (2d6 + 8) force
damage and be knocked prone. The dragon then
can move up to half its movement speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the
dragon takes a lair action to cause one of the
following effects. The dragon can’t use the same
effect two rounds in a row.

* The dragon manifests _amplify_ at no cost.

* The dragon manifests _the real_ at no cost. It
does not require concentration and lasts for
10 minutes.

* The dragon casts _dispel magic_. The spell automatically
ends spells of 5th level or lower. For
each spell of 6th level or higher on the target,
make an ability check using the dragon’s
Intelligence as its spellcasting ability. The DC
equals 10 + the spell’s leveI. On a successful
check, the spell ends.

**Regional** Effects

Intelligent creatures who sleep within 12 miles of
a ruby dragon’s lair dream of people they know
transforming into other people, of places they’ve
been dissolving to reveal barren wastelands.
