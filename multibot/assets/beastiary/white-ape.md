"White Ape";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "6 (2300 XP)"
_languages_: "Common"
_skills_: "Acrobatics +6, Athletics +7, Intimidation +2, Perception +5, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "40 ft., climb 40 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 16 (+3) | 18 (+4) | 8 (-1) | 14 (+2) | 8 (-1) |

___Hatred for Spellcasters.___ The white ape does one extra die of damage (d8 or d10, respectively) per attack against an enemy it has seen cast a spell.

___Arcane Wasting (Disease).___ When the bodily fluid of an infected creature touches a humanoid or when an infected creature casts a spell (direct or indirect) on a humanoid, that humanoid must succeed on a DC 15 Constitution saving throw or become infected with arcane wasting. Beginning 1d6 days after infection, the infected creature must make a DC 15 Constitution saving throw at the end of each long rest. If the saving throw fails, the victim loses 1d3 Intelligence and 1d3 Wisdom. Lost Intelligence and Wisdom can't be recovered while the disease persists. If the saving throw succeeds, nothing happens; the disease ends after the second consecutive successful saving throws. Once the disease ends, lost Intelligence and Wisdom can be restored by greaterrestoration or comparable magic. The disease is also cured by lesser restoration if the caster makes a successful DC 15 spellcasting check.

**Actions**

___Multiattack.___ The ape makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage, and the target must succeed on a DC 14 Constitution saving throw or contract the arcane wasting disease (see sidebar).

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) slashing damage.

___Frenzy (1/Day).___ When two or more foes are adjacent to the ape, it can enter a deadly battle frenzy. Instead of using its normal multiattack, a frenzied white ape makes one bite attack and two claw attacks against each enemy within 5 feet of it. Melee attacks against the white ape are made with advantage from the end of that turn until the start of the white ape's next turn.

