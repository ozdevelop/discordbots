"Clockwork Watchman";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Athletics +4, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Con +3"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "55 (10d8 + 10)"
_armor class_: "14 (natural armor)"
_stats_: | 14 (+2) | 12 (+1) | 12 (+1) | 5 (-3) | 10 (+0) | 1 (-5) |

___Immutable Form.___ The clockwork watchman is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The clockwork watchman has advantage on saving throws against spells and other magical effects.

**Actions**

___Halberd.___ Melee Weapon Attack: +4 to hit, reach 10 ft., onetarget. Hit: 7 (1d10 + 2) slashing damage.

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage.

___Net Cannon.___ Ranged Weapon Attack: +3 to hit, range 5/15 ft., one target, size Large or smaller. Hit: the target is restrained. A mechanism within the clockwork huntsman's chest can fire a net with a 20-foot trailing cable anchored within the watchman's chest. A creature can free itself (or another creature) from the net by using its action to make a successful DC 10 Strength check or by dealing 5 slashing damage to the net at AC 10. The watchman can fire up to four nets before it must be reloaded.

