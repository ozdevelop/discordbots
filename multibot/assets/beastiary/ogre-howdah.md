"Ogre Howdah";;;_page_number_: 221
_size_: Large giant
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft., passive Perception 8"
_speed_: "40 ft."
_hit points_: "59  (7d10 + 21)"
_armor class_: "13 (breastplate)"
_stats_: | 19 (+4) | 8 (-1) | 16 (+3) | 5 (-2) | 7 (-1) | 7 (-1) |

___Howdah.___ The ogre carries a compact fort on its back. Up to four Small creatures can ride in the fort without squeezing. To make a melee attack against a target within 5 feet of the ogre, they must use spears or weapons with reach. Creatures in the fort have three-quarters cover against attacks and effects from outside it. If the ogre dies, creatures in the fort are placed in unoccupied spaces within 5 feet of the ogre.

**Actions**

___Mace___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.