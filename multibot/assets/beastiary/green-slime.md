"Green Slime";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft. passive Perception 10"
_damage_vulnerabilities_: "cold, fire, radiant"
_damage_immunities_: "acid"
_condition_immunities_: "charmed, frightened, poisoned, prone, stunned"
_speed_: "0 ft."
_hit points_: "5 (1d6 + 2)"
_armor class_: "5"
_stats_: | 1 (-5) | 1 (-5) | 15 (+2) | 1 (-5) | 10 (+0) | 1 (-5) |

___Delicate.___ Sunlight, any effect that cures disease, and any effect that deals cold, fire, or radiant damage instantly destroys a patch of green slime.

**Actions**

___Drop.___ Clinging to walls and ceilings, the green slime drops down when it detects movement below it.
A creature aware of the slime's presence can avoid it with a successful DC 10 Dexterity saving throw.  Otherwise, the slime can't be avoided as it drops.

A creature that comes into contact with the green slime takes 5 (1d10) acid damage.  The creature takes the damage again at the start of each of its turns until the slime is scraped off or destroyed.
Against wood or metal, green slime deals 11 (2d10) acid damage each round, and any nonmagical metal or wooden tool or weapon used to scrape off the slime is effectively destroyed.
