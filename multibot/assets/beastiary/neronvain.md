"Neronvain";;;_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Draconic, Elvish, Infernal"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +7, Perception +5"
_damage_immunities_: "poison"
_saving_throws_: "Con +6, Wis +5"
_speed_: "30 ft."
_hit points_: "117 (18d8+36)"
_armor class_: "17"
_condition_immunities_: "charmed, frightened, poisoned"
_stats_: | 8 (-1) | 17 (+3) | 15 (+2) | 16 (+3) | 13 (+1) | 18 (+4) |

___Draconic Majesty.___ Neronvain adds his Charisma bonus to his AC (included).

**Actions**

___Fey Ancestry.___ Magic can't put Neronvain to sleep.

___Multiattack.___ Neronvain makes two attacks, either with his shortsword or Eldritch Arrow.

___Shortsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage plus 13 (3d8) poison damage.

___Eldritch Arrow.___ Ranged Spell Attack: +7 to hit, range 120 ft., one target. Hit: 11 (2d10) force damage plus 9 (2d8) poison damage.

___Poisonous Cloud (2/Day).___ Poison gas fills a 20-foot-radius sphere centered on a point Neronvain can see within 50 feet of him. The gas spreads around corners and remains until the start of Neronvain's next turn. Each creature that starts its turn in the gas must succeed on a DC 16 Constitution saving throw or be poisoned for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. >>>