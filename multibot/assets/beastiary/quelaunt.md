"Quelaunt";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "15 (13000 XP)"
_languages_: "Deep Speech, telepathy 120 ft."
_skills_: "Arcana +5, Perception +9, Stealth +9"
_senses_: "darkvision 300 ft., passive Perception 19"
_saving_throws_: "Cha +11, Dex +9, Wis +9"
_damage_vulnerabilities_: "radiant"
_damage_immunities_: "cold, necrotic, poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "225 (18d10 + 126)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 18 (+4) | 24 (+7) | 10 (+0) | 18 (+4) | 23 (+6) |

___Interdimensional Movement.___ A quelaunt can innately cast _misty step_ as a bonus action once per round.

___Alien Mind.___ The quelaunt’s thoughts are unreadable, and any creature attempting
to mentally interact with it must make a DC 17 Intelligence saving throw or take 10 (2d6 + 3) psychic damage.

___Aura of Fear.___ The quelaunt mentally assaults the mind of all hostile creatures it can see, filling their heads with insane mutterings and preying upon their fears and negative emotions.  If a creature starts its turn within 20 ft. of the quelaunt, it must make a DC 17 Wisdom saving throw or be frightened for 1 minute.  The target may repeat the saving throw at the end of each of its turns, ending the effect on a success.  Creatures who succeed on the saving throw are immune to this effect for 24 hours. 

___Legendary Resistance (2/day).___ If the quelaunt fails a saving throw, it can choose to succeed instead.

___Innate Spellcasting.___ The quelaunt's innate spellcasting ability is 
Charisma (spell save DC 19, +11 to hit with spell attacks).  It can innately cast the following spells, 
requiring no components:

* At will: _misty step_

* 1/day each: _fear, fly, sleep_ (cast at 5th level)

**Actions**

___Multiattack.___ The quelaunt makes three claw attacks.

___Claw.___ Melee Weapon Attack. +10 to hit, reach 10 ft., one target. Hit: 18 (2d12 + 5) 
slashing damage plus 13 (3d8) necrotic damage.

___Maddening Whispers.___ The quelaunt telepathically assaults a target it can see within 60 ft. 
The target must succeed on a DC 17 Wisdom saving throw or suffer 33 (6d10) psychic damage and 
be stunned until the end of the target's next turn.

**Reactions**

___Mist Form.___ Upon taking damage from an attack, the quelaunt's body partially dissolves into smoke and mist 
before reforming again.  The quelaunt gains resistance to all damage from the triggering attack.
