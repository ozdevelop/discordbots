"Azza Gremlin";;;_size_: Small fey
_alignment_: neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Primordial"
_senses_: "darkvision 120 ft., passive Perception 11"
_damage_immunities_: "lightning, thunder"
_speed_: "10 ft., fly 40 ft. (hover)"
_hit points_: "7 (2d6)"
_armor class_: "14"
_stats_: | 5 (-3) | 18 (+4) | 10 (+0) | 12 (+1) | 13 (+1) | 10 (+0) |

___Contagious Lightning.___ A creature that touches the azza gremlin or hits it with a melee attack using a metal weapon receives a discharge of lightning. The creature must succeed on a DC 10 Constitution saving throw or attract lightning for 1 minute. For the duration, attacks that cause lightning damage have advantage against this creature, the creature has disadvantage on saving throws against lightning damage and lightning effects, and if the creature takes lightning damage, it is paralyzed until the end of its next turn. An affected creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

**Actions**

___Lightning Jolt.___ Melee or Ranged Spell Attack: +6 to hit, reach 5 ft. or range 30 ft., one creature. Hit: 3 (1d6) lightning damage, and the target is affected by Contagious Lightning.

**Reactions**

___Ride the Bolt.___ The azza gremlin can travel instantly along any bolt of lightning. When it is within 5 feet of a lightning effect, the azza can teleport to any unoccupied space inside or within 5 feet of that lightning effect.

