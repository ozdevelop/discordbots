"Swarm of Rot Grubs";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 10 ft."
_speed_: "5 ft., climb 5 ft."
_hit points_: "22 (5d8)"
_armor class_: "8"
_condition_immunities_: "charmed, frightened, grappled, paralyzed, petrified, prone, restrained"
_damage_resistances_: "piercing, slashing"
_stats_: | 2 (-4) | 7 (-2) | 10 (0) | 1 (-5) | 2 (-4) | 1 (-5) |

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny maggot. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Bites.___ Melee Weapon Attack: +0 to hit, reach 0 ft., one creature in the swarm's space. Hit: The target is infested by 1d4 rot grubs. At the start of each of the target's turns, the target takes 1d6 piercing damage per rot grub infesting it. Applying fire to the bite wound before the end of the target's next turn deals 1 fire damage to the target and kills these rot grubs. After this time, these rot grubs are too far under the skin to be burned. If a target infested by rot grubs ends its turn with 0 hit points, it dies as the rot grubs burrow into its heart and kill it. Any effect that cures disease kills all rot grubs infesting the target.