"Albino Cave Spider";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +4"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 14"
_speed_: "20 ft., climb 10 ft."
_hit points_: "2 (1d4)"
_armor class_: "15 (natural armor)"
_stats_: | 4 (-3) | 14 (+2) | 10 (+0) | 2 (-4) | 10 (+0) | 3 (-4) |

___Spider Climb.___ The spider can climb difficult surfaces, including upside
down on ceilings, without needing to make an ability check.

___Web Sense.___ While in contact with a web, the spider knows the exact
location of any other creature in contact with the same web.

___Web Walker.___ The spider ignores movement restrictions caused by
webbing.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage, and the target must attempt a DC 13 Constitution
saving throw taking 2 (1d4) poison damage on a failure. If the target
misses its saving throw by 5 or more, it is poisoned for 1 hour.
