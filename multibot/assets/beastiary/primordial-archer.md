"Primordial Archer";;;_size_: Medium humanoid (any race)
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "any two languages"
_skills_: "Nature +3, Perception +5, Stealth +8, Survival +5"
_senses_: "passive Perception 15"
_saving_throws_: "Dex +6, Str +2"
_speed_: "30 ft."
_hit points_: "77 (14d8 + 14)"
_armor class_: "15 (leather)"
_stats_: | 10 (+0) | 19 (+4) | 12 (+1) | 12 (+1) | 16 (+3) | 8 (-1) |

___Elemental Arrows.___ Whenever the archer draws an
arrow from its quiver, the archer can imbue it with
elemental magics, imbuing the arrow with special
properties depending on the element chosen:

* Fire – The arrow deals an additional 7 (2d6) fire damage.
* Ice – Any creature hit by this arrow takes an additional 3 (1d6) cold damage and has its movement speed reduced by 10 feet on its next turn.
* Poison – Any creature hit by this arrow must succeed on a DC 13 Constitution saving throw or become poisoned until the end of its next turn.
* Lightning – Any creature hit by this arrow takes an additional 3 (1d6) lightning damage and can’t use reactions until its next turn.

**Actions**

___Multiattack.___ The archer uses its overcharged shot if
able, then makes two attacks with its longbow.

___Shortsword.___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Longbow.___ Ranged Weapon Attack: +6 to hit, range
150/600 ft., one target. Hit: 8 (1d8 + 4) piercing
damage.

___Overcharged Shot (Recharge 5-6).___ The next arrow
the archer fires this turn gains the effects of two
elemental arrows instead of one.
