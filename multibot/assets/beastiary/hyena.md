"Hyena";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_skills_: "Perception +3"
_speed_: "50 ft."
_hit points_: "5 (1d8+1)"
_armor class_: "11"
_stats_: | 11 (0) | 13 (+1) | 12 (+1) | 2 (-4) | 12 (+1) | 5 (-3) |

___Pack Tactics.___ The hyena has advantage on an attack roll against a creature if at least one of the hyena's allies is within 5 ft. of the creature and the ally isn't incapacitated.

**Actions**

___Bite.___ Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 3 (1d6) piercing damage.