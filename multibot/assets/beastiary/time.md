"Time";;;_size_: Large construct
_alignment_: lawful neutral
_challenge_: "9 (5,000 XP)"
_languages_: "all those of the creature who summoned it"
_senses_: "truesight 60 ft., passive Perception 14"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "161 (17d10 + 68)"
_armor class_: "19 (natural armor)"
_stats_: | 18 (+4) | 20 (+5) | 18 (+4) | 15 (+2) | 11 (+0) | 10 (+0) |

___The Law of Time.___ Enemies within 15 feet cannot
take bonus actions or reactions.

___Chaos Vulnerability.___ The Inexorables have disadvantage
on all saving throws against spells.

___Inexorable.___ The Inexorables are immune to any
effects that would slow them or deny them
actions or movement.

___Time Is Inexorable (Recharge 6).___ At the end of
the round, after all creatures have acted, Time
takes another turn.

**Actions**

___Multiattack.___ Time makes three slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach
10 ft., one target. Hit: 16 (2d10 + 5) bludgeoning
damage.

___Haste (Recharge 5–6).___ Until the end of its next
turn, Time magically gains a +2 bonus to its AC,
has advantage on Dexterity saving throws, and
can use its slam attack as a bonus action.
