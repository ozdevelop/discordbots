"Hoard Golem";;;_size_: Huge construct
_alignment_: unaligned
_challenge_: "12 (8400 XP)"
_languages_: "understands the language of its creator but can't speak"
_skills_: "Athletics +10, Perception +4"
_senses_: "darkvision 120 ft., passive Perception 14"
_saving_throws_: "Con +9"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhausted, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "161 (14d12 + 70)"
_armor class_: "18 (natural armor)"
_stats_: | 22 (+6) | 15 (+2) | 20 (+5) | 3 (-4) | 11 (+0) | 1 (-5) |

___Strike with Awe.___ Creatures within 120 feet of an immobile hoard golem suffer disadvantage on Wisdom (Perception) checks. A creature's sheer glee on discovering a vast hoard of treasure distracts it from its surroundings.

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 39 (6d10 + 6) bludgeoning damage.

___Thieving Whirlwind (Recharge 5-6).___ The hoard golem transforms into a 20-foot radius whirlwind of the treasures of which it is composed. In this form, it has immunity to all slashing and piercing damage. As a whirlwind, it can enter other creatures' spaces and stop there. Every creature in a space the whirlwind occupies must make a DC 17 Dexterity saving throw. On a failure, a target takes 40 (6d10 + 7) bludgeoning damage and the whirlwind removes the most valuable visible item on the target, including wielded items, but not armor. If the saving throw is successful, the target takes half the bludgeoning damage and retains all possessions. The golem can remain in whirlwind form for up to 3 rounds, or it can transform back to its normal form on any of its turns as a bonus action.

