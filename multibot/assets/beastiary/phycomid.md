"Phycomid";;;_size_: Small plant
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_skills_: "Stealth +4"
_senses_: "tremorsense 30 ft., passive Perception 10"
_damage_immunities_: "fire, psychic"
_condition_immunities_: "frightened, prone, stunned, unconscious"
_speed_: "10 ft."
_hit points_: "27 (6d6 + 6)"
_armor class_: "12 (natural armor)"
_stats_: | 8 (-1) | 10 (+0) | 13 (+1) | 2 (-4) | 11 (+0) | 1 (-5) |

**Actions**

___Fluid Globule.___ Ranged Weapon Attack: +2 to hit, range 20 ft., one
target. Hit: 7 (2d6) acid damage.

___Debilitating Spores (3/day).___ The phycomid ejects spores in a 10-foot
radius. All creatures within this area must succeed a DC 13 Constitution
saving throw, or take 10 (3d6) necrotic damage and have its hit point
maximum reduced by an amount equal to the damage taken. The reduction
lasts until the target finishes a long rest.
