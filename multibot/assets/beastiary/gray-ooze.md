"Gray Ooze";;;_size_: Medium ooze
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_skills_: "Stealth +2"
_speed_: "10 ft., climb 10 ft."
_hit points_: "22 (3d8+9)"
_armor class_: "8"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_damage_resistances_: "acid, cold, fire"
_stats_: | 12 (+1) | 6 (-2) | 16 (+3) | 1 (-5) | 6 (-2) | 2 (-4) |

___Amorphous.___ The ooze can move through a space as narrow as 1 inch wide without squeezing.

___Corrode Metal.___ Any nonmagical weapon made of metal that hits the ooze corrodes. After dealing damage, the weapon takes a permanent and cumulative -1 penalty to damage rolls. If its penalty drops to -5, the weapon is destroyed. Nonmagical ammunition made of metal that hits the ooze is destroyed after dealing damage.

The ooze can eat through 2-inch-thick, nonmagical metal in 1 round.

___False Appearance.___ While the ooze remains motionless, it is indistinguishable from an oily pool or wet rock.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) bludgeoning damage plus 7 (2d6) acid damage, and if the target is wearing nonmagical metal armor, its armor is partly corroded and takes a permanent and cumulative -1 penalty to the AC it offers. The armor is destroyed if the penalty reduces its AC to 10.