"Star Spawn Grue";;;_page_number_: 234
_size_: Small aberration
_alignment_: neutral evil
_challenge_: "1/4 (50 XP)"
_languages_: "Deep Speech"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "psychic"
_speed_: "30 ft."
_hit points_: "17  (5d6)"
_armor class_: "11"
_stats_: | 6 (-2) | 13 (+1) | 10 (0) | 9 (0) | 11 (0) | 6 (-2) |

___Aura of Madness.___ Creatures within 20 feet of the grue that aren't aberrations have disadvantage on saving throws, as well as on attack rolls against creatures other than a star spawn grue.

**Actions**

___Confounding Bite___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) piercing damage, and the target must succeed on a DC 10 Wisdom saving throw or attack rolls against it have advantage until the start of the grue's next turn.