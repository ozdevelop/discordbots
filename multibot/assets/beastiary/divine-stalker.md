"Divine Stalker";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any two languages"
_skills_: "Acrobatics +8, Medicine +7, Religion +4, Survival +7"
_senses_: "passive Perception 13"
_saving_throws_: "Str +6, Dex +8"
_speed_: "30 ft."
_hit points_: "81 (6d10 + 4d8 + 30)"
_armor class_: "16 (breastplate)"
_stats_: | 14 (+2) | 18 (+4) | 16 (+3) | 10 (+0) | 16 (+3) | 8 (-1) |

___Combat Caster.___ The stalker can perform the somatic
components of spells even when it has weapons or
a shield in one or both hands.

___Divine Arrows.___ The stalker magically infuses each of
its arrows with divine energy. Its longbow attacks
deal an additional 4 (1d8) radiant damage (included
in the attack).

___Spellcasting.___ The stalker is a 9th-level spellcaster. Its
spellcasting ability is wisdom (spell save DC 15, +7
to hit with spell attacks). The stalker has the
following ranger and cleric spells prepared:

* Cantrips (at will): _mending, resistance_

* 1st level (4 slots): _charm person, disguise self, fog cloud, hunter's mark_

* 2nd level (3 slots): _find traps, spike growth, spiritual weapon_

* 3rd level (3 slots): _conjure barrage, lightning arrow_

**Actions**

___Multiattack.___ The stalker makes three attacks with its
shortsword or two attacks with its longbow.

___Shortsword.___ Melee Weapon Attack: +8 to hit, reach
5 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Longbow.___ Ranged Weapon Attack: +10 to hit, range
150/600 ft., one target. Hit: 8 (1d8 + 4) piercing
damage plus 4 (1d8) radiant damage.

___Warding Arrow (1/Day).___ The stalker fires an arrow at
a location it can see within range of its longbow.
When this arrow strikes its mark, it unleashes a
pulse of protective energies. Each creature of the
stalker's choice within 10 feet of the arrow gain 15
temporary hit points.
