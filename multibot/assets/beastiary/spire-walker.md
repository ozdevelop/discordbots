"Spire Walker";;;_size_: Tiny fey
_alignment_: chaotic neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Sylvan"
_senses_: ", passive Perception 10"
_saving_throws_: "Dex +7"
_damage_immunities_: "lightning, thunder"
_damage_resistances_: "piercing from nonmagical weapons"
_speed_: "20 ft."
_hit points_: "38 (11d4 + 22)"
_armor class_: "16 (natural armor)"
_stats_: | 3 (-4) | 18 (+4) | 14 (+2) | 11 (+0) | 10 (+0) | 14 (+2) |

___Energized Body.___ A creature that hits the spire walker with a melee attack using a metal weapon takes 5 (1d10) lightning damage.

___Innate Spellcasting.___ The spire walker's innate spellcasting ability is Charisma (spell save DC 12, +4 to hit with spell attacks). The spire walker can innately cast the following spells, requiring no material components:

* At will: _produce spark _(as the cantrip produce flame, but it does lightning damage)

* 3/day each: _dancing lights, feather fall, invisibility_

* 1/day each: _faerie fire, thunderwave_

___Steeple Step.___ The spire walker can use 10 feet of its movement to step magically from its position to the point of a steeple, mast, or other spire-like feature that is in view within 30 feet. The spire walker has advantage on Dexterity (Acrobatics) checks and Dexterity saving throws while it is standing on a steeple or any similar narrow, steep structure or feature.

**Actions**

___Lightning Dart.___ Ranged Weapon Attack: +6 to hit, range 20/60 ft., one target. Hit: 1 piercing damage plus 9 (2d8) lightning damage. If the attack misses, the target still takes 4 (1d8) lightning damage. Whether the attack hits or misses its intended target, every other creature within 10 feet of the target takes 9 (2d8) lightning damage, or half damage with a successful DC 14 Dexterity saving throw.

