"Eternal Flame Guardian";;;_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common"
_skills_: "Intimidation +3, Perception +2"
_speed_: "30 ft."
_hit points_: "45 (7d8+14)"
_armor class_: "17 (breastplate, shield; 15 while using a crossbow)"
_damage_resistances_: "fire"
_stats_: | 15 (+2) | 13 (+1) | 14 (+2) | 8 (-1) | 11 (0) | 13 (+1) |

___Flaming Weapon (Recharges after a Short or Long Rest).___ As a bonus action, the guard can wreath one melee weapon it is wielding in flame. The guard is unharmed by this fire, which lasts until the end of the guard's next turn. While wreathed in flame, the weapon deals an extra 3 (1d6) fire damage on a hit.

**Actions**

___Multiattack.___ The guard makes two melee attacks.

___Longsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +3 to hit, range 100/400 ft., one target. Hit: 6 (1d10 + 1) piercing damage.