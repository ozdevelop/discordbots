"Child Of The Briar";;;_size_: Tiny plant
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Briarclick, Common, Sylvan"
_skills_: "Perception +4, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_vulnerabilities_: "fire"
_speed_: "20 ft., climb 10 ft."
_hit points_: "50 (20d4)"
_armor class_: "13"
_stats_: | 6 (-2) | 17 (+3) | 11 (+0) | 13 (+1) | 10 (+0) | 14 (+2) |

___Fey Blood.___ Children of the briar count as both plant and fey for any effect related to type.

**Actions**

___Multiattack.___ A child of the briar makes two claw attacks. If both attacks hit the same target, the target is grappled (escape DC 13) and the child of the briar uses its Thorny Grapple on it.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Spitdart Tongue (Recharge 4-6).___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., one target. Hit: 6 (1d6 + 3) piercing damage. Every child of the briar can shoot thorns from its mouth.

___Entangle.___ Two children of the briar working together can cast a version of the entangle spell with no components, at will. Both creatures must be within 10 feet of each other, and both must use their action to cast the spell. The entangled area must include at least one of the casters but doesn't need to be centered on either caster. Creatures in the area must make a DC 13 Strength saving throw or be restrained. All children of the briar are immune to the spell's effects.

___Thorny Grapple.___ A child of the briar's long thorny limbs help it grapple creatures up to Medium size. A grappled creature takes 2 (1d4) piercing damage at the end of the child's turn for as long as it remains grappled.

