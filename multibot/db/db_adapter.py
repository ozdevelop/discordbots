import sqlite3
import json
import db.QUERIES as queries
from db.birthday import Birthday

database_file = ""
with open("config.json") as file:
    config = json.load(file)
database_file = config['birthday_databse']


def __create_connection():
    conn = sqlite3.connect(database_file)
    return conn


def create_tables():
    conn = __create_connection()
    cursor = conn.cursor()
    cursor.execute(queries.create_birthday_table)
    cursor.execute(queries.create_channel_data_table)
    conn.commit()
    cursor.close()
    conn.close()


def drop_tables():
    conn = __create_connection()
    cursor = conn.cursor()
    cursor.execute(queries.delete_birthday_table)
    cursor.execute(queries.delete_channel_table)
    conn.commit()
    cursor.close()
    conn.close()

# #############


def use_db(query, *args, get_data=False, get_only_one=False):
    conn = __create_connection()
    cursor = conn.cursor()
    print(args, query)
    test = query.format(args)
    print(test)
    q = query.split("%s")
    r = ""
    for i in range(len(q)-1):
        r += q[i] + '"'+str(args[i])+'"'
    r += q[-1]
    print(r)
    cursor.execute(r)
    data = None
    if get_data:
        if get_only_one:
            data = cursor.fetchone()
        else:
            data = cursor.fetchall()
    conn.commit()
    cursor.close()
    conn.close()
    return data


def create_birthday(user_id, day, month, year):
    use_db(queries.create_birthday, user_id, day, month, year)


def set_channel(server_id, channel_id):
    use_db(queries.set_channel, str(server_id), str(channel_id))


def get_birthday_all():
    rows = use_db(queries.get_birthday_all, get_data=True)
    birthdays = []
    for row in rows:
        birthdays.append(
            Birthday(user_id=row[0], day=row[1], month=row[2], year=row[3]))
    return birthdays


def get_birthday_one(user_id):
    birthday_data = use_db(queries.get_birthday_one,
                           user_id, get_data=True, get_only_one=True)
    result = None
    if birthday_data:
        result = Birthday(
            birthday_data[0], birthday_data[1], birthday_data[2], birthday_data[3])
    return result


def get_channel(server_id):
    return use_db(queries.get_channel, str(server_id), get_data=True, get_only_one=True)


def update_birthday(user_id, day, month, year):
    use_db(queries.update_birthday, month, day, year, user_id)


def update_channel(server_id, channel_id):
    use_db(queries.update_channel, str(channel_id), str(server_id))


def delete_channel(server_id):
    use_db(queries.delete_channel, str(server_id))


def delete_birthday(user_id):
    use_db(queries.delete_birthday, user_id)
